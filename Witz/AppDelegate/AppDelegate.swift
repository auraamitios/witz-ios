//
//  AppDelegate.swift
//  Witz
//
//  Created by Amit Tripathi on 9/5/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import KeychainSwift
import UserNotifications
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import AVFoundation
import AudioToolbox
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var locUpdate : LocationUpdate!
    
    fileprivate func setupKeyboardManager() {    
        
        // Setup IQKeyboardManager
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 8.0
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    }
     //MARK:- Google services initailize
    final fileprivate func initializeGoogleMapAPI() {GMSServices.provideAPIKey(GOOGLE_MAP_API_KEY)}
    final fileprivate func initializeGooglePlaceAPI() {GMSPlacesClient.provideAPIKey(GOOGLE_PLACES_API_KEY)}
    
    //MARK:- check login session
    fileprivate func checkLoginSession() {
        if (UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DriverAccessToken) != nil) {
            Utility.driverSideMenuSetUp()
        }else {
            if (UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.RiderAccessToken) != nil) {
                AppManager.sharedInstance.loggedUserRider = true
                //                Utility.getLoggedRiderDetail()
                Utility.riderSideMenuSetUp()
            }
        }
    }
    
    class func sharedInstance()->AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func getVendorID() {
        let keychain = KeychainSwift()
        let UUID = keychain.get(LinkStrings.KeychainKeys.UUID)
        if  UUID==nil {
            let vendorID = UIDevice.current.identifierForVendor!.uuidString
            //            print("Vendor ID>>>>>>\(vendorID)")
            keychain.set(vendorID, forKey: LinkStrings.KeychainKeys.UUID)
        }
    }
    
    func fetchUserCurrentLocation() {
        locUpdate = LocationUpdate.sharedInstance
        // locUpdate.grantLocationPermission()
        locUpdate.initiateTracking()
    }
    
    //MARK:- APNS Delegates Method(s)
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
        WitzAPNSManager.sharedInstance.isAPNSRequestGranted = true
        WitzAPNSManager.sharedInstance.setUpDeviceToken(deviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // The token is not currently available.
        WitzAPNSManager.sharedInstance.isAPNSRequestGranted = false
        //        print(" Remote notification support is unavailable due to error: \(error.localizedDescription) ")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(" Payload data\(userInfo)")
        let aps = userInfo[AnyHashable("aps")] as? [String:Any]
        let message = userInfo[AnyHashable("message")] as? [String:Any]
        
        print("data\(aps!)=======================> \(String(describing: message))")
        DispatchQueue.main.async {
            self.handleNotificationResponse(Notificaiton: userInfo)
        }
//        let actionHandler = { (action:UIAlertAction!) -> Void in
//
//        }
//        AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: (aps!["alert"] as? String)!, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: actionHandler)
        

        if aps?["content-available"] as? Int == 1 {
            
            completionHandler(.newData)
        } else  {
           
            completionHandler(.newData)
        }
    }
    
    //MARK:- ######################  Notification Handling   ##############
    
    lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Main, bundle: nil)
    lazy var storyBoardRider : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Rider, bundle: nil)
    
    func handleNotificationResponse(Notificaiton userInfo:[AnyHashable:Any]){
        
        let aps = userInfo[AnyHashable("aps")] as? [String:Any]
        let notificationData =  userInfo[AnyHashable("message")] as? [String:Any]
        let type  =  (notificationData!["flag"] as? Int ?? 0)
        let response = JSON.init(notificationData as Any)
        print(response)
        let modal =  InstantRidePushModal(json:response)
        switch type{
        case NotificationType.RIDE_REQUEST.rawValue :
            
            /////////// Sharing Instant Request
            if modal.isSharedTrip == true{
                let capacityLimit =  Int(DriverManager.sharedInstance.user_Driver.seating_capacity ?? 0)
                if DriverManager.sharedInstance.ignoreRequestNow == true || capacityLimit == DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0 {
                    DispatchQueue.global(qos: .background).async {
                        
                        let parameters =   ["tripId":modal.tripId ?? ""]
                        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/refuse_job_by_driver", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                        }, failure: { (error) in
                        })
                    }
                    
                }else{
                    if let rootViewControllerShare =  storyBoard.instantiateViewController(withIdentifier: "ShareRequestPopUpViewController") as? UINavigationController {
                        if let controller =   rootViewControllerShare.viewControllers[0] as? ShareRequestPopUpViewController{
                            controller.tripId =  modal.tripId
                        }
                        AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewControllerShare, animated: false, completion: {
                            
                        })
                    }
                }
            }else{    /////////// Instant Request
                if DriverManager.sharedInstance.ignoreRequestNow == true {
                    DispatchQueue.global(qos: .background).async {
                        
                        let parameters =   ["tripId":modal.tripId ?? ""]
                        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/refuse_job_by_driver", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                        }, failure: { (error) in
                        })
                    }
                }else{
                    if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "RideRequestPopUpViewController") as? UINavigationController {
                        if let controller =   rootViewController.viewControllers[0] as? RideRequestPopUpViewController{
                            controller.tripId =  modal.tripId
                        }
                        AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewController, animated: false, completion: {
                            
                        })
                    }
                }
            }
            break
            
        case NotificationType.DRIVER_DOCUMENT_ACTION_ADMIN.rawValue:
            
            if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "DocumentApprovalViewController") as? DocumentApprovalViewController {
                let navController = UINavigationController.init(rootViewController: rootViewController)
                navController.navigationBar.barTintColor = App_Text_Color
                rootViewController.modalDataPush = response
                AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
            }
            break
            
        case NotificationType.INFORM_DRIVER.rawValue:
            if let controller = AppDelegate.sharedInstance().window?.currentViewController() as? WaitForRiderViewController{
                controller.reloadTimer()
            }else if let controller1 = AppDelegate.sharedInstance().window?.rootViewController?.childViewControllers[1].presentedViewController as? WaitForRiderViewController{
                controller1.reloadTimer()
            }
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: (aps!["alert"] as? String)!, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
            break
            
        case NotificationType.BOOKING_REQUEST.rawValue:
            if checkForRunningTrip() == true{
                if let tripID = response["tripData"]["_id"].string{
                    
                    if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "ReservationRequestPopUpViewController") as? UINavigationController {
                        
                        if let controller =   rootViewController.viewControllers[0] as? ReservationRequestPopUpViewController{
                            controller.tripId =  tripID
                            let str_date = response["tripData"]["bookingDate"].string
                            controller.dateBooking = str_date
                        }
                        AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewController, animated: false, completion: { })
                    }
                }
            }
            break
            
        case NotificationType.RIDE_CANCEL_BY_RIDER.rawValue:
            
            if  DriverManager.sharedInstance.reservationTripGoingOn == true{
                DriverManager.sharedInstance.reservationTripGoingOn = false
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
            }else if DriverManager.sharedInstance.shareTripGoingOn == true {
                DriverManager.sharedInstance.rideRequestCanceled = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: RemoveOneRide), object: nil, userInfo: userInfo)
            } else{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismiss), object: nil, userInfo: userInfo)
            }
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: (aps!["alert"] as? String)!, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
            break
            
        case NotificationType.ACCEPT_BOOKING_OFFER.rawValue:
            if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "ReservationConfirmedViewController") as? UINavigationController {
                
                if let controller =   rootViewController.viewControllers[0] as? ReservationConfirmedViewController{
                    controller.dateToShow = response["tripData"]["bookingDate"].string?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? " "
                }
                AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewController, animated: false, completion: { })
            }
            break
            
        case NotificationType.RIDE_REQUEST_ACCEPTED_BY_DRIVER.rawValue:
            let dict =  notificationData?["tripData"] as? [String : Any] ?? ["":""]
            guard  let trip_id = dict["_id"] as? String  else{
                return
            }
            RiderManager.sharedInstance.tripIDForRider = trip_id
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.ShowDriverInfo), object: nil, userInfo: userInfo)
            break
            
        case NotificationType.NO_DRIVER.rawValue:
            print("no driver")
            
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message:LinkStrings.RiderAlerts.BusyCar, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.ShowRetryOption), object: nil, userInfo: userInfo)
            })
            
            break
            
        case NotificationType.RIDER_NOT_COMING.rawValue:
            print("Cancelation")
            
            Utility.riderSideMenuSetUp()
            break
            
        case NotificationType.DRIVER_REACHED_STARTING_LOCATION.rawValue:
            let dict =  notificationData?["data"] as? [String : Any] ?? ["":""]
            if let driver_id = dict["driverId"] as? String {
                RiderManager.sharedInstance.bookedDriverId = driver_id                
            }
            if let tripData = notificationData?["tripData"] as? [String : Any]{
                if let tripType = tripData["tripType"] as? String{
                    self.fillDriverInfo(data: userInfo)
                    if tripType == "3"{ //aggregate
                        guard let tripID = tripData["_id"] as? String else {return}
                        RiderManager.sharedInstance.tripIDForRider = tripID
                        if let rootViewController =  storyBoardRider.instantiateViewController(withIdentifier: "RiderOnTripViewController") as? RiderOnTripViewController {
                            let navController = UINavigationController.init(rootViewController: rootViewController)
                            navController.navigationBar.barTintColor = App_Text_Color
                            rootViewController.aggregateRideOn = true
                            if let pushTime = response["pushDateTime"].string?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                                let currentTime = Date()
                                rootViewController.timePassed = Int(currentTime.secondsSince(pushTime))}
                            AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
                        }
                        return
                    }
                    if tripType == "2"{ //reservation
                        
                        guard let tripID = tripData["_id"] as? String else {return}
                        RiderManager.sharedInstance.tripIDForRider = tripID
                        if let rootViewController =  storyBoardRider.instantiateViewController(withIdentifier: "RiderOnTripViewController") as? RiderOnTripViewController {
                            if let pushTime = response["pushDateTime"].string?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                                let currentTime = Date()
                                rootViewController.timePassed = Int(currentTime.secondsSince(pushTime))}
                            let navController = UINavigationController.init(rootViewController: rootViewController)
                            navController.isNavigationBarHidden = true
                            
                            //                            navController.navigationBar.barTintColor = App_Text_Color
                            AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
                        }
                        return
                    }
                    if tripType == "1"{ // instant
                        guard let tripID = tripData["_id"] as? String else {return}
                        RiderManager.sharedInstance.tripIDForRider = tripID
                        if let rootViewController =  storyBoardRider.instantiateViewController(withIdentifier: "RiderOnTripViewController") as? RiderOnTripViewController {
                            let navController = UINavigationController.init(rootViewController: rootViewController)
                            if let pushTime = response["pushDateTime"].string?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                                let currentTime = Date()
                                rootViewController.timePassed = Int(currentTime.secondsSince(pushTime))}
                            rootViewController.aggregateRideOn = true
                            //                            navController.navigationBar.barTintColor = App_Text_Color
                            navController.isNavigationBarHidden = true
                            AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
                        }
                        return
                    }
                    
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideTime), object: nil)
            
            break
            
        case NotificationType.REACHED_ARRIVAL_LOCATION.rawValue:
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: (aps!["alert"] as? String)!, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.Tripended), object: nil)
            break
        case NotificationType.RIDE_CANCEL_BY_DRIVER.rawValue:
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: (aps!["alert"] as? String)!, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
            Utility.riderSideMenuSetUp()
            break
            
        case NotificationType.DRIVER_START_TRIP.rawValue:
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: (aps!["alert"] as? String)!, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideStart), object: nil)
            break
            
        case NotificationType.NEW_AGGREGATE_REQUEST.rawValue:
            
            if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "AggregateRequestPopUpViewController") as? UINavigationController {
                if let controller =   rootViewController.viewControllers[0] as? AggregateRequestPopUpViewController{
                    controller.groupId = response["aggregateGroupId"].string
                }
                
                AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewController, animated: false, completion: {
                    
                })
            }
            break
        case NotificationType.FINAL_AGGREGATE_RIDE.rawValue:
            
            if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "AggregateRequestConfirmedViewController") as? UINavigationController {
                if let controller =   rootViewController.viewControllers[0] as? AggregateRequestConfirmedViewController{
                    controller.groupIdToSend = response["aggregateFinalTripId"].string
                }
                
                AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewController, animated: false, completion: {
                    
                })
            }
            break
        case NotificationType.AGGREGATE_BID_OFFERS_RIDER.rawValue :
            let storyBoardAgg = UIStoryboard(name: LinkStrings.StoryboardName.RiderAggregate, bundle: nil)
            
            if let rootViewController =  storyBoardAgg.instantiateViewController(withIdentifier: "ServiceReuestAggregateRiderViewController") as? ServiceReuestAggregateRiderViewController {
                let navController = UINavigationController.init(rootViewController: rootViewController)
                navController.navigationBar.barTintColor = App_Text_Color
                rootViewController.notificationID = response["notification_id"].string
                AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
            }
            break
        default:
            break
        }
    }
    

    
    //MARK:- UIApplication delegates
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UIBarButtonItem.appearance().tintColor = UIColor.white
        
        let accessToken = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DeviceToken)
        print(accessToken as AnyObject)
        if accessToken == nil {
            WitzAPNSManager.sharedInstance.requestAuthorization { (granted, error) in
            }
        }
        setupKeyboardManager()
        getVendorID()
        initializeGoogleMapAPI()
        initializeGooglePlaceAPI()
        fetchUserCurrentLocation()
        checkLoginSession()
        
        //Forcefully change language of app to English
        let languages  = UserDefaults.standard.object(forKey: "AppleLanguages") as? [String]
        if !(languages?.first == "en" ){
            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print("yes we are  going",#function)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print(#function)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print(#function)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        //DispatchQueue.main.async {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        /**************
         Check if there is any pending trip (app terminated ! resume the ride again)*******/
        
        if  UserDefaultManager.sharedManager.boolForKey(Key: LinkStrings.DriverOnTrip.TripPending) == true{
            
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/checkTrip", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
                
                UserDefaultManager.sharedManager.removeValue(key: LinkStrings.DriverOnTrip.TripPending) // Regain the access to pending trip now
                
                let modal = TripPendingBackgorundModal(json: response?["data"] ?? ["":""])
                if modal.tripStatus == true{
                    DriverManager.sharedInstance.trip_Pending = modal
                    //                // store the trip detail in another modal as well to avoid the check in flow of ride time
                    DriverManager.sharedInstance.ride_Details = TripModalDriver(json: response?["data"] ?? ["":""])
                    print("yes we are in trip",#function)
                    
                    if DriverManager.sharedInstance.trip_Pending?.isSharedTrip == true{
                        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/get_shared_trip_details_full", withInputString: ["tripId":modal.tripData?.id ?? ""], requestType: .post, isAuthorization: true, success: { (response) in
                            if let data = response?["data"]["logsData"]["sharingInfoData"].array{
                                if data.count > 0 {
                                    for dict in data{
                                        if ((DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0) > 0) {
                                            DriverManager.sharedInstance.arr_ShareTrips?.append(TripModalDriver(json:dict))
                                        }else{
                                            DriverManager.sharedInstance.arr_ShareTrips = [TripModalDriver(json:dict)]
                                        }
                                    }
                                }
                                print(data)
                                if let rootViewControllerShare =  self.storyBoard.instantiateViewController(withIdentifier: "ShareRequestPopUpViewController") as? UINavigationController {
                                    if let controller =   rootViewControllerShare.viewControllers[0] as? ShareRequestPopUpViewController{
                                        controller.tripId =  modal.tripData?.id
                                        controller.isTripPending = true
                                    }
                                    AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewControllerShare, animated: false, completion: { })
                                }
                            }
                        }) { (error) in}
                    }else{
                        if let rootViewController =  self.storyBoard.instantiateViewController(withIdentifier: "RideRequestPopUpViewController") as? UINavigationController {
                            if let controller =   rootViewController.viewControllers[0] as? RideRequestPopUpViewController{
                                controller.tripId =  modal.tripData?.id
                                controller.isTripPending = true
                            }
                            AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewController, animated: false, completion: { })
                        }
                        print(#function)
                    }
                }
            }) { (error) in}
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        if checkForRunningTrip() == false{
            UserDefaultManager.sharedManager.setBool(key: LinkStrings.DriverOnTrip.TripPending) // driver on trip store in userdefault
        }
        print(#function)
    }
    
     //MARK:- Some utility func
    /**
     This method will return if Driver completing one of trip or idle
     */
    func  checkForRunningTrip() -> Bool{
        if DriverManager.sharedInstance.isDriverOnTrip == true{
            return false // driver on trip
            
        }
        if DriverManager.sharedInstance.reservationTripGoingOn == true{
            return false // driver on trip
            
        }
        if DriverManager.sharedInstance.shareTripGoingOn == true{
            return false // driver on trip
            
        }
        return true // driver idle now
        
    }
    
    func fillDriverInfo(data:Any) {
        let result:[String:Any] = data as! [String:Any]
        //        print("data hereqqqqq-----***** \(result)")
        
        let message = result["message"] as? [String:Any]
        let detail = message!["data"] as? [String:Any] ?? ["":""]
        let personalInfo = detail["personal_information"] as? [String:Any] ?? ["":""]
        let photo = detail["pic"] as? [String:Any] ?? ["":""]
        let vehicleInfo = detail["vehicle_information"] as? [String:Any] ?? ["":""]
        let dict = message!["tripData"] as? [String:Any] ?? ["":""]
        let driverMobile = detail["mobile"] as? String ?? ""
        let valueName = (personalInfo["fullName"] as? String ?? "").nameFormatted
        let valueModel = vehicleInfo["model"] as? String ?? ""
        let valuePlateNumber = vehicleInfo["vehicle_number"] as? String ?? ""
        let valueRating = "\((detail["avgRating"] as? Double ?? 0).rounded())"
        if let driverID = detail["_id"] as? String {
            RiderManager.sharedInstance.bookedDriverId = driverID
        }
        let km = "\((dict["driverReachedKM"] as? Double ?? 0)/1000)"
        let time = "\((dict["driverReachedTime"] as? Double ?? 0)/60)"
        let imgUrl = "\(kBaseImageURL)\(photo["original"] ?? "")"
        RiderManager.sharedInstance.bookedDriverProfile =  DriverBasicModal.init(name: valueName , image: imgUrl, km: km , min:  time ,rating: valueRating ,vehicleName: valueModel ,vehicleNumber: valuePlateNumber ,mobileNumber: driverMobile)
    }
    
}

