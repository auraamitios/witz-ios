//
//  ViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/5/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class ViewController: WitzSuperViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func action_RiderBtn(_ sender: Any) {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let riderVC : RiderLoginViewController = storyBoard.instantiateViewController(withIdentifier: "RiderLoginViewController") as! RiderLoginViewController
        self.navigationController?.pushViewController(riderVC, animated: true)
        
    }

    @IBAction func action_DriverBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "loginVC", sender: nil)
    }
    
}

