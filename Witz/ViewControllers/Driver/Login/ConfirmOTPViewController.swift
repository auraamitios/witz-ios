//
//  ConfirmOTPViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/5/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire
class ConfirmOTPViewController: WitzSuperViewController {

    @IBOutlet fileprivate weak var txtFirst: SquareTextField!
    @IBOutlet fileprivate weak var txtSecond: SquareTextField!
    @IBOutlet fileprivate weak var txtThrid: SquareTextField!
    @IBOutlet fileprivate weak var txtFourth: SquareTextField!
    @IBOutlet fileprivate weak var btnConfirmOTP: UIButton!
    
  fileprivate  let objWitzConnectionManager = WitzConnectionManager()
    
    var strMobile : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.objWitzConnectionManager.delegate = self
        toggleCreateBtnState(false)
        //self.performSegue(withIdentifier: "CreatePassword", sender: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //self.navigationController?.navigationBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OTPValidation()->Bool {
        if (txtFirst.text?.isEmpty)! || (txtSecond.text?.isEmpty)! || (txtThrid.text?.isEmpty)! || (txtFourth.text?.isEmpty)! {
            return false
        }else {
            return true
        }
    }
    
    func createOTPString()->String {
        let appendString = txtFirst.text! + txtSecond.text! + txtThrid.text! + txtFourth.text!
        print("OTP string>>>>>>\(appendString)")
        return appendString
    }
    
    func toggleCreateBtnState(_ enable:Bool) {
        if enable {
            self.btnConfirmOTP.alpha = 1.0
            self.btnConfirmOTP.isEnabled = true
        }else {
            self.btnConfirmOTP.alpha = 0.5
            self.btnConfirmOTP.isEnabled = false
        }
    }
    //MARK:: IBOutlet actions
    @IBAction func action_DoneBtn(_ sender: Any) {
        guard OTPValidation() else {
            return
        }
        OTPVerification(createOTPString())
    }
    
    @IBAction func action_Txtfield(_ sender: SquareTextField) {
        switch sender.tag {
        case 1:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtSecond.becomeFirstResponder()
            }
            break
        case 2:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtThrid.becomeFirstResponder()
            }else{
                txtFirst.becomeFirstResponder()
            }
            break
        case 3:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtFourth.becomeFirstResponder()
            }else{
                txtSecond.becomeFirstResponder()
            }
            break
        case 4:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtFourth.resignFirstResponder()
            }else{
                txtThrid.becomeFirstResponder()
            }
            break
        default:
            break
        }
    }
    
    @IBAction func sendPinAgain(_ sender: UIButton) {
        self.reSendOtp()
    }
    
    
    //MARK::API's call
    fileprivate func OTPVerification (_ otpCode:String) {
        
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        let params: Parameters = ["mobile":strMobile,"type":"normal", "code":otpCode]
        print(params)
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/confirm_mobile_verification", withInputString: params, requestType: .post, withCurrentTask: .Confirm_Mobile_Verification_Driver, isAuthorization: false)
        
    }

    fileprivate func reSendOtp(){
        
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let parameters = ["mobile":strMobile ?? "","type":"normal"]
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/mobile_verification", withInputString: parameters, requestType: .post, isAuthorization: false, success: { (response) in
                    self.hideLoader()
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:"Code Sent" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                }, controller: self)
                
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ConfirmOTPViewController:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        var pinFlag = true
        switch textField.tag {
        case 1:
            pinFlag = true
            break
        case 2:
            if self.txtFirst.text == "" {
                pinFlag = false
            }
            break
        case 3:
            if self.txtFirst.text == "" || self.txtSecond.text == "" {
                pinFlag = false
            }
            break
        case 4:
            if self.txtFirst.text == "" || self.txtSecond.text == "" || self.txtThrid.text == "" {
                pinFlag = false
            }
            break
        default:
            break
        }
        return pinFlag
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
        switch textField.tag {
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            break
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            return true
        }
        let limitLength = 1
        let newLength = text.count + string.count - range.length
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
         toggleCreateBtnState(false)
        if textField.tag == 4 {
            if newLength > 0 {
                toggleCreateBtnState(true)
            }else {
                toggleCreateBtnState(false)
            }
        }

        return allowedCharacters.isSuperset(of: characterSet) && newLength <= limitLength
        
        //        let limitLength = 1
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        return newLength <= limitLength
    }
    
}

extension ConfirmOTPViewController:WitzConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        print("data\(sender)")
        switch serviceType {
        case .Confirm_Mobile_Verification_Driver:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                self.performSegue(withIdentifier: "CreatePassword", sender: nil)
            }else {
                let actionHandler = { (action:UIAlertAction!) -> Void in
                }
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            }
            break
            
        default:
            break
        }
    }
    func didFailWithError(sender: AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}

