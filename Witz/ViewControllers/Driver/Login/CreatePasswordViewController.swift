//
//  CreatePasswordViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/6/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD
import KeychainSwift
class CreatePasswordViewController: WitzSuperViewController {
    
    let objWitzConnectionManager = WitzConnectionManager()
    
    @IBOutlet weak var txtPassword: TextFieldInsets!
    @IBOutlet weak var txtRePassword: TextFieldInsets!
    @IBOutlet weak var btnCreatePassword: UIButton!
    @IBOutlet weak var txt_Code: TextFieldInsets!
    @IBOutlet weak var view_TxtCode: UIView!
    @IBOutlet weak var constraint_Top_BtnCreate: NSLayoutConstraint!
    
    var isForgot : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.objWitzConnectionManager.delegate = self
        if isForgot == true{
            view_TxtCode.isHidden = true
            constraint_Top_BtnCreate.constant = -25
        }
        toggleCreateBtnState(false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK::API's call
    fileprivate func forgotPassword (_ password:String) {
        
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        showLoader()
        let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        let params: Parameters = ["mobile":strMobile, "password":password]
        print(params)
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/reset_password", withInputString: params, requestType: .post, withCurrentTask: .Forgot_Password, isAuthorization: false)
        
    }
    
    fileprivate func createNewPassword (_ password:String) {
        
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        showLoader()
        let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        let keychain = KeychainSwift()
        let UUID = keychain.get(LinkStrings.KeychainKeys.UUID)
        var params: Parameters = ["mobile":strMobile,"deviceType":"0", "password":password, "deviceToken":UUID!]
        if txt_Code.text?.isBlank() == false{
            params["promotioncode"] = txt_Code.text
        }
        print(params)
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/create", withInputString: params, requestType: .post, withCurrentTask: .Create_Password_Driver, isAuthorization: false)
        
    }
    
    func validateReTypePassword()->Bool {
        if txtPassword.text != txtRePassword.text {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.SignIn.RePassword, style: .alert, actionButtonTitle: "Ok", completionHandler: actionHandler, controller: self)
            return false
        }
        return true
    }
    
    func validationCheck()->Bool {
        if (txtPassword.text?.isEmpty)! || (txtRePassword.text?.isEmpty)! {
            return false
        }
        return true
    }
    
    func popToDriverLoginScreen() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is DriverLoginViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func toggleCreateBtnState(_ enable:Bool) {
        if enable && (txtPassword.text?.length ?? 0) > 5{
            self.btnCreatePassword.alpha = 1.0
            self.btnCreatePassword.isUserInteractionEnabled = true
        }else {
            self.btnCreatePassword.alpha = 0.5
            self.btnCreatePassword.isUserInteractionEnabled = false
        }
    }
    
    
    //MARK::IBOutlets actions
    
    
    @IBAction func action_Referal(_ sender: UIButton) {
        let storyBoardRider : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let controller = storyBoardRider.instantiateViewController(withIdentifier: "RiderPromotionCodeViewController") as! RiderPromotionCodeViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller)
    }
    
    @IBAction func action_CreatePassword(_ sender: Any) {
        guard validationCheck() else {return}
        guard validateReTypePassword()else {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.SignIn.RePassword, style: .alert, actionButtonTitle: "Ok", completionHandler: actionHandler, controller: self)
            return
        }
        if isForgot == true{
            forgotPassword(txtPassword.text!)       // forgot passWord
        }else{
            createNewPassword(txtPassword.text!)   // create new password
        }
    }
}

extension CreatePasswordViewController:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            return true
        }
        if textField.tag == 2 {
            let newLength = text.count + string.count - range.length
            if newLength < 6 {
                toggleCreateBtnState(false)
            }else {
                toggleCreateBtnState(true)
            }
        }
        return true
    }
}

extension CreatePasswordViewController:WitzConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        
        print("data\(sender)")
        switch serviceType {
        case .Forgot_Password:
            //popToDriverLoginScreen()
            
            let result = sender as! JSON
            let actionHandler = { (action:UIAlertAction!) -> Void in
                
                UserDefaultManager.sharedManager.removeValue(key:LinkStrings.UserDefaultKeys.DriverAccessToken)
                Utility.jumpToLoginScreen()
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            break
            
        case .Create_Password_Driver:
            //popToDriverLoginScreen()
            
            let result = sender as! JSON
            let actionHandler = { (action:UIAlertAction!) -> Void in
                Utility.driverSideMenuSetUp()
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            break
            
            
        default:
            break
        }
    }
    func didFailWithError(sender: AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}

extension CreatePasswordViewController : RiderPromotionCodeDelegate{
    
    func referCodeRecieved(_ str: String) {
        self.txt_Code.text = str
    }
}
