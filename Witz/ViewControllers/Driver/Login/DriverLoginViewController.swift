//
//  DriverLoginViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/5/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class DriverLoginViewController: WitzSuperViewController {

    @IBOutlet weak var logoTopLayout: NSLayoutConstraint!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet var countryView: UIView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgFlag: UIImageView!
    var countryCode = ""
    var flagSelected = FlagType.TURKEY
    
    let objWitzConnectionManager = WitzConnectionManager()
    var mobileStr:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.txtMobileNo.setPlaceholder(placeholderString: "Mobile number", color: UIColor.white)
        self.txtMobileNo.delegate = self
        
        self.objWitzConnectionManager.delegate = self
        countryCode = "+90"
        
//        do {
//            try FileManager.default.createDirectory(
//                at: NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("upload")!,
//                withIntermediateDirectories: true,
//                attributes: nil)
//        } catch {
//            print("Creating 'upload' directory failed. Error: \(error)")
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK::API's call
    fileprivate func mobileVerification (_ number:String) {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        var no = number.withoutSpacesAndNewLines
        no = countryCode+no
        mobileStr = no // store for future use
        showLoader()
        UserDefaultManager.sharedManager.addValue(object: no as AnyObject, key: LinkStrings.UserDefaultKeys.MobileNo)
        let params: Parameters = ["mobile":no,"type":"normal"]
        print(params)
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/mobile_verification", withInputString: params, requestType: .post, withCurrentTask: APIType.Mobile_Verification_Driver, isAuthorization: false)
        
    }
    

    
    @IBAction func action_CountryBtn(_ sender: Any) {
        
//        let bezierParams = UICubicTimingParameters(controlPoint1: CGPoint(x: 0.05, y: 0.95),
//                                                   controlPoint2: CGPoint(x: 0.15, y: 0.95))
//
//        let animator = UIViewPropertyAnimator(duration: 4, timingParameters:bezierParams)
//
//        animator.addAnimations {
//
//            self.countryView.frame = CGRect(x: 20, y: Utility.windowHeight()-80, width: Utility.windowWidth()-60, height: 80)
//            self.view.addSubview(self.countryView)
//        }
//
//        animator.startAnimation()
//        UIViewPropertyAnimator(duration: 1, curve: .easeIn) {
//            self.countryView.frame = CGRect(x: 20, y: Utility.windowHeight()-350, width: Utility.windowWidth()-60, height: 80)
//            self.view.addSubview(self.countryView)
//            }.startAnimation()
        
        self.countryView.frame = CGRect(x: 30, y: Utility.windowHeight()-350, width: Utility.windowWidth()-60, height: 120)
        self.view.addSubview(self.countryView)
        
    }
    @IBAction func action_India(_ sender: Any) {
        countryCode = "+91"
        self.lblCountryCode.text = "+91"
        imgFlag.image = #imageLiteral(resourceName: "India")
        flagSelected = FlagType.INDIA
        UIViewPropertyAnimator(duration: 1, curve: .easeOut) {
            self.countryView.removeFromSuperview()
            }.startAnimation()
    }
    
    @IBAction func action_Turkey(_ sender: Any) {
        countryCode = "+90"
        self.lblCountryCode.text = "+90"
         imgFlag.image = #imageLiteral(resourceName: "tr_flag")
        flagSelected = FlagType.TURKEY
        UIViewPropertyAnimator(duration: 1, curve: .easeOut) {
            self.countryView.removeFromSuperview()
            }.startAnimation()
    }
    
    @IBAction func action_SaudiArabia(_ sender: UIButton) {
        countryCode = "+966"
        self.lblCountryCode.text = "+966"
        imgFlag.image = #imageLiteral(resourceName: "Flag_of_Saudi_Arabia")
        flagSelected = FlagType.SAUDI
        UIViewPropertyAnimator(duration: 1, curve: .easeOut) {
            self.countryView.removeFromSuperview()
            }.startAnimation()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "OTPVC" {
            let controller = segue.destination as! ConfirmOTPViewController
            controller.strMobile = mobileStr
        }
    }
    

}

extension DriverLoginViewController:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        logoTopLayout.constant = 2
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        logoTopLayout.constant = 143
        var trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
        trimmed = trimmed.withoutSpacesAndNewLines
        if flagSelected == .SAUDI{
            if trimmed.length >= 9{
                mobileVerification(trimmed)
            }
        }
        else if trimmed.length == 10 {
             mobileVerification(trimmed)
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }

        let limitLength = 13
        let newLength = text.count + string.count - range.length
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        if string.count>0 {
            if newLength == 4 || newLength == 8 || newLength == 11{
                textField.text = textField.text?.appending(" ")
            }
        }
        return allowedCharacters.isSuperset(of: characterSet) && newLength <= limitLength
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
}

extension DriverLoginViewController:WitzConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        hideLoader()
        
        print("data\(sender)")
        switch serviceType {
        case .Mobile_Verification_Driver:
            let result = sender as! JSON
            if result["statusCode"] == 400 {
                let data = result["data"].dictionary
                if data?["isUserAvail"] == true {
                    self.performSegue(withIdentifier: "AskPassword", sender: nil)
                }else {
                    //self.performSegue(withIdentifier: "OTPVC", sender: nil)
                }
            }else {
//                let actionHandler = { (action:UIAlertAction!) -> Void in
//                    
//                }
//                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
                self.performSegue(withIdentifier: "OTPVC", sender: nil)
            }
            
            break
            
        default:
            break
        }
    }
    func didFailWithError(sender: AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        hideLoader()
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}
