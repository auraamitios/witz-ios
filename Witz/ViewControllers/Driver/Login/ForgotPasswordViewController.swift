//
//  ForgotPasswordViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class ForgotPasswordViewController: WitzSuperViewController ,UITextFieldDelegate{
    
    let objWitzConnectionManager = WitzConnectionManager()
    
    @IBOutlet weak var txtFirst: SquareTextField!
    @IBOutlet weak var txtTwo: SquareTextField!
    @IBOutlet weak var txtThree: SquareTextField!
    @IBOutlet weak var txtFour: SquareTextField!
    @IBOutlet weak var btnOTP: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.objWitzConnectionManager.delegate = self
        forgotPassword()
    }
    
    //MARK::API's call
    fileprivate func forgotPassword () {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        let params: Parameters = ["mobile":strMobile,"type":"forgot"]
        print(params)
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/forgot_password", withInputString: params, requestType: .post, withCurrentTask: .Forgot_Password, isAuthorization: false)
        
    }
    
    @IBAction func action_OTPFields(_ sender: SquareTextField) {
        switch sender.tag {
        case 1:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtTwo.becomeFirstResponder()
            }
            break
        case 2:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtThree.becomeFirstResponder()
            }else{
                txtFirst.becomeFirstResponder()
            }
            break
        case 3:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtFour.becomeFirstResponder()
            }else{
                txtTwo.becomeFirstResponder()
            }
            break
        case 4:
            if (sender.text?.length)! > 0 && sender.text != " " {
                txtFour.resignFirstResponder()
            }else{
                txtThree.becomeFirstResponder()
            }
            break
        default:
            break
        }
    }
    
    @IBAction func action_OTPBtn(_ sender: UIButton) {
        
        if OTPValidation() == true{
            view.endEditing(true)
            guard self.checkNetworkStatus() else {
                return
            }
            showLoader()
            let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/confirm_mobile_verification", withInputString: ["mobile":strMobile,"type":"forgot", "code":createOTPString()], requestType: .post, isAuthorization: false, success: { (response) in
                
                self.hideLoader()
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    if response?["statusCode"] == 200 {
                        self.performSegue(withIdentifier: "NewPassword", sender: nil)
                    }
                }, controller: self)
             }) { (error) in
                self.hideLoader()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OTPValidation()->Bool {
        if (txtFirst.text?.isEmpty)! || (txtTwo.text?.isEmpty)! || (txtThree.text?.isEmpty)! || (txtFour.text?.isEmpty)! {
            return false
        }else {
            return true
        }
    }
    
    func createOTPString()->String {
        let appendString = txtFirst.text! + txtTwo.text! + txtThree.text! + txtFour.text!
        print("OTP string>>>>>>\(appendString)")
        return appendString
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        var pinFlag = true
        switch textField.tag {
        case 1:
            pinFlag = true
            break
        case 2:
            if self.txtFirst.text == "" {
                pinFlag = false
            }
            break
        case 3:
            if self.txtFirst.text == "" || self.txtTwo.text == "" {
                pinFlag = false
            }
            break
        case 4:
            if self.txtFirst.text == "" || self.txtTwo.text == "" || self.txtThree.text == "" {
                pinFlag = false
            }
            break
        default:
            break
        }
        return pinFlag
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
        switch textField.tag {
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            break
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            return true
        }
        let limitLength = 1
        let newLength = text.count + string.count - range.length
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        return allowedCharacters.isSuperset(of: characterSet) && newLength <= limitLength
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! CreatePasswordViewController
        controller.isForgot = true
    }
}

extension ForgotPasswordViewController:WitzConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        
        print("data\(sender)")
        switch serviceType {
        case .Forgot_Password:
            
            let result = sender as! JSON
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            break
            
        default:
            break
        }
    }
    func didFailWithError(sender: AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}
