//
//  AskPasswordViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import KeychainSwift

class AskPasswordViewController: WitzSuperViewController {
    
    let objWitzConnectionManager = WitzConnectionManager()

    @IBOutlet weak var txtPassword: TextFieldInsets!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.objWitzConnectionManager.delegate = self
        txtPassword.becomeFirstResponder()
    }
    
    //MARK::API's call
    fileprivate func createDriver (_ password:String) {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        
        var accessToken = ""
        if (UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DeviceToken) != nil) {
            accessToken = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DeviceToken) as! String
        }else {
            let keychain = KeychainSwift()
            accessToken = keychain.get(LinkStrings.KeychainKeys.UUID)!
        }
        let locations = AppManager.sharedInstance.locationCoordinates
        let coordinates = locations.last?.coordinate
        let params: Parameters = ["mobile":strMobile,"deviceType":"0", "password":password, "lat":(coordinates?.latitude)!, "lng":(coordinates?.longitude)!, "deviceToken":accessToken]
        print(params)
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/login", withInputString: params, requestType: .post, withCurrentTask: .Driver_Login, isAuthorization: false)
    }

    @IBAction func action_ForgotPassword(_ sender: Any) {
        self.performSegue(withIdentifier: "Forgot", sender: nil)
    }
    @IBAction func action_LoginBtn(_ sender: Any) {
        guard !self.txtPassword.isEmpty else {
            return
        }
        createDriver(self.txtPassword.text!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AskPasswordViewController:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
}

extension AskPasswordViewController:WitzConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        
        print("data\(sender)")
        switch serviceType {
        case .Driver_Login:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                Utility.driverSideMenuSetUp()
            }else {
                let actionHandler = { (action:UIAlertAction!) -> Void in
                    //                self.performSegue(withIdentifier: "LandingHomeVC", sender: nil)
                }
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            }
            break
            
        default:
            break
        }
    }
    func didFailWithError(sender: AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}

