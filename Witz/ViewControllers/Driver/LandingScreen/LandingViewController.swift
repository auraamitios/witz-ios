//
//  LandingViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/8/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import SevenSwitch
import GoogleMaps
import SwiftyJSON

class LandingViewController: WitzSuperViewController {
    

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var bgView1: UIView!
    @IBOutlet weak var bgView2: UIView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_VehicleName: UILabel!
    @IBOutlet weak var lbl_PlateNumber: UILabel!
    @IBOutlet weak var lbl_Rating: UILabel!
    
    let objWitzConnectionManager = WitzConnectionManager()
    var locUpdate:LocationUpdate!
    var marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    let onlineSwitch = SevenSwitch(frame: CGRect(x: 0, y: 0, width: 110, height: 35))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetUp()
//        print("\(#function) -- \(self)")
//        print(#function)
        
        DispatchQueue.global(qos: .background).async {
            self.loadDriver()
        }
        
        self.objWitzConnectionManager.delegate = self
        self.mapView.addSubview(self.bottomView)
        locUpdate = LocationUpdate.sharedInstance
        locUpdate.delegate = self
        locUpdate.startTracking()
        
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //routeCalculation()
        AppManager.sharedInstance.loggedUserRider = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        displayUserCurrentLocation()
    }
    
    func initialSetUp() {
        
        onlineSwitch.center = CGPoint(x: self.view.bounds.size.width * 0.5, y: self.view.bounds.size.height * 0.5)
        onlineSwitch.addTarget(self, action: #selector(LandingViewController.switchChanged(_:)), for: UIControlEvents.valueChanged)
        onlineSwitch.on = false
        onlineSwitch.onImage = #imageLiteral(resourceName: "onlineicon")
        onlineSwitch.offImage = #imageLiteral(resourceName: "offlineicon")
        onlineSwitch.onTintColor = App_Base_Color
        onlineSwitch.inactiveColor = UIColor.lightGray
        self.navigationItem.titleView = onlineSwitch
        self.setNavigationBarItem()
    }
    
    func mapSetUp() {
        #if (arch(i386) || arch(x86_64)) && (os(iOS) || os(watchOS) || os(tvOS))
            print(" Running Simulator ")
        #else
            print(" Running on Device ")
        #endif

        self.mapView.addSubview(self.bottomView)
    }
    
    func displayUserCurrentLocation() {
        let locations = AppManager.sharedInstance.locationCoordinates
        guard let coordinates = locations.last?.coordinate else {
            return
        }
        self.mapView.camera = GMSCameraPosition(target: coordinates, zoom: 15, bearing: 0, viewingAngle: 0)
        let position = CLLocationCoordinate2D(latitude: (coordinates.latitude), longitude:(coordinates.longitude))
        marker.position = position
        marker.title = "Witz"
        marker.map = self.mapView
        marker.appearAnimation = GMSMarkerAnimation.pop
       marker.icon = DriverManager.sharedInstance.VehicleOwnedbyDriver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    //MARK:: custom methods 
    
    func switchChanged(_ sender: SevenSwitch) {
        print("\(#function) -- \(self)")
        print("Changed value to: \(sender.on)")
        if sender.on == true{
            change_Duty("Idle")
        }else{
            change_Duty("Offline")
            
        }
    }
    
    func user_GetVerified(completed:Bool ) {
        
        self.bgView2.isHidden = !completed
        self.bgView1.isHidden = completed
        self.view.layoutIfNeeded()
    }
    
    //MARK::API's call
    fileprivate func change_Duty(_ str : String) {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/change_duty_status", withInputString: ["status":str], requestType: .post, withCurrentTask: .Driver_Duty , isAuthorization: true)
    }
    
    //MARK::API's call
    public func loadDriver() {
        
        guard self.checkNetworkStatus() else {return}
        var localTimeZoneName: String { return TimeZone.current.identifier }
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/accesstokenlogin", withInputString: ["timezone":localTimeZoneName], requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true)
    }
    
    func menuAction() {
    }
    
    //MARK::IBOutlets methods
    @IBAction func action_completeRegistration(_ sender: Any) {
        self.performSegue(withIdentifier: "Individual", sender: nil)
        //FIXME::---->>>>
        //        self.performSegue(withIdentifier: "Individua", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    fileprivate func updateDriverLocation(){
        let locations = AppManager.sharedInstance.locationCoordinates
        let coordinates = locations.last?.coordinate
        let parameters = ["lat":(coordinates?.latitude ?? 0),"lng":(coordinates?.longitude ?? 0),"arrivalTime":"0","arrivalDistance":"0"] as [String : Any]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/update_location", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
            
        }) { (error) in
            
        }
    }
}

extension LandingViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {}
    func leftDidOpen() {}
    func leftWillClose() {}
    func leftDidClose() {}
    func rightWillOpen() {}
    func rightDidOpen() {}
    func rightWillClose() {}
    func rightDidClose() {}
}
//MARK:: Location delegates
extension LandingViewController:LocationDelegates {
    
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = false
            
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            marker.position = position
            marker.title = "Witz"
            marker.map = self.mapView
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.icon = DriverManager.sharedInstance.VehicleOwnedbyDriver()
//                UIImage(named: "vehicle-car_on-map")
            locUpdate.stopTracking()
        }
    }
    
    func didFailToUpdate(error: Error) {
        
    }
}

extension LandingViewController:WitzConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
//        print("data\(sender)")
        switch serviceType {
        case .Driver_Duty:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                
                
            }else {
                
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    
                }, controller: self)
            }
            break
  
        case .Driver_Info:
            let result = sender as! JSON
            DispatchQueue.main.async {
                if result["statusCode"] == 200 {
                    // check driver status
                    if let driver = responseData as? DriverInfo {
                        DriverManager.sharedInstance.ignoreRequestNow = false  // New request  allowed
                        DriverManager.sharedInstance.isDriverOnTrip = false
                        DriverManager.sharedInstance.reservationTripGoingOn = false
                        DriverManager.sharedInstance.shareTripGoingOn = false
                        
                        DriverManager.sharedInstance.user_Driver = driver
                        
                        self.lbl_Name.text = driver.driver_fullName
                        self.lbl_VehicleName.text = (driver.subBrand ?? "")+" "+(driver.model ?? "")
                        self.lbl_Rating.text = "\(String(format: "%.1f",driver.avgRating ?? 0.0))"
                        self.lbl_PlateNumber.text = "- \(driver.vehicle_number ?? "")"
                        if driver.isVerified == true{
                            self.user_GetVerified(completed: true) // verified driver
                            self.onlineSwitch.on = true
                            self.onlineSwitch.isUserInteractionEnabled = true
                        }else{
                            self.user_GetVerified(completed: false)
                            self.onlineSwitch.on = false  // not allowed to be online untill verified
                            self.onlineSwitch.isUserInteractionEnabled = false
                            return
                        }
                        
                        if  driver.status == "Offline"{
                            self.onlineSwitch.on = false
                        }else
                        {
                            self.onlineSwitch.on = true  // idle state
                        }
                       
                        DispatchQueue.global(qos: .background).async {
                            self.updateDriverLocation()
                        }
                      
                    }
                }else {
                    
                    AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                        
                    }, controller: self)
                }
            }
            break
        default:
            break
        }
    }
    
    func didFailWithError(sender: AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        hideLoader()
        if sender is ServerError {
            let error = sender as! ServerError
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            }, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            }, controller: self)
        }else {
            let errorMsg = sender as! String
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            }, controller: self)
        }
    }
    
}
