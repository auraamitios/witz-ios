//
//  WaitForRiderViewController.swift
//  Witz
//
//  Created by abhishek kumar on 06/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class WaitForRiderViewController: UIViewController {
    
    @IBOutlet weak var lbl_Heading: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    var timeLeftToAccept = 180
    var min = 2
    var sec = 59
    fileprivate var task : DispatchWorkItem?
    
    @IBOutlet weak var btn_GoToTravel: UIView!
    var notAllowToStartRide = true
    
    @IBOutlet weak var lbl_ShowWaitAlert: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if DriverManager.sharedInstance.shareTripGoingOn == true{ // Shared Instant trip
            NotificationCenter.default.addObserver(self, selector: #selector(removeRide), name: NSNotification.Name(rawValue: RemoveOneRide), object: nil)
        }
        //  if driver do nothing
        task = DispatchWorkItem {
            self.cancelTask(cancelRide: true)
        }
        
        // Driver has 3 min to accept the ride
        Utility.stopTimer(after: TimeInterval(timeLeftToAccept), task)
        Utility.delegate = self
        let alert =   UIAlertController.init(title: LinkStrings.App.AppName, message: LinkStrings.DriverOnTrip.WaitRider , preferredStyle: .alert)
        let action = UIAlertAction.init(title: LinkStrings.MostCommon.Ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc fileprivate func removeRide(object:NSNotification){
        
        if let result:[String:Any] = object.userInfo as? [String:Any] {
            //    let result:[String:Any] = object.userInfo as! [String:Any]
            let message : [String:Any] = result["message"] as! [String:Any]
            print("data hereqqqqq-----***** \(message)")
            
            let riderData = message["riderData"] as? [String:Any] ?? ["":""]
            let tripData = message["tripData"] as? [String:Any] ?? ["":""]
            if let riderIdCheck =  riderData["_id"] as? String{
                if let tripIdCheck = tripData["_id"] as? String{
                    if DriverManager.sharedInstance.tripId == tripIdCheck && DriverManager.sharedInstance.ride_Details?.riderData?.id == riderIdCheck{
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    fileprivate func cancelTask(cancelRide:Bool){
        Utility.stopTimer()
        self.task?.cancel()
        if cancelRide == true{
            //            performSegue(withIdentifier: "segue_RideCancelByRider", sender: nil)
            self.riderNotReached()
        }
    }
    
    public func reloadTimer(){
        //        btn_GoToTravel.isUserInteractionEnabled = true
        notAllowToStartRide = false
        let alert = UIAlertController.init(title: LinkStrings.App.AppName, message: LinkStrings.DriverOnTrip.StartNow, preferredStyle: .alert)
        let action = UIAlertAction.init(title:  LinkStrings.MostCommon.Ok, style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
        min = 2
        sec = 59
        timeLeftToAccept = 180
        lbl_Heading.text = LinkStrings.DriverOnTrip.RiderComing
        self.cancelTask(cancelRide: false)
        task = DispatchWorkItem {
            self.riderNotReached()
        }
        Utility.stopTimer(after: TimeInterval(timeLeftToAccept), task)
        
    }
    
    func riderNotReached(){
        AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.DriverOnTrip.RiderNotReached, style: .alert, actionButtonTitle: LinkStrings.Yes, completionHandler: { (action:UIAlertAction!) -> Void in
            guard self.checkNetworkStatus() else {
                return
            }
            self.showLoader()
            let parameters = ["tripId": DriverManager.sharedInstance.tripId ?? "" ] as [String : Any]

            if DriverManager.sharedInstance.isAggregateTrip == true{
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/absent_ride_rider_not_coming", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            
            if DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0 > 1{
            self.dismiss(animated: true, completion: {
            DriverManager.sharedInstance.rideRequestCanceled = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RemoveOneRide), object: nil)
            })
            }else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
            }
            }) { (error ) in
                self.hideLoader()
                self.riderNotReached()
            }
        }else{
           
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/cancel_ride_rider_not_coming", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                self.hideLoader()
                
                if  DriverManager.sharedInstance.reservationTripGoingOn == true{  // reservation trip
                    DriverManager.sharedInstance.reservationTripGoingOn = false
                    
                    /*Dismiss all present view controllers first then change  rootviewcontroller*/
                    Utility.driverSideMenuSetUp()
//                    AppDelegate.sharedInstance().window?.rootViewController?.dismiss(animated: false, completion: {
//                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
//                    })
                    
                }else if DriverManager.sharedInstance.shareTripGoingOn == true{ // Shared Instant trip
                    if DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0 > 1{
                        self.dismiss(animated: true, completion: {
                            DriverManager.sharedInstance.rideRequestCanceled = true
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RemoveOneRide), object: nil)
                        })
                    }else{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
                    }
                }else{         //  Special Instant trip
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismiss), object: nil)
                }
            }) { (error ) in
                self.hideLoader()
                self.riderNotReached()
            }
        }
        }, controller: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToTravel(_ sender: UIButton) {
        
        if notAllowToStartRide == true{
            self.lbl_ShowWaitAlert.isHidden = false
            self.lbl_ShowWaitAlert.text = LinkStrings.DriverOnTrip.RiderWait

            self.lbl_ShowWaitAlert.fadeIn(duration: 2.0, completion: { (complete) in
                self.lbl_ShowWaitAlert.fadeOut(duration: 1.0, completion: { (complete) in
                })
            })

        }else{
            guard self.checkNetworkStatus() else {
                return
            }
            showLoader()
            var parameters =   ["tripId": DriverManager.sharedInstance.tripId ?? "" , "riderId":DriverManager.sharedInstance.ride_Details?.riderData?.id ?? ""]
            if DriverManager.sharedInstance.isAggregateTrip == true {
                parameters["riderId"] = DriverManager.sharedInstance.ride_DetailsAggreagate?.riderId ?? ""
            }
            WitzConnectionManager().makeAPICall(functionName: "driver/start_trip", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                self.hideLoader()
                if response?["statusCode"] == 200{
                    
                    if DriverManager.sharedInstance.ride_Details?.tripData?.isShared == true{
                        DriverManager.sharedInstance.ignoreRequestNow = false // New request allowed
                    }
                    if DriverManager.sharedInstance.isAggregateTrip == true {
                        DriverManager.sharedInstance.ignoreRequestNow = true // New request not  allowed
                    }
                    self.dismiss(animated: true, completion: nil)
                }else{
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    
                }, controller: self)
                }
                
            }, failure: { (error) in
                
                self.hideLoader()
            })
        }
    }
}

extension WaitForRiderViewController: UtilityTimerDelegate{
    
    func countDownTimer(value: Int) {
        sec = sec-1
        if sec == 0 {
            min = min-1
            sec = 59
        }
        if min < 0{
            return
        }
        lbl_Time.text = "\(min):\( sec > 9 ? "\(sec)" : "0\(sec)")"
    }
}
