//
//  TripEndedViewController.swift
//  Witz
//
//  Created by abhishek kumar on 06/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class TripEndedViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(feedbackTime))
        self.view.addGestureRecognizer(tap)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Updateslider"), object: nil)  // remove the observer for location Slider Updation 
    }
    
    @IBAction func rateRiderTapped(_ sender: UIButton) {
        feedbackTime()
    }
    
    fileprivate lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.DriverAggregate, bundle: nil)
    
    func feedbackTime(){
        if DriverManager.sharedInstance.ride_Details?.tripData?.isShared == true{
            
            self.performSegue(withIdentifier: "segue_CompleteTrip", sender: nil)
            
        }else if DriverManager.sharedInstance.isAggregateTrip == true{
            
            let controller = self.storyBoard.instantiateViewController(withIdentifier: "ViewAllPassengerViewController") as! ViewAllPassengerViewController
            controller.tripAboutEnd = true
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        else{
            
            self.performSegue(withIdentifier: "segue_FeedbackTime", sender: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_FeedbackTime"{
            let controller = segue.destination as! FeedbackViewController
            let nav = UINavigationController.init(rootViewController: controller)
              nav.navigationBar.barTintColor = UIColor.init(red: 69.0/255.0, green: 78.0/255.0, blue: 79.0/255.0, alpha: 1.0)
            self.present(nav, animated: true, completion: nil)
        }
    }
}
