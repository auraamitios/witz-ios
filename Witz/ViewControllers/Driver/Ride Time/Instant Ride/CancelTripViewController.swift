//
//  CancelTripViewController.swift
//  Witz
//
//  Created by abhishek kumar on 06/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CancelTripViewController: WitzSuperViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeTrip))
        self.view.addGestureRecognizer(tap)
    }
    
    func closeTrip(){
        self.dismiss(animated: true, completion: {})        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
