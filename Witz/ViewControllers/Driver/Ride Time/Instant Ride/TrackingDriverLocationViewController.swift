//
//  TrackingDriverLocationViewController.swift
//  Witz
//
//  Created by abhishek kumar on 03/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import  CoreLocation

class TrackingDriverLocationViewController: UIViewController {
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var slider_DriverLocation: UISlider!
    
    @IBOutlet weak var constraint_ViewRideOn_Height: NSLayoutConstraint!
    
    @IBOutlet weak var view_Tracking: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        slider_DriverLocation.setThumbImage(#imageLiteral(resourceName: "on_map_starting_location"), for: .normal)
        let tripDetails =    DriverManager.sharedInstance.ride_Details?.tripData
        self.lbl_Status.text = "\(String(format: "%.2f",tripDetails?.distance ?? 0))KM/\(Int(tripDetails?.duration ?? 0))Min"

        
        // add observer to update slider
        NotificationCenter.default.addObserver(self, selector: #selector(updateSlider(_:)), name: NSNotification.Name(rawValue: "Updateslider"), object: CLLocationCoordinate2D.self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateSlider(_ value: CLLocationCoordinate2D){
        
        AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:"yes we got slider value\(value)", style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
           
        }, controller: self)
    }
    
    @IBAction func dropRider(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "TripAboutToEndViewController") as! TripAboutToEndViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
        
    }
}
