//
//  TripAboutToEndViewController.swift
//  Witz
//
//  Created by abhishek kumar on 06/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import CoreLocation

protocol TripAboutToEndDelegate : NSObjectProtocol {
    func reloadViewData()
    
}

class TripAboutToEndViewController: UIViewController {
    
    
    var tripEndingFor : Int?  // only for share
    var delegate : TripAboutToEndDelegate? // only for share
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func close_View(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func EndTrip(_ sender: UIButton) {
        
        let alert  = UIAlertController.init(title: LinkStrings.AlertTitle.Title, message: LinkStrings.DriverOnTrip.Drop, preferredStyle: .alert)
        let ok = UIAlertAction.init(title: LinkStrings.MostCommon.Ok, style: .default) { (action) in
            
            let distance = DriverManager.sharedInstance.driverStartLocation?.distance(from:DriverManager.sharedInstance.driverEndLocation ?? CLLocation.init(latitude: 0.0, longitude: 0.0))
            
            
            let kmTravel = (distance ?? 0)/1000
            
            guard self.checkNetworkStatus() else {
                return
            }
            self.showLoader()
            
            var apiToHit = "end_trip"  // for normal trip ending
            var parameters =   ["tripId": DriverManager.sharedInstance.tripId ?? "" , "riderId":DriverManager.sharedInstance.ride_Details?.riderData?.id ?? "","kmTravel":kmTravel] as [String : Any]
            
            //******************  rider id should be get from drop modal  for Aggregate *********/
            if DriverManager.sharedInstance.isAggregateTrip == true{
                //                print(DriverManager.sharedInstance.ride_DetailsDropAggregate?.riderName ?? "")
                //                 print(DriverManager.sharedInstance.ride_DetailsDropAggregate?.riderId ?? "")
                parameters["riderId"] = DriverManager.sharedInstance.ride_DetailsDropAggregate?.riderId ?? ""
                parameters["tripId"] = DriverManager.sharedInstance.tripIdDrop ?? ""
                apiToHit = "end_trip_Aggregate" // for Aggregate trip ending
            }
            
            //******************  rider id should be get from drop modal only for sharing *********/
            if DriverManager.sharedInstance.ride_Details?.tripData?.isShared == true{
                parameters["riderId"] = DriverManager.sharedInstance.ride_DetailsDrop?.riderData?.id ?? ""
                parameters["tripId"] = DriverManager.sharedInstance.tripIdDrop ?? ""
            }
            
            
            
            WitzConnectionManager().makeAPICall(functionName: "driver/\(apiToHit)", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                self.hideLoader()
                if response?["statusCode"] == 200{
                    if DriverManager.sharedInstance.ride_Details?.tripData?.isShared == true || DriverManager.sharedInstance.isAggregateTrip == true{
                        //*******************************Get the array and sort whom to Drop****************************/
                        for (i,element) in DriverManager.sharedInstance.arr_SortedShareTrips!.enumerated(){
                            var tripToServe = element
                            
                            if i == self.tripEndingFor ?? 65746{
                                tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] = KeysForSharingPickUpViewController.PickUPStatus.DROPPED
                                DriverManager.sharedInstance.arr_SortedShareTrips?.remove(at: i)
                                DriverManager.sharedInstance.arr_SortedShareTrips?.insert(tripToServe, at: i)
                            }
                            if KeysForSharingPickUpViewController.DROP ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITDROP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String || KeysForSharingPickUpViewController.PICK ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
                                
                                self.dismiss(animated: true, completion: {
                                    if DriverManager.sharedInstance.isAggregateTrip == false{ DriverManager.sharedInstance.ignoreRequestNow = false // New request allowed
                                        
                                    }
                                    self.delegate?.reloadViewData()
                                })
                                return
                            }
                            
                        }
                    }
                    if DriverManager.sharedInstance.isAggregateTrip == true{
                        self.dismiss(animated: true, completion: {
                            self.delegate?.reloadViewData()
                        })
                        return
                    }
                    
                    self.performSegue(withIdentifier: "segue_tripEnded", sender: nil)
                }
                
            }, failure: { (error) in
                
                self.hideLoader()
            })
        }
        let cancel = UIAlertAction.init(title: LinkStrings.MostCommon.cancel, style: .cancel) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! TripEndedViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
    }
    
}
