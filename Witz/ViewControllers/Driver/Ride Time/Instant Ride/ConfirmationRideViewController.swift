//
//  ConfirmationRideViewController.swift
//  Witz
//
//  Created by abhishek kumar on 01/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class ConfirmationRideViewController: UIViewController {
    @IBOutlet weak fileprivate var lbl_DistanceTime: UILabel!
    @IBOutlet weak fileprivate var lbl_DriverEarning: UILabel!
    @IBOutlet weak fileprivate var lbl_Time: UILabel!
    @IBOutlet weak fileprivate var mapView: GMSMapView!
    
    var tripID : String?
    fileprivate var task : DispatchWorkItem?
    fileprivate var marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    var currentLocation = CLLocationCoordinate2D(latitude: 0, longitude:0)
    var driverTime = 0
    var driverKM = 0
    var timeLeft : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  if driver do nothing
        task = DispatchWorkItem {
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        }
        // Driver has 1 min to accept the ride
        Utility.stopTimer(after:  TimeInterval(timeLeft ?? 59), task)
        Utility.delegate = self
        
        // Do any additional setup after loading the view.
        
        
        showLoader()
        LocationUpdate.sharedInstance.delegate = self
        LocationUpdate.sharedInstance.startTracking()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        LocationUpdate.sharedInstance.stopTracking()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     //MARK:- button action
    @IBAction func acceptRide(_ sender: UIButton) {
        
       
        guard self.checkNetworkStatus() else {
            return
        }
        if let value = DriverManager.sharedInstance.ride_Details?.tripData?.riderId{
             showLoader()
            let parameters =  ["tripId":tripID!,"riderId": value,"driverReachedTime":"\(driverTime)","driverReachedKM":"\(driverKM)"] as [String : Any]
            
            WitzConnectionManager().makeAPICall(functionName: "driver/accept_ride_request", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                
                self.hideLoader()
//                print(response)
                if response?["statusCode"] == 200{
                    if let  result = response?["data"]{
                        DriverManager.sharedInstance.ride_Details = TripModalDriver(json: result)
                    }
                    self.performSegue(withIdentifier: "segue_ContactRider", sender: nil)
                }else
                {
                    AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                         self.cancelTask(cancelRide: true)
                    }, controller: self)
                }
                
                
            }, failure: { (error) in
                self.hideLoader()
                
            })
            
        }
        cancelTask(cancelRide: false)
    }
    
    @IBAction func rejectRide(_ sender: UIButton) {
        showLoader()
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["tripId":tripID!]
        
        WitzConnectionManager().makeAPICall(functionName: "driver/refuse_job_by_driver", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
            
        }, failure: { (error) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        })
    }
    
    fileprivate func cancelTask(cancelRide:Bool){
        Utility.stopTimer()
        self.task?.cancel()
          DriverManager.sharedInstance.ignoreRequestNow = false // New request  allowed
        if cancelRide == true{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismiss), object: nil)
        }
    }
    
    //MARK:- get Ride Detail from server
    public func getRideDetail(){
        if tripID != nil{
            
            guard self.checkNetworkStatus() else {
                return
            }
            let parameters =   ["tripId":tripID!]
            
            WitzConnectionManager().makeAPICall(functionName: "driver/get_trip_detail", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                
                if let  result = response?["data"]{
                    DriverManager.sharedInstance.ride_Details = TripModalDriver(json: result)
                    let tripDetails =    DriverManager.sharedInstance.ride_Details?.tripData
                   
                    self.lbl_DistanceTime.text = "\(String(format: "%.2f",tripDetails?.distance ?? 0))KM/\(Int(tripDetails?.duration ?? 0))Min"
                    self.lbl_DriverEarning.text =  "\(String(format: "%.2f",tripDetails?.driverEarning ?? 0)) ₺"
                    let lat =  tripDetails?.startLocation?.coordinates?[1] ?? 0
                    let lng = tripDetails?.startLocation?.coordinates?[0] ?? 0
                    let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude:CLLocationDegrees(lng))
                    
                    self.mapView.camera = GMSCameraPosition(target:coordinates , zoom: 15, bearing: 0, viewingAngle: 0)
                    let marker1 = GMSMarker(position: coordinates)
                    marker1.position = coordinates
                    marker1.title = "Pick UP"
                    marker1.appearAnimation = GMSMarkerAnimation.pop
                    marker1.icon = #imageLiteral(resourceName: "on_map_starting_location")
                    marker1.map = self.mapView
                    // get the End location
                    let lat1 =  tripDetails?.endLocation?.coordinates?[1] ?? 0
                    let lng1 = tripDetails?.endLocation?.coordinates?[0] ?? 0
                    let coordinates2 = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat1), longitude:CLLocationDegrees(lng1))
                    //Marker for Drop
                    let marker2 = GMSMarker(position: coordinates2)
                    marker2.title = "Drop Location"
                    marker2.appearAnimation = GMSMarkerAnimation.pop
                    marker2.icon = #imageLiteral(resourceName: "on_map_arrival_location")
                    marker2.map = self.mapView
                    
                  
//                    self.showPolyLine(source: self.currentLocation, destination: self.source!)
                    
                    self.showPolyLine(source:coordinates, destination: coordinates2)
                }
                
            }, failure: { (error) in
                
            })
        }
    }
    
     //MARK:- custom method
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
         hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            //            print("wayponits****************\(distance)")
            self.showPath(polyStr: returnPath)
            let detail = distance[0] as! [String:Any]
            let dist = detail["distance"] as? [String:Any]
            let time = detail["duration"] as? [String:Any]
            
            if let km = dist!["text"] as? String {
                print(km.westernArabicNumeralsOnly)
                self.driverKM  = Int(km.westernArabicNumeralsOnly)!
            }
            if let time = time!["text"] as? String {
                print(time.westernArabicNumeralsOnly)
                self.driverTime  =  Int(time.westernArabicNumeralsOnly)!
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = App_Base_Color
        polyline.map = self.mapView
//        hideLoader()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_ContactRider"{
            if let controller = segue.destination as? ContactRiderViewController{
                controller.tripID = self.tripID
            }
            
        }
    }
}
//MARK::TimerDelegates
extension ConfirmationRideViewController: UtilityTimerDelegate{
    
    func countDownTimer(value: Int) {
        if value == 0 {
            self.hideLoader()
            self.cancelTask(cancelRide: true)
            
        }
        lbl_Time.text = "\(value > 9 ? "\(value)" : "0\(value)")"
    }
}

//MARK::LocationDelegates
extension ConfirmationRideViewController:LocationDelegates {
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {

            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            self.marker.position = currentLocation
            marker.title = "Witz"
            marker.map = self.mapView
            marker.appearAnimation = GMSMarkerAnimation.pop
           marker.icon = DriverManager.sharedInstance.VehicleOwnedbyDriver()
            LocationUpdate.sharedInstance.stopTracking()
            self.hideLoader()
            DispatchQueue.global(qos: .background).async { self.getRideDetail()}
        }
    }
    
    func didFailToUpdate(error: Error) {
        
    }
}
