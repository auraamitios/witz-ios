//
//  GettingStratedViewController.swift
//  Witz
//
//  Created by abhishek kumar on 03/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

protocol GettingStartedViewControllerDelegate : NSObjectProtocol {
    func presentNewController()
}

class GettingStratedViewController : UIViewController {
    
    weak var delegate : GettingStartedViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if DriverManager.sharedInstance.shareTripGoingOn == true{ // Shared Instant trip
            NotificationCenter.default.addObserver(self, selector: #selector(removeRide), name: NSNotification.Name(rawValue: RemoveOneRide), object: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc fileprivate func removeRide(object:NSNotification){
        if let result:[String:Any] = object.userInfo as? [String:Any] {
            let message : [String:Any] = result["message"] as! [String:Any]
            print("data hereqqqqq-----***** \(message)")
            
            let riderData = message["riderData"] as? [String:Any] ?? ["":""]
            let tripData = message["tripData"] as? [String:Any] ?? ["":""]
            if let riderIdCheck =  riderData["_id"] as? String{
                if let tripIdCheck = tripData["_id"] as? String{
                    if DriverManager.sharedInstance.tripId == tripIdCheck && DriverManager.sharedInstance.ride_Details?.riderData?.id == riderIdCheck{
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @IBAction func passengerCagir(_ sender: UIButton) {
        self.showLoader()
        guard self.checkNetworkStatus() else {
            return
        }
        var parameters =   ["tripId": DriverManager.sharedInstance.tripId ?? "" , "riderId":DriverManager.sharedInstance.ride_Details?.riderData?.id ?? ""]
        if DriverManager.sharedInstance.isAggregateTrip == true {
            parameters["riderId"] = DriverManager.sharedInstance.ride_DetailsAggreagate?.riderId ?? ""
        }
        
        WitzConnectionManager().makeAPICall(functionName: "driver/send_reached_push_to_rider", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            self.hideLoader()
            self.dismiss(animated: true, completion:{
                self.delegate?.presentNewController()
            })
            
        }, failure: { (error) in
            
            self.hideLoader()
        })
    }
    
    @IBAction func communicateWithPassenger(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactRiderViewController") as! ContactRiderViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.onlyContact = true
        let nav = UINavigationController.init(rootViewController: controller)
        nav.navigationBar.barTintColor = UIColor.init(red: 69.0/255.0, green: 78.0/255.0, blue: 79.0/255.0, alpha: 1.0)
        self.present(nav, animated: true, completion: nil)
    }
    
}
