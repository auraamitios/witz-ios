//
//  RideRequestPopUpViewController.swift
//  Witz
//
//  Created by abhishek kumar on 26/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import AVFoundation

class RideRequestPopUpViewController:  UIViewController {
    
    var isTripPending : Bool?
    var tripId : String?
    var timeLeftToAccept = 59
    fileprivate var task : DispatchWorkItem?
    
    @IBOutlet weak var lbl_Time: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
        //  if driver do nothing
        task = DispatchWorkItem {
            self.cancelTask(cancelRide: true)
        }
        
        // Driver has 1 min to accept the ride
        Utility.stopTimer(after: TimeInterval(timeLeftToAccept), task)
        Utility.delegate = self
        
       
        
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(cancelRide), name: NSNotification.Name(rawValue: RideDismiss), object: nil)
        playSound()
        DriverManager.sharedInstance.ignoreRequestNow = true // New request not allowed
        
        //Check if there is pending trip to complete
        if isTripPending == true{
              DriverManager.sharedInstance.isDriverOnTrip = true  // driver not get any other request now
            player?.stop()
            cancelTask(cancelRide: false)
            print( DriverManager.sharedInstance.trip_Pending?.tripData?.status ?? 0)
             DriverManager.sharedInstance.tripId = self.tripId
            switch(DriverManager.sharedInstance.trip_Pending?.tripData?.status ?? 0){
            case TripOnState.ON_THE_WAY.rawValue:
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactRiderViewController") as! ContactRiderViewController
                controller.tripID = self.tripId
               
                self.navigationController?.pushViewController(controller, animated: false)
                break
                
            case TripOnState.ONGOING.rawValue:
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "DriverOnTripViewController") as! DriverOnTripViewController
                controller.tripID = self.tripId
                controller.isTripPending = true
                self.navigationController?.pushViewController(controller, animated: false)
                break
                
            default :
                
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.stop()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///MARK:- Button Actions
    
    @IBAction func viewRequest(_ sender: UIButton) {
        DriverManager.sharedInstance.isDriverOnTrip = true  // driver not get any other request now
        cancelTask(cancelRide: false)
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        showLoader()
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["tripId":tripId ?? ""]
        
        WitzConnectionManager().makeAPICall(functionName: "driver/refuse_job_by_driver", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
            
        }, failure: { (error) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        })
        
        //        cancelTask(cancelRide: true)
    }
    
    //MARK:- CUSTOM METHOD
    
    var player: AVAudioPlayer?
    
    func playSound() {
        
        guard let url = Bundle.main.url(forResource: "soundMsg", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
            player.numberOfLoops = 5
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    // Ride is cancelled
    func cancelRide(data:NSNotification?){
        
        
        if let controller = AppDelegate.sharedInstance().window?.rootViewController?.presentedViewController{
            controller.dismiss(animated: true, completion: nil)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RideDismiss), object: nil)
        self.dismiss(animated: true, completion: nil)
        DriverManager.sharedInstance.isDriverOnTrip = false   // driver idle now
        DriverManager.sharedInstance.ignoreRequestNow = false
        if data != nil {
            if let data1:Any = data!.userInfo{
                let result:[String:Any] = data1 as! [String:Any]
                let aps = result["aps"] as? [String:Any]
                let alert = UIAlertController.init(title: LinkStrings.App.AppName, message: (aps!["alert"] as? String ??  LinkStrings.DriverOnTrip.RideCanceled) ,preferredStyle: .alert)
                let action = UIAlertAction.init(title:  LinkStrings.MostCommon.Ok, style: .cancel, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    fileprivate func cancelTask(cancelRide:Bool){
        Utility.stopTimer()
        self.task?.cancel()
        if cancelRide == true{
            DriverManager.sharedInstance.isDriverOnTrip = false // driver idle now
             DriverManager.sharedInstance.ignoreRequestNow = false  // New request  allowed
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_ViewRideRequest" {
            if let controller =  segue.destination as? ConfirmationRideViewController{
                controller.tripID = self.tripId
                controller.timeLeft = timeLeftToAccept
                DriverManager.sharedInstance.tripId = self.tripId
            }
        }
    }
    
        
    
}

extension RideRequestPopUpViewController: UtilityTimerDelegate{
    
    func countDownTimer(value: Int) {
        timeLeftToAccept = value
        if value == 0 {
            self.cancelTask(cancelRide: true)
        }
        
        lbl_Time.text = "\(value > 9 ? "\(value)" : "0\(value)")"
    }
}
