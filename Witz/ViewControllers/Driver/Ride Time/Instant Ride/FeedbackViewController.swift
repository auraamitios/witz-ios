//
//  FeedbackViewController.swift
//  Witz
//
//  Created by abhishek kumar on 06/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos

class FeedbackViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var imgView_Profile: UIImageView!
    @IBOutlet weak var view_Rating: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        showContactDeails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func doneFeedback(_ sender: UIButton) {
        
        DriverManager.sharedInstance.ignoreRequestNow = false  // New request  allowed
        
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let parameters = ["tripId": DriverManager.sharedInstance.tripId ?? "" , "riderId":DriverManager.sharedInstance.ride_Details?.riderData?.id ?? "","riderRating":view_Rating.rating] as [String : Any]
        
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/rate_rider", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            if  DriverManager.sharedInstance.reservationTripGoingOn == true{
                DriverManager.sharedInstance.reservationTripGoingOn = false
                
                /*Dismiss all present view controllers first then change  rootviewcontroller*/
                AppDelegate.sharedInstance().window?.rootViewController?.dismiss(animated: false, completion: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
                })
                
            }else{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismiss), object: nil)
            }
        }) { (error ) in
            self.hideLoader()
        }
    }
    
    fileprivate  func showContactDeails(){
        let rider =  DriverManager.sharedInstance.ride_Details?.riderData
        lbl_Name.text = rider?.fullName?.nameFormatted
        
        let imgUrl = "\(kBaseImageURL)\(rider?.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
    }
}
