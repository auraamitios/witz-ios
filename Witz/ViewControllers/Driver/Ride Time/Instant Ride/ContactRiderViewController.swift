//
//  ContactRiderViewController.swift
//  Witz
//
//  Created by abhishek kumar on 03/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher

class ContactRiderViewController: WitzSuperViewController {

    @IBOutlet weak fileprivate var lbl_Name: UILabel!
    @IBOutlet weak fileprivate var imgView_Profile: UIImageView!
    @IBOutlet weak fileprivate var lbl_Rating: UILabel!
    @IBOutlet weak fileprivate var constraint_Height_view_Access: NSLayoutConstraint!
    @IBOutlet weak fileprivate var view_Access: UIView!
    
    var tripID : String?
   var onlyContact : Bool?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if onlyContact == true{
            self.view_Access.isHidden = true
            self.constraint_Height_view_Access.constant = 0
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeContact))
            self.view.addGestureRecognizer(tap)
        }
        showContactDeails()
        addLeftNavigationBarButton()

 }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    //MARK:: Private Method
     func addLeftNavigationBarButton() {
        
        self.left_barButton = UIButton(type: .custom)
        self.left_barButton.setImage(UIImage(named: "Round_back"), for: .normal)

        self.left_barButton.addTarget(self, action: #selector(ContactRiderViewController.cancelRideNow), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: self.left_barButton)
        self.navigationItem.leftBarButtonItem = item1
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
  fileprivate  func showContactDeails(){
    let rider =  DriverManager.sharedInstance.ride_Details?.riderData
        lbl_Name.text = rider?.fullName?.nameFormatted ?? ""
        lbl_Rating.text = "\(String(format: "%.2f",rider?.avgRating ?? 0))"
        let imgUrl = "\(kBaseImageURL)\(rider?.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
    }
    
    @IBAction func cancelRideAction(_ sender: Any) {
    cancelRideNow()
    }
    
    func cancelRideNow(){
       
        AlertHelper.displayAlert(title: LinkStrings.DriverOnTrip.WannaCancel, message: LinkStrings.DriverOnTrip.RideCancelMsg, style: .alert, actionButtonTitle: [LinkStrings.Yes,LinkStrings.Cancel], completionHandler: { (action) in
            
            print(action.title ?? "")
            
            if action.title == LinkStrings.Yes{
            guard self.checkNetworkStatus() else {
                return
            }
            
                self.showLoader()
                let parameters = ["tripId": DriverManager.sharedInstance.tripId ?? ""] as [String : Any]
                
                WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/cancel_ride", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                    self.hideLoader()
                    if  DriverManager.sharedInstance.reservationTripGoingOn == true{
                        DriverManager.sharedInstance.reservationTripGoingOn = false
                        
                        /*Dismiss all present view controllers first then change  rootviewcontroller*/
                        AppDelegate.sharedInstance().window?.rootViewController?.dismiss(animated: false, completion: {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
                        })
                        
                    }else{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismiss), object: nil)
                    }
                }) { (error ) in
                    self.hideLoader()
                }
            }
          
        }, controller: self)
    }
    
    func closeContact(){
        self.dismiss(animated: true, completion: nil)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RIDEDISMISS"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Action
    @IBAction func placeCall(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CallingTwillioViewController") as! CallingTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)

    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
    
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MessageTwillioViewController") as! MessageTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func accessToBeginningPoint(_ sender: UIButton) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_RideStarted"{
            if let controller = segue.destination as? DriverOnTripViewController{
                controller.tripID = self.tripID
            }
        }
    }
}
