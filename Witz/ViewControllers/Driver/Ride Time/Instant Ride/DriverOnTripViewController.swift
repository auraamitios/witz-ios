//
//  DriverOnTripViewController.swift
//  Witz
//
//  Created by abhishek kumar on 03/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import GoogleMaps
import Alamofire
import SwiftyJSON
import MapKit
import CoreLocation

class DriverOnTripViewController: UIViewController {
    @IBOutlet weak var btn_Navigate: UIButton!
    @IBOutlet weak fileprivate var view_Map: GMSMapView!
    @IBOutlet weak var constraint_Bottom_viewAMap: NSLayoutConstraint!
    
    @IBOutlet var viewPinForRider: UIView!
    @IBOutlet weak var lbl_pinTime: UILabel!
    
    var showDestination = false  // this will check if driver pick the rider
    var tripID : String?
    fileprivate var marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    var currentLocation = CLLocationCoordinate2D(latitude: 0, longitude:0)
    var markerAdded : Bool?
    var isTripPending : Bool?
    /******view Rider Detail outlets******/
    @IBOutlet weak fileprivate var view_RiderDetail: UIView!
    @IBOutlet weak fileprivate var imgVIew_UserProfile: UIImageView!
    @IBOutlet weak fileprivate var lbl_UserRating: UILabel!
    @IBOutlet weak fileprivate var lbl_UserName: UILabel!
    
    
    
    /******view Contact Rider outlets******/
    @IBOutlet weak fileprivate var view_ContactRider: UIView!
    @IBOutlet weak var view_Tracking: UIView!
    var source : CLLocationCoordinate2D?
    var destination : CLLocationCoordinate2D?
    var zoomForMap : Float = 15.0
    fileprivate var timerLocationGet : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        marker.title = "Witz"
        marker.map = self.view_Map
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon = DriverManager.sharedInstance.VehicleOwnedbyDriver()
        
        // add gesture for swipe
        let swipeUp = UISwipeGestureRecognizer.init(target: self, action: #selector(viewMore))
        swipeUp.direction = .up
        view_RiderDetail.addGestureRecognizer(swipeUp)
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(viewLess))
        swipeDown.direction = .down
        view_ContactRider.addGestureRecognizer(swipeDown)
        
        //        self.perform(#selector(confirmRiderArrived), with: nil, afterDelay: 1.5)
        showContactDeails()
        showLoader()
        LocationUpdate.sharedInstance.delegate = self
        LocationUpdate.sharedInstance.startTracking()
        self.view_Map.bringSubview(toFront: btn_Navigate)
        
        // timer to update driver Location
        timerLocationGet = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(DriverOnTripViewController.UpdateDriverLocation), userInfo: nil, repeats: true)
        
        
        if isTripPending == true{
            showDropView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        LocationUpdate.sharedInstance.fetchLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //        LocationUpdate.sharedInstance.stopTracking()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        timerLocationGet?.invalidate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func UpdateDriverLocation(){
        let parameters = ["lat":(self.currentLocation.latitude ),"lng":(self.currentLocation.longitude ),"arrivalTime":"0","arrivalDistance":"0"] as [String : Any]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/update_location", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
            
        }) { (error) in
            
        }
    }
    
    /******Work for view Rider Detail ******/
    
    fileprivate  func showContactDeails(){
        let rider =  DriverManager.sharedInstance.ride_Details?.riderData
        lbl_UserName.text = rider?.fullName?.nameFormatted
        lbl_UserRating.text = "\( String(format: "%.1f",rider?.avgRating ?? 0))"
        let imgUrl = "\(kBaseImageURL)\(rider?.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgVIew_UserProfile.image = thumbImage
                    }
                    self.imgVIew_UserProfile.setNeedsDisplay()
                }
            })
        }
    }
    
    fileprivate func showDropView(){
        view_ContactRider.isHidden = true
        view_RiderDetail.isHidden = true
        constraint_Bottom_viewAMap.constant = 80
        LocationUpdate.sharedInstance.fetchLocation()
        self.addsubView()
    }
    
    // MARK: - Button Action
    @IBAction func slideUPtoViewMore(_ sender: UIButton) {
        viewMore()
    }
    
    @IBAction func pickupRider(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "GettingStratedViewController") as! GettingStratedViewController
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
        showDropView()
        showDestination = true  // rider picking process start show path to drop
        self.view_Map.clear()
    }
    
    /**
     Add tracking view controller as subview for tracking of driver
     */
    
    func addsubView(){
        DriverManager.sharedInstance.driverStartLocation = CLLocation.init(latitude: self.currentLocation.latitude, longitude: self.currentLocation.longitude)
        self.view_Tracking.setNeedsLayout()
        self.view_Tracking.isHidden = false
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "TrackingDriverLocationViewController") as! TrackingDriverLocationViewController
        controller.view.frame = self.view_Tracking.frame
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        self.didMove(toParentViewController: controller)
    }
    
    
    @objc fileprivate func viewMore(){
        view_RiderDetail.isHidden = true
        view_ContactRider.isHidden = false
    }
    
    @objc fileprivate  func viewLess(){
        view_RiderDetail.isHidden = false
        view_ContactRider.isHidden = true
    }
    
    /******Work for view Contact Rider ******/
    // MARK: - Button Action
    @IBAction func slideDownToHide(_ sender: UIButton) {
        viewLess()
    }
    
    @IBAction func communicateWithRider(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactRiderViewController") as! ContactRiderViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.onlyContact = true
        let nav = UINavigationController.init(rootViewController: controller)
        nav.navigationBar.barTintColor = UIColor.init(red: 69.0/255.0, green: 78.0/255.0, blue: 79.0/255.0, alpha: 1.0)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func navigateMap(_ sender: UIButton) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            
            if self.source != nil && self.destination != nil{
                if let testURL = URL(string:
                    "comgooglemaps://?saddr=\(self.source?.latitude ?? 0.0),\(self.source?.longitude ?? 0.0)&daddr=\(self.destination?.latitude ?? 0.0),\(self.destination?.longitude ?? 0.0)&directionsmode=driving"){
                    
                    UIApplication.shared.open(testURL, options: ["":""], completionHandler: { (completer) in  })
                    
                }
            }
            
        }else{
            NSLog("Can't use com.google.maps://");
            if let testURL = URL(string:
                "http://maps.apple.com/?saddr=\(self.source?.latitude ?? 0.0),\(self.source?.longitude ?? 0.0)&daddr=\(self.destination?.latitude ?? 0.0),\(self.destination?.longitude ?? 0.0)"){
                UIApplication.shared.open(testURL, options: ["":""], completionHandler: { (completer) in  })
            }
        }
    }
    
    //MARK:- get Ride Detail from server
    public func getRideDetail(){
        if tripID != nil{
            
            guard self.checkNetworkStatus() else {
                return
            }
            print(DriverManager.sharedInstance.ride_Details?.tripData?.id ?? "j")
            let parameters =   ["tripId":tripID!]
            
            WitzConnectionManager().makeAPICall(functionName: "driver/get_trip_detail", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                
                self.hideLoader()
                if let  result = response?["data"]{
                    DriverManager.sharedInstance.ride_Details = TripModalDriver(json: result)
                    let tripDetails =    DriverManager.sharedInstance.ride_Details?.tripData
                    
                    // get the start location
                    let lat =  tripDetails?.startLocation?.coordinates?[1] ?? 0
                    let lng = tripDetails?.startLocation?.coordinates?[0] ?? 0
                    let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude:CLLocationDegrees(lng))
                    self.source = coordinates
                    //Marker for pick up
                    self.view_Map.camera = GMSCameraPosition(target:coordinates , zoom: 15, bearing: 0, viewingAngle: 0)
                    
                    
                    // get the End location
                    let lat1 =  tripDetails?.endLocation?.coordinates?[1] ?? 0
                    let lng1 = tripDetails?.endLocation?.coordinates?[0] ?? 0
                    let coordinates2 = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat1), longitude:CLLocationDegrees(lng1))
                    self.destination = coordinates2
                    
                    self.marker.position = self.currentLocation  // Driver itself
                    self.showPolyLine(source: self.currentLocation, destination: self.source!)
                    //                    self.showPolyLine(source: self.source!, destination: self.destination!)
                }
                
            }, failure: { (error) in
                
                self.hideLoader()
            })
        }
    }
    
    //MARK:- custom method
    /**
     This method will return the path and distance between two coordinates
     */
    
    //    var locationUpdatedNow = false
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            self.showPath(polyStr: returnPath)
            
            DispatchQueue.main.async {
                // heading towards rider location for pick up
                if self.showDestination == false{
                    let arr = Utility.getTimeAndDistance(data: distance)
                    let marker1 = GMSMarker(position: destination)
                    marker1.title = "Pick UP Location"
                    marker1.appearAnimation = GMSMarkerAnimation.pop
                    marker1.icon = self.getPinWith(time: "\(arr[1]) Mins")
                    marker1.map = self.view_Map
                }
            }
        }
    }
    
    /**
     Draw path on the map
     */
    
    fileprivate func showPath(polyStr :String){
        DispatchQueue.main.async {
            let path = GMSPath(fromEncodedPath: polyStr)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.view_Map
        }
        hideLoader()
    }
    
    
    /**
     Send the waypoints to rider for drawing same path
     */
    func updateWayPointToRider(_ data:[[String: Any]])    {
        
        let parameters =   ["tripId":tripID!,"wayEndPoints":data] as [String:Any]
        print(parameters)
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/update_trip_way_points_IOS", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
            //            print(response)
        }, failure: { (error) in
            //            print(error.debugDescription)
        })
    }
    
    /**
     get the icon using storyboard view
     */
    fileprivate func getPinWith(time:String ) -> UIImage{
        lbl_pinTime.text = time
        //grab it
        UIGraphicsBeginImageContextWithOptions(viewPinForRider.bounds.size, false, UIScreen.main.scale)
        viewPinForRider.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imagePick = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imagePick ?? #imageLiteral(resourceName: "on_map_arrival_location")
    }
    
    
}
//MARK::LocationDelegates
extension DriverOnTripViewController:LocationDelegates {
    
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.view_Map.isMyLocationEnabled = true
            self.view_Map.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            zoomForMap = self.view_Map.camera.zoom  // zoom as per user decided to zoom the map
            self.view_Map.camera = GMSCameraPosition(target: location.coordinate, zoom: zoomForMap , bearing: 0, viewingAngle: 0)
            currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            DriverManager.sharedInstance.driverEndLocation =  CLLocation.init(latitude: self.currentLocation.latitude, longitude: self.currentLocation.longitude) // keep track of the driver end location as well as update current location
            
            
            if source == nil || destination == nil{
                markerAdded = false
                DispatchQueue.global(qos: .background).async { self.getRideDetail()}
            }else
            {
                // driver picked the rider now show path to drop
                if showDestination == true{
                    //                    self.view_Map.clear()
                    self.showPolyLine(source: self.currentLocation, destination: destination!)
                    
                    if markerAdded == false{
                        markerAdded = true
                        let marker1 = GMSMarker(position: source!)
                        marker1.title = "Pick UP Location"
                        marker1.appearAnimation = GMSMarkerAnimation.pop
                        marker1.icon = #imageLiteral(resourceName: "on_map_starting_location")
                        marker1.map = self.view_Map
                        //Marker for Drop
                        let marker2 = GMSMarker(position: destination!)
                        marker2.title = "Drop Location"
                        marker2.appearAnimation = GMSMarkerAnimation.pop
                        marker2.icon = #imageLiteral(resourceName: "on_map_arrival_location")
                        marker2.map = self.view_Map
                    }
                }
            }
            marker.position = currentLocation
            marker.title = "Witz"
            marker.map = self.view_Map
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.icon = #imageLiteral(resourceName: "vehicle-car_on-map")
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Updateslider"), object: self.currentLocation)
        }
    }
    
    func didFailToUpdate(error: Error) {
        
    }
}



//MARK::GettingStratedDelegate
extension DriverOnTripViewController: GettingStartedViewControllerDelegate{
    
    func presentNewController() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "WaitForRiderViewController") as! WaitForRiderViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
        
    }
}


