//
//  ConfirmationShareRideViewController.swift
//  Witz
//
//  Created by abhishek kumar on 26/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class ConfirmationShareRideViewController: UIViewController {
    
    @IBOutlet weak fileprivate var lbl_DistanceTime: UILabel!
    @IBOutlet weak fileprivate var lbl_DriverEarning: UILabel!
    @IBOutlet weak fileprivate var lbl_Time: UILabel!
    @IBOutlet weak fileprivate var mapView: GMSMapView!
    @IBOutlet weak var lbl_PickupAddress: UILabel!
    @IBOutlet weak var lbl_DropAddress: UILabel!
    
    var loadDetailOnce = true  // keep check for one time load only
    
    var tripID : String?
    fileprivate var task : DispatchWorkItem?
    fileprivate var marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    var currentLocation = CLLocationCoordinate2D(latitude: 0, longitude:0)
    var driverTime = 0
    var driverKM = 0
    var timeLeft : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //  if driver do nothing
        task = DispatchWorkItem {
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        }
        // Driver has 1 min to accept the ride
        Utility.stopTimer(after: TimeInterval(timeLeft ?? 3), task)
        Utility.delegate = self
        
        // Do any additional setup after loading the view.
        
        
        showLoader()
        LocationUpdate.sharedInstance.delegate = self
        LocationUpdate.sharedInstance.startTracking()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        LocationUpdate.sharedInstance.stopTracking()
    }
    
    fileprivate func cancelTask(cancelRide:Bool){
        Utility.stopTimer() // stop timer
        self.task?.cancel() // cancel the scheduled task
         DriverManager.sharedInstance.ignoreRequestNow = false // New request  allowed
        if cancelRide == true{
            
            if DriverManager.sharedInstance.arr_ShareTrips?.count == 0 || DriverManager.sharedInstance.arr_ShareTrips == nil{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissShare), object: nil) // post notification to end the trip
            }else{
                if let controller = AppDelegate.sharedInstance().window?.rootViewController?.presentedViewController{
                    controller.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    
    //MARK:- Button Action
    
    @IBAction func acceptTapped(_ sender: UIButton) {
        
        guard self.checkNetworkStatus() else {
            return
        }
        if let value = DriverManager.sharedInstance.ride_Details?.tripData?.riderId{
            guard let price = DriverManager.sharedInstance.ride_Details?.tripData?.driverEarning else{
                return
            }
            showLoader()
            var parameters =  ["tripId":tripID!,"riderId": value,"driverReachedTime":driverTime,"driverReachedKM":driverKM,"price":price] as [String : Any]
            
            if ((DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0) > 0) {
                let modal = DriverManager.sharedInstance.arr_ShareTrips?.first
                parameters["sharedTripId"] = modal?.tripData?.id ?? ""
            }else{
                parameters["sharedTripId"] = DriverManager.sharedInstance.ride_Details?.tripData?.id ?? ""
            }
            
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/accept_shared_ride_request", withInputString: parameters, requestType: .post, isAuthorization: true, success: {
                (response) in
                
                self.hideLoader()
                //                print(response)
                if response?["statusCode"] == 200{
                    /*Add the detail of trip in array to keep the track*/
                    if ((DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0) > 0) {
                        DriverManager.sharedInstance.arr_ShareTrips?.append(DriverManager.sharedInstance.ride_Details!)
                    }else{
                        DriverManager.sharedInstance.arr_ShareTrips = [DriverManager.sharedInstance.ride_Details!]
                    }
                    
//                    if let  result = response?["data"]{
//                        DriverManager.sharedInstance.ride_Details = TripModalDriver(json: result)
//                    }
                    DriverManager.sharedInstance.shareTripGoingOn = true
                    DriverManager.sharedInstance.newShareRequestAdded = true
                    if ((DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0) > 1) {
                        if let controller = AppDelegate.sharedInstance().window?.rootViewController?.presentedViewController{
                            controller.dismiss(animated: true, completion: nil)
                        }
                    }else{
                        self.performSegue(withIdentifier: "segue_TripShareStart", sender: nil)
                    }
                }else
                {
                    AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                        self.cancelTask(cancelRide: true)
                    }, controller: self)
                }
            }, failure: { (error) in
                self.hideLoader()
            })
        }
        cancelTask(cancelRide: false)
    }
    
    @IBAction func rejectTapped(_ sender: UIButton) {
        
        showLoader()
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["tripId":tripID!]
        
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/refuse_job_by_driver", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
            
        }, failure: { (error) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        })
        
    }
    
    //MARK:- get Ride Detail from server
    public func getRideDetailShare(){
        if tripID != nil{
            
            guard self.checkNetworkStatus() else {
                return
            }
            let parameters =   ["tripId":tripID!]
            
            WitzConnectionManager().makeAPICall(functionName: "driver/get_trip_detail", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                
                self.hideLoader()
                if let  result = response?["data"]{
                    
                    print(result)
                    DriverManager.sharedInstance.ride_Details = TripModalDriver(json: result)
                    let tripDetails =    DriverManager.sharedInstance.ride_Details?.tripData
                    
                    
                    /*Populate data on view for trip*/
                    
                    self.lbl_DistanceTime.text =  "\(String(format: "%.2f", tripDetails?.distance ?? 0)) KM/\(String(format: "%.2f",tripDetails?.duration ?? 0) ) Min"
                    self.lbl_DriverEarning.text =  "\(String(format: "%.2f",tripDetails?.driverEarning ?? 0)) ₺"
                    self.lbl_PickupAddress.text = tripDetails?.startAddress
                    self.lbl_DropAddress.text = tripDetails?.endAddress
                    let lat =  tripDetails?.startLocation?.coordinates?[1] ?? 0
                    let lng = tripDetails?.startLocation?.coordinates?[0] ?? 0
                    let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude:CLLocationDegrees(lng))
                    let dropCoordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetails?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetails?.endLocation?.coordinates?[0] ?? 0))
                    
                    /*Rider location on map*/
                    self.mapView.camera = GMSCameraPosition(target:coordinates , zoom: 15, bearing: 0, viewingAngle: 0)
                    let marker1 = GMSMarker(position: coordinates)
                    marker1.position = coordinates
                    marker1.title = "Pick UP"
                    marker1.appearAnimation = GMSMarkerAnimation.pop
                    marker1.icon = #imageLiteral(resourceName: "on_map_starting_location")
                    marker1.map = self.mapView
                    
//                    /*Add the detail of trip in array to keep the track*/
//                   if ((DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0) > 0) {
//                    DriverManager.sharedInstance.arr_ShareTrips?.append(TripModalDriver(json: result))
//                   }else{
//                    DriverManager.sharedInstance.arr_ShareTrips = [TripModalDriver(json: result)]
//                    }
                    
                    /*show polyline from driver current location to rider location only*/
                    self.showPolyLine(source: self.currentLocation, destination: coordinates,dropLocation: dropCoordinates)
                }
                
            }, failure: { (error) in
                
                self.hideLoader()
            })
        }
    }
    
    //MARK:- custom method
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D,dropLocation:CLLocationCoordinate2D){
         hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
           
            DispatchQueue.main.async {self.showPath(polyStr: returnPath)} // draw path from current location to pickup
            
            //*Now get the distance and duration from pickup and drop location*//
             hitsLimit_Google = 5
            GooglePathHelper.getPathPolyPoints(from: destination, destination: dropLocation) { (returnPath, distance) in
                let detail = distance[0] as! [String:Any]
                let dist = detail["distance"] as? [String:Any]
                let time = detail["duration"] as? [String:Any]
                
                /* Km and time  to send if driver accept ride*/
                if let km = dist!["text"] as? String {
                    print(km.westernArabicNumeralsOnly)
                    self.driverKM  = Int(km.westernArabicNumeralsOnly)!
                }
                if let time = time!["text"] as? String {
                    print(time.westernArabicNumeralsOnly)
                    self.driverTime  =  Int(time.westernArabicNumeralsOnly)!
                }
            }
     }
}
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
        hideLoader()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK::TimerDelegates
extension ConfirmationShareRideViewController: UtilityTimerDelegate{
    
    func countDownTimer(value: Int) {
        if value == 0 {
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        }
        lbl_Time.text = "\(value)"
    }
}



//MARK::LocationDelegates
extension ConfirmationShareRideViewController:LocationDelegates {
    
    
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
       
        if let location = locations.last {
            
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            self.marker.position = currentLocation
            marker.title = "Witz"
            marker.map = self.mapView
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.icon = DriverManager.sharedInstance.VehicleOwnedbyDriver()
            LocationUpdate.sharedInstance.stopTracking()
            if loadDetailOnce == true{
                loadDetailOnce = false
                 DispatchQueue.global(qos: .background).async { self.getRideDetailShare()}
            }
        }
    }
    
    func didFailToUpdate(error: Error) {
        
    }
}
