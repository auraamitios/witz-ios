//
//  ShareRequestPopUpViewController.swift
//  Witz
//
//  Created by abhishek kumar on 26/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import AVFoundation

class ShareRequestPopUpViewController: UIViewController {
    
    var isTripPending : Bool?
    var tripId : String?
    var timeLeftToAccept = 59
    fileprivate var task : DispatchWorkItem?
    
    @IBOutlet weak var lbl_Time: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //  if driver do nothing
        task = DispatchWorkItem {
            self.cancelTask(cancelRide: true)
        }
        
        // Driver has 1 min to accept the ride
        Utility.stopTimer(after: TimeInterval(timeLeftToAccept), task)
        Utility.delegate = self
        
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(cancelRide), name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
        playSound()
        
        DriverManager.sharedInstance.ignoreRequestNow = true // New request not allowed
        
        //Check if there is pending trip to complete
        if isTripPending == true{
            player?.stop()
            cancelTask(cancelRide: false)
            
            DriverManager.sharedInstance.shareTripGoingOn = true
            DriverManager.sharedInstance.newShareRequestAdded = true
             DriverManager.sharedInstance.ignoreRequestNow = false // New request  allowed
            DriverManager.sharedInstance.tripId = self.tripId
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SharingPickUpViewController") as! SharingPickUpViewController
            controller.isPendingTrip = true
            self.navigationController?.pushViewController(controller, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.stop()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    ///MARK:- Button Actions
    
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        showLoader()
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["tripId":tripId ?? ""]
        WitzConnectionManager().makeAPICall(functionName: "driver/refuse_job_by_driver", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
            
        }, failure: { (error) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        })
    }
    
    //MARK:- CUSTOM METHOD
    
    var player: AVAudioPlayer?
    
    func playSound() {
        
        guard let url = Bundle.main.url(forResource: "soundMsg", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
            player.numberOfLoops = 5
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    // Ride is cancelled
    func cancelRide(){
        if let controller = AppDelegate.sharedInstance().window?.rootViewController?.presentedViewController{
            controller.dismiss(animated: true, completion: nil)
        }
        DriverManager.sharedInstance.arr_SortedShareTrips?.removeAll()
        DriverManager.sharedInstance.arr_ShareTrips?.removeAll()
        DriverManager.sharedInstance.ignoreRequestNow = false  // New request  allowed
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RemoveOneRide), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func cancelTask(cancelRide:Bool){
        Utility.stopTimer()
        self.task?.cancel()
        if cancelRide == true{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_DetailShare" {
            cancelTask(cancelRide: false)
            if let controller =  segue.destination as? ConfirmationShareRideViewController{
                controller.tripID = self.tripId
                controller.timeLeft = timeLeftToAccept
                DriverManager.sharedInstance.tripId = self.tripId
            }
        }
    }
}

extension ShareRequestPopUpViewController: UtilityTimerDelegate{
    
    func countDownTimer(value: Int) {
        
        timeLeftToAccept = value
        if value == 0 {
            self.cancelTask(cancelRide: true)
        }
        lbl_Time.text = "\(value)"
    }
}
