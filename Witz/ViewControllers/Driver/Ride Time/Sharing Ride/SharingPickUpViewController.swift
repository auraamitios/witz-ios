//
//  SharingPickUpViewController.swift
//  Witz
//
//  Created by abhishek kumar on 26/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit


public struct KeysForSharingPickUpViewController{
    
    static let TRIPID = NSLocalizedString("trip_id", comment: "trip_id")
    static let RIDEDETAILS = NSLocalizedString("ride_details", comment: "ride_details")
    static let TYPE = NSLocalizedString("type", comment: "type")
    static let PICK = NSLocalizedString("pick", comment: "pick")
    static let DROP = NSLocalizedString("drop", comment: "drop")
    static let COORDINATE = NSLocalizedString("coordinate", comment: "coordinate")
    static let ENDCOORDINATE = NSLocalizedString("end_coordinate", comment: "end_coordinate")
    static let DIST = NSLocalizedString("dist", comment: "dist")
    static let PICKUPNUMBER = NSLocalizedString("pickup_number", comment: "pickup_number")
    static let ADDRESS = NSLocalizedString("address", comment: "address")
    static let ENDADDRESS = NSLocalizedString("end_address", comment: "end_address")
    static let NAME = NSLocalizedString("name", comment: "name")
    
    struct PickUPStatus {
        
        static let STATUS = NSLocalizedString("status", comment: "status")
        static let WAITPICKUP = NSLocalizedString("waiting for pickup", comment: "wait_picked")
        static let WAITDROP = NSLocalizedString("waiting for drop", comment: "wait_dropped")
        static let PICKED = NSLocalizedString("picked", comment: "picked")
        static let DROPPED = NSLocalizedString("dropped", comment: "dropped")
    }
}

class SharingPickUpViewController: UIViewController {
    var isPendingTrip : Bool?
  
    
    @IBOutlet weak fileprivate var lbl_PassengerCount: UILabel!
    @IBOutlet weak fileprivate var lbl_DurationTime: UILabel!
    @IBOutlet weak fileprivate var lbl_DriverName: UILabel!   // this label use to show rider name instead :P
    @IBOutlet weak fileprivate var lbl_PickUpAddress: UILabel!
    @IBOutlet weak fileprivate var btn_Pickup: UIButton!
    @IBOutlet weak fileprivate var btn_ViewTasks: UIButton!
    @IBOutlet weak fileprivate var btn_Call: UIButton!
    @IBOutlet weak fileprivate var btn_Msg: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var view_Duration_Pickup: UIView!
    @IBOutlet weak var view_Pickup: UIView!
    @IBOutlet weak var view_DropPosition: UIView!
    
    fileprivate var currentLocation = AppManager.sharedInstance.locationCoordinates.last?.coordinate ?? CLLocationCoordinate2D(latitude: APPLEOFFICELOCATION_LAT, longitude: APPLEOFFICELOCATION_LNG)
    fileprivate var marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    
    fileprivate var sourceNav : CLLocationCoordinate2D?
    fileprivate var destinationNav : CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let img = #imageLiteral(resourceName: "contact_phone_gray_big").withRenderingMode(.alwaysTemplate)
        let img1 = #imageLiteral(resourceName: "message_big").withRenderingMode(.alwaysTemplate)
        
        btn_Call.setImage(img, for: .normal)
        btn_Msg.setImage(img1, for: .normal)
        btn_Call.tintColor = .white
        btn_Msg.tintColor = .white
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeRide), name: NSNotification.Name(rawValue: RemoveOneRide), object: nil)
        
        showLoader()
        LocationUpdate.sharedInstance.delegate = self
        LocationUpdate.sharedInstance.startTracking()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkTheTrips()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        LocationUpdate.sharedInstance.stopTracking()
    }
    
    /**
     This will show driver vehicle icon on the map
     */
    fileprivate func showDriverIcon(location: CLLocationCoordinate2D){
        self.marker.icon = DriverManager.sharedInstance.VehicleOwnedbyDriver()
        self.marker.position = location
        self.marker.title = "Witz"
        self.marker.appearAnimation = GMSMarkerAnimation.pop
        self.marker.map = self.mapView
    }
    
    // Ride is cancelled
    @objc fileprivate func removeRide(object:NSNotification){
        let parameters =   ["tripId":DriverManager.sharedInstance.tripId!]
        
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/get_shared_trip_details_full", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            //            print(response)
            self.hideLoader()
            if let  result = response?["data"]["logsData"]["sharingInfoData"].array{
                
                print(result)
                DriverManager.sharedInstance.arr_ShareTrips?.removeAll()
                for  sharedData in result{ // get all the trips
                    let modal = TripModalDriver(json: sharedData)
                    if self.isRideCanceled(trip: modal) == false{  // check if ride is not cancelled
                        DriverManager.sharedInstance.ride_Details = modal
                        DriverManager.sharedInstance.arr_ShareTrips?.append(modal)
                    }
                }
                self.perform(#selector(self.refreshViewAfterRideRemove), with: nil, afterDelay: 4.0) // refresh
                self.checkTheTrips() //recalculate trips
                
            }
        }) { (error ) in
            self.hideLoader()
        }
    }
    
    func refreshViewAfterRideRemove(){
        for (_,element) in DriverManager.sharedInstance.arr_SortedShareTrips!.enumerated(){
            
            var tripToServe = element
            if KeysForSharingPickUpViewController.PICK ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&
                KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
//                print("this one is for pickup \(i)")
                break
            }else if KeysForSharingPickUpViewController.DROP ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITDROP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
                
                //****Still there pending trip for Drop
                self.addsubView()
                break
            }
        }
    }
    
    /**
     This method return if your ride is cancelled
     */
    func isRideCanceled(trip:TripModalDriver) -> Bool{
        let status = trip.tripData?.status ?? 8709
        
        if  SharingTripType.RIDER_CANCEL_TRIP_PNALITY.rawValue == status{
            
            return true
            
        }else if SharingTripType.CANCELLED_BY_RIDER.rawValue == status{
            
            let alert = UIAlertController.init(title: LinkStrings.App.AppName, message: "\(trip.riderData?.fullName?.nameFormatted ?? "" ) has cancelled Trip on \(trip.tripData?.updatedAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: LinkStrings.OK, style: .default, handler: { (action) in
                if (DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0) == 0{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
                }
            })
            
            alert.addAction(ok)
            self.present(alert, animated: true, completion: {
                
            })
            
            return true
        }
        else if SharingTripType.CANCELLED_BY_DRIVER.rawValue == status{
            return true
        }
        else if SharingTripType.RIDER_NOT_REACHED.rawValue == status{
            
            return true
        }
        
        return false
    }
    
    /**
     this will check the arr of shared trip and calculate the trip distance and apply the algorithm afterwards
     */
    
    @objc fileprivate func checkTheTrips(){
        mapView.clear()
        
        var arr_Sorted = [[String : Any]]()   // contain the trips modified dictonary for calculation
        var i = 0  // keep the sequence of customers
        let source =  CLLocation(latitude: CLLocationDegrees(currentLocation.latitude), longitude:CLLocationDegrees(currentLocation.longitude))
        showDriverIcon(location: currentLocation) // show driver icon on map
        let key = KeysForSharingPickUpViewController.DIST // The key you want to sort by
        
        if DriverManager.sharedInstance.newShareRequestAdded == true || DriverManager.sharedInstance.rideRequestCanceled == true{ //Recalculate only if new ride added or removed
            DriverManager.sharedInstance.newShareRequestAdded = false
            DriverManager.sharedInstance.rideRequestCanceled = false
            //*******************************get the data in a formatted way to sort****************************/
            
            if DriverManager.sharedInstance.arr_ShareTrips?.count ?? 0 > 0{
                for modal in DriverManager.sharedInstance.arr_ShareTrips!{
                    
                    let coordinatesStart = CLLocation(latitude: CLLocationDegrees(modal.tripData?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.tripData?.startLocation?.coordinates?[0] ?? 0))
                    let coordinatesEnd = CLLocation(latitude: CLLocationDegrees(modal.tripData?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.tripData?.endLocation?.coordinates?[0] ?? 0))
                    let distPick = source.distance(from: coordinatesStart)  //pickup dist
                    let distDrop = source.distance(from: coordinatesEnd)
                    i = i+1
                    //check if trip is already sorted before
                    if DriverManager.sharedInstance.arr_SortedShareTrips != nil{
                        var check = false
                        for dict in DriverManager.sharedInstance.arr_SortedShareTrips!{
                            if (modal.tripData?.id ?? "" ) == dict[KeysForSharingPickUpViewController.TRIPID] as! String
                            {
                                check = true
                                arr_Sorted.append(dict)
                            }
                        }
                        if check == true { continue} // yes already processed no need to go further
                    }
                    
                    //pickup Dict
                    var dictAllPickup = [KeysForSharingPickUpViewController.TYPE:KeysForSharingPickUpViewController.PICK,
                                         KeysForSharingPickUpViewController.COORDINATE:coordinatesStart,
                                         KeysForSharingPickUpViewController.DIST:distPick,
                                         KeysForSharingPickUpViewController.PICKUPNUMBER:i,
                                         KeysForSharingPickUpViewController.TRIPID:modal.tripData?.id ?? "",
                                         KeysForSharingPickUpViewController.RIDEDETAILS: modal,                                 KeysForSharingPickUpViewController.PickUPStatus.STATUS: KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP,KeysForSharingPickUpViewController.NAME:modal.riderData?.fullName ?? "Rider Name" , KeysForSharingPickUpViewController.ADDRESS : modal.tripData?.startAddress ?? "" ]
                        as [String : Any]
                    
                    //Drop Dict
                    var dictAllDrop = [KeysForSharingPickUpViewController.TYPE:KeysForSharingPickUpViewController.DROP,
                                       KeysForSharingPickUpViewController.COORDINATE:coordinatesEnd,
                                       KeysForSharingPickUpViewController.DIST:distDrop,
                                       KeysForSharingPickUpViewController.PICKUPNUMBER:i,
                                       KeysForSharingPickUpViewController.TRIPID:modal.tripData?.id ?? "",
                                       KeysForSharingPickUpViewController.RIDEDETAILS: modal,
                                       KeysForSharingPickUpViewController.PickUPStatus.STATUS: KeysForSharingPickUpViewController.PickUPStatus.WAITDROP,KeysForSharingPickUpViewController.NAME:modal.riderData?.fullName ?? "Rider Name" , KeysForSharingPickUpViewController.ADDRESS : modal.tripData?.endAddress ?? "" ] as [String : Any]
                    
                    // this  handling done if app terminated
                    if isPendingTrip == true{
                        switch(modal.tripData?.status ?? 0){
                            
                        case TripOnState.ON_THE_WAY.rawValue:
                            
                            break
                            
                        case TripOnState.ONGOING.rawValue:
                            dictAllPickup[KeysForSharingPickUpViewController.PickUPStatus.STATUS] = KeysForSharingPickUpViewController.PickUPStatus.PICKED
                            break
                            
                        case TripOnState.COMPLETED.rawValue:
                            dictAllDrop[KeysForSharingPickUpViewController.PickUPStatus.STATUS] = KeysForSharingPickUpViewController.PickUPStatus.DROPPED
                            break
                            
                        default :
                            
                            break
                        }
                    }
                    
                    arr_Sorted.append(dictAllPickup)
                    arr_Sorted.append(dictAllDrop)
                }
                
            }else{
                //                NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
                // No trips to process
                return
            }
            
            DriverManager.sharedInstance.arr_SortedShareTrips = arr_Sorted.sorted {  // this one keep the track of sorted trips in global
                switch ($0[key], $1[key]) {
                case let (lhs as Double, rhs as Double):
                    return  lhs < rhs
                default:
                    return true
                }
            }
            if isPendingTrip == true{manage_PickAndDrop_ViewShow_ForTermintedApp()} // only because app terminated
        }
        
        //*******************************get the sorted array now process to show on screen****************************/
        showDataOnScreen()
        
    }
    
    /**
     Populate Data on screen and show Station on map
     */
    
    fileprivate func showDataOnScreen(){
        
        let source =  CLLocation(latitude: CLLocationDegrees(currentLocation.latitude), longitude:CLLocationDegrees(currentLocation.longitude))
        var lastCoordinate = source.coordinate
        var singlePathDrawn = false
        var arr_Tracker = [0]  // keep track we processed the source time to process pickups and drops
        
        for modal in DriverManager.sharedInstance.arr_SortedShareTrips!{
            
            let pickup = modal[KeysForSharingPickUpViewController.COORDINATE] as! CLLocation
            
            if modal[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String == KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP || modal[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String == KeysForSharingPickUpViewController.PickUPStatus.WAITDROP{
                
                if singlePathDrawn == false{   // allowed only one path to draw
                    singlePathDrawn = true
                    if DriverManager.sharedInstance.isAggregateTrip == true{
                      showDetailsAggregate(data: modal) //Populate data on view
                    }else{
                        showDetails(data: modal) //Populate data on view
                    }
                    if arr_Tracker.count > 1{
                        showPolyLine(source: lastCoordinate, destination: pickup.coordinate)
                        sourceNav = lastCoordinate
                        destinationNav = pickup.coordinate
                    }else{
                        showPolyLine(source: source.coordinate, destination: pickup.coordinate)
                        sourceNav = source.coordinate
                        destinationNav = pickup.coordinate
                    }
                }
            }
            
            lastCoordinate = pickup.coordinate  //last coordinate
            if KeysForSharingPickUpViewController.DROP == modal[KeysForSharingPickUpViewController.TYPE] as? String ?? "" {
                putMarker(with: "Drop Location", coordinate: pickup.coordinate, color: .darkGray, number: modal[KeysForSharingPickUpViewController.PICKUPNUMBER] as! Int)
            }else{
                putMarker(with: "PickUP Location", coordinate: pickup.coordinate, color: App_Base_Color, number: modal[KeysForSharingPickUpViewController.PICKUPNUMBER] as! Int)
            }
            arr_Tracker.append(modal[KeysForSharingPickUpViewController.PICKUPNUMBER] as! Int)
        }
        
        //********************** check if there is SharingDropViewController then reload data on that viewcontroller as well  **********************
        let arrControllers = self.childViewControllers
        if arrControllers.count > 0{
            if  let contoller = arrControllers[0] as? SharingDropViewController{
                contoller.reloadDataOnView()
            }
        }
    }
    
    fileprivate func putMarker(with title:String,coordinate:CLLocationCoordinate2D, color:UIColor, number:Int){
        let marker2 = GMSMarker(position: coordinate)
        marker2.title = title
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon = getLabelCount(number: number, color: color)
        marker2.map = mapView
    }
    
    fileprivate func getLabelCount(number:Int , color:UIColor) -> UIImage{
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        label.textColor = .white
        label.cornerRadius = 15
        label.text = "\(number)" // set text here
        label.textAlignment = .center
        label.backgroundColor = color
        
        //grab it
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, UIScreen.main.scale)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imagePick = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imagePick ?? #imageLiteral(resourceName: "on_map_arrival_location")
    }
    
    fileprivate func showDetails(data:[String:Any]){
        let rideData = (data[KeysForSharingPickUpViewController.RIDEDETAILS] as! TripModalDriver)
        self.lbl_PassengerCount.text = "\(data[KeysForSharingPickUpViewController.PICKUPNUMBER] as! Int)"
        self.lbl_DriverName.text = (data[KeysForSharingPickUpViewController.NAME] as? String)?.nameFormatted
        self.lbl_DurationTime.text = "\(String(format: "%.2f", rideData.tripData?.distance ?? 0)) KM/\(Int(rideData.tripData?.duration ?? 0)) Min"
        //        self.lbl_DurationTime.text = "\(String(format: "%.2f", DriverManager.sharedInstance.ride_Details?.tripData?.distance ?? 0)) KM/\(Int(DriverManager.sharedInstance.ride_Details?.tripData?.duration ?? 0)) Min"
        self.lbl_PickUpAddress.text = data[KeysForSharingPickUpViewController.ADDRESS] as? String
        
    }
    
    fileprivate func showDetailsAggregate(data:[String:Any]){
        let rideData = (data[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal)
        self.lbl_PassengerCount.text = "\(data[KeysForSharingPickUpViewController.PICKUPNUMBER] as! Int)"
        self.lbl_DriverName.text = (data[KeysForSharingPickUpViewController.NAME] as? String)?.nameFormatted
        self.lbl_DurationTime.text = "\(String(format: "%.2f", rideData.distance ?? 0)) KM/\(Int(rideData.duration ?? 0)) Min"
        self.lbl_PickUpAddress.text = data[KeysForSharingPickUpViewController.ADDRESS] as? String
    }
    
    //MARK:- custom method
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            DispatchQueue.main.async {self.showPath(polyStr: returnPath)}
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        } 
        hideLoader()
    }
    
    /**
     Add Drop view controller as subview for Drop Option for driver
     */
    
    func addsubView(){
        DriverManager.sharedInstance.driverStartLocation = CLLocation.init(latitude: self.currentLocation.latitude, longitude: self.currentLocation.longitude)
        self.view_DropPosition.setNeedsLayout()
        self.view_DropPosition.isHidden = false
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SharingDropViewController") as! SharingDropViewController
        controller.view.frame = self.view_DropPosition.frame
        controller.delegateShare = self
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        self.didMove(toParentViewController: controller)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     This will handle the view show only if app comes from terminated state
     */
    
    fileprivate func manage_PickAndDrop_ViewShow_ForTermintedApp(){
        //*******************************Get the array and sort whom to pick****************************/
        for (_,element) in DriverManager.sharedInstance.arr_SortedShareTrips!.enumerated(){
            
            var tripToServe = element
            if KeysForSharingPickUpViewController.PICK ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&
                KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
                //****this will process the trip whose pickup pending

                
                DriverManager.sharedInstance.tripId = (tripToServe[KeysForSharingPickUpViewController.TRIPID] as! String)
                DriverManager.sharedInstance.ride_Details = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! TripModalDriver)
                break
            }else if KeysForSharingPickUpViewController.DROP ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITDROP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
               
                //****Still there pending trip for Drop
                self.addsubView()
                break
            }
        }
    }

    //MARK:- Button Action
    
    @IBAction func navigateToMap(_ sender: UIButton) {
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            
            if self.sourceNav != nil && self.destinationNav != nil{
                if let testURL = URL(string:
                    "comgooglemaps://?saddr=\(self.sourceNav?.latitude ?? 0.0),\(self.sourceNav?.longitude ?? 0.0)&daddr=\(self.destinationNav?.latitude ?? 0.0),\(self.destinationNav?.longitude ?? 0.0)&directionsmode=driving"){
                    
                    UIApplication.shared.open(testURL, options: ["":""], completionHandler: { (completer) in  })
                }
            }
        }
        else{
            NSLog("Can't use com.google.maps://");
            if let testURL = URL(string:
                "http://maps.apple.com/?saddr=\(self.sourceNav?.latitude ?? 0.0),\(self.sourceNav?.longitude ?? 0.0)&daddr=\(self.destinationNav?.latitude ?? 0.0),\(self.destinationNav?.longitude ?? 0.0)"){
                UIApplication.shared.open(testURL, options: ["":""], completionHandler: { (completer) in  })
            }
        }
    }
    
    @IBAction func call_Rider(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CallingTwillioViewController") as! CallingTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func msg_Rider(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MessageTwillioViewController") as! MessageTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func pickUp(_ sender: UIButton) {
        
        manage_PickAndDrop_ViewShow()  // manage  station for pick and drop
        DriverManager.sharedInstance.ignoreRequestNow = true // New request not allowed
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "GettingStratedViewController") as! GettingStratedViewController
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true) {
            //            self.checkTheTrips()
            self.showDataOnScreen()
        }
    }
    
    @IBAction func viewAllTasks(_ sender: UIButton) {
        
    }
    
    /**
     This will handle the view show for normal sharing going on
     */
    
    @objc fileprivate func manage_PickAndDrop_ViewShow(){
        //*******************************Get the array and sort whom to pick****************************/
        for (i,element) in DriverManager.sharedInstance.arr_SortedShareTrips!.enumerated(){
            
            var tripToServe = element
            if KeysForSharingPickUpViewController.PICK ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&  
                KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
                
                 //****this will process the trip whose pickup pending
//                print("this one is for pickup \(i)")
//                print((tripToServe[KeysForSharingPickUpViewController.TRIPID] as! String))
//                print((tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! TripModalDriver))
                
                DriverManager.sharedInstance.tripId = (tripToServe[KeysForSharingPickUpViewController.TRIPID] as! String)
                
                // Aggregate Trip Handling
                if DriverManager.sharedInstance.isAggregateTrip == true{
                     DriverManager.sharedInstance.ride_DetailsAggreagate = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal)
                }else{
                      // Shared Trip Handling
                DriverManager.sharedInstance.ride_Details = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! TripModalDriver)
                }
                tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] = KeysForSharingPickUpViewController.PickUPStatus.PICKED
                DriverManager.sharedInstance.arr_SortedShareTrips?.remove(at: i)
                DriverManager.sharedInstance.arr_SortedShareTrips?.insert(tripToServe, at: i)
                
                
                //******* if next station is for drop then add subview for drop view
                
                var lastPossibleIndex = i+1
                while (lastPossibleIndex < DriverManager.sharedInstance.arr_SortedShareTrips?.count ?? 0){
                    var nextTrip =  DriverManager.sharedInstance.arr_SortedShareTrips![lastPossibleIndex]
                    lastPossibleIndex = lastPossibleIndex+1
                    if KeysForSharingPickUpViewController.DROP ==  nextTrip[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITDROP == nextTrip[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String {
                        
                        //****Still there pending trip for Drop
                        self.addsubView()
                        break
                    }
                    if KeysForSharingPickUpViewController.PICK ==  nextTrip[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.PICKED == nextTrip[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String {
                        continue  //because this one is already picked nothing to do
                    }
                }
                break
            }else if KeysForSharingPickUpViewController.DROP ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITDROP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
              
                 //****Still there pending trip for Drop
                self.addsubView()
                break
            }
        }
    }
}

//MARK::LocationDelegates
extension SharingPickUpViewController: LocationDelegates {
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            /* In case if current location is not the starting location of trip **/
            let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(DriverManager.sharedInstance.ride_Details?.tripData?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(DriverManager.sharedInstance.ride_Details?.tripData?.startLocation?.coordinates?[0] ?? 0))
            self.mapView.animate(toLocation: coordinates)
            currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            showDriverIcon(location: currentLocation)
        }
    }
    
    func didFailToUpdate(error: Error) {
        self.hideLoader()
    }
}

//MARK::GettingStratedDelegate
extension SharingPickUpViewController: GettingStartedViewControllerDelegate{
    
    func presentNewController() {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "WaitForRiderViewController") as! WaitForRiderViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
}

//MARK::SharingDropViewControllerDelegate
extension SharingPickUpViewController: SharingDropViewControllerDelegate{
    func removeDropView() {
        self.view_DropPosition.isHidden = true
        self.view_DropPosition.sendSubview(toBack: self.view)
    }
}
