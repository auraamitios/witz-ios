//
//  SharingDropViewController.swift
//  Witz
//
//  Created by abhishek kumar on 27/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

protocol SharingDropViewControllerDelegate : NSObjectProtocol {
    func removeDropView()
}

class SharingDropViewController: UIViewController {

    @IBOutlet weak fileprivate var lbl_Address: UILabel!
    @IBOutlet weak fileprivate var lbl_NextDrop: UILabel!
    @IBOutlet weak fileprivate var lbl_DriverName: UILabel! // this label use to show rider name instead :P
    @IBOutlet weak fileprivate var lbl_NumberOfTask: UILabel!
    @IBOutlet weak fileprivate var lbl_DurationTime: UILabel!
    
    weak var delegateShare : SharingDropViewControllerDelegate?
    var sendIndex : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        showDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showDetails()
    }
    
    public func reloadDataOnView(){
        showDetails()
    }
    
    fileprivate func showDetails(){
        
        //*******************************Get the array and sort whom to Drop****************************/
        for (i,element) in DriverManager.sharedInstance.arr_SortedShareTrips!.enumerated(){
           
            var tripToServe = element
            if KeysForSharingPickUpViewController.DROP ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITDROP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
                
                print(tripToServe)
                //****this will process the trip whose drop pending
                DriverManager.sharedInstance.tripIdDrop = (tripToServe[KeysForSharingPickUpViewController.TRIPID] as! String)
                
                // Aggregate Trip Handling
                if DriverManager.sharedInstance.isAggregateTrip == true {
                    DriverManager.sharedInstance.ride_DetailsDropAggregate = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal)

                    let rideData = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal)
                    self.lbl_DurationTime.text = "\(String(format: "%.2f", rideData.distance ?? 0)) KM/\(Int(rideData.duration ?? 0)) Min"
                }else{
                    // Shared Trip Handling
                DriverManager.sharedInstance.ride_DetailsDrop = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! TripModalDriver)
                    let rideData = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! TripModalDriver)
                    self.lbl_DurationTime.text = "\(String(format: "%.2f", rideData.tripData?.distance ?? 0)) KM/\(Int(rideData.tripData?.duration ?? 0)) Min"
                }
                
                sendIndex = i
                self.lbl_NumberOfTask.text = "\(tripToServe[KeysForSharingPickUpViewController.PICKUPNUMBER] as! Int)"
                self.lbl_DriverName.text = (tripToServe[KeysForSharingPickUpViewController.NAME] as? String)?.nameFormatted
                self.lbl_Address.text = tripToServe[KeysForSharingPickUpViewController.ADDRESS] as? String
                let lastPossibleIndex = i+1
                
                
//                Check if there is more trip  for pickup remain unprocessed
                if lastPossibleIndex < DriverManager.sharedInstance.arr_SortedShareTrips?.count ?? 0{
                    
                    var nextTrip =  DriverManager.sharedInstance.arr_SortedShareTrips![lastPossibleIndex]
                    if KeysForSharingPickUpViewController.PICK ==  nextTrip[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP == nextTrip[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
                         //****Still there pending trip for pickup
                        delegateShare?.removeDropView()
                        self.view.removeFromSuperview()
                        self.removeFromParentViewController()
                    }
                }
                break
                
            }else if KeysForSharingPickUpViewController.PICK ==  tripToServe[KeysForSharingPickUpViewController.TYPE] as! String &&  KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP == tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String{
              
                //****Still there pending trip for pickup
                delegateShare?.removeDropView()
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
                break
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func view_AllTasks(_ sender: UIButton) {
        
    }
    
    @IBAction func dropRider(_ sender: UIButton) {
        
        showDetails()
        DriverManager.sharedInstance.ignoreRequestNow = true // New request not allowed
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "TripAboutToEndViewController") as! TripAboutToEndViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.tripEndingFor = sendIndex
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
}

// Trip About to end delegate
extension SharingDropViewController : TripAboutToEndDelegate{
    func reloadViewData() {
        showDetails()
    }
    
    
}
