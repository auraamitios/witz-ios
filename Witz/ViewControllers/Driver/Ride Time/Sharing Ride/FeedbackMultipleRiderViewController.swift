//
//  FeedbackMultipleRiderViewController.swift
//  Witz
//
//  Created by abhishek kumar on 27/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class FeedbackMultipleRiderViewController: WitzSuperViewController {
    
    @IBOutlet weak var table_View: UITableView!
    
    var dictStar = [1:0,2:0,3:0,4:0,0:0]
    var arrUser = [TripModalDriver]()
    var arrUserAggregate = [AggregateDriverStartTripModal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        fetchRiders()
        DispatchQueue.global(qos: .background).async {
            self.change_Duty("Idle")}
    }
    
    
    fileprivate func change_Duty(_ str : String) {
        guard self.checkNetworkStatus() else {
            return
        }
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/change_duty_status", withInputString: ["status":str], requestType: .post, isAuthorization: true, success: { (response) in
        }) { (error ) in
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchRiders()  {
        if DriverManager.sharedInstance.isAggregateTrip == true{
            for modal in DriverManager.sharedInstance.arr_SortedShareTrips!{
//                if modal[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String == KeysForSharingPickUpViewController.PickUPStatus.DROPPED{
                
                    let rider = modal[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal
                    arrUserAggregate.append(rider)
//                }
            }
        }else{
        for modal in DriverManager.sharedInstance.arr_SortedShareTrips!{
            if modal[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as! String == KeysForSharingPickUpViewController.PickUPStatus.DROPPED{
                
                let rider = modal[KeysForSharingPickUpViewController.RIDEDETAILS] as! TripModalDriver
                arrUser.append(rider)
            }
            }
        }
    }
    
    @IBAction func finsih_Feedback(_ sender: UIButton) {
        DriverManager.sharedInstance.ignoreRequestNow = false  // New request  allowed
        DriverManager.sharedInstance.shareTripGoingOn = false
        DriverManager.sharedInstance.arr_SortedShareTrips?.removeAll()
        DriverManager.sharedInstance.arr_ShareTrips?.removeAll()
        
        if DriverManager.sharedInstance.isAggregateTrip == true{
            self.dismiss(animated: true, completion: {
                DriverManager.sharedInstance.isAggregateTrip = false
                Utility.driverSideMenuSetUp()
            })
            return
        }
//        print("value for user \(dictStar)")
        self.dismiss(animated: true) {

            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
        }
    }
}
extension  FeedbackMultipleRiderViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if DriverManager.sharedInstance.isAggregateTrip == true{ return arrUserAggregate.count }
        return arrUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFeedbackMultipleRider", for: indexPath) as! CellFeedbackMultipleRider
        cell.delegate = self
        if DriverManager.sharedInstance.isAggregateTrip == true{
            cell.configureCellAggregate(with:arrUserAggregate[indexPath.row], index: indexPath)
        }else{
        cell.configureCell(with:arrUser[indexPath.row], index: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
extension FeedbackMultipleRiderViewController : feedBackStarCount{
    
    func feedBackStars(star: Int, forCell: Int) {
        if DriverManager.sharedInstance.isAggregateTrip == true{
            let modal = arrUserAggregate[forCell]
            doneFeedback(riderSTars: star, tripId: modal.id ?? "", riderId: modal.riderId ?? "")
            dictStar[forCell] = star
            return
        }
        let modal = arrUser[forCell]
        doneFeedback(riderSTars: star, tripId: modal.tripData?.id ?? "", riderId: modal.riderData?.id ?? "")
        dictStar[forCell] = star
    }
    
    fileprivate func doneFeedback(riderSTars:Int , tripId: String ,riderId:String)  {
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let parameters = ["tripId": tripId , "riderId":riderId,"riderRating":riderSTars] as [String : Any]
        
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/rate_rider", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
        }) { (error ) in
            self.hideLoader()
        }
    }
    
}
