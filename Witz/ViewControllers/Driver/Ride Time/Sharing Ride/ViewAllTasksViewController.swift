//
//  ViewAllTasksViewController.swift
//  Witz
//
//  Created by abhishek kumar on 27/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit


class ViewAllTasksViewController: UIViewController {
    
    @IBOutlet weak fileprivate var table_View: UITableView!
   
    var arrTasks  = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

        checkTheTrips()
        
        table_View.rowHeight = 140
        table_View.estimatedRowHeight = UITableViewAutomaticDimension
        
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /**
     
     this will check the arr of shared trip and calculate the trip distance and apply the algorithm afterwards
     */
    
    fileprivate func checkTheTrips(){

        arrTasks = DriverManager.sharedInstance.arr_SortedShareTrips ?? []
        table_View.reloadData()
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension  ViewAllTasksViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTasksList", for: indexPath) as! CellTasksList
        cell.configureCell(with:arrTasks[indexPath.row], index: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

