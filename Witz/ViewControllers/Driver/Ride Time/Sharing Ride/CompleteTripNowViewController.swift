//
//  CompleteTripNowViewController.swift
//  Witz
//
//  Created by abhishek kumar on 27/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CompleteTripNowViewController: UIViewController {
    
    @IBOutlet weak var btn_CompletTrip: UIButton!
    @IBOutlet weak fileprivate var table_View: UITableView!
    var arrCompletedTasks  = [[String:Any]]()
    
    var fromHistory : Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        checkTheTrips()
        if fromHistory == true{
            btn_CompletTrip.setTitle("BACK", for: .normal)
        }else{
            DriverManager.sharedInstance.ignoreRequestNow = true  // New request not allowed
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     
     this will check the arr of shared trip and populate
     */
    
    fileprivate func checkTheTrips(){
        arrCompletedTasks = DriverManager.sharedInstance.arr_SortedShareTrips ?? []
        table_View.reloadData()
    }
    
    @IBAction func completeTripTapped(_ sender: UIButton) {
        
        if fromHistory == true{
            self.navigationController?.popViewController()
        }else{
        self.performSegue(withIdentifier: "segue_SharedFeedback", sender: nil)
        }
        
    }
    
    
}
extension  CompleteTripNowViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCompletedTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellCompletedTasksList", for: indexPath) as! CellTasksList
        cell.configureCellCompletedTask(with:arrCompletedTasks[indexPath.row], index: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

