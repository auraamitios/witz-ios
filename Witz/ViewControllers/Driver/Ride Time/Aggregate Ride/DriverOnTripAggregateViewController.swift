//
//  DriverOnTripAggregateViewController.swift
//  Witz
//
//  Created by abhishek kumar on 16/04/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class DriverOnTripAggregateViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_DurationTime: UILabel!
    @IBOutlet weak var btn_EndAggregate: UIButton!
    
    fileprivate var marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
fileprivate lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Main, bundle: nil)
    var grpID : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        showLoader()
        LocationUpdate.sharedInstance.delegate = self
        LocationUpdate.sharedInstance.startTracking()
        if grpID != nil {
            getRideDetailAggregate(grpId: grpID!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        LocationUpdate.sharedInstance.stopTracking()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Button Action
   
    
    @IBAction func endAggregate_Tapped(_ sender: UIButton) {
        
        if stillRiderOnVehicle() == false{
        let controller = self.storyBoard.instantiateViewController(withIdentifier: "TripEndedViewController") as! TripEndedViewController
        self.navigationController?.pushViewController(controller, animated: true)
        }else {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.DriverPopUpMessage.DropAllFirst , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            }, controller: self)
        }
    }
    
    fileprivate func stillRiderOnVehicle() -> Bool{
       let arr = DriverManager.sharedInstance.arr_SortedShareTrips
        var statusToSend = false
        for item in arr! {
            if KeysForSharingPickUpViewController.PickUPStatus.DROPPED
                != item[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as? String ?? ""{
                
                statusToSend = true
                break
            }
        }
        return statusToSend
    }
    
    //MARK:- get Ride Detail from server
    public func getRideDetailAggregate(grpId : String){
        
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =  ["aggregateGroupId":grpId]
        showLoader()
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "aggregate/get_final_aggregate_trip", withInputString: parameters, requestType: .get, isAuthorization: false, success: { (response) in
            self.hideLoader()
            if response?["statusCode"] == 200{
                self.hideLoader()
                if let  result = response?["data"]{
                    
                    print(result)
                    let tripDetails = AgrregateTripModal(json: result)
                    
                    if (tripDetails.wayPoints?.count ?? 0) > 0 {self.drawPathUsing(wayPoints: tripDetails.wayPoints!)}
                    
                    if (tripDetails.trips?.count ?? 0)  > 0 {
                        self.plotRider_OnMap(tripDetails.trips!)
                        self.lbl_DurationTime.text = "\(String(format: "%.2f",tripDetails.trips![0].distance ?? 0.0))KM/\(String(format: "%.2f",tripDetails.trips![0].duration ?? 0.0))Min"
                    }
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
   
    fileprivate func drawPathUsing(wayPoints:[WayPointsAggregate]){
        
        mapView.clear()
        let start = wayPoints[0]
        let source =  CLLocation(latitude: CLLocationDegrees(start.coordinates?[1] ?? 0), longitude:CLLocationDegrees(start.coordinates?[0] ?? 0))
        
        putMarker(with: "Pickup Location", coordinate: source.coordinate, img: #imageLiteral(resourceName: "flag"))  // pickup Station
        drawCircle(position: source.coordinate) // overlay drawed
        let end = wayPoints[wayPoints.count-1]
        let destination =  CLLocation(latitude: CLLocationDegrees(end.coordinates?[1] ?? 0), longitude:CLLocationDegrees(end.coordinates?[0] ?? 0))
        putMarker(with: "Drop Location", coordinate: destination.coordinate, img: #imageLiteral(resourceName: "flag"))// drop Station
        
        var arr = [[String : Any]]()  // it will keep all coodinate and distance from source
        
        if wayPoints.count < 3{ // no halt between the path
        showPolyLine(source: source.coordinate, destination: destination.coordinate, isDashedLine: false)
        }
        
        // preparing arr of dict whose conatains coordinate and distance
        for i in 1..<wayPoints.count {
            
            let modal = wayPoints[i]
            let halt =  CLLocation(latitude: CLLocationDegrees(modal.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.coordinates?[0] ?? 0))
            let distPick = source.distance(from: halt)
            putMarker(with: "Stop", coordinate: halt.coordinate, img: #imageLiteral(resourceName: "flag"))// stop Station
            drawCircle(position: halt.coordinate) // overlay drawed
            
            let dict = [KeysForSharingPickUpViewController.COORDINATE:halt.coordinate,KeysForSharingPickUpViewController.DIST:distPick] as [String : Any]
            arr.append(dict)
        }
        
        let key = KeysForSharingPickUpViewController.DIST // The key you want to sort by
        
        let finalarray = arr.sorted {  // this one keep the track of sorted coordinates for plotting
            switch ($0[key], $1[key]) {
            case let (lhs as Double, rhs as Double):
                return  lhs < rhs
            default:
                return true
            }
        }
        
        var sourceStation = source.coordinate // get the source coordinate
        for i in 0..<finalarray.count {
            let dict = finalarray[i]
            let station = dict[KeysForSharingPickUpViewController.COORDINATE] as! CLLocationCoordinate2D
            showPolyLine(source: sourceStation , destination: station, isDashedLine: false)
            sourceStation = station // change source station now
        }
    }
    
    fileprivate func plotRider_OnMap(_ arrTrip:[TripsAggregate]){
        
        var i = 0  // keep the sequence of customers
        for modal in arrTrip{
            
            i = i+1
            
            let source =  CLLocation(latitude: CLLocationDegrees(modal.adminMarkPickUpLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.adminMarkPickUpLocation?.coordinates?[0] ?? 0))
            let destination =  CLLocation(latitude: CLLocationDegrees(modal.adminMarkDropOffLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.adminMarkDropOffLocation?.coordinates?[0] ?? 0))
            
            putMarker(with: "Pickup Location", coordinate: source.coordinate, img: #imageLiteral(resourceName: "riderPick "))
            putMarker(with: "Drop Location", coordinate: destination.coordinate,  img: #imageLiteral(resourceName: "riderDrop"))
            
        }
    }
    
    fileprivate func drawCircle(position: CLLocationCoordinate2D) {
        let circle = GMSCircle(position: position, radius: 50)
        circle.strokeColor = App_Base_Color
        circle.fillColor = UIColor(red: 0, green: 1, blue: 0, alpha: 0.5)
        circle.map = mapView
    }
    
    fileprivate func putMarker(with title:String,coordinate:CLLocationCoordinate2D, img:UIImage){
        let marker2 = GMSMarker(position: coordinate)
        marker2.title = title
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon = img
        marker2.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        marker2.map = mapView
    }
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D ,isDashedLine:Bool){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            DispatchQueue.main.async { self.showPath(polyStr: returnPath, isDashedLine: isDashedLine)}
        }
    }
    
    fileprivate func showPath(polyStr :String ,isDashedLine:Bool){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            
            if isDashedLine {
                let styles :[GMSStrokeStyle] = [GMSStrokeStyle.solidColor(.green),GMSStrokeStyle.solidColor(.clear)]
                let length : [NSNumber] = [10,5]
                polyline.spans = GMSStyleSpans(polyline.path!, styles, length, GMSLengthKind.rhumb)
            }
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100.0))
        }
        hideLoader()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        let controller = segue.destination as! ViewAllPassengerViewController
    }
    
}
//MARK::LocationDelegates
extension DriverOnTripAggregateViewController: LocationDelegates {
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            
            self.marker.position = location.coordinate
            marker.title = "Witz"
            marker.map = self.mapView
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.icon = DriverManager.sharedInstance.VehicleOwnedbyDriver()
            self.hideLoader()
        }
    }
    
    func didFailToUpdate(error: Error) {
        self.hideLoader()
    }
}
