//
//  AggregateStartConfirmationViewController.swift
//  Witz
//
//  Created by abhishek kumar on 27/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import CoreLocation

enum ShiftTime : Int {
    case Morning = 1
    case Evening
}
class AggregateStartConfirmationViewController: UIViewController {
    var groupID : String?
    var navController : UINavigationController?
    fileprivate var currentLocation = CLLocationCoordinate2D(latitude: AppManager.sharedInstance.locationCoordinates.last?.coordinate.latitude ?? APPLEOFFICELOCATION_LAT, longitude:AppManager.sharedInstance.locationCoordinates.last?.coordinate.longitude  ?? APPLEOFFICELOCATION_LNG)
    
    //properties for data passing
    
    var shiftFor : ShiftTime?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeView))
        self.view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func closeView()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func arrivalTimeTapped(_ sender: UIButton) {
       
        if shiftFor == ShiftTime.Morning{
            getFinalAggregate(with: ShiftTime.Morning)
        }else{
            getFinalAggregate(with: ShiftTime.Evening)
        }
    }
    
    @IBAction func later_Tapped(_ sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
//        Utility.driverSideMenuSetUp()
    }
    
    
    fileprivate func getFinalAggregate(with type:ShiftTime){
        
        guard self.checkNetworkStatus() else {
            return
        }
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        let parameters =  ["aggregateGroupId":groupID!,"today":"true","aggregateTripType":type.rawValue,"timezone":localTimeZoneName] as [String : Any]
        showLoader()
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/get_Final_aggregate_trips_by_groupId", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            if response?["statusCode"] == 200{
                
                if let data = response?["data"]["tripsInfo"].array{
                    if data.count > 0 {
                        var arr = [AggregateDriverStartTripModal]()
                        for dict in data {
                            let modal = AggregateDriverStartTripModal.init(json: dict)
                            arr.append(modal)
                        }
                        if arr.count > 0 {
                            self.showTripsCalculation(arrTrip:arr)
                        }
                    }else{
                        if type == ShiftTime.Morning{
                            self.tripCompletedAlready(strMsg: LinkStrings.DriverOnTrip.Aggregate.MorningCompleted)
                        }else{
                            self.tripCompletedAlready(strMsg: LinkStrings.DriverOnTrip.Aggregate.EveningCompleted)
                        }
                    }
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
    
    //MARK:- custom method to show on view all task
    
    fileprivate func showTripsCalculation(arrTrip:[AggregateDriverStartTripModal]){
        
        var arr_Sorted = [[String : Any]]()   // contain the trips modified dictonary for calculation
        var i = 0  // keep the sequence of customers
        let source =  CLLocation(latitude: CLLocationDegrees(currentLocation.latitude), longitude:CLLocationDegrees(currentLocation.longitude))
        
        // this calulation  for aggregate trip
        
        for modal in arrTrip{
            let coordinatesStart = CLLocation(latitude: CLLocationDegrees(modal.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.startLocation?.coordinates?[0] ?? 0))
            let coordinatesEnd = CLLocation(latitude: CLLocationDegrees(modal.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.endLocation?.coordinates?[0] ?? 0))
            let distPick = source.distance(from: coordinatesStart)  //pickup dist
            i = i+1
            
            //pickup Dict
            let dictAllPickup = [KeysForSharingPickUpViewController.TYPE:KeysForSharingPickUpViewController.PICK,
                                 KeysForSharingPickUpViewController.COORDINATE:coordinatesStart,
                                 KeysForSharingPickUpViewController.ENDCOORDINATE:coordinatesEnd,
                                 KeysForSharingPickUpViewController.DIST:distPick,
                                 KeysForSharingPickUpViewController.PICKUPNUMBER:i,
                                 KeysForSharingPickUpViewController.TRIPID:modal.id ?? "",
                                 KeysForSharingPickUpViewController.RIDEDETAILS: modal,                                 KeysForSharingPickUpViewController.PickUPStatus.STATUS: KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP,KeysForSharingPickUpViewController.NAME:modal.riderName ?? "Rider Name" , KeysForSharingPickUpViewController.ADDRESS : modal.startAddress ?? "", KeysForSharingPickUpViewController.ENDADDRESS : modal.endAddress ?? "" ]
                as [String : Any]
            
            arr_Sorted.append(dictAllPickup)
        }
        
        DriverManager.sharedInstance.arr_SortedShareTrips = arr_Sorted

        
        //Now go to next controller to start the trip
        self.prepareRidePrequest()
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func tripCompletedAlready(strMsg:String){
        let alert  = UIAlertController.init(title: LinkStrings.AlertTitle.Title, message: strMsg, preferredStyle: .alert)
        let ok = UIAlertAction.init(title: LinkStrings.MostCommon.Ok, style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.DriverAggregate, bundle: nil)
    
    func prepareRidePrequest(){
        DriverManager.sharedInstance.shareTripGoingOn = true
        DriverManager.sharedInstance.ignoreRequestNow = true // New request not allowed
        //        DriverManager.sharedInstance.tripId = groupID
        let controller = self.storyBoard.instantiateViewController(withIdentifier: "DriverOnTripAggregateViewController") as! DriverOnTripAggregateViewController
        DriverManager.sharedInstance.isAggregateTrip = true
        controller.grpID = groupID
        navController?.pushViewController(controller, animated: false)
    }
}
