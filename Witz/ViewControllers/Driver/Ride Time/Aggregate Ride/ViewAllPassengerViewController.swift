//
//  ViewAllPassengerViewController.swift
//  Witz
//
//  Created by abhishek kumar on 16/04/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class ViewAllPassengerViewController: UIViewController {
    
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var btn_Back: UIButton!
    
    fileprivate var arrTasks  = [[String:Any]]()
    fileprivate  lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Main, bundle: nil)
    
    //properties for data passing
    
    var shiftFor : ShiftTime?
    var fromHistory : Bool?
    var tripAboutEnd : Bool?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if tripAboutEnd == true{
            btn_Back.setTitle(LinkStrings.DriverOnTrip.CompleteTrip, for: .normal)
        }
        checkTheTrips()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /**
     
     this will check the arr of shared trip and calculate the trip distance and apply the algorithm afterwards
     */
    
    fileprivate func checkTheTrips(){
        
        arrTasks = DriverManager.sharedInstance.arr_SortedShareTrips ?? []
        table_View.reloadData()
    }
    
    //MARK :- Button Action
    
    @IBAction func back_Action(_ sender: UIButton) {
        
        if tripAboutEnd == true{
            
            let controller = self.storyBoard.instantiateViewController(withIdentifier: "FeedbackMultipleRiderViewController") as! FeedbackMultipleRiderViewController
            self.present(controller, animated: true, completion: nil)
            
            return
        }
        
        self.navigationController?.popViewController()
    }
    
}
//MARK:-
extension ViewAllPassengerViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellViewAllPassenger", for: indexPath) as! CellViewAllPassenger
     
        var hideButton = false
        if fromHistory == true || tripAboutEnd == true{
           hideButton = true
            cell.btn_PickDrop.isHidden = true
            cell.constraint_BtnPick_Drop.constant = 0
        }
        
        cell.configureCell(with:arrTasks[indexPath.row], index: indexPath, shift : self.shiftFor , hideButton : hideButton)
        
        cell.btn_PickDrop.addTarget(self, action: #selector(pickOrDropRider(_:)),  for: .touchUpInside)
        cell.btn_Call.addTarget(self, action: #selector(callRider(_:)),  for: .touchUpInside)
        cell.btn_Msg.addTarget(self, action: #selector(msgRider(_:)),  for: .touchUpInside)

        return cell
    }
    
    
    /**
     Drop or Pick rider Selection
     */
    func pickOrDropRider(_ sender:UIButton){
        
        if  sender.isSelected == false{
            pickRider(index: sender.tag)
        }else{
            dropRider(index: sender.tag)
        }
    }
    
    /**
     Show pickup flow controllers now to pick the rider
     */
    fileprivate func pickRider(index:Int){
        
        var tripToServe = arrTasks[index]
        DriverManager.sharedInstance.tripId = (tripToServe[KeysForSharingPickUpViewController.TRIPID] as! String)
        // Aggregate Trip Handling
        DriverManager.sharedInstance.ride_DetailsAggreagate = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal)
        let controller = storyBoard.instantiateViewController(withIdentifier: "GettingStratedViewController") as! GettingStratedViewController
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true)
        
        tripToServe[KeysForSharingPickUpViewController.PickUPStatus.STATUS] = KeysForSharingPickUpViewController.PickUPStatus.PICKED
        arrTasks.remove(at: index)
        arrTasks.insert(tripToServe, at: index)
        DriverManager.sharedInstance.arr_SortedShareTrips = arrTasks
        table_View.reloadData()
    }
    
    /**
     Show Drop flow controllers now to drop the rider
     */
    fileprivate func dropRider(index:Int){
        
        var tripToServe = arrTasks[index]
        DriverManager.sharedInstance.tripIdDrop = (tripToServe[KeysForSharingPickUpViewController.TRIPID] as! String)
        
        // Aggregate Trip Handling
        DriverManager.sharedInstance.ride_DetailsDropAggregate = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal)
        
        DriverManager.sharedInstance.ignoreRequestNow = true // New request not allowed
        
        let controller = storyBoard.instantiateViewController(withIdentifier: "TripAboutToEndViewController") as! TripAboutToEndViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.tripEndingFor = index
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    
    @objc fileprivate func callRider(_ sender:UIButton){
        
        let index = sender.tag
        var tripToServe = arrTasks[index]
        
        print(tripToServe)
        // Aggregate Trip Handling
        DriverManager.sharedInstance.ride_DetailsAggreagate = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal)
        
        let controller = storyBoard.instantiateViewController(withIdentifier: "CallingTwillioViewController") as! CallingTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc fileprivate func msgRider(_ sender:UIButton){
        let index = sender.tag
        var tripToServe = arrTasks[index]
        
        // Aggregate Trip Handling
        DriverManager.sharedInstance.ride_DetailsAggreagate = (tripToServe[KeysForSharingPickUpViewController.RIDEDETAILS] as! AggregateDriverStartTripModal)
        let controller = storyBoard.instantiateViewController(withIdentifier: "MessageTwillioViewController") as! MessageTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
}

//MARK:- GettingStratedDelegate
extension ViewAllPassengerViewController: GettingStartedViewControllerDelegate{
    
    func presentNewController() {
        
        let controller = storyBoard.instantiateViewController(withIdentifier: "WaitForRiderViewController") as! WaitForRiderViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
}

//MARK:- Trip About to end delegate
extension ViewAllPassengerViewController : TripAboutToEndDelegate{
   
    func reloadViewData() {
       
        table_View.reloadData {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
