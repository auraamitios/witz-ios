//
//  AggregateRequestConfirmedViewController.swift
//  Witz
//
//  Created by abhishek kumar on 08/02/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class AggregateRequestConfirmedViewController: WitzSuperViewController {

    var groupIdToSend : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//         self.setNavigationBarItem()
        self.left_barButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let controller = segue.destination as! SharingTripDetailsViewController
        controller.tripForAggregate = groupIdToSend
    }

}
