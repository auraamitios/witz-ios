//
//  SendOfferAggregateViewController.swift
//  Witz
//
//  Created by abhishek kumar on 27/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class SendOfferAggregateViewController: UIViewController {
    
    @IBOutlet fileprivate weak var lbl_DistanceTime: UILabel!
    @IBOutlet fileprivate weak var txt_Offer: UITextField!
    @IBOutlet fileprivate weak var btn_AcceptTerms: UIButton!
    
    @IBOutlet fileprivate weak var btn_Send: UIButton!
    @IBOutlet fileprivate weak var lbl_Exit: UILabel!
    @IBOutlet fileprivate weak var lbl_Arrival: UILabel!
    
    @IBOutlet fileprivate weak var lbl_StartAddress: UILabel!
    @IBOutlet fileprivate weak var lbl_EndAddress: UILabel!
    
    //Week Days
    
    @IBOutlet fileprivate var lbl_days: [UILabel]!
   
    var numberOfDaySelected : [Int]?
    var strArrival : String?
    var strExit : String?
    var strDistance : String?
    var strStartAddress : String?
    var strEndAddress : String?
    var groupID : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if numberOfDaySelected != nil {
            daysSelectedOfWeek(number: numberOfDaySelected!)
        }
        lbl_DistanceTime.text = strDistance
        lbl_Arrival.text = strArrival
        lbl_Exit.text = strExit
        lbl_StartAddress.text = strStartAddress
        lbl_EndAddress.text = strEndAddress
        txt_Offer.keyboardType = .numberPad
        sendButtonActivation(allowed: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func termsAndConditions(_ sender: UIButton) {
        
    }
    
    @IBAction func acceptTheTerms(_ sender: UIButton) {
        btn_AcceptTerms.isSelected = !btn_AcceptTerms.isSelected
        if btn_AcceptTerms.isSelected == true{
              sendButtonActivation(allowed: true)
            return
        }
        sendButtonActivation(allowed: false)
    }
    
    @IBAction func sendOffer(_ sender: UIButton) {
        if groupID != nil {
            if  txt_Offer.text?.isBlank() == true{
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyAmount , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            }, controller: self)
            return
        }
        guard self.checkNetworkStatus() else {
            return
        }
       
        let parameters =  ["groupAggregateTripId":groupID!,"bidOffer":self.txt_Offer.text ?? "NA"]
        showLoader()
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "aggregate/send_bid_offer_aggregate", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            if response?["statusCode"] == 200{
                
                if let controller = AppDelegate.sharedInstance().window?.rootViewController?.presentedViewController{
                    controller.dismiss(animated: true, completion: nil)
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
        }
    }
    
    //MARK:- custom method
    /**
     allow user to interact with send button
     */
    fileprivate func sendButtonActivation(allowed:Bool){
        if allowed == true{
            btn_Send.isUserInteractionEnabled = true
            btn_Send.alpha = 1.0
        return
        }
        btn_Send.isUserInteractionEnabled = false
        btn_Send.alpha = 0.5
    }
    
    /**
     this will help you indicate the selected days of week
     */
    fileprivate func daysSelectedOfWeek(number:[Int]){
        
        for  (index,lbl)  in lbl_days.enumerated() {
            if number.contains(index+1){
                lbl.backgroundColor = .white
                lbl.textColor = .darkGray
            }
            if index == 6 {  // for sunday selection only
                if number.contains(0){
                    lbl.backgroundColor = .white
                    lbl.textColor = .darkGray
                }
            }
        }
    }
}
