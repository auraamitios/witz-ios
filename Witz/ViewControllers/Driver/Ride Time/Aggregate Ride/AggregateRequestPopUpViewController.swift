//
//  AggregateRequestPopUpViewController.swift
//  Witz
//
//  Created by abhishek kumar on 08/02/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class AggregateRequestPopUpViewController: UIViewController {

    var groupId : String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        if groupId != nil{
//            print(groupId)
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       let controller = segue.destination as! GiveOfferAggregateViewController
        controller.groupID = groupId
    }
}
