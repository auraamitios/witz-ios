//
//  ReservationConfirmedViewController.swift
//  Witz
//
//  Created by abhishek kumar on 05/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import AVFoundation

class ReservationConfirmedViewController: UIViewController {
    @IBOutlet weak var lbl_Date: UILabel!
    
    var dateToShow : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeView))
        self.view.addGestureRecognizer(tap)
        if dateToShow != nil {
            lbl_Date.text = dateToShow
        }
//        playSound()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
//    var player: AVAudioPlayer?
    
//    func playSound() {
//
//        guard let url = Bundle.main.url(forResource: "soundMsg", withExtension: "mp3") else { return }
//
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
//            try AVAudioSession.sharedInstance().setActive(true)
//
//            player = try AVAudioPlayer(contentsOf: url)
//            guard let player = player else { return }
//
//            player.play()
//            player.numberOfLoops = 5
//
//        } catch let error {
//            print(error.localizedDescription)
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func closeView()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        closeView()
    }
}
