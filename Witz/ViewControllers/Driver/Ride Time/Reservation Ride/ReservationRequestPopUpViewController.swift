//
//  ReservationRequestPopUpViewController.swift
//  Witz
//
//  Created by abhishek kumar on 30/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import AVFoundation

class ReservationRequestPopUpViewController: UIViewController {
    
    var tripId : String?
    var dateBooking : String?
    var dateCreated : String?
    var timeLeftToAccept = 59
    fileprivate var task : DispatchWorkItem?
    
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(cancelRide), name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
        
        if dateBooking != nil {
            lbl_Date.text = dateBooking?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            checkAndScheduleTimer()
        }
        playSound()
        DriverManager.sharedInstance.ignoreRequestNow = true // New request not allowed
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.stop()
    }
    
    fileprivate func checkAndScheduleTimer(){
        let currentTime = Date()
        let date = dateBooking?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        
        if (date?.hoursSince(currentTime) ?? 44) < 24.0 {
            timeLeftToAccept =    DriverManager.sharedInstance.user_Driver.reservationGeneralSetting?.singleDayDriverAcceptanceDuration ?? 0
        }else{
            timeLeftToAccept =   DriverManager.sharedInstance.user_Driver.reservationGeneralSetting?.futureDayDriverAcceptanceDuration ?? 0
        }
        
        
        //  if driver do nothing
        task = DispatchWorkItem {
            self.cancelTask(cancelRide: true)
        }
        var value = timeLeftToAccept * 60 // now in min
        
        if dateCreated != nil {
           if let dateCreate = dateCreated?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
           { let diff = Int(currentTime.secondsSince(dateCreate))
            value = value - diff
            }
        }
        
        if value > 0{
            // Driver has time to accept the ride
            Utility.stopTimer(after: TimeInterval(value), task)
            Utility.delegate = self
            
        }else{self.dismiss(animated: true, completion: nil)}
    }
    
    //MARK:- Button Action
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        cancelTask(cancelRide: true)       
    }
    
    //MARK:- CUSTOM METHOD
    
    var player: AVAudioPlayer?
    
    func playSound() {
        
        guard let url = Bundle.main.url(forResource: "soundMsg", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
            player.numberOfLoops = 5
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    // Ride is cancelled
    func cancelRide(){
        
        if let controller = AppDelegate.sharedInstance().window?.rootViewController?.presentedViewController{
            controller.dismiss(animated: true, completion: nil)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func cancelTask(cancelRide:Bool){
        Utility.stopTimer()
        self.task?.cancel()
        if cancelRide == true{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_ReservationDetail" {
            cancelTask(cancelRide: false)
            if let controller =  segue.destination as? GiveReservaionOfferViewController{
                controller.tripID = self.tripId
                controller.timeLeft = timeLeftToAccept
            }
        }
    }
    
}

extension ReservationRequestPopUpViewController: UtilityTimerDelegate{
    
    func countDownTimer(value: Int) {
        let hour =  Int(value) / 3600
        let minutes = Int(value) / 60 % 60
        timeLeftToAccept = value
        if value == 0 {
            self.cancelTask(cancelRide: true)
        }
        lbl_Time.text = "\(hour > 9 ? "\(hour)" : "0\(hour)"):\(minutes > 9 ? "\(minutes)" : "0\(minutes)")"
    }
}
