//
//  GiveReservaionOfferViewController.swift
//  Witz
//
//  Created by abhishek kumar on 30/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class GiveReservaionOfferViewController: UIViewController {
    
    
    
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_KM: UILabel!
    @IBOutlet weak var lbl_Min: UILabel!
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    // View Bid Offer 
    @IBOutlet weak var view_BidOffer: UIView!
    @IBOutlet weak var txt_Amount: UITextField!
    
    var tripID : String?
    fileprivate var task : DispatchWorkItem?
    fileprivate var marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    var currentLocation = CLLocationCoordinate2D(latitude: 0, longitude:0)
    var driverTime = 0
    var driverKM = 0
    var timeLeft : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //  if driver do nothing
        task = DispatchWorkItem {
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        }
        // Driver has 1 min to accept the ride
        Utility.stopTimer(after: TimeInterval(timeLeft ?? 59), task)
        Utility.delegate = self
        
        // Do any additional setup after loading the view.
        txt_Amount.keyboardType = .numberPad
        showLoader()
        LocationUpdate.sharedInstance.delegate = self
        LocationUpdate.sharedInstance.startTracking()
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        LocationUpdate.sharedInstance.stopTracking()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Action
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func give_Offers(_ sender: UIButton) {
        
        view_BidOffer.isHidden = false
    }
    
    @IBAction func decline_Request(_ sender: UIButton) {
        
        showLoader()
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["tripId":tripID!]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/rejectReservationOffer", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        }, failure: { (error) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        })
    }
    
    @IBAction func submit_Bid(_ sender: UIButton) {
        if  txt_Amount.text?.isBlank() == true{
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyAmount , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            }, controller: self)
            return
        }
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let parameters =   ["tripId":tripID!,"riderId":DriverManager.sharedInstance.ride_Details?.riderData?.id ?? "","offerPrice":txt_Amount.text ?? "Na","vehicleType":DriverManager.sharedInstance.ride_Details?.tripData?.vehicleType ?? ""]
        
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/accept_booking_send_offer", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        }) { (error) in
            self.hideLoader()
        }
    }
    
    
    //MARK:- CUSTOM METHOD
    
    fileprivate func cancelTask(cancelRide:Bool){
        Utility.stopTimer()
        self.task?.cancel()
        DriverManager.sharedInstance.ignoreRequestNow = false // New request  allowed
        if cancelRide == true{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
        }
    }
    
    
    //MARK:- get Ride Detail from server
    public func getRideDetail(){
        if tripID != nil{
            
            guard self.checkNetworkStatus() else {
                return
            }
            let parameters =   ["tripId":tripID!]
            
            WitzConnectionManager().makeAPICall(functionName: "driver/get_trip_detail", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                
                
                if let  result = response?["data"]{
                    DriverManager.sharedInstance.ride_Details = TripModalDriver(json: result)
                    let tripDetails =    DriverManager.sharedInstance.ride_Details?.tripData
                    self.lbl_KM.text = "\(tripDetails?.distance?.rounded() ?? 0)KM"
                    self.lbl_Min.text = "\(tripDetails?.duration?.rounded() ?? 0)Min"
                    self.lbl_DateTime.text = tripDetails?.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                    
                    
                    // Rider Pickup Location rendenring
                    let lat =  tripDetails?.startLocation?.coordinates?[1] ?? 0
                    let lng = tripDetails?.startLocation?.coordinates?[0] ?? 0
                    let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude:CLLocationDegrees(lng))
                    
                    self.mapView.camera = GMSCameraPosition(target:coordinates , zoom: 15, bearing: 0, viewingAngle: 0)
                    let marker1 = GMSMarker(position: coordinates)
                    marker1.position = coordinates
                    marker1.title = "Pick UP"
                    marker1.appearAnimation = GMSMarkerAnimation.pop
                    marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
                    marker1.map = self.mapView
                    
                    //Rider Drop Location rendering
                    let lat1 =  tripDetails?.endLocation?.coordinates?[1] ?? 0
                    let lng1 = tripDetails?.endLocation?.coordinates?[0] ?? 0
                    let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat1), longitude:CLLocationDegrees(lng1))
                    
                    let marker = GMSMarker(position: coordinates1)
                    marker.position = coordinates1
                    marker.title = "Drop Location"
                    marker.appearAnimation = GMSMarkerAnimation.pop
                    marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
                    marker.map = self.mapView
                    
                    self.showPolyLine(source: coordinates, destination: coordinates1)
                }
                
            }, failure: { (error) in
                
                self.hideLoader()
            })
        }
    }
    //MARK:- custom method
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            //            print("wayponits****************\(distance)")
            DispatchQueue.main.async {self.showPath(polyStr: returnPath)}
            let detail = distance[0] as! [String:Any]
            let dist = detail["distance"] as? [String:Any]
            let time = detail["duration"] as? [String:Any]
            
            if let km = dist!["text"] as? String {
                self.driverKM  = Int(km.westernArabicNumeralsOnly)!
            }
            if let time = time!["text"] as? String {
                self.driverTime  =  Int(time.westernArabicNumeralsOnly)!
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        }
        hideLoader()
    }
    
}

//MARK::TimerDelegates
extension GiveReservaionOfferViewController: UtilityTimerDelegate{
    
    func countDownTimer(value: Int) {
        let hour =  Int(value) / 3600
        let minutes = Int(value) / 60 % 60
        if value == 0 {
            self.hideLoader()
            self.cancelTask(cancelRide: true)
        }
        lbl_Time.text = "\(hour > 9 ? "\(hour)" : "0\(hour)"):\(minutes > 9 ? "\(minutes)" : "0\(minutes)")"
    }
}

//MARK::LocationDelegates
extension GiveReservaionOfferViewController: LocationDelegates {
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            LocationUpdate.sharedInstance.stopTracking()
            DispatchQueue.global(qos: .background).async { self.getRideDetail()}
        }
    }
    
    func didFailToUpdate(error: Error) {
        
    }
}
