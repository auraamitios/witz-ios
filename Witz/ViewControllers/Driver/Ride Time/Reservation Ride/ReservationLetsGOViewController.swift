//
//  ReservationLetsGOViewController.swift
//  Witz
//
//  Created by abhishek kumar on 07/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps


class ReservationLetsGOViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_KM: UILabel!
    @IBOutlet weak var lbl_Amount: UILabel!
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    var tripDetail : TripsInfo?
    
//    fileprivate var marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    var currentLocation = CLLocationCoordinate2D(latitude: 0, longitude:0)
    var driverTime = 0
    var driverKM = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        showLoader()
        LocationUpdate.sharedInstance.delegate = self
        LocationUpdate.sharedInstance.startTracking()
        showTripDetails()
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(cancelRideRes), name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    // Ride is cancelled
    func cancelRideRes(){
        
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let landingVC = storyboard.instantiateViewController(withIdentifier: "RootNav")
        self.slideMenuController()?.changeMainViewController(landingVC, close: true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RideDismissReservation), object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        LocationUpdate.sharedInstance.stopTracking()
    }
    //MARK:- Button Action
    
    @IBAction func communicateWithRider(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactRiderViewController") as! ContactRiderViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.onlyContact = true
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func goToStartingPoint(_ sender: UIButton) {
        let alert  = UIAlertController.init(title: LinkStrings.AlertTitle.Title, message: LinkStrings.DriverOnTrip.WannaStart, preferredStyle: .alert)
        let ok = UIAlertAction.init(title: LinkStrings.MostCommon.Yes, style: .default) { (action) in
            
            self.performSegue(withIdentifier: "segue_ReservationStartTrip", sender: nil)
        }
        let cancel = UIAlertAction.init(title: LinkStrings.MostCommon.No, style: .cancel) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showTripDetails(){
        
        self.lbl_KM.text = "\(tripDetail?.distance?.rounded() ?? 0.0) KM/ \( tripDetail?.duration?.rounded() ?? 0)Min"
        lbl_Amount.text = "\(tripDetail?.driverEarning ?? 0.0) ₺"
        self.lbl_DateTime.text = tripDetail?.bookingDate?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.startLocation?.coordinates?[0] ?? 0))
        let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.endLocation?.coordinates?[0] ?? 0))
        
        //Marker for pick up
        let marker1 = GMSMarker(position: coordinates)
        marker1.title = "Pick UP Location"
        marker1.appearAnimation = GMSMarkerAnimation.pop
        marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize.init(width: 30.0, height: 30.0))
        marker1.map = mapView
        
        //Marker for Drop
        let marker2 = GMSMarker(position: coordinates1)
        marker2.title = "Drop Location"
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize.init(width: 30.0, height: 30.0))
        marker2.map = mapView
        
        showLoader()
        self.showPolyLine(source: coordinates, destination: coordinates1)
        
    }
    
    //MARK:- custom method
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            //            print("wayponits****************\(distance)")
            self.showPath(polyStr: returnPath)
            let detail = distance[0] as! [String:Any]
            let dist = detail["distance"] as? [String:Any]
            let time = detail["duration"] as? [String:Any]
            
            if let km = dist!["text"] as? String {
                print(km.westernArabicNumeralsOnly)
                
                
                self.driverKM  = Int(km.westernArabicNumeralsOnly)!
            }
            if let time = time!["text"] as? String {
                print(time.westernArabicNumeralsOnly)
              
                self.driverTime  =  Int(time.westernArabicNumeralsOnly)!
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = App_Base_Color
        polyline.map = self.mapView
        hideLoader()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_ReservationStartTrip"{
            if let controller = segue.destination as? DriverOnTripViewController{
                controller.tripID = self.tripDetail?.id
                DriverManager.sharedInstance.reservationTripGoingOn = true
              DriverManager.sharedInstance.tripId =   self.tripDetail?.id
            }
        }
    }
    
}

//MARK::LocationDelegates
extension ReservationLetsGOViewController: LocationDelegates {
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            /* In case if current location is not the starting location of trip **/
             let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.startLocation?.coordinates?[0] ?? 0))
               self.mapView.animate(toLocation: coordinates)
            
            currentLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            LocationUpdate.sharedInstance.stopTracking()
        }
    }
    
    func didFailToUpdate(error: Error) {
        
    }
}
