//
//  LeftViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/13/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case main = 0
    case setting
}
protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController: WitzSuperViewController {
    
    var menus = ["Main"]
    var landingViewController: WitzSuperViewController!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
//        let landingViewController = storyboard.instantiateViewController(withIdentifier: "LandingViewController") as! LandingViewController
//        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
//        let rootVC = storyBoard.instantiateViewController(withIdentifier: "MapRootNav")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .swift, .java, .go, .nonMenu:
                let cell = BaseTableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: BaseTableViewCell.identifier)
                cell.setData(menus[indexPath.row])
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
}
