//
//  PIFViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GooglePlaces
import SwiftyJSON
import Alamofire
class PIFViewController: WitzSuperViewController {
    
    @IBOutlet weak var corporateView: UIView!
    @IBOutlet weak var btnTopLayout: NSLayoutConstraint!
    
    @IBOutlet weak var txt_Name: TextFieldInsets!
    @IBOutlet weak var txt_Email: TextFieldInsets!
    @IBOutlet weak var txt_Address: TextFieldInsets!
    @IBOutlet weak var txt_CompanyName: TextFieldInsets!
    
    @IBOutlet weak var btn_Corporate: UIButton!
    let objWitzConnectionManager = WitzConnectionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txt_Address.delegate = self
        setUserDetail()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if DriverManager.sharedInstance.addressSelected != nil{
             txt_Address.text =  DriverManager.sharedInstance.addressSelected ?? ""
            DriverManager.sharedInstance.addressSelected = nil
        }
    }
    
    func setUserDetail() {
        let userDetails  = DriverManager.sharedInstance.user_Driver
        txt_Name.text = userDetails.driver_fullName
        txt_Email.text = userDetails.driver_email
        txt_Address.text = userDetails.driverAddress
        if userDetails.acc_type?.contains("corporate") == true{
            action_CorRegister(btn_Corporate)
            txt_CompanyName.text = userDetails.company_name
        }
    }
    
//    func openGooglePlacePrompt(){
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//        //autocompleteController.tableCellBackgroundColor = UIColor.cyan
//        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
//        UINavigationBar.appearance().barTintColor = UIColor(red: 79.0/255, green: 89.0/255, blue: 89.0/255, alpha: 1.0)
//        UINavigationBar.appearance().tintColor = UIColor.white
//
//        //UISearchBar.appearance().barStyle = UIBarStyle.default
//        UISearchBar.appearance().barTintColor = UIColor.white
//        //UISearchBar.appearance().setTextColor = UIColor.white
//
//        let search = UISearchBar()
//        //        search.setNewcolor(color: UIColor.white)
//        for subView: UIView in search.subviews {
//            for secondLevelSubview: UIView in subView.subviews {
//                if (secondLevelSubview is UITextField) {
//                    let searchBarTextField = secondLevelSubview as? UITextField
//                    //set font color here
//                    searchBarTextField?.textColor = UIColor.white
//                    break
//                }
//            }
//        }
//        present(autocompleteController, animated: true, completion: nil)
//    }
    
    @IBAction func action_CorRegister(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            btnTopLayout.constant = 150.0
            corporateView.isHidden = false
        }else {
            btnTopLayout.constant = 66.0
            corporateView.isHidden = true
        }
    }
    
    @IBAction func action_Save(_ sender: UIButton) {
        
        if validation(){    // mandatory fields are filled
            saveDriverDetail()  // proceed
        }
    }
    
    //    MARK:- Validation for blank fields
    func validation() -> Bool{
        if (txt_Name.text?.isBlank())!{
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyName , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            
            return false
        }
        if (txt_Email.text?.isBlank())!{
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyEmail , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            
            return false
        }
        if !(txt_Email.text?.isEmail)!{
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.InvalidEmail , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            
            return false
        }
        if (txt_Address.text?.isBlank())!{
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyAddress , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            
            return false
        }
        if btn_Corporate.isSelected{
            if (txt_CompanyName.text?.isBlank())!{
                
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyCompany , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    
                }, controller: self)
                
                return false
            }
        }
        return true
    }
    
    //MARK::API's call
    fileprivate func saveDriverDetail() {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        var parameters =   ["fullName":txt_Name.text ?? "","email":txt_Email.text ?? "" , "address":txt_Address.text ?? ""]
        if btn_Corporate.isSelected {
            parameters["acc_type"] = "corporate"
            parameters["company_name"] = txt_CompanyName.text ?? ""
            DriverManager.sharedInstance.user_Driver.company_name = txt_CompanyName.text ?? ""
            DriverManager.sharedInstance.user_Driver.acc_type = "corporate"
        }else{
            parameters["acc_type"] = "individual"
            DriverManager.sharedInstance.user_Driver.acc_type = "individual"
        }
        showLoader()
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/add_personal_information", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.Form,arr_img:nil,success: { (response) in
            self.hideLoader()
//            print(response)
            
            DriverManager.sharedInstance.user_Driver.driver_fullName = self.txt_Name.text ?? ""
            DriverManager.sharedInstance.user_Driver.driver_email = self.txt_Email.text ?? ""
            DriverManager.sharedInstance.user_Driver.driverAddress = self.txt_Address.text ?? ""
            self.performSegue(withIdentifier: "StepOne", sender: nil)  // detail saved go for next step
            
        }, failure: { (error) in
           self.hideLoader()
//            print(error)
        })
    }
    
}

extension PIFViewController:UITextFieldDelegate{
    

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      
        if textField == txt_Address{
        self.performSegue(withIdentifier: "segue_AddressSelect", sender: nil)
            return false
        }
//        openGooglePlacePrompt()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
}

//extension PIFViewController: GMSAutocompleteViewControllerDelegate {
//    
//    // Handle the user's selection.
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        print("Place name: \(place.name)")
//        print("Place Id: \(place.placeID)")
//        print("Place address: \(place.formattedAddress!)")
//        //print("Place attributions: \(place.attributions)")
//        txt_Address.text = place.formattedAddress ?? place.name
//        dismiss(animated: true, completion: nil)
//    }
//    
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        print("Error: ", error.localizedDescription)
//    }
//    
//    // User canceled the operation.
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//    
//    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//    
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//    
//}


