//
//  LeftViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/14/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import Kingfisher
enum LeftMenu: Int {
    case HOME = 0
    case TRIPS
    case WALLET
    case POINTS
    case NOTIFICATION
    case INVITE
    case LOGOUT
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}
class LeftViewController: WitzSuperViewController {
    
    @IBOutlet weak var menuTableView: UITableView!
    
    var menus = [["show":false,"name":LinkStrings.DriverSideMenuItem.HOME,"icon":#imageLiteral(resourceName: "my_home_address_white")],["show":true,"name":LinkStrings.DriverSideMenuItem.MYTRIPS,"icon":#imageLiteral(resourceName: "menu_dashboard_trips")] ,["show":false,"name":LinkStrings.DriverSideMenuItem.MYWALLET,"icon":#imageLiteral(resourceName: "menu_dashboard_my_wallet")],["show":false,"name":LinkStrings.DriverSideMenuItem.POINTS,"icon":#imageLiteral(resourceName: "menu_dashboard_my_points")],["show":true,"name":LinkStrings.DriverSideMenuItem.NOTIFICATION,"icon":#imageLiteral(resourceName: "menu_dashboard_messages")],["show":false,"name":LinkStrings.DriverSideMenuItem.INVITE,"icon":#imageLiteral(resourceName: "menu_dashboard_invite")],["show":false,"name":LinkStrings.DriverSideMenuItem.LOGOUT,"icon":#imageLiteral(resourceName: "menu_dashboard_log_out")]]
    
    var imageHeaderView: ImageHeaderView!
    let objWitzConnectionManager = WitzConnectionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.bgLogoImageView.isHidden = true
        self.menuTableView.tableFooterView = UIView(frame: .zero)
        
        self.imageHeaderView = ImageHeaderView.loadNib()
        self.imageHeaderView.frame.size.width = self.view.bounds.width
        self.view.addSubview(self.imageHeaderView)
        self.view.backgroundColor = UIColor.init(hexString: "4E5A59")
        self.imageHeaderView.backgroundColor = UIColor.init(hexString: "4E5A59")
        self.imageHeaderView.btnProfile.addTarget(self, action:  #selector(LeftViewController.profileScreen), for: .touchUpInside)
        self.setNavigationBarItem()
        
        self.objWitzConnectionManager.delegate = self
        
        self.imageHeaderView.lbl_Profile_Version.text = "PROFILE VIEW  (v.\(versionBuild))"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utility.getDriverInfoRefreshed()
        setUserDetail()
        
        
    }
    
    func setUserDetail() {
        let userDetails  = DriverManager.sharedInstance.user_Driver
        self.imageHeaderView.lbl_Name?.text = userDetails.driver_fullName ?? ""
        menuTableView.reloadData()
        let imgUrl = "\(kBaseImageURL)\(userDetails.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imageHeaderView.profileImage.image = thumbImage
                    }
                    self.imageHeaderView.profileImage.setNeedsDisplay()
                }
            })
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func profileScreen() {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "DriverProfileViewController")
        self.slideMenuController()?.changeMainViewController(profileVC, close: true)
    }
    
    func driverLogout() {
        guard self.checkNetworkStatus() else {
            return
        }
        ActivityIndicatorUtil.enableActivityIndicator(self.view, status: nil, mask: SVProgressHUDMaskType.custom, maskColor: SpinnerBGColor, style: SVProgressHUDStyle.dark)
        let token = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DriverAccessToken) as! String
        let params: Parameters = ["authorization":"bearer"+" "+token]
        print(("\(#function) -- \(self)--\(params)"))
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/logout", withInputString: params, requestType: .put, withCurrentTask: APIType.Driver_Logout, isAuthorization: true)
    }
    
    @IBAction func contact_Action(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let contactVC = storyboard.instantiateViewController(withIdentifier: "ContactUsViewController")
        self.slideMenuController()?.changeMainViewController(contactVC, close: true)
    }
    
    @IBAction func help_Action(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let helpVC = storyboard.instantiateViewController(withIdentifier: "HelpViewController")
        self.slideMenuController()?.changeMainViewController(helpVC, close: true)
        
    }
    
}

extension LeftViewController : UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.section) {
            self.changeViewController(menu)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .HOME, .TRIPS, .WALLET, .POINTS, .NOTIFICATION, .INVITE, .LOGOUT:
                return LeftMenuCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        var ht:CGFloat = 0.0
        if section == 6{
            ht = 25
        }else {
            ht = 8
        }
        return ht
    }
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        if self.tableView == scrollView {
    //
    //        }
    //    }
}


//MARK:- ****** Table view Delegates
extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: LeftMenuCell.identifier, for: indexPath) as! LeftMenuCell
        let dict = menus[indexPath.section]
        cell.updateCell(with: indexPath, dict["name"] as! String, dict["icon"] as! UIImage ,showNotification:dict["show"] as! Bool)
        return cell
    }
}


extension LeftViewController:LeftMenuProtocol {
    
    func changeViewController(_ menu: LeftMenu) {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        
        switch menu {
        case .HOME:
            let landingVC = storyboard.instantiateViewController(withIdentifier: "RootNav")
            self.slideMenuController()?.changeMainViewController(landingVC, close: true)
            
        case .TRIPS:
            let tripsVC = storyboard.instantiateViewController(withIdentifier: "MyTripsViewController")
            
            self.slideMenuController()?.changeMainViewController(tripsVC, close: true)
        case .WALLET:
            let walletVC = storyboard.instantiateViewController(withIdentifier: "WalletViewController")
            self.slideMenuController()?.changeMainViewController(walletVC, close: true)
        case .POINTS:
            let pointsVC = storyboard.instantiateViewController(withIdentifier: "PointsViewController")
            self.slideMenuController()?.changeMainViewController(pointsVC, close: true)
        case .NOTIFICATION:
            let notificationVC = storyboard.instantiateViewController(withIdentifier: "NotificationViewController")
            self.slideMenuController()?.changeMainViewController(notificationVC, close: true)
        case .INVITE:
            let inviteVC = storyboard.instantiateViewController(withIdentifier: "InviteViewController")
            self.slideMenuController()?.changeMainViewController(inviteVC, close: true)
        case .LOGOUT:
            driverLogout()
            
        }
    }
}

extension LeftViewController:WitzConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        
        switch serviceType {
        case .Driver_Logout:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                UserDefaultManager.sharedManager.removeValue(key:LinkStrings.UserDefaultKeys.DriverAccessToken)
                Utility.jumpToLoginScreen()
            }else {
                
                let msg = result["message"].stringValue
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:msg, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                }, controller: self)
            }
            break
        default:
            break
        }
        
    }
    
    func didFailWithError(sender: AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}
