//
//  WebViewViewController.swift
//  Witz
//
//  Created by abhishek kumar on 23/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit


class WebViewViewController: WitzSuperViewController {
    @IBOutlet weak var web_View: UIWebView!
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    var strURL : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        loader.startAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if strURL != nil {
            if let url = strURL?.url{
                let request = URLRequest.init(url: url)
                web_View.loadRequest(request)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension WebViewViewController : UIWebViewDelegate{
    
    func webViewDidStartLoad(_ webView: UIWebView) {
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loader.stopAnimating()
        loader.isHidden = true
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        loader.stopAnimating()
        loader.isHidden = true
    }
    
}
