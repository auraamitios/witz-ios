//
//  DriverProfileViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/27/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON
import  Kingfisher

class DriverProfileViewController: WitzSuperViewController {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var imgView_Profile: UIImageView!
   
    @IBOutlet weak var view_BankInfo: UIView!
    @IBOutlet weak var constraint_Height_Viewbank: NSLayoutConstraint!
    
     fileprivate var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBarItem()
        imgView_Profile.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(chooseImage))
        self.imgView_Profile.addGestureRecognizer(tap)
        AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: LinkStrings.DriverPopUpMessage.ProfileInactiveInfo, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         loadDriverProfile()
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK::IBOutlets actions
    
    @IBAction func action_PersonalInfo(_ sender: UIButton) {
        self.performSegue(withIdentifier: "PersonalInfo", sender: nil)
    }
    
    @IBAction func action_BankInfo(_ sender: UIButton) {
        self.performSegue(withIdentifier: "BankInfo", sender: nil)
    }
    
    @IBAction func action_VehicleInfo(_ sender: UIButton) {
        self.performSegue(withIdentifier: "VehicleInfo", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "VehicleInfo"{
         if let controller =  segue.destination as? VehicleInfoViewController{
              controller.isSaveButtonHidden = true
            }
        }
    }
   
    //MARK::Custom Method

    public func loadDriverProfile() {
        
        let userDetails  = DriverManager.sharedInstance.user_Driver
        self.lbl_Name?.text = userDetails.driver_fullName ?? ""
        let imgUrl = "\(kBaseImageURL)\(userDetails.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
            
        }
        if  DriverManager.sharedInstance.user_Driver.acc_type?.contains("corporate") == true{
            view_BankInfo.isHidden = true
            constraint_Height_Viewbank.constant = 0
        }else{
            view_BankInfo.isHidden = false
            constraint_Height_Viewbank.constant = 50
        }
    }
    
    //Image choose
    
    @objc fileprivate func chooseImage(){
        
        let actionSheet = UIAlertController.init(title: "Choose Image", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (action) in
            
            if (!UIImagePickerController.isSourceTypeAvailable(.camera))
            {self.dismiss(animated: true, completion: nil)
            }
            else{
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let gallery  = UIAlertAction.init(title: "Gallery", style: .default) { (action) in
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel  = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
}

extension DriverProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]) {
        
        
        if let possibleImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imgView_Profile.image = possibleImage
            uploadImage(imgToUpload: possibleImage)
        } else if let possibleImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgView_Profile.image = possibleImage
            uploadImage(imgToUpload: possibleImage)
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
   fileprivate func uploadImage(imgToUpload:UIImage){
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
    
    var parameters =   ["fullName":DriverManager.sharedInstance.user_Driver.driver_fullName ?? "","email":DriverManager.sharedInstance.user_Driver.driver_email ?? "", "address":DriverManager.sharedInstance.user_Driver.driverAddress ?? "","pic":imgToUpload] as [String:Any]
    
    parameters["acc_type"] = DriverManager.sharedInstance.user_Driver.acc_type ?? "individual"
    
    if  DriverManager.sharedInstance.user_Driver.acc_type?.contains("corporate") == true{
        parameters["company_name"] = DriverManager.sharedInstance.user_Driver.company_name ?? ""
    }
    let  arrImg = [imgToUpload]
    showLoader()
    WitzConnectionManager().makeAPICall(functionName: "driver/add_personal_information", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.Form,arr_img:arrImg,success: { (response) in
        self.hideLoader()
        AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "", style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            
                                }, controller: self)
        
         if response?["statusCode"] == 200{
            let driver = DriverInfo.parseDriverInfoJson(data1: response)
            DriverManager.sharedInstance.user_Driver = driver
        }
        
    }, failure: { (error) in
        self.hideLoader()
        print(error.debugDescription)
    })
    }    
}
