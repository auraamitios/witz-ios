//
//  ContactUsViewController.swift
//  Witz
//
//  Created by abhishek kumar on 23/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ContactUsViewController: WitzSuperViewController {
    
    @IBOutlet weak var constraint_Bottom_submit: NSLayoutConstraint!
    @IBOutlet weak var txt_Subject: UITextField!
    @IBOutlet weak var txtView_Message: KMPlaceholderTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setNavigationBarItem()
        txtView_Message.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        IQKeyboardManager.sharedManager().enable = false
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.Contact
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    
    @IBAction func suubmit_Request(_ sender: UIButton) {
        
        guard validation() else {return}
        if AppManager.sharedInstance.loggedUserRider {
            showLoader()
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/send_email", withInputString: ["subject":txt_Subject.text ?? "","message":txtView_Message.text], requestType: .post, isAuthorization: true, success: { (response) in
                self.hideLoader()
                
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    if response?["statusCode"] == 200{
                        self.txt_Subject.text = ""
                        self.txtView_Message.text = ""
                    }
                }, controller: self)
                
            }, failure: { (error) in
                self.hideLoader()
            })
            
        }else {
           
                showLoader()
                WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/send_email", withInputString: ["subject":txt_Subject.text ?? "","message":txtView_Message.text], requestType: .post, isAuthorization: true, success: { (response) in
                    self.hideLoader()
                    
                    AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                        if response?["statusCode"] == 200{
                            self.txt_Subject.text = ""
                            self.txtView_Message.text = ""
                        }
                    }, controller: self)
                    
                }, failure: { (error) in
                    self.hideLoader()
                })
                
            
        }
    }
    
    func validation() -> Bool {
        
        if  txt_Subject.text?.isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyTopic , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        if  txtView_Message.text?.isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyMessage , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension ContactUsViewController : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        constraint_Bottom_submit.constant = 300
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         constraint_Bottom_submit.constant = 10
    }
}

extension ContactUsViewController : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
       constraint_Bottom_submit.constant = 300
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
          constraint_Bottom_submit.constant = 10
    }
}
