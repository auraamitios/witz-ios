//
//  InviteViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/14/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON
import MessageUI
import SVProgressHUD
import KeychainSwift

class InviteViewController: WitzSuperViewController {
    
    @IBOutlet weak var btn_ReferralCode: UIButton!
    
//    let objWitzConnectionManager = WitzConnectionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.btn_ReferralCode.setTitle(DriverManager.sharedInstance.user_Driver.refferalCode, for: .normal)
//        self.objWitzConnectionManager.delegate = self
        self.navigationItem.titleView = nil
        self.navigationItem.title = "INVITE"
        self.setNavigationBarItem()
        self.navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 0.2471, green: 0.2902, blue: 0.2941, alpha: 1.0)
//        getDriverInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 0.2471, green: 0.2902, blue: 0.2941, alpha: 1.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 0.2471, green: 0.2902, blue: 0.2941, alpha: 1.0)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK::API's call
//    fileprivate func getDriverInfo() {
//        view.endEditing(true)
//        guard self.checkNetworkStatus() else {
//            return
//        }
//        self.objWitzConnectionManager.makeAPICall(functionName: "driver/accesstokenlogin", withInputString: ["":""], requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true)
//    }
    
    @IBAction func copyCode(_ sender: UIButton) {
    UIPasteboard.general.string = sender.title(for: .normal)
        
        print(UIPasteboard.general.string ?? "Content not copied")
       
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Copied.ContentCopied, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            })
    }
    
    @IBAction func invite_Email(_ sender: UIButton) {
        let mailURL = URL(string: "mailto:")!
        if UIApplication.shared.canOpenURL(mailURL) {
            if MFMailComposeViewController.canSendMail(){
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients([""])
            mailComposerVC.setSubject("Referal Code")
            mailComposerVC.setMessageBody("Haven't tried Witz yet? Sign up with my code \(btn_ReferralCode.title(for: .normal)!) and enjoy the most affordable cabs rides!", isHTML: false)
                self.present(mailComposerVC, animated: true, completion: nil)
            }
        else {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderSideMenuItem.MailAlert, style: .alert, actionButtonTitle: "Ok", completionHandler: { (action) in
            }, controller: self)
        }
        }
    }
    
    @IBAction func invite_SMS(_ sender: UIButton) {
        if (MFMessageComposeViewController.canSendText()) {
            UIBarButtonItem.appearance().tintColor = UIColor.gray
            let controller = MFMessageComposeViewController()
            controller.body = "Haven't tried Witz yet? Sign up with my code \(btn_ReferralCode.title(for: .normal)!) and enjoy the most affordable cabs rides!"
//            controller.recipients = [""]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        } else {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderSideMenuItem.MsgAlert, style: .alert, actionButtonTitle: "Ok", completionHandler: { (action) in
            }, controller: self)
        }
    }
    
    @IBAction func shareInSocialMedia(_ sender: UIButton) {
       
        let myWebsite = NSURL(string:"http://www.google.com/")
        let str = "Haven't tried Witz yet? Sign up with my code \(btn_ReferralCode.title(for: .normal)!) and enjoy the most affordable cabs rides!"
        let img: UIImage = #imageLiteral(resourceName: "witz_logo_bg")
        
        guard let url = myWebsite else {
            print("nothing found")
            return
        }
        let shareItems:Array = [img, url,str] as [Any]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension InviteViewController : MFMessageComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
       UIBarButtonItem.appearance().tintColor = UIColor.white
        self.dismiss(animated: true, completion: nil)
    }
}


extension InviteViewController : MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
    self.dismiss(animated: true, completion: nil)
    }
}

//extension InviteViewController:WitzConnectionManagerDelegate {
//
//    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
//        Utility.hideNetworkIndicator()
//        print("data\(sender)")
//        switch serviceType {
//        case .Driver_Info:
//            let result = sender as! JSON
//            if result["statusCode"] == 200 {
//
//                if let driver = responseData as? DriverInfo {
//                    self.btn_ReferralCode.setTitle(driver.refferalCode, for: .normal)
//                }
//            }else {
//                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
//
//                }, controller: self)
//            }
//            break
//
//        default:
//            break
//        }
//    }
//
//    func didFailWithError(sender: AnyObject) {
//        Utility.hideNetworkIndicator()
//
//        if sender is ServerError {
//            let error = sender as! ServerError
//         AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
//            }, controller: self)
//
//        }else if sender is ServerResponse {
//            let error = sender as! ServerResponse
//               AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
//                }, controller: self)
//        }else {
//            let errorMsg = sender as! String
//
//                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
//                }, controller: self)
//        }
//    }
//}

