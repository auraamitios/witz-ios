//
//  MyTripsViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/14/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class MyTripsViewController: WitzSuperViewController ,UIGestureRecognizerDelegate{
    
    fileprivate var sectionToLoad : Int?  //  for which header tapped
    fileprivate let arr_SectionTitle = [["header":LinkStrings.TripsType.PastJourney,"row":[["name":LinkStrings.TripsType.Instant,"icon": #imageLiteral(resourceName: "trip_where_yellow")],["name":LinkStrings.TripsType.Reservation,"icon":#imageLiteral(resourceName: "reservation_to_where_yellow")],["name":LinkStrings.TripsType.Aggregate,"icon":#imageLiteral(resourceName: "menu_page_aggregate")]]],
                                        ["header":LinkStrings.TripsType.FutureJourney,"row":[["name":LinkStrings.TripsType.Reservation,"icon":#imageLiteral(resourceName: "reservation_to_where_yellow")],["name":LinkStrings.TripsType.Aggregate,"icon":#imageLiteral(resourceName: "menu_page_aggregate")]]],
                                        ["header":LinkStrings.TripsType.PendingJourney,"row":[["name":LinkStrings.TripsType.Reservation,"icon":#imageLiteral(resourceName: "reservation_to_where_yellow")],["name":LinkStrings.TripsType.Aggregate,"icon":#imageLiteral(resourceName: "menu_page_aggregate")]]]]
    
    
    @IBOutlet weak var table_View: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setNavigationBarItem()
        self.navigationItem.titleView = nil
        self.navigationItem.title = "Trip Details"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        if sectionToLoad != nil{            // collpase the view
            if sectionToLoad == sender?.view?.tag{
                sectionToLoad = nil
                table_View.reloadData()
                
                return   //keep other header unchanged
            }
        }
        sectionToLoad = sender?.view?.tag
        if sectionToLoad != nil{            // expand the view
            table_View.reloadData()
  
        }
        
    }
    
}

//MARK:- Table View Delegates
extension MyTripsViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arr_SectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sectionToLoad != nil {
            if sectionToLoad == section{    // expand the view to show rows
                return  section == 0 ? 3 : 2
            }else{      // keep other section collpased
                return 0
            }
        }else{
            return 0  // default
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellMyTrips", for: indexPath) as! CellMyTrips
        
        if let arr_data = arr_SectionTitle[indexPath.section]["row"] as? [[String:Any]] {
            cell.lbl_Title.text = arr_data[indexPath.row]["name"] as? String ?? ""
            cell.imgView_Icon?.image = arr_data[indexPath.row]["icon"] as? UIImage ?? #imageLiteral(resourceName: "trip_where_yellow")
         
            switch(indexPath.section){
            case 1:
                if indexPath.row == 0 {
                    let count = DriverManager.sharedInstance.user_Driver.reservationFutureTripCount ?? 0
                    cell.lbl_Count.isHidden = true
                    if count > 0 {
                        cell.lbl_Count.isHidden = false
                        cell.lbl_Count.text = "\(count)"
                    }
                }else{
                    
                    //FIXME:: update this later
                    cell.lbl_Count.isHidden = true
                    let count = DriverManager.sharedInstance.user_Driver.serviceRequestFutureTripCount ?? 0
                    if count > 0 {
                        cell.lbl_Count.isHidden = false
                        cell.lbl_Count.text = "\(count)"
                    }
                }
                break
            case 2:
                if indexPath.row == 0 {
                    let count = DriverManager.sharedInstance.user_Driver.reservationPendingTripCount ?? 0
                    cell.lbl_Count.isHidden = true
                    if count > 0 {
                        cell.lbl_Count.isHidden = false
                        cell.lbl_Count.text = "\(count)"
                    }
                }else{
                    //FIXME:: update this later
                    cell.lbl_Count.isHidden = true
                    let count = DriverManager.sharedInstance.user_Driver.serviceRequestPendingTripCount ?? 0
                    if count > 0 {
                        cell.lbl_Count.isHidden = false
                        cell.lbl_Count.text = "\(count)"
                    }
                }
                break
            default:
                cell.lbl_Count.isHidden = true
            }
        
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headercell = tableView.dequeueReusableCell(withIdentifier: "Cell_HeaderMyTrips") as! CellMyTrips
        headercell.tag = section
        headercell.lbl_HeaderTitle.text = arr_SectionTitle[section]["header"] as? String ?? ""
        
        //*_*_**_ tap for expandable cell
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(sender:)))
        tap.delegate = self
        headercell.addGestureRecognizer(tap)
        
        
//        if sectionToLoad != nil {
//            if sectionToLoad == section{    // expanded section
                headercell.view_Header.backgroundColor = App_Base_Color
                headercell.lbl_HeaderTitle.textColor = .white
                
//            }else{      // collapsed section
//                headercell.view_Header.backgroundColor = .white
//                headercell.lbl_HeaderTitle.textColor = .darkGray
//            }
//        }
        switch(section){
            
        case 1:
            
            let count = (DriverManager.sharedInstance.user_Driver.reservationFutureTripCount ?? 0)+(DriverManager.sharedInstance.user_Driver.serviceRequestFutureTripCount ?? 0)
            headercell.lbl_HeaderCount.isHidden = true
            if count > 0 {
                headercell.lbl_HeaderCount.isHidden = false
                headercell.lbl_HeaderCount.text = "\(count)"
            }
            
            break
        case 2:
            
            let count = (DriverManager.sharedInstance.user_Driver.reservationPendingTripCount ?? 0)+(DriverManager.sharedInstance.user_Driver.serviceRequestPendingTripCount ?? 0)
            headercell.lbl_HeaderCount.isHidden = true
            if count > 0 {
                headercell.lbl_HeaderCount.isHidden = false
                headercell.lbl_HeaderCount.text = "\(count)"
                
            }
            
            break
        default:
            headercell.lbl_HeaderCount.isHidden = true
            break
        }
        return headercell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
        if let arr_data = arr_SectionTitle[indexPath.section]["row"] as? [[String:Any]] {
            
            if let jrnyType = arr_SectionTitle[indexPath.section]["header"] as? String{
                
                let rowName = arr_data[indexPath.row]["name"] as? String ?? ""
                performSegue(withIdentifier: "segue_ReservationTrips", sender: [jrnyType,rowName])
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_ReservationTrips"{
            if let controller = segue.destination as?  ReservationTripsViewController {
                controller.tripType = sender as? [String]
            }
        }
    }
}

