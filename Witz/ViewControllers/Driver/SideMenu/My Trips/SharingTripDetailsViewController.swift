//
//  SharingTripDetailsViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/12/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class SharingTripDetailsViewController: WitzSuperViewController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var lbl_startAddress: UILabel!
    @IBOutlet weak var lbl_EndAddress: UILabel!
    @IBOutlet weak var lbl_Amount: UILabel!
    @IBOutlet weak var lbl_numberOfTask: UILabel!
    @IBOutlet weak var lbl_numberOfRider: UILabel!
    @IBOutlet weak var lbl_kmTotal: UILabel!
    @IBOutlet weak var lbl_TimeTotal: UILabel!
    
    //properties for data passing
    
    var shiftFor : ShiftTime?
    var tripForAggregate : String?
    var letsGoNeeded : Bool?
    var tripDetail : TripsInfo?
    
    //local variable
    fileprivate  var arr_Trip = [SharingInfoData]()
    fileprivate var arr_Aggregate = [TripsAggregate]()
    fileprivate var strDurationDistance = ""
    fileprivate var tripDetail_Aggregate : AgrregateTripModal?
    
    fileprivate var currentLocation = CLLocationCoordinate2D(latitude: AppManager.sharedInstance.locationCoordinates.last?.coordinate.latitude ?? APPLEOFFICELOCATION_LAT, longitude:AppManager.sharedInstance.locationCoordinates.last?.coordinate.longitude  ?? APPLEOFFICELOCATION_LNG)
    
    @IBOutlet weak var btn_LetsGo: UIButton!
    @IBOutlet weak var constarint_Width_btnLetsGo: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_Trailing_btnLetsGo: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = false
        mapView.shouldHideToolbarPlaceholder = true
        mapView.delegate = self
        
        hideLetsGo()
        if letsGoNeeded == true{
            showLetsGo()
        }
        if tripDetail != nil{
            showTripDetails()
        }else if tripForAggregate != nil {
            self.getTripDetailAggregate()
        }
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    
    //MARK:- custom method
    /**
     This func will show detail of trip from modal
     */
    func showTripDetails(){
        
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.startLocation?.coordinates?[1] ?? Float(APPLEOFFICELOCATION_LAT)), longitude:CLLocationDegrees(tripDetail?.startLocation?.coordinates?[0] ?? Float(APPLEOFFICELOCATION_LNG)))
        currentLocation = coordinates  // this is driver start location
        let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.endLocation?.coordinates?[0] ?? 0))
        
        
        if tripDetail?.status == 21 || tripDetail?.status == 20{
            
        }else{
            //Marker for pick up
            putMarker(icon: #imageLiteral(resourceName: "on_map_starting_location"), coordinate: coordinates)
            //Marker for Drop
            putMarker(icon: #imageLiteral(resourceName: "on_map_arrival_location"), coordinate: coordinates1)
            self.showPolyLine(source: coordinates, destination: coordinates1, isDashedLine: false)
        }
        // now get the remaining data from api
        guard self.checkNetworkStatus() else {
            return
        }
        print(tripDetail?.id ?? "j")
        if let tripID = tripDetail?.id{
            let parameters =   ["tripId":tripID]
            showLoader()
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/get_shared_trip_details", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                self.hideLoader()
                if let  result = response?["data"]{
                    let modal = SharedRideDetailModal(json: result)
                    
                    //Show data on screen
                    self.lbl_numberOfRider.text =  "\(self.lbl_numberOfRider.text ?? ""): \(modal.logsData?.numberOfPersonRiding ?? 1)"
                    self.lbl_numberOfTask.text = "\(self.lbl_numberOfTask.text ?? ""): \(modal.logsData?.numberOfPersonRiding ?? 1)"
                    self.lbl_kmTotal.text = "Km/Total: \(String(format: "%.2f",modal.totalDistance ?? 0)) KM"
                    self.lbl_TimeTotal.text = "Time/Total: \( Int(modal.totalDuration ?? 0)) Min"
                    self.lbl_Amount.text = "\(String(format: "%.2f",self.tripDetail?.driverEarning ?? 0.0)) ₺"
                    if modal.logsData?.sharingInfoData?.count ?? 0 > 0 {
                        self.lbl_startAddress.text = modal.logsData?.sharingInfoData?[0].startAddress ?? ""
                        self.lbl_EndAddress.text = modal.logsData?.sharingInfoData?[0].endAddress ?? ""
                    }
                    self.lbl_DateTime.text = modal.logsData?.updatedAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                    self.strDurationDistance = "\(String(format: "%.2f",modal.totalDistance ?? 0)) KM / \( Int(modal.totalDuration ?? 0)) Min"
                    
                    self.arr_Trip = modal.logsData?.sharingInfoData ?? []
                    self.showTripsCalculation() // calculate to order the pick and drop
                }
            }, failure: { (error) in
                self.hideLoader()
            })
        }
    }
    
    func hideLetsGo()  {
        btn_LetsGo.isHidden = true
        constarint_Width_btnLetsGo.constant = -20
        constraint_Trailing_btnLetsGo.constant = 10
    }
    
    func showLetsGo()  {
        
        btn_LetsGo.isHidden = false
        constarint_Width_btnLetsGo.constant = (self.view.frame.size.width/2) - 20
        constraint_Trailing_btnLetsGo.constant = 20
    }
           /**
     Put marker on map
     */
    fileprivate func putMarker(icon : UIImage , coordinate: CLLocationCoordinate2D){
        let marker = GMSMarker(position: coordinate)
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon = icon
        marker.map = mapView
        
    }
    
    
    /**
     use to show detail for aggreagate trip
     */
    //MARK:- get Ride Detail from server
    public func getTripDetailAggregate(){
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =  ["aggregateGroupId":tripForAggregate!]
        showLoader()
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "aggregate/get_final_aggregate_trip", withInputString: parameters, requestType: .get, isAuthorization: false, success: { (response) in
            self.hideLoader()
            if response?["statusCode"] == 200{
                self.hideLoader()
                if let  result = response?["data"]{
                    print(result)
                    let tripDetails = AgrregateTripModal(json: result)
                    self.tripDetail_Aggregate = tripDetails  // save detail for passing
                    if (tripDetails.wayPoints?.count ?? 0) > 0 {self.drawPathUsing(wayPoints: tripDetails.wayPoints!)}
                    if (tripDetails.trips?.count ?? 0)  > 0 {
                        self.arr_Aggregate = tripDetails.trips ?? []
                        self.showTripsCalculation()
                    }
                    self.lbl_numberOfRider.text =  "\(self.lbl_numberOfRider.text ?? ""): \(tripDetails.numberOFActiveRider ?? 1)"
                    self.lbl_numberOfTask.text = "\(self.lbl_numberOfTask.text ?? ""): \(tripDetails.numberOFActiveRider ?? 1)"
                    self.lbl_Amount.text = "\(String(format: "%.2f",tripDetails.driverEarning ?? 0.0)) ₺"
                    
                    self.lbl_DateTime.text = "\(tripDetails.workingTime?.time?.arivalTime ?? "9:00")-\(tripDetails.workingTime?.time?.departureTime ?? "18:00")" + "(" + Utility.aggregateDaysDriver(data: tripDetails) + ")"
                    self.lbl_startAddress.text = tripDetails.aggregateStartLocation ?? ""
                    self.lbl_EndAddress.text = tripDetails.aggregateEndLocation ?? ""
                    
                    if self.shiftFor != nil {
                        if self.shiftFor == ShiftTime.Evening{
                            self.lbl_startAddress.text = tripDetails.aggregateEndLocation ?? ""
                            self.lbl_EndAddress.text = tripDetails.aggregateStartLocation ?? ""
                        }
                    }
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
    
    
    fileprivate func drawPathUsing(wayPoints:[WayPointsAggregate]){
        
        mapView.clear()
        let start = wayPoints[0]
        let source =  CLLocation(latitude: CLLocationDegrees(start.coordinates?[1] ?? 0), longitude:CLLocationDegrees(start.coordinates?[0] ?? 0))
        putMarker(icon: #imageLiteral(resourceName: "flag"), coordinate: source.coordinate) // pickup Station
        let end = wayPoints[wayPoints.count-1]
        let destination =  CLLocation(latitude: CLLocationDegrees(end.coordinates?[1] ?? 0), longitude:CLLocationDegrees(end.coordinates?[0] ?? 0))
        putMarker(icon: #imageLiteral(resourceName: "flag"), coordinate: destination.coordinate)// drop Station
        
        var arr = [[String : Any]]()  // it will keep all coodinate and distance from source
        
        if wayPoints.count < 3{ // no halt between the path
            showPolyLine(source: source.coordinate, destination: destination.coordinate, isDashedLine: false)
                self.setRiderPoints(start: source.coordinate, end: destination.coordinate)
        }
        
        // preparing arr of dict whose conatains coordinate and distance
        for i in 1..<wayPoints.count {
            
            let modal = wayPoints[i]
            let halt =  CLLocation(latitude: CLLocationDegrees(modal.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.coordinates?[0] ?? 0))
            let distPick = source.distance(from: halt)
            putMarker(icon: #imageLiteral(resourceName: "flag"), coordinate: halt.coordinate)// stop Station
            
            let dict = [KeysForSharingPickUpViewController.COORDINATE:halt.coordinate,KeysForSharingPickUpViewController.DIST:distPick] as [String : Any]
            arr.append(dict)
        }
        
        let key = KeysForSharingPickUpViewController.DIST // The key you want to sort by
        
        let finalarray = arr.sorted {  // this one keep the track of sorted coordinates for plotting
            switch ($0[key], $1[key]) {
            case let (lhs as Double, rhs as Double):
                return  lhs < rhs
            default:
                return true
            }
        }
        
        var sourceStation = source.coordinate // get the source coordinate
        for i in 1..<finalarray.count {
            let dict = finalarray[i]
            let station = dict[KeysForSharingPickUpViewController.COORDINATE] as! CLLocationCoordinate2D
            showPolyLine(source: sourceStation , destination: station, isDashedLine: false)
            sourceStation = station // change source station now
        }
    
        self.setRiderPoints(start: sourceStation, end: source.coordinate)
    }
    
    
    fileprivate func setRiderPoints(start:CLLocationCoordinate2D,end:CLLocationCoordinate2D){
            putMarker(icon: #imageLiteral(resourceName: "riderPick "), coordinate: start) // pickup Rider
            putMarker(icon: #imageLiteral(resourceName: "riderDrop"), coordinate: end) //  drop Rider
    }
    //MARK:- custom method to show on view all task
    
    fileprivate func showTripsCalculation(){
        
        var arr_Sorted = [[String : Any]]()   // contain the trips modified dictonary for calculation
        var i = 0  // keep the sequence of customers
        let source =  CLLocation(latitude: CLLocationDegrees(currentLocation.latitude), longitude:CLLocationDegrees(currentLocation.longitude))
        let key = KeysForSharingPickUpViewController.DIST // The key you want to sort by
        
        //*******************************get the data in a formatted way to sort****************************/
        
        if self.arr_Trip.count  > 0{  // if sharing trip going on
            for modal in self.arr_Trip{
                
                let coordinatesStart = CLLocation(latitude: CLLocationDegrees(modal.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.startLocation?.coordinates?[0] ?? 0))
                let coordinatesEnd = CLLocation(latitude: CLLocationDegrees(modal.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.endLocation?.coordinates?[0] ?? 0))
                let distPick = source.distance(from: coordinatesStart)  //pickup dist
                let distDrop = source.distance(from: coordinatesEnd)
                
                i = i+1
                
                
                //pickup Dict
                let dictAllPickup = [KeysForSharingPickUpViewController.TYPE:KeysForSharingPickUpViewController.PICK,
                                     KeysForSharingPickUpViewController.COORDINATE:coordinatesStart,
                                     KeysForSharingPickUpViewController.DIST:distPick,
                                     KeysForSharingPickUpViewController.PICKUPNUMBER:i,
                                     KeysForSharingPickUpViewController.TRIPID:modal.tripId ?? "",
                                     KeysForSharingPickUpViewController.PickUPStatus.STATUS: KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP,KeysForSharingPickUpViewController.NAME:modal.riderName ?? "Rider Name" , KeysForSharingPickUpViewController.ADDRESS : modal.startAddress ?? "" ]
                    as [String : Any]
                
                //Drop Dict
                let dictAllDrop = [KeysForSharingPickUpViewController.TYPE:KeysForSharingPickUpViewController.DROP,
                                   KeysForSharingPickUpViewController.COORDINATE:coordinatesEnd,
                                   KeysForSharingPickUpViewController.DIST:distDrop,
                                   KeysForSharingPickUpViewController.PICKUPNUMBER:i,
                                   KeysForSharingPickUpViewController.TRIPID:modal.tripId ?? "",
                                   KeysForSharingPickUpViewController.PickUPStatus.STATUS: KeysForSharingPickUpViewController.PickUPStatus.WAITDROP,KeysForSharingPickUpViewController.NAME:modal.riderName ?? "Rider Name" , KeysForSharingPickUpViewController.ADDRESS : modal.endAddress ?? "" ] as [String : Any]
                
                arr_Sorted.append(dictAllPickup)
                arr_Sorted.append(dictAllDrop)
                
            }
        }
        else if self.arr_Aggregate.count > 0 {  // this calulation is for aggregate trip
//            var driverTime :Float = 0.0
//            var driverKM :Float = 0.0
            
            var arr_Sorted = [[String : Any]]()   // contain the trips modified dictonary for calculation
            var i = 0  // keep the sequence of customers
            let source =  CLLocation(latitude: CLLocationDegrees(currentLocation.latitude), longitude:CLLocationDegrees(currentLocation.longitude))
            
            // this calulation  for aggregate trip
            
            for modal in self.arr_Aggregate{
                let coordinatesStart = CLLocation(latitude: CLLocationDegrees(modal.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.startLocation?.coordinates?[0] ?? 0))
                let coordinatesEnd = CLLocation(latitude: CLLocationDegrees(modal.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.endLocation?.coordinates?[0] ?? 0))
                let distPick = source.distance(from: coordinatesStart)  //pickup dist
                i = i+1
//                driverKM = driverKM + (modal.distance ?? 0.0)
//                driverTime = driverTime + (modal.duration ?? 0.0)
                
                self.lbl_kmTotal.text = "Km/Total: \(String(format: "%.2f",modal.distance ?? 0.0)) KM"
                self.lbl_TimeTotal.text = "Time/Total: \(String(format: "%.2f",modal.duration ?? 0.0)) Min"
                self.strDurationDistance = "\(String(format: "%.2f",modal.distance ?? 0.0)) KM /\(String(format: "%.2f",modal.duration ?? 0.0)) Min"
                //pickup Dict
                let dictAllPickup = [KeysForSharingPickUpViewController.TYPE:KeysForSharingPickUpViewController.PICK,
                                     KeysForSharingPickUpViewController.COORDINATE:coordinatesStart,
                                     KeysForSharingPickUpViewController.ENDCOORDINATE:coordinatesEnd,
                                     KeysForSharingPickUpViewController.DIST:distPick,
                                     KeysForSharingPickUpViewController.PICKUPNUMBER:i,
                                     KeysForSharingPickUpViewController.TRIPID:modal.id ?? "",
                                     KeysForSharingPickUpViewController.RIDEDETAILS: modal,                                 KeysForSharingPickUpViewController.PickUPStatus.STATUS: KeysForSharingPickUpViewController.PickUPStatus.WAITPICKUP,KeysForSharingPickUpViewController.NAME:modal.riderName ?? "Rider Name" , KeysForSharingPickUpViewController.ADDRESS : modal.startAddress ?? "", KeysForSharingPickUpViewController.ENDADDRESS : modal.endAddress ?? "" ]
                    as [String : Any]
                
                arr_Sorted.append(dictAllPickup)
            }
            
            DriverManager.sharedInstance.arr_SortedShareTrips = arr_Sorted // this one keep the track of  trips in global
            return
        }
        
        DriverManager.sharedInstance.arr_SortedShareTrips  = arr_Sorted.sorted {  // this one keep the track of sorted trips in global
            switch ($0[key], $1[key]) {
            case let (lhs as Double, rhs as Double):
                return  lhs < rhs
            default:
                return true
            }
        }
    }
    
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D,isDashedLine:Bool){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
        hideLoader()
    }
    lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.DriverAggregate, bundle: nil)
    @IBAction func viewAllTasks(_ sender: UIButton) {
        
        if tripForAggregate != nil {
            
            let controller = self.storyBoard.instantiateViewController(withIdentifier: "ViewAllPassengerViewController") as! ViewAllPassengerViewController
            controller.fromHistory = true
            controller.shiftFor = self.shiftFor
            self.navigationController?.pushViewController(controller, animated: true)
            
            return
        }
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CompleteTripNowViewController") as! CompleteTripNowViewController
        controller.fromHistory = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func letsGo(_ sender: UIButton) {
        
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(finishRide), name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
        DriverManager.sharedInstance.arr_ShareTripsAggregate = self.arr_Aggregate
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AggregateStartConfirmationViewController") as! AggregateStartConfirmationViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.groupID = tripForAggregate
        controller.shiftFor = self.shiftFor
        controller.navController = self.navigationController
        self.present(controller, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Ride is cancelled
    func finishRide(){
        DriverManager.sharedInstance.arr_SortedShareTrips?.removeAll()
        DriverManager.sharedInstance.arr_ShareTripsAggregate?.removeAll()
        DriverManager.sharedInstance.ignoreRequestNow = false  // New request  allowed
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RideDismissShare), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: RemoveOneRide), object: nil)
        Utility.driverSideMenuSetUp()
    }
}

extension SharingTripDetailsViewController : GMSMapViewDelegate{
    // MARK: GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewStaticMapViewController") as! ViewStaticMapViewController
        controller.strTimeKm = strDurationDistance
        if self.tripDetail_Aggregate != nil{
            controller.tripDetails_Aggregate = self.tripDetail_Aggregate
        }else{
            controller.tripDetails = tripDetail
            controller.isShared = true
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
