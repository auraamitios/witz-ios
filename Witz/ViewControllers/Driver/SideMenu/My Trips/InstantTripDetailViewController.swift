//
//  InstantTripDetailViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/12/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher
import Cosmos

class InstantTripDetailViewController: WitzSuperViewController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var lbl_startAddress: UILabel!
    @IBOutlet weak var lbl_EndAddress: UILabel!
    @IBOutlet weak var lbl_Amount: UILabel!
    @IBOutlet weak var lbl_RiderName: UILabel!
    @IBOutlet weak var lbl_Km: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var imgView_Profile: UIImageView!
    
    @IBOutlet weak var view_StrarRating: CosmosView!
    
    var tripDetail : TripsInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = false
        mapView.shouldHideToolbarPlaceholder = true
        mapView.delegate = self
        showTripDetails()
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    func showTripDetails(){
        
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.startLocation?.coordinates?[0] ?? 0))
//        currentLocation = coordinates  // this is driver start location
        let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.endLocation?.coordinates?[0] ?? 0))
        
        
        lbl_DateTime.text = tripDetail?.createdAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        
       lbl_Km.text = "\(String(format: "%.2f",tripDetail?.distance ?? 0)) KM"
        lbl_Time.text = "\(Int(tripDetail?.duration ?? 0)) Min"
        lbl_Amount.text = "\(tripDetail?.driverEarning ?? 0.0) ₺"
        lbl_startAddress.text = tripDetail?.startAddress ?? ""
        lbl_EndAddress.text = tripDetail?.endAddress ?? ""

        //Marker for pick up
        let marker1 = GMSMarker(position: coordinates)
        marker1.title = "Pick UP Location"
        marker1.appearAnimation = GMSMarkerAnimation.pop
        marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize.init(width: 30.0, height: 30.0))
        marker1.map = mapView
        
        //Marker for Drop
        let marker2 = GMSMarker(position: coordinates1)
        marker2.title = "Drop Location"
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize.init(width: 30.0, height: 30.0))
        marker2.map = mapView
        showLoader()
        self.showPolyLine(source: coordinates, destination: coordinates1)
        getRideDetail()
    }
    
    
    //MARK:- get Ride Detail from server
    public func getRideDetail(){
        
        guard self.checkNetworkStatus() else {
            return
        }
        print(tripDetail?.id ?? "j")
        if let tripID = tripDetail?.id{
            let parameters =   ["tripId":tripID]
            
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/get_trip_detail", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
                self.hideLoader()
                if let  result = response?["data"]{
                    DriverManager.sharedInstance.ride_Details = TripModalDriver(json: result)
                    let riderDetails =    DriverManager.sharedInstance.ride_Details?.riderData
                    self.lbl_RiderName.text =  riderDetails?.fullName?.nameFormatted ?? ""
                    self.view_StrarRating.rating =  riderDetails?.avgRating ?? 0.0
                    let imgUrl = "\(kBaseImageURL)\(riderDetails?.pic?.original ?? "")"
                    if let url = imgUrl.url{
                        KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                            DispatchQueue.main.async {
                                
                                if let thumbImage = image {
                                    self.imgView_Profile.image = thumbImage
                                }
                                self.imgView_Profile.setNeedsDisplay()
                            }
                        })
                    }
                }
            }, failure: { (error) in
                self.hideLoader()
            })
        }
    }
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
         hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
         self.hideLoader()
    }
    
}

extension InstantTripDetailViewController : GMSMapViewDelegate{
    // MARK: GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewStaticMapViewController") as! ViewStaticMapViewController
        controller.strTimeKm = (self.lbl_Km.text ?? "") + "/ " + (self.lbl_Time.text ?? "")
        controller.tripDetails = tripDetail
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
