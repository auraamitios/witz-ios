//
//  ReservationTripsViewController.swift
//  Witz
//
//  Created by abhishek kumar on 01/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class ReservationTripsViewController: WitzSuperViewController {
    
    @IBOutlet weak var view_NoTrips: UIView!
    @IBOutlet weak var lbl_AlertMessage: UILabel!
    @IBOutlet weak var table_View: UITableView!
    
    var arr_Trips = [TripsInfo]()
    var arr_AggregateTrips = [AggregateModal2]()
    var arr_AggregatePending = [PendingAggregateModal]()
    var tripType : [String]?
    
    @IBOutlet weak var lbl_Journey: UILabel!
    @IBOutlet weak var btn_Days: UIButton!
    
    var arrDays = [LinkStrings.DriverOnTrip.Days.Today,LinkStrings.DriverOnTrip.Days.Yesterday,LinkStrings.DriverOnTrip.Days.Last7Days,LinkStrings.DriverOnTrip.Days.All]
    var daySelected : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if tripType != nil {
            lbl_Journey.text = tripType?[1]
            self.navigationItem.titleView = nil
            self.navigationItem.title = tripType?[0]
            
            //            **********************************show alert message on label
            self.lbl_AlertMessage.text = "No \((tripType?[1].prefix(1).uppercased() ?? "") + (tripType?[1].dropFirst().lowercased() ?? "")) Trips Found"
            
            if tripType?[0] == LinkStrings.TripsType.FutureJourney{
                self.arrDays.removeAll()
                self.arrDays = [LinkStrings.DriverOnTrip.Days.Today,LinkStrings.DriverOnTrip.Days.Tomorrow,LinkStrings.DriverOnTrip.Days.ThisWeek,LinkStrings.DriverOnTrip.Days.ThisMonth,LinkStrings.DriverOnTrip.Days.All]
            }
            if tripType?[0] == LinkStrings.TripsType.PendingJourney{
                self.lbl_Journey.isHidden = true
                self.btn_Days.isHidden = true
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showDetails()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showDetails(){
        if daySelected != nil{
            self.btn_Days.setTitle(daySelected, for:.normal)
            self.loadTripsDetail(by: daySelected!)
        }else{
            self.btn_Days.setTitle(LinkStrings.DriverOnTrip.Days.Today, for:.normal)
            self.loadTripsDetail(by: LinkStrings.DriverOnTrip.Days.Today)
        }
    }
    
    
    @IBAction func selectDays(_ sender: UIButton) {
        let controller = UIAlertController.init(title: "Choose from list", message: nil, preferredStyle: .actionSheet)
        /*****************  Create Action sheet of days  *****************/
        for string in arrDays{
            let action = UIAlertAction(title: string, style: .default, handler: { (action) -> Void in
                /***************** load data according to selected day *****************/
                self.btn_Days.setTitle(string, for:.normal)
                self.loadTripsDetail(by: string)
                self.daySelected = string
            })
            controller.addAction(action)
        }
        
        let cancel = UIAlertAction(title: LinkStrings.MostCommon.cancel, style: .cancel, handler: { (action) -> Void in
        })
        
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    func loadTripsDetail(by day:String){
        
        guard self.checkNetworkStatus() else {return}
        showLoader()
        DispatchQueue.global(qos: .background).async {
            var localTimeZoneName: String { return TimeZone.current.identifier }
            
            var parameters = ["skip":"0","limit":"100", "timezone":localTimeZoneName] as [String : Any]
            
            /***********************************Trip selected for  past ***********************************/
            /***********************************Trip selected for Instant ***********************************/
            if self.tripType?[1] == LinkStrings.TripsType.Instant{
                parameters["pastTrip"] = "true"
                parameters["instantTrips"] = "true"
            }
            
            /***********************************Trip selected for reservation ***********************************/
            if self.tripType?[1] == LinkStrings.TripsType.Reservation{
                
                /*Trip selected for Future as well */
                if self.tripType?[0] == LinkStrings.TripsType.FutureJourney{
                    parameters["pastTrip"] = "false"
                    parameters["reservationTrip"] = "true"
                }
                /*Trip selected for Past as well */
                if self.tripType?[0] == LinkStrings.TripsType.PastJourney{
                    parameters["pastTrip"] = "true"
                    parameters["reservationTrip"] = "true"
                }
                /*Trip selected for Pending as well */
                if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                    parameters["reservationPendingTrips"] = "true"
                }
                
            }
            
            
            
            /*Day selected */
            if day == LinkStrings.DriverOnTrip.Days.Today{
                parameters["today"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Yesterday{
                parameters["yesterDay"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Last7Days{
                parameters["lastWeek"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Tomorrow{
                parameters["tomorrow"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.ThisWeek{
                parameters["thisWeek"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.ThisMonth{
                parameters["thisMonth"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.LastMonth{
                parameters["lastMonth"] = "true"
            }
            
            /*************************************Trip selected for Aggregate***************************************/
            
            if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
                
                /*********************************Trip selected for Future as well *********************************************/
                if self.tripType?[0] == LinkStrings.TripsType.FutureJourney || self.tripType?[0] == LinkStrings.TripsType.PastJourney{
                   
                    self.getAggregateTrips(parameters: parameters)
                    return
                }
                /*Trip selected for Pending as well */
                if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                    parameters["serviceRequestPending"] = "true"
                }
                
                
            }
            
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/driver_trips", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                DispatchQueue.main.async {
                    self.hideLoader()
                    self.arr_AggregateTrips.removeAll()
                    self.arr_AggregatePending.removeAll()
                    if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
                        if let arr = response?["data"]["tripsInfo"].array{
                        if arr.count  > 0 {
                            for item in arr{
                                let modal = PendingAggregateModal.init(json: item)
                                self.arr_AggregatePending.append(modal)
                            }
                        }
                        }
                    }else{
                        let modal = TripDriverModal.init(json: response?["data"] ?? ["":""])
                        self.arr_Trips = modal.tripsInfo ?? []
                    }
                    self.table_View.reloadData()
                }
            }, failure: { (error) in
                self.hideLoader()
            })
        }
    }
    
    fileprivate func getAggregateTrips(parameters :  [String : Any]){
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/aggregate_trips", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            DispatchQueue.main.async {
                print(response ?? "")
                self.hideLoader()
                self.arr_AggregateTrips.removeAll()
                
                if self.tripType?[0] == LinkStrings.TripsType.FutureJourney {
                  if let arrHomeToWrk = response?["data"]["futureTrips"]["homeToWork"].array
                   {
                    if arrHomeToWrk.count  > 0 {
                        for item in arrHomeToWrk{
                         let modal = AggregateModal2.init(json: item)
                              modal.aggregateTripType = ShiftTime.Morning.rawValue
                            let timeToCheck = modal.aggregateTripTime?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm:ss", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? ""
                            
                            if self.arr_AggregateTrips.count == 0{
                                 self.arr_AggregateTrips.append(modal)
                            }else {
                                
                                let modalLast = self.arr_AggregateTrips.last
//                                print("date to compare \(timeToCheck)")
//                                print("date we got \((modalLast?.aggregateTripTime?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm:ss", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? ""))")
                                if (modalLast?.aggregateTripTime?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm:ss", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "") != timeToCheck {
                                    self.arr_AggregateTrips.append(modal)
                                }
                            }
                        }
                    }
                }
                    if let arrWrkToHome = response?["data"]["futureTrips"]["workToHome"].array
                    {
                        if arrWrkToHome.count  > 0 {
                            for item in arrWrkToHome{
                                let modal = AggregateModal2.init(json: item)
                                modal.aggregateTripType = ShiftTime.Evening.rawValue
                                let timeToCheck = modal.aggregateTripTime
                                let modalLast = self.arr_AggregateTrips.last
                                if modalLast?.aggregateTripType != ShiftTime.Evening.rawValue ||  modalLast?.aggregateTripType == ShiftTime.Evening.rawValue && modalLast?.aggregateTripTime != timeToCheck
                                {
                                    self.arr_AggregateTrips.append(modal)
                                }
                            }
                        }
                    }
                }
                
//                if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
//                    let arr = response?["data"]["aggregateTripsInfo"].array
//                    if (arr?.count ?? 0) > 0 {
//                        for item in arr!{
//                            let modal = AgrregateTripModal.init(json: item)
//                            if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
//                                self.arr_AggregateTrips.append(modal)
//                            }else{
//                                let modalReturn =  AgrregateTripModal.init(json: item)
//                                modal.aggregateTripType = ShiftTime.Morning.rawValue
//                                self.arr_AggregateTrips.append(modal)
//                                modalReturn.aggregateTripType = ShiftTime.Evening.rawValue
//                                self.arr_AggregateTrips.append(modalReturn)
//                            }
//                        }
//                    }
//                }
                self.table_View.reloadData()
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
}

//MARK:- Table View Delegates
extension ReservationTripsViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if arr_Trips.count > 0 || arr_AggregateTrips.count > 0 || arr_AggregatePending.count > 0{
            
            var countToReturn = 0
            /***********************************Trip selected for Instant ***********************************/
            if self.tripType?[1] == LinkStrings.TripsType.Instant{
                let arrInstant =  arr_Trips.filter{
                    $0.tripType == "1"
                }
                arr_Trips = arrInstant
                countToReturn = arr_Trips.count
            }
                
                /***********************************Trip selected for reservation ***********************************/
            else if self.tripType?[1] == LinkStrings.TripsType.Reservation{
                let arrReservation =  arr_Trips.filter{
                    $0.tripType == "2"
                }
                arr_Trips = arrReservation
                countToReturn = arr_Trips.count
            }
                
                /*************************************Trip selected for Aggregate***************************************/
                
            else if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
                if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                    countToReturn = arr_AggregatePending.count
                }else{
                    countToReturn = arr_AggregateTrips.count
                }
            }
            
            if countToReturn > 0 {
                view_NoTrips.isHidden = true   //hide no trips view
            }else{
                view_NoTrips.isHidden = false //show no trips view after all evaluation
            }
            return countToReturn
            
        }else{
            view_NoTrips.isHidden = false //show no trips view
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellReservationTrips", for: indexPath) as! CellReservationTrips
        /***********************************Trip selected for reservation ***********************************/
        if self.tripType?[1] == LinkStrings.TripsType.Reservation {
            if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                cell.configureCell(with: arr_Trips[indexPath.row], index: indexPath,isPending: true)
            }else{
                cell.configureCell(with: arr_Trips[indexPath.row], index: indexPath,isPending: false)
            }
        }
            /*************************************Trip selected for Aggregate***************************************/
            
        else  if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
            if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                cell.configureCell_PendingAggregate(with: arr_AggregatePending[indexPath.row])
            }else{
                cell.configureAggregateCell(with: arr_AggregateTrips[indexPath.row], index: indexPath,isPending: false)
            }
        }
        else{
            cell.configureCell(with: arr_Trips[indexPath.row], index: indexPath,isPending: false)
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tripType != nil {
            let value = (tripType?[0] ?? "")
            switch(value){
            case LinkStrings.TripsType.PastJourney:
                if  (tripType?[1] ?? "") == LinkStrings.TripsType.Instant{
                    let modal =  arr_Trips[indexPath.row]
                    
                    if modal.isShared == true{
                        performSegue(withIdentifier: "segue_SharedTripDetail", sender: indexPath)
                        
                    }else{
                        performSegue(withIdentifier: "segue_InstantTripDetail", sender: indexPath)
                        
                    }
                }
                else if  (tripType?[1] ?? "") == LinkStrings.TripsType.Reservation{
                    performSegue(withIdentifier: "segue_InstantTripDetail", sender: indexPath)
//                    performSegue(withIdentifier: "segue_ReservationTripDetail", sender: indexPath)
                }
                else if  (tripType?[1] ?? "") == LinkStrings.TripsType.Aggregate{
                    let modal =  arr_AggregateTrips[indexPath.row]
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "SharingTripDetailsViewController") as! SharingTripDetailsViewController
                    controller.tripForAggregate = modal.aggregateFinalTripId
                    
                    if (modal.aggregateTripType != nil){
                        if modal.aggregateTripType == ShiftTime.Morning.rawValue{
                            controller.shiftFor = ShiftTime.Morning
                        }else{
                            controller.shiftFor = ShiftTime.Evening
                        }
                    }
                    
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                break
            case LinkStrings.TripsType.FutureJourney:
                if  (tripType?[1] ?? "") == LinkStrings.TripsType.Reservation{
                    performSegue(withIdentifier: "segue_ReservationTripDetail", sender: indexPath)
                }
                else if  (tripType?[1] ?? "") == LinkStrings.TripsType.Aggregate{
                    let modal =  arr_AggregateTrips[indexPath.row]
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "SharingTripDetailsViewController") as! SharingTripDetailsViewController
                    controller.tripForAggregate = modal.aggregateFinalTripId
                    if (modal.aggregateTripType != nil){
                        if modal.aggregateTripType == ShiftTime.Morning.rawValue{
                            controller.shiftFor = ShiftTime.Morning
                        }else{
                            controller.shiftFor = ShiftTime.Evening
                        }
                    }
                    let dateString = modal.aggregateTripTime?.getDateFromString(formatHave: "yyyy-MM-dd")
                    let currentDate = Date()
                    if currentDate.day == dateString?.day {
                        controller.letsGoNeeded = true
                    }
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                break
            case LinkStrings.TripsType.PendingJourney:
                if  (tripType?[1] ?? "") == LinkStrings.TripsType.Reservation{
                    performSegue(withIdentifier: "segue_ReservationTripDetail", sender: indexPath)
                }
                else if  (tripType?[1] ?? "") == LinkStrings.TripsType.Aggregate{
                    performSegue(withIdentifier: "segue_AggregateDetail", sender: indexPath)
                }
                break
            default:
                print("nothing to do")
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_ReservationTripDetail" {
            if let controller = segue.destination as?  ReservationTripDetailViewController{
                if let index = sender as? IndexPath{
                    controller.tripDetail = self.arr_Trips[index.row]
                    controller.tripType = self.tripType
                }
            }
        }else  if segue.identifier == "segue_AggregateDetail" {
            if let controller = segue.destination as?  GiveOfferAggregateViewController{
                if let index = sender as? IndexPath{
                    controller.groupID = self.arr_AggregatePending[index.row].groupAggregateTripId
                }
            }
        }
        else if segue.identifier == "segue_SharedTripDetail" {
            if let controller = segue.destination as?  SharingTripDetailsViewController{
                if let index = sender as? IndexPath{
                    controller.tripDetail = self.arr_Trips[index.row]
                }
            }
        }else if segue.identifier == "segue_InstantTripDetail" {
            if let controller = segue.destination as?  InstantTripDetailViewController{
                if let index = sender as? IndexPath{
                    controller.tripDetail = self.arr_Trips[index.row]
                }
            }
        }
    }
}


