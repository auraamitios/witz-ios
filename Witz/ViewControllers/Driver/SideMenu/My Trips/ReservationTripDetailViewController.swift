//
//  ReservationTripDetailViewController.swift
//  Witz
//
//  Created by abhishek kumar on 01/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//


import UIKit
import GoogleMaps
import Kingfisher
import Cosmos

class ReservationTripDetailViewController: WitzSuperViewController {
    @IBOutlet weak var imgView_Profile: UIImageView!
    @IBOutlet weak var lbl_startAddress: UILabel!
    @IBOutlet weak var lbl_EndAddress: UILabel!
    @IBOutlet weak var lbl_RiderName: UILabel!
    @IBOutlet weak var lbl_Amount: UILabel!
    @IBOutlet weak var lbl_DistanceTime: UILabel!
    @IBOutlet weak var view_StarRating: CosmosView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var constraint_TrailingLetsGo: NSLayoutConstraint!
    @IBOutlet weak var constraint_widthLetsGo: NSLayoutConstraint!
    @IBOutlet weak var btn_letGo: UIButton!
    @IBOutlet weak var view_Cancel: UIView!
    @IBOutlet weak var lbl_Seprator: UILabel!
    @IBOutlet weak var btn_Msg: UIButton!
    @IBOutlet weak var btn_Call: UIButton!
    
    var tripDetail : TripsInfo?
    var tripType : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = false
        mapView.shouldHideToolbarPlaceholder = true
        mapView.delegate = self
        self.performSelector(onMainThread: #selector(hideLetsGo), with: nil, waitUntilDone: true)
        if tripDetail != nil{
            showTripDetails()
            getRideDetail()
        }
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func showTripDetails(){
        
        lbl_DistanceTime.text = tripDetail?.bookingDate?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        lbl_Amount.text = "\(tripDetail?.driverEarning ?? 0.0) ₺"
        lbl_startAddress.text = tripDetail?.startAddress ?? ""
        lbl_EndAddress.text = tripDetail?.endAddress ?? ""
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.startLocation?.coordinates?[0] ?? 0))
        let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.endLocation?.coordinates?[0] ?? 0))
        
        if tripType != nil {
            self.navigationItem.titleView = nil
            
            if tripType?[0] == LinkStrings.TripsType.FutureJourney && self.tripType?[1] == LinkStrings.TripsType.Reservation{
                let currentTime = Date()
                let date = tripDetail?.bookingDate?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                self.navigationItem.title = LinkStrings.TitleForView.FutureJourney
                //This one is the time limit in which driver can start the trip
                let driverHasTimeLimit =  DriverManager.sharedInstance.user_Driver.reservationGeneralSetting?.driverReadyTime ?? 0
                let diff = date?.minutesSince(currentTime) ?? 00.0
                
                //allowed to start the ride if still there is time
                if diff < Double(driverHasTimeLimit) && diff > 0{
                    showLetsGo()
                }else{
                    hideLetsGo()  // hide the button as time is not accurate for start the trip
                }
            }else  if tripType?[0] == LinkStrings.TripsType.PastJourney && self.tripType?[1] == LinkStrings.TripsType.Reservation {
                self.navigationItem.title = LinkStrings.TitleForView.PastTrip
                view_Cancel.isHidden = true
                btn_Call.isHidden = true
                btn_Msg.isHidden = true
                lbl_Seprator.isHidden = true
            }else if self.tripType?[1] == LinkStrings.TripsType.Reservation{
                self.navigationItem.title = LinkStrings.TitleForView.PendingRequest
                if let arrBids = tripDetail?.bidByDriver{
                    for data in arrBids{
                        if data.driveId == DriverManager.sharedInstance.user_Driver.driver_Id{
                            self.lbl_Amount.text = "\(data.offerPriceByDriver?.twoDecimalString ?? "") ₺"
                        }
                    }
                }
            }
        }
        
        //Marker for pick up
        let marker1 = GMSMarker(position: coordinates)
        marker1.title = "Pick UP Location"
        marker1.appearAnimation = GMSMarkerAnimation.pop
        marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize.init(width: 30.0, height: 30.0))
        marker1.map = mapView
        
        //Marker for Drop
        let marker2 = GMSMarker(position: coordinates1)
        marker2.title = "Drop Location"
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon =  Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize.init(width: 30.0, height: 30.0))
        marker2.map = mapView
        
        showLoader()
        self.showPolyLine(source: coordinates, destination: coordinates1)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_ReservationStart"{
            if let controller = segue.destination as? ReservationLetsGOViewController{
                //                controller.tripID = tripDetail?.id
                controller.tripDetail = tripDetail
            }
        }
    }
    
    //MARK:- custom method
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            //            print("wayponits****************\(distance)")
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }  
        hideLoader()
    }
    
    //MARK:- get Ride Detail from server
    public func getRideDetail(){
        
        guard self.checkNetworkStatus() else {
            return
        }
        print(tripDetail?.id ?? "j")
        if let tripID = tripDetail?.id{
            let parameters =   ["tripId":tripID]
            
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/get_trip_detail", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
                self.hideLoader()
                if let  result = response?["data"]{
                    DriverManager.sharedInstance.ride_Details = TripModalDriver(json: result)
                    
                    let riderDetails =    DriverManager.sharedInstance.ride_Details?.riderData
                    self.lbl_RiderName.text =  riderDetails?.fullName?.nameFormatted ?? ""
                    self.view_StarRating.rating = riderDetails?.avgRating ?? 0.0
                    let imgUrl = "\(kBaseImageURL)\(riderDetails?.pic?.original ?? "")"
                    if let url = imgUrl.url{
                        KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                            DispatchQueue.main.async {
                                
                                if let thumbImage = image {
                                    self.imgView_Profile.image = thumbImage
                                }
                                self.imgView_Profile.setNeedsDisplay()
                            }
                        })
                    }
                }
            }, failure: { (error) in
                self.hideLoader()
            })
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideLetsGo()  {
        
        btn_letGo.isHidden = true
        constraint_widthLetsGo.constant = 0
        constraint_TrailingLetsGo.constant = 10
    }
    
    func showLetsGo()  {
        
        btn_letGo.isHidden = false
        constraint_widthLetsGo.constant = (self.view.frame.size.width/2) - 20
        constraint_TrailingLetsGo.constant = 20
    }
    
    //MARK:- Button Action
    
    @IBAction func callAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CallingTwillioViewController") as! CallingTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func messageAction(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MessageTwillioViewController") as! MessageTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func cancelRequest(_ sender: UIButton) {
        if tripType != nil {
            if tripType?[0] == LinkStrings.TripsType.FutureJourney && self.tripType?[1] == LinkStrings.TripsType.Reservation{  // this one is for future trips
                AlertHelper.displayAlert(title: LinkStrings.App.AppName, message: LinkStrings.DriverPopUpMessage.CancelReservation, style: .alert, actionButtonTitle: [LinkStrings.Yes,LinkStrings.Cancel], completionHandler: { (action) in
                    
                    print(action.title ?? "")
                    
                    if action.title == LinkStrings.Yes{
                        guard self.checkNetworkStatus() else {
                            return
                        }
                        if let tripID = self.tripDetail?.id{
                            self.showLoader()
                            let parameters =   ["tripId":tripID]
                            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/cancel_ride", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                                self.hideLoader()
                                DispatchQueue.global(qos: .background).async {
                                    Utility.getDriverInfoRefreshed()
                                }
                                self.navigationController?.popViewController(animated: true)
                            }, failure: { (error) in
                                self.hideLoader()
                            })
                        }
                    }
                }, controller: self)
            }else{  // reject the offer given by driver reservation in pending
                AlertHelper.displayAlert(title: LinkStrings.App.AppName, message: LinkStrings.DriverPopUpMessage.CancelReservation, style: .alert, actionButtonTitle: [LinkStrings.Yes,LinkStrings.Cancel], completionHandler: { (action) in
                    
                    print(action.title ?? "")
                    
                    if action.title == LinkStrings.Yes{guard self.checkNetworkStatus() else {
                        return
                        }
                        if let tripID = self.tripDetail?.id{
                            self.showLoader()
                            let parameters =   ["tripId":tripID]
                            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/cancelReservationOffer", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
                                self.hideLoader()
                                DispatchQueue.global(qos: .background).async {
                                    Utility.getDriverInfoRefreshed()
                                }
                                self.navigationController?.popViewController(animated: true)
                            }, failure: { (error) in
                                self.hideLoader()
                            })
                        }
                    }
                }, controller: self)
            }
        }
    }
}

extension ReservationTripDetailViewController : GMSMapViewDelegate{
    // MARK: GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewStaticMapViewController") as! ViewStaticMapViewController
        controller.strTimeKm =   "\(String(format: "%.2f",tripDetail?.distance ?? 0)) KM/ \(Int(tripDetail?.duration ?? 0))Min"
        controller.tripDetails = tripDetail
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
