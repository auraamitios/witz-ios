//
//  NotificationViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/14/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationViewController: WitzSuperViewController {
    final let kConstLimit = 20
    
    @IBOutlet weak var barBtn_clearAll: UIBarButtonItem!
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var view_NoNotificaiton: UIView!
    fileprivate var arr_Notification = [JSON]()
    var limit = 0
    var totalNotification = 0
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setNavigationBarItem()
        table_View.addSubview(refreshControl)
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(refreshThruPullDown), for: UIControlEvents.valueChanged)
        barBtn_clearAll.setTitleTextAttributes([NSFontAttributeName:  UIFont.systemFont(ofSize: 14)], for: UIControlState.normal)
        
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.Notification
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadNotificationData(skip:0,limit: "\(kConstLimit)")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func refreshThruPullDown(){
        loadNotificationData(skip: 0, limit: "\(kConstLimit)")
    }
    //MARK:- Button Action
    
    @IBAction func clearAll(_ sender: UIBarButtonItem) {
        
        AlertHelper.displayAlert(title: LinkStrings.App.AppName, message: LinkStrings.Notifications.WantDelete, style: .alert, actionButtonTitle: [LinkStrings.OK,LinkStrings.Cancel], completionHandler: { (action) in
            
            print(action.title ?? "")
            
            if action.title == LinkStrings.OK{
                guard self.checkNetworkStatus() else {
                    return
                }
                self.showLoader()
                WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/deleteAllNotification", withInputString: ["":""], requestType: .put, isAuthorization: true, success: { (response) in
                    self.hideLoader()
                    
                    if response?["statusCode"] == 200 {
                        self.arr_Notification.removeAll()
                        DispatchQueue.global(qos: .background).async {
                            Utility.getDriverInfoRefreshed()
                        }
                        self.table_View.reloadData()
                    }
                    
                }) { (error ) in
                    self.hideLoader()
                }
                
            }
            
        }, controller: self)
    }
    
    
    /**
     Load Notification data from server and polpulate on screen
     */
    
    public func loadNotificationData(skip:Int ,limit:String){
        
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let parameters =   ["skip": "\(skip)" , "limit":limit]
        
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/getNotifications", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            self.refreshControl.endRefreshing()
            if let arr = response?["data"]["notificationData"].array{
                
                if skip > 0 {
                    for modal in arr{
                        self.arr_Notification.append(modal)
                    }
                }else{
                    self.arr_Notification = arr
                    
                }
                self.limit = self.arr_Notification.count
                self.totalNotification = response?["data"]["count"].int ?? self.kConstLimit
                self.table_View.reloadData()
            }
        }) { (error ) in
            self.hideLoader()
            self.refreshControl.endRefreshing()
        }
    }
}

extension  NotificationViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arr_Notification.count >  0 {
            view_NoNotificaiton.isHidden = true
        }
        else{
            view_NoNotificaiton.isHidden = false
        }
        
        return arr_Notification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellNotification", for: indexPath) as! CellNotification
        cell.updateCellData(arr_Notification[indexPath.row], index: indexPath)
        cell.btn_Clear.addTarget(self, action: #selector(clearNotification(_: )), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == limit-1{
            if limit >= totalNotification{return}
            limit = limit+kConstLimit
            loadNotificationData(skip: limit-kConstLimit, limit: "\(limit)")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let modal = arr_Notification[indexPath.row]
        DispatchQueue.global(qos: .background).async {
            self.markAsRead(id: modal["_id"].string ?? "", rowTapped: indexPath.row)
        }
        handleNotificationTapped(modal: modal, rowTapped: indexPath.row)
    }
    
    
    /**
     Clear single Notification
     */
    
    func clearNotification(_ sender: UIButton){
        guard self.checkNetworkStatus() else {return}
        let notificationId = self.arr_Notification[sender.tag]
        self.showLoader()
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/deleteNotification", withInputString: ["notificationId":notificationId["_id"].string ?? ""], requestType: .put, isAuthorization: true, success: { (response) in
            self.hideLoader()
            self.arr_Notification.remove(at: sender.tag)
            DispatchQueue.global(qos: .background).async {
                Utility.getDriverInfoRefreshed()
            }
            self.table_View.reloadData()
        }) { (error ) in
            self.hideLoader()
        }
    }
    
    /**
     Mark as Read Notification
     */
    
    fileprivate func markAsRead(id:String , rowTapped:Int){
        
        guard self.checkNetworkStatus() else {
            return
        }
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/readNotification", withInputString: ["notificationId":id], requestType: .put, isAuthorization: true, success: { (response) in
            
            var modal = self.arr_Notification[rowTapped]
            modal["isRead"] = true
            self.arr_Notification.remove(at: rowTapped)
            self.arr_Notification.insert(modal, at: rowTapped)
            DispatchQueue.main.async {
                self.table_View.reloadRows(at: [IndexPath.init(row: rowTapped, section: 0)], with: .fade)
            }
        }) { (error ) in
        }
    }
    
    
    
    /**
     Handle Notification as per different type
     */
    
    fileprivate func handleNotificationTapped(modal:JSON , rowTapped:Int){
        
        let notificationFor = Int(modal["notificationCode"].string ?? "0")!
        let storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Main, bundle: nil)
        
        switch (notificationFor){
        case NotificationType.ACCEPT_BOOKING_OFFER.rawValue:
            
            self.performSegue(withIdentifier: "segue_OfferAccepted", sender: modal["tripId"]["bookingDate"].string?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
            
            break
            
        case NotificationType.BOOKING_REQUEST.rawValue:
            
            if let tripID = modal["tripId"]["_id"].string{
                if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "ReservationRequestPopUpViewController") as? UINavigationController {
                    if let controller =   rootViewController.viewControllers[0] as? ReservationRequestPopUpViewController{
                        controller.tripId =  tripID
                        let dateBooking = modal["tripId"]["bookingDate"].string
                        let valueTime = modal["tripId"]["createdAt"].string
                        controller.dateBooking = dateBooking
                        controller.dateCreated = valueTime
                        var timeLeftToAccept =  0
                        let currentTime = Date()
                        let date = dateBooking?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                        
                        if (date?.hoursSince(currentTime) ?? 44) < 24.0 {
                            timeLeftToAccept =    DriverManager.sharedInstance.user_Driver.reservationGeneralSetting?.singleDayDriverAcceptanceDuration ?? 0
                        }else{
                            timeLeftToAccept =   DriverManager.sharedInstance.user_Driver.reservationGeneralSetting?.futureDayDriverAcceptanceDuration ?? 0
                        }
                        
                        if let dateCreate = valueTime?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                        {
                            timeLeftToAccept =  timeLeftToAccept * 60 //now in min
                            let diff = Int(currentTime.secondsSince(dateCreate))
                            
                            if (timeLeftToAccept - diff) < 0 {
                                
                                AlertHelper.displayOkAlert(title: LinkStrings.App.AppName, message: LinkStrings.Notifications.TimeOut, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: {(action) in }, controller: self)
                                
                                return
                            }
                        }
                    }
                    AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewController, animated: false, completion: { })
                }
            }
            break
            
        case NotificationType.NEW_AGGREGATE_REQUEST.rawValue:
            
            if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "AggregateRequestPopUpViewController") as? UINavigationController {
                if let controller =   rootViewController.viewControllers[0] as? AggregateRequestPopUpViewController{
                    controller.groupId = modal["groupAggregateTripId"].string
                }
                
                AppDelegate.sharedInstance().window?.currentViewController()?.present(rootViewController, animated: false, completion: {
                    
                })
            }
            break
            
        case NotificationType.BOOKING_REMINDER.rawValue:
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ReservationTripDetailViewController") as! ReservationTripDetailViewController
            controller.tripType = [LinkStrings.TripsType.FutureJourney,LinkStrings.TripsType.Reservation]
            controller.tripDetail = TripsInfo.init(json:modal["tripId"])
            //                TripsInfo.init(json: modal)
            self.navigationController?.pushViewController(controller,animated:true)
            
            break
            
        case NotificationType.RIDE_CANCEL_BY_RIDER.rawValue:
            let alert = UIAlertController.init(title: LinkStrings.App.AppName, message: "\(modal["riderId"]["fullName"].string?.capitalized.nameFormatted ?? "" ) has cancelled Trip on \(modal["notificationDate"].string?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: LinkStrings.OK, style: .default, handler: { (action) in
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: {
                
            })
            
            break
            
        case NotificationType.FINAL_AGGREGATE_RIDE.rawValue:
            
            if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "AggregateRequestConfirmedViewController") as? UINavigationController {
                if let controller =   rootViewController.viewControllers[0] as? AggregateRequestConfirmedViewController{
                    controller.groupIdToSend = modal["groupAggregateTripId"].string
                }
                self.present(rootViewController, animated: true, completion: {
                    
                })
            }
            break
            
        case NotificationType.DRIVER_DOCUMENT_ACTION_ADMIN.rawValue:
            self.performSegue(withIdentifier: "segue_DocumentVerification", sender: modal)
            break
            
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_OfferAccepted"{
            if let rootViewController = segue.destination as? UINavigationController{
                if let controller =   rootViewController.viewControllers[0] as? ReservationConfirmedViewController{
                    controller.dateToShow = sender as? String
                }
            }
        }
        else if segue.identifier == "segue_DocumentVerification"{
            if let controller = segue.destination as? DocumentApprovalViewController{
                controller.modalData = sender as? JSON
            }
        }
    }
}
