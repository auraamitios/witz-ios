//
//  DocumentApprovalViewController.swift
//  Witz
//
//  Created by abhishek kumar on 19/01/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON

class DocumentApprovalViewController: WitzSuperViewController {

    @IBOutlet weak var imgView_Doc: UIImageView!
    @IBOutlet weak var lbl_Comments: UILabel!
    @IBOutlet weak var lbl_DocName: UILabel!
    
    var modalData: JSON?
    var modalDataPush: JSON?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if modalData != nil {
             if modalData?["document_verification_status"].bool == false{
                imgView_Doc.image = #imageLiteral(resourceName: "reject")
            }
             lbl_DocName.text = modalData?["document_name"].string
             lbl_Comments.text = modalData?["notificationMsg"].string ?? ""
        }
        else if  modalDataPush != nil {
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeView))
            self.view.addGestureRecognizer(tap)
            self.left_barButton.isHidden = true

            if modalDataPush?["approveStatus"].bool == false{
                imgView_Doc.image = #imageLiteral(resourceName: "reject")
            }
            lbl_DocName.text = modalDataPush?["document_name"].string
            lbl_Comments.text = modalDataPush?["comment"].string ?? ""
        }
    }
  
    func closeView()  {
        self.dismiss(animated: false) {
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
