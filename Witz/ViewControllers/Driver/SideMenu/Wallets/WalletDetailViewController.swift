//
//  WalletDetailViewController.swift
//  Witz
//
//  Created by abhishek kumar on 23/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class WalletDetailViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var lbl_Amount: UILabel!
    @IBOutlet weak var lbl_StartAddress: UILabel!
    @IBOutlet weak var lbl_EndAddress: UILabel!
    @IBOutlet weak var lbl_Km: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    var tripDetail : Trips_Wallet?
    var urlToSend = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if tripDetail != nil{
            showTripDetails()
        }
    }
    
    func showTripDetails(){
        lbl_DateTime.text = tripDetail?.createdAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        lbl_Km.text = "\(String(format: "%.2f",tripDetail?.distance ?? 0.0))"
        lbl_Time.text = "\( String(format: "%.2f",tripDetail?.duration ?? 0))"
        lbl_Amount.text = "\(tripDetail?.driverEarning ?? 0.0) ₺"
        lbl_StartAddress.text = tripDetail?.startAddress ?? ""
        lbl_EndAddress.text = tripDetail?.endAddress ?? ""
        
        urlToSend =  getImageUrl(sourceLat: tripDetail?.startLocation?.coordinates?[1] ?? 0, sourceLng: tripDetail?.startLocation?.coordinates?[0] ?? 0, destinationLat: tripDetail?.endLocation?.coordinates?[1] ?? 0, destinationLng: tripDetail?.endLocation?.coordinates?[0] ?? 0)
        
    }
    
    
    func getImageUrl(sourceLat:Float,sourceLng:Float,destinationLat:Float,destinationLng:Float) -> String{
        let width = Int(self.view.frame.size.width*1.5)
        let height = Int(self.view.frame.size.height*1.5)
        
        let str =  "https://maps.googleapis.com/maps/api/staticmap?size=\(width)x\(height)&maptype=roadmap&markers=size:mid%7Ccolor:red%7C\(sourceLat),\(sourceLng)%7C\(destinationLat),\(destinationLng)"
        
        return str
    }
    
    @IBAction func viewOnMap(_ sender: UIButton) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_ViewMap" {
            if let controller = segue.destination as? ViewStaticMapViewController{
                controller.strImage  = urlToSend
                controller.strTimeKm  = "\(lbl_Km.text ?? "") KM/\(lbl_Time.text ?? "") Min"
                controller.tripDetailsWallet = tripDetail
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
