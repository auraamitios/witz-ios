//
//  WalletViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/14/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class WalletViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_Promotion: UILabel!
    @IBOutlet weak var lbl_DaySelected: UILabel!
    @IBOutlet weak var lbl_TripCount: UILabel!
    @IBOutlet weak var lbl_KmCount: UILabel!
    @IBOutlet weak var lbl_EarningCount: UILabel!
    @IBOutlet weak var btn_DaySelected: UIBarButtonItem!
    @IBOutlet weak var table_View: UITableView!
    
    let arrDays = [LinkStrings.DriverOnTrip.Days.Today,LinkStrings.DriverOnTrip.Days.Yesterday,LinkStrings.DriverOnTrip.Days.Last7Days,LinkStrings.DriverOnTrip.Days.All]
    
    var arr_Trips = [Trips_Wallet]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setNavigationBarItem()
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.DriverSideMenuItem.MYWALLET
        showDetails()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showDetails(){
        self.btn_DaySelected.title = LinkStrings.DriverOnTrip.Days.Today
        self.lbl_DaySelected.text = LinkStrings.DriverOnTrip.Days.Today
        self.loadWalletData(by: LinkStrings.DriverOnTrip.Days.Today)
    }
    
    @IBAction func selectDayToShowData(_ sender: UIBarButtonItem) {
        
        let controller = UIAlertController.init(title: "Choose from list", message: nil, preferredStyle: .actionSheet)
        
        for string in arrDays{
            let action = UIAlertAction(title: string, style: .default, handler: { (action) -> Void in
                self.btn_DaySelected.title = string
                self.lbl_DaySelected.text = string
                self.loadWalletData(by: string)
            })
            controller.addAction(action)
        }
        
        let cancel = UIAlertAction(title: LinkStrings.MostCommon.cancel, style: .cancel, handler: { (action) -> Void in
        })
        
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    func loadWalletData(by day:String){
        
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        DispatchQueue.global(qos: .background).async {
            var localTimeZoneName: String {return TimeZone.current.identifier}
            var parameters =   ["timezone":localTimeZoneName] as [String : Any]
            if day == LinkStrings.DriverOnTrip.Days.Today{
                parameters["today"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Yesterday{
                parameters["yesterday"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Last7Days{
                parameters["thisWeek"] = "true"
            }
            
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/my-earnings", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                    let modal = WalletFinalDriverModal.init(json: response?["data"] ?? ["":""])
                    self.lbl_EarningCount.text = "\(modal.totalAmount ?? 0.0) ₺"
                    self.lbl_TripCount.text = "\(modal.totalTrips ?? 0)"
                    self.lbl_KmCount.text = "\(modal.totalKm ?? 0.0)"
                    self.lbl_Promotion.text = " Promotional : \((modal.referalAmount ?? 0.0)+(modal.welcomeWallet ?? 0.0)) ₺  "
                    self.arr_Trips = modal.trips ?? []
                    self.table_View.reloadData()
                }
                
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }
    }
    
    //    func loadDriverEarning(by day:String){
    //
    //        guard self.checkNetworkStatus() else {
    //            return
    //        }
    //        showLoader()
    //        var localTimeZoneName: String { return TimeZone.current.identifier }
    //        var parameters =   [ "timezone":localTimeZoneName] as [String : Any]
    //
    //        if day == LinkStrings.DriverOnTrip.Days.Today{
    //            parameters["today"] = "true"
    //        }else if day == LinkStrings.DriverOnTrip.Days.Yesterday{
    //            parameters["yesterDay"] = "true"
    //        }else if day == LinkStrings.DriverOnTrip.Days.Last7Days{
    //            parameters["thisWeek"] = "true"
    //        }else {
    ////            parameters["thisMonth"] = "true"
    ////            parameters["thisWeek"] = "true"
    ////            parameters["yesterDay"] = "true"
    ////            parameters["today"] = "true"
    ////            parameters["lastMonth"] = "true"
    //        }
    //
    //           loadWalletData(by: day)  // load trip as well
    //
    //        DispatchQueue.global(qos: .background).async {
    //            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/driver_earning", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
    //
    //                DispatchQueue.main.async {
    //                    self.hideLoader()
    //                    let modal = DriverEarningModal.init(json: response?["data"] ?? ["":""])
    //                    self.lbl_EarningCount.text = "\(modal.earning ?? 0.0) ₺"
    //                    self.lbl_TripCount.text = "\(modal.count ?? 0)"
    //                    self.lbl_KmCount.text = "\(modal.kM ?? 0.0)"
    //                }
    //
    //            }, failure: { (error) in
    //                self.hideLoader()
    //            })
    //        }
    //    }
}

extension WalletViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_Trips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_Wallet", for: indexPath) as! CellWallet
        cell.configureCell(with: arr_Trips[indexPath.row], index: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segue_WalletDetail", sender: indexPath)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_WalletDetail"{
            if let controller = segue.destination as?  WalletDetailViewController{
                if let index = sender as? IndexPath{
                    controller.tripDetail = self.arr_Trips[index.row]
                }
            }
        }
    }
}
