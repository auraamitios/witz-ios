//
//  ViewStaticMapViewController.swift
//  Witz
//
//  Created by abhishek kumar on 23/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import GoogleMaps

class ViewStaticMapViewController: WitzSuperViewController {
    
    @IBOutlet weak var imgView_map: UIImageView!
    @IBOutlet weak var lbl_TimeDuration: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    
    //properties to get data from another controller
     var tripDetails_Aggregate: AgrregateTripModal?
    var tripDetailsWallet: Trips_Wallet?
    var tripDetails : TripsInfo?
    var strImage : String?
    var strTimeKm : String?
    var imgAlready : UIImage?
    var isShared : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mapView.isUserInteractionEnabled = true
     
        if strTimeKm != nil {
            lbl_TimeDuration.text = strTimeKm
        }
        if isShared == true{
            
            showSharedTripDetails() // this one for shared multiple path
            
        }else if tripDetails_Aggregate != nil {
            
           showAggregateTrip() // this one for aggregate multiple path
            
        }else if tripDetailsWallet != nil {
            
            let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetailsWallet?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetailsWallet?.startLocation?.coordinates?[0] ?? 0))
            let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetailsWallet?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetailsWallet?.endLocation?.coordinates?[0] ?? 0))
            showTripDetails(source: coordinates , destination:coordinates1 ) //normal single path for wallet
            
        }else{
            
            let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetails?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetails?.startLocation?.coordinates?[0] ?? 0))
            let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetails?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetails?.endLocation?.coordinates?[0] ?? 0))
            showTripDetails(source: coordinates , destination:coordinates1 )  //normal single path
            
        }
    }
    
    fileprivate func showAggregateTrip(){
        if (tripDetails_Aggregate?.wayPoints?.count ?? 0) > 0 {self.drawPathUsing(wayPoints: (tripDetails_Aggregate?.wayPoints)!)}
    }
    
    
    fileprivate func drawPathUsing(wayPoints:[WayPointsAggregate]){
        
        mapView.clear()
        let start = wayPoints[0]
        let source =  CLLocation(latitude: CLLocationDegrees(start.coordinates?[1] ?? 0), longitude:CLLocationDegrees(start.coordinates?[0] ?? 0))
        putMarkerAggregate(icon: #imageLiteral(resourceName: "flag"), coordinate: source.coordinate) // pickup Station
        let end = wayPoints[wayPoints.count-1]
        let destination =  CLLocation(latitude: CLLocationDegrees(end.coordinates?[1] ?? 0), longitude:CLLocationDegrees(end.coordinates?[0] ?? 0))
        putMarkerAggregate(icon: #imageLiteral(resourceName: "flag"), coordinate: destination.coordinate)// drop Station
        var arr = [[String : Any]]()  // it will keep all coodinate and distance from source
        
        if wayPoints.count < 3{ // no halt between the path
            showPolyLine(source: source.coordinate, destination: destination.coordinate)
            self.setRiderPoints(start: source.coordinate, end: destination.coordinate)
        }
        
        // preparing arr of dict whose conatains coordinate and distance
        for i in 1..<wayPoints.count {
            
            let modal = wayPoints[i]
            let halt =  CLLocation(latitude: CLLocationDegrees(modal.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modal.coordinates?[0] ?? 0))
            let distPick = source.distance(from: halt)
            putMarkerAggregate(icon: #imageLiteral(resourceName: "flag"), coordinate: halt.coordinate)// stop Station
            
            let dict = [KeysForSharingPickUpViewController.COORDINATE:halt.coordinate,KeysForSharingPickUpViewController.DIST:distPick] as [String : Any]
            arr.append(dict)
        }
        
        let key = KeysForSharingPickUpViewController.DIST // The key you want to sort by
        
        let finalarray = arr.sorted {  // this one keep the track of sorted coordinates for plotting
            switch ($0[key], $1[key]) {
            case let (lhs as Double, rhs as Double):
                return  lhs < rhs
            default:
                return true
            }
        }
        
        var sourceStation = source.coordinate // get the source coordinate
        for i in 0..<finalarray.count {
            let dict = finalarray[i]
            let station = dict[KeysForSharingPickUpViewController.COORDINATE] as! CLLocationCoordinate2D
            showPolyLine(source: sourceStation , destination: station)
            sourceStation = station // change source station now
        }
        self.setRiderPoints(start: sourceStation, end: source.coordinate)
    }
    
    fileprivate func setRiderPoints(start:CLLocationCoordinate2D,end:CLLocationCoordinate2D){
        putMarkerAggregate(icon: #imageLiteral(resourceName: "riderPick "), coordinate: start) // pickup Rider
        putMarkerAggregate(icon: #imageLiteral(resourceName: "riderDrop"), coordinate: end) // drop Rider
    }
    
    func showTripDetails(source :CLLocationCoordinate2D ,destination:CLLocationCoordinate2D ){
        
        //Marker for pick up
        let marker1 = GMSMarker(position: source)
        marker1.title = "Pick UP Location"
        marker1.appearAnimation = GMSMarkerAnimation.pop
        marker1.icon = #imageLiteral(resourceName: "on_map_starting_location")
        marker1.map = mapView
        
        //Marker for Drop
        let marker2 = GMSMarker(position: destination)
        marker2.title = "Drop Location"
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon = #imageLiteral(resourceName: "on_map_arrival_location")
        marker2.map = mapView
        
        self.showPolyLine(source: source, destination: destination)
        
    }
    //MARK:- custom method
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
       hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            //            print("wayponits****************\(distance)")
            DispatchQueue.main.sync {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- here we start  methods for shared trip
    
    /**
     Populate Data on screen and show Station on map
     */
    
    fileprivate func showSharedTripDetails(){
        var lastCoordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetails?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetails?.startLocation?.coordinates?[0] ?? 0))
        mapView.isUserInteractionEnabled = true
        if DriverManager.sharedInstance.arr_SortedShareTrips != nil{
            for modal in DriverManager.sharedInstance.arr_SortedShareTrips!{
                
                let pickup = modal[KeysForSharingPickUpViewController.COORDINATE] as! CLLocation
                showPolyLine(source: lastCoordinate, destination: pickup.coordinate)
                lastCoordinate = pickup.coordinate  //last coordinate
                if KeysForSharingPickUpViewController.DROP == modal[KeysForSharingPickUpViewController.TYPE] as? String ?? "" {
                    putMarker(with: "Drop Location", coordinate: pickup.coordinate, color: .darkGray, number: modal[KeysForSharingPickUpViewController.PICKUPNUMBER] as! Int)
                }else{
                    putMarker(with: "PickUP Location", coordinate: pickup.coordinate, color: App_Base_Color, number: modal[KeysForSharingPickUpViewController.PICKUPNUMBER] as! Int)
                }
            }
        }
    }
    
    
    fileprivate func putMarker(with title:String,coordinate:CLLocationCoordinate2D, color:UIColor, number:Int){
        let marker2 = GMSMarker(position: coordinate)
        marker2.title = title
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon = getLabelCount(number: number, color: color)
        marker2.map = mapView
    }
    
    /**
     Put marker image on map
     */
    fileprivate func putMarkerAggregate(icon : UIImage , coordinate: CLLocationCoordinate2D){
        let marker = GMSMarker(position: coordinate)
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon = icon
        marker.map = mapView
    }
    
    fileprivate func getLabelCount(number:Int , color:UIColor) -> UIImage{
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        label.textColor = .white
        label.cornerRadius = 15
        label.text = "\(number)" // set text here
        label.textAlignment = .center
        label.backgroundColor = color
        
        //grab it
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, UIScreen.main.scale)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imagePick = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imagePick ?? #imageLiteral(resourceName: "on_map_arrival_location")
    }
    
}
