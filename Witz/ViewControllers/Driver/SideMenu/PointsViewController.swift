//
//  PointsViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/14/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class PointsViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_OverallPoints: UILabel!
    @IBOutlet weak var lbl_TotalTravel: UILabel!
    @IBOutlet weak var lbl_ScoredJourney: UILabel!
    @IBOutlet weak var lbl_FiveStarJourney: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setNavigationBarItem()
        loadPointData()
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.Rating
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func loadPointData(){
        lbl_FiveStarJourney.text = "\(Int(DriverManager.sharedInstance.user_Driver.fiveStarTrips ?? 0))"
        lbl_TotalTravel.text = "\(Int(DriverManager.sharedInstance.user_Driver.totalTrips ?? 0))"
        lbl_OverallPoints.text = "\(DriverManager.sharedInstance.user_Driver.avgRating?.rounded() ?? 0)"
        lbl_ScoredJourney.text = "\(Int(DriverManager.sharedInstance.user_Driver.ratedTrips ?? 0))"
    }
}
