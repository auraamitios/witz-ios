//
//  HelpViewController.swift
//  Witz
//
//  Created by abhishek kumar on 23/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class HelpViewController: WitzSuperViewController {
    
    @IBOutlet weak var btn_Faq: UIButton!
    @IBOutlet weak var btn_Tag: UIButton!
    @IBOutlet weak var btn_Privacy: UIButton!
    @IBOutlet weak var btn_Terms: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.setNavigationBarItem()
        
        btn_Faq.isExclusiveTouch = true
        btn_Privacy.isExclusiveTouch = true
        btn_Terms.isExclusiveTouch = true
        btn_Tag.isExclusiveTouch = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Actionany
    @IBAction func frequentlyAsked(_ sender: UIButton) {
        moveToWebview(faqURL)
    }
    
    @IBAction func termsOfUse(_ sender: UIButton) {
     moveToWebview(termURL)
    }
    @IBAction func aboutPrivacy(_ sender: UIButton) {
      moveToWebview(privacyURL)
    }
    @IBAction func tagSearch(_ sender: UIButton) {
     moveToWebview(tagURL)
    }
    
    func moveToWebview(_ path:String )  {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        controller.strURL = path
        self.navigationController?.pushViewController(controller)
    }
}
