//
//  UploadDocViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/13/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

protocol UploadDocumentDelegate:NSObjectProtocol{
    func fileUpdatedNow()
}

class UploadDocViewController: WitzSuperViewController {
    
    weak var delegate : UploadDocumentDelegate?
    
    @IBOutlet weak fileprivate var btn_Submit: UIButton!
    @IBOutlet weak fileprivate var btn_SelectDoc: UIButton!
    
    @IBOutlet weak fileprivate var collection_View: UICollectionView!
    var titleStr = ""
    var parameterKey = ""
    var arr_AlreadyImg : [String]?
    fileprivate var imagePicker = UIImagePickerController()
    fileprivate var arr_ImagesDoc = [UIImage]()
    fileprivate var arr_ImagesURL = [String]()
    let objWitzConnectionManager = WitzConnectionManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationItem.titleView = nil
        self.navigationItem.title = titleStr
       
        if arr_AlreadyImg != nil {
            print(arr_AlreadyImg)
            showLoader("Fetching Documents...")
            for strUrl in arr_AlreadyImg!{
                
                if strUrl == "NA"{
                    self.hideLoader()
                    break
                }
                self.arr_ImagesURL.append(strUrl)
                let imgUrl = "\(kBaseImageURL)\(strUrl)"
                if let url = imgUrl.url{
                    KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                        DispatchQueue.main.async {
                            self.hideLoader()
                            if let thumbImage = image {
                                self.arr_ImagesDoc.append(thumbImage)
                                self.collection_View.reloadData()
                            }
                        }
                    })
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func selectDocument(_ sender: UIButton) {
        if arr_ImagesDoc.count < 5 {
            chooseImage()
        }else
        {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.UploadDocument.LimitReached , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            
        }
    }
    
    @IBAction func submitDoc(_ sender: UIButton) {
        if arr_ImagesURL.count > 0 {
            view.endEditing(true)
            guard self.checkNetworkStatus() else {
                return
            }
            var parameters = ["name":parameterKey]
            for  i in 0..<arr_ImagesURL.count{
                parameters["pic\(i+1)"] =  arr_ImagesURL[i]
            }
            
            showLoader()
            
            self.objWitzConnectionManager.makeAPICall(functionName: "driver/add_documents", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                
                self.hideLoader()
                
                if var arr_documents =   DriverManager.sharedInstance.user_Driver.arryDocuments
                {
                    
                    for (idx, element) in arr_documents.enumerated() {
                        if var dict = element as? [String:Any] {
                            for (key,value) in dict {
                                print("\(key): \(value)")
                                if String(describing: value) == self.parameterKey{
                                    dict["uploaded"] = true
                                    dict["path"] = self.arr_ImagesURL
                                    arr_documents.remove(at: idx)
                                    arr_documents.insert(dict as AnyObject, at: idx)
                                    DriverManager.sharedInstance.user_Driver.arryDocuments = arr_documents
                                    break
                                }
                            }
                        }
                    }
//                     Utility.getDriverInfoRefreshed()
//                    print(DriverManager.sharedInstance.user_Driver.arryDocuments ?? "")
                    
                }
                self.delegate?.fileUpdatedNow()
                self.navigationController?.popViewController()
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }else{
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.UploadDocument.MinOneNeeded , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
        }
    }
    
    func uploadImage(imgToUpload:UIImage){
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["pic": imgToUpload]
        showLoader()
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/upload_file", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.Form,arr_img:[imgToUpload],success: { (response) in
            
            self.hideLoader()
            
            if let url = response?["data"]["filename"].stringValue
            {
                self.arr_ImagesURL.append(url)
            }
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
    
    
    
    //Image choose
    
    func chooseImage(){
        
        let actionSheet = UIAlertController.init(title: "Choose Image", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (action) in
            
            if (!UIImagePickerController.isSourceTypeAvailable(.camera))
            {self.dismiss(animated: true, completion: nil)
            }
            else{
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let gallery  = UIAlertAction.init(title: "Gallery", style: .default) { (action) in
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel  = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
}

extension UploadDocViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]) {
        
        
        if let possibleImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            arr_ImagesDoc.append(possibleImage)
            uploadImage(imgToUpload: possibleImage)
        } else if let possibleImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            arr_ImagesDoc.append(possibleImage)
            uploadImage(imgToUpload: possibleImage)
        }
        
        self.dismiss(animated: true, completion: nil)
        self.collection_View.reloadData()
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension UploadDocViewController : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,DoumentImageCellDelegate{
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arr_ImagesDoc.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentImageCell", for: indexPath)  as! DocumentImageCell
        cell.setupCell(with: indexPath, arr_ImagesDoc[indexPath.row])
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        return CGSize(width: (width)/4, height: (width - 10)/4) // width & height are the same to make a square cell
    }
    
    // delegate to delete image
    func deleteImageCalled(_ sender: UIButton) {
        
        arr_ImagesDoc.remove(at: sender.tag)
        
        let imgUrl = arr_ImagesURL[sender.tag]
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["picPath": imgUrl]
        showLoader()
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/remove_file", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            
            self.hideLoader()
            self.collection_View.reloadData()
            
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
        
        arr_ImagesURL.remove(at: sender.tag)
        
    }
}

