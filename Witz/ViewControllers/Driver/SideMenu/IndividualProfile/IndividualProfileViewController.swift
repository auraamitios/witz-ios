//
//  IndividualProfileViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/28/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import GooglePlaces

let profileCellIdentifier = "ProfileCell"

class IndividualProfileViewController: WitzSuperViewController {
    @IBOutlet weak var imgView_Profile: UIImageView!
    
    @IBOutlet weak var scroll_View: UIScrollView!
    @IBOutlet weak var imgView_addImage: UIImageView!
    @IBOutlet weak var constraint_HeightTableView: NSLayoutConstraint!
    
    fileprivate var imagePicker = UIImagePickerController()
    var titles = ["NAME-SURNAME", "E-MAIL", "MOBILE-NUMBER", "ADDRESS"]
    var icons = ["end_trip_rider_choice", "e-mail", "phone", "create_home_address"]
    fileprivate var arrValues = ["","","",""]
    var imgToUpload : UIImage?
    
    @IBOutlet weak var tblProfile: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadDriverProfile()
        imgView_Profile.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(chooseImage))
        self.imgView_Profile.addGestureRecognizer(tap)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if DriverManager.sharedInstance.addressSelected != nil{
            arrValues[3] = DriverManager.sharedInstance.addressSelected ?? ""
            DriverManager.sharedInstance.addressSelected = nil
            tblProfile.reloadRows(at: [IndexPath.init(row: 0, section: 3)], with: .fade)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK::Custom Method
    
    public func loadDriverProfile() {
        
        let userDetails  = DriverManager.sharedInstance.user_Driver
        arrValues[0] = userDetails.driver_fullName ?? ""
        arrValues[1] = userDetails.driver_email ?? ""
        arrValues[2] = userDetails.mobile ?? ""
        arrValues[3] = userDetails.driverAddress ?? ""
        let imgUrl = "\(kBaseImageURL)\(userDetails.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
    }
}


extension IndividualProfileViewController:UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- UITableView Delegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        self.constraint_HeightTableView.constant = CGFloat((titles.count+1)*100)
        return titles.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == titles.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: bottomCellIdentifier, for: indexPath) as! ButtonCell
            cell.cellDelegate = self
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath) as! DriverProfileCell
            cell.updateCell(with: indexPath, titles[indexPath.section], icons[indexPath.section] ,valueStr: arrValues[indexPath.section])
            cell.txtValue.delegate = self
            cell.txtValue.isUserInteractionEnabled = true
            if indexPath.section == 2 {
                cell.txtValue.isUserInteractionEnabled = false
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        let itemHt:CGFloat = (UIScreen.main.bounds.width)/3.2
    //        return itemHt
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "" {
        }
    }
}

//MARK:: ImagePicker Delegate
extension IndividualProfileViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //Image choose
    
    func chooseImage(){
        
        let actionSheet = UIAlertController.init(title: "Choose Image", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (action) in
            
            if (!UIImagePickerController.isSourceTypeAvailable(.camera))
            {self.dismiss(animated: true, completion: nil)
            }
            else{
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let gallery  = UIAlertAction.init(title: "Gallery", style: .default) { (action) in
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel  = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]) {
        
        
        if let possibleImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imgView_Profile.image = possibleImage
            imgToUpload = possibleImage
        } else if let possibleImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgView_Profile.image = possibleImage
            imgToUpload = possibleImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension IndividualProfileViewController:UITextFieldDelegate{
  
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var editing = true
        if textField.tag == 3 {
            self.performSegue(withIdentifier: "add_Address", sender: nil)
            editing = false
        }else {
            editing = true
        }
        return editing
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
        switch textField.tag {
        case 0:
            arrValues[0] = textField.text ?? ""
            break
        case 1:
            arrValues[1] = textField.text ?? ""
            break
        case 2:
            arrValues[2] = textField.text ?? ""
            break
        case 3:
            arrValues[3] = textField.text ?? ""
            break
            
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    
    
}
//MARK::GMSAutocompleteViewControllerDelegate
//extension IndividualProfileViewController: GMSAutocompleteViewControllerDelegate {
//
//    func openGooglePlacePrompt(){
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//        //autocompleteController.tableCellBackgroundColor = UIColor.cyan
//        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
//        UINavigationBar.appearance().barTintColor = UIColor(red: 79.0/255, green: 89.0/255, blue: 89.0/255, alpha: 1.0)
//        UINavigationBar.appearance().tintColor = UIColor.white
//
//        //UISearchBar.appearance().barStyle = UIBarStyle.default
//        UISearchBar.appearance().barTintColor = UIColor.white
//        //UISearchBar.appearance().setTextColor = UIColor.white
//
//        let search = UISearchBar()
//        //        search.setNewcolor(color: UIColor.white)
//        for subView: UIView in search.subviews {
//            for secondLevelSubview: UIView in subView.subviews {
//                if (secondLevelSubview is UITextField) {
//                    let searchBarTextField = secondLevelSubview as? UITextField
//                    //set font color here
//                    searchBarTextField?.textColor = UIColor.white
//                    break
//                }
//            }
//        }
//        present(autocompleteController, animated: true, completion: nil)
//    }
//
//    // Handle the user's selection.
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        arrValues[3] = place.formattedAddress ?? place.name
//        self.tblProfile.reloadData()
//        dismiss(animated: true, completion: nil)
//    }
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        print("Error: ", error.localizedDescription)
//    }
//
//    // User canceled the operation.
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//}



extension IndividualProfileViewController:ButtonCellDelegate {
    func validation () -> Bool{
        
        if  arrValues[0].isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyName , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        if  arrValues[1].isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyEmail , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        if  arrValues[1].isEmail == false {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.InvalidEmail , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        
        if arrValues[2].isBlank() == true{
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyPassword , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        
        if arrValues[3].isBlank() == true{
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyAddress , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        return true
    }
    
    func saveButtonClicked(sender: UIButton) {
        if validation() == true{
            saveDriverDetail()
        }
    }
   
    //MARK::API's call
    fileprivate func saveDriverDetail() {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        
        var parameters =   ["fullName":arrValues[0],"email":arrValues[1] , "address":arrValues[3]] as [String:Any]
        
         parameters["acc_type"] = DriverManager.sharedInstance.user_Driver.acc_type ?? "individual"
        
        if  DriverManager.sharedInstance.user_Driver.acc_type?.contains("corporate") == true{
            parameters["company_name"] = DriverManager.sharedInstance.user_Driver.company_name ?? ""
        }
            
        var arrImg : [UIImage]? = nil
        if imgToUpload != nil{
            parameters["pic"] = imgToUpload
            arrImg = [UIImage]()
            arrImg?.append(imgToUpload!)
        }
        
        showLoader()
        WitzConnectionManager().makeAPICall(functionName: "driver/add_personal_information", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.Form,arr_img:arrImg,success: { (response) in
            self.hideLoader()
//            print(response)
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
                self.navigationController?.popViewController()
            }, controller: self)
            
            let driver = DriverInfo.parseDriverInfoJson(data1: response)
            DriverManager.sharedInstance.user_Driver = driver
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
}
