//
//  BankInfoViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/3/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class BankInfoViewController: WitzSuperViewController {
    
     let objWitzConnectionManager = WitzConnectionManager()
    @IBOutlet weak var txt_AccountName: UITextField!
    @IBOutlet weak var txt_AccountNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        showBankDetails()
        txt_AccountNumber.tag = 879
        txt_AccountNumber.delegate = self
    }
    
    
    func showBankDetails(){
        txt_AccountName.text =   DriverManager.sharedInstance.user_Driver.account_name
        txt_AccountNumber.text =  DriverManager.sharedInstance.user_Driver.account_number
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func save_BankDetails(_ sender: UIButton) {
        
        if validation(){
            updateBankInfo()
        }
    }
    
    fileprivate func validation() -> Bool{
        
        if txt_AccountName.text?.isBlank() == true{
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyAccountName , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        if txt_AccountNumber.text?.isBlank() == true{
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyBankAccount , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            
            return false
        }
        
        return true
    }
    

    func updateBankInfo(){
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["account_name": txt_AccountName.text!,"account_number":txt_AccountNumber.text! ]
        
        //save data locally in modal as well
        DriverManager.sharedInstance.user_Driver.account_name = txt_AccountName.text!
        DriverManager.sharedInstance.user_Driver.account_number = txt_AccountNumber.text!
        showLoader()
        self.objWitzConnectionManager.makeAPICall(functionName: "driver/add_bank_information", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.Form,arr_img:nil,success: { (response) in
            self.hideLoader()
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                DriverManager.sharedInstance.user_Driver.bankAccountIsVerified = true
                self.navigationController?.popViewController() 
            }, controller: self)
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    
    }
}
extension BankInfoViewController : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        if textField.tag == 879 {
            
            let newLength = text.count + string.count - range.length

            var limitLength = 0
                limitLength = 32
                if string.count>0 {
                    if newLength % 5 == 0 {
                        textField.text = textField.text?.appending(" ")
                    }
                }
                
            return  newLength <= limitLength
        }else {
            return true
        }
    }
}
