//
//  DriverPinAddressViewController.swift
//  Witz
//
//  Created by abhishek kumar on 17/01/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
class DriverPinAddressViewController: WitzSuperViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var btn_SetAddress: UIButton!
    @IBOutlet weak var pin_Icon: UIImageView!
    let locManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mapView.bringSubview(toFront: self.pin_Icon)
        self.btn_SetAddress.isEnabled = false
        self.btn_SetAddress.alpha = 0.5
        locManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: "Ok", completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
            return
        }
        fetchLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locManager.stopUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func fetchLocation() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        //locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        locManager.requestLocation()
        showLoader()
    }
    
    func backToProfileScreen() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is IndividualProfileViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }else if aViewController is PIFViewController{
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    @IBAction func action_SetAddress(_ sender: UIButton) {
        backToProfileScreen()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DriverPinAddressViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {}
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                self.lbl_Address.text = returnAddress
                DriverManager.sharedInstance.addressSelected = returnAddress
                self.btn_SetAddress.isEnabled = true
                self.btn_SetAddress.alpha = 1.0
            }
        })
    }
}

extension DriverPinAddressViewController:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.hideLoader()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.hideLoader()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.hideLoader()
    }
}

