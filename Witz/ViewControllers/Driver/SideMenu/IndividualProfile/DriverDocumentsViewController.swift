//
//  DriverDocumentsViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class DriverDocumentsViewController: WitzSuperViewController {
    
    var titles = [["name":"Birth Certificate","parameter":"birth_cert","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Driving License","parameter":"driver_lic","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Vehicle License","parameter":"vehicle_lic","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Criminal Record","parameter":"criminal_record","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Surge Health Report","parameter":"surge_health_report","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Compulsory Traffic Insurance","parameter":"compulsory_traffic_insurance","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Seat Insurance","parameter":"seat_insurance","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"D2 Certificates of Authority","parameter":"d2_certificate_of_authority","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"SRC Certificate","parameter":"src_certificate","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Psychotechnical Certificate","parameter":"psychotechnical_certificate","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Surucu Detail Document","parameter":"surucu_detail_document","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"Road Certificate","parameter":"road_certificate","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]],
                  ["name":"TURSAB Certificate","parameter":"TURSAB_certificate","uploaded":"false","isVerified":"false","comment":"Not uploaded yet !", "path":["NA"]]]
    
    @IBOutlet weak var tblDocuments: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        DriverManager.sharedInstance.user_Driver.isDoucmentVerified = true // assume document as verify
        realoadStatus()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    
    //    Check the all document status uploaded
    func realoadStatus(){

        if let arr_documents =  DriverManager.sharedInstance.user_Driver.arryDocuments
        {
            for (idx, element) in arr_documents.enumerated() {
                if idx == titles.count{break}
                if var dict = element as? [String:Any] {
                    if let status =  dict["uploaded"] as? Bool{
                        
                        if status == true{
                            titles[idx]["uploaded"] = "true"
                            titles[idx]["comment"] = "Doc sent, waiting approval !"
                            if let  paths = dict["path"] as? [String]  {
                                titles[idx]["path"] =  paths
                            }
                        }else{
                            titles[idx]["uploaded"] = "false"
                            DriverManager.sharedInstance.user_Driver.isDoucmentVerified = false // Still document pending
                        }
                    }
                    if let status =  dict["isVerified"] as? Bool{
                        
                        if status == true{
                            titles[idx]["isVerified"] = "true"
                            titles[idx]["comment"] = dict["comment"] as? String ?? ""
                        }else{
                            titles[idx]["isVerified"] = "false"
                            DriverManager.sharedInstance.user_Driver.isDoucmentVerified = false // Still document pending
                        }
                    }
                }
            }
            tblDocuments.reloadData()
        }
    }
}
extension DriverDocumentsViewController:UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- UITableView Delegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: DriverDocumentCell.identifier, for: indexPath) as! DriverDocumentCell
        var verified = false
        if titles[indexPath.section]["isVerified"] as! String == "true"{
            verified = true
        }
        if titles[indexPath.section]["uploaded"] as! String == "true"{
            cell.updateCell(with: indexPath, titles[indexPath.section]["name"] as! String, true, verified,titles[indexPath.section]["comment"] as! String)
        } else{
            cell.updateCell(with: indexPath, titles[indexPath.section]["name"] as! String, false, verified,titles[indexPath.section]["comment"] as! String)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "UploadDoc", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "UploadDoc" {
            let uploadVC = segue.destination as! UploadDocViewController
            let indexPath = tblDocuments.indexPathForSelectedRow
            let cell = tblDocuments.cellForRow(at: indexPath!) as! DriverDocumentCell
            uploadVC.titleStr = cell.lblDocName.text!
            uploadVC.parameterKey = titles[(indexPath?.section)!]["parameter"] as! String
            if let paths =  titles[(indexPath?.section)!]["path"] as? [String]{
                uploadVC.arr_AlreadyImg = paths
            }
            uploadVC.delegate = self
        }
    }
}
extension DriverDocumentsViewController:UploadDocumentDelegate {
    
    // Delegate method for uploadDocument
    func fileUpdatedNow(){
        realoadStatus()
    }
    
}
