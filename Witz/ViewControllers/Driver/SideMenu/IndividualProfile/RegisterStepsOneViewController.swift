//
//  RegisterStepsOneViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/13/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class RegisterStepsOneViewController: WitzSuperViewController {
    
  
    @IBOutlet weak var imgView_Status_Vehicle: UIImageView!
    @IBOutlet weak var imgView_Status_Bank: UIImageView!
    @IBOutlet weak var imgView_Status_Document: UIImageView!
    
    @IBOutlet weak var view_VehicleInfo: UIView!
    @IBOutlet weak var view_BankInfo: UIView!
    @IBOutlet weak var view_Documnet: UIView!
    
    @IBOutlet weak var constraint_Height_ViewBankInfo: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showUserDetails()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func showUserDetails(){
        
        /*******Check if account is individual or corporate*******/
        if  DriverManager.sharedInstance.user_Driver.acc_type?.contains("corporate") == true{
            view_BankInfo.isHidden = true
            constraint_Height_ViewBankInfo.constant = 0
        }else{
            view_BankInfo.isHidden = false
            constraint_Height_ViewBankInfo.constant = 50
        }
        
        
        
        /*******Check if Vehicle is verified *******/
        if DriverManager.sharedInstance.user_Driver.vehicleVerified  == true{
            imgView_Status_Vehicle.image = #imageLiteral(resourceName: "ok_my_documents")
        }else{
            imgView_Status_Vehicle.image = #imageLiteral(resourceName: "attention")
        }
        
        
        /*******Check if Bank account is verified *******/
        if DriverManager.sharedInstance.user_Driver.bankAccountIsVerified  == true{
            imgView_Status_Bank.image = #imageLiteral(resourceName: "ok_my_documents")
        }else{
            imgView_Status_Bank.image = #imageLiteral(resourceName: "attention")
        }
        
        
        /*******Check if Document is verified *******/
        if DriverManager.sharedInstance.user_Driver.isDoucmentVerified  == true{
            imgView_Status_Document.image = #imageLiteral(resourceName: "ok_my_documents")
        }else{
            var countDocUpload = 0
            if let arr_documents =  DriverManager.sharedInstance.user_Driver.arryDocuments
            {
                for (_, element) in arr_documents.enumerated() {
                    if var dict = element as? [String:Any] {
                        if let status =  dict["uploaded"] as? Bool{
                            if status == true{
                              countDocUpload = countDocUpload+1
                            }
                        }
                    }
                }
            }
            if countDocUpload  == DriverManager.sharedInstance.user_Driver.arryDocuments?.count { //Approval wait
                let img =  #imageLiteral(resourceName: "attention").withRenderingMode(.alwaysTemplate)
                imgView_Status_Document.image = img
                imgView_Status_Document.tintColor = .orange
                
            }else{ imgView_Status_Document.image = #imageLiteral(resourceName: "attention")} //Pending
        }
    }
    
    //MARK::IBOutlets actions
    
    @IBAction func action_VehicleInfo(_ sender: Any) {
        if DriverManager.sharedInstance.user_Driver.vehicleVerified  == false {
              self.performSegue(withIdentifier: "segue_VehicleDetail", sender: nil)
        }else{
            AlertHelper.displayOkAlert(title: LinkStrings.App.AppName, message: LinkStrings.Register.VehicleInProgress, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action) in
                
            }, controller: self)
        }
    }
    
    @IBAction func action_BankInfo(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segue_BankDetail", sender: nil)
    }
    
    @IBAction func action_Documents(_ sender: UIButton) {
        self.performSegue(withIdentifier: "DriverDocuments", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_VehicleDetail"{
            if let controller =  segue.destination as? VehicleInfoViewController{
                controller.isSaveButtonHidden = false
            }
        }
    }

}
