//
//  DriverSearchAddressViewController.swift
//  Witz
//
//  Created by abhishek kumar on 17/01/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GooglePlaces
class DriverSearchAddressViewController: WitzSuperViewController {

    var backBtn = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.left_barButton.isHidden = true
        self.backBtn = UIButton(type: .custom)
        self.backBtn.setImage(UIImage(named: "Round_back"), for: .normal)
        self.backBtn.addTarget(self, action: #selector(DriverSearchAddressViewController.goBack), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: self.backBtn)
        self.navigationItem.leftBarButtonItem = item1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func goBack() {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func action_PinDestination(_ sender: UIButton) {
        self.performSegue(withIdentifier: "pin_Location", sender: nil)
    }
    @IBAction func action_SearchAddress(_ sender: UIButton) {
        openGooglePlacePrompt()
    }
}

extension DriverSearchAddressViewController: GMSAutocompleteViewControllerDelegate {
    
    func openGooglePlacePrompt(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        //autocompleteController.tableCellBackgroundColor = UIColor.cyan
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red: 79.0/255, green: 89.0/255, blue: 89.0/255, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        
        //UISearchBar.appearance().barStyle = UIBarStyle.default
        UISearchBar.appearance().barTintColor = UIColor.white
        //UISearchBar.appearance().setTextColor = UIColor.white
        
        let search = UISearchBar()
        //        search.setNewcolor(color: UIColor.white)
        for subView: UIView in search.subviews {
            for secondLevelSubview: UIView in subView.subviews {
                if (secondLevelSubview is UITextField) {
                    let searchBarTextField = secondLevelSubview as? UITextField
                    //set font color here
                    searchBarTextField?.textColor = UIColor.white
                    break
                }
            }
        }
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("searched address***********\(place.formattedAddress ?? place.name)")
         DriverManager.sharedInstance.addressSelected = place.formattedAddress ?? place.name
//        self.getLatLongUsingAddress(address: place.formattedAddress ?? place.name)
        dismiss(animated: true, completion: nil)
         _=navigationController?.popViewController(animated: true)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //*********** geocode ************//
    func getLatLongUsingAddress(address:String) {
        
        GeocodeHelper.getLocationFromAddress(from: address) { (returnLoc, info) in
            print(address)
//            DriverManager.sharedInstance.addressSelected = address
        }
    }
}
