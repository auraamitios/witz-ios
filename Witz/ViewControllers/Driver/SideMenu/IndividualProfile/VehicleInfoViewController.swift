//
//  VehicleInfoViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/3/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON

let vehicleCellIdentifier = "VehicleCell"

class VehicleInfoViewController: WitzSuperViewController {
    fileprivate var arrVehicleInfoFields = ["", "", "", "", "", "", "",""]
    let objWitzConnectionManager = WitzConnectionManager()
    fileprivate var arr_VehilceType = [VehicleTypeModal]()
    fileprivate var arr_VehilceBrand = [BrandModal]()
    fileprivate var arr_VehilceModel = [VehicleModelDetailModal]()
    fileprivate var currentVehicleTypeId = ""
    
    var titles = [LinkStrings.VehilceDetail.RowTitles.VEHICLE, LinkStrings.VehilceDetail.RowTitles.BRAND, LinkStrings.VehilceDetail.RowTitles.MODEL, LinkStrings.VehilceDetail.RowTitles.PLATE, LinkStrings.VehilceDetail.RowTitles.YEAR, LinkStrings.VehilceDetail.RowTitles.COLOR, LinkStrings.VehilceDetail.RowTitles.SEAT]
    let arr_colors = [LinkStrings.VehilceDetail.Colors.Black,LinkStrings.VehilceDetail.Colors.Blue,LinkStrings.VehilceDetail.Colors.White,LinkStrings.VehilceDetail.Colors.Red,LinkStrings.VehilceDetail.Colors.Yellow,LinkStrings.VehilceDetail.Colors.Green,LinkStrings.VehilceDetail.Colors.Grey]
    
    let baseyear = 1990
    var arr_Years = [Int]()
    
    @IBOutlet weak var tblVehicle: UITableView!
    @IBOutlet var picker_View: UIPickerView!
    var activeTextField : Int?
    var isSaveButtonHidden : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showVehicleInfo()
        // Do any additional setup after loading the view.
        getALLVehicleType()
        
        var i = baseyear
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let myComponents = myCalendar?.components( .year, from:Date())
        while i <= myComponents?.year ?? baseyear {
            arr_Years.append(i)
            i = i+1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func  showVehicleInfo(){
        let user_Driver = DriverManager.sharedInstance.user_Driver
        arrVehicleInfoFields[0] = user_Driver.brand ?? ""
        arrVehicleInfoFields[1] = user_Driver.subBrand ?? ""
        arrVehicleInfoFields[2] = user_Driver.model ?? ""
        arrVehicleInfoFields[3] = user_Driver.vehicle_number ?? ""
        arrVehicleInfoFields[4] = "\(Int(user_Driver.year ?? 0))"
        arrVehicleInfoFields[5] = user_Driver.color ?? LinkStrings.VehilceDetail.Colors.Black
        arrVehicleInfoFields[6] = "\(Int(user_Driver.seating_capacity ?? 0))"
        
    }
    
    // Get all tha avialable Vehicle types
    func getALLVehicleType(){
        
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        
        self.objWitzConnectionManager.makeAPICall(functionName: "vehicles/get_all_vehicles", withInputString: ["":""], requestType: .get, withCurrentTask: .Driver_Info , isAuthorization: false,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            self.hideLoader()
            let arr = response?["data"].array ?? []
            self.arr_VehilceType.removeAll()
            if arr.count > 0 {
                for dict in arr  {
                    let modal =  VehicleTypeModal(json: dict)
                    self.arr_VehilceType.append(modal)
                }
                
                if DriverManager.sharedInstance.user_Driver.brand == nil ||  DriverManager.sharedInstance.user_Driver.brand == ""{
                    if let cell =  self.tblVehicle.cellForRow(at: IndexPath(row: 0, section: 0)) as? VehicleInfoCell{
                        cell.txtValue.text =  self.arr_VehilceType[0].name ?? ""
                    }
                    if  self.arrVehicleInfoFields[0] == ""{
                        self.arrVehicleInfoFields[0] = self.arr_VehilceType[0].name ?? ""
                    }
                    DispatchQueue.global(qos: .background).async {
                        self.getAllBrands(self.arr_VehilceType[0].id ?? "")
                    }
                }
            }
            self.picker_View.reloadAllComponents()
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
    
    // Show all the brands for selected vehicletype
    func getAllBrands(_ id: String){
        
        currentVehicleTypeId = id // store selected vehicle type
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        
        self.objWitzConnectionManager.makeAPICall(functionName: "vehicles/get_vehicle_brands?id=\(id)", withInputString: ["":""], requestType: .get, withCurrentTask: .Driver_Info , isAuthorization: false,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            self.hideLoader()
            let arr = response?["data"]["brand"].array ?? []
            self.arr_VehilceBrand.removeAll()
            if arr.count > 0 {
                for dict in arr  {
                    let modal =   BrandModal(json: dict)
                    self.arr_VehilceBrand.append(modal)
                }
                if let cell =  self.tblVehicle.cellForRow(at: IndexPath(row: 1, section: 0)) as? VehicleInfoCell{
                    cell.txtValue.text = self.arr_VehilceBrand[0].name ?? ""
                }
                if  self.arrVehicleInfoFields[1] == ""{
                    self.arrVehicleInfoFields[1] = self.arr_VehilceBrand[0].name ?? ""
                }
                DispatchQueue.global(qos: .background).async {
                    self.getAllModels(self.arr_VehilceBrand[0].id ?? "")
                }
            }
            
        }, failure: { (error) in
            self.hideLoader()
            print(error ?? "")
        })
    }
    // Show all the Model for selected Brand
    func getAllModels(_ id: String){
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        
        self.objWitzConnectionManager.makeAPICall(functionName: "vehicles/get_all_models?id=\(currentVehicleTypeId)&brandId=\(id)", withInputString: ["":""], requestType: .get, withCurrentTask: .Driver_Info , isAuthorization: false,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            self.hideLoader()
            let arr = response?["data"]["modelData"].array ?? []
            self.arr_VehilceModel.removeAll()
            if arr.count > 0 {
                
                for dict in arr  {
                    let modal =   VehicleModelDetailModal(json: dict)
                    self.arr_VehilceModel.append(modal)
                }
                if let cell =  self.tblVehicle.cellForRow(at: IndexPath(row: 2, section: 0)) as? VehicleInfoCell{
                    cell.txtValue.text = self.arr_VehilceModel[0].brand?[0].model?[0].name ?? ""
                }
                if let cell1 = self.tblVehicle.cellForRow(at: IndexPath(row: 6, section: 0)) as? VehicleInfoCell{
                    cell1.txtValue.text =  "\(Int(self.arr_VehilceModel[0].brand?[0].model?[0].seats ?? 0))"
                }
                if  self.arrVehicleInfoFields[2] == ""{
                    self.arrVehicleInfoFields[2] = self.arr_VehilceModel[0].brand?[0].model?[0].name ?? ""
                }
                self.arrVehicleInfoFields[6] = "\(Int(self.arr_VehilceModel[0].brand?[0].model?[0].seats ?? 0))"
            }
            
            
        }, failure: { (error) in
            self.hideLoader()
            print(error?.localizedDescription ?? "")
        })
    }
    
    func saveVehicleInfo(){
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        let parameters =   ["brand": arrVehicleInfoFields[0],"subBrand": arrVehicleInfoFields[1] , "color": arrVehicleInfoFields[5] ,"model":arrVehicleInfoFields[2],"vehicle_number":arrVehicleInfoFields[3] ,"year":arrVehicleInfoFields[4],"seating_capacity":arrVehicleInfoFields[6]]
        
        //save data locally in modal as well
        DriverManager.sharedInstance.user_Driver.brand = arrVehicleInfoFields[0]
        DriverManager.sharedInstance.user_Driver.subBrand = arrVehicleInfoFields[1]
        DriverManager.sharedInstance.user_Driver.model = arrVehicleInfoFields[2]
        DriverManager.sharedInstance.user_Driver.vehicle_number = arrVehicleInfoFields[3]
        DriverManager.sharedInstance.user_Driver.year = Double(arrVehicleInfoFields[4])
        DriverManager.sharedInstance.user_Driver.color = arrVehicleInfoFields[5]
        DriverManager.sharedInstance.user_Driver.seating_capacity = Double(arrVehicleInfoFields[6])
        showLoader()
        self.objWitzConnectionManager.makeAPICallUrlEncoded(functionName: "driver/add_vehicle_information", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
            
            self.hideLoader()
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                if response?["statusCode"] == 200{
                DriverManager.sharedInstance.user_Driver.vehicleVerified = true
                self.navigationController?.popViewController()
                }
            }, controller: self)
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }    
}

extension VehicleInfoViewController:UITableViewDataSource,UITableViewDelegate {
    
    //MARK:- UITableView Delegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return  1
        //        return isSaveButtonHidden == true ? titles.count : titles.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return 1
        return isSaveButtonHidden == true ? titles.count : titles.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == titles.count && isSaveButtonHidden == false{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: bottomCellIdentifier, for: indexPath) as! ButtonCell
            cell.cellDelegate = self
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: vehicleCellIdentifier, for: indexPath) as! VehicleInfoCell
            cell.updateCell(with: indexPath, titles[indexPath.row],value:arrVehicleInfoFields[indexPath.row] ,isSaveButtonHidden == true ? false : true)
            cell.txtValue.delegate = self
            print("Cell Loaded again\(indexPath.row)")
            if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 4 {
                cell.txtValue.inputView = picker_View
            }else if indexPath.row == 6{
                cell.txtValue.keyboardType = .numberPad
            }else{
                cell.txtValue.keyboardType = .alphabet
            }
                   return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension VehicleInfoViewController:ButtonCellDelegate {
    func saveButtonClicked(sender: UIButton) {
        print(arrVehicleInfoFields)
        self.saveVehicleInfo()
    }
}

extension VehicleInfoViewController:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField.tag
        print(activeTextField ?? 0)
        if activeTextField == 0 || activeTextField == 1 || activeTextField == 2 || activeTextField == 5  || activeTextField == 4{
            print("picker selected")
            textField.inputView = picker_View
            self.picker_View.reloadAllComponents()
        }else if textField.tag == 6{
            print("number pad selected")
            textField.inputView = nil
            textField.keyboardType = .numberPad
        }else{
            textField.inputView = nil
            textField.keyboardType = .alphabet
            print("Alphabet selected")
            
        }
        textField.reloadInputViews()

        return true
    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        activeTextField = textField.tag
//        print(activeTextField ?? 0)
//        if activeTextField == 0 || activeTextField == 1 || activeTextField == 2 || activeTextField == 5  || activeTextField == 4{
//            print("picker selected")
//            self.picker_View.reloadAllComponents()
//        }else if textField.tag == 6{
//            print("number pad selected")
//            textField.keyboardType = .numberPad
//        }else{
//            textField.keyboardType = .alphabet
//            print("Alphabet selected")
//        }
//    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
        switch textField.tag {
        case 0:
            arrVehicleInfoFields[0] = textField.text ?? ""
            break
        case 1:
            arrVehicleInfoFields[1] = textField.text ?? ""
            break
        case 2:
            arrVehicleInfoFields[2] = textField.text ?? ""
            break
        case 3:
            arrVehicleInfoFields[3] = textField.text ?? ""
            break
        case 4:
            arrVehicleInfoFields[4] = textField.text ?? ""
            break
        case 5:
            arrVehicleInfoFields[5] = textField.text ?? ""
            break
        case 6:
            arrVehicleInfoFields[6] = textField.text ?? ""
            break
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            return true
        }
        
        if textField.tag == 4 || textField.tag == 6 {
            
            let newLength = text.count + string.count - range.length
            
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            var limitLength = 2
            if textField.tag == 4 {
                limitLength = 4
            }else if textField.tag == 6 {
                limitLength = 2
            }
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= limitLength
        }
        return true
    }
}

extension  VehicleInfoViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch activeTextField! {
            
        case 0:
            if arr_VehilceType.count > 0{
                return arr_VehilceType.count
            }
            self.view.endEditing(true)
            return 0
        case 1:
            if arr_VehilceBrand.count > 0{
                return arr_VehilceBrand.count
            }
            self.view.endEditing(true)
            return 0
        case 2:
            if arr_VehilceModel.count > 0{
                return arr_VehilceModel[0].brand?[0].model?.count ?? 0
            }
            self.view.endEditing(true)
            return 0
            
        case 4:
            return arr_Years.count
            
        case 5:
            return arr_colors.count
            
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch activeTextField! {
            
        case 0:
            return arr_VehilceType[row].name
            
        case 1:
            return arr_VehilceBrand[row].name
            
        case 2:
            return arr_VehilceModel[0].brand?[0].model?[row].name
            
        case 4:
            return "\(arr_Years[row])"
            
        case 5:
            return arr_colors[row]
            
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if let cell =  self.tblVehicle.cellForRow(at: IndexPath(row: activeTextField!, section: 0)) as? VehicleInfoCell{
            
            switch activeTextField! {
                
            case 0:
                if arr_VehilceType.count > 0{
                    cell.txtValue.text = arr_VehilceType[row].name
                    arrVehicleInfoFields[0] = arr_VehilceType[row].name ?? ""
                    self.getAllBrands(arr_VehilceType[row].id ?? "")
                }
                
            case 1:
                if arr_VehilceBrand.count > 0{
                    
                    cell.txtValue.text = arr_VehilceBrand[row].name
                    arrVehicleInfoFields[1] = arr_VehilceBrand[row].name ?? ""
                    self.getAllModels(arr_VehilceBrand[row].id ?? "")
                }
                
            case 2:
                if arr_VehilceModel.count > 0{
                    cell.txtValue.text =  arr_VehilceModel[0].brand?[0].model?[row].name ?? ""
                    arrVehicleInfoFields[2] = arr_VehilceModel[0].brand?[0].model?[row].name ?? ""
                    arrVehicleInfoFields[6] = "\(Int(arr_VehilceModel[0].brand?[0].model?[row].seats ?? 0))"
                }
                
            case 4:
                cell.txtValue.text = "\(arr_Years[row])"
                arrVehicleInfoFields[4] =  "\(arr_Years[row])"
                
            case 5:
                cell.txtValue.text = arr_colors[row]
                arrVehicleInfoFields[5] = arr_colors[row]
                
            default:
                break
            }
            self.tblVehicle.reloadData()
        }
    }
    
    
}

