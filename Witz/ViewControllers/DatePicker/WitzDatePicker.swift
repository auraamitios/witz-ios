//
//  WitzDatePicker.swift
//  Witz
//
//  Created by Amit Tripathi on 1/22/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

protocol WitzDatePickerDelegate:class {
    func selectedDate(_ pickerView: WitzDatePicker, bookingDate:Date, selectedDate date: [Any])
    func viewRemoved(_ remove:Bool)
    func viewShouldPop(_ pop:Bool)
}
class WitzDatePicker: UIViewController {
    
    @IBOutlet weak var btn_SetLocation: UIButton!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var witzPicker: UIDatePicker!
    @IBOutlet weak var tapView: UIView!
    weak var pickerDelegate:WitzDatePickerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeView))
        self.tapView.addGestureRecognizer(tap)
        self.witzPicker.setValue(UIColor.white, forKeyPath: "textColor")
        self.witzPicker.minimumDate = Date()
        btn_SetLocation.isUserInteractionEnabled = false
        btn_SetLocation.alpha = 0.5
//        DispatchQueue.global(qos: .background).async {
//            self.getCurrentTimeZone()
//        }
    }
    
    func closeView()  {
        self.dismiss(animated: false) {
            if self.pickerDelegate != nil {
                self.pickerDelegate?.viewShouldPop(true)
            }
        }
    }
    
    fileprivate func getCurrentTimeZone() {
        guard self.checkNetworkStatus() else {return}
        let locations = AppManager.sharedInstance.locationCoordinates
        let coordinates = locations.last?.coordinate
        let url = URL(string: "http://api.geonames.org/timezoneJSON?lat=\(coordinates?.latitude ?? 0)&lng=\(coordinates?.longitude ?? 0)&username=shanker.yapits")
        if url == nil {
            return
        }
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    print(json)
                    print("timeZone-->\(json["timezoneId"] ?? "" as AnyObject)******gmtOffset-->\(json["gmtOffset"] ?? "" as AnyObject)")
                    DispatchQueue.main.async {
                        RiderManager.sharedInstance.timeZoneId = (json["timezoneId"] as? String)
                    }
                    OperationQueue.main.addOperation({
                    })
                }catch let error as NSError{
                    print(error)
                }
            }
        }).resume()
    }

    @IBAction func action_Picker(_ sender: UIDatePicker) {
        
        let finalformatter = DateFormatter()
        let dateFormatter = DateFormatter()
        let timeFormatter = DateFormatter()
        
        var selectedDate = ""
        var selectedTime = ""
        var dateF = ""
        var selectedDateTime = Date()
        var data = [Any]()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        selectedDate = dateFormatter.string(from: sender.date)
//        print("selectedDate-->\(dateFormatter.string(from: sender.date))")
        
        timeFormatter.timeStyle = DateFormatter.Style.medium
        selectedTime = timeFormatter.string(from: sender.date)
//        print("selectedTime-->\(timeFormatter.string(from: sender.date))")
        
        dateFormatter.dateFormat  = "EE"//"EE" to get short style //"EEEE" to get full style
        let dayInWeek = dateFormatter.string(from: sender.date)
        print("day-->\(dayInWeek)")
        let finalDate = "\(selectedDate)\(" ")\(dayInWeek)\("/ ")\(selectedTime)"

//        finalformatter.dateStyle = DateFormatter.Style.medium
//        finalformatter.timeStyle = DateFormatter.Style.medium
        finalformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateF = finalformatter.string(from: sender.date)
//        finalformatter.timeZone = TimeZone.current
//        finalformatter.timeZone = TimeZone(abbreviation: "IST")
        selectedDateTime = sender.date
        
        print("selectedDateTime*******-->\(selectedDateTime)")
        print("dateF*******-->\(dateF)")
        data.insert(finalDate as String, at: 0)
        data.insert(dateF as String, at: 1)
        data.insert(selectedDateTime as Date, at: 2)
        
//        data.insert(sender.date, at: 3)
        
        let dFor = DateFormatter()
        dFor.dateFormat = "yyyy-MM-dd"
        let ssDate = dFor.string(from: sender.date)
        data.insert(ssDate as String, at: 3)
        print(ssDate)
        print(data )
        
        if checkMinReservationTime(dateString: dateF) == true{
        if self.pickerDelegate != nil {
            self.pickerDelegate?.selectedDate(self, bookingDate: sender.date, selectedDate: data)
            }}
    }
    @IBAction func action_SetLocation(_ sender: UIButton) {
        self.dismiss(animated: false) {
            if self.pickerDelegate != nil {
                self.pickerDelegate?.viewRemoved(true)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK::custom methods
    func checkMinReservationTime(dateString : String) -> Bool {
        let time = RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.minimumReservationTime ?? 0.0
        print("time-\(time * 60)\("totalTime-->")\(time * 60)")
        print(dateString)
        let dateFor = DateFormatter()
        dateFor.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let selDate = dateFor.date(from: dateString)
        let todayDate = Date()
        let addReserveTime = TimeInterval(time * 60)
        if (selDate?.minutesSince(todayDate) ?? 66) < addReserveTime {
            btn_SetLocation.isUserInteractionEnabled = false
            btn_SetLocation.alpha = 0.5
                AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: "Booking in past time is not allowed", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                })
            return false
        }
        btn_SetLocation.isUserInteractionEnabled = true
        btn_SetLocation.alpha = 1.0
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
