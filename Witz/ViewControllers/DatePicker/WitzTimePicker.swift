//
//  WitzTimePicker.swift
//  Witz
//
//  Created by Amit Tripathi on 2/23/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

protocol WitzTimePickerDelegate:class {
    func selectedTime(_ pickerView: WitzTimePicker, bookingTime:Date, rideTime:String)
    func viewRemoved(_ remove:Bool)
    func viewShouldPop(_ pop:Bool)
}
class WitzTimePicker: UIViewController {

    @IBOutlet weak var time_Picker: UIDatePicker!
    @IBOutlet weak var view_Overlay: UIView!
    @IBOutlet weak var lbl_time: UILabel!
    weak var pickerDelegate:WitzTimePickerDelegate?
    var finalTime:Date?
    var finaltimeStr:String?
    var is12HrsFormat:Bool?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeView))
        self.view_Overlay.addGestureRecognizer(tap)
        self.time_Picker.setValue(UIColor.white, forKeyPath: "textColor")
//        self.time_Picker.minimumDate = Date()
        
        let locale = NSLocale.current
        let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
        if formatter.contains("a") {
            print("*********phone is set to 12 hours**********")
            is12HrsFormat = true
        } else {
            print("*********phone is set to 24 hours**********")
            is12HrsFormat = false
        }
        
        showCurrentTime()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showCurrentTime() {
        let date = self.time_Picker.date
        finalTime = date
        if is12HrsFormat ?? false {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            self.lbl_time.text = dateFormatter.string(from: date)
            finaltimeStr = self.lbl_time.text
        }else {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            self.lbl_time.text = dateFormatter.string(from: date)
             finaltimeStr = self.lbl_time.text
        }
    }
    
    func closeView(send:Bool)  {
        self.dismiss(animated: false) {
//            self.pickerDelegate?.viewShouldPop(true)
            if send {
                self.sendSelectedTime()
            }
        }
    }
    
    func sendSelectedTime() {
        if self.pickerDelegate != nil {
//            self.pickerDelegate?.selectedTime(self, bookingTime: finalTime ?? Date())

            self.pickerDelegate?.selectedTime(self, bookingTime: finalTime ?? Date(), rideTime: finaltimeStr ?? "09:00")
        }
    }
    
    @IBAction func change_TimePicker(_ sender: UIDatePicker) {
        
        if is12HrsFormat ?? false {
            finalTime = sender.date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            let date = dateFormatter.string(from: sender.date)
            print(date)
            print(Utility.convertTime12HrsTo24Hrs(selectedDate: date))
            self.lbl_time.text = date
            finaltimeStr = Utility.convertTime12HrsTo24Hrs(selectedDate: date)
            
        }else {
            finalTime = sender.date
            print(sender.date.timeString())
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            let date = dateFormatter.date(from: sender.date.timeString())
            
            // To convert the date into an HH:mm format
            dateFormatter.dateFormat = "HH:mm"
            let dateString = dateFormatter.string(from: date!)
            self.lbl_time.text = dateString
            finaltimeStr = dateString
            print(dateString)
        }
        
    }
    @IBAction func action_Cancel(_ sender: UIButton) {
        closeView(send: false)
    }
    
    @IBAction func action_Ok(_ sender: UIButton) {
        closeView(send: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
