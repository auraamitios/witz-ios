//
//  WitzSuperViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 9/5/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class WitzSuperViewController: UIViewController {

    var left_barButton:UIButton!
    var right_barButton:UIButton!
    var bgLogoImageView:UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.init(hexString: "596161")
        
        let logo = UIImage(named: "witz_logo_status-bar")
        let titleView = UIImageView(image:logo)
        self.navigationItem.titleView = titleView
        let imageName = "witz_logo_bg"
        let image = UIImage(named: imageName)
        bgLogoImageView = UIImageView(image: image!)
        bgLogoImageView.frame = CGRect(x: 0, y: 80, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        //Utility.addEqualConstraints(for: bgLogoImageView, inSuperView: self.view)
        self.view.addSubview(bgLogoImageView)
        self.view.sendSubview(toBack: bgLogoImageView)
        
        addLeftNavigationBarButton()
        UIBarButtonItem.appearance().tintColor = UIColor.white
        //addRightNavigationBarButton()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:: Private API's
    fileprivate func addLeftNavigationBarButton() {
        
        self.left_barButton = UIButton(type: .custom)
        self.left_barButton.setImage(UIImage(named: "Round_back"), for: .normal)
        //self.left_barButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        self.left_barButton.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
//        self.left_barButton.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        self.left_barButton.addTarget(self, action: #selector(WitzSuperViewController.pushBack), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: self.left_barButton)
        self.navigationItem.leftBarButtonItem = item1
    }
    
    fileprivate func addRightNavigationBarButton() {
        
        self.right_barButton = UIButton(type: .custom)
        self.right_barButton.setImage(UIImage(named: "Round_back"), for: .normal)
        self.right_barButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        let item1 = UIBarButtonItem(customView: self.right_barButton)
        self.navigationItem.rightBarButtonItem = item1
    }

    func pushBack() {
        _=navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
