//
//  RiderOTPVerification.swift
//  Witz
//
//  Created by Malvinder Singh on 26/09/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class RiderOTPVerificationController: WitzSuperViewController {
    
    @IBOutlet fileprivate weak var firstTxtNum: SquareTextField!
    @IBOutlet fileprivate weak var secondTxtNum: SquareTextField!
    @IBOutlet fileprivate weak var thirdTxtNum: SquareTextField!
    @IBOutlet fileprivate weak var fourthTxtNum: SquareTextField!
    @IBOutlet fileprivate weak var btnConfirmOTP: UIButton!
    
    fileprivate let objWitzConnectionManager = RiderConnectionManager()
    
    var strMobile : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.objWitzConnectionManager.delegate = self
        toggleCreateBtnState(false)
        //        self.performSegue(withIdentifier: "RiderRegister", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func OTPValidation() -> Bool {
        if (firstTxtNum.text?.isEmpty)! || (secondTxtNum.text?.isEmpty)! || (thirdTxtNum.text?.isEmpty)! || (fourthTxtNum.text?.isEmpty)! {
            return false
        }
        return true
    }
    
    func createOTPString() -> String {
        let appendString = firstTxtNum.text! + secondTxtNum.text! + thirdTxtNum.text! + fourthTxtNum.text!
        print ("sending Test OTP >>>> \(appendString)")
        return appendString
    }
    
    func toggleCreateBtnState(_ enable:Bool) {
        if enable {
            self.btnConfirmOTP.alpha = 1.0
            self.btnConfirmOTP.isEnabled = true
        }else {
            self.btnConfirmOTP.alpha = 0.5
            self.btnConfirmOTP.isEnabled = false 
            
        }
    }
    
    // send pin again if not received
    @IBAction func sendPinAgain(_ sender: Any) {
        self.reSendOtp()
    }
    
    // Verify OTP for user
    @IBAction func checkRiderOTP(_ sender: Any) {
        // check if OTP is enetered
        guard OTPValidation() else {
            return
        }
        // send data for TP Verification
        OTPVerification(createOTPString())
    }
    
    @IBAction func actionTxtField(_ sender: SquareTextField) {
        
        switch sender.tag {
        case 1:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                secondTxtNum.becomeFirstResponder()
            }
            break
            
        case 2:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                thirdTxtNum.becomeFirstResponder()
            }else{
                firstTxtNum.becomeFirstResponder()
            }
            break
            
        case 3:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                fourthTxtNum.becomeFirstResponder()
            }else{
                secondTxtNum.becomeFirstResponder()
            }
            break
            
        case 4:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                fourthTxtNum.becomeFirstResponder()
            }else{
                thirdTxtNum.becomeFirstResponder()
            }
            break
            
        default:
            break
        }
    }
    
    fileprivate func OTPVerification(_ otpCode: String) {
        view.endEditing(true)
        
        guard self.checkNetworkStatus() else {
            return
        }
        self.showLoader()
        let numMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        
        let params: Parameters = ["mobile": numMobile, "type": "normal", "code": otpCode]
        
        self.objWitzConnectionManager.APICall(functionName: "rider/confirm_mobile_verification", withInputString: params, requestType: .post, withCurrentTask: .CONFIRM_RIDER_MOBILE_VERIFICATION, isAuthorization: false)
    }
    
    fileprivate func reSendOtp(){
        
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let parameters = ["mobile":strMobile ?? "","type":"normal"]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/mobile_verification", withInputString: parameters, requestType: .post, isAuthorization: false, success: { (response) in
            self.hideLoader()
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:"Code Sent" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
            }, controller: self)
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
}

extension RiderOTPVerificationController:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var pinFlag = true
        switch textField.tag {
        case 1:
            pinFlag = true
            break
            
        case 2:
            if self.firstTxtNum.text == "" {
                pinFlag = false
            }
            break
            
        case 3:
            if self.firstTxtNum.text == "" || self.secondTxtNum.text == "" {
                pinFlag = false
            }
            break
            
        case 4:
            if self.firstTxtNum.text == "" || self.secondTxtNum.text == "" || self.thirdTxtNum.text == "" {
                pinFlag = false
            }
            break
            
        default:
            break
        }
        
        return pinFlag
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        
        guard !trimmed.isEmpty else {
            textField.text = ""
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        let limitLength = 1
        let newLength = text.count + string.count - range.length
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        toggleCreateBtnState(false)
        if textField.tag == 4 {
            if newLength > 0 {toggleCreateBtnState(true)}
            else {toggleCreateBtnState(false)}
        }
        
        return allowedCharacters.isSuperset(of: characterSet) && newLength <= limitLength
    }
}

extension RiderOTPVerificationController:RiderConnectionManagerDelegate {
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        //        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        self.hideLoader()
        
        print("Rider CONFIRM OTP: \(sender)")
        
        switch serviceType {
        case .CONFIRM_RIDER_MOBILE_VERIFICATION:
            let result = sender as! JSON
            let actionHandler = { (action:UIAlertAction!) -> Void in
                if result["statusCode"] == 200{
                    self.performSegue(withIdentifier: "RiderRegister", sender: nil)
                    print ("SUCCESSS")
                }
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: result["message"].stringValue, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            break
            
        default:
            break
        }
    }
    
    func didFailWithError(sender: AnyObject) {
        //        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        self.hideLoader()
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action: UIAlertAction!) -> Void in }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: error.response_message!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
        else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action: UIAlertAction!) -> Void in }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
        else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}
