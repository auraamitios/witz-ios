//
//  EnterPasswordViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import KeychainSwift
import SwiftyJSON
class EnterPasswordViewController: WitzSuperViewController {

    @IBOutlet weak var txtPassword: TextFieldInsets!
    let objWitzConnMgr = RiderConnectionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.objWitzConnMgr.delegate = self
        txtPassword.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK::API's call
    fileprivate func riderLogin (_ password:String) {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
//        ActivityIndicatorUtil.enableActivityIndicator(self.view, status: nil, mask: SVProgressHUDMaskType.custom, maskColor: SpinnerBGColor, style: SVProgressHUDStyle.dark)
        self.showLoader()
        let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        
        var accessToken = ""
        if (UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DeviceToken) != nil) {
            accessToken = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DeviceToken) as! String
        }else {
            let keychain = KeychainSwift()
            accessToken = keychain.get(LinkStrings.KeychainKeys.UUID)!
        }
        let locations = AppManager.sharedInstance.locationCoordinates
        print(locations)
        let coordinates = locations.last?.coordinate
        showLoader()
        let params: Parameters = ["mobile":strMobile,"deviceType":"0", "password":password, "lat":coordinates?.latitude ?? 0.0,"lng":coordinates?.longitude ?? 0.0, "deviceToken":accessToken]
        print(params)
        self.objWitzConnMgr.APICall(functionName: "rider/login", withInputString: params, requestType: .post, withCurrentTask: .LOGIN_RIDER, isAuthorization: false)
        
    }

    @IBAction func action_Login(_ sender: Any) {
        guard !self.txtPassword.isEmpty else {
            return
        }
        riderLogin(self.txtPassword.text!)
    }
    @IBAction func action_ForgotPassword(_ sender: Any) {
        self.performSegue(withIdentifier: "forgot_Password", sender: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension EnterPasswordViewController:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
}

extension EnterPasswordViewController:RiderConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
//        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        self.hideLoader()
        print("data\(sender)")
        switch serviceType {
        case .LOGIN_RIDER:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                Utility.riderSideMenuSetUp()
            }else {
                let actionHandler = { (action:UIAlertAction!) -> Void in
                }
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:result["message"].stringValue , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            }
            break
            
        default:
            break
        }
    }
    func didFailWithError(sender: AnyObject) {
//        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        self.hideLoader()
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}
