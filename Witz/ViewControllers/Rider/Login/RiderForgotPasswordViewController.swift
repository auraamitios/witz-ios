//
//  RiderForgotPasswordViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/11/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class RiderForgotPasswordViewController: WitzSuperViewController {

    @IBOutlet weak var txtFirst: SquareTextField!
    @IBOutlet weak var txtTwo: SquareTextField!
    @IBOutlet weak var txtThree: SquareTextField!
    @IBOutlet weak var txtFour: SquareTextField!
    @IBOutlet weak var btnOTP: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        forgotPassword()
    }
    
    fileprivate func forgotPassword () {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/forgot_password", withInputString: ["mobile":strMobile,"type":"forgot"], requestType: .post, isAuthorization: false, success: { (response) in
            print(response as Any)
            DispatchQueue.main.async {
                self.hideLoader()
                if response?["statusCode"] == 200 {
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }

    @IBAction func action_OTPFields(_ sender: SquareTextField) {
        switch sender.tag {
        case 1:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtTwo.becomeFirstResponder()
            }
            break
        case 2:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtThree.becomeFirstResponder()
            }else{
                txtFirst.becomeFirstResponder()
            }
            break
        case 3:
            if (sender.text?.length)!>0 && sender.text != " " {
                txtFour.becomeFirstResponder()
            }else{
                txtTwo.becomeFirstResponder()
            }
            break
        case 4:
            if (sender.text?.length)! > 0 && sender.text != " " {
                txtFour.resignFirstResponder()
            }else{
                txtThree.becomeFirstResponder()
            }
            break
        default:
            break
        }
    }
    
    @IBAction func action_OTPBtn(_ sender: UIButton) {
        
        if OTPValidation() == true{
            view.endEditing(true)
            guard self.checkNetworkStatus() else {return}
            showLoader()
            let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/confirm_mobile_verification", withInputString: ["mobile":strMobile,"type":"forgot", "code":createOTPString()], requestType: .post, isAuthorization: false, success: { (response) in
                
                self.hideLoader()
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    if response?["statusCode"] == 200 {
                        self.performSegue(withIdentifier: "create_Password", sender: nil)
                    }
                }, controller: self)
            }) { (error) in
                self.hideLoader()
            }
        }
    }
    
    func OTPValidation()->Bool {
        if (txtFirst.text?.isEmpty)! || (txtTwo.text?.isEmpty)! || (txtThree.text?.isEmpty)! || (txtFour.text?.isEmpty)! {
            return false
        }else {
            return true
        }
    }
    
    func createOTPString()->String {
        let appendString = txtFirst.text! + txtTwo.text! + txtThree.text! + txtFour.text!
        print("OTP string>>>>>>\(appendString)")
        return appendString
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RiderForgotPasswordViewController:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        var pinFlag = true
        switch textField.tag {
        case 1:
            pinFlag = true
            break
        case 2:
            if self.txtFirst.text == "" {
                pinFlag = false
            }
            break
        case 3:
            if self.txtFirst.text == "" || self.txtTwo.text == "" {
                pinFlag = false
            }
            break
        case 4:
            if self.txtFirst.text == "" || self.txtTwo.text == "" || self.txtThree.text == "" {
                pinFlag = false
            }
            break
        default:
            break
        }
        return pinFlag
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
        switch textField.tag {
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            break
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            return true
        }
        let limitLength = 1
        let newLength = text.count + string.count - range.length
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        
        return allowedCharacters.isSuperset(of: characterSet) && newLength <= limitLength
    }
}
