//
//  TransferDashboardViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/6/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
enum RideTypes : Int {
    case Instant = 0
    case Reservation
    case Aggregate
}
let dashCellIdentifier = "DashCell"
class TransferDashboardViewController: WitzSuperViewController {

    var typeSelected : RideTypes?
    var titles = ["INSTANT JOURNEY", "RESERVATION", "SERVICE REQUEST"]
    var icons = ["trip_where_yellow", "reservation_to_where_yellow", "menu_page_aggregate"]
    
    @IBOutlet weak var tblDashboard: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.setNavigationBarItem()
        
       DispatchQueue.global(qos: .background).async {
          self.getRiderInfo()
        Utility.getLoggedRiderDetail({ (complete) in
        })
          Utility.getRiderNotificationBadgeCount()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppManager.sharedInstance.loggedUserRider = true
        RiderManager.sharedInstance.comingFromReservation = false
        if typeSelected != nil {
            switch (typeSelected!){
            case RideTypes.Instant:
                 self.performSegue(withIdentifier: "Instant", sender: nil)
                 typeSelected = nil
                break
            case RideTypes.Reservation:
                RiderManager.sharedInstance.comingFromReservation = true
                self.performSegue(withIdentifier: "reservation_Ride", sender: nil)
                typeSelected = nil
                break
            case RideTypes.Aggregate:
                let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.RiderAggregate, bundle: nil)
                let riderVC : AggregateLandingViewController = storyBoard.instantiateViewController(withIdentifier: "AggregateLandingViewController") as! AggregateLandingViewController
                self.navigationController?.pushViewController(riderVC, animated: true)
                typeSelected = nil
                break
            }
        }
        self.getTripsCounts()
    }

    //MARK::API's call
    fileprivate func getTripsCounts() {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/getCounts", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            print(response ?? "" )
            let riderCount = RiderCountModal.init(json: response?["data"] ?? "")
            RiderManager.sharedInstance.allCounts = riderCount
        }) { (error ) in
            
        }
     }
    
    public func getRiderInfo(){
        guard self.checkNetworkStatus() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/accesstokenlogin", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            print(response ?? "" )
            
            let rider = RiderInfo.parseRiderInfoJson(data1: response)
            RiderManager.sharedInstance.riderInfo = rider
            //**** TODO::Dec20******
            let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
            RiderManager.sharedInstance.riderLoginData = riderData
            
        }) { (error ) in
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TransferDashboardViewController:UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- UITableView Delegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return titles.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: dashCellIdentifier, for: indexPath) as! DashBoardCell
        cell.updateCell(with: indexPath, titles[indexPath.section], icons[indexPath.section])
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            self.performSegue(withIdentifier: "Instant", sender: nil)
            break
        case 1:
            RiderManager.sharedInstance.comingFromReservation = true
            self.performSegue(withIdentifier: "reservation_Ride", sender: nil)
            break
        case 2:
            let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.RiderAggregate, bundle: nil)
            let riderVC : AggregateLandingViewController = storyBoard.instantiateViewController(withIdentifier: "AggregateLandingViewController") as! AggregateLandingViewController
            self.navigationController?.pushViewController(riderVC, animated: true)
           
            break
        default:
            break
        }
    }
}

extension TransferDashboardViewController:RiderConnectionManagerDelegate {
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
//        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        self.hideLoader()
        
        
        print("rider Data \(sender)")
//        switch serviceType {
//        case .RIDER_MOBILE_VERIFICATION:
//            let result = sender as! JSON
//            print (result)
//            if result["statusCode"] == 400 {
//                let data = result["data"].dictionary
//                if data?["isUserAvail"] == true {
//                    self.performSegue(withIdentifier: "EnterPassword", sender: nil)
//                }
//                else {}
//            }
//            else {
//                print ("Navigating to OTP Screen")
//                self.performSegue(withIdentifier: "RiderOTPVC", sender: nil)
//            }
//            break
//
//        default:
//            break
//
//        }
    }
    
    func didFailWithError(sender: AnyObject) {
//        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        self.hideLoader()
        
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}

