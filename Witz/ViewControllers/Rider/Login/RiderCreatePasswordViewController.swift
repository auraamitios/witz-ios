//
//  RiderCreatePasswordViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/11/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class RiderCreatePasswordViewController: WitzSuperViewController {

    @IBOutlet weak var txtPassword: TextFieldInsets!
    @IBOutlet weak var txtRePassword: TextFieldInsets!
    @IBOutlet weak var btnCreatePassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        toggleCreateBtnState(false)

        // Do any additional setup after loading the view.
        txtPassword.becomeFirstResponder()
    }

    func toggleCreateBtnState(_ enable:Bool) {
        if enable && (txtPassword.text?.length ?? 0) > 5{
            self.btnCreatePassword.alpha = 1.0
            self.btnCreatePassword.isUserInteractionEnabled = true
        }else {
            self.btnCreatePassword.alpha = 0.5
            self.btnCreatePassword.isUserInteractionEnabled = false
        }
    }
    
    func popToRiderLoginScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is EnterPasswordViewController {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
    }
    func popToRiderProfileScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is RiderProfileViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    //MARK::API's call
    fileprivate func resetPassword (_ password:String) {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        let strMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/reset_password", withInputString: ["mobile":strMobile,"password":password], requestType: .post, isAuthorization: false, success: { (response) in
            print(response as Any)
            DispatchQueue.main.async {
                self.hideLoader()
                if response?["statusCode"] == 200 {
                    if RiderManager.sharedInstance.changePasswordFromProfile ?? false {
                        self.popToRiderProfileScreen()
                    }else {
                        self.popToRiderLoginScreen()
                    }
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
        
    }
    
    func validationCheck()->Bool {
        if (txtPassword.text?.isEmpty)! || (txtRePassword.text?.isEmpty)! {
            return false
        }
        return true
    }
    func validateReTypePassword()->Bool {
        if txtPassword.text != txtRePassword.text {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.SignIn.RePassword, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: actionHandler, controller: self)
            return false
        }
        return true
    }
    
    //MARK::IBOutlets actions
    
    @IBAction func action_CreatePassword(_ sender: Any) {
        guard validationCheck() else {return}
        guard validateReTypePassword()else {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.SignIn.RePassword, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: actionHandler, controller: self)
            return
        }
        resetPassword(txtPassword.text ?? "") 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RiderCreatePasswordViewController:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            return true
        }
        if textField.tag == 2 {
            let newLength = text.count + string.count - range.length
            if newLength < 6 {
                toggleCreateBtnState(false)
            }else {
                toggleCreateBtnState(true)
            }
        }
        return true
    }
}
