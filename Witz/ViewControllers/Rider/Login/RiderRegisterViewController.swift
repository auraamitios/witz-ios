//
//  RiderRegisterViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/6/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON
import KeychainSwift

let riderCellIdentifier = "RiderCell"
class RiderRegisterViewController: WitzSuperViewController {
    
    
    var titles = ["NAME-SURNAME", "E-MAIL", "PASSWORD", "REFERAL CODE"]
    var icons = ["end_trip_rider_choice", "e-mail", "password_yellow", "promotion_code"]
    var arrValue = ["","","",""]
    var imgToUpload : UIImage?
    fileprivate var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var constraint_HeightTableView: NSLayoutConstraint!
    @IBOutlet weak var tblRider: UITableView!
    @IBOutlet weak var riderProfileImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        
        riderProfileImg.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(chooseImage))
        self.riderProfileImg.addGestureRecognizer(tap)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_ProfileBtn(_ sender: UIButton) {
        view.endEditing(true)
        chooseImage()
    }
}

//MARK:: ImagePicker Delegate
extension RiderRegisterViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //Image choose
    
    func chooseImage(){
        self.view.endEditing(true)
        let actionSheet = UIAlertController.init(title: "Choose Image", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (action) in
            
            if (!UIImagePickerController.isSourceTypeAvailable(.camera))
            {self.dismiss(animated: true, completion: nil)
            }
            else{
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let gallery  = UIAlertAction.init(title: "Gallery", style: .default) { (action) in
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel  = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]) {
        
        
        if let possibleImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            riderProfileImg.image = possibleImage
            imgToUpload = possibleImage
        } else if let possibleImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            riderProfileImg.image = possibleImage
            imgToUpload = possibleImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension RiderRegisterViewController:UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- UITableView Delegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        constraint_HeightTableView.constant = CGFloat((titles.count+1)*80)
        return titles.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == titles.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: bottomCellIdentifier, for: indexPath) as! ButtonCell
            cell.cellDelegate = self
            cell.setButtonTitle(title: "SIGN UP")
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: riderCellIdentifier, for: indexPath) as! RiderCell
            cell.updateCell(with: indexPath, titles[indexPath.section], icons[indexPath.section],valueStr:arrValue[indexPath.section])
            cell.txtValue.delegate = self
            cell.txtValue.isSecureTextEntry = false
            if indexPath.section == 2{
                cell.txtValue.isSecureTextEntry = true
                
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 4{
            return 30
        }
        return 15
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension RiderRegisterViewController:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 3{
            self.moveToReferal()
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
        switch textField.tag {
        case 0:
            arrValue[0] = textField.text ?? ""
            break
        case 1:
            arrValue[1] = textField.text ?? ""
            break
        case 2:
            arrValue[2] = textField.text ?? ""
            break
        case 3:
            arrValue[3] = textField.text ?? ""
            break
            
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func moveToReferal(){
        let storyBoardRider : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let controller = storyBoardRider.instantiateViewController(withIdentifier: "RiderPromotionCodeViewController") as! RiderPromotionCodeViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller)
    }
}

extension RiderRegisterViewController:ButtonCellDelegate {
    
    func validation () -> Bool{
        //        if imgToUpload == nil {
        //            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EMptyProfileImage  , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
        //
        //            }, controller: self)
        //            return false
        //        }
        if  arrValue[0].isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyName , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        if  arrValue[1].isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyEmail , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        if  arrValue[1].isEmail == false {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.InvalidEmail , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        
        if arrValue[2].isBlank() == true{
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyPassword , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        return true
    }
    
    func saveButtonClicked(sender: UIButton) {
        if validation() == true{
            
            let numMobile = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.MobileNo) as! String
            
            let locations = AppManager.sharedInstance.locationCoordinates
            print(locations)
            let coordinates = locations.last?.coordinate
            
            var accessToken = ""
            if (UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DeviceToken) != nil) {
                accessToken = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DeviceToken) as! String
            }else {
                let keychain = KeychainSwift()
                accessToken = keychain.get(LinkStrings.KeychainKeys.UUID)!
            }
            
            view.endEditing(true)
            guard self.checkNetworkStatus() else {
                return
            }
            var parameters =   ["fullName": arrValue[0],"email": arrValue[1] , "password": arrValue[2] ,"mobile":numMobile,"deviceToken":accessToken,"lat":"\(coordinates?.latitude ?? 0.0)","lng":"\(coordinates?.longitude ?? 0.0)" , "deviceType":"0"] as [String : Any]
            
            var arrimg : [UIImage]?
            if imgToUpload != nil {
                parameters["pic"] = imgToUpload!
                arrimg = [imgToUpload!]
            }
            
            
            showLoader()
            WitzConnectionManager().makeAPICall(functionName: "rider/create", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: false,headerType:.Form,arr_img:arrimg,success: { (response) in
                
                self.hideLoader()
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    if response?["statusCode"] == 201 {
                        let riderData = RiderInfo.parseRiderInfoJson(data1: response)
                        RiderManager.sharedInstance.riderInfo = riderData
                        
                        //**** TODO::Dec20******
                        let riderLoginData = RiderLoginData.init(json: response?["data"] ?? ["":""])
                        RiderManager.sharedInstance.riderLoginData = riderLoginData
                        
                        if let token =   response?["data"]["accessToken"].string{
                            UserDefaultManager.sharedManager.addValue(object: token as AnyObject, key: LinkStrings.UserDefaultKeys.RiderAccessToken)
                        }
                        
                        AppManager.sharedInstance.loggedUserRider = true
                        Utility.riderSideMenuSetUp()
                    }
                }, controller: self)
                
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }
    }
}

extension RiderRegisterViewController : RiderPromotionCodeDelegate{
    
    func referCodeRecieved(_ str: String) {
        arrValue[3] = str
        self.tblRider.reloadSections(IndexSet.init(integer: 3), with: .fade)
    }
}
