//
//  AggregateJourneyViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 12/28/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import Cosmos
import Kingfisher

struct dictKey {
  static let pick = "Pick"
   static let drop = "Drop"
}

class AggregateJourneyViewController: WitzSuperViewController {
    
    @IBOutlet weak var view_DriverDetail: UIView!
    @IBOutlet weak var imgView_Driver: UIImageView!
    @IBOutlet weak var view_RatingDriver: CosmosView!
    @IBOutlet weak var lbl_NameDriver: UILabel!
    @IBOutlet weak var lbl_Brand: UILabel!
    @IBOutlet weak var lbl_VehicleName: UILabel!
    
    @IBOutlet weak var constraint_HeightViewDriverDetail: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_HeightBtnCancel: NSLayoutConstraint!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_Days: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_PickAddress: UILabel!
    @IBOutlet weak var lbl_DropAddress: UILabel!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Route: UIButton!
    
    var cancelBtnShown:Bool?
    var tripData:Trips?
    
    fileprivate var dictMarkAdmin = [String:LocationsAggregate]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.btn_Cancel.isHidden = cancelBtnShown ?? true
        if cancelBtnShown == true{
            constraint_HeightBtnCancel.constant = 0
        }else{
            constraint_HeightViewDriverDetail.constant = 0
            self.view_DriverDetail.isHidden = true
        }
        
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = false
        mapView.shouldHideToolbarPlaceholder = true
        
        self.mapView.bringSubview(toFront: self.btn_Route)
        
        showTripDetails()
        addPickLocationIcon()
        addDropLocationIcon()
        getPathPoints()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mapView = nil
    }
    
    func showTripDetails() {
//        if tripData?.aggregateTripType == 1 {
            self.lbl_PickAddress.text = tripData?.startAddress ?? ""
            self.lbl_DropAddress.text = tripData?.endAddress ?? ""
//        }else {
//            
//            self.lbl_PickAddress.text = tripData?.endAddress ?? ""
//            self.lbl_DropAddress.text = tripData?.startAddress ?? ""
//        }
        
        
        let timeData = tripData?.workingTime![0]
        self.lbl_Time.text = "Time:\(timeData?.time?.arivalTime ?? "") - \(timeData?.time?.departureTime ?? "")"
        self.lbl_Days.text = Utility.aggregateDays(data: tripData!)
        getRideDetailAggregate(trip: tripData?.id ?? "")
        
    }
    
    fileprivate func addPickLocationIcon() {
        let pickPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripData?.startLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripData?.startLocation?.coordinates![0] ?? 0)))
        let position = CLLocationCoordinate2D(latitude: pickPosition.latitude, longitude:pickPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        
//        if tripData?.aggregateTripType == 1 {
            marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
//        }else {
//            marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
//        }
        
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    fileprivate func addDropLocationIcon () {
        let dropPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripData?.endLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripData?.endLocation?.coordinates![0] ?? 0)))
        let position = CLLocationCoordinate2D(latitude: dropPosition.latitude, longitude:dropPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        
//        if tripData?.aggregateTripType == 1 {
            marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
//        }else {
//            marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
//        }
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    fileprivate func getPathPoints() {
        let pickPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripData?.startLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripData?.startLocation?.coordinates![0] ?? 0)))
        let dropPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripData?.endLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripData?.endLocation?.coordinates![0] ?? 0)))
        
        self.mapView.animate(to: GMSCameraPosition(target: pickPosition, zoom: 11.5, bearing: 0, viewingAngle: 45))
        hitsLimit_Google = 5
        
        GooglePathHelper.getPathPolyPoints(from: pickPosition, destination: dropPosition) { (returnPath, distance) in
            print(distance)
            self.showPath(polyStr: returnPath)
        }
    }
    
    fileprivate func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = App_Base_Color
        polyline.map = self.mapView
        DispatchQueue.main.async {
            let bounds = GMSCoordinateBounds(path: path!)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
            //            self.mapView.animate(toZoom: 12.0)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- get Ride Detail from server
    public func getRideDetailAggregate(trip:String){
        //RiderManager.sharedInstance.tripIDForRider ?? ""
        guard self.checkNetworkStatus() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_trip_details", withInputString: ["tripId":trip], requestType: .put, isAuthorization: true, success: { (response) in
            //            print("response\(String(describing: response))")
            self.hideLoader()
            if response?["statusCode"] == 200 {
                DispatchQueue.main.async {
                    if let detail =  response?["data"].dictionary{
                        print(detail)
                        self.lbl_VehicleName.text = "\(detail["driverData"]?["vehicle_information"]["model"] ?? "")"
                        self.lbl_Brand.text = "\(detail["driverData"]?["vehicle_information"]["vehicle_number"] ?? "")"
                        self.lbl_NameDriver.text = "\(detail["driverData"]?["personal_information"]["fullName"] ?? "")".nameFormatted
                        let driverMobile = "\(detail["driverData"]?["personal_information"]["mobile"] ?? "")"
                        let valuePlateNumber = "\(detail["driverData"]?["personal_information"]["vehicle_number"] ?? "")"

                        if let rating = (detail["driverData"]?["avgRating"] ?? 0.0).double {
                            print(rating)
                            self.view_RatingDriver.rating = rating
                            
                        }
                        
                        if detail["driverData"]?["pic"]["original"] != "" {
                            let imgUrl = "\(kBaseImageURL)\(detail["driverData"]?["pic"]["original"] ?? "")"
                            if let url = imgUrl.url{
                                self.imgView_Driver.image = nil
                                self.imgView_Driver.setNeedsDisplay()
                                KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                                    DispatchQueue.main.async {
                                        if let thumbImage = image {
                                            self.imgView_Driver.image = thumbImage
                                        }
                                    }
                                })
                            }
                        }
                       print(self.view_RatingDriver.rating)

                        RiderManager.sharedInstance.bookedDriverProfile =  DriverBasicModal.init(name: self.lbl_NameDriver.text! , image: "", km: "" , min:  "" ,rating: "" ,vehicleName: self.lbl_VehicleName.text! ,vehicleNumber: valuePlateNumber ,mobileNumber: driverMobile)

                        self.dictMarkAdmin[dictKey.pick] =  LocationsAggregate.init(json:  detail["data"]?["adminMarkPickUpLocation"] ?? ["":""])
                        self.dictMarkAdmin[dictKey.drop] =  LocationsAggregate.init(json:  detail["data"]?["adminMarkDropOffLocation"] ?? ["":""])
                    }
                }
            }
            
        }, failure: { (error) in
            print(error.debugDescription)
            self.hideLoader()
        })
    }
    
    
    //MARK:-  button Action
    lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Main, bundle: nil)
    @IBAction func action_Call(_ sender: UIButton) {
        let controller = storyBoard.instantiateViewController(withIdentifier: "CallingTwillioViewController") as! CallingTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func action_Msg(_ sender: UIButton) {
        let controller = storyBoard.instantiateViewController(withIdentifier: "MessageTwillioViewController") as! MessageTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func action_Cancel(_ sender: UIButton) {
        cancelAggregate()
    }
    
    @IBAction func showRoute(_ sender: UIButton) {
        self.performSegue(withIdentifier: "show_Route", sender: tripData)
    }
    
    func cancelAggregate() {
        AlertHelper.displayAlertView(title: LinkStrings.AlertTitle.Title, message:LinkStrings.RiderAlerts.RemoveAggregate, style: .alert, actionButtonTitle: [LinkStrings.MostCommon.Yes, LinkStrings.MostCommon.No], completionHandler: { (action:UIAlertAction!) -> Void in
            if action.title==LinkStrings.MostCommon.Yes {
                guard self.checkNetworkStatus() else {return}
                self.showLoader()
                WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/cancel_aggregate_pending_ride", withInputString: ["tripId":self.tripData?.id ?? ""], requestType: .post, isAuthorization: true, success: { (response) in
                    print("response\(String(describing: response))")
                    DispatchQueue.main.async {
                        self.hideLoader()
                        if response?["statusCode"] == 200 {
                            self.navigationController?.popViewController()
                        }
                    }
                }, failure: { (error) in
                    self.hideLoader()
                })
            }
        })
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "show_Route" {
            if let controller = segue.destination as?  DisplayRouteViewController {
                controller.tripData = sender as? Trips
                controller.dictMarkAdmin = self.dictMarkAdmin
                print(dictMarkAdmin)
            }
        }
    }
}
