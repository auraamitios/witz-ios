//
//  AggregatePendingTripViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 12/26/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class AggregatePendingTripViewController: WitzSuperViewController {

    @IBOutlet weak var lbl_JourneyType: UILabel!
    @IBOutlet weak var btn_Days: UIButton!
    @IBOutlet weak var tbl_PendingTrips: UITableView!
    @IBOutlet weak var view_NoTrips: UIView!
    @IBOutlet weak var lbl_AlertMessage: UILabel!
    
    var arr_Trips = [Trips]()
    var tripType : [String]?
    var arrDays = [LinkStrings.DriverOnTrip.Days.Today,LinkStrings.DriverOnTrip.Days.Yesterday,LinkStrings.DriverOnTrip.Days.Last7Days,LinkStrings.DriverOnTrip.Days.All]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)

        if tripType != nil {
            lbl_JourneyType.text = tripType?[1]
            self.navigationItem.titleView = nil
            self.navigationItem.title = tripType?[0]
            
            //**************show alert message on label************//
            self.lbl_AlertMessage.text = "No \((tripType?[1].prefix(1).uppercased() ?? "") + (tripType?[1].dropFirst().lowercased() ?? "")) Trips Found"
            
            if tripType?[0] == LinkStrings.TripsType.FutureJourney{
                self.arrDays.removeAll()
                self.arrDays = [LinkStrings.DriverOnTrip.Days.Today,LinkStrings.DriverOnTrip.Days.Tomorrow,LinkStrings.DriverOnTrip.Days.ThisWeek,LinkStrings.DriverOnTrip.Days.ThisMonth,LinkStrings.DriverOnTrip.Days.All]
            }
            if tripType?[0] == LinkStrings.TripsType.PendingJourney{
                self.lbl_JourneyType.isHidden = true
                self.btn_Days.isHidden = true
            }
        }
        showDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showDetails(){
        self.btn_Days.setTitle(LinkStrings.DriverOnTrip.Days.Today, for:.normal)
        self.loadTripsDetail(by: LinkStrings.DriverOnTrip.Days.Today)
    }
    @IBAction func action_Days(_ sender: UIButton) {
        let controller = UIAlertController.init(title: "Choose from list", message: nil, preferredStyle: .actionSheet)
        /*****************  Create Action sheet of days  *****************/
        for string in arrDays{
            let action = UIAlertAction(title: string, style: .default, handler: { (action) -> Void in
                /***************** load data according to selected day *****************/
                self.btn_Days.setTitle(string, for:.normal)
                self.loadTripsDetail(by: string)
            })
            controller.addAction(action)
        }
        
        let cancel = UIAlertAction(title: LinkStrings.MostCommon.cancel, style: .cancel, handler: { (action) -> Void in
        })
        
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    func loadTripsDetail(by day:String){
        
        guard self.checkNetworkStatus() else {return}
        showLoader()
        DispatchQueue.global(qos: .background).async {
            var localTimeZoneName: String { return TimeZone.current.identifier }
            
            var parameters = ["skip":"0","limit":"100", "timezone":localTimeZoneName] as [String : Any]
            
            /***********************************Trip selected for  past ***********************************/
            /***********************************Trip selected for Instant ***********************************/
            if self.tripType?[1] == LinkStrings.TripsType.Instant{
                parameters["pastTrip"] = "true"
                parameters["instantTrips"] = "true"
            }
            /***********************************Trip selected for reservation ***********************************/
            if self.tripType?[1] == LinkStrings.TripsType.Reservation{
                
                /*Trip selected for Future as well */
                if self.tripType?[0] == LinkStrings.TripsType.FutureJourney{
                    parameters["pastTrip"] = "false"
                    parameters["reservationTrip"] = "true"
                }
                /*Trip selected for Past as well */
                if self.tripType?[0] == LinkStrings.TripsType.PastJourney{
                    parameters["pastTrip"] = "true"
                    parameters["reservationTrip"] = "true"
                }
                /*Trip selected for Pending as well */
                if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                    parameters["reservationPendingTrips"] = "true"
                }
            }
            
            /*****************Trip selected for Aggregate*****************/
            
            if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
                /**************Trip selected for Future as well ***************/
                if self.tripType?[0] == LinkStrings.TripsType.FutureJourney{
                    parameters["pastTrip"] = "false"
                    parameters["serviceRequest"] = "true"
                }
                /*Trip selected for Past as well */
                if self.tripType?[0] == LinkStrings.TripsType.PastJourney{
                    parameters["pastTrip"] = "true"
                    parameters["serviceRequest"] = "true"
                }
                /*Trip selected for Pending as well */
                if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                    parameters["serviceRequestPending"] = "true"
                }
            }
            
            /*Day selected */
            if day == LinkStrings.DriverOnTrip.Days.Today{
                parameters["today"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Yesterday{
                parameters["yesterDay"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Last7Days{
                parameters["lastWeek"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Tomorrow{
                parameters["tomorrow"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.ThisWeek{
                parameters["thisWeek"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.ThisMonth{
                parameters["thisMonth"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.LastMonth{
                parameters["lastMonth"] = "true"
            }
            print(parameters)
            //MARK:: API call
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/rider_trips", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                print(response as Any)
                DispatchQueue.main.async {
                    self.hideLoader()
                    self.arr_Trips.removeAll()
                    if let data  =  response?["data"]["trips"].array{
                        if data.count > 0 {
                            for dict in data{
                              let tripModal = Trips.init(json: dict["tripId"])
                                self.arr_Trips.append(tripModal)
                            }
                        }
                    }
                    self.tbl_PendingTrips.reloadData()
                }
            }, failure: { (error) in
                self.hideLoader()
            })
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AggregatePendingTripViewController:UITableViewDelegate, UITableViewDataSource, UITableViewDataSourcePrefetching {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arr_Trips.count > 0 {

            if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
                let arrAggregate =  arr_Trips.filter{
                    $0.tripType == "3"
                }
                arr_Trips = arrAggregate
            }
            if arr_Trips.count > 0 {
                view_NoTrips.isHidden = true   //hide no trips view
            }else{
                view_NoTrips.isHidden = false //show no trips view after all evaluation
            }
            return arr_Trips.count
        }else{
            view_NoTrips.isHidden = false //show no trips view
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AggregatePendingTripCell.identifier, for: indexPath) as! AggregatePendingTripCell
        cell.configureCellRider(with: arr_Trips[indexPath.row], index: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = arr_Trips[indexPath.row]
        self.performSegue(withIdentifier: "journey_View", sender: data)
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "journey_View"{
            if let controller = segue.destination as?  AggregateJourneyViewController {
                controller.cancelBtnShown = false
                controller.tripData = sender as? Trips
            }
        }
    }
    
}
