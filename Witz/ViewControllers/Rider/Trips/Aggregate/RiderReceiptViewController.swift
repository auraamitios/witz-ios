//
//  RiderReceiptViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/4/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class RiderReceiptViewController: WitzSuperViewController {
    
    // ******** penalty view iVar **********//
    
    @IBOutlet weak var view_Penalty: UIView!
    @IBOutlet weak var lbl_FareMoney: UILabel!
    @IBOutlet weak var lbl_TotalMoney: UILabel!
    @IBOutlet weak var lbl_WalletMoney: UILabel!
    @IBOutlet weak var lbl_CardMoney: UILabel!

    // ******** non-penalty view iVar **********//
    @IBOutlet weak var view_Normal: UIView!
    @IBOutlet weak var lbl_Fare: UILabel!
    @IBOutlet weak var lbl_Tax: UILabel!
    @IBOutlet weak var lbl_Total: UILabel!
    @IBOutlet weak var lbl_Wallet: UILabel!
    @IBOutlet weak var lbl_Card: UILabel!
    
    var tripDetail : TripsRider?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        
        if tripDetail != nil {
            showTripDetails()
        }
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.PastTrip
    }

    func showTripDetails() {
        switch tripDetail?.status {
        case 21?:
            //cancel by rider
            self.view_Normal.isHidden = true
            self.view_Penalty.isHidden = false
            self.lbl_FareMoney.text = "\(String(format: "%.2f", tripDetail?.riderCancelationFee ?? 0.0) + " ₺")"
            self.lbl_TotalMoney.text = "\(String(format: "%.2f", tripDetail?.walletPayment ?? 0.0) + " ₺")"
            self.lbl_WalletMoney.text = "\(String(format: "%.2f", tripDetail?.walletPayment ?? 0.0) + " ₺")"
            let ab = tripDetail?.riderCancelationFee ?? 0.0
            let bc = tripDetail?.walletPayment ?? 0.0
            let res = ab - bc
            self.lbl_CardMoney.text = "\(String(format: "%.2f", res) + " ₺")"
            break
        case 12?:
            //cancel by driver
            self.view_Normal.isHidden = true
            self.view_Penalty.isHidden = false
            self.lbl_FareMoney.text = "\(String(format: "%.2f", tripDetail?.price ?? 0.0) + " ₺")"
            self.lbl_TotalMoney.text = "\(String(format: "%.2f", tripDetail?.price ?? 0.0) + " ₺")"
            self.lbl_WalletMoney.text = "\(String(format: "%.2f", tripDetail?.walletPayment ?? 0.0) + " ₺")"
//            let ab = tripDetail?.riderCancelationFee ?? 0.0
//            let bc = tripDetail?.walletPayment ?? 0.0
//            let res = ab - bc
            self.lbl_CardMoney.text = "NA"
            break
        default:
            //ride completed
            self.view_Normal.isHidden = false
            self.view_Penalty.isHidden = true
            let ab = tripDetail?.price ?? 0.0
            let bc = tripDetail?.riderTaxApplied ?? 0.0
            let res = ab - bc
//            self.lbl_Fare.text = "\(String(format: "%.2f", tripDetail?.price ?? 0.0) + " ₺")"
            self.lbl_Fare.text = "\(String(format: "%.2f", res) + " ₺")"
            self.lbl_Tax.text = "\(String(format: "%.2f", tripDetail?.riderTaxApplied ?? 0.0) + " ₺")"
            self.lbl_Total.text = "\(String(format: "%.2f", tripDetail?.price ?? 0.0) + " ₺")"
            self.lbl_Wallet.text = "\(tripDetail?.walletPayment ?? 0.0) ₺"
//            self.lbl_Card.text = "\(tripDetail?.price ?? 0.0) - \(tripDetail?.walletPayment ?? Int(0.0)) ₺"

            let cd = tripDetail?.price ?? 0.0
            let ec = tripDetail?.walletPayment ?? 0.0
            let res1 = cd - ec
            self.lbl_Card.text = "\(String(format: "%.2f", res1) + " ₺")"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
