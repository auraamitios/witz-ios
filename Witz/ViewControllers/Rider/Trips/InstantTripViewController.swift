//
//  InstantTripViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 12/26/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher
import SwiftyJSON
class InstantTripViewController: WitzSuperViewController {

    @IBOutlet weak var trip_MapView: GMSMapView!
    @IBOutlet weak var lbl_Fare: UILabel!
    @IBOutlet weak var lbl_PickUpAddress: UILabel!
    @IBOutlet weak var lbl_DropAddress: UILabel!
    @IBOutlet weak var lbl_KM: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var imgView_Pic: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_VehicleName: UILabel!
    @IBOutlet weak var lbl_VehicleNumber: UILabel!
    @IBOutlet weak var lbl_TripDateTime: UILabel!
    
    @IBOutlet var starImages: [UIImageView]!
    
    var tripDetail : TripsRider?
    var tripType : [String]?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)

        trip_MapView.settings.scrollGestures = false
        trip_MapView.settings.zoomGestures = false
        trip_MapView.shouldHideToolbarPlaceholder = true
        
        if tripDetail != nil{
            showTripDetails()
            getTripDetail()
        }
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.PastTrip
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK::- custom method

    func showTripDetails(){
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.startLocation?.coordinates?[0] ?? 0))
        let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.endLocation?.coordinates?[0] ?? 0))
        
        lbl_PickUpAddress.text = tripDetail?.startAddress ?? ""
        lbl_DropAddress.text = tripDetail?.endAddress ?? ""
        if tripType![0] == LinkStrings.TripsType.PastJourney && tripType![1] == LinkStrings.TripsType.Reservation {
            lbl_TripDateTime.text = tripDetail?.bookingDate?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "Journey Date"

        }else{
        lbl_TripDateTime.text = tripDetail?.createdAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "Journey Date"
        }
        self.lbl_KM.text = "\(String(format: "%.2f", tripDetail?.distance ?? 0.0) + " Km")"
        self.lbl_Fare.text = "\(tripDetail?.price ?? 0) ₺"
        self.lbl_Time.text = "\(tripDetail?.duration ?? 0)\(" ")\("min")"

        
        //Marker for pick up
        let marker1 = GMSMarker(position: coordinates)
        marker1.title = "Pick UP Location"
        marker1.appearAnimation = GMSMarkerAnimation.pop
       
        marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker1.map = trip_MapView
        
        //Marker for Drop
        let marker2 = GMSMarker(position: coordinates1)
        marker2.title = "Drop Location"
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon =  Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker2.map = trip_MapView
        
        showLoader()
        self.showPolyLine(source: coordinates, destination: coordinates1)
        
    }
    
    func getTripDetail() {
        //RiderManager.sharedInstance.tripIDForRider ?? ""
        guard self.checkNetworkStatus() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_trip_details", withInputString: ["tripId":tripDetail?.id ?? ""], requestType: .put, isAuthorization: true, success: { (response) in
            print("response\(String(describing: response))")
            self.hideLoader()
            if response?["statusCode"] == 200 {
                DispatchQueue.main.async {
                    if let detail =  response?["data"].dictionary{
                        print(detail)
                        self.lbl_VehicleName.text = "\(detail["driverData"]?["vehicle_information"]["model"] ?? "")"
                        self.lbl_VehicleNumber.text = "\(detail["driverData"]?["vehicle_information"]["vehicle_number"] ?? "")"
                        self.lbl_Name.text = "\(detail["driverData"]?["personal_information"]["fullName"] ?? "")".nameFormatted
                        
                        if let rating = (detail["driverData"]?["avgRating"] ?? 0.0).double {
                        print(rating)
                            self.setDriverRating(rating: rating)}
                        if detail["driverData"]?["pic"]["original"] != "" {
                            let imgUrl = "\(kBaseImageURL)\(detail["driverData"]?["pic"]["original"] ?? "")"
                            let url = imgUrl.url
                            self.imgView_Pic.image = nil
                            self.imgView_Pic.setNeedsDisplay()
                            self.downloadImage(url!)
                        }
                    }
                }
            }
            
        }, failure: { (error) in
            print(error.debugDescription)
            self.hideLoader()
        })
    }
    
    func setDriverRating(rating:Double) {
        //menu_dashboard_my_points
        //menu_page_my_points
        
        switch rating {
        case 1:
//            for star in starImages {
//                star.image = #imageLiteral(resourceName: "menu_page_my_points")
//            }
            starImages[4].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[3].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            starImages[2].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            starImages[1].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            starImages[0].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            break
        case 2:
            starImages[4].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[3].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[2].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            starImages[1].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            starImages[0].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            break
        case 3:
            starImages[4].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[3].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[2].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[1].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            starImages[0].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            break
        case 4:
            starImages[4].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[3].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[2].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[1].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[0].image = #imageLiteral(resourceName: "menu_dashboard_my_points")
            break
        case 5:
            starImages[4].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[3].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[2].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[1].image = #imageLiteral(resourceName: "menu_page_my_points")
            starImages[0].image = #imageLiteral(resourceName: "menu_page_my_points")
            break
            
        default:
            break
        }
        
    }
    func downloadImage(_ url:URL) {
        KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            DispatchQueue.main.async {
                if let thumbImage = image {
                    self.imgView_Pic.image = thumbImage
                }
            }
        })
    }
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5

        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.trip_MapView
            let bounds = GMSCoordinateBounds(path: path)
            self.trip_MapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
        hideLoader()
    }
    
    @IBAction func action_Receipt(_ sender: UIButton) {
        self.performSegue(withIdentifier: "receipt", sender: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "receipt"{
            if let controller = segue.destination as?  RiderReceiptViewController {
                controller.tripDetail = tripDetail
            }
        }
    }

}

extension InstantTripViewController : GMSMapViewDelegate{
    // MARK: GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "DisplayRouteViewController") as? DisplayRouteViewController{
            
            controller.tripDetail = tripDetail
            //            self.present(controller, animated: true, completion: {
            //                // Driver has 1 min to accept the ride
            //                Utility.stopTimer(after: TimeInterval(self.timeLeftToAccept), self.task)
            //                Utility.delegate = self
            //            })
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}
