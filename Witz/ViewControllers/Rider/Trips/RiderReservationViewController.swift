//
//  RiderReservationViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 12/19/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher
class RiderReservationViewController: WitzSuperViewController {
    
    @IBOutlet weak var mapView_Reservation: GMSMapView!
    @IBOutlet weak var lbl_ReserveDate: UILabel!
    @IBOutlet weak var lbl_PickUpAddress: UILabel!
    @IBOutlet weak var lbl_DropAddress: UILabel!
    @IBOutlet weak var lbl_ReservationsCount: UILabel!
    @IBOutlet weak var bid_Stackview: UIStackView!
    @IBOutlet weak var view_CancelBid: UIView!
    
    @IBOutlet weak var lbl_Title_viewCancelBid: UILabel!
    var tripDetail : TripsRider?
    var tripType : [String]?
    
    fileprivate var task:DispatchWorkItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.PendingRequest
        mapView_Reservation.settings.scrollGestures = false
        mapView_Reservation.settings.zoomGestures = false
        mapView_Reservation.shouldHideToolbarPlaceholder = true
        mapView_Reservation.delegate = self
        
        if tripDetail != nil{
            showTripDetails()
            //            getRideDetailRider()
        }
        print("time********-->\(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.singleDayRiderTopFiveBidsDuration ?? 0)")
        self.perform(#selector(showBidReviewView), with: nil, afterDelay: TimeInterval(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.singleDayRiderTopFiveBidsDuration ?? 0))
        
        do {
            // Set the map style by passing a valid JSON string.
            mapView_Reservation.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    func showBidReviewView() {
        task = DispatchWorkItem {
            print("time finished ************")
            self.bid_Stackview.isHidden = false
            self.view_CancelBid.isHidden = true
        }
    }
    func showTripDetails(){
        self.bid_Stackview.isHidden = true
        self.view_CancelBid.isHidden = false
        //minimum reservation time to show bids
        let currentTime = Date()
        if let dateCreate = tripDetail?.createdAt?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){

            if let bookingDate = tripDetail?.bookingDate?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                
                if bookingDate.hoursSince(dateCreate) < 24{
                    // Same day reservation request handling
                    let timeRemain = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.singleDayRiderTopFiveBidsDuration ?? 0)
                    if (currentTime.minutesSince(dateCreate)) > timeRemain {
                        self.bid_Stackview.isHidden = false
                        self.view_CancelBid.isHidden = true
                        
                        let timeRemain1 = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.singleDayRiderOfferClosureDuration ?? 0)
                        
                        if (currentTime.minutesSince(dateCreate)) > timeRemain1 {   // time to accept reservation over so cancel reservation will show from now only
                            self.bid_Stackview.isHidden = true
                            self.view_CancelBid.isHidden = false
                            self.lbl_Title_viewCancelBid.text = "Close Reservation"
                            
                        }
                    }
                }else{
                    // Future day reservation request handling
                    let timeRemainFuture = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.futureDayRiderTopFiveBidsDuration ?? 0)
                    if (currentTime.minutesSince(dateCreate)) > timeRemainFuture {
                        self.bid_Stackview.isHidden = false
                        self.view_CancelBid.isHidden = true
                        
                        let timeRemain1 = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.futureDayRiderOfferClosureDuration ?? 0)
                        
                        if (currentTime.minutesSince(dateCreate)) > timeRemain1 {   // time to accept reservation over so cancel reservation will show from now only
                            self.bid_Stackview.isHidden = true
                            self.view_CancelBid.isHidden = false
                            self.lbl_Title_viewCancelBid.text = "Close Reservation"

                        }
                    }

                }
            }
        }

        lbl_PickUpAddress.text = tripDetail?.startAddress ?? ""
        lbl_DropAddress.text = tripDetail?.endAddress ?? ""
        lbl_ReserveDate.text = tripDetail?.bookingDate?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.startLocation?.coordinates?[0] ?? 0))
        let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.endLocation?.coordinates?[0] ?? 0))
        self.lbl_ReservationsCount.text = "\(tripDetail?.bidByDriver?.count ?? 0)"
         
        //Marker for pick up
        let marker1 = GMSMarker(position: coordinates)
        marker1.title = "Pick UP Location"
        marker1.appearAnimation = GMSMarkerAnimation.pop
        marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker1.map = mapView_Reservation
        
        //Marker for Drop
        let marker2 = GMSMarker(position: coordinates1)
        marker2.title = "Drop Location"
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker2.map = mapView_Reservation
        
        showLoader()
        self.showPolyLine(source: coordinates, destination: coordinates1)
        
    }
    //MARK:- custom method
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5

        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            //            print("wayponits****************\(distance)")
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView_Reservation
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView_Reservation.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
        hideLoader()
    }
    
    //MARK:- get Ride Detail from server
    //    public func getRideDetailRider(){
    ////        get_price_offer_from_drivers
    //        guard self.checkNetworkStatus() else {
    //            return
    //        }
    //        print(tripDetail?.id ?? "j")
    //        if let tripID = tripDetail?.id{
    //            let parameters =   ["tripId":tripID]
    //
    //            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_trip_detail", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
    //                self.hideLoader()
    //                if let  result = response?["data"]{
    //                }
    //            }, failure: { (error) in
    //                self.hideLoader()
    //            })
    //        }
    //    }
    
    //MARK:: IBOutlets actions
    
    @IBAction func action_ReviewBids(_ sender: UIButton) {
        self.performSegue(withIdentifier: "review_Bids", sender: nil)
    }
    
    @IBAction func action_CloseReservationRequest(_ sender: UIButton) {
        self.cancelReservation()
    }
    
    @IBAction func action_CancelReservation(_ sender: UIButton) {
        cancelReservation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showReviewBidsOnTime() {
        
    }
    
    func cancelReservation() {
        AlertHelper.displayAlertView(title: LinkStrings.AlertTitle.Title, message:LinkStrings.RiderAlerts.RemoveReservation, style: .alert, actionButtonTitle: [LinkStrings.MostCommon.Yes, LinkStrings.MostCommon.No], completionHandler: { (action:UIAlertAction!) -> Void in
            if action.title==LinkStrings.MostCommon.Yes {
                guard self.checkNetworkStatus() else {return}
                self.showLoader()
                WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/cancelReservationTrip", withInputString: ["tripId":self.tripDetail?.id ?? ""], requestType: .put, isAuthorization: true, success: { (response) in
                    print("response\(String(describing: response))")
                    DispatchQueue.main.async {
                        self.hideLoader()
                        if response?["statusCode"] == 200 {
                            self.jumpToTripsMenuScreen()
                        }
                    }
                }, failure: { (error) in
                    self.hideLoader()
                })
            }
        })
    }
    
    func jumpToTripsMenuScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is TripsViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "review_Bids" {
            if let controller = segue.destination as?  ReviewBidsViewController{
                controller.tripID =  tripDetail?.id ?? ""
            }
        }
    }
}
// MARK: GMSMapViewDelegate
extension RiderReservationViewController : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "DisplayRouteViewController") as? DisplayRouteViewController{
            controller.tripDetail = tripDetail
            self.navigationController?.pushViewController(controller, animated: true)
            //            self.present(controller, animated: true, completion: {
            //                // Driver has 1 min to accept the ride
            //                Utility.stopTimer(after: TimeInterval(self.timeLeftToAccept), self.task)
            //                Utility.delegate = self
            //            })
        }
        
    }
}
