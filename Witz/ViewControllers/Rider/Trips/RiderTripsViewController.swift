//
//  RiderTripsViewController.swift
//  Witz
//
//  Created by abhishek kumar on 21/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class RiderTripsViewController: WitzSuperViewController {
    
    @IBOutlet weak var view_NoTrips: UIView!
    @IBOutlet weak var lbl_AlertMessage: UILabel!
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var lbl_Journey: UILabel!
    @IBOutlet weak var btn_Days: UIButton!
    
    var arr_Trips = [TripsRider]()
    var tripType : [String]?
    
    var arrDays = [LinkStrings.DriverOnTrip.Days.Today,LinkStrings.DriverOnTrip.Days.Yesterday,LinkStrings.DriverOnTrip.Days.Last7Days,LinkStrings.DriverOnTrip.Days.All]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        
        if tripType != nil {
            lbl_Journey.text = tripType?[1]
            self.navigationItem.titleView = nil
            self.navigationItem.title = tripType?[0]
            
            //            **********************************show alert message on label
            self.lbl_AlertMessage.text = "No \((tripType?[1].prefix(1).uppercased() ?? "") + (tripType?[1].dropFirst().lowercased() ?? "")) Trips Found"
            
            if tripType?[0] == LinkStrings.TripsType.FutureJourney{
                self.arrDays.removeAll()
                self.arrDays = [LinkStrings.DriverOnTrip.Days.Today,LinkStrings.DriverOnTrip.Days.Tomorrow,LinkStrings.DriverOnTrip.Days.ThisWeek,LinkStrings.DriverOnTrip.Days.ThisMonth,LinkStrings.DriverOnTrip.Days.All]
            }
            if tripType?[0] == LinkStrings.TripsType.PendingJourney{
                self.lbl_Journey.isHidden = true
                self.btn_Days.isHidden = true
            }
        }
        showDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showDetails(){
        self.btn_Days.setTitle(LinkStrings.DriverOnTrip.Days.Today, for:.normal)
        self.loadTripsDetail(by: LinkStrings.DriverOnTrip.Days.Today)
    }
    
    @IBAction func selectDays(_ sender: UIButton) {
        let controller = UIAlertController.init(title: "Choose from list", message: nil, preferredStyle: .actionSheet)
        /*****************  Create Action sheet of days  *****************/
        for string in arrDays{
            let action = UIAlertAction(title: string, style: .default, handler: { (action) -> Void in
                /***************** load data according to selected day *****************/
                self.btn_Days.setTitle(string, for:.normal)
                self.loadTripsDetail(by: string)
            })
            controller.addAction(action)
        }
        
        let cancel = UIAlertAction(title: LinkStrings.MostCommon.cancel, style: .cancel, handler: { (action) -> Void in
        })
        
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    func loadTripsDetail(by day:String){
        
        guard self.checkNetworkStatus() else {return}
        showLoader()
        DispatchQueue.global(qos: .background).async {
            var localTimeZoneName: String { return TimeZone.current.identifier }
            
            var parameters = ["skip":"0","limit":"100", "timezone":localTimeZoneName] as [String : Any]
            
            /***********************************Trip selected for  past ***********************************/
            /***********************************Trip selected for Instant ***********************************/
            if self.tripType?[1] == LinkStrings.TripsType.Instant{
                parameters["pastTrip"] = "true"
                parameters["instantTrips"] = "true"
            }
            
            /***********************************Trip selected for reservation ***********************************/
            if self.tripType?[1] == LinkStrings.TripsType.Reservation{
                
                /*Trip selected for Future as well */
                if self.tripType?[0] == LinkStrings.TripsType.FutureJourney{
                    parameters["pastTrip"] = "false"
                    parameters["reservationTrip"] = "true"
                }
                /*Trip selected for Past as well */
                if self.tripType?[0] == LinkStrings.TripsType.PastJourney{
                    parameters["pastTrip"] = "true"
                    parameters["reservationTrip"] = "true"
                }
                /*Trip selected for Pending as well */
                if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                    parameters["reservationPendingTrips"] = "true"
                }
            }
            
            /*************************************Trip selected for Aggregate***************************************/
            
            if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
                
                /*********************************Trip selected for Future as well *********************************************/
                if self.tripType?[0] == LinkStrings.TripsType.FutureJourney{
                    parameters["pastTrip"] = "false"
                    parameters["serviceRequest"] = "true"
                }
                /*Trip selected for Past as well */
                if self.tripType?[0] == LinkStrings.TripsType.PastJourney{
                    parameters["pastTrip"] = "true"
                    parameters["serviceRequest"] = "true"
                }
                /*Trip selected for Pending as well */
                if self.tripType?[0] == LinkStrings.TripsType.PendingJourney{
                    parameters["serviceRequestPending"] = "true"
                }
            }
            
            
            /*Day selected */
            if day == LinkStrings.DriverOnTrip.Days.Today{
                parameters["today"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Yesterday{
                parameters["yesterDay"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Last7Days{
                parameters["lastWeek"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.Tomorrow{
                parameters["tomorrow"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.ThisWeek{
                parameters["thisWeek"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.ThisMonth{
                parameters["thisMonth"] = "true"
            }else if day == LinkStrings.DriverOnTrip.Days.LastMonth{
                parameters["lastMonth"] = "true"
            }
            
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/rider_trips", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                //                print(response)
                DispatchQueue.main.async {
                    self.hideLoader()
                    let modal = RiderTripsModal.init(json: response?["data"] ?? ["":""])
                    self.arr_Trips = modal.trips ?? []
                    self.table_View.reloadData()
                }
            }, failure: { (error) in
                self.hideLoader()
            })
        }
    }
}

//MARK:- Table View Delegates
extension RiderTripsViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arr_Trips.count > 0 {
            
            /***********************************Trip selected for Instant ***********************************/
            if self.tripType?[1] == LinkStrings.TripsType.Instant{
                let arrInstant =  arr_Trips.filter{
                    $0.tripType == "1"
                }
                arr_Trips = arrInstant
            }
                
                /***********************************Trip selected for reservation ***********************************/
            else if self.tripType?[1] == LinkStrings.TripsType.Reservation{
                let arrReservation =  arr_Trips.filter{
                    $0.tripType == "2"
                }
                arr_Trips = arrReservation
            }
                
                /*************************************Trip selected for Aggregate***************************************/
                
            else if self.tripType?[1] == LinkStrings.TripsType.Aggregate{
                let arrAggregate =  arr_Trips.filter{
                    $0.tripType == "3"
                }
                arr_Trips = arrAggregate
            }
            if arr_Trips.count > 0 {
                view_NoTrips.isHidden = true   //hide no trips view
            }else{
                view_NoTrips.isHidden = false //show no trips view after all evaluation
            }
            return arr_Trips.count
        }else{
            view_NoTrips.isHidden = false //show no trips view
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellReservationTrips", for: indexPath) as! CellReservationTrips
       let showCount = self.checkBidShowTime(tripDetail: arr_Trips[indexPath.row])
        cell.configureCellRider(with: arr_Trips[indexPath.row], index: indexPath, tripType: tripType ?? [],showBid: showCount)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tripType![1] == LinkStrings.TripsType.Instant {
            performSegue(withIdentifier: "trip_Instant", sender: indexPath)
        }else if tripType![0] == LinkStrings.TripsType.PastJourney && tripType![1] == LinkStrings.TripsType.Reservation {
            performSegue(withIdentifier: "trip_Instant", sender: indexPath)
        } else if tripType![0] == LinkStrings.TripsType.FutureJourney && tripType![1] == LinkStrings.TripsType.Reservation {
            performSegue(withIdentifier: "segue_FutureJourney", sender: indexPath)
        }else {
            performSegue(withIdentifier: "segue_TripDetail", sender: indexPath)
        }
    }
    
    fileprivate func checkBidShowTime(tripDetail:TripsRider) -> Bool{
        //minimum reservation time to show bids
        let currentTime = Date()
        if let dateCreate = tripDetail.createdAt?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
            
            if let bookingDate = tripDetail.bookingDate?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                
                if bookingDate.hoursSince(dateCreate) < 24{
                    // Same day reservation request handling
                    let timeRemain = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.singleDayRiderTopFiveBidsDuration ?? 0)
                    if (currentTime.minutesSince(dateCreate)) > timeRemain {
                      
                        let timeRemain1 = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.singleDayRiderOfferClosureDuration ?? 0)
                        
                        if (currentTime.minutesSince(dateCreate)) > timeRemain1 {   // time to accept reservation over so cancel reservation will show from now only
                            return false
                        }
                        return true }
                }else{
                    // Future day reservation request handling
                    let timeRemainFuture = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.futureDayRiderTopFiveBidsDuration ?? 0)
                    if (currentTime.minutesSince(dateCreate)) > timeRemainFuture {
                      
                        let timeRemain1 = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.futureDayRiderOfferClosureDuration ?? 0)
                        if (currentTime.minutesSince(dateCreate)) > timeRemain1 {   // time to accept reservation over so cancel reservation will show from now only
                            return false
                        }
                        return true }
                }
            }
            return false
        }
   return false }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_TripDetail" {
            if let controller = segue.destination as? RiderReservationViewController{
                if let index = sender as? IndexPath{
                    controller.tripDetail = self.arr_Trips[index.row]
                    controller.tripType = self.tripType
                }
            }
        }else if segue.identifier == "trip_Instant" {
            if let controller = segue.destination as? InstantTripViewController{
                if let index = sender as? IndexPath{
                    controller.tripDetail = self.arr_Trips[index.row]
                    controller.tripType = self.tripType
                }
            }
        }else if segue.identifier == "segue_FutureJourney" {
            if let controller = segue.destination as? RiderFutureJourneyViewController{
                if let index = sender as? IndexPath{
                    controller.tripDetail = self.arr_Trips[index.row]
                    controller.tripType = self.tripType
                }
            }
        }
    }
}
