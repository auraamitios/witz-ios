//
//  ReviewBidsViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 12/19/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class ReviewBidsViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_ReserveTime: UILabel!
    @IBOutlet weak var lbl_ReserveMsg: UILabel!
    @IBOutlet weak var table_View: UITableView!
    
    var tripID : String?
    var arrDriverList = [DriverDataRider]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.PriceDeals
        //  if driver do nothing
        
        if tripID != nil {showReviewData()}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showReviewData(){
        guard self.checkNetworkStatus() else {return}
        showLoader()
        print(tripID ?? "")
        let parameters = ["tripId":tripID ?? ""]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_price_offer_from_drivers", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            print(response ?? "")
            if let  result = response?["data"]{
                
                let modal = BidFromDriverModal.init(json: result)
                self.lbl_ReserveTime.text = modal.bookingDate?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                var timeLeftToAccept = 0
                //minimum reservation time left to accept bids
                let currentTime = Date()
                if let dateCreate = modal.createdAt?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                    
                    if let bookingDate = modal.bookingDate?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                        
                        if bookingDate.hoursSince(dateCreate) < 24{
                            // Same day reservation request handling
                            let timeRemain = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.singleDayRiderBidAcceptanceDuration ?? 0)
                            if (currentTime.minutesSince(dateCreate)) > timeRemain {
                                timeLeftToAccept = 0 // time over
                                self.timeOverToAccept()
                            }else {
                                
                                timeLeftToAccept = Int(timeRemain - currentTime.minutesSince(dateCreate))
                            }
                        }else{
                            // Future day reservation request handling
                            let timeRemainFuture = Double(RiderManager.sharedInstance.riderLoginData?.reservationGeneralSetting?.futureDayRiderBidAcceptanceDuration ?? 0)
                            if (currentTime.minutesSince(dateCreate)) > timeRemainFuture {
                                timeLeftToAccept = 0 // time over
                                self.timeOverToAccept()
                            }else {
                                timeLeftToAccept = Int(timeRemainFuture - currentTime.minutesSince(dateCreate))
                            }
                        }
                    }
                }
                let hour =  Int(timeLeftToAccept) / 60
                let minutes = Int(timeLeftToAccept) % 60
                
                let finalSetTime = "\(hour > 9 ? "\(hour)" : "0\(hour)")Hr :\(minutes > 9 ? "\(minutes)" : "0\(minutes)")Mins"
                
                self.lbl_ReserveMsg.text = "You have \(finalSetTime) to accept one offer"
                self.arrDriverList = modal.driverData ?? []
                self.table_View.reloadData()
                
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
    
    func timeOverToAccept(){
        let alert = UIAlertController.init(title: LinkStrings.App.AppName, message: LinkStrings.Notifications.TimeOutOffer  ,preferredStyle: .alert)
        let action = UIAlertAction.init(title:  LinkStrings.MostCommon.Ok, style: .cancel, handler: {(action) in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK:- Table View Delegates
extension ReviewBidsViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrDriverList.count > 0 {
            self.lbl_ReserveMsg.isHidden = false
        }else{
            self.lbl_ReserveMsg.isHidden = true
        }
        return arrDriverList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBid", for: indexPath) as! CellBidsRider
        cell.configureCellRider(with: arrDriverList[indexPath.row], index: indexPath)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "approve_Bid", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "approve_Bid" {
            if let controller = segue.destination as?  BidApprovalOfferViewController{
                if let index = sender as? IndexPath{
                    controller.driverInfo = self.arrDriverList[index.row]
                    controller.tripID = self.tripID ?? ""
                }
            }
        }
    }
}
