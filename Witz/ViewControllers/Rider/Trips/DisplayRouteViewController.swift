//
//  DisplayRouteViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/5/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
class DisplayRouteViewController: WitzSuperViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_Distance: UILabel!
    
    //outlet for aggregate trip only
    
    @IBOutlet weak var view_AggregateDetails: UIView!
    @IBOutlet weak var lbl_StartPointDetail: UILabel!
    @IBOutlet weak var lbl_EndPointDetail: UILabel!
    @IBOutlet weak var lbl_TotalTripDetail: UILabel!
    
    
    var tripData:Trips?
    var tripDetail:TripsRider?
    
    var dictMarkAdmin : [String:LocationsAggregate]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        mapView.shouldHideToolbarPlaceholder = true
        if tripData != nil {
            self.lbl_Distance.text = "\(String(format: "%.2f", tripData?.distance?.double ?? 0.0) + " Km")\(" -- ")\(String(format: "%.2f",tripData?.duration ?? 0))\("min")"
            showRoute(type:0) //aggregate trips
            view_AggregateDetails.isHidden = false
            self.lbl_TotalTripDetail.text = self.lbl_Distance.text
            
        }else if tripDetail != nil{
            self.lbl_Distance.text = "\(String(format: "%.2f", tripDetail?.distance?.double ?? 0.0) + " Km")\("--")\(String(format: "%.2f",tripDetail?.duration ?? 0))\("min")"
            showRoute(type:1)//other trips
            
            view_AggregateDetails.isHidden = true
        }
    }
    
    func showRoute(type:Int) {
        addPickLocationIcon(type:type)
        addDropLocationIcon(type:type)
        getPathPoints(type:type)
    }
    
    fileprivate func addPickLocationIcon(type:Int) {
        if type == 0 {
            
            // rider request the aggregate from
            let pickPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripData?.startLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripData?.startLocation?.coordinates![0] ?? 0)))
            let position = CLLocationCoordinate2D(latitude: pickPosition.latitude, longitude:pickPosition.longitude)
            let marker = GMSMarker(position: position)
            marker.map = self.mapView
//            if tripData?.aggregateTripType == 1 {
                marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "riderPick "), scaledToSize: CGSize(width: 30.0, height: 30.0))
//            }else {
//                marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "riderDrop "), scaledToSize: CGSize(width: 30.0, height: 30.0))
//            }
            marker.appearAnimation = GMSMarkerAnimation.pop
            
            // this will show starting point and ending point of driver
            if dictMarkAdmin != nil {
                if let mark = dictMarkAdmin![dictKey.pick]{
                    
                    if mark.coordinates != nil {
                        
                        let pickMark = CLLocationCoordinate2D(latitude: (CLLocationDegrees(mark.coordinates![1] )), longitude: (CLLocationDegrees(mark.coordinates![0])))
                        putflagOnMap(position: pickMark)  // put flag
                        showRiderWalkingDistance(pickPosition: pickPosition, dropPosition: pickMark, isStart: true)
                        
                    }
                }
            }
        }else {
            let pickPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripDetail?.startLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripDetail?.startLocation?.coordinates![0] ?? 0)))
            let position = CLLocationCoordinate2D(latitude: pickPosition.latitude, longitude:pickPosition.longitude)
            let marker = GMSMarker(position: position)
            marker.map = self.mapView
            marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
            marker.appearAnimation = GMSMarkerAnimation.pop
        }
    }
    
    fileprivate func addDropLocationIcon (type:Int) {
        if type == 0 {
            // rider request the aggregate from
            let dropPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripData?.endLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripData?.endLocation?.coordinates![0] ?? 0)))
            let position = CLLocationCoordinate2D(latitude: dropPosition.latitude, longitude:dropPosition.longitude)
            let marker = GMSMarker(position: position)
            marker.map = self.mapView
//            if tripData?.aggregateTripType == 1 {
            
                marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "riderDrop"), scaledToSize: CGSize(width: 30.0, height: 30.0))
//            }else {
//                marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "riderPick "), scaledToSize: CGSize(width: 30.0, height: 30.0))
//            }
            marker.appearAnimation = GMSMarkerAnimation.pop
            
            // this will show starting point and ending point of driver
            if dictMarkAdmin != nil {
                if let mark = dictMarkAdmin![dictKey.drop]{
                    
                    if mark.coordinates != nil {
                        
                        let dropMark = CLLocationCoordinate2D(latitude: (CLLocationDegrees(mark.coordinates![1] )), longitude: (CLLocationDegrees(mark.coordinates![0] )))
                        putflagOnMap(position: dropMark)  // put flag
                        showRiderWalkingDistance(pickPosition: dropPosition, dropPosition: dropMark, isStart: false) //show dash line
                        
                    }
                }
            }
        }else {
            let dropPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripDetail?.endLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripDetail?.endLocation?.coordinates![0] ?? 0)))
            let position = CLLocationCoordinate2D(latitude: dropPosition.latitude, longitude:dropPosition.longitude)
            let marker = GMSMarker(position: position)
            marker.map = self.mapView
            marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
            marker.appearAnimation = GMSMarkerAnimation.pop
        }
        
    }
    
    
    /*
     This will genrated dash line path between to CLLocationCoordinate2D
     */
    
    fileprivate func showRiderWalkingDistance(pickPosition:CLLocationCoordinate2D,dropPosition:CLLocationCoordinate2D ,isStart:Bool){
        GooglePathHelper.getPathPolyPointsWalking(from: pickPosition, destination: dropPosition) { (returnPath, distance) in
            print(distance)
            DispatchQueue.main.async {
                if isStart == true{
                    ReverseGeocode.getAddressByCoords(coords: dropPosition, returnAddress: { (str) in
                        let arr = Utility.getTimeAndDistance(data: distance)
                        self.lbl_StartPointDetail.text = str + "\n" +  self.timeToShow(distance: arr[0], time: arr[1])
                    })
                }else{
                    ReverseGeocode.getAddressByCoords(coords: dropPosition, returnAddress: { (str) in
                        let arr = Utility.getTimeAndDistance(data: distance)
                        self.lbl_EndPointDetail.text = str + "\n" +  self.timeToShow(distance: arr[0], time: arr[1])
                    })
                }
            }
            self.showPath(polyStr: returnPath, isDashedLine: true)
        }
    }
    
    /*
     Show vehicle station or stop
     */
    
    fileprivate func timeToShow(distance: String ,time: String) -> String{
        
        var strFinal = "\(distance) Km -- \(time) Min"

        if distance.contains("m") && time.contains("min"){
            let strDist = distance.components(separatedBy: "m")
            let val = Float(strDist[0]) ?? 0
            print(val*1000)
            strFinal = "\(val*1000) m -- \(time)"
        }else if distance.contains("m") || distance.contains("km"){
            let strDist = distance.components(separatedBy: "m")
            let val = Float(strDist[0]) ?? 0
            print(val*1000)
            strFinal = "\(val*1000) m -- \(time) min"
        }else if time.contains("min") || time.contains(" mins"){
            let strDist = distance
            let val = Float(strDist) ?? 0
            print(val*1000)
            strFinal = "\(val*1000) m -- \(time) "
        }
     return "(" + strFinal + ")"
    }
    
    /*
     Show vehicle station or stop
     */
    
    fileprivate func putflagOnMap(position :CLLocationCoordinate2D){
        
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "flag") , scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    /*
     get path drawn from start to end location
     */
    
    fileprivate func getPathPoints(type:Int) {
        if type == 0 {
            
            // this will show starting point and ending point of driver
            if dictMarkAdmin != nil {
                var pickMark : CLLocationCoordinate2D?
                var dropMark : CLLocationCoordinate2D?
                
                if let mark = dictMarkAdmin![dictKey.pick]{
                    
                    if mark.coordinates != nil {
                        
                        pickMark = CLLocationCoordinate2D(latitude: (CLLocationDegrees(mark.coordinates![1] )), longitude: (CLLocationDegrees(mark.coordinates![0])))
                    }
                }
                if let mark1 = dictMarkAdmin![dictKey.drop]{
                    dropMark = CLLocationCoordinate2D(latitude: (CLLocationDegrees(mark1.coordinates![1] )), longitude: (CLLocationDegrees(mark1.coordinates![0] )))
                }
                if pickMark == nil || dropMark == nil {return}
                
                hitsLimit_Google = 5
                
                GooglePathHelper.getPathPolyPoints(from: pickMark!, destination: dropMark!) { (returnPath, distance) in
                    print(distance)
                    self.showPath(polyStr: returnPath, isDashedLine: false)
                }
            }
            
            //            let pickPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripData?.startLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripData?.startLocation?.coordinates![0] ?? 0)))
            //            let dropPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripData?.endLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripData?.endLocation?.coordinates![0] ?? 0)))
            
            //            self.mapView.animate(to: GMSCameraPosition(target: pickPosition, zoom: 13, bearing: 0, viewingAngle: 0))
            //            hitsLimit_Google = 5
            //
            //            GooglePathHelper.getPathPolyPoints(from: pickPosition, destination: dropPosition) { (returnPath, distance) in
            //                print(distance)
            //                self.showPath(polyStr: returnPath, isDashedLine: false)
            //            }
        }else {
            let pickPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripDetail?.startLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripDetail?.startLocation?.coordinates![0] ?? 0)))
            let dropPosition = CLLocationCoordinate2D(latitude: (CLLocationDegrees(tripDetail?.endLocation?.coordinates![1] ?? 0)), longitude: (CLLocationDegrees(tripDetail?.endLocation?.coordinates![0] ?? 0)))
            
            self.mapView.animate(to: GMSCameraPosition(target: pickPosition, zoom: 13, bearing: 0, viewingAngle: 0))
            hitsLimit_Google = 5
            
            GooglePathHelper.getPathPolyPoints(from: pickPosition, destination: dropPosition) { (returnPath, distance) in
                print(distance)
                self.showPath(polyStr: returnPath, isDashedLine: false)
            }
        }
        
    }
    
    
    /*
     draw path on google map
     */
    
    fileprivate func showPath(polyStr :String ,isDashedLine:Bool){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            DispatchQueue.main.async {
                let polyline = GMSPolyline(path: path)
                polyline.strokeWidth = 4.0
                polyline.strokeColor = App_Base_Color
                polyline.map = self.mapView
                
                if isDashedLine {
                    let styles :[GMSStrokeStyle] = [GMSStrokeStyle.solidColor(.green),GMSStrokeStyle.solidColor(.clear)]
                    let length : [NSNumber] = [10,5]
                    polyline.spans = GMSStyleSpans(polyline.path!, styles, length, GMSLengthKind.rhumb)
                    return
                }
                let bounds = GMSCoordinateBounds(path: path)
                self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100.0))
            }
        }
        hideLoader()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
