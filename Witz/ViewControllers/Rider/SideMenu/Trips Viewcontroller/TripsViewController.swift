//
//  TripsViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class TripsViewController: WitzSuperViewController {

    fileprivate var sectionToLoad : Int?  //  for which header tapped
    fileprivate let arr_SectionTitle = [["header":LinkStrings.TripsType.PastJourney,"row":[["name":LinkStrings.TripsType.Instant,"icon":#imageLiteral(resourceName: "trip_where_yellow")],["name":LinkStrings.TripsType.Reservation,"icon":#imageLiteral(resourceName: "reservation_to_where_yellow")],["name":LinkStrings.TripsType.Aggregate,"icon":#imageLiteral(resourceName: "menu_page_aggregate")]]],
                                        ["header":LinkStrings.TripsType.FutureJourney,"row":[["name":LinkStrings.TripsType.Reservation,"icon":#imageLiteral(resourceName: "reservation_to_where_yellow")],["name":LinkStrings.TripsType.Aggregate,"icon":#imageLiteral(resourceName: "menu_page_aggregate")]]],
                                        ["header":LinkStrings.TripsType.PendingJourney,"row":[["name":LinkStrings.TripsType.Reservation,"icon":#imageLiteral(resourceName: "reservation_to_where_yellow")],["name":LinkStrings.TripsType.Aggregate,"icon":#imageLiteral(resourceName: "menu_page_aggregate")]]]]
    
    @IBOutlet weak var tbl_Trips: UITableView!
    
    var journeyType:JourneyType?
    var journeyNature:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        
        self.setNavigationBarItem()
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.TripDetails
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utility.getRiderTripsCounts{ (complete) in
             DispatchQueue.main.async {
                self.tbl_Trips.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:: Tap gesture action
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        let tag:Int = sender?.view?.tag ?? 0
        switch tag {
        case 0:
            journeyType = JourneyType.PAST
            break
        case 1:
            journeyType = JourneyType.FUTURE
            break
        case 2:
            journeyType = JourneyType.PENDING
            break

        default:
            break
        }
        if sectionToLoad != nil{            // collpase the view
            if sectionToLoad == sender?.view?.tag{
                sectionToLoad = nil
                tbl_Trips.reloadData()
                return   //keep other header unchanged
            }
        }
        sectionToLoad = sender?.view?.tag
        if sectionToLoad != nil{            // expand the view
            tbl_Trips.reloadData()
        }
    }
    
    //MARK::API call's
    
    func getAllRiderTripsDetails(journeyType:JourneyType) {
        guard self.checkNetworkStatus() else {return}
        showLoader()
        DispatchQueue.global(qos: .background).async {
            var localTimeZoneName: String { return TimeZone.current.identifier }
            var parameters =   ["timezone":localTimeZoneName] as [String : Any]
            switch journeyType {
            case .PAST:
                if self.journeyNature == LinkStrings.TripsType.Instant {
                    parameters["instantTrips"] = "true"
                }else if self.journeyNature == LinkStrings.TripsType.Reservation {
                    parameters["reservationTrip"] = "true"
                }else if self.journeyNature == LinkStrings.TripsType.Aggregate {
                }
                parameters["pastTrip"] = "true"
                
                break
            case .FUTURE:
                if self.journeyNature == LinkStrings.TripsType.Instant {
                    parameters["instantTrips"] = "true"
                }else if self.journeyNature == LinkStrings.TripsType.Reservation {
                    parameters["reservationTrip"] = "true"
                }else if self.journeyNature == LinkStrings.TripsType.Aggregate {
                }
                parameters["pastTrip"] = "false"
                break
            case .PENDING:
                if self.journeyNature == LinkStrings.TripsType.Instant {
                    parameters["instantTrips"] = "true"
                }else if self.journeyNature == LinkStrings.TripsType.Reservation {
                    parameters["reservationTrip"] = "true"
                }else if self.journeyNature == LinkStrings.TripsType.Aggregate {
                }
                parameters["pastTrip"] = "false"
                parameters["serviceRequestPending"] = "true"
                break
                
            }
            RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/rider_trips", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
                print("response******\(String(describing: response))")
                if response?["statusCode"] == 200 {
                    DispatchQueue.main.async {
                        self.hideLoader()
                        self.performSegue(withIdentifier: "reservation_Trips", sender: nil)
                    }
                }
                
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }
    }
}

//MARK:- Table View Delegates
extension TripsViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arr_SectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sectionToLoad != nil {
            if sectionToLoad == section{    // expand the view to show rows
                return section == 0 ? 3 : 2
            }else{      // keep other section collpased
                return 0
            }
        }else{
            return 0  // default
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RiderTripCell.identifier, for: indexPath) as! RiderTripCell
        
        if let arr_data = arr_SectionTitle[indexPath.section]["row"] as? [[String:Any]] {
            cell.lbl_Title.text = arr_data[indexPath.row]["name"] as? String ?? ""
            cell.img_Icon?.image = arr_data[indexPath.row]["icon"] as? UIImage ?? #imageLiteral(resourceName: "trip_where_yellow")
        }
        
        switch(indexPath.section){
        case 1:
            if indexPath.row == 0 {
                let count = RiderManager.sharedInstance.allCounts?.reservationFutureTripCount ?? 0
                cell.lbl_Count.isHidden = true
                if count > 0 {
                    cell.lbl_Count.isHidden = false
                    cell.lbl_Count.text = "\(count)"
                }
            }else if indexPath.row == 1 {
                let count = RiderManager.sharedInstance.allCounts?.serviceRequestFutureTripCount ?? 0
                cell.lbl_Count.isHidden = true
                if count > 0 {
                    cell.lbl_Count.isHidden = false
                    cell.lbl_Count.text = "\(count)"
                }
            }else {
                //FIXME:: update this later1
                cell.lbl_Count.isHidden = true
            }
            break
        case 2:
            if indexPath.row == 0 {
                let count = RiderManager.sharedInstance.allCounts?.reservationPendingTripCount ?? 0
                cell.lbl_Count.isHidden = true
                if count > 0 {
                    cell.lbl_Count.isHidden = false
                    cell.lbl_Count.text = "\(count)"
                }
            }else{
                cell.lbl_Count.isHidden = true
                let count = RiderManager.sharedInstance.allCounts?.serviceRequestPendingTripCount ?? 0
                if count > 0 {
                    cell.lbl_Count.isHidden = false
                    cell.lbl_Count.text = "\(count)"
                }
            }
            break
        default:
            cell.lbl_Count.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headercell = tableView.dequeueReusableCell(withIdentifier: "Rider_HeaderMyTrips") as! RiderTripCell
        headercell.tag = section
        headercell.lbl_HeaderTitle.text = arr_SectionTitle[section]["header"] as? String ?? ""
        
        //*_*_**_ tap for expandable cell
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(sender:)))
        //tap.delegate = self
        headercell.addGestureRecognizer(tap)
        
        switch(section){
        case 1:
            let count = (RiderManager.sharedInstance.allCounts?.serviceRequestFutureTripCount ?? 0) + (RiderManager.sharedInstance.allCounts?.reservationFutureTripCount ?? 0)

            headercell.lbl_HeaderCount.isHidden = true
            if count > 0 {
                headercell.lbl_HeaderCount.isHidden = false
                headercell.lbl_HeaderCount.text = "\(count)"
            }
            break
            
        case 2:
            let count = (RiderManager.sharedInstance.allCounts?.reservationPendingTripCount ?? 0)+(RiderManager.sharedInstance.allCounts?.serviceRequestPendingTripCount ?? 0)
            headercell.lbl_HeaderCount.isHidden = true
            if count > 0 {
                headercell.lbl_HeaderCount.isHidden = false
                headercell.lbl_HeaderCount.text = "\(count)"
            }
            break
            
        default:
            headercell.lbl_HeaderCount.isHidden = true
            break
        }
        
//        if sectionToLoad != nil {
//            if sectionToLoad == section{    // expanded section
                headercell.view_Header.backgroundColor = App_Base_Color
                headercell.lbl_HeaderTitle.textColor = .white
                headercell.lbl_HeaderCount.layer.borderColor = UIColor.white.cgColor
                headercell.lbl_HeaderCount.textColor = .white
//                headercell.lbl_HeaderCount.isHidden = true
                
//            }else{      // collpased section
//                headercell.view_Header.backgroundColor = .white
//                headercell.lbl_HeaderTitle.textColor = .darkGray
//                headercell.lbl_HeaderCount.layer.borderColor = App_Base_Color.cgColor
//                headercell.lbl_HeaderCount.textColor = App_Text_Color
////                headercell.lbl_HeaderCount.isHidden = false
//            }
//        }
        return headercell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if indexPath.section == 0 && indexPath.row == 0 {
            if let arr_data = arr_SectionTitle[indexPath.section]["row"] as? [[String:Any]] {
                if let jrnyType = arr_SectionTitle[indexPath.section]["header"] as? String{
                    let rowName = arr_data[indexPath.row]["name"] as? String ?? ""
                    performSegue(withIdentifier: "reservation_Trips", sender: [jrnyType,rowName])
                }
            }
        }
        else if indexPath.section == 0 && indexPath.row == 2 || indexPath.section == 1 && indexPath.row == 1 {
            if let arr_data = arr_SectionTitle[indexPath.section]["row"] as? [[String:Any]] {
                if let jrnyType = arr_SectionTitle[indexPath.section]["header"] as? String{
                    let rowName = arr_data[indexPath.row]["name"] as? String ?? ""
                    performSegue(withIdentifier: "aggregate_All", sender: [jrnyType,rowName])
                }
            }
        }
        else if indexPath.section == 2 && indexPath.row == 1 {
            if let arr_data = arr_SectionTitle[indexPath.section]["row"] as? [[String:Any]] {
                if let jrnyType = arr_SectionTitle[indexPath.section]["header"] as? String{
                    let rowName = arr_data[indexPath.row]["name"] as? String ?? ""
                    performSegue(withIdentifier: "aggregate_Pending", sender: [jrnyType,rowName])
                }
            }
        }
        else if let arr_data = arr_SectionTitle[indexPath.section]["row"] as? [[String:Any]] {
            if let jrnyType = arr_SectionTitle[indexPath.section]["header"] as? String{
                let rowName = arr_data[indexPath.row]["name"] as? String ?? ""
                performSegue(withIdentifier: "reservation_Trips", sender: [jrnyType,rowName])
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "reservation_Trips"{
            if let controller = segue.destination as?  RiderTripsViewController {
                controller.tripType = sender as? [String]
            }
        }else if segue.identifier == "trip_Instant" {
            if let controller = segue.destination as?  RiderTripsViewController {
                controller.tripType = sender as? [String]
            }
        }
        else if segue.identifier == "aggregate_All" {
            if let controller = segue.destination as?  AggregateTripViewController {
                controller.tripType = sender as? [String]
            }
        }
        else if segue.identifier == "aggregate_Pending" {
            if let controller = segue.destination as?  AggregatePendingTripViewController {
                controller.tripType = sender as? [String]
            }
        }
    }
}
