//
//  RiderProfileViewController.swift
//  Witz
//
//  Created by abhishek kumar on 20/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import GooglePlaces

class RiderProfileViewController: WitzSuperViewController {
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var imgView_Profile: UIImageView!
    @IBOutlet weak var txt_HomeAddress: UITextField!
    @IBOutlet weak var txt_BusinessAddress: UITextField!    
    @IBOutlet weak var view_EditProfile: UIView!
    
    fileprivate var imagePicker = UIImagePickerController()
    var imgToUpload : UIImage?
    var activeTextField = 0
    var backBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(editProfile))
        view_EditProfile.addGestureRecognizer(tap)
//        let tapImage = UITapGestureRecognizer.init(target: self, action: #selector(chooseImage))
//        imgView_Profile.addGestureRecognizer(tapImage)
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.EditProfile
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        setUserDetail()
    }
    
    func setUserDetail() {
        let userDetails  = RiderManager.sharedInstance.riderInfo
        self.lbl_Name.text = userDetails?.fullName ?? ""
//        self.txt_HomeAddress.text = userDetails?.homeAddress ?? ""
//        self.txt_BusinessAddress.text = userDetails?.workAddress ?? ""

        self.txt_HomeAddress.text = RiderManager.sharedInstance.riderLoginData?.homeAddress ?? ""
        self.txt_BusinessAddress.text = RiderManager.sharedInstance.riderLoginData?.workAddress ?? ""
        let imgUrl = "\(kBaseImageURL)\(userDetails?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
    }

    func editProfile(){
        performSegue(withIdentifier: "segue_PersonalInfo", sender: nil)
    }
    
    //MARK:: API call
    func addAddressToServer(data:CLLocationCoordinate2D, type:Int, _ address:String) {
        guard self.checkNetworkStatus() else {return}
        print(data)
        self.showLoader()
        var parameters = ["lat":data.latitude , "lng":data.longitude] as [String : Any]
        var methodName = ""
        if type == 0 {
            methodName = "rider/add_home_address"
            parameters["homeAddress"] = address
        }else {
            methodName = "rider/add_work_address"
            parameters["workAddress"] = address
        }
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: methodName, withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            print(response as Any)
            DispatchQueue.main.async {
                self.hideLoader()
                if response?["statusCode"] == 200 {
                    let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
                    RiderManager.sharedInstance.riderLoginData = riderData
                    self.setUserDetail()
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
    
    func getLatLongUsingAddress(address:String) {
        GeocodeHelper.getLocationFromAddress(from: address) { (returnLoc, info) in
            self.addAddressToServer(data: returnLoc, type: self.activeTextField, address)
        }
    }
    
    func searchAddress(type:String) {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"AddHomeWorkAddressViewController") as! AddHomeWorkAddressViewController
        viewController.addressType = type
        viewController.comingFromProfile = true
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    func changePassword() {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"RiderForgotPasswordViewController") as! RiderForgotPasswordViewController
        RiderManager.sharedInstance.changePasswordFromProfile = true
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    //MARK::IBOutlets actions
    
    @IBAction func action_ChangePassword(_ sender: UIButton) {
        changePassword()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK:: ImagePicker Delegate
extension RiderProfileViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //Image choose
    
    func chooseImage(){
        
        let actionSheet = UIAlertController.init(title: "Choose Image", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (action) in
            
            if (!UIImagePickerController.isSourceTypeAvailable(.camera))
            {self.dismiss(animated: true, completion: nil)
            }
            else{
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let gallery  = UIAlertAction.init(title: "Gallery", style: .default) { (action) in
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel  = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]) {
        
        if let possibleImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imgView_Profile.image = possibleImage
            imgToUpload = possibleImage
        } else if let possibleImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgView_Profile.image = possibleImage
            imgToUpload = possibleImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension RiderProfileViewController:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField.tag
        if activeTextField == 0 {
            searchAddress(type: "Add home address")
        }else {
            searchAddress(type: "Add work address")
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        guard !trimmed.isEmpty else {
            textField.text=""
            return
        }
    }
 }

extension RiderProfileViewController: GMSAutocompleteViewControllerDelegate {
    
    func openGooglePlacePrompt(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        //autocompleteController.tableCellBackgroundColor = UIColor.cyan
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red: 79.0/255, green: 89.0/255, blue: 89.0/255, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        
        //UISearchBar.appearance().barStyle = UIBarStyle.default
        UISearchBar.appearance().barTintColor = UIColor.white
        //UISearchBar.appearance().setTextColor = UIColor.white
        
        let search = UISearchBar()
        //        search.setNewcolor(color: UIColor.white)
        for subView: UIView in search.subviews {
            for secondLevelSubview: UIView in subView.subviews {
                if (secondLevelSubview is UITextField) {
                    let searchBarTextField = secondLevelSubview as? UITextField
                    //set font color here
                    searchBarTextField?.textColor = UIColor.white
                    break
                }
            }
        }
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if activeTextField == 0{
        txt_HomeAddress.text = place.formattedAddress ?? place.name
        }else{
          txt_BusinessAddress.text = place.formattedAddress ?? place.name
        }

        self.getLatLongUsingAddress(address: place.formattedAddress ?? place.name)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}



extension RiderProfileViewController:ButtonCellDelegate {
    func validation () -> Bool{
        
        if  txt_HomeAddress.text?.isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyAddress , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        if  txt_BusinessAddress.text?.isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyAddress , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
      
        return true
    }
    
    func saveButtonClicked(sender: UIButton) {
        if validation() == true{
//            saveDriverDetail()
        }
    }
    //MARK::API's call
//    fileprivate func saveDriverDetail() {
//        view.endEditing(true)
//        guard self.checkNetworkStatus() else {
//            return
//        }
//
//        var parameters =   ["fullName":arrValues[0],"email":arrValues[1] , "address":arrValues[3]] as [String:Any]
//
//        parameters["acc_type"] = DriverManager.sharedInstance.user_Driver.acc_type ?? "individual"
//
//        if  DriverManager.sharedInstance.user_Driver.acc_type?.contains("corporate") == true{
//            parameters["company_name"] = DriverManager.sharedInstance.user_Driver.company_name ?? ""
//        }
//
//        var arrImg : [UIImage]? = nil
//        if imgToUpload != nil{
//            parameters["pic"] = imgToUpload
//            arrImg = [UIImage]()
//            arrImg?.append(imgToUpload!)
//        }
//
//        showLoader()
//        WitzConnectionManager().makeAPICall(functionName: "driver/add_personal_information", withInputString: parameters, requestType: .put, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.Form,arr_img:arrImg,success: { (response) in
//            self.hideLoader()
//            //            print(response)
//
//            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
//
//                self.navigationController?.popViewController()
//            }, controller: self)
//
//            let driver = DriverInfo.parseDriverInfoJson(data1: response)
//            DriverManager.sharedInstance.user_Driver = driver
//
//        }, failure: { (error) in
//            self.hideLoader()
//            print(error.debugDescription)
//        })
//    }
}

