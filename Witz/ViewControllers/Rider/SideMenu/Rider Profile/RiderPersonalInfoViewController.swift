//
//  RiderPersonalInfoViewController.swift
//  Witz
//
//  Created by abhishek kumar on 20/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher

class RiderPersonalInfoViewController: WitzSuperViewController {

    @IBOutlet weak var lbl_EditProfile: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var imgView_Profile: UIImageView!
    @IBOutlet weak var txt_Name: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var txt_Mobile: UITextField!
    
    fileprivate var imagePicker = UIImagePickerController()
    var imgToUpload : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        

        let tapImage = UITapGestureRecognizer.init(target: self, action: #selector(chooseImage))
        imgView_Profile.addGestureRecognizer(tapImage)
        setUserDetail()
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.EditProfile
        self.lbl_EditProfile.text = LinkStrings.TitleForView.EditProfile

    }

    func setUserDetail() {
        let userDetails  = RiderManager.sharedInstance.riderInfo
        self.lbl_Name.text = userDetails?.fullName ?? ""
        self.txt_Name.text = userDetails?.fullName ?? ""
        self.txt_Mobile.text = userDetails?.mobile ?? ""
        self.txt_Email.text = userDetails?.email ?? ""
        let imgUrl = "\(kBaseImageURL)\(userDetails?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func saveProfile(_ sender: UIButton) {
        saveRiderDetail()
    }
    
    //MARK::API's call
    fileprivate func saveRiderDetail() {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {
            return
        }
        
        var parameters =   ["fullName":txt_Name.text ?? "" ,"email":txt_Email.text ?? "" ] as [String:Any]
      var arrImg : [UIImage]? = nil
        if imgToUpload != nil{
            parameters["pic"] = imgToUpload
            arrImg = [UIImage]()
            arrImg?.append(imgToUpload!)
        }
       
        showLoader()
        WitzConnectionManager().makeAPICall(functionName: "rider/update_profile", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.Form,arr_img:arrImg,success: { (response) in
            self.hideLoader()
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:response?["message"].stringValue ?? "" , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
                self.navigationController?.popViewController()
            }, controller: self)
            
            let rider = RiderInfo.parseRiderInfoJson(data1: response)
            RiderManager.sharedInstance.riderInfo = rider
            
            //**** TODO::Dec20******
            let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
            RiderManager.sharedInstance.riderLoginData = riderData
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
}

//MARK:: ImagePicker Delegate
extension RiderPersonalInfoViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //Image choose
    
    func chooseImage(){
        
        let actionSheet = UIAlertController.init(title: "Choose Image", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction.init(title: "Camera", style: .default) { (action) in
            
            if (!UIImagePickerController.isSourceTypeAvailable(.camera))
            {self.dismiss(animated: true, completion: nil)
            }
            else{
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let gallery  = UIAlertAction.init(title: "Gallery", style: .default) { (action) in
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancel  = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]) {
        
        
        if let possibleImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imgView_Profile.image = possibleImage
            imgToUpload = possibleImage
        } else if let possibleImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgView_Profile.image = possibleImage
            imgToUpload = possibleImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
