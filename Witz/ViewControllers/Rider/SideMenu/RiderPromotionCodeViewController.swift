//
//  RiderPromotionCodeViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 11/29/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

protocol RiderPromotionCodeDelegate : NSObjectProtocol{
    func referCodeRecieved(_ str: String)
}

class RiderPromotionCodeViewController: WitzSuperViewController {
    
    @IBOutlet weak var txtOne: SquareTextField!
    @IBOutlet weak var txtTwo: SquareTextField!
    @IBOutlet weak var txtThree: SquareTextField!
    @IBOutlet weak var txtFour: SquareTextField!
    @IBOutlet weak var txtFive: SquareTextField!
    @IBOutlet weak var txtSix: SquareTextField!
    @IBOutlet weak var btnSend: UIButton!
    
    var delegate : RiderPromotionCodeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txtOne.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func applyPromotionCode(promoCode:String) {
        showLoader()
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/applyPromoCode", withInputString: ["promoCode":promoCode], requestType: .put, isAuthorization: true, success: { (response) in
            self.hideLoader()
            if response?["statusCode"] == 200 {
            }else {
                AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: response?["message"].string ?? "", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                })
            }
        }) { (error) in
            self.hideLoader()
            print(error?.localizedDescription as Any)
        }
    }
    
    func createOTPString() -> String {
        let compOTPStr = txtOne.text! + txtTwo.text! + txtThree.text! + txtFour.text! + txtFive.text! + txtSix.text!
        let appendString = compOTPStr
        return appendString
    }
    
    func toggleSendBtnState(_ enable:Bool) {
        if enable {
            self.btnSend.alpha = 1.0
            self.btnSend.isEnabled = true
        }else {
            self.btnSend.alpha = 0.5
            self.btnSend.isEnabled = false
        }
    }
    
    @IBAction func action_SendBtn(_ sender: UIButton) {
        if delegate != nil {
            delegate?.referCodeRecieved(createOTPString())
            self.navigationController?.popViewController(animated: true)
            return
        }
        applyPromotionCode(promoCode: createOTPString())
    }
    
    @IBAction func action_EditingChange(_ sender: SquareTextField) {
        
        switch sender.tag {
        case 1:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                txtTwo.becomeFirstResponder()
            }
            break
            
        case 2:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                txtThree.becomeFirstResponder()
            }else{
                txtOne.becomeFirstResponder()
            }
            break
            
        case 3:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                txtFour.becomeFirstResponder()
            }else{
                txtTwo.becomeFirstResponder()
            }
            break
            
        case 4:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                txtFive.becomeFirstResponder()
            }else{
                txtThree.becomeFirstResponder()
            }
            break
        case 5:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                txtSix.becomeFirstResponder()
            }else{
                txtFour.becomeFirstResponder()
            }
            break
        case 6:
            if (sender.text?.length)! > 0 && (sender.text != " ") {
                txtSix.resignFirstResponder()
            }else{
                txtFive.becomeFirstResponder()
            }
            break
            
        default:
            break
        }
    }
    
    deinit {
    }
}

extension RiderPromotionCodeViewController:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var pinFlag = true
        switch textField.tag {
        case 1:
            pinFlag = true
            break
            
        case 2:
            if self.txtOne.text == "" {
                pinFlag = false
            }
            break
            
        case 3:
            if self.txtOne.text == "" || self.txtTwo.text == "" {
                pinFlag = false
            }
            break
            
        case 4:
            if self.txtOne.text == "" || self.txtTwo.text == "" || self.txtThree.text == "" {
                pinFlag = false
            }
            break
        case 5:
            if self.txtOne.text == "" || self.txtTwo.text == "" || self.txtThree.text == "" || self.txtFour.text == "" {
                pinFlag = false
            }
            break
        case 6:
            if self.txtOne.text == "" || self.txtTwo.text == "" || self.txtThree.text == "" || self.txtFour.text == "" || self.txtFive.text == ""{
                pinFlag = false
            }
            break
            
        default:
            break
        }
        return pinFlag
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmed = (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        
        guard !trimmed.isEmpty else {
            textField.text = ""
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        let limitLength = 1
        let newLength = text.count + string.count - range.length
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        toggleSendBtnState(false)
        if textField.tag == 6 {
            if newLength > 0 {toggleSendBtnState(true)}
            else {toggleSendBtnState(false)}
        }
        
        return allowedCharacters.isSuperset(of: characterSet) && newLength <= limitLength
    }
}
