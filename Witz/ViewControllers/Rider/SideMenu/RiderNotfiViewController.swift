//
//  RiderNotfiViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON

class RiderNotfiViewController: WitzSuperViewController {
    
    @IBOutlet weak var barBtn_clearAll: UIBarButtonItem!
    @IBOutlet weak var view_Notification: UIView!
    @IBOutlet weak var table_View: UITableView!
    fileprivate var arr_Notification = [JSON]()
    var refreshControl = UIRefreshControl()
    final let kConstLimit = 20
    var limit = 0
    var totalNotification = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.setNavigationBarItem()
        table_View.addSubview(refreshControl)
        refreshControl.tintColor = App_Base_Color
        refreshControl.addTarget(self, action: #selector(refreshThruPullDown), for: UIControlEvents.valueChanged)
        loadRiderNotificationData(skip:0,limit: "\(kConstLimit)")
        
        barBtn_clearAll.setTitleTextAttributes([NSFontAttributeName:  UIFont.systemFont(ofSize: 14)], for: UIControlState.normal)
        
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.Notification
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshThruPullDown(){
        loadRiderNotificationData(skip: 0, limit: "\(kConstLimit)")
    }
    
    //MARK:: API call
    fileprivate func loadRiderNotificationData(skip:Int ,limit:String){
        guard self.checkNetworkStatus() else {return}
        showLoader()
        let parameters = ["skip": "\(skip)" , "limit":limit]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/getNotifications", withInputString: parameters, requestType: .put, isAuthorization: true, success: { (response) in
            print("notification-->\(String(describing: response))")
            self.hideLoader()
            self.refreshControl.endRefreshing()
            print("notificationsData--?\(String(describing: response))")
            if let arr = response?["data"]["notificationData"].array{
                if skip > 0 {
                    for modal in arr{
                        self.arr_Notification.append(modal)
                    }
                }else{
                    self.arr_Notification = arr
                }
                self.limit = self.arr_Notification.count
                self.totalNotification = response?["data"]["count"].int ?? self.kConstLimit
                self.table_View.reloadData()
            }
            
        }) { (error ) in
            self.hideLoader()
            self.refreshControl.endRefreshing()
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func clearAll(_ sender: UIBarButtonItem) {

        guard self.arr_Notification.count > 0 else {return}
        
        AlertHelper.displayAlert(title: LinkStrings.App.AppName, message: LinkStrings.Notifications.WantDelete, style: .alert, actionButtonTitle: [LinkStrings.OK,LinkStrings.Cancel], completionHandler: { (action) in
            
            print(action.title ?? "")
            
            if action.title == LinkStrings.OK{
                guard self.checkNetworkStatus() else {return}
                self.showLoader()
                WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/deleteALLNotification", withInputString: ["":""], requestType: .put, isAuthorization: true, success: { (response) in
                    self.hideLoader()
                    if response?["statusCode"] == 200 {
                        self.arr_Notification.removeAll()
                        DispatchQueue.global(qos: .background).async {
                            Utility.getLoggedRiderDetail({ (complete) in
                            })
                        }
                        self.table_View.reloadData()
                    }
                    
                }) { (error ) in
                    self.hideLoader()
                }
            }
            
        }, controller: self)
    }
}

extension RiderNotfiViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arr_Notification.count >  0 {
            view_Notification.isHidden = true
        }
        else{
            view_Notification.isHidden = false
        }
        return arr_Notification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellNotificationRider", for: indexPath) as! CellNotification
        cell.configureCellForRider(arr_Notification[indexPath.row], index: indexPath)
        cell.btn_Clear.addTarget(self, action: #selector(clearNotification(_: )), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == limit-1{
            if limit >= totalNotification{return}
            limit = limit+kConstLimit
            loadRiderNotificationData(skip: limit-kConstLimit, limit: "\(limit)")
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let modal = arr_Notification[indexPath.row]
        DispatchQueue.global(qos: .background).async {
            self.markAsRead(id: modal["_id"].string ?? "", rowTapped: indexPath.row)
        }
        handleNotificationTapped(modal: modal, rowTapped: indexPath.row)
    }
    
    func clearNotification(_ sender: UIButton){
        guard self.checkNetworkStatus() else {return}
        showLoader()
        let notifId = arr_Notification[sender.tag]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/deleteNotification", withInputString: ["notificationId":notifId["_id"].string ?? ""], requestType: .put, isAuthorization: true, success: { (response) in
            self.hideLoader()
            print(response ?? "")
            if response?["statusCode"] == 200 {
                self.arr_Notification.remove(at: sender.tag)
                self.table_View.reloadData()
            }
            
        }) { (error ) in
            print(error?.localizedDescription ?? "")
            self.hideLoader()
        }
    }
    
    /**
     Mark as Read Notification
     */
    
    fileprivate func markAsRead(id:String , rowTapped:Int){
        guard self.checkNetworkStatus() else {return}
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/readNotification", withInputString: ["notificationId":id], requestType: .put, isAuthorization: true, success: { (response) in
            var modal = self.arr_Notification[rowTapped]
            modal["isRead"] = true
            self.arr_Notification.remove(at: rowTapped)
            self.arr_Notification.insert(modal, at: rowTapped)
            DispatchQueue.main.async {
                self.table_View.reloadRows(at: [IndexPath.init(row: rowTapped, section: 0)], with: .fade)
            }
        }) { (error ) in
        }
    }
    
    /**
     Handle Notification as per different type
     */
    
    fileprivate func handleNotificationTapped(modal:JSON , rowTapped:Int) {
        
        let storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Rider, bundle: nil)
       let notificationFor = Int(modal["notificationCode"].string ?? "0")!
                switch (notificationFor){
                case NotificationType.OFFER_PRICE_NOT_SEND_BY_DRIVER.rawValue://27
                   var strMsg = ""
                    if modal["tripId"] == JSON.null {
                        if modal["notificationCode"].string == "27" || modal["notificationCode"].string == "9" {
                            if modal["notificationCode"].string == "27" {
                                WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/deleteNotification", withInputString: ["notificationId":modal["_id"].string ?? ""], requestType: .put, isAuthorization: true, success: { (response) in
                                }, failure: { (error) in
                                })
                            }
                        }
                        
                    }else {
                        strMsg = "\(modal["tripId"]["localbookingDateTime"].string?.convertDateFormatterWithTimeZone(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                        if modal["notificationCode"].string == "27" || modal["notificationCode"].string == "9" {
                            if modal["notificationCode"].string == "27" {
                                WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/deleteNotification", withInputString: ["notificationId":modal["_id"].string ?? ""], requestType: .put, isAuthorization: true, success: { (response) in
                                    
                                }, failure: { (error) in
                                })
                            }
                        }
                    }
                   
                   strMsg.append("\n"+(modal["tripId"].string ?? LinkStrings.RiderReservation.NoOffer))
                   
                    AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:strMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action) in
                    }, controller: self)
                    break
                    
                case NotificationType.SEND_OFFER_PRICE.rawValue://9
                    self.performSegue(withIdentifier: "segue_BidInfo", sender: modal["tripId"].dictionaryValue["_id"]?.stringValue ?? "")
                    
                    break
                case NotificationType.RIDE_CANCEL_BY_DRIVER.rawValue://20
                    
                    var timeCancel = "of " + (modal["tripId"]["bookingDate"].string?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")
                    if let tripType = modal["tripType"].string{
                        
                        if tripType == "1"{
                          timeCancel = "on " + (modal["notificationDate"].string?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")
                        }
                    }
                    AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:"\(modal["driverId"]["personal_information"]["fullName"].string?.capitalized.nameFormatted ?? "" ) has cancelled Trip \(timeCancel)", style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action) in
                    }, controller: self)
                    break
                    
                case NotificationType.RIDER_AGGREGATE_CONFIRM_BY_MANAGER.rawValue ://31
                    
                    AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderNotificationsMsg.AggreagteAssign, style: .alert, actionButtonTitle: "Done", completionHandler: { (action) in
                    }, controller: self)
                    
                    break
                    
                case NotificationType.AGGREGATE_BID_OFFERS_RIDER.rawValue :
                    
                    let modalFinal = AggreateRequestBidModal(json:modal)
                    
                    if (modalFinal.tripId?.aggregatePaymentStatus ?? 0) == 1{
                        print("payment done")
                         let storyBoardAgg = UIStoryboard(name: LinkStrings.StoryboardName.RiderAggregate, bundle: nil)
                        let controller = storyBoardAgg.instantiateViewController(withIdentifier: "AggregatePaymentRecievedViewController") as! AggregatePaymentRecievedViewController
                        controller.modalRecieve = modalFinal
                        controller.isServerDataRequired = true
                        self.navigationController?.pushViewController(controller)
                    }else{
                        let storyBoardAgg = UIStoryboard(name: LinkStrings.StoryboardName.RiderAggregate, bundle: nil)
                        if let rootViewController =  storyBoardAgg.instantiateViewController(withIdentifier: "ServiceReuestAggregateRiderViewController") as? ServiceReuestAggregateRiderViewController {
                            let navController = UINavigationController.init(rootViewController: rootViewController)
                            navController.navigationBar.barTintColor = App_Text_Color
                            rootViewController.modalAggregate = modalFinal
                            AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
                        }
                    }
                   break
                    
                case NotificationType.DRIVER_REACHED_STARTING_LOCATION.rawValue:
//                    let dict =  modal["data"] as? [String : Any] ?? ["":""]
                    if let driver_id = modal["driverId"]["_id"].string {
                        RiderManager.sharedInstance.bookedDriverId = driver_id
                    }
                    if let tripData = modal["tripId"].dictionary{
                        if let tripType = tripData["tripType"]?.string{
                            self.fillDriverInfo(data: modal)
                            if tripType == "3"{ //aggregate
                                guard let tripID = tripData["_id"]?.string else {return}
                                RiderManager.sharedInstance.tripIDForRider = tripID
                                if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "RiderOnTripViewController") as? RiderOnTripViewController {
                                    let navController = UINavigationController.init(rootViewController: rootViewController)
                                    navController.navigationBar.barTintColor = App_Text_Color
                                    rootViewController.aggregateRideOn = true
                                    if let pushTime = modal["notificationDate"].string?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                                        let currentTime = Date()
                                        rootViewController.timePassed = Int(currentTime.secondsSince(pushTime))}
                                    AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
                                }
                                return
                            }
                            if tripType == "2"{ //reservation
                                guard let tripID = tripData["_id"]?.string else {return}
                                RiderManager.sharedInstance.tripIDForRider = tripID
                                if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "RiderOnTripViewController") as? RiderOnTripViewController {
                                    if let pushTime = modal["notificationDate"].string?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                                        let currentTime = Date()
                                        rootViewController.timePassed = Int(currentTime.secondsSince(pushTime))}
                                    let navController = UINavigationController.init(rootViewController: rootViewController)
                                    navController.isNavigationBarHidden = true

                                    //                            navController.navigationBar.barTintColor = App_Text_Color
                                    AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
                                }
                                return
                            }
                            if tripType == "1"{ // instant
                                guard let tripID = tripData["_id"]?.string else {return}
                                RiderManager.sharedInstance.tripIDForRider = tripID
                                if let rootViewController =  storyBoard.instantiateViewController(withIdentifier: "RiderOnTripViewController") as? RiderOnTripViewController {
                                    let navController = UINavigationController.init(rootViewController: rootViewController)
                                    if let pushTime = modal["notificationDate"].string?.getDateFromString(formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ"){
                                        let currentTime = Date()
                                        rootViewController.timePassed = Int(currentTime.secondsSince(pushTime))}
                                    rootViewController.aggregateRideOn = true
                                    //                            navController.navigationBar.barTintColor = App_Text_Color
                                    navController.isNavigationBarHidden = true
                                    AppDelegate.sharedInstance().window?.currentViewController()?.present(navController, animated: false, completion: { })
                                }
                                return
                            }

                        }
                    }
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideTime), object: nil)
                    
                    break
                default:
                    break
                }
        self.table_View.reloadData()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_TripTwo"{
            if let controller = segue.destination as? NotificationMessageViewController {
                let notifCode = sender as? Int ?? 0
                controller.notifCode = notifCode
            }
        }else if segue.identifier == "segue_BidInfo"{
            if let controller = segue.destination as? ReviewBidsViewController {
                let tripId = sender as? String
                controller.tripID = tripId
            }
        }
    }
    
    fileprivate func fillDriverInfo(data:JSON) {
        if let result:[String:Any] = data.dictionary{
        //        print("data hereqqqqq-----***** \(result)")
        
        let message = result["message"] as? [String:Any]
        let detail = message!["data"] as? [String:Any] ?? ["":""]
        let personalInfo = detail["personal_information"] as? [String:Any] ?? ["":""]
        let photo = detail["pic"] as? [String:Any] ?? ["":""]
        let vehicleInfo = detail["vehicle_information"] as? [String:Any] ?? ["":""]
        let dict = message!["tripData"] as? [String:Any] ?? ["":""]
        let driverMobile = detail["mobile"] as? String ?? ""
        let valueName = (personalInfo["fullName"] as? String ?? "").nameFormatted
        let valueModel = vehicleInfo["model"] as? String ?? ""
        let valuePlateNumber = vehicleInfo["vehicle_number"] as? String ?? ""
        let valueRating = "\((detail["avgRating"] as? Double ?? 0).rounded())"
        if let driverID = detail["_id"] as? String {
            RiderManager.sharedInstance.bookedDriverId = driverID
        }
        let km = "\((dict["driverReachedKM"] as? Double ?? 0)/1000)"
        let time = "\((dict["driverReachedTime"] as? Double ?? 0)/60)"
        let imgUrl = "\(kBaseImageURL)\(photo["original"] ?? "")"
        RiderManager.sharedInstance.bookedDriverProfile =  DriverBasicModal.init(name: valueName , image: imgUrl, km: km , min:  time ,rating: valueRating ,vehicleName: valueModel ,vehicleNumber: valuePlateNumber ,mobileNumber: driverMobile)
        }
    }
}

