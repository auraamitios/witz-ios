//
//  RiderLeftMenuViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Kingfisher

enum RiderLeftMenu: Int {
    case HOME = 0
    case INSTANT
    case RESERVATION
    case AGGREGATE
    case TRIPS
    case WALLET
    case NOTIFICATION
    case INVITE
    case LOGOUT
}

protocol RiderLeftMenuProtocol : class {
    func changeViewController(_ menu: RiderLeftMenu)
}
class RiderLeftMenuViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var tblRiderMenu: UITableView!
    @IBOutlet weak var lbl_Profile_Version: UILabel!
    
    @IBOutlet weak var imgView_Profile: UIImageView!
    var objConnectionManager = RiderConnectionManager()
   
    var menus = [["show":false,"name":LinkStrings.RiderSideMenuItem.HOME,"icon":#imageLiteral(resourceName: "my_home_address_white")],["show":false,"name":LinkStrings.RiderSideMenuItem.INSTANT,"icon":#imageLiteral(resourceName: "menu_dashboard_instant_transfer")], ["show":false,"name":LinkStrings.RiderSideMenuItem.RESERVATION,"icon":#imageLiteral(resourceName: "set_reservation_time")],["show":false,"name":LinkStrings.RiderSideMenuItem.AGGREGATE,"icon":#imageLiteral(resourceName: "aggregate_white")],["show":true,"name":LinkStrings.RiderSideMenuItem.TRIPS,"icon":#imageLiteral(resourceName: "menu_dashboard_trips")] ,["show":false,"name":LinkStrings.RiderSideMenuItem.MYWALLET,"icon":#imageLiteral(resourceName: "menu_dashboard_my_wallet")],["show":true,"name":LinkStrings.RiderSideMenuItem.NOTIFICATION,"icon":#imageLiteral(resourceName: "menu_dashboard_messages")],["show":false,"name":LinkStrings.RiderSideMenuItem.INVITE,"icon":#imageLiteral(resourceName: "menu_dashboard_invite")],["show":false,"name":LinkStrings.RiderSideMenuItem.LOGOUT,"icon":#imageLiteral(resourceName: "menu_dashboard_log_out")]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.bgLogoImageView.isHidden = true
        self.objConnectionManager.delegate = self
        
        lbl_Profile_Version.text = "PROFILE VIEW (v.\(versionBuild))"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppManager.sharedInstance.loggedUserRider = true
        setUserDetail()
        DispatchQueue.global(qos: .background).async {
            Utility.updateRiderLocation()
            Utility.getLoggedRiderDetail({ (complete) in
               })
            print("location update API called *****************")
        }
    }
    
    func setUserDetail() {
        let userDetails  = RiderManager.sharedInstance.riderInfo
        self.lbl_Name.text = userDetails?.fullName ?? ""
        let imgUrl = "\(kBaseImageURL)\(userDetails?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
        tblRiderMenu.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func view_Profile(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let tripsVC = storyboard.instantiateViewController(withIdentifier: "RiderProfileViewController")
        self.slideMenuController()?.changeMainViewController(tripsVC, close: true)
    }
    
    @IBAction func action_Help(_ sender: Any) {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let helpVC = storyboard.instantiateViewController(withIdentifier: "HelpViewController")
        self.slideMenuController()?.changeMainViewController(helpVC, close: true)
    }
    
    @IBAction func action_Contact(_ sender: Any) {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let contactVC = storyboard.instantiateViewController(withIdentifier: "ContactUsViewController")
        self.slideMenuController()?.changeMainViewController(contactVC, close: true)
    }
    
    func riderLogout() {
        guard self.checkNetworkStatus() else {return}
        ActivityIndicatorUtil.enableActivityIndicator(self.view, status: nil, mask: SVProgressHUDMaskType.custom, maskColor: SpinnerBGColor, style: SVProgressHUDStyle.dark)
        self.objConnectionManager.APICall(functionName: "rider/logout", withInputString: ["":""], requestType: .put, withCurrentTask: APIType.RIDER_LOGOUT, isAuthorization: true)
    }
    
}

extension RiderLeftMenuViewController : UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = RiderLeftMenu(rawValue: indexPath.section) {
            self.changeViewController(menu)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = RiderLeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .HOME, .INSTANT, .RESERVATION, .AGGREGATE, .TRIPS, .WALLET, .NOTIFICATION, .INVITE, .LOGOUT:
                return RiderMenuCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var ht:CGFloat = 0.0
        if section == 8{
            ht = 25
        }else {
            ht = 8
        }
        return ht
    }
}

extension RiderLeftMenuViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RiderMenuCell.identifier, for: indexPath) as! RiderMenuCell
        let dict = menus[indexPath.section]
        cell.updateCell(with: indexPath, dict["name"] as! String, dict["icon"] as! UIImage ,showNotification:dict["show"] as! Bool)
        return cell
    }
}
extension RiderLeftMenuViewController:RiderLeftMenuProtocol {
    
    func changeViewController(_ menu: RiderLeftMenu) {
        let storyboard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        switch menu {
        case .HOME:
            let homeVC = storyboard.instantiateViewController(withIdentifier: "RootRider")
            self.slideMenuController()?.changeMainViewController(homeVC, close: true)
            break
        case .INSTANT:
//            let tripsVC = storyboard.instantiateViewController(withIdentifier: "InstantTransferViewController")
//            self.slideMenuController()?.changeMainViewController(tripsVC, close: true)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "RootRider") as! UINavigationController
            if let controller =  homeVC.viewControllers[0] as? TransferDashboardViewController{
                controller.typeSelected = RideTypes.Instant
            }
            self.slideMenuController()?.changeMainViewController(homeVC, close: true)
            break
        case .RESERVATION:
//            let walletVC = storyboard.instantiateViewController(withIdentifier: "ReservationViewController")
//            self.slideMenuController()?.changeMainViewController(walletVC, close: true)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "RootRider") as! UINavigationController
            if let controller =  homeVC.viewControllers[0] as? TransferDashboardViewController{
               controller.typeSelected = RideTypes.Reservation
            }
            self.slideMenuController()?.changeMainViewController(homeVC, close: true)
            break
        case .AGGREGATE:
            let homeVC = storyboard.instantiateViewController(withIdentifier: "RootRider") as! UINavigationController
            if let controller =  homeVC.viewControllers[0] as? TransferDashboardViewController{
                controller.typeSelected = RideTypes.Aggregate
            }
            self.slideMenuController()?.changeMainViewController(homeVC, close: true)
            
//            let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.RiderAggregate, bundle: nil)
//            let riderVC : AggregateLandingViewController = storyBoard.instantiateViewController(withIdentifier: "AggregateLandingViewController") as! AggregateLandingViewController
//            let navController = UINavigationController.init(rootViewController: riderVC)
//            navController.navigationBar.barTintColor = App_Text_Color
//            self.slideMenuController()?.changeMainViewController(navController, close: true)
            break
        case .TRIPS:
            let notificationVC = storyboard.instantiateViewController(withIdentifier: "TripsViewController")
            self.slideMenuController()?.changeMainViewController(notificationVC, close: true)
            break
        case .WALLET:
            let inviteVC = storyboard.instantiateViewController(withIdentifier: "MyWalletViewController")
            self.slideMenuController()?.changeMainViewController(inviteVC, close: true)
            break
        case .NOTIFICATION:
            let inviteVC = storyboard.instantiateViewController(withIdentifier: "RiderNotfiViewController")
            self.slideMenuController()?.changeMainViewController(inviteVC, close: true)
            break
        case .INVITE:
            let inviteVC = storyboard.instantiateViewController(withIdentifier: "RiderInviteViewController")
            self.slideMenuController()?.changeMainViewController(inviteVC, close: true)
            break
        case .LOGOUT:
            riderLogout()
            break
            
        }
    }
}
extension RiderLeftMenuViewController:RiderConnectionManagerDelegate {
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        switch serviceType {
        case .RIDER_LOGOUT:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                AppManager.sharedInstance.loggedUserRider = false
                UserDefaultManager.sharedManager.removeValue(key: LinkStrings.UserDefaultKeys.RiderAccessToken)
                Utility.jumpToLoginScreen()
            }else {
                let msg = result["message"].stringValue
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:msg, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                }, controller: self)
            }
            break
        default:
            break
        }
    }
    
    func didFailWithError(sender: AnyObject) {
        ActivityIndicatorUtil.disableActivityIndicator(self.view)
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}

