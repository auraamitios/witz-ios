//
//  NotificationMessageViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/5/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class NotificationMessageViewController: WitzSuperViewController {

    //***** view RideCancelled iVar ********//
    @IBOutlet weak var view_RideCancelled: UIView!
    @IBOutlet weak var lbl_Message: UILabel!
    
    //***** view NoOffer iVar *******//
    @IBOutlet weak var view_NoOffer: UIView!
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var lbl_Msg: UILabel!
    
    var notifCode:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func showRespectiveView() {
        switch notifCode {
        case NotificationType.OFFER_PRICE_NOT_SEND_BY_DRIVER.rawValue?:
            self.view_NoOffer.isHidden = false
            self.view_RideCancelled.isHidden = true
            break
        case NotificationType.RIDE_CANCEL_BY_DRIVER.rawValue?:
            self.view_NoOffer.isHidden = true
            self.view_RideCancelled.isHidden = false
            break
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
