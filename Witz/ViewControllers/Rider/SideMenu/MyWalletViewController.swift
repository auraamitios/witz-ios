//
//  MyWalletViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class MyWalletViewController: WitzSuperViewController {

    @IBOutlet weak var lbl_Credit: UILabel!
    @IBOutlet weak var lbl_Referral: UILabel!
    @IBOutlet weak var lbl_Promotion: UILabel!
    @IBOutlet weak var tbl_Cards: UITableView!
    
    var strTitle = ["ADD A NEW PAYMENT METHOD    "]
    var icons = ["payment_method_credit_card"]
    var arrCards = [Cards]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 100, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.setNavigationBarItem()
        tbl_Cards.tableFooterView = UIView()
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.MyWallet
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getRiderLoginData()
        self.getAllCards()
        DispatchQueue.global(qos: .background).async {
            self.getPaymentGatewayId()
        }
        
    }
    
    func getRiderLoginData() {
        guard Utility.isConnectedToNetwork() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/accesstokenlogin", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            print("response\(String(describing: response))")
            if response?["statusCode"] == 200 {
                //DispatchQueue.main.async {
                let rider = RiderInfo.parseRiderInfoJson(data1: response)
                RiderManager.sharedInstance.riderInfo = rider
                //**** TODO::Dec20******
                let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
                RiderManager.sharedInstance.riderLoginData = riderData
                
                self.showReferralData()
                self.tbl_Cards.reloadData()
                //}
            }
            
        }, failure: { (error) in
            print(error?.localizedDescription as Any)
        })
    }
    
    func showReferralData() {
        self.lbl_Credit.text = "\(RiderManager.sharedInstance.riderLoginData?.totalCredits ?? 0)"
        self.lbl_Referral.text = "\(RiderManager.sharedInstance.riderLoginData?.totalRefferalAmount ?? 0)"
        self.lbl_Promotion.text = "\(RiderManager.sharedInstance.riderLoginData?.promotionTotalCredits ?? 0)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: API call's
    func getPaymentGatewayId() {
        RiderConnectionManager().makeAPICall(functionName: "rider/getPaymentMethod", withInputString: ["":""], requestType: .get, withCurrentTask: .CREATE_RIDER , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            print("response\(String(describing: response))")
            //store paymentGateway
            if let arr_data =  response?["data"].array{
                RiderManager.sharedInstance.riderGatewayDetail = PaymentGateway.init(json: arr_data[0] )
            }
            
        }, failure: { (error) in
            print(error.debugDescription)
        })
    }
    
    func getAllCards(){
        showLoader()
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_cards", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            
            self.arrCards.removeAll()
            if let arr_data =  response?["data"].array{
                if arr_data.count > 0{
                for card in arr_data{
                    let modal = Cards.init(json: card)
                    self.arrCards.append(modal)
                }
                }
            }
            DispatchQueue.main.async {
                self.hideLoader()
                self.tbl_Cards.reloadData()
            }
            
        }, failure: { (error) in
            self.hideLoader()
        })
    }
}

extension MyWalletViewController:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if section == 0 {
            rowCount = self.arrCards.count
        }else {
            rowCount = 1
        }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ListCardCell.identifier, for: indexPath) as! ListCardCell
            cell.configureCell(with: indexPath, (self.arrCards[indexPath.row]))
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CardCell.identifier, for: indexPath) as! CardCell
            cell.configureCell(with: indexPath, strTitle[indexPath.row], icons[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: CardHeaderCell.identifier) as! CardHeaderCell
        headerCell.backgroundColor = UIColor.clear
        switch (section) {
        case 0:
            headerCell.lbl_Title.text = "List of Credit Card Available"
            break
        case 1:
            headerCell.lbl_Title.text = "Add Credit Card"
            break
        default:
            headerCell.lbl_Title.text = "Other"
            break
        }
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 0 {
            self.performSegue(withIdentifier: "wallet_AddCard", sender: nil)
        }else if indexPath.section == 0 {
            let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
            if let controller = storyBoard.instantiateViewController(withIdentifier: "EditCardViewController") as? EditCardViewController{
                controller.card = self.arrCards[indexPath.row]
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}
