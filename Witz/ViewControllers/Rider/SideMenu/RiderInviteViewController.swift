//
//  RiderInviteViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import MessageUI
import SwiftyJSON
class RiderInviteViewController: WitzSuperViewController {

    @IBOutlet weak var btn_ReferralCode: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.setNavigationBarItem()
        self.navigationItem.titleView = nil
        self.navigationItem.title = "INVITE"
       // getRiderInfo()
        self.btn_ReferralCode.setTitle(RiderManager.sharedInstance.riderLoginData?.refferalCode ?? "Refferal code", for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 0.2471, green: 0.2902, blue: 0.2941, alpha: 1.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 0.2471, green: 0.2902, blue: 0.2941, alpha: 1.0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK::API's call
    fileprivate func getRiderInfo() {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/accesstokenlogin", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            print("response\(String(describing: response))")
            if response?["statusCode"] == 200 {
                DispatchQueue.main.async {
                    self.hideLoader()
                    let rider = RiderInfo.parseRiderInfoJson(data1: response)
                    RiderManager.sharedInstance.riderInfo = rider
                    self.btn_ReferralCode.setTitle(rider.refferalCode ?? "", for: .normal)
                    
                    //**** TODO::Dec20******
                    let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
                    RiderManager.sharedInstance.riderLoginData = riderData
                }
            }
            
        }, failure: { (error) in
            print(error.debugDescription)
            self.hideLoader()
        })
    }

    //MARK::IBOutlets actions
    
    @IBAction func action_ReferralCode(_ sender: UIButton) {
        UIPasteboard.general.string = sender.title(for: .normal)
        
        print(UIPasteboard.general.string ?? "Content not copied")
        
        AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Copied.ContentCopied, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
        })
    }
    @IBAction func action_Email(_ sender: UIButton) {

        let mailURL = URL(string: "mailto:")!
        if UIApplication.shared.canOpenURL(mailURL) {
            if MFMailComposeViewController.canSendMail(){
                let mailComposerVC = MFMailComposeViewController()
                mailComposerVC.mailComposeDelegate = self
                mailComposerVC.setToRecipients([""])
                mailComposerVC.setSubject("Referal Code")
                mailComposerVC.setMessageBody("Haven't tried Witz yet? Sign up with my code \(btn_ReferralCode.title(for: .normal)!) and enjoy the most affordable cabs rides!", isHTML: false)
                self.present(mailComposerVC, animated: true, completion: nil)
            }else {
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderSideMenuItem.MailAlert, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action) in
                }, controller: self)
            }
        }
    }
    @IBAction func action_SMS(_ sender: UIButton) {
        if (MFMessageComposeViewController.canSendText()) {
            UIBarButtonItem.appearance().tintColor = UIColor.gray
            let controller = MFMessageComposeViewController()
            controller.body = "Haven't tried Witz yet? Sign up with my code \(btn_ReferralCode.title(for: .normal)!) and enjoy the most affordable cabs rides!"
//            controller.recipients = [""]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    @IBAction func action_Media(_ sender: UIButton) {
        
        let myWebsite = NSURL(string:"http://www.google.com/")
        let str = "your referal code is :\(btn_ReferralCode.title(for: .normal)!)"
        let img: UIImage = #imageLiteral(resourceName: "witz_logo_bg")
        
        guard let url = myWebsite else {
            print("nothing found")
            return
        }
        let shareItems:Array = [img, url,str] as [Any]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}

extension RiderInviteViewController : MFMessageComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
        self.dismiss(animated: true, completion: nil)
    }
}


extension RiderInviteViewController : MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
        self.dismiss(animated: true, completion: nil)
    }
}
