//
//  PinHomeWorkAddressViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/2/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
class PinHomeWorkAddressViewController: WitzSuperViewController {

    var addressType:String?
    var isProfileScreen:Bool?
    var coord:[Double]?
    var finalAddress:String?
    let locManager = CLLocationManager()
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var btn_SetAddress: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        self.btn_SetAddress.setTitle(addressType ?? "Set address", for: .normal)
        self.btn_SetAddress.isEnabled = false
        self.btn_SetAddress.alpha = 0.5
        locManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
            })
            return
        }
        fetchLocation()
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locManager.stopUpdatingLocation()
    }
    @IBAction func action_SetAddress(_ sender: UIButton) {
        addAddressToServer(data: coord!, finalAddress!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func fetchLocation() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        //locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        locManager.requestLocation()
        showLoader()
    }
    
    func backToReserveAddress() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is RiderJourneyPlanViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    func backToProfileScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is RiderProfileViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    //MARK:: API call
    func addAddressToServer(data:[Double], _ address:String) {
        guard self.checkNetworkStatus() else {return}
        print(data)
        self.showLoader()
        var parameters = ["lat":(data[0]), "lng":(data[1])] as [String : Any]
        var methodName = ""
        if addressType == "Add home address" {
            methodName = "rider/add_home_address"
            parameters["homeAddress"] = address
        }else {
            methodName = "rider/add_work_address"
            parameters["workAddress"] = address
        }
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: methodName, withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            print(response as Any)
            DispatchQueue.main.async {
                self.hideLoader()
                if response?["statusCode"] == 200 {
                    let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
                    RiderManager.sharedInstance.riderLoginData = riderData
                    if self.isProfileScreen ?? false{
                        self.backToProfileScreen()
                    }else {
                        self.backToReserveAddress()
                    }
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PinHomeWorkAddressViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {}
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        AppManager.sharedInstance.dropCoordinates = position
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                self.lbl_Address.text = returnAddress
//                self.addAddressToServer(data: [mapView.camera.target.latitude, mapView.camera.target.longitude], returnAddress)
                self.coord = [mapView.camera.target.latitude, mapView.camera.target.longitude]
                self.finalAddress = returnAddress
                RiderManager.sharedInstance.dropAddress = returnAddress
                self.btn_SetAddress.isEnabled = true
                self.btn_SetAddress.alpha = 1.0
            }
        })
    }
}

extension PinHomeWorkAddressViewController:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.hideLoader()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        AppManager.sharedInstance.locationCoordinates = locations
        if let location = locations.last {
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.hideLoader()
            self.locManager.stopUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.hideLoader()
    }
}
