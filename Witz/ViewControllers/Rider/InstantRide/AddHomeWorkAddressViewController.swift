//
//  AddHomeWorkAddressViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/2/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GooglePlaces
class AddHomeWorkAddressViewController: WitzSuperViewController {

    var addressType:String?
    var comingFromProfile:Bool?
    var backBtn = UIButton()
//    var locationManager = GeocodeManager.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.navigationItem.titleView = nil
        self.navigationItem.title = addressType ?? ""
        
        if comingFromProfile ?? false {
            self.left_barButton.isHidden = true
            self.backBtn = UIButton(type: .custom)
            self.backBtn.setImage(UIImage(named: "Round_back"), for: .normal)
            self.backBtn.addTarget(self, action: #selector(AddHomeWorkAddressViewController.goBack), for: .touchUpInside)
            let item1 = UIBarButtonItem(customView: self.backBtn)
            self.navigationItem.leftBarButtonItem = item1
        }
        
    }
    func goBack() {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func action_PinDestination(_ sender: UIButton) {
        self.performSegue(withIdentifier: "pin_Address", sender: nil)
    }
    @IBAction func action_SearchAddress(_ sender: UIButton) {
        openGooglePlacePrompt()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pin_Address" {
            if let controller = segue.destination as? PinHomeWorkAddressViewController{
                controller.addressType = addressType
                controller.isProfileScreen = comingFromProfile
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //TODO:: pop to specific VC
    func backToSearchAddress() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is SearchAddressViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    func backToProfileScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is RiderProfileViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    //MARK:: API call
    func addAddressToServer(position:CLLocationCoordinate2D, _ address:String) {
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        AppManager.sharedInstance.dropCoordinates = position
        RiderManager.sharedInstance.dropAddress = address
        var parameters = ["lat":(position.latitude), "lng":(position.longitude)] as [String : Any]
        var methodName = ""
        if addressType == "Add home address" {
            methodName = "rider/add_home_address"
            parameters["homeAddress"] = address
        }else {
            methodName = "rider/add_work_address"
            parameters["workAddress"] = address
        }
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: methodName, withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            print(response as Any)
            DispatchQueue.main.async {
                self.hideLoader()
                if response?["statusCode"] == 200 {
                    let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
                    RiderManager.sharedInstance.riderLoginData = riderData
                    if self.comingFromProfile ?? false{
                        self.backToProfileScreen()
                    }else {
                        self.backToSearchAddress()
                    }
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddHomeWorkAddressViewController: GMSAutocompleteViewControllerDelegate {
    
    func openGooglePlacePrompt(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        //autocompleteController.tableCellBackgroundColor = UIColor.cyan
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red: 79.0/255, green: 89.0/255, blue: 89.0/255, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        
        //UISearchBar.appearance().barStyle = UIBarStyle.default
        UISearchBar.appearance().barTintColor = UIColor.white
        //UISearchBar.appearance().setTextColor = UIColor.white
        
        let search = UISearchBar()
        //        search.setNewcolor(color: UIColor.white)
        for subView: UIView in search.subviews {
            for secondLevelSubview: UIView in subView.subviews {
                if (secondLevelSubview is UITextField) {
                    let searchBarTextField = secondLevelSubview as? UITextField
                    //set font color here
                    searchBarTextField?.textColor = UIColor.white
                    break
                }
            }
        }
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("searched address***********\(place.formattedAddress ?? place.name)")
        self.getLatLongUsingAddress(address: place.formattedAddress ?? place.name)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //*********** geocode ************//
    func getLatLongUsingAddress(address:String) {
 
        GeocodeHelper.getLocationFromAddress(from: address) { (returnLoc, info) in
            self.addAddressToServer(position: returnLoc, address)
        }
    }
}
