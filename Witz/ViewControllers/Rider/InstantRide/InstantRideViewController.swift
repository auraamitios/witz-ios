//
//  InstantRideViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/16/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import Alamofire
class InstantRideViewController: WitzSuperViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblCurrentAddress: UILabel!
    @IBOutlet weak var bottomLayout: NSLayoutConstraint!
    @IBOutlet weak var lblTime: UILabel!
    
    var locUpdate:LocationUpdate!
    let objWitzConnectionManager = RiderConnectionManager()
    var drivers:NearByDrivers?
    var riderCoords:CLLocationCoordinate2D?
    var driverCoords:CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.objWitzConnectionManager.delegate = self
        locUpdate = LocationUpdate.sharedInstance
        locUpdate.delegate = self
        locUpdate.startTracking()
        
        bottomLayout.constant = 30 //71
        self.mapView.bringSubview(toFront: addressView)
        //getCurrentAddress()
        locationPermissionCheck()
        
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        showLoader()
        
        //        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        //        DispatchQueue.main.asyncAfter(deadline: when) {
        //            // Your code with delay
        //        }
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
        //            self.hideLoader()
        //         })
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        displayUserCurrentLocation()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locUpdate.stopTracking()
    }
    
    func displayUserCurrentLocation() {
        let locations = AppManager.sharedInstance.locationCoordinates
        guard let coordinates = locations.last?.coordinate else { return}
        self.mapView.camera = GMSCameraPosition(target: coordinates, zoom: 15, bearing: 0, viewingAngle: 0)
        let position = CLLocationCoordinate2D(latitude: (coordinates.latitude), longitude:(coordinates.longitude))
        let marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
        marker.position = position
        marker.title = "Witz"
        marker.map = self.mapView
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon = UIImage(named: "vehicle-car_on-map")
    }
    func locationPermissionCheck() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCurrentAddress(){
        //DispatchQueue.global(qos: .background).async {
        let locations = AppManager.sharedInstance.locationCoordinates
        print(locations)
        let coordinates = locations.last?.coordinate
        print((coordinates?.latitude)!)
        print((coordinates?.longitude)!)
        //let location: CLLocation = CLLocation(latitude: (coordinates?.latitude)!,
        //                                              longitude: (coordinates?.longitude)!)
        ReverseGeocode.getAddressByCoords(coords: coordinates!, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                self.lblCurrentAddress.text = returnAddress
            }
        })
        //}
    }
    
    func jumpToReservationScreen() {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "RiderJourneyPlanViewController") as? RiderJourneyPlanViewController{
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    fileprivate func getPathPoints() {
        hitsLimit_Google = 5
 GooglePathHelper.getPathPolyPoints(from: self.riderCoords!, destination: self.driverCoords!) { (returnPath, distance) in
//            print("waypoints****************\(distance)")
            DispatchQueue.main.async {
                self.getTimeDetail(data: distance)
            }
        }
    }
    
    func getTimeDetail(data:[Any]) {
        if data.count > 0 {
            if let detail = data[0] as? [String:Any]{
                let time = detail["duration"] as? [String:Any]
                let timeStr = time!["text"] as! String
                print("time-->\(timeStr)")
                self.lblTime.text = timeStr
                RiderManager.sharedInstance.nearestDriverTime = timeStr
            }
        }
    }
                
    @IBAction func action_Reservation(_ sender: UIButton) {
        jumpToReservationScreen()
    }
    @IBAction func action_Destination(_ sender: Any) {
        let btn = sender as! UIButton
        switch btn.tag {
        case 1:
            break
        case 2:
            //FIXME:: Dec-4
            startInstantRide()
            // bypass for now
            // self.performSegue(withIdentifier: "Destination", sender: nil)
            break
        default:
            break
        }
    }
    
    @IBAction func action_CurrentLocation(_ sender: Any) {
        locUpdate.startTracking()
    }
    @IBAction func action_BookRide(_ sender: Any) {
        
    }
    
    //MARK::API call
    
    fileprivate func startInstantRide() {
        RiderManager.sharedInstance.navigationControl = self.navigationController
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        let locations = AppManager.sharedInstance.locationCoordinates
        let coordinates = locations.last?.coordinate
        let params: Parameters = ["lat": (coordinates?.latitude)!, "lng": (coordinates?.longitude)!]
        print(params)
        
        self.objWitzConnectionManager.APICall(functionName: "rider/instant", withInputString: params, requestType: .post, withCurrentTask: .START_INSTANT_RIDE, isAuthorization: true)
    }
    
    func getNearByDriversList(position:CLLocationCoordinate2D){
        guard self.checkNetworkStatus() else {return}
        let params = ["lat":(position.latitude), "lng":(position.longitude)] as [String : Any]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/instant", withInputString: params, requestType: .post, isAuthorization: true, success: { (response) in
            print(response ?? "")
            if response?["statusCode"] == 200{
            if let result = response?["data"]{
                self.drivers = NearByDrivers.init(json: result)
                if self.drivers?.driver?.count == 0 {
                    return
                }
                let data = self.drivers?.driver![0]
                if let coord = data?.currentLocation?.coordinates {
                    let lng = coord[0]
                    let lat = coord[1]
                    self.driverCoords = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))
                    self.getPathPoints()
                }
                }
            }
        }, failure: { (error) in
            print(error.debugDescription)
        })
    }
}

//MARK::GMSMapViewDelegate
extension InstantRideViewController:GMSMapViewDelegate {
    
    //called when the map is idle
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
        
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        riderCoords = position
        getNearByDriversList(position: position)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                self.lblCurrentAddress.text = returnAddress
            }
        })
        
        AppManager.sharedInstance.pickUpCoordinates = position
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        let latitute = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        let position = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)
        AppManager.sharedInstance.pickUpCoordinates = position
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                self.lblCurrentAddress.text = returnAddress
            }
        })
    }
}

//MARK::LocationDelegates
extension InstantRideViewController:LocationDelegates {
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            //DispatchQueue.main.async {
            self.mapView.animate(to: GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0))
            let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
                DispatchQueue.main.async {
                    self.lblCurrentAddress.text = returnAddress
                }
            })
            
            let marker = GMSMarker(position: position)
            marker.title = "Witz"
            marker.map = self.mapView
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.icon = UIImage(named: "vehicle-car_on-map")
            self.locUpdate.stopTracking()
            self.hideLoader()
            //}
        }
    }
    
    func didFailToUpdate(error: Error) {
        
    }
}

extension InstantRideViewController:RiderConnectionManagerDelegate {
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        
        self.hideLoader()
        print("reseponseData-->>: \(sender)")
        switch serviceType {
        case .START_INSTANT_RIDE:
            let result = sender as! JSON
            if result["statusCode"] == 200  {
                let data = result["data"].dictionaryValue
                if data["driver"]?.arrayValue.count != 0 {
                    self.performSegue(withIdentifier: "Destination", sender: nil)
                }else {
                    let actionHandler = { (action:UIAlertAction!) -> Void in
                    }
                    AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderAlerts.DriverNotAvailable, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler)
                    return
                }
            }else {
                let actionHandler = { (action:UIAlertAction!) -> Void in
                }
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: result["message"].stringValue, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            }
            break
            
        default:
            break
        }
    }
    
    func didFailWithError(sender: AnyObject) {
        self.hideLoader()
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action: UIAlertAction!) -> Void in }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: error.response_message!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
        else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action: UIAlertAction!) -> Void in }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
        else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}

