//
//  FeedbackForRiderViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 11/15/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos

class FeedbackForRiderViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var imgView_Profile: UIImageView!
    @IBOutlet weak var view_Rating: CosmosView!
    
    var driverStars = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        showContactDeails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    @IBAction func doneFeedback(_ sender: UIButton) {
        guard self.checkNetworkStatus() else {
            return
        }
        showLoader()
        let parameters =   ["tripId": RiderManager.sharedInstance.tripIDForRider ?? "" , "driverId":RiderManager.sharedInstance.bookedDriverId ?? "","driverRating":view_Rating.rating] as [String : Any]
        
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/rate_driver", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            Utility.riderSideMenuSetUp()
            
        }) { (error ) in
            self.hideLoader()
        }
    }
    
    @IBAction func back_Action(_ sender: UIButton) {
        Utility.riderSideMenuSetUp()
    }
    
    fileprivate  func showContactDeails(){
        let driver = RiderManager.sharedInstance.bookedDriverProfile

        lbl_Name.text = driver?.name?.nameFormatted

        let imgUrl = driver?.image ?? ""
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {

                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
    }
}
