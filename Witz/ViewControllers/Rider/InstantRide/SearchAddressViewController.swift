//
//  SearchAddressViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/23/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GooglePlaces
import SwiftyJSON
class SearchAddressViewController: WitzSuperViewController {
    
    @IBOutlet weak var btnDropAdd: UIButton!
    @IBOutlet weak var btnHomeAdd: UIButton!
    @IBOutlet weak var btnWorkAdd: UIButton!
    var addressDict = ["":""]
    var btnTag:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.titleView = nil
        self.navigationItem.title = "WHERE ARE YOU GOING TO"
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showAddressDetails()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_PinDestination(_ sender: UIButton) {
        RiderManager.sharedInstance.homeWorkAddressSelected = false
        if RiderManager.sharedInstance.comingFromReservation ?? false  {
        }else {
           self.performSegue(withIdentifier: "PinDestiny", sender: nil)
        }
    }
    
    func showAddressDetails() {
        self.btnHomeAdd.setTitle(RiderManager.sharedInstance.riderLoginData?.homeAddress ?? "Add home address", for: .normal)
        self.btnWorkAdd.setTitle(RiderManager.sharedInstance.riderLoginData?.workAddress ?? "Add work address", for: .normal)
        
//        if (RiderManager.sharedInstance.riderLoginData?.homeAddress?.isEmpty)! {
//            self.btnHomeAdd.setTitle("Add home address", for: .normal)
//        }
//        if (RiderManager.sharedInstance.riderLoginData?.workAddress?.isEmpty)! {
//            self.btnWorkAdd.setTitle("Add work address", for: .normal)
//        }
        
        if RiderManager.sharedInstance.riderLoginData?.homeAddress == "" {
            self.btnHomeAdd.setTitle("Add home address", for: .normal)
        }
        if RiderManager.sharedInstance.riderLoginData?.workAddress == "" {
            self.btnWorkAdd.setTitle("Add work address", for: .normal)
        }
    }
    @IBAction func action_SearchAddress(_ sender: UIButton) {
        btnTag = sender.tag
        if sender.tag == 1 {
//            RiderManager.sharedInstance.homeWorkAddressSelected = false
            RiderManager.sharedInstance.homeWorkAddressSelected = true

            openGooglePlacePrompt()
        }else {
            if sender.currentTitle == "Add home address" || sender.currentTitle == "Add work address" {
                RiderManager.sharedInstance.homeWorkAddressSelected = true
                self.performSegue(withIdentifier: "add_Address", sender: sender.tag)

            }else {
                //*************** called when user choose Home/Work address *****************//
                RiderManager.sharedInstance.homeWorkAddressSelected = true
                RiderManager.sharedInstance.dropAddress = sender.currentTitle
                GeocodeHelper.getLocationFromAddress(from: sender.currentTitle ?? "", returnData: { (returnLoc, info) in
                    print(returnLoc,info)
                    AppManager.sharedInstance.dropCoordinates = returnLoc
                    self.performSegue(withIdentifier: "PinDestiny", sender: nil)
                })
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "add_Address" {
            let tag = sender as? Int
            if tag == 2 {
                if let controller = segue.destination as?  AddHomeWorkAddressViewController{
                    controller.addressType = "Add home address"
                    controller.comingFromProfile = false
                }
            }else {
                if let controller = segue.destination as?  AddHomeWorkAddressViewController{
                    controller.addressType = "Add work address"
                    controller.comingFromProfile = false
                }
            }
        }
    }
}

extension SearchAddressViewController: GMSAutocompleteViewControllerDelegate {
    
    func openGooglePlacePrompt(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        //autocompleteController.tableCellBackgroundColor = UIColor.cyan
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red: 79.0/255, green: 89.0/255, blue: 89.0/255, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        
        //UISearchBar.appearance().barStyle = UIBarStyle.default
        UISearchBar.appearance().barTintColor = UIColor.white
        //UISearchBar.appearance().setTextColor = UIColor.white
        
        let search = UISearchBar()
        //        search.setNewcolor(color: UIColor.white)
        for subView: UIView in search.subviews {
            for secondLevelSubview: UIView in subView.subviews {
                if (secondLevelSubview is UITextField) {
                    let searchBarTextField = secondLevelSubview as? UITextField
                    //set font color here
                    searchBarTextField?.textColor = UIColor.white
                    break
                }
            }
        }
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("searched address***********\(place.formattedAddress ?? place.name)")
        
        switch btnTag {
        case 1:
            addressDict = ["DestinationAddress":place.formattedAddress ?? place.name]
//            self.btnDropAdd.setTitle(place.formattedAddress ?? place.name, for: .normal)
//            self.getLatLongUsingAddress(address: place.formattedAddress ?? place.name)
            break
        case 2:
            addressDict = ["HomeAddress":place.formattedAddress ?? place.name]
            self.btnHomeAdd.setTitle(place.formattedAddress ?? place.name, for: .normal)
            break
        case 3:
            addressDict = ["WorkAddress":place.formattedAddress ?? place.name]
            self.btnWorkAdd.setTitle(place.formattedAddress ?? place.name, for: .normal)
            break
        default:
            break
        }
        
        AppManager.sharedInstance.dropCoordinates = place.coordinate
        RiderManager.sharedInstance.dropAddress = place.formattedAddress ?? place.name
      
        self.performSegue(withIdentifier: "PinDestiny", sender: nil)
        dismiss(animated: true) {
//            self.getLatLongUsingAddress(address: place.formattedAddress ?? place.name)
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //**************** called in case of search address using Google Autocomplete ****************//
    func getLatLongUsingAddress(address:String) {
        GeocodeHelper.getLocationFromAddress(from: address) { (returnLoc, info) in
            AppManager.sharedInstance.dropCoordinates = returnLoc
            RiderManager.sharedInstance.dropAddress = address
            self.hideLoader()
            self.performSegue(withIdentifier: "PinDestiny", sender: nil)
        }
    }
}
