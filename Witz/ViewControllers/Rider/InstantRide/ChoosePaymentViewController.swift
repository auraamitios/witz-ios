//
//  ChoosePaymentViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/31/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class ChoosePaymentViewController: WitzSuperViewController {

    var fromTrip:Bool?
    var fromTripReserve :Bool?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 100, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        getPaymentGatewayId()
    }
    func getPaymentGatewayId() {
        RiderConnectionManager().makeAPICall(functionName: "rider/getPaymentMethod", withInputString: ["":""], requestType: .get, withCurrentTask: .CREATE_RIDER , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            print("response\(String(describing: response))")
            
            //store paymentGateway
            if let arr_data =  response?["data"].array{
                RiderManager.sharedInstance.riderGatewayDetail = PaymentGateway.init(json: arr_data[0] )
            }
            
        }, failure: { (error) in
            print(error.debugDescription)
        })
    }
    func checkAddedCards() {
        RiderConnectionManager().makeAPICall(functionName: "rider/checkCard", withInputString: ["paymentGatewayId":""], requestType: .put, withCurrentTask: .CREATE_RIDER , isAuthorization: false,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            //let arr = response?["data"].array ?? []
        }, failure: { (error) in
            print(error.debugDescription)
        })
    }
    
    @IBAction func action_AddCard(_ sender: UIButton) {
        self.performSegue(withIdentifier: "AddCard", sender: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AddCard" {
            if let controller = segue.destination as? AddCardViewController {
                controller.fromTrip = self.fromTrip
                controller.fromTripReserve = self.fromTripReserve

            }
        }
        
    }

}
