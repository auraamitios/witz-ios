//
//  AddPaymentViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/31/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class AddPaymentViewController: WitzSuperViewController {

    @IBOutlet weak var tblCards: UITableView!
    
    var strTitle = ["ADD A NEW PAYMENT METHOD ", "ADD A PROMOTION CODE"]
    var icons = ["payment_method_credit_card","promotion_code"]
    var arrCards = [Cards]()

    var fromTrip:Bool?
    var fromTripReserve:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 100, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        tblCards.tableFooterView = UIView()
        tblCards.estimatedSectionHeaderHeight = 40.0
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllCards()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAllCards(){
        showLoader()
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_cards", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            print(response)
            self.arrCards.removeAll()
            if let arr_data =  response?["data"].array{
                if arr_data.count > 0{
                    for card in arr_data{
                        let modal = Cards.init(json: card)
                        self.arrCards.append(modal)
                    }
                }
            }
            DispatchQueue.main.async {
                self.hideLoader()
                self.tblCards.reloadData()
            }
            
        }, failure: { (error) in
            self.hideLoader()
        })
    }
}

extension AddPaymentViewController:UITableViewDelegate,UITableViewDataSource,UITableViewDataSourcePrefetching {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if section == 0 {
            rowCount = self.arrCards.count ?? 0
        }else {
            rowCount = 2
        }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ListCardCell.identifier, for: indexPath) as! ListCardCell
            cell.configureCell(with: indexPath, (self.arrCards[indexPath.row]))
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CardCell.identifier, for: indexPath) as! CardCell
            cell.configureCell(with: indexPath, strTitle[indexPath.row], icons[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: CardHeaderCell.identifier) as! CardHeaderCell
        headerCell.backgroundColor = UIColor.clear
        switch (section) {
        case 0:
            headerCell.lbl_Title.text = "List of Credit Card Available"
            break
        case 1:
            headerCell.lbl_Title.text = "Add Credit Card"
            break
        default:
            headerCell.lbl_Title.text = "Other"
            break
        }
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.performSegue(withIdentifier: "segue_EditCard", sender: indexPath)
        }else if indexPath.section == 1 && indexPath.row == 0 {
            self.performSegue(withIdentifier: "ChoosePayment", sender: nil)
        }else if indexPath.section == 1 && indexPath.row == 1 {
            self.performSegue(withIdentifier: "segue_PromotionCode", sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_EditCard" {
            if let controller = segue.destination as? EditCardViewController {
                if let index = sender as? IndexPath{
                    controller.card = self.arrCards[index.row]
                    controller.fromTrip = self.fromTrip
                    controller.fromTripReserve = self.fromTripReserve
                }
            }
        }else if segue.identifier == "ChoosePayment" {
            if let controller = segue.destination as? ChoosePaymentViewController {
                    controller.fromTrip = self.fromTrip
                controller.fromTripReserve = self.fromTripReserve
   
            }
        }
    }
    
}
