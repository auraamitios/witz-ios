//
//  EditCardViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/7/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
class EditCardViewController: WitzSuperViewController {
    
    @IBOutlet weak var tbl_EditCards: UITableView!
    var icons = ["userIcon","payment_method_credit_card","date_credit_card","cvv_code_credit_card","payment_method_credit_card"]
    var card:Cards?
    var fromTrip:Bool?
    var fromTripReserve :Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 100, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.tbl_EditCards.tableFooterView = UIView()
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.EditCard
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeDefaultCard() {
        guard self.checkNetworkStatus() else {return}
        showLoader()
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/make_default_card", withInputString: ["_id":card?.id ?? ""], requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            if response?["statusCode"] == 200 {
                Utility.getLoggedRiderDetail({ (complete) in
                    DispatchQueue.main.async {
                        if self.fromTrip == true{
                            self.backToTripDestination()
                            return
                        }else if  self.fromTripReserve == true{
                            self.backToTripDestinationReserve()
                            return
                        }
                        self.navigationController?.popViewController(animated: false)
                    }})
            }
        }, failure: { (error) in
            self.hideLoader()
            print(error?.localizedDescription as Any)
        })
    }
    func deleteCard() {
        guard self.checkNetworkStatus() else {return}
        showLoader()
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/delete_card", withInputString: ["_id":card?.id ?? ""], requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            if response?["statusCode"] == 200 {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: false)
                }
            }
        }, failure: { (error) in
            self.hideLoader()
            print(error?.localizedDescription as Any)
        })
    }
    func backToTripDestination() {
        showLoader()
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is PinDestinationViewController {
                    self.hideLoader()
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    func backToTripDestinationReserve() {
        showLoader()
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                self.hideLoader()
                if aViewController is ReservationPinDestinationViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension EditCardViewController:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return icons.count + 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section >= icons.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: EditCardBottomCell.identifier, for: indexPath) as! EditCardBottomCell
            cell.configureCell(with: indexPath)
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: EditCardCell.identifier, for: indexPath) as! EditCardCell
            cell.configureCell(with: indexPath, card!, icons[indexPath.section])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == icons.count{
            return 40
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 5 {
            makeDefaultCard()
        }else if indexPath.section == 6 {
            deleteCard()
        }
    }
}
