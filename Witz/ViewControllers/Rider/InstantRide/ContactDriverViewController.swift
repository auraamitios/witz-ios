//
//  ContactDriverViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 11/15/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher
import SwiftyJSON

class ContactDriverViewController: WitzSuperViewController {
    
    @IBOutlet weak fileprivate var lbl_Name: UILabel!
    @IBOutlet weak fileprivate var imgView_Profile: UIImageView!
    @IBOutlet weak fileprivate var lbl_Rating: UILabel!
    @IBOutlet weak var lbl_Car: UILabel!
    @IBOutlet weak var lbl_CarNo: UILabel!
    @IBOutlet weak var lbl_Distance: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var view_DriverCard: UIView!
    
    @IBOutlet weak var stackView_ArrivalTime: UIStackView!
    var driverData:Any?
    var disableTap : Bool?
    
    static let sharedInstance = ContactDriverViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.CarOnRoad
        
        if driverData != nil{ self.fillDriverInfo(data: driverData ?? "")}else{
            showContactDeails()
        }
        
        if disableTap == nil{
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(moveToNewControllerTapped))
            self.view_DriverCard.addGestureRecognizer(tap)
        }else{
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeContact))
            self.view_DriverCard.addGestureRecognizer(tap)
        }
        
        addLeftNavigationBarButton()
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(moveToNewController), name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideTime), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    func closeContact()  {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cancelRide(){
        
        AlertHelper.displayAlert(title: LinkStrings.RiderObserverKeys.WannaCancel, message: LinkStrings.RiderObserverKeys.RideCancelMsg, style: .alert, actionButtonTitle: [LinkStrings.Yes,LinkStrings.Cancel], completionHandler: { (action) in
            
            print(action.title ?? "")
            
            if action.title == LinkStrings.Yes{
                self.view.endEditing(true)
                guard self.checkNetworkStatus() else {return}
                self.showLoader()
                RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/cancel_ride", withInputString: ["tripId":RiderManager.sharedInstance.tripIDForRider ?? ""], requestType: .post, isAuthorization: true, success: { (response) in
                    print(response ?? "" )
                    self.hideLoader()
                    Utility.riderSideMenuSetUp()
                }) { (error ) in
                    self.hideLoader()
                    AlertHelper.displayAlertViewOKButton(title:  LinkStrings.AlertTitle.Title, message: LinkStrings.RiderAlerts.InternetNotWorking, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Retry, completionHandler: { (action:UIAlertAction!) -> Void in
                        if action.title==LinkStrings.MostCommon.Retry {
                            self.cancelRide()
                        }
                    })
                }
            }
            
        }, controller: self)
    }
    
    //MARK:: Private Method
    func addLeftNavigationBarButton() {
        
        self.left_barButton = UIButton(type: .custom)
        self.left_barButton.setImage(UIImage(named: "Round_back"), for: .normal)
        
        self.left_barButton.addTarget(self, action: #selector(ContactDriverViewController.cancelRide), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: self.left_barButton)
        self.navigationItem.leftBarButtonItem = item1
    }
    fileprivate  func showContactDeails(){
        let driver = RiderManager.sharedInstance.bookedDriverProfile
        lbl_Name.text = driver?.name?.nameFormatted
        lbl_Rating.text = driver?.rating
        lbl_Car.text = driver?.vehicleName
        lbl_CarNo.text = driver?.vehicleNumber
        let imgUrl = driver?.image ?? ""
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
        
        self.lbl_Distance.isHidden = true
        self.lbl_Time.isHidden = true
        self.stackView_ArrivalTime.isHidden = true
    }
    
    func moveToNewController(){
        performSegue(withIdentifier: "segue_RiderOnTrip", sender: nil)
        //remove observer for Ride Time as work is fullfill now
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideTime), object: nil)
    }
    
    func moveToNewControllerTapped(){
        performSegue(withIdentifier: "segue_RiderOnTrip", sender: "tapped")
        //remove observer for Ride Time as work is fullfill now
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideTime), object: nil)
    }
    
    
    func fillDriverInfo(data:Any) {
        let result:[String:Any] = data as! [String:Any]
        print("data hereqqqqq-----***** \(result)")
        
        let message = result["message"] as? [String:Any]
        let detail = message!["data"] as? [String:Any] ?? ["":""]
        let personalInfo = detail["personal_information"] as? [String:Any] ?? ["":""]
        let photo = detail["pic"] as? [String:Any] ?? ["":""]
        let vehicleInfo = detail["vehicle_information"] as? [String:Any] ?? ["":""]
        let dict = message!["tripData"] as? [String:Any] ?? ["":""]
        let driverMobile = detail["mobile"] as? String ?? ""
        self.lbl_Name.text = (personalInfo["fullName"] as? String ?? "").nameFormatted
        self.lbl_Car.text = vehicleInfo["model"] as? String ?? ""
        self.lbl_CarNo.text = vehicleInfo["vehicle_number"] as? String ?? ""
        self.lbl_Rating.text = "\((detail["avgRating"] as? Double ?? 0).rounded())"
        
        if let driverID = detail["_id"] as? String {
            RiderManager.sharedInstance.bookedDriverId = driverID
        }
        
        let km = (dict["driverReachedKM"] as? Double ?? 0)/1000
        let time = (dict["driverReachedTime"] as? Double ?? 0)/60
        self.lbl_Time.text = "\(String(format: "%.1f", time))Min"
        self.lbl_Distance.text = "\(String(format: "%.2f",km))Km/"
        
        let imgUrl = "\(kBaseImageURL)\(photo["original"] ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    if let thumbImage = image {
                        self.imgView_Profile.image = thumbImage
                    }
                    self.imgView_Profile.setNeedsDisplay()
                }
            })
        }
        
        RiderManager.sharedInstance.bookedDriverProfile =  DriverBasicModal.init(name: self.lbl_Name.text ?? "", image: imgUrl, km: self.lbl_Distance.text ?? "", min:  self.lbl_Time.text ?? "",rating: self.lbl_Rating.text ?? "",vehicleName: self.lbl_Car.text ?? "",vehicleNumber: self.lbl_CarNo.text ?? "",mobileNumber: driverMobile)
    }
    lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Main, bundle: nil)
    
    //MARK:- Button Action
    @IBAction func placeCall(_ sender: UIButton) {
        let controller = storyBoard.instantiateViewController(withIdentifier: "CallingTwillioViewController") as! CallingTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        let controller = storyBoard.instantiateViewController(withIdentifier: "MessageTwillioViewController") as! MessageTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segue_RiderOnTrip" {
            if sender != nil{
                let controller =  segue.destination as! RiderOnTripViewController
                controller.isTapReason = true
            }
        }
    }
}

