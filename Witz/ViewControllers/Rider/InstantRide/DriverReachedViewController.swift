//
//  DriverReachedViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 12/20/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher

class DriverReachedViewController: UIViewController {

    @IBOutlet weak var lbl_DriverName: UILabel!
    @IBOutlet weak var img_DriverPic: UIImageView!
    @IBOutlet weak var lbl_Rating: UILabel!
    @IBOutlet weak var lbl_VehicleName: UILabel!
    @IBOutlet weak var lbl_VehicleNumber: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        showContactDeails()
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeContact))
        self.view.addGestureRecognizer(tap)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate  func showContactDeails(){
        let driver = RiderManager.sharedInstance.bookedDriverProfile
        
        lbl_DriverName.text = driver?.name
        lbl_Rating.text = driver?.rating
        lbl_VehicleName.text = driver?.vehicleName
        lbl_VehicleNumber.text = driver?.vehicleNumber
        
        let imgUrl = driver?.image ?? ""
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.img_DriverPic.image = thumbImage
                    }
                    self.img_DriverPic.setNeedsDisplay()
                }
            })
        }
    }
    
    func closeContact()  {
        self.dismiss(animated: true, completion: nil)
    }

}
