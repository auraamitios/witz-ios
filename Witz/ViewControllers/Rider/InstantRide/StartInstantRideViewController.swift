//
//  StartInstantRideViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 11/15/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
class StartInstantRideViewController: WitzSuperViewController {
    
    @IBOutlet weak var mapInstant: GMSMapView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var img_RadarView: UIImageView!
    
    @IBOutlet weak var lbl_Time: UILabel!
    var tripID : String?
    let locManager = CLLocationManager()
    var backBtn = UIButton()
    
    var retryParameter : [String:Any]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        print(retryParameter ?? "")
        img_RadarView.image = #imageLiteral(resourceName: "radar")
        self.rotateRadar()
        
        self.lbl_Time.text =  RiderManager.sharedInstance.nearestDriverTime
        
        self.mapInstant.settings.setAllGesturesEnabled(false)
        /*We fix the location issue */
        locManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
            })
            return
        }
        fetchLocation()
        self.left_barButton.isHidden = true
        self.backBtn = UIButton(type: .custom)
        self.backBtn.setImage(UIImage(named: "Round_back"), for: .normal)
        //self.left_barButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //        self.left_barButton.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        //        self.left_barButton.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        self.backBtn.addTarget(self, action: #selector(StartInstantRideViewController.cancelInstantRideRequest), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: self.backBtn)
        self.navigationItem.leftBarButtonItem = item1
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
        //            self.hideLoader()
        //        })
        //        self.perform(#selector(showPickUpLocation), with: nil, afterDelay: 2.5)
        //        _ = Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { (timer) in
        //            self.showPickUpLocation()
        //        }
        
        //add observer to show Driver info pop up
        NotificationCenter.default.addObserver(self, selector: #selector(showDriverPopUp), name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.ShowDriverInfo), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(retryCallDriver), name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.ShowRetryOption), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locManager.stopUpdatingLocation()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.ShowDriverInfo), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.ShowRetryOption), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func fetchLocation() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        //locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        locManager.requestLocation()
        showLoader()
    }
    
    fileprivate func rotateRadar(){
       
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(.pi * 2.0)
        rotateAnimation.duration = 2.0
        rotateAnimation.repeatCount = Float.greatestFiniteMagnitude;
        
        img_RadarView.layer.add(rotateAnimation, forKey: nil)
    }
    
    public func grantLocationPermission() {
        locManager.requestAlwaysAuthorization()
    }
    
    func showDriverPopUp(object:NSNotification) {
        let data:Any = object.userInfo!
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactDriverViewController") as! ContactDriverViewController
        controller.driverData = data
        self.navigationController?.pushViewController(controller)
    }
    
    func retryCallDriver(){
        self.retryParameter!["parentRecurrsiveTripId"] = self.tripID
        let controller = UIAlertController.init(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderAlerts.NoDriverCurrently, preferredStyle: .alert)
        let retry = UIAlertAction(title: LinkStrings.MostCommon.Retry, style: .default, handler: { (action) -> Void in
            guard self.checkNetworkStatus() else {return}
            self.showLoader()
            RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/call_ride_request", withInputString:  self.retryParameter, requestType: .post, isAuthorization: true, success: { (response) in
                self.hideLoader()
                if response?["statusCode"] != 200 {
                    AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: response?["message"].string ?? "", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                        self.retryCallDriver()
                    })
                }
            }, failure: { (error) in
                print(error.debugDescription)
                self.hideLoader()
            })
              })
        let cancel = UIAlertAction(title: LinkStrings.MostCommon.cancel, style: .default, handler: { (action) -> Void in
             self.popToDashBoard()
        })
         controller.addAction(retry)
        controller.addAction(cancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    //MARK:: IBOutlets actions
    @IBAction func action_BtnCancel(_ sender: UIButton) {
        cancelInstantRideRequest()
    }
    
    func cancelInstantRideRequest() {
        AlertHelper.displayAlertView(title: LinkStrings.AlertTitle.Title, message:LinkStrings.RiderAlerts.InstantCancelRide, style: .alert, actionButtonTitle: [LinkStrings.MostCommon.Ok, LinkStrings.MostCommon.cancel], completionHandler: { (action:UIAlertAction!) -> Void in
            if action.title==LinkStrings.MostCommon.Ok {
                self.cancelRideRequest()
            }
        })
    }
    
    func cancelRideRequest(){
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/cancel_ride", withInputString: ["tripId":self.tripID ?? ""], requestType: .post, isAuthorization: true, success: { (response) in
            print(response ?? "" )
            self.hideLoader()
            self.popToDashBoard()
        }) { (error ) in
            self.hideLoader()
            self.popToDashBoard()
        }
    }
    
    func popToDashBoard() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is TransferDashboardViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
}

//MARK:: Mapview delegates
extension StartInstantRideViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {}
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        //        let lat = mapView.camera.target.latitude
        //        let lng = mapView.camera.target.longitude
        //        print("latitude:\(lat)")
        //        print("latitude:\(lng)")
        //returnPostionOfMapView(mapView: mapView)
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        AppManager.sharedInstance.dropCoordinates = position
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            
        })
    }
}

extension StartInstantRideViewController:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.hideLoader()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        AppManager.sharedInstance.locationCoordinates = locations
        if let location = locations.last {
            self.mapInstant.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.hideLoader()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.hideLoader()
    }
}

