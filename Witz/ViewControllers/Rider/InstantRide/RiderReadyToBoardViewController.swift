//
//  RiderReadyToBoardViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 11/15/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import GoogleMaps

class RiderReadyToBoardViewController: UIViewController {
    
    var isTapReason: Bool? // this will check if user come on screen by tap
    fileprivate var timerLocationGet : Timer?
    var timePassed : Int?
    var timeLeftToAccept = 180
    var min = 2
    var sec = 59
    fileprivate var task : DispatchWorkItem?
    var backBtn = UIButton()
    
    var readyToBoardClicked = false
    
    
    //Outlets
    
    @IBOutlet var viewPinForDriver: UIView!
    @IBOutlet weak var lbl_pinTime: UILabel!
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var map_View: GMSMapView!
    @IBOutlet weak var lbl_distanceTime: UILabel!
    /******view Rider Detail outlets******/
    @IBOutlet weak fileprivate var view_DriverDetail: UIView!
    @IBOutlet weak fileprivate var imgVIew_UserProfile: UIImageView!
    @IBOutlet weak fileprivate var lbl_UserRating: UILabel!
    @IBOutlet weak fileprivate var lbl_UserName: UILabel!
    @IBOutlet weak var lbl_VehicleNumber: UILabel!
    @IBOutlet weak var lbl_DriverVehicle: UILabel!
    /******view Contact Rider outlets******/
    @IBOutlet weak fileprivate var view_ContactDriver: UIView!
    @IBOutlet weak fileprivate var view_readyToBoard: UIView!
    @IBOutlet weak var view_Timer: UIView!
    @IBOutlet weak var imgView_CountDownTimer: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // add gesture for swipe
        let swipeUp = UISwipeGestureRecognizer.init(target: self, action: #selector(viewMore))
        swipeUp.direction = .up
        view_DriverDetail.addGestureRecognizer(swipeUp)
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(viewLess))
        swipeDown.direction = .down
        view_ContactDriver.addGestureRecognizer(swipeDown)
        
        //        self.left_barButton.isHidden = true
        self.backBtn = UIButton(type: .custom)
        self.backBtn.setImage(UIImage(named: "Round_back"), for: .normal)
        self.backBtn.addTarget(self, action: #selector(RiderReadyToBoardViewController.cancelInstantRideRequest), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: self.backBtn)
        self.navigationItem.leftBarButtonItem = item1
        
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(startTimeToBoard), name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideTime), object: nil)
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(closeCurrentView), name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideStart), object: nil)
        showContactDeails()
        if isTapReason == true{
            view_readyToBoard.isHidden = true
            //            view_Timer.isHidden = true
            imgView_CountDownTimer.isHidden = true
            lbl_Time.isHidden = true
        }else{
            startTimeToBoard()
        }
        timerLocationGet = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(RiderReadyToBoardViewController.getDriverLocation), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getDriverLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.timerLocationGet != nil {
            self.timerLocationGet?.invalidate()
            self.timerLocationGet = nil
        }
        
    }
    func closeCurrentView(){
        self.dismiss(animated: true, completion: {
            self.dismiss(animated: true, completion:nil)
        })
        timerLocationGet?.invalidate()
        //remove observer for Ride Start
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideStart), object: nil)
    }
    
    func startTimeToBoard(){
        
        self.perform(#selector(showDriverProfile), with: nil, afterDelay: 0.4)
        //  if Rider do nothing
        task = DispatchWorkItem {
            self.cancelTask(cancelRide: true)
        }
        view_readyToBoard.isHidden = false
        //        view_Timer.isHidden = false
        imgView_CountDownTimer.isHidden = false
        lbl_Time.isHidden = false
        view_ContactDriver.isHidden = true
        view_DriverDetail.isHidden = true
        //remove observer for Ride Time as work is fullfill now
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.RideTime), object: nil)
    }
    
    fileprivate  func showContactDeails(){
        let driver = RiderManager.sharedInstance.bookedDriverProfile
        
        lbl_UserName.text = driver?.name?.nameFormatted
        lbl_UserRating.text = driver?.rating
        lbl_DriverVehicle.text = driver?.vehicleName
        lbl_VehicleNumber.text = driver?.vehicleNumber
//        lbl_distanceTime.text = "\(driver?.km ?? "")\(driver?.min ?? "")"
//        lbl_pinTime.text =
        
        let imgUrl = driver?.image ?? ""
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgVIew_UserProfile.image = thumbImage
                    }
                    self.imgVIew_UserProfile.setNeedsDisplay()
                }
            })
        }
    }
    
    /**
     get the icon using storyboard view
     */
    fileprivate func getPinWith(time:String ) -> UIImage{
        lbl_pinTime.text = time
        //grab it
        UIGraphicsBeginImageContextWithOptions(viewPinForDriver.bounds.size, false, UIScreen.main.scale)
        viewPinForDriver.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imagePick = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imagePick ?? #imageLiteral(resourceName: "on_map_arrival_location")
    }
    
    
    fileprivate func getTripStatus(id:String){
        guard self.checkNetworkStatus() else {return}
        let params = ["tripId":id]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_trip_details", withInputString: params, requestType: .put, isAuthorization: true, success: { (response) in
//            print(response ?? "")
            let modalTrip = Trips.init(json: response?["data"]["data"] ?? ["":""])
            
            if self.isTripStillValid(status: modalTrip.status ?? 0){
                DispatchQueue.main.async {
                        AlertHelper.displayOkAlert(title: LinkStrings.App.AppName, message: LinkStrings.RiderAlerts.DriverCancelTrip, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action) in
                            Utility.riderSideMenuSetUp()
                        }, controller: self)
                    

                }
            }
        }, failure: { (error) in
            
        })
    }
    
    fileprivate func isTripStillValid(status :Int) -> Bool{
        switch status {
        case SharingTripType.CANCELLED_BY_DRIVER.rawValue:
            return true
        case SharingTripType.RIDER_NOT_REACHED.rawValue:
            return true
        case SharingTripType.CANCELLED_BY_RIDER.rawValue:
            return true
        case SharingTripType.RIDER_CANCEL_TRIP_PNALITY.rawValue:
            return true
        case SharingTripType.Driver_CANCEL_TRIP_PNALITY.rawValue:
            return true
        default:
            return false
        }
    }
    
    func getDriverLocation(){
        guard self.checkNetworkStatus() else {return}
          DispatchQueue.global(qos: .background).async {
        let parameters =   ["tripId": RiderManager.sharedInstance.tripIDForRider ?? "" , "driverId":RiderManager.sharedInstance.bookedDriverId ?? ""] as [String : Any]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_driver_location", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            
            let modalCurrentLoc =  RiderHomeLocation.init(json:   response?["data"]["currentLocation"] ?? "") // driver currrent loc
            let modalStartLoc =  RiderHomeLocation.init(json:   response?["data"]["startLocation"] ?? "") // trip start point
            
            let driverLoc = CLLocationCoordinate2D(latitude: Double(modalCurrentLoc.coordinates?[1] ?? 0), longitude: Double(modalCurrentLoc.coordinates?[0] ?? 0))
            let startLoc = CLLocationCoordinate2D(latitude: Double(modalStartLoc.coordinates?[1] ?? 0), longitude: Double(modalStartLoc.coordinates?[0] ?? 0))
            
            self.addDriverLocationIcon(driverLoc)
            self.getPathPoints(driverLoc, startLoc)
                self.getTripStatus(id: RiderManager.sharedInstance.tripIDForRider ?? "")
        }) { (error ) in
        }
        }
    }
    
    /**
     It will present  Reached driver's profile
     */
    
    func showDriverProfile(){
        if let controller = storyBoard.instantiateViewController(withIdentifier: "DriverReachedViewController") as? DriverReachedViewController{
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: true, completion: {
                // Driver has N min to accept the ride
                if self.timePassed != nil {
                    self.timeLeftToAccept = self.timeLeftToAccept - (self.timePassed ?? 0)
                    self.min = self.timeLeftToAccept / 60
                    self.sec = self.timeLeftToAccept % 60
                }
                if self.timeLeftToAccept > 0{
                    Utility.stopTimer(after: TimeInterval(self.timeLeftToAccept), self.task)
                    Utility.delegate = self
                    
                }else{
                    AlertHelper.displayOkAlert(title: LinkStrings.App.AppName, message: LinkStrings.RiderAlerts.TimePassed, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action) in
                        Utility.riderSideMenuSetUp()
                    }, controller: self)
                }
            })
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Cancel ride Pop Up for options
     */
    func cancelInstantRideRequest() {
        
        var msg = LinkStrings.RiderAlerts.InstantCancelRide
        if readyToBoardClicked == true {
            msg = LinkStrings.RiderAlerts.PenaltyCancelRide
        }
        
        AlertHelper.displayAlertView(title: LinkStrings.AlertTitle.Title, message:msg, style: .alert, actionButtonTitle: [LinkStrings.MostCommon.Ok, LinkStrings.MostCommon.cancel], completionHandler: { (action:UIAlertAction!) -> Void in
            if action.title==LinkStrings.MostCommon.Ok {
                self.cancelRideRequest()
            }
        })
    }
    
    /**
     Cancel ride after user choose to cancel
     */
    
    func cancelRideRequest(){
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/cancel_ride", withInputString: ["tripId":RiderManager.sharedInstance.tripIDForRider ?? ""], requestType: .post, isAuthorization: true, success: { (response) in
//            print(response ?? "" )
            self.hideLoader()
            Utility.riderSideMenuSetUp()
        }) { (error ) in
            self.hideLoader()
            AlertHelper.displayAlertViewOKButton(title:  LinkStrings.AlertTitle.Title, message: LinkStrings.RiderAlerts.InternetNotWorking, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Retry, completionHandler: { (action:UIAlertAction!) -> Void in
                if action.title==LinkStrings.MostCommon.Retry {
                    self.cancelRideRequest()
                }
            })
        }
    }
    
    
    /**
     Cancel Task schedule for time
     */
    fileprivate func cancelTask(cancelRide:Bool){
        Utility.stopTimer()
        self.task?.cancel()
        if cancelRide == true{
            
            //            self.dismiss(animated: true, completion: nil)
        }
    }
    lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Rider, bundle: nil)
    @IBAction func action_ContactDriver(_ sender: UIButton) {
        if let controller = storyBoard.instantiateViewController(withIdentifier: "ContactDriverViewController") as? ContactDriverViewController{
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            controller.disableTap = true
            self.present(controller, animated: true, completion: nil)
        }
    }
    @IBAction func action_cancelRide(_ sender: UIButton) {
        var title = LinkStrings.Yes
        var msg = LinkStrings.RiderAlerts.InstantCancelRide
        if readyToBoardClicked == true {
            msg = LinkStrings.RiderAlerts.PenaltyCancelRide
            title = LinkStrings.OK
        }
        AlertHelper.displayAlert(title: LinkStrings.App.AppName, message: msg, style: .alert, actionButtonTitle: [title,LinkStrings.Cancel], completionHandler: { (action) in
//            print(action.title ?? "")
            if action.title == LinkStrings.Yes || action.title == LinkStrings.OK {
                self.dismiss(animated: true) {
                    self.cancelRideRequest()
                }
            }
        }, controller: self)
    }
    
    @IBAction func readyToBoard(_ sender: UIButton) {
        
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        let parameters = ["driverId": RiderManager.sharedInstance.bookedDriverId ?? "","tripId":RiderManager.sharedInstance.tripIDForRider ?? ""]
        WitzConnectionManager().makeAPICall(functionName: "rider/inform_driver", withInputString: parameters, requestType: .post, withCurrentTask: .Driver_Info , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            self.hideLoader()
            //            self.dismiss(animated: true, completion: nil)
            self.view_readyToBoard.isHidden = true
            //            self.view_Timer.isHidden = true
            self.readyToBoardClicked = true
            self.imgView_CountDownTimer.isHidden = true
            self.lbl_Time.isHidden = true

            self.view_ContactDriver.isHidden = false
            self.view_DriverDetail.isHidden = false
        }, failure: { (error) in
            self.hideLoader()
        })
    }
    @IBAction func back_Action(_ sender: UIButton) {
    cancelInstantRideRequest()
    }
    
    @IBAction func communicateWithDriver(_ sender: UIButton) {
        
        if let controller = storyBoard.instantiateViewController(withIdentifier: "ContactDriverViewController") as? ContactDriverViewController{
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            controller.disableTap = true
            self.present(controller, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func viewLess_action(_ sender: UIButton) {
        viewLess()
    }
    
    @IBAction func viewMore_Action(_ sender: UIButton) {
        viewMore()
    }
    
    @objc fileprivate  func viewLess(){
        view_DriverDetail.isHidden = false
        view_ContactDriver.isHidden = true
    }
    
    @objc fileprivate func viewMore(){
        view_DriverDetail.isHidden = true
        view_ContactDriver.isHidden = false
    }
}


extension RiderReadyToBoardViewController: UtilityTimerDelegate{
    
    func countDownTimer(value: Int) {
        sec = sec-1
        if sec == 0 {
            min = min-1
            sec = 59
        }
        if value == 0 {
            Utility.stopTimer()
            self.task?.cancel()
        }
        if min < 0{
            return
        }
        
        lbl_Time.text = "\(min):\( sec > 9 ? "\(sec)" : "0\(sec)")"
    }
}

extension RiderReadyToBoardViewController {
    
    fileprivate func addDriverLocationIcon(_ loc : CLLocationCoordinate2D) {
        let position = CLLocationCoordinate2D(latitude: loc.latitude, longitude:loc.longitude)
        let marker = GMSMarker(position: position)
        DispatchQueue.main.async {
            marker.map = self.map_View
            marker.icon = #imageLiteral(resourceName: "vehicle-car_on-map")
            marker.appearAnimation = GMSMarkerAnimation.pop
            self.map_View.animate(toLocation: loc)
            self.map_View.animate(toZoom: 12.0)
        }
    }
    
    fileprivate func getPathPoints(_ start : CLLocationCoordinate2D,_ end : CLLocationCoordinate2D) {
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: start, destination: end) { (returnPath, distance) in
            self.hideLoader()
            self.showPath(polyStr: returnPath)

            DispatchQueue.main.async {
                let arr = Utility.getTimeAndDistance(data: distance)
                let marker1 = GMSMarker(position: end)
                marker1.title = "Pick UP Location"
                marker1.appearAnimation = GMSMarkerAnimation.pop
                marker1.icon = self.getPinWith(time:  self.timeToShow(distance: arr[0], time: arr[1]))
                marker1.map = self.map_View
            
//                self.lbl_distanceTime.text = "\(arr[0]) Km \n \(arr[1]) Min"
            }
        }
    }
    
    fileprivate func timeToShow(distance: String ,time: String) -> String{
        
        var strFinal = "\(distance) Km \n \(time) Min"
        
        if distance.contains("m") && time.contains("min"){
            strFinal =  "\(distance)  \n \(time) "
        }else  if distance.contains("m") {
            strFinal =  "\(distance)  \n \(time) min"
        }else  if time.contains("min"){
            strFinal =  "\(distance) m \n \(time) "
        }
        return  strFinal
    }
    
    /**
     Draw path on the map
     */
    
    fileprivate func showPath(polyStr :String){
        DispatchQueue.main.async {
            let path = GMSPath(fromEncodedPath: polyStr)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.map_View
        }
        hideLoader()
    }
}

