//
//  PinDestinationViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 10/23/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class PinDestinationViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_PaymentMethod: UILabel!
    @IBOutlet weak var constraint_Bottom_MapView: NSLayoutConstraint!
    @IBOutlet weak var pinMapview: GMSMapView!
    @IBOutlet weak var pinStack: UIStackView!
    @IBOutlet weak var lblDropAddress: UILabel!
    @IBOutlet weak var lblPickUpAddress: UILabel!
    @IBOutlet weak var btnBookRide: UIButton!
    @IBOutlet weak var vehicleCollection: UICollectionView!
    @IBOutlet weak var viewVehicleList: UIView!
    
    @IBOutlet weak var imgView_DestinationPin: UIImageView!
    
    //Vehicle Price iVar
    @IBOutlet weak var viewPriceListing: UIView!
    @IBOutlet weak var priceCollection: UICollectionView!
    @IBOutlet weak var seatCountPicker: UIPickerView!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var lblSharePrice: UILabel!
    @IBOutlet weak var lblSpecial: UILabel!
    @IBOutlet weak var lblSpcl: UILabel!
    @IBOutlet weak var lblSpecialPrice: UILabel!
    @IBOutlet weak var lblSpclPrice: UILabel!
    
    @IBOutlet weak var priveCollectionFlowLayout: UICollectionViewFlowLayout!
    //Selected Vehicle Price iVar
    @IBOutlet var selectedVehicleView: UIView!
    @IBOutlet weak var selectedVehicleCollection: UICollectionView!
    
    @IBOutlet weak var view_SpecialRideOption: UIView!
    @IBOutlet weak var view_SingleRideOption: UIView!
    
    @IBOutlet weak var view_ShareRideOption: UIView!
    @IBOutlet weak var lbl_Toast: UILabel!
    @IBOutlet weak var lbl_Change: UILabel!
    @IBOutlet weak var constraint_Width_SelectedCollectionView: NSLayoutConstraint!
    
    var lbl_Title : UILabel?
    var locUpdate:LocationUpdate!
    let objWitzConnMgr = RiderConnectionManager()
    var bounds : GMSCoordinateBounds?
    var tripaData = AddTripData()
    var vehicleIndex = 0
    var passsengerCount = "1"
    
    var arr_seatCount = [Int]()
    var routeDistance:[Any]?
    
    // variable use to transfer data
    var currentModal : VehiclesData?
    var currentPrice = ""
    var specialPrice = 0.0
    var sharePrice = 0.0
    var vehicleIndexSelected = 0
    
    var tripId = ""
    var vehicleName = ""
    
    var isShareSelected:Bool?
    var userMovedMap:Bool?
    var cardAdded:Bool?
    
    var rideBooked = false
    //    var sharedPriceData:[String:JSON]?
    var sharedPriceData:SharedPrice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        locUpdate = LocationUpdate.sharedInstance
        locUpdate.delegate = self
        locUpdate.startTracking()
        self.objWitzConnMgr.delegate = self
        
        self.imgView_DestinationPin.isHidden = RiderManager.sharedInstance.homeWorkAddressSelected ?? false
        self.pinMapview.bringSubview(toFront: self.pinStack)
        self.pinMapview.bringSubview(toFront: self.btnBookRide)
        self.priceCollection.isPagingEnabled = true
        
        do {
            // Set the map style by passing a valid JSON string.
            pinMapview.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        lbl_Title = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: 0, width: 150, height: 30))
        lbl_Title?.font = UIFont.systemFont(ofSize: 15)
        lbl_Title?.textColor = .white
        lbl_Title?.textAlignment = .center
        self.navigationItem.titleView = lbl_Title
        getAllVehiclesList()
        showJourneyAddress()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.userMovedMap = false
        if (RiderManager.sharedInstance.riderInfo?.cards?.count ?? 0) > 0{
               lbl_PaymentMethod.text = RiderManager.sharedInstance.riderLoginData?.cards?[0].cardType ?? "VISA"
                    cardAdded = true
                    self.lbl_Change.isHidden = false
        }else {
            cardAdded = false
            self.lbl_Change.isHidden = true
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locUpdate.stopTracking()
        self.userMovedMap = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showJourneyAddress() {
        if RiderManager.sharedInstance.homeWorkAddressSelected ?? false {
            fillDropLocationAddress()
            showLoader()
            addPickLocationIcon()
            addDropLocationIcon()
            getPathPoints_Second()
        }
        fillPickUpLocationAddress()
        DispatchQueue.global(qos: .background).async {
            self.getCurrentTimeZone()
        }
    }
    //MARK:: API call
    func getAllVehiclesList() {
        guard self.checkNetworkStatus() else {return}
        let params: Parameters = ["instant": "true"]
        if RiderManager.sharedInstance.vehiclesData.count>0 {
            RiderManager.sharedInstance.vehiclesData.removeAll()
        }
        self.objWitzConnMgr.APICall(functionName: "rider/get_all_vehicles", withInputString: params, requestType: .post, withCurrentTask: .GET_ALL_VEHICLES, isAuthorization: true)
    }
    
    fileprivate func getCurrentTimeZone() {
        guard self.checkNetworkStatus() else {return}
   
        let url = URL(string: "http://api.geonames.org/timezoneJSON?lat=\(AppManager.sharedInstance.pickUpCoordinates.latitude)&lng=\(AppManager.sharedInstance.pickUpCoordinates.longitude )&username=shanker.yapits")
        if url == nil {
            return
        }
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    print(json)
                    print("timeZone-->\(json["timezoneId"] ?? "" as AnyObject)******gmtOffset-->\(json["gmtOffset"] ?? "" as AnyObject)")
                    DispatchQueue.main.async {
                        RiderManager.sharedInstance.timeZoneId = (json["timezoneId"] as? String)
                        print("timeZoneid--\( RiderManager.sharedInstance.timeZoneId ?? "")")
                    }
                }catch let error as NSError{
                    print(error)
                }
            }
        }).resume()
    }
    
    func bookInstantRide(data:[Any]) {
        if data.count > 0 {
            
            if let detail = data[0] as? [String:Any]{
                let sCoor = detail["start_location"] as? [String:Any]
                let eCoor = detail["end_location"] as? [String:Any]
                let dist = detail["distance"] as? [String:Any]
                let time = detail["duration"] as? [String:Any]
                
                let distStr = dist!["text"] as! String
                let timeStr = time!["text"] as! String
                
                print("distance-->\(distStr)")
                print("time-->\(timeStr)")
                
                var finalTime = ""
                if timeStr.contains("hours") {
                    var hrs = timeStr.components(separatedBy:" hours")
                    print(hrs)
                    var mins = [""]
                    if hrs[1].contains("mins") {
                        mins = hrs[1].components(separatedBy:" mins")
                    }else {
                        mins = hrs[1].components(separatedBy:" min")
                    }
                    print(mins)
                    let hours = hrs[0]
                    print(hours)
                    var min = mins[0]
                    print(min)
                    let hrs1 = (hours.int ?? 0) * 60
                    print(hrs1)
                    min = min.replacingOccurrences(of: " ", with: "")
                    print(min)
                    print(min.int ?? 0)
                    let tDurs = (min.int ?? 0) + hrs1
                    print(tDurs)
                    finalTime = "\(tDurs)"
                }else if timeStr.contains("hour"){
                    var hrs = timeStr.components(separatedBy:" hour")
                    print(hrs)
                    var mins = [""]
                    if hrs[1].contains("mins") {
                        mins = hrs[1].components(separatedBy:" mins")
                    }else {
                        mins = hrs[1].components(separatedBy:" min")
                    }
                    print(mins)
                    let hours = hrs[0]
                    print(hours)
                    var min = mins[0]
                    print(min)
                    let hrs1 = (hours.int ?? 0) * 60
                    print(hrs1)
                    min = min.replacingOccurrences(of: " ", with: "")
                    print(min)
                    print(min.int ?? 0)
                    let tDurs = (min.int ?? 0) + hrs1
                    print(tDurs)
                    finalTime = "\(tDurs)"
                }
                else {
                    finalTime = timeStr.replacingOccurrences(of: " mins", with: "")
                }
                let finalDist = distStr.replacingOccurrences(of: " km", with: "")
                print("journey duration in mins----\(finalTime)")
                
//                self.navigationItem.titleView = nil
                  lbl_Title?.text = "\(finalDist)\("km")\(" ")\(finalTime)\("min")"
                if finalTime.contains("dakika"){
                      lbl_Title?.text  = "\(finalDist)\("km")\(" ")\(finalTime)"
                }
                guard self.checkNetworkStatus() else {return}
                showLoader()
                
                DispatchQueue.main.async {
                    let startAddress = self.lblPickUpAddress.text ?? ""
                    let endAddress = self.lblDropAddress.text ?? ""
                    RiderManager.sharedInstance.pickUpAddress = startAddress
                    RiderManager.sharedInstance.dropAddress = endAddress
                    let params: Parameters = ["tripType":"1",
                                              "start_lat":sCoor!["lat"]!,
                                              "start_lng":sCoor!["lng"]!,
                                              "end_lat":eCoor!["lat"]!,
                                              "end_lng":eCoor!["lng"]!,
                                              "distance":Double(finalDist) ?? 0.0,
                                              "duration": Double(finalTime) ?? 0.0 ,
                                              "start_address" :startAddress,
                                              "end_address": endAddress,
                                              "timezoneString":RiderManager.sharedInstance.timeZoneId ?? Default_Time_Zone]
                    print(params)
                    self.objWitzConnMgr.APICall(functionName: "rider/add_trip", withInputString: params, requestType: .post, withCurrentTask: .ADD_RIDER_TRIP, isAuthorization: true)
                }
            }
        }
    }
    
    func show_KmTime(data:[Any]){
        
        if data.count > 0 {
            
            if let detail = data[0] as? [String:Any]{
                //                let sCoor = detail["start_location"] as? [String:Any]
                //                let eCoor = detail["end_location"] as? [String:Any]
                let dist = detail["distance"] as? [String:Any]
                let time = detail["duration"] as? [String:Any]
                
                let distStr = dist!["text"] as! String
                let timeStr = time!["text"] as! String
                
                //                print("distance-->\(distStr)")
                //                print("time-->\(timeStr)")
                //
                var finalTime = ""
                if timeStr.contains("hours") {
                    var hrs = timeStr.components(separatedBy:" hours")
                    //                    print(hrs)
                    var mins = [""]
                    if hrs[1].contains("mins") {
                        mins = hrs[1].components(separatedBy:" mins")
                    }else {
                        mins = hrs[1].components(separatedBy:" min")
                    }
                    //                    print(mins)
                    let hours = hrs[0]
                    //                    print(hours)
                    var min = mins[0]
                    //                    print(min)
                    let hrs1 = (hours.int ?? 0) * 60
                    //                    print(hrs1)
                    min = min.replacingOccurrences(of: " ", with: "")
                    //                    print(min)
                    //                    print(min.int ?? 0)
                    let tDurs = (min.int ?? 0) + hrs1
                    //                    print(tDurs)
                    finalTime = "\(tDurs)"
                }else if timeStr.contains("hour"){
                    var hrs = timeStr.components(separatedBy:" hour")
                    //                    print(hrs)
                    var mins = [""]
                    if hrs[1].contains("mins") {
                        mins = hrs[1].components(separatedBy:" mins")
                    }else {
                        mins = hrs[1].components(separatedBy:" min")
                    }
                    //                    print(mins)
                    let hours = hrs[0]
                    //                    print(hours)
                    var min = mins[0]
                    //                    print(min)
                    let hrs1 = (hours.int ?? 0) * 60
                    //                    print(hrs1)
                    min = min.replacingOccurrences(of: " ", with: "")
                    //                    print(min)
                    //                    print(min.int ?? 0)
                    let tDurs = (min.int ?? 0) + hrs1
                    //                    print(tDurs)
                    finalTime = "\(tDurs)"
                }
                else {
                    finalTime = timeStr.replacingOccurrences(of: " mins", with: "")
                }
                let finalDist = distStr.replacingOccurrences(of: " km", with: "")
                //                print("journey duration in mins----\(finalTime)")
                
//                self.navigationItem.titleView = nil
                lbl_Title?.text = "\(finalDist)\("km")\(" ")\(finalTime)\("min")"
                if finalTime.contains("dakika"){
                    lbl_Title?.text = "\(finalDist)\("km")\(" ")\(finalTime)"
                }
            }
        }
        
    }
    
    func getSharedPrice(vehicleIndex:Int) {
        guard self.checkNetworkStatus() else {return}
        let params:Parameters = ["tripId":self.tripaData.trip_Id ?? "", "vehicleType":RiderManager.sharedInstance.vehiclesData[vehicleIndex].name ?? ""]
        self.objWitzConnMgr.APICall(functionName: "rider/get_shared_price", withInputString: params, requestType: .post, withCurrentTask: .GET_SHARED_PRICE, isAuthorization: true)
    }
    
    //MARK:: Private APIs
    
    fileprivate func initCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:5, left:5,bottom:5,right:5)
        let itemWidth:CGFloat = (UIScreen.main.bounds.width)/5.5
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        print("cell size*********:\(layout.itemSize)")
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        self.selectedVehicleCollection.collectionViewLayout = layout
    }
    
    fileprivate func fillPickUpLocationAddress() {
        ReverseGeocode.getAddressByCoords(coords: AppManager.sharedInstance.pickUpCoordinates, returnAddress: { (returnAddress) in
            print(returnAddress)
            self.lblPickUpAddress.text = returnAddress
        })
    }
    fileprivate func fillDropLocationAddress() {
        ReverseGeocode.getAddressByCoords(coords: AppManager.sharedInstance.dropCoordinates, returnAddress: { (returnAddress) in
            //            self.lblPickUpAddress.text = returnAddress
            self.lblDropAddress.text = returnAddress
        })
    }
    
    fileprivate func addPickLocationIcon() {
        let pickPosition = AppManager.sharedInstance.pickUpCoordinates
        let position = CLLocationCoordinate2D(latitude: pickPosition.latitude, longitude:pickPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.pinMapview
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
        
    }
    
    fileprivate func addDropLocationIcon () {
        let dropPosition = AppManager.sharedInstance.dropCoordinates
        let position = CLLocationCoordinate2D(latitude: dropPosition.latitude, longitude:dropPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.pinMapview
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    //MARK:: Draw root path
    //****** called in case of search, Home or Work address option selected **********//
    fileprivate func getPathPoints_Second() {
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: AppManager.sharedInstance.pickUpCoordinates, destination: AppManager.sharedInstance.dropCoordinates) { (returnPath, distance) in
            self.hideLoader()
            //            print("waypoints****************\(distance)")
            self.routeDistance = distance
            self.showPath(polyStr: returnPath)
        }
    }
    //****** called in case of pin destination option **********//
    fileprivate func getPathPoints(bookRide : Bool) {
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: AppManager.sharedInstance.pickUpCoordinates, destination: AppManager.sharedInstance.dropCoordinates) { (returnPath, distance) in
            self.hideLoader()
            //            print("waypoints****************\(distance)")
            DispatchQueue.main.async {  self.show_KmTime(data: distance)
                if bookRide == true{
                    self.showPath(polyStr: returnPath)
                    self.bookInstantRide(data: distance)
                }
            }
            
        }
    }
    fileprivate func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = App_Base_Color
        polyline.map = self.pinMapview
        DispatchQueue.main.async {
            self.bounds = GMSCoordinateBounds(path: path!)
            //            self.pinMapview.animate(with: GMSCameraUpdate.fit(self.bounds!, withPadding: 50.0))
            self.pinMapview.animate(with: GMSCameraUpdate.fit(self.bounds!, with: UIEdgeInsetsMake(100, 10, 78, 10)))
            
        }
    }
    
    func hideAddressView() {
        self.pinStack.isHidden = true
        self.btnBookRide.isHidden = true
        self.imgView_DestinationPin.isHidden = true
    }
    
    func displayVehicleList() {
        hideAddressView()
        self.constraint_Bottom_MapView.constant =  self.viewVehicleList.frame.size.height
        if self.bounds != nil {
            self.pinMapview.animate(with: GMSCameraUpdate.fit(self.bounds!, with: UIEdgeInsetsMake(100, 10, 78, 10)))
            
        }
        self.viewVehicleList.isHidden = false
        self.vehicleCollection.reloadData()
    }
    
    func hideVehicleListingView() {
        self.viewVehicleList.isHidden = true
        self.viewPriceListing.isHidden = false
        self.constraint_Bottom_MapView.constant =  self.viewPriceListing.frame.size.height
        if self.bounds != nil {
            self.pinMapview.animate(with: GMSCameraUpdate.fit(self.bounds!, with: UIEdgeInsetsMake(20, 10, 77, 10)))
        }
        self.priceCollection.reloadData()
        self.seatCountPicker.reloadAllComponents()
        self.priceCollection.selectItem(at: IndexPath.init(row: vehicleIndexSelected, section: 0), animated: true, scrollPosition: .centeredHorizontally)
    }
    
    func showSelectedVehicleListWithPrice() {
        self.viewPriceListing.isHidden = true
        self.constraint_Bottom_MapView.constant = 320
        self.selectedVehicleView.frame = CGRect(x: 0, y: Utility.windowHeight()-385, width: Utility.windowWidth(), height: 320)
        self.view.addSubview(self.selectedVehicleView)
        self.view.bringSubview(toFront: self.selectedVehicleView)
        //initCollectionView()
        let index =  priceCollection.indexPathsForVisibleItems
        print("priceCollection selected items\(index)")
        self.selectedVehicleCollection.reloadData()
        self.selectedVehicleCollection.selectItem(at: index[0], animated: true, scrollPosition: .centeredHorizontally)
    }
    
    //MARK:- ##### IBOutlets actions
    
    @IBAction func sharingTripSelected(_ sender: UIButton) {
        self.isShareSelected = true
        currentPrice = "Shared " + (self.lblSharePrice.text ?? "")
        showSelectedState(with: 0)
        if let cost = Double(self.lblSharePrice.text ?? ""){
        if cost > self.specialPrice {
            priceComparisonAlert()
        }
        }
    }
    
    @IBAction func specialTripSelected(_ sender: UIButton) {
        self.isShareSelected = false
        currentPrice = "Special " + (self.lblSpclPrice.text ?? "")
        currentPrice = "Special " + (self.lblSpecialPrice.text ?? "")
        showSelectedState(with: 1)
    }
    
    func jumpToSpecialScenario() {
        self.isShareSelected = false
        currentPrice = "Special " + (self.lblSpclPrice.text ?? "")
        currentPrice = "Special " + (self.lblSpecialPrice.text ?? "")
        showSelectedState(with: 1)
    }
    
    func showSelectedState(with index:Int){
        if index == 0 {
            view_SpecialRideOption.backgroundColor = UIColor.white
            view_ShareRideOption.backgroundColor = UIColor.init(red: 0.8510, green: 0.6863, blue: 0.2784, alpha: 1.0)
            view_SingleRideOption.backgroundColor = UIColor.white
            lblShare.textColor = .white
            lblSharePrice.textColor = .white
            lblSpecial.textColor = UIColor.lightGray
            lblSpecialPrice.textColor = UIColor.lightGray
            lblSpclPrice.textColor = UIColor.lightGray
            lblSpcl.textColor = UIColor.lightGray
        }else{
            view_SpecialRideOption.backgroundColor = UIColor.init(red: 0.8510, green: 0.6863, blue: 0.2784, alpha: 1.0)
            view_SingleRideOption.backgroundColor = UIColor.init(red: 0.8510, green: 0.6863, blue: 0.2784, alpha: 1.0)
            view_ShareRideOption.backgroundColor = UIColor.white
            lblSpecial.textColor = .white
            lblSpecialPrice.textColor = .white
            lblSpclPrice.textColor = .white
            lblSpcl.textColor = .white
            lblShare.textColor = UIColor.lightGray
            lblSharePrice.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func action_BookRide(_ sender: UIButton) {
        
        guard !(self.lblDropAddress.text?.isEmpty)! else {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderAlerts.DropLocation, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: actionHandler, controller: self)
            return
        }
        //    call in case of home or work addresss clicked or via search as well
        if RiderManager.sharedInstance.homeWorkAddressSelected ?? false {
            bookInstantRide(data: routeDistance ?? [])
        }else {   // choose address thru pin
            showLoader()
            DispatchQueue.main.async {
                self.addPickLocationIcon()
                self.addDropLocationIcon()
                self.rideBooked = true
                self.getPathPoints(bookRide: true)
                
            }
        }
    }
    
    @IBAction func action_Call(_ sender: UIButton) {
        showSelectedVehicleListWithPrice()
    }
    
    @IBAction func action_Payment(_ sender: UIButton) {
        self.performSegue(withIdentifier: "CreditCard", sender: nil)
    }
    
    @IBAction func action_CallBtn(_ sender: UIButton) {
        guard self.cardAdded ?? false else {
            self.showToastMsg()
            return
        }
        if self.isShareSelected ?? false {
            createShareRide()
        }else {
            createInstantRide()
        }
    }
    
    func showToastMsg() {
        self.lbl_Toast.isHidden = false
        self.lbl_Toast.text = LinkStrings.RiderBooking.CardNotAdded
        self.lbl_Toast.fadeIn(duration: 2.0, completion: { (complete) in
            self.lbl_Toast.fadeOut(duration: 1.0, completion: { (complete) in
            })
        })
    }
    
    func createInstantRideUsingProceedAction() {
        self.isShareSelected = true
        createInstantRideViaShare()
    }
    
    //MARK:: API call
    func createInstantRide() {
        guard self.checkNetworkStatus() else {return}
        showLoader()
        
        var paymentId:String?
        if (RiderManager.sharedInstance.riderLoginData?.cards?.count ?? 0) > 0{
            if RiderManager.sharedInstance.riderLoginData?.cards?[0].defaultStatus ?? false {
                paymentId = RiderManager.sharedInstance.riderLoginData?.cards?[0].id ?? "5a251b5aab354d6839d0bb89"
            }
        }
        let parameters = ["tripId":self.tripaData.trip_Id ?? "","seatsRequired":passsengerCount,"cardId":paymentId ?? "5a251b5aab354d6839d0bb89"]
        print("paymentId------------->\(paymentId ?? "")")
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/call_ride_request", withInputString:parameters , requestType: .post, isAuthorization: true, success: { (response) in
            //            print("response\(String(describing: response))")
            self.hideLoader()
            if response?["statusCode"] == 200 {
                self.performSegue(withIdentifier: "StartRide", sender: parameters)
            }else {
                AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: response?["message"].string ?? "", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                })
            }
        }, failure: { (error) in
            print(error.debugDescription)
            self.hideLoader()
        })
    }
    
    func createInstantRideViaShare() {
        guard self.checkNetworkStatus() else {return}
        showLoader()
        var paymentId:String?
        if (RiderManager.sharedInstance.riderLoginData?.cards?.count ?? 0) > 0{
            if RiderManager.sharedInstance.riderLoginData?.cards?[0].defaultStatus ?? false {
                paymentId = RiderManager.sharedInstance.riderLoginData?.cards?[0].id ?? "5a251b5aab354d6839d0bb89"
            }
        }
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/call_ride_request", withInputString: ["tripId":self.tripaData.trip_Id ?? "","seatsRequired":passsengerCount,"cardId":paymentId ?? "5a251b5aab354d6839d0bb89"], requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            if response?["statusCode"] == 200 {
            }else {
                AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: response?["message"].string ?? "", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                })
            }
        }, failure: { (error) in
            print(error.debugDescription as Any)
            self.hideLoader()
        })
    }
    
    func createShareRide() {
        guard self.checkNetworkStatus() else {return}
        showLoader()
        let adminEarn = self.sharedPriceData?.adminEarning ?? 0.0
        
        let prams = ["tripId":(self.tripaData.trip_Id ?? "")!,
                     "price":self.sharePrice,
                     "driverEarning":(self.sharedPriceData?.driverEarning ?? 0.0)!,
                     "riderTaxApplied":(self.sharedPriceData?.riderTaxApplied ?? 0.0)!,
                     "driverTaxApplied":(self.sharedPriceData?.driverTaxApplied ?? 0.0)!,
                     "adminEarning":adminEarn] as [String : Any]
        
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/create_sharing_ride", withInputString: prams
            , requestType: .post, isAuthorization: true, success: { (response) in
                
                print("response\(String(describing: response))")
                self.hideLoader()
                if response?["statusCode"] == 200 {
                    self.callInstantRide(arg: true, completion: { (completion) in
                        if completion {
                            self.performSegue(withIdentifier: "StartRide", sender: prams)
                        }
                    })
                }else {
                    AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: response?["message"].string ?? "", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                    })
                }
        }, failure: { (error) in
            print(error.debugDescription)
            self.hideLoader()
        })
    }
    
    func callInstantRide(arg: Bool, completion: (Bool) -> ()) {
        print("First line of code executed")
        // do stuff here to determine what you want to "send back".
        // we are just sending the Boolean value that was sent in "back"
        createInstantRideUsingProceedAction()
        completion(arg)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StartRide"{
            if let controller = segue.destination as? StartInstantRideViewController{
                controller.tripID = self.tripId
                controller.retryParameter = (sender as! [String : Any])
            }
        }else if segue.identifier == "CreditCard"{
            if let controller = segue.destination as? AddPaymentViewController{
                controller.fromTrip = true
            }
        }
    }
}

//MARK:: Mapview delegates
extension PinDestinationViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {}
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("willMove")
        userMovedMap = true
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        if RiderManager.sharedInstance.homeWorkAddressSelected ?? false {
            self.lblDropAddress.text = RiderManager.sharedInstance.dropAddress ?? ""
        }else {
            guard self.userMovedMap ?? false else {
                return
            }
            if rideBooked == false{ self.getPathPoints(bookRide: false)}
            returnPostionOfMapView(mapView: mapView)
        }
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        AppManager.sharedInstance.dropCoordinates = position
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            self.lblDropAddress.text = returnAddress
        })
    }
}
//MARK::Location deleagtes
extension PinDestinationViewController:LocationDelegates {
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.pinMapview.isMyLocationEnabled = true
            self.pinMapview.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            self.pinMapview.camera = GMSCameraPosition(target: location.coordinate, zoom: 13, bearing: 0, viewingAngle: 0)
            let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            let marker = GMSMarker(position: position)
            //marker.title = "Witz"
            //marker.map = self.pinMapview
            marker.appearAnimation = GMSMarkerAnimation.pop
            locUpdate.stopTracking()
            self.hideLoader()
        }
    }
    
    func didFailToUpdate(error: Error) {
    }
}
extension PinDestinationViewController:UICollectionViewDelegate,UICollectionViewDataSource {
    
    //MARK:- UICollectionView DataSource Method(s)
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let widthToSet =  CGFloat(RiderManager.sharedInstance.vehiclesData.count * 75)
        if widthToSet <= self.view.frame.size.width{
            constraint_Width_SelectedCollectionView.constant = widthToSet
        }else{
            constraint_Width_SelectedCollectionView.constant = self.view.frame.size.width
        }
        return RiderManager.sharedInstance.vehiclesData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 1 {
            var cell:VehicleCell?
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? VehicleCell
            let modal = RiderManager.sharedInstance.vehiclesData[indexPath.row]
            cell?.updateCell(with: indexPath, modal)
            return cell!
            
        }else if collectionView.tag == 2{
            var cell:VehiclePriceCell?
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? VehiclePriceCell
            let modal = RiderManager.sharedInstance.vehiclesData[indexPath.row]
            cell?.updateCell(with: indexPath, modal)
            if modal.isShared == true{
                self.singleButton(show: false ,with: modal.name ?? "")
            }else{
                self.singleButton(show: true ,with: modal.name ?? "")
            }
            
            self.seatCountToShow(seats: modal.seats ?? 0)
            self.currentModal = modal
            return cell!
        }else {
            var cell:SelectedVehicleCell?
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: SelectedVehicleCell.reuseIdentifier, for: indexPath) as? SelectedVehicleCell
            cell?.configureCell(with: indexPath, RiderManager.sharedInstance.vehiclesData[indexPath.row])
            
            if cell?.isSelected == true{
                cell?.lblRiderCount.isHidden = false
                cell?.lblPrice.isHidden = false
                cell?.lblRiderCount.text = passsengerCount
                cell?.lblPrice.text = currentPrice
                cell?.vehicleStack.isHidden = false
            }
            
            //            if indexPath.row == 0 { cell?.isSelected = true }
            return cell!
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 1 {
            let cell:VehicleCell = collectionView.cellForItem(at: indexPath) as! VehicleCell
            cell.imgVehicle.layer.borderColor = App_Base_Color.cgColor
            vehicleIndexSelected = indexPath.row
            hideVehicleListingView()
        }
    }
    
    func singleButton(show:Bool,with vehicle:String){
        
        view_SingleRideOption.isHidden = !show
        view_ShareRideOption.isHidden = show
        view_SpecialRideOption.isHidden = show
        if self.tripId.isEmpty {
            return
        }
        if show == true{
            DispatchQueue.global(qos: .background).async {
                self.getSpecialPrice(vehicle)
            }
        }else{
            DispatchQueue.global(qos: .background).async {
                self.getSpecialPrice(vehicle)
                self.getSharedPrice(vehicle)
            }
        }
    }
    
    func priceComparisonAlert() {
        if isShareSelected ?? false {
            AlertHelper.displayAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderAlerts.JumpToSpecial, style: .alert, actionButtonTitle: ["Close", "Proceed"], completionHandler: { (action:UIAlertAction!) -> Void in
                if action.title == "Proceed" {
                    self.isShareSelected = true
                    self.showSelectedVehicleListWithPrice()
                }
            }, controller: self)
        }
    }
    //MARK:: get price API call
    fileprivate func getSharedPrice(_ vehicle:String){
        self.showLoader()
        RiderConnectionManager().makeAPICall(functionName: "rider/get_shared_price", withInputString: ["tripId":self.tripId,"vehicleType":vehicle], requestType: .post, withCurrentTask: .CREATE_RIDER , isAuthorization: true  ,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            print("response\(String(describing: response))")
            self.hideLoader()
            DispatchQueue.main.async {
                if response?["statusCode"] == 200 {
                    //                    self.sharedPriceData = response?["data"].dictionary
                    self.sharedPriceData = SharedPrice.init(json: response?["data"] ?? [])
                    print(self.sharedPriceData ?? [])
                    if let price = response?["data"]["price"].string {
                        print(price)
                        self.lblSharePrice.text =  "\(price)"
                        self.sharePrice = Double(price) ?? 0
                    }
                }
            }
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
    
    fileprivate func getSpecialPrice(_ vehicle:String){
        self.showLoader()
        RiderConnectionManager().makeAPICall(functionName: "rider/get_price", withInputString: ["tripId":self.tripId,"vehicleType":vehicle], requestType: .post, withCurrentTask: .CREATE_RIDER , isAuthorization: true  ,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            print("response\(String(describing: response))")
            self.hideLoader()
            DispatchQueue.main.async {
                if response?["statusCode"] == 200 {
                    if let price = response?["data"]["price"].double {
                        print(price)
                        self.lblSpecialPrice.text =  "\(price)"
                        self.lblSpclPrice.text =  "\(price)"
                        self.specialPrice = Double(price)
                        self.currentPrice = "Special " + (self.lblSpclPrice.text ?? "")
                        self.currentPrice = "Special " + (self.lblSpecialPrice.text ?? "")
                    }
                }
            }
            
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
    
    func seatCountToShow(seats:Int){
        showSelectedState(with: 1)
        arr_seatCount.removeAll()
        for i in 1...seats{
            arr_seatCount.append(i)
        }
        self.seatCountPicker.reloadAllComponents()
    }
}

//MARK::UIPickerViewDelegate
extension PinDestinationViewController:UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return arr_seatCount.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let rowTitle = "\(arr_seatCount[row])"  //convert int value into string
        return rowTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        passsengerCount = "\(arr_seatCount[row])"  //convert int value into string
        
        let count = Double(arr_seatCount[row])
        
        if specialPrice == 0 || sharePrice == 0 {
            DispatchQueue.main.async {
                self.getSpecialPrice(self.vehicleName)
                self.getSharedPrice(self.vehicleName)
            }
            return
        }
        //        lblSpclPrice.text = "\( count * specialPrice)"
        //        lblSpecialPrice.text = "\(count * specialPrice)"
        lblSharePrice.text = "\(count * sharePrice)"
        
        if (count * self.sharePrice) > self.specialPrice {
            priceComparisonAlert()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        //var attributedString = NSAttributedString()
        let attributedString = NSAttributedString(string: "\(arr_seatCount[row])", attributes: [NSForegroundColorAttributeName : UIColor.white])
        return attributedString
    }
}

extension PinDestinationViewController:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell: UICollectionViewCell in self.priceCollection.visibleCells {
            let indexPath: IndexPath? = self.priceCollection.indexPath(for: cell)
            print("vehicle index************\(String(describing: indexPath?.row))")
            vehicleIndex = (indexPath?.row)!
            self.seatCountPicker.reloadAllComponents()
            self.showLoader()
            
            guard self.checkNetworkStatus() else {return}
            let params = ["tripId":self.tripaData.trip_Id ?? "","vehicleType":RiderManager.sharedInstance.vehiclesData[vehicleIndex].name ?? ""]
            //            let params = ["tripId":self.tripaData.trip_Id!, "vehicleType":RiderManager.sharedInstance.vehiclesData[vehicleIndex].name!]
            //        let params:Parameters = ["tripId":self.tripaData.trip_Id ?? "", "vehicleType":RiderManager.sharedInstance.vehiclesData[vehicleIndex].name ?? ""]
            print("params***********\(params)")
            self.objWitzConnMgr.APICall(functionName: "rider/get_price", withInputString: params, requestType: .post, withCurrentTask: .GET_PRICE, isAuthorization: true)
            
            getSharedPrice(vehicleIndex: vehicleIndex)
        }
    }
    
}
extension PinDestinationViewController:RiderConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        self.hideLoader()
        print("vehicle Data \(sender)")
        switch serviceType {
        case .GET_ALL_VEHICLES:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                self.vehicleCollection.reloadData()
                self.selectedVehicleCollection.reloadData()
            }
            break
        case .ADD_RIDER_TRIP:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                self.tripaData = responseData as! AddTripData
                RiderManager.sharedInstance.currentTripData = self.tripaData
                self.tripId = self.tripaData.trip_Id ?? ""
                RiderManager.sharedInstance.tripIDForRider = self.tripId 
                displayVehicleList()
            }
            break
        case .GET_PRICE:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
            }
            break
        case .GET_SHARED_PRICE:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
            }
            break
        default:
            break
        }
    }
    
    func didFailWithError(sender: AnyObject) {
        self.hideLoader()
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}


