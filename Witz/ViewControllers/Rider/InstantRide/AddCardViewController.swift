//
//  AddCardViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 11/2/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Alamofire
import CCValidator
enum CardType: String {
    case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
    static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
}




class AddCardViewController: WitzSuperViewController {
    var fromTrip:Bool?
   var fromTripReserve :Bool?

    var icons = ["userIcon","payment_method_credit_card","date_credit_card","cvv_code_credit_card","payment_method_credit_card"]
    var titles = ["NAME ON CARD", "CREDIT CARD NUMBER", "EXPIRATION DATE", "CVV CODE", "GIVE A NAME YOUR CREDIT CARD"]
    
    var dataArry = ["","","","","GIVE A NAME YOUR CREDIT CARD"]
    @IBOutlet weak var tblAddCard: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)

        DispatchQueue.global(qos: .background).async {
            self.getPaymentGatewayId()
        }
        // Do any additional setup after loading the view.
        self.tblAddCard.tableFooterView = UIView()
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.AddCard
    }
    
    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
    
    func luhnCheck(number: String) -> Bool {
        var sum = 0
        let digitStrings = number.reversed().map { String($0) }

        for tuple in digitStrings.enumerated() {
            guard let digit = Int(tuple.element) else { return false }
            let odd = tuple.offset % 2 == 1

            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        return sum % 10 == 0
    }
    
    func checkCardNumber(input: String) -> (type: CardType, formatted: String, valid: Bool) {
        // Get only numbers from the input string
        let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "")
 
        var type: CardType = .Unknown
        var formatted = ""
        var valid = false

        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card
                break
            }
        }

        // check validity
        valid = luhnCheck(number: numberOnly)

        // format
        var formatted4 = ""
        for character in numberOnly.characters {
            if formatted4.count == 4 {
                formatted += formatted4 + " "
                formatted4 = ""
            }
            formatted4.append(character)
        }

        formatted += formatted4 // the rest

        // return the tuple
        return (type, formatted, valid)
    }
    
    func addCreditCard() {
        view.endEditing(true)
        guard self.checkNetworkStatus() else {return}
        if let idToSend = RiderManager.sharedInstance.riderGatewayDetail?.gatewayId{
            showLoader()
            let params = ["paymentGatewayId":idToSend, "nameOnCard":dataArry[0], "creditCardNo":dataArry[1], "expireDate":dataArry[2], "cvv":dataArry[3], "cardType":dataArry[4], "defaultStatus":"1"] as [String : Any]
//            print(params)
            RiderConnectionManager().makeAPICall(functionName: "rider/addCreditCard", withInputString: params, requestType: .post, withCurrentTask: .CREATE_RIDER , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
                self.hideLoader()
                print("responseData******\( response ?? "")")
                
                if response?["statusCode"] == 200 {
                    Utility.getLoggedRiderDetail({ (complete) in
                        DispatchQueue.main.async {
                            if self.fromTrip == true{
                                self.backToTripDestination()
                                return
                            }else if  self.fromTripReserve == true{
                                self.backToTripDestinationReserve()
                                return
                            }
                            self.navigationController?.popViewController(animated: false)
                        }
                    })
                  
                }else{
                AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: response?["message"].string ?? "", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                })
                }
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }
    }
    
    func backToTripDestination() {
        showLoader()
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is PinDestinationViewController {
                    self.hideLoader()
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    func backToTripDestinationReserve() {
        showLoader()
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                self.hideLoader()
                if aViewController is ReservationPinDestinationViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    func validation() -> Bool{
        if dataArry.count < 5{return false}
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getPaymentGatewayId() {
        RiderConnectionManager().makeAPICall(functionName: "rider/getPaymentMethod", withInputString: ["":""], requestType: .get, withCurrentTask: .CREATE_RIDER , isAuthorization: true,headerType:.URLENCODED,arr_img:nil,success: { (response) in
            print("response\(String(describing: response))")
            
            //store paymentGateway
            if let arr_data = response?["data"].array{
                RiderManager.sharedInstance.riderGatewayDetail = PaymentGateway.init(json: arr_data[0] )
            }
            
        }, failure: { (error) in
            print(error.debugDescription)
        })
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AddCardViewController:UITableViewDelegate,UITableViewDataSource,UITableViewDataSourcePrefetching {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return titles.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == titles.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: bottomCellIdentifier, for: indexPath) as! ButtonCell
            cell.cellDelegate = self
            cell.setButtonTitle(title: "ADD CREDIT CARD")
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddCardCell.identifier, for: indexPath) as! AddCardCell
            cell.configureCell(with: indexPath, titles[indexPath.section], icons[indexPath.section],data:dataArry[indexPath.section], delegate: self)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 5{
            return 30
        }
        return 10
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 5 {
            return  50
        }
        return 80
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
    }
}

extension AddCardViewController:ButtonCellDelegate {
    func saveButtonClicked(sender: UIButton) {
        if validation(){ addCreditCard()}else{
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: "Please fill all details", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
            })
        }
    }
}
extension AddCardViewController:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard !textField.isEmpty else {
            textField.text=""
            return
        }
        let index = IndexPath(row: 0, section: textField.tag)
        if let cell: AddCardCell = self.tblAddCard.cellForRow(at: index) as? AddCardCell{
        var filteredVal =  cell.txtValue.text ?? ""
        
        if index.section == 1 || index.section == 3 {
            filteredVal = cell.txtValue.text ?? "".withoutSpacesAndNewLines
            filteredVal = filteredVal.replacingOccurrences(of: "/", with: "")
            filteredVal = filteredVal.replacingOccurrences(of: " ", with: "")
            print(filteredVal)
        }else if index.section == 2 {
            filteredVal = cell.txtValue.text ?? "".withoutSpacesAndNewLines
            filteredVal = filteredVal.replacingOccurrences(of: "/", with: ".")
            print(filteredVal)
        }
        
        if dataArry.indices.contains(index.section) {
            dataArry.remove(at: index.section)
            dataArry.insert(filteredVal, at: index.section)
        }else {
            dataArry.insert(filteredVal, at: index.section)
        }
        }
        dataArry.removeDuplicates()
        if dataArry.count == 4 {
//            if (RiderManager.sharedInstance.riderInfo?.cards?.count ?? 0) > 0 {
//                dataArry.insert("\(RiderManager.sharedInstance.riderInfo?.cards?[0]["cardType"] ?? "")", at: 4)
//            }else {
                let numberAsString = dataArry[1]
                let recognizedType = CCValidator.typeCheckingPrefixOnly(creditCardNumber: numberAsString)
                switch recognizedType.rawValue {
                case 0:
                    dataArry.insert("AmericanExpress", at: 4)
                    break
                case 1:
                    dataArry.insert("Dankort", at: 4)
                    break
                case 2:
                    dataArry.insert("DinersClub", at: 4)
                    break
                case 3:
                    dataArry.insert("Discover", at: 4)
                    break
                case 4:
                    dataArry.insert("JCB", at: 4)
                    break
                case 5:
                    dataArry.insert("Mastero", at: 4)
                    break
                case 6:
                    dataArry.insert("MasterCard", at: 4)
                    break
                case 7:
                    dataArry.insert("UnionPay", at: 4)
                    break
                case 8:
                    dataArry.insert("VisaElectron", at: 4)
                    break
                case 9:
                    dataArry.insert("Visa", at: 4)
                    break
                default:
                    dataArry.insert("GIVE A NAME YOUR CREDIT CARD", at: 4)
                    break
                   
                }
             titles[4] = dataArry[4]
            tblAddCard.reloadData()
//            }
        }
        print("value************\(dataArry)")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        if textField.tag == 1 || textField.tag == 2 || textField.tag == 3{
            
            let newLength = text.count + string.count - range.length
            //print("new lenght value\(newLength)")
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            var limitLength = 0
            if textField.tag == 1 {
                limitLength = 19
                if string.count>0 {
                    if newLength % 5 == 0 {
                        textField.text = textField.text?.appending(" ")
                    }
                }
                
            }else if textField.tag == 2 {
                limitLength = 5
                if string.count>0 {
                    if newLength == 3  {
                        textField.text = textField.text?.appending("/")
                    }
                }
            }else {
                limitLength = 3
            }
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= limitLength
        }else {
            return true
        }
    }
    
}
