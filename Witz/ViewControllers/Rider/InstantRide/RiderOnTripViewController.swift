//
//  RiderOnTripViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 11/15/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class RiderOnTripViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var slider_DriverLocation: UISlider!
    @IBOutlet weak var constraint_ViewRideOn_Height: NSLayoutConstraint!
    @IBOutlet weak var view_Tracking: UIView!
    
    var timePassed : Int?
    var isTapReason: Bool? // this will check if user come on screen by tap
    var aggregateRideOn: Bool? // check for aggregate trip only or get the detail of trip again
    let locManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
        
        getTripDataForRider()
        self.perform(#selector(presentGettingStarted), with: nil, afterDelay:0.6 )
        
        // add observer to dismiss view controller
        NotificationCenter.default.addObserver(self, selector: #selector(feedbackTime), name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.Tripended), object: nil)
        
        locManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
            return
        }
        fetchLocation()
    }
    
  
    
    public func fetchLocation() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        //locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        locManager.requestLocation()
        showLoader()
    }
    
    public func grantLocationPermission() {
        locManager.requestAlwaysAuthorization()
    }
    
    fileprivate func addPickLocationIcon() {
        let pickPosition = AppManager.sharedInstance.pickUpCoordinates
        let position = CLLocationCoordinate2D(latitude: pickPosition.latitude, longitude:pickPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
        
    }
    
    fileprivate func addDropLocationIcon () {
        let dropPosition = AppManager.sharedInstance.dropCoordinates
        let position = CLLocationCoordinate2D(latitude: dropPosition.latitude, longitude:dropPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    fileprivate func getPathPoints() {
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: AppManager.sharedInstance.pickUpCoordinates, destination: AppManager.sharedInstance.dropCoordinates) { (returnPath, distance) in
            self.hideLoader()
            print("waypoints****************\(distance)")
            self.showPath(polyStr: returnPath)
            //            let detail = distance[0] as! [String:Any]
            //            let dist = detail["distance"] as? [String:Any]
            //            let time = detail["duration"] as? [String:Any]
            //            DispatchQueue.main.async {
            //       self.lbl_Status.text = "\(dist!["text"] as? String ?? "0.0" ) Km/\(time!["text"] as? String ?? "0.0") Min"
            //
            //            }
        }
    }
    fileprivate func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = App_Base_Color
        polyline.map = self.mapView
        DispatchQueue.main.async {
            let bounds = GMSCoordinateBounds(path: path!)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
            self.mapView.animate(toZoom: 12.0)
        }
    }
    func presentGettingStarted(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RiderReadyToBoardViewController") as! RiderReadyToBoardViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.isTapReason = isTapReason
        controller.timePassed = self.timePassed
        controller.map_View = self.mapView
        self.present(controller, animated: true, completion: nil)
        view_Tracking.isHidden = false
    }
    lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Rider, bundle: nil)
    func feedbackTime(){
        //        self.performSegue(withIdentifier: "segue_TripEndedNow", sender: nil)
        
        if let controller = storyBoard.instantiateViewController(withIdentifier: "TripEndedNowViewController") as? TripEndedNowViewController{
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: true, completion: nil)
            
        }
        
        //remove observer for Trip  Ended  as work is fullfill now
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.Tripended), object: nil)
    }
    
    
    /**
     Data load from server for trip
     */
    public func getTripDataForRider(){
        //    RiderManager.sharedInstance.tripIDForRider
        self.addPickLocationIcon()
        self.addDropLocationIcon()
        self.getPathPoints()
        guard self.checkNetworkStatus() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_trip_details", withInputString: ["tripId":RiderManager.sharedInstance.tripIDForRider ?? ""], requestType: .put, isAuthorization: true, success: { (response) in
            print("response\(String(describing: response))")
            self.hideLoader()
            if response?["statusCode"] == 200 {
                if let detail =  response?["data"].dictionary{
                    self.lbl_Status.text = "\(String(format: "%.2f",detail["data"]?["distance"].double ?? 0.0)) Km/\(String(format: "%.1f",detail["data"]?["duration"].double ?? 0.0)) Min"
//                    response?.rawData()
//                    let tripData = AddTripData.parseVehiclesData(data: detail  , outerData: response!)
//
//                    RiderManager.sharedInstance.currentTripData = tripData
                    
                    if self.aggregateRideOn == true {
                        let startLocation = RiderHomeLocation.init(json: detail["data"]?["startLocation"] ?? "")
                        let endLocation = RiderHomeLocation.init(json: detail["data"]?["endLocation"] ?? "")
                        AppManager.sharedInstance.pickUpCoordinates = CLLocationCoordinate2D(latitude: Double(startLocation.coordinates?[1] ?? 0), longitude: Double(startLocation.coordinates?[0] ?? 0))
                        AppManager.sharedInstance.dropCoordinates = CLLocationCoordinate2D(latitude: Double(endLocation.coordinates?[1] ?? 0), longitude: Double(endLocation.coordinates?[0] ?? 0))
                        self.mapView.clear()
                        self.addPickLocationIcon()
                        self.addDropLocationIcon()
                        self.getPathPoints()
                    }
                }
            }
        }, failure: { (error) in
            print(error.debugDescription)
            self.hideLoader()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension RiderOnTripViewController:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.hideLoader()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //        AppManager.sharedInstance.locationCoordinates = locations
        if let location = locations.last {
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            //
            self.hideLoader()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.hideLoader()
    }
}
