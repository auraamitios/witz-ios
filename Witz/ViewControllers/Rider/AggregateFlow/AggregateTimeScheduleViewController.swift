//
//  AggregateTimeScheduleViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/21/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
class AggregateTimeScheduleViewController: WitzSuperViewController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_TimeDetail: UILabel!
    
    //Week Days
    @IBOutlet var lbl_Days: [UILabel]!
    var numberOfDaySelected : [Int]?
    var routeDistance:[Any]?
    var aggregateTime:[String]?
    var aggregateDays:[Int]?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if numberOfDaySelected != nil{ daysSelectedOfWeek(number: numberOfDaySelected!)}
//        print("journeyDays------>\(numberOfDaySelected ?? [])")
        displayTime()
//        self.mapView.settings.setAllGesturesEnabled(false)
        addPickLocationIcon()
        addDropLocationIcon()
        getPathPoints()
        
        print(aggregateDays ?? [])
    }
    
    func displayTime() {
        let stringAttributed = NSMutableAttributedString.init(string: "\(aggregateTime![0])\(" Business Arrival Time")\("/")\(aggregateTime![1])\(" Business Exit Time")")
        let font = UIFont(name: "verdana-bold", size: 14.0)
        stringAttributed.addAttribute(NSFontAttributeName, value:font!, range: NSRange.init(location: 0, length: 6))
//        stringAttributed.addAttribute(NSForegroundColorAttributeName, value: UIColor.green, range: NSRange.init(location: 30, length: 5))
        stringAttributed.addAttribute(NSFontAttributeName, value:font!, range: NSRange.init(location: 28, length: 5))
        self.lbl_TimeDetail.attributedText = stringAttributed
        
    }
    fileprivate func addPickLocationIcon() {
        let pickPosition = AppManager.sharedInstance.pickUpCoordinates
        let position = CLLocationCoordinate2D(latitude: pickPosition.latitude, longitude:pickPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    fileprivate func addDropLocationIcon () {
        let dropPosition = AppManager.sharedInstance.dropCoordinates
        let position = CLLocationCoordinate2D(latitude: dropPosition.latitude, longitude:dropPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    //MARK:: Draw root path
    fileprivate func getPathPoints() {
        hitsLimit_Google = 5

        GooglePathHelper.getPathPolyPoints(from: AppManager.sharedInstance.pickUpCoordinates, destination: AppManager.sharedInstance.dropCoordinates) { (returnPath, distance) in
            self.hideLoader()
            self.routeDistance = distance
            self.showPath(polyStr: returnPath)
        }
    }
    
    fileprivate func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = App_Base_Color
        polyline.map = self.mapView
        DispatchQueue.main.async {
            let bounds = GMSCoordinateBounds(path: path!)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
//            self.mapView.animate(toZoom: 13.0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_Submit(_ sender: UIButton) {
        guard self.checkNetworkStatus() else {return}
        bookAggregateRideRequest(data: routeDistance ?? [])
    }
    
    //MARK::book aggregate request
    fileprivate func bookAggregateRideRequest(data:[Any]) {
        let detail = data[0] as! [String:Any]
        let sCoor = detail["start_location"] as? [String:Any]
        let eCoor = detail["end_location"] as? [String:Any]
        let dist = detail["distance"] as? [String:Any]
        let time = detail["duration"] as? [String:Any]
        
        let distStr = dist!["text"] as! String
        let timeStr = time!["text"] as! String
        
        print("distance-->\(distStr)")
        print("time-->\(timeStr)")
        
        var finalTime = ""
        if timeStr.contains("hours") {
            var hrs = timeStr.components(separatedBy:" hours")
//            print(hrs)
            var mins = [""]
            if hrs[1].contains("mins") {
                mins = hrs[1].components(separatedBy:" mins")
            }else {
                mins = hrs[1].components(separatedBy:" min")
            }
//            print(mins)
            let hours = hrs[0]
//            print(hours)
            var min = mins[0]
//            print(min)
            let hrs1 = (hours.int ?? 0) * 60
//            print(hrs1)
            min = min.replacingOccurrences(of: " ", with: "")
//            print(min)
//            print(min.int ?? 0)
            let tDurs = (min.int ?? 0) + hrs1
//            print(tDurs)
            finalTime = "\(tDurs)"
        }else if timeStr.contains("hour"){
            var hrs = timeStr.components(separatedBy:" hour")
//            print(hrs)
            var mins = [""]
            if hrs[1].contains("mins") {
                mins = hrs[1].components(separatedBy:" mins")
            }else {
                mins = hrs[1].components(separatedBy:" min")
            }
//            print(mins)
            let hours = hrs[0]
//            print(hours)
            var min = mins[0]
//            print(min)
            let hrs1 = (hours.int ?? 0) * 60
//            print(hrs1)
            min = min.replacingOccurrences(of: " ", with: "")
//            print(min)
//            print(min.int ?? 0)
            let tDurs = (min.int ?? 0) + hrs1
//            print(tDurs)
            finalTime = "\(tDurs)"
        }
        else {
            finalTime = timeStr.replacingOccurrences(of: " mins", with: "")
        }
        let finalDist = distStr.replacingOccurrences(of: " km", with: "")
        print("journey duration in mins----\(finalTime)")
        
        self.showLoader()
        
        DispatchQueue.main.async {
            
            let startAddress = RiderManager.sharedInstance.pickUpAddress ?? ""
            let endAddress = RiderManager.sharedInstance.dropAddress ?? ""
            let params: Parameters = ["tripType":"3","start_lat":sCoor!["lat"]!,
                                      "start_lng":sCoor!["lng"]!,"end_lat":eCoor!["lat"]!,
                                      "end_lng":eCoor!["lng"]!,
                                      "distance":Double(finalDist) ?? 0.0,
                                      "duration":Double(finalTime) ?? 0.0,
                                      "start_address" :startAddress,
                                      "end_address": endAddress,
                                      "timezoneString":RiderManager.sharedInstance.timeZoneId ?? Default_Time_Zone,
                                      "timezone":RiderManager.sharedInstance.timeZoneId ?? Default_Time_Zone,
                                      "aggregateDayNumber":self.aggregateDays ?? [1],
                                      "aggregateTime": [self.aggregateTime?[0] ?? "09:00", self.aggregateTime?[1] ?? "18:00"]]
            
            print("params------->\(params)")
            RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/add_trip_ios", withInputString: params, requestType: .post, isAuthorization: true, success: { (response) in
                print("response\(String(describing: response))")
                self.hideLoader()

                if response?["statusCode"] == 200 {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "segue_Confirmed", sender: nil)
                    }
                }else {
                    let data = response
                    AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: data?["message"].stringValue ?? "", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                    }, controller: self)
                }
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- custom method
    
    /**
     this will help you indicate the selected days of week
     */
    fileprivate func daysSelectedOfWeek(number:[Int]){
//        numberOfDaySelected = number  //store value to pass to another viewcontroller
        for (index,lbl) in lbl_Days.enumerated() {
           let item = number[index]
            print("daysSelected******>\(item)")
            if item == 1{
                lbl.backgroundColor = .white
                lbl.textColor = .darkGray
            }
        }
    }
    
}
