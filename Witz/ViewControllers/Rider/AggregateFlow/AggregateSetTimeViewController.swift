//
//  AggregateSetTimeViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/21/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class AggregateSetTimeViewController: WitzSuperViewController {
    
    @IBOutlet weak var tbl_Days: UITableView!
    @IBOutlet weak var btn_SetTime: UIButton!
    @IBOutlet weak var btn_Arrival: UIButton!
    @IBOutlet weak var btn_Exit: UIButton!
    
    var arrDays = ["Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday", "Sunday"]
    var arrDaysState = [1,1,1,1,1,0,0]
    var arrivalTime:Bool?
    var isDaySelected:Bool?
    var aggregateDayNumber = [1,2,3,4,5]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.navigationItem.titleView = nil
        self.navigationItem.title = "Set Arrival Time"
        self.tbl_Days.tableFooterView = UIView()
        self.btn_SetTime.setTitle("Set Arrival Time", for: .normal)
        //        createLocalArry()
    }
    
    //TODO::toggleArry
    //    func createLocalArry() {
    //        print(arrDays[..<5])
    //        for _ in 0..<arrDays.count {
    //            self.arrDaysState.append(0)
    //        }
    ////        print(self.arrDaysState)
    //    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_SetTime(_ sender: UIButton) {
        guard self.daySelectionValidation() else {
            showTimePickerView()
            return
        }
        self.performSegue(withIdentifier: "segue_Summary", sender: nil)
    }
    
    @IBAction func action_ArrivalTime(_ sender: UIButton) {
        self.arrivalTime = true
        showTimePickerView()
    }
    
    @IBAction func action_ExitTime(_ sender: UIButton) {
        self.arrivalTime = false
        showTimePickerView()
    }
    
    func showTimePickerView(){
        let storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.DatePicker, bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "WitzTimePicker") as? WitzTimePicker{
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            controller.pickerDelegate = self
            self.present(controller, animated: true, completion: {
            })
        }
    }
    
    func daySelectionValidation()->Bool {
        if self.btn_Arrival.currentTitle == "HH:MM" && self.btn_Exit.currentTitle == "HH:MM" {
            self.arrivalTime = true
            self.btn_SetTime.setTitle("Set Arrival Time", for: .normal)
            return false
        }else if self.btn_Arrival.currentTitle != "HH:MM" && self.btn_Exit.currentTitle == "HH:MM" {
            self.arrivalTime = false
            self.btn_SetTime.setTitle("Set Exit Time", for: .normal)
            return false
        }
        self.btn_SetTime.setTitle("Finalize Aggregate Request", for: .normal)
        
        return true
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let controller = segue.destination as! AggregateTimeScheduleViewController
        controller.numberOfDaySelected = self.arrDaysState
        controller.aggregateTime = [self.btn_Arrival.currentTitle ?? "", self.btn_Exit.currentTitle ?? ""]
        controller.aggregateDays = aggregateDayNumber
    }
}

extension AggregateSetTimeViewController:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AggregateSetTimeCell.identifier, for: indexPath) as! AggregateSetTimeCell
        cell.configureCell(with: indexPath, arrDays[indexPath.row], selectFlag: arrDaysState[indexPath.row])
        //        cell.btn_Box.addTarget(self, action: #selector(action_CheckBtn(_:)), for: .touchUpInside)
        return cell
    }
    

    
    //    func action_CheckBtn(_ sender: UIButton) {
    //        guard self.daySelectionValidation() else {
    //            showTimePickerView()
    //            return
    //        }
    //        self.btn_SetTime.setTitle("Finalize Aggregate Request", for: .normal)
    //        let index = sender.tag
    //
    //        print("before--\(arrDaysState)")
    //        if self.arrDaysState[index] == 0 {
    //            self.arrDaysState.remove(at: index)
    //            self.arrDaysState.insert(1, at: index)
    //            createAggregateDaysArry(day: index)
    //        }else {
    //            self.arrDaysState.remove(at: index)
    //            self.arrDaysState.insert(0, at: index)
    //            removeDayFromArry(day: index)
    //        }
    //        print("after--\(arrDaysState)")
    ////        print("altered--\(arrDaysState.indexes(of: index))")
    //        //DispatchQueue.main.async {
    //        self.tbl_Days.reloadData()
    ////            self.tbl_Days.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .fade)
    //        //}
    //
    //    }
    
    //    func createAggregateDaysArry(day:Int) {
    //
    //        switch day {
    //        case AggregateRequestDay.Monday.rawValue:
    //            aggregateDayNumber.append(1)
    //            break
    //        case AggregateRequestDay.Tuesday.rawValue:
    //            aggregateDayNumber.append(2)
    //            break
    //        case AggregateRequestDay.Wednesday.rawValue:
    //            aggregateDayNumber.append(3)
    //            break
    //        case AggregateRequestDay.Thrusday.rawValue:
    //            aggregateDayNumber.append(4)
    //            break
    //        case AggregateRequestDay.Friday.rawValue:
    //            aggregateDayNumber.append(5)
    //            break
    //        case AggregateRequestDay.Saturday.rawValue:
    //            aggregateDayNumber.append(6)
    //            break
    //        case AggregateRequestDay.Sunday.rawValue:
    //            aggregateDayNumber.append(0)
    //            break
    //        default:
    //            break
    //        }
    //        print("aggreDays------>\(aggregateDayNumber)")
    //    }
    
    //    func removeDayFromArry(day:Int) {
    //
    //        if aggregateDayNumber.contains(day) {
    //            for val in 0..<aggregateDayNumber.count {
    //                if val == day {
    //                    aggregateDayNumber.remove(at: day)
    //                }
    //            }
    //        }
    //        print("aggreDays------>\(aggregateDayNumber)")
    //    }
}

extension AggregateSetTimeViewController:WitzTimePickerDelegate {
    //    func selectedTime(_ pickerView: WitzTimePicker, bookingTime: Date) {
    //        print("booking Time--->\(bookingTime)")
    //        let dateFormatter = DateFormatter()
    //        dateFormatter.dateFormat = "HH:mm"
    //        if self.arrivalTime ?? false{
    //            self.btn_Arrival.setTitle(dateFormatter.string(from: bookingTime), for: .normal)
    //        }else {
    //            self.btn_Exit.setTitle(dateFormatter.string(from: bookingTime), for: .normal)
    //        }
    //    }
    func selectedTime(_ pickerView: WitzTimePicker, bookingTime: Date, rideTime: String) {
        
        if self.arrivalTime ?? false{
            self.btn_Arrival.setTitle(rideTime, for: .normal)
        }else {
            self.btn_Exit.setTitle(rideTime, for: .normal)
        }
       _ = self.daySelectionValidation()

    }
    func viewRemoved(_ remove: Bool) {
        
    }
    func viewShouldPop(_ pop: Bool) {
        
    }
}
