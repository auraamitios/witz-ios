//
//  AggregateDropAddressViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/21/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
class AggregateDropAddressViewController: WitzSuperViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_PickUp: UILabel!
    @IBOutlet weak var lbl_Destination: UILabel!
    var userMovedMap:Bool?
//    var routeDistance:[Any]?
    let locManager = CLLocationManager()
    
    @IBOutlet weak var imgView_Pin: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        locManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
            })
            return
        }
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        showJourneyAddress()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userMovedMap = false
        fetchLocation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func fetchLocation() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        //locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        locManager.requestLocation()
        showLoader()
    }
    func showJourneyAddress() {
        if RiderManager.sharedInstance.homeWorkAddressSelected ?? false {
            fillDropLocationAddress()
            showLoader()
            addPickLocationIcon()
            addDropLocationIcon()
            getPathPoints_Second()
            self.imgView_Pin.isHidden = true
        }
//        fillPickUpLocationAddress()
        self.lbl_PickUp.text = RiderManager.sharedInstance.pickUpAddress ?? ""
    }
//    fileprivate func fillPickUpLocationAddress() {
//        ReverseGeocode.getAddressByCoords(coords: AppManager.sharedInstance.pickUpCoordinates, returnAddress: { (returnAddress) in
//            print(returnAddress)
//            self.lbl_PickUp.text = returnAddress
//        })
//    }
    fileprivate func fillDropLocationAddress() {
        ReverseGeocode.getAddressByCoords(coords: AppManager.sharedInstance.dropCoordinates, returnAddress: { (returnAddress) in
            self.lbl_Destination.text = returnAddress
        })
    }
    
    fileprivate func addPickLocationIcon() {
        let pickPosition = AppManager.sharedInstance.pickUpCoordinates
        let position = CLLocationCoordinate2D(latitude: pickPosition.latitude, longitude:pickPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        marker.icon = UIImage(named: "on_map_starting_location")
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    fileprivate func addDropLocationIcon () {
        let dropPosition = AppManager.sharedInstance.dropCoordinates
        let position = CLLocationCoordinate2D(latitude: dropPosition.latitude, longitude:dropPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        marker.icon = UIImage(named: "on_map_arrival_location")
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    //MARK:: Draw root path
    //****** called in case of search, Home or Work address option selected **********//
    fileprivate func getPathPoints_Second() {
        hitsLimit_Google = 5

        GooglePathHelper.getPathPolyPoints(from: AppManager.sharedInstance.pickUpCoordinates, destination: AppManager.sharedInstance.dropCoordinates) { (returnPath, distance) in
            self.hideLoader()
//            self.routeDistance = distance
            self.showPath(polyStr: returnPath)
        }
    }
    //****** called in case of pin destination option **********//
    fileprivate func getPathPoints() {
        hitsLimit_Google = 5

        GooglePathHelper.getPathPolyPoints(from: AppManager.sharedInstance.pickUpCoordinates, destination: AppManager.sharedInstance.dropCoordinates) { (returnPath, distance) in
            self.hideLoader()
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = App_Base_Color
        polyline.map = self.mapView
        DispatchQueue.main.async {
            let bounds = GMSCoordinateBounds(path: path!)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 15.0))
//            self.mapView.animate(toZoom: 12.0)
        }
    }
    
    func confirmAggregateRequest(arg: Bool, completion: (Bool) -> ()) {
        guard !(self.lbl_Destination.text?.isEmpty)! else {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderAlerts.DropLocation, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: actionHandler, controller: self)
            return
        }
        if RiderManager.sharedInstance.homeWorkAddressSelected ?? false {
//            self.performSegue(withIdentifier: "segue_SetTime", sender: nil)
        }else {
            showLoader()
            DispatchQueue.main.async {
                self.addPickLocationIcon()
                self.addDropLocationIcon()
                self.getPathPoints()
//                self.performSegue(withIdentifier: "segue_SetTime", sender: nil)
            }
        }
        completion(arg)
    }
    @IBAction func action_SetTime(_ sender: UIButton) {
        confirmAggregateRequest(arg: true) { (completion) in
            if completion {
                self.performSegue(withIdentifier: "segue_SetTime", sender: nil)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension AggregateDropAddressViewController:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.hideLoader()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        AppManager.sharedInstance.locationCoordinates = locations
        if let location = locations.last {
            self.mapView.animate(to: GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0))
            self.hideLoader()
            self.locManager.stopUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.hideLoader()
    }
}

//MARK:: Mapview delegates
extension AggregateDropAddressViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {}
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        userMovedMap = true
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        if RiderManager.sharedInstance.homeWorkAddressSelected ?? false {
            self.lbl_Destination.text = RiderManager.sharedInstance.dropAddress ?? ""
        }else {
            guard self.userMovedMap ?? false else {
                return
            }
            returnPostionOfMapView(mapView: mapView)
        }
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        AppManager.sharedInstance.dropCoordinates = position
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            self.lbl_Destination.text = returnAddress
            RiderManager.sharedInstance.dropAddress  = returnAddress
        })
    }
}
