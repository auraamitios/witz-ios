//
//  AggregateLandingViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/21/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class AggregateLandingViewController: WitzSuperViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_PickUp: UILabel!
    
    let locManager = CLLocationManager()
    var drivers:NearByDrivers?
    var riderCoords:CLLocationCoordinate2D?
    var driverCoords:CLLocationCoordinate2D?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        locManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
            })
            return
        }
        
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchLocation()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        displayUserCurrentLocation()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locManager.stopUpdatingLocation()
    }
    public func fetchLocation() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        //locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        locManager.requestLocation()
        showLoader()
    }
    func displayUserCurrentLocation() {
        let locations = AppManager.sharedInstance.locationCoordinates
        guard let coordinates = locations.last?.coordinate else { return}
        self.mapView.camera = GMSCameraPosition(target: coordinates, zoom: 15, bearing: 0, viewingAngle: 0)
        let position = CLLocationCoordinate2D(latitude: (coordinates.latitude), longitude:(coordinates.longitude))
        let marker : GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
        marker.position = position
        marker.title = "Witz"
        marker.map = self.mapView
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon = UIImage(named: "vehicle-car_on-map")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func action_Destination(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segue_SearchAddress", sender: nil)
    }
    
    @IBAction func action_LocateMe(_ sender: UIButton) {
        displayUserCurrentLocation()
    }
    
    func getNearByDriversList(position:CLLocationCoordinate2D){
        guard self.checkNetworkStatus() else {return}
        let params = ["lat":(position.latitude), "lng":(position.longitude)] as [String : Any]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/instant", withInputString: params, requestType: .post, isAuthorization: true, success: { (response) in
            print(response ?? "")
            if response?["statusCode"] == 200{
                if let result = response?["data"]{
                    self.drivers = NearByDrivers.init(json: result)
                    if self.drivers?.driver?.count == 0 {
                        return
                    }
                    let data = self.drivers?.driver![0]
                    if let coord = data?.currentLocation?.coordinates {
                        let lng = coord[0]
                        let lat = coord[1]
                        self.driverCoords = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))
                        self.getPathPoints()
                    }
                }
            }
        }, failure: { (error) in
            print(error.debugDescription)
        })
    }
    
    fileprivate func getPathPoints() {
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: self.riderCoords!, destination: self.driverCoords!) { (returnPath, distance) in
            DispatchQueue.main.async {
                self.getTimeDetail(data: distance)
            }
        }
    }
    
    func getTimeDetail(data:[Any]) {
        if data.count > 0 {
            if let detail = data[0] as? [String:Any]{
                let time = detail["duration"] as? [String:Any]
                let timeStr = time!["text"] as! String
                print("time-->\(timeStr)")
                self.lbl_Time.text = timeStr
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AggregateLandingViewController:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.hideLoader()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        AppManager.sharedInstance.locationCoordinates = locations
        if let location = locations.last {
            self.mapView.animate(to: GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0))
            self.hideLoader()
            self.locManager.stopUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.hideLoader()
    }
}

//MARK:: Mapview delegates
extension AggregateLandingViewController:GMSMapViewDelegate {
    
    //called when the map is idle
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        riderCoords = position
        getNearByDriversList(position: position)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                self.lbl_PickUp.text = returnAddress
            }
        })
        
        AppManager.sharedInstance.pickUpCoordinates = position
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        let latitute = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        let position = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)
        AppManager.sharedInstance.pickUpCoordinates = position
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                RiderManager.sharedInstance.pickUpAddress = returnAddress
                self.lbl_PickUp.text = returnAddress
            }
        })
    }
}
