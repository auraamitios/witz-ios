//
//  AggregateConfirmedViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/21/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class AggregateConfirmedViewController: WitzSuperViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.left_barButton.isHidden = true
        let backBtn = UIButton(type: .custom)
        backBtn.setImage(UIImage(named: "Round_back"), for: .normal)
        backBtn.addTarget(self, action: #selector(AggregateConfirmedViewController.backToDashboard), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = item1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backToDashboard() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is TransferDashboardViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    @IBAction func action_Ok(_ sender: UIButton) {
        backToDashboard()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
