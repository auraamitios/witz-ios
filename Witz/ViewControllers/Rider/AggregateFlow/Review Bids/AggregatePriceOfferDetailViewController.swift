//
//  AggregatePriceOfferDetailViewController.swift
//  Witz
//
//  Created by abhishek kumar on 05/03/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class AggregatePriceOfferDetailViewController: WitzSuperViewController {
    @IBOutlet weak var lbl_Brand: UILabel!
    @IBOutlet weak var lbl_PlateNumber: UILabel!
    @IBOutlet weak var view_MapView: GMSMapView!
    @IBOutlet weak var lbl_WeekDays: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_StartAddress: UILabel!
    @IBOutlet weak var lbl_EndAddress: UILabel!
    @IBOutlet weak var lbl_costCalculation: UILabel!
    @IBOutlet weak var lbl_Change: UILabel!
    @IBOutlet weak var lbl_PaymentMethod: UILabel!
    
    var modalToSend : AggreateRequestBidModal?
    var modalTrip : Trips?
    var cardAdded:Bool?
     var dictMarkAdmin = [String:LocationsAggregate]()
    
    @IBOutlet weak var btn_Pay: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        btn_Pay.isUserInteractionEnabled = false
        btn_Pay.alpha = 0.6
        if modalToSend != nil {
            self.showTripData()
            //            if let idToSend = modalToSend?.tripId?.id{
            ////                loadServerData(id: idToSend)
            //            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func termsOfUse(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        controller.strURL = termURL
        self.navigationController?.pushViewController(controller)
    }
    
    @IBAction func change_Payment(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "AddPaymentViewController") as! AddPaymentViewController
        self.navigationController?.pushViewController(controller)
    }
    
    @IBAction func action_Pay(_ sender: UIButton) {
        guard self.cardAdded ?? false else {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderBooking.CardNotAdded, style: .alert, actionButtonTitle: LinkStrings.Done, completionHandler: { (action) in
            }, controller: self)
            return
        }
        var paymentId:String?
        if (RiderManager.sharedInstance.riderLoginData?.cards?.count ?? 0) > 0{
            if RiderManager.sharedInstance.riderLoginData?.cards?[0].defaultStatus ?? false {
                paymentId = RiderManager.sharedInstance.riderLoginData?.cards?[0].id ?? "5a251b5aab354d6839d0bb89"
            }
        }
        var bidId:String?
        if (modalToSend?.bidData?.count ?? 0) > 0{
            let modal =  modalToSend?.bidData![0]
            bidId = modal?.id
        }
        
        guard self.checkNetworkStatus() else {return}
        showLoader()
        let params = ["bidId":bidId ?? "","cardId":paymentId ?? "5a251b5aab354d6839d0bb89","tripId":modalToSend?.tripId?.id ?? ""]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "aggregate/accept_bid_for_aggregate_trip", withInputString: params, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            print(response ?? "")
            self.performSegue(withIdentifier: "segue_PaymentDone", sender: nil)
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
    
    @IBAction func action_acceptTerms(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        btn_Pay.isUserInteractionEnabled = false
        btn_Pay.alpha = 0.6
        
        if sender.isSelected == true{
            btn_Pay.isUserInteractionEnabled = true
            btn_Pay.alpha = 1.0
        }
    }
    
    @IBAction func action_ShowRoute(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: LinkStrings.StoryboardName.Rider, bundle: nil)
    let controller = storyBoard.instantiateViewController(withIdentifier: "DisplayRouteViewController") as! DisplayRouteViewController
        controller.tripData = self.modalTrip
        controller.dictMarkAdmin = self.dictMarkAdmin
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    fileprivate func showTripData(){
        
        //Show card if rider got any
        if (RiderManager.sharedInstance.riderInfo?.cards?.count ?? 0) > 0{
            lbl_PaymentMethod.text = "\(RiderManager.sharedInstance.riderInfo?.cards?[0]["cardType"] ?? "")"
            cardAdded = true
            self.lbl_Change.isHidden = false
        }else {
            cardAdded = false
            self.lbl_Change.isHidden = true
        }
        
        //        show trip related details
        if (modalToSend?.bidData?.count ?? 0) > 0{
            let modal =  modalToSend?.bidData![0]
            self.lbl_Brand.text = "\(modal?.driverId?.vehicleInformation?.subBrand ?? "") \(modal?.driverId?.vehicleInformation?.color ?? "")"
            self.lbl_PlateNumber.text = modal?.driverId?.vehicleInformation?.vehicleNumber
        }
        if modalTrip != nil {
            let bidAmount = String(format: "%.2f",(self.modalToSend?.bidData![0].perseatPrice ?? 0.0) / Float(modalTrip?.numberOfDays ?? 0))
            let str = "\(bidAmount) TL x \(modalTrip?.numberOfDays ?? 0) = \(self.modalToSend?.bidData![0].perseatPrice?.twoDecimalString ?? " ") TL"
            self.modalToSend?.tripId?.numberOfDays = modalTrip?.numberOfDays // save days to pass on another view
            self.lbl_costCalculation.text = str
            self.lbl_WeekDays.text = Utility.aggregateDays(data: modalTrip!)
            if (modalTrip?.workingTime?.count ?? 0) > 0  {
                let dict = modalTrip?.workingTime?[0]
                self.lbl_Time.text = "Time : \(dict?.time?.arivalTime ?? "00:00") - \(dict?.time?.departureTime ?? "00:00")"
            }
            self.lbl_StartAddress.text = modalTrip?.startAddress
            self.lbl_EndAddress.text = modalTrip?.endAddress
            
            let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(modalTrip?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modalTrip?.startLocation?.coordinates?[0] ?? 0))
            let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(modalTrip?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modalTrip?.endLocation?.coordinates?[0] ?? 0))
            
            //Marker for pick up
            let marker1 = GMSMarker(position: coordinates)
            marker1.title = "Pick UP Location"
            marker1.appearAnimation = GMSMarkerAnimation.pop
            marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
            marker1.map = self.view_MapView
            
            //Marker for Drop
            let marker2 = GMSMarker(position: coordinates1)
            marker2.title = "Drop Location"
            marker2.appearAnimation = GMSMarkerAnimation.pop
            marker2.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
            marker2.map = self.view_MapView
            self.showPolyLine(source: coordinates, destination: coordinates1)
            fillDataInAnotherModal() // parse data to pass on another view controller
        }
    }
    
    func fillDataInAnotherModal(){
        
        modalToSend?.tripId?.startAddress = modalTrip?.startAddress
        modalToSend?.tripId?.endAddress = modalTrip?.endAddress
        modalToSend?.tripId?.startLocation?.type = modalTrip?.startLocation?.type
        modalToSend?.tripId?.endLocation?.type = modalTrip?.startLocation?.type
        modalToSend?.tripId?.startLocation?.coordinates = modalTrip?.startLocation?.coordinates
        modalToSend?.tripId?.endLocation?.coordinates = modalTrip?.endLocation?.coordinates
    }
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.view_MapView
            let bounds = GMSCoordinateBounds(path: path)
            self.view_MapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let controller = segue.destination as! AggregatePaymentRecievedViewController
        controller.modalRecieve = modalToSend
        controller.weekDayData = self.lbl_WeekDays.text
        controller.timeData = self.lbl_Time.text
    }
}

