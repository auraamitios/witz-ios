//
//  ServiceReuestAggregateRiderViewController.swift
//  Witz
//
//  Created by abhishek kumar on 06/03/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON

class ServiceReuestAggregateRiderViewController: WitzSuperViewController {
    
    var modalAggregate : AggreateRequestBidModal?
    var notificationID : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.left_barButton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
        if notificationID != nil {
            loadServerData(id: notificationID!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadServerData(id:String){
        guard self.checkNetworkStatus() else {return}
        showLoader()
        let params = ["notification_id":id]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_aggregate_bid_data", withInputString: params, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            print(response ?? "")
//            self.modalTrip = Trips.init(json: response?["data"]["data"] ?? ["":""])
//            self.bidAmount = "\((self.modalAggRec?.bidData![0].perseatPrice ?? 0.0) / Float(self.modalTrip?.numberOfDays ?? 0))"
//            self.table_View.reloadData()
            if let value = response?["data"]["bidData"].array{
                if value.count > 0 {
                    let modal = BidData_BidAggregate.init(json: value[0])
                    self.modalAggregate  = AggreateRequestBidModal.init(json: response!) // intiate the modal with wrong data to get the instatnce
                    self.modalAggregate?.bidData = [modal]
                    self.modalAggregate?.tripId?.id = response?["data"]["tripId"].string
                    self.modalAggregate?.tripId?.numberOfDays = response?["data"]["numberOfDays"].int
                    self.modalAggregate?.groupAggregateTripId = response?["data"]["groupAggregateTripId"].string
                   
                }
            }
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
    func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let controller = segue.destination as! OfferAggregateRecievedViewController
        controller.modalAggRec = self.modalAggregate
        
    }
    
    
}
