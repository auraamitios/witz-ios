//
//  OfferAggregateRecievedViewController.swift
//  Witz
//
//  Created by abhishek kumar on 06/03/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON

class OfferAggregateRecievedViewController: WitzSuperViewController {
    
    var modalAggRec : AggreateRequestBidModal?
    var bidAmount : String?
    var modalTrip : Trips?
    fileprivate var dictMarkAdmin = [String:LocationsAggregate]()

    
    @IBOutlet weak var table_View: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let idToSend = modalAggRec?.tripId?.id{
            loadServerData(id: idToSend)
        }
    }
    
    func loadServerData(id:String){
        guard self.checkNetworkStatus() else {return}
        showLoader()
        let params = ["tripId":id]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_trip_details", withInputString: params, requestType: .put, isAuthorization: true, success: { (response) in
            self.hideLoader()
            print(response ?? "")
            self.modalTrip = Trips.init(json: response?["data"]["data"] ?? ["":""])
            self.bidAmount = String(format: "%.2f",((self.modalAggRec?.bidData![0].perseatPrice ?? 0.0) / Float(self.modalTrip?.numberOfDays ?? 0)))
            self.dictMarkAdmin[dictKey.pick] =  LocationsAggregate.init(json:  response?["data"]["data"]["adminMarkPickUpLocation"] ?? ["":""])
            self.dictMarkAdmin[dictKey.drop] =  LocationsAggregate.init(json:  response?["data"]["data"]["adminMarkDropOffLocation"] ?? ["":""])
            self.table_View.reloadData()
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension OfferAggregateRecievedViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBidAggregate") as! CellBidsRider
        if modalAggRec != nil && bidAmount != nil{
            cell.configureCellAggregateRider(with: modalAggRec!,bidAmount:  bidAmount!, index: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "segue_PriceDetail", sender: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let controller = segue.destination as! AggregatePriceOfferDetailViewController
        controller.modalToSend = self.modalAggRec
        controller.modalTrip = self.modalTrip
        controller.dictMarkAdmin = self.dictMarkAdmin
    }
}
