//
//  AggregatePaymentRecievedViewController.swift
//  Witz
//
//  Created by abhishek kumar on 06/03/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class AggregatePaymentRecievedViewController: WitzSuperViewController {
    
    @IBOutlet weak var lbl_paymentDetails: UILabel!
    @IBOutlet weak var lbl_Brand: UILabel!
    @IBOutlet weak var lbl_PlateNumber: UILabel!
    @IBOutlet weak var lbl_StartAddress: UILabel!
    @IBOutlet weak var lbl_EndAddress: UILabel!
    @IBOutlet weak var view_MapView: GMSMapView!
    @IBOutlet weak var lbl_WeekDays: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    var modalTrip : Trips?
    var modalRecieve : AggreateRequestBidModal?
    var timeData : String?
    var weekDayData : String?
    var isServerDataRequired : Bool?
    var dictMarkAdmin = [String:LocationsAggregate]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if modalRecieve != nil {
            showTripData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button ACtion
    @IBAction func action_Ok(_ sender: UIButton) {
        Utility.riderSideMenuSetUp()
    }
    
    @IBAction func action_ShowRoute(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "DisplayRouteViewController") as! DisplayRouteViewController
        controller.tripData = self.modalTrip
        controller.dictMarkAdmin = self.dictMarkAdmin
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Method Custom

    fileprivate func showTripData(){
        if (modalRecieve?.bidData?.count ?? 0) > 0{
            let modal =  modalRecieve?.bidData![0]
            self.lbl_Brand.text = "\(modal?.driverId?.vehicleInformation?.subBrand ?? "") \(modal?.driverId?.vehicleInformation?.color ?? "")"
            self.lbl_PlateNumber.text = modal?.driverId?.vehicleInformation?.vehicleNumber
            self.lbl_StartAddress.text = modalRecieve?.tripId?.startAddress
            self.lbl_EndAddress.text = modalRecieve?.tripId?.endAddress
            self.lbl_paymentDetails.text = "\(modal?.perseatPrice?.twoDecimalString ?? " ") TL"
            let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(modalRecieve?.tripId?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modalRecieve?.tripId?.startLocation?.coordinates?[0] ?? 0))
            let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(modalRecieve?.tripId?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(modalRecieve?.tripId?.endLocation?.coordinates?[0] ?? 0))
            self.lbl_WeekDays.text = weekDayData
            self.lbl_Time.text = timeData
            
            if isServerDataRequired == true{
                if let idToSend = modalRecieve?.tripId?.id{
                    self.loadServerData(id: idToSend)
                }
            }
            //Marker for pick up
            let marker1 = GMSMarker(position: coordinates)
            marker1.title = "Pick UP Location"
            marker1.appearAnimation = GMSMarkerAnimation.pop
            marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
            marker1.map = self.view_MapView
            
            //Marker for Drop
            let marker2 = GMSMarker(position: coordinates1)
            marker2.title = "Drop Location"
            marker2.appearAnimation = GMSMarkerAnimation.pop
            marker2.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
            marker2.map = self.view_MapView
            self.showPolyLine(source: coordinates, destination: coordinates1)
        }
    }
        func loadServerData(id:String){
            guard self.checkNetworkStatus() else {return}
            showLoader()
            let params = ["tripId":id]
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_trip_details", withInputString: params, requestType: .put, isAuthorization: true, success: { (response) in
                self.hideLoader()
                print(response ?? "")
                let modalTrip = Trips.init(json: response?["data"]["data"] ?? ["":""])
                self.modalTrip = modalTrip
                self.lbl_WeekDays.text = Utility.aggregateDays(data: modalTrip)
                if (modalTrip.workingTime?.count ?? 0) > 0  {
                    let dict = modalTrip.workingTime?[0]
                    self.lbl_Time.text = "Time : \(dict?.time?.arivalTime ?? "00:00") - \(dict?.time?.departureTime ?? "00:00")"
                }
                
                let bidAmount = String(format: "%.2f",(modalTrip.price ?? 0) / Float(modalTrip.numberOfDays ?? 0))
                self.lbl_paymentDetails.text = "\(bidAmount) TL x \(modalTrip.numberOfDays ?? 0) = \(modalTrip.price?.twoDecimalString ?? " ") TL"
                
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }
    
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.view_MapView
            let bounds = GMSCoordinateBounds(path: path)
            self.view_MapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
    }
    
}
