//
//  AcceptBidSuccessViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/30/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class AcceptBidSuccessViewController: WitzSuperViewController {

     var backBtn = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.titleView = nil
        self.navigationItem.title = "APPROVAL OF OFFER"
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        
        self.left_barButton.isHidden = true
        self.backBtn = UIButton(type: .custom)
        self.backBtn.setImage(UIImage(named: "Round_back"), for: .normal)
        self.backBtn.addTarget(self, action: #selector(AcceptBidSuccessViewController.backToTripsMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: self.backBtn)
        self.navigationItem.leftBarButtonItem = item1
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(backToTripsMenu))
        self.view.addGestureRecognizer(tap)
    }

    func backToTripsMenu() {
        Utility.riderSideMenuSetUp()
//        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
////            for aViewController in viewControllers {
////                if aViewController is TripsViewController {
////                    self.navigationController!.popToViewController(aViewController, animated: false)
////                }
////            }
//        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
