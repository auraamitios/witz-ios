//
//  RiderJourneyPlanViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 12/1/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import Alamofire

class RiderJourneyPlanViewController: WitzSuperViewController {

    @IBOutlet weak var planRide_Map: GMSMapView!
    @IBOutlet weak var view_SelectLocation: UIView!
    @IBOutlet weak var lbl_PickUpAddress: UILabel!

    @IBOutlet weak var btn_ConfirmRide: UIButton!
    @IBOutlet weak var lbl_DateTime: UILabel!
    
    //reserve vehicles view iVar
    @IBOutlet weak var pinIcon: UIImageView!
    @IBOutlet weak var lbl_PastBookingAlert: UILabel!
    
//    var locUpdate:LocationUpdate!
    let locManager = CLLocationManager()
    var timeZoneId:String?
    var reservationRide:ReservationRideModal?
    var dateData:[Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.planRide_Map.bringSubview(toFront: self.view_SelectLocation)
        self.planRide_Map.bringSubview(toFront: self.pinIcon)
        
        self.pinIcon.isHidden = true
        RiderManager.sharedInstance.pickUpAddress = ""
        self.planRide_Map.bringSubview(toFront: self.lbl_PastBookingAlert)
        
        
        //check if user give the permission of location
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
            return
        }
        do {
            // Set the map style by passing a valid JSON string.
            planRide_Map.mapStyle = try GMSMapStyle(jsonString: GOOGLE_STYLE_SILVER)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        ////********* Location  permissions
        showLoader()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        locManager.startUpdatingLocation()
        locManager.requestLocation()
        
        view_SelectLocation.isHidden = true
        DispatchQueue.global(qos: .background).async {
            self.getCurrentTimeZone()
        }
        
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.JourneyPlan
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showDatePickerView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      locManager.stopUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showDatePickerView(){
        let storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.DatePicker, bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "WitzDatePicker") as? WitzDatePicker{
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            controller.pickerDelegate = self
            self.present(controller, animated: true, completion: {
            })
        }
    }
    
    func removeDatePickerView() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func showLocationSelectionView(alter:Bool) {
//        if alter {
//            self.lbl_Arrival.isHidden = false
//            self.lbl_Destination.text = "Destination"
//            self.btn_ConfirmRide.isEnabled = false
//            self.btn_ConfirmRide.alpha = 0.7
//            self.view_SelectLocation.frame = CGRect(x: 0, y: Utility.windowHeight()-374, width: Utility.windowWidth(), height: 310)
//            self.planRide_Map.addSubview(self.view_SelectLocation)
//            self.planRide_Map.bringSubview(toFront: self.view_SelectLocation)
//        }else {
//            self.lbl_Arrival.isHidden = true
//            self.lbl_Destination.text = "Where ?"

            self.view_SelectLocation.frame = CGRect(x: 0, y: Utility.windowHeight()-310, width: Utility.windowWidth(), height: 310)
            self.planRide_Map.addSubview(self.view_SelectLocation)
            self.planRide_Map.bringSubview(toFront: self.view_SelectLocation)
//        }
        self.view.layoutIfNeeded()
    }
    
    //MARK:: API call
    
    fileprivate func getCurrentTimeZone() {
        guard self.checkNetworkStatus() else {return}
        let locations = AppManager.sharedInstance.locationCoordinates
        let coordinates = locations.last?.coordinate
        let url = URL(string: "http://api.geonames.org/timezoneJSON?lat=\(coordinates?.latitude ?? 0)&lng=\(coordinates?.longitude ?? 0)&username=shanker.yapits")
        if url == nil {
            return
        }
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    print(json)
                    print("timeZone-->\(json["timezoneId"] ?? "" as AnyObject)******gmtOffset-->\(json["gmtOffset"] ?? "" as AnyObject)")
                    DispatchQueue.main.async {
                        self.timeZoneId = (json["timezoneId"] as? String)
                        RiderManager.sharedInstance.timeZoneId = (json["timezoneId"] as? String)
//                        self.lbl_TimeZone.text = (json["gmtOffset"] as? String)
                        print("timeZoneid--\(self.timeZoneId ?? "")")
                    }
                    OperationQueue.main.addOperation({
                    })
                }catch let error as NSError{
                    print(error)
                }
            }
        }).resume()
    }
    
    func showLocationView() {
        view_SelectLocation.isHidden = false
        self.pinIcon.isHidden = false
        showLocationSelectionView(alter: false)
    }
    //MARK::IBOutlets actions
 
    @IBAction func action_DropAddress(_ sender: UIButton) {
        self.view_SelectLocation.removeFromSuperview()
//        showLocationSelectionView(alter: true)
//        self.performSegue(withIdentifier: "reserve_Drop", sender: nil)
        self.performSegue(withIdentifier: "reserve_Ride", sender: nil)
    }
}

//MARK::GMSMapViewDelegate
extension RiderJourneyPlanViewController:GMSMapViewDelegate {
    
    //called when the map is idle
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
//        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
//        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
//            DispatchQueue.main.async {
//                if RiderManager.sharedInstance.chooseDropLocResrve ?? false {
//                    self.lbl_Destination.text = returnAddress
//                }else {
//                    self.lbl_PickUpAddress.text = returnAddress
//                }
//            }
//        })
//        AppManager.sharedInstance.pickUpCoordinates = position
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
       
        let latitute = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        let position = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                self.lbl_PickUpAddress.text = returnAddress
                RiderManager.sharedInstance.pickUpAddress = returnAddress
                AppManager.sharedInstance.pickUpCoordinates = position
            }
        })
    }
}

//MARK::LocationDelegates
extension RiderJourneyPlanViewController:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.planRide_Map.isMyLocationEnabled = true
            self.planRide_Map.settings.myLocationButton = true
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        AppManager.sharedInstance.locationCoordinates = locations
        if let location = locations.last {
            self.planRide_Map.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.locManager.stopUpdatingLocation()
            self.hideLoader()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.hideLoader()
    }
}

//extension RiderJourneyPlanViewController:LocationDelegates {
//    func authorizationStatus(status: CLAuthorizationStatus) {
//        if status == .authorizedWhenInUse {
//            self.planRide_Map.isMyLocationEnabled = true
//            self.planRide_Map.settings.myLocationButton = true
//        }
//    }
//
//    func didUpdateLocation(locations: [CLLocation]) {
//        if let location = locations.last {
//            self.planRide_Map.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
////            let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
////            let marker = GMSMarker(position: position)
////            marker.title = "Witz"
////            marker.map = self.planRide_Map
////            marker.appearAnimation = GMSMarkerAnimation.pop
////            marker.icon = UIImage(named: "vehicle-car_on-map")
//            self.locUpdate.stopTracking()
//            self.hideLoader()
//        }
//    }
//    func didFailToUpdate(error: Error) {
//         self.hideLoader()
//    }
//}
//MARK:: WitzDatePickerDelegate
extension RiderJourneyPlanViewController:WitzDatePickerDelegate {
    func selectedDate(_ pickerView: WitzDatePicker, bookingDate: Date, selectedDate date: [Any]) {
        print("date-->\(date)")
        dateData = date
        lbl_DateTime.text = dateData![0] as? String
        RiderManager.sharedInstance.reservationBookingDate = bookingDate
        RiderManager.sharedInstance.resereBookDate = dateData![0] as? String
//        checkMinReservationTime()
    }
    
    func viewRemoved(_ remove: Bool) {
        if remove {self.showLocationView()}
    }
    func viewShouldPop(_ pop: Bool) {
        self.navigationController?.popViewController(animated: false)
    }
}
