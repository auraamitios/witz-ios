//
//  LocateAddressViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/19/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
class LocateAddressViewController: WitzSuperViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var btnAddress: UIButton!
    
    var addressType:String?
    var coord:[Double]?
    var finalAddress:String?
    let locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: "Ok", completionHandler: { (action:UIAlertAction!) -> Void in
            })
            return
        }
        fetchLocation()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locManager.stopUpdatingLocation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func fetchLocation() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        //locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        locManager.requestLocation()
        showLoader()
    }
    @IBAction func action_Address(_ sender: UIButton) {
        addAddressToServer(data: coord!, finalAddress!)
    }
    
    //MARK:: API call
    func addAddressToServer(data:[Double], _ address:String) {
        guard self.checkNetworkStatus() else {return}
        print(data)
        self.showLoader()
        var parameters = ["lat":(data[0]), "lng":(data[1])] as [String : Any]
        var methodName = ""
        if addressType == "Add home address" {
            methodName = "rider/add_home_address"
            parameters["homeAddress"] = address
        }else {
            methodName = "rider/add_work_address"
            parameters["workAddress"] = address
        }
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: methodName, withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            print(response as Any)
            DispatchQueue.main.async {
                self.hideLoader()
                if response?["statusCode"] == 200 {
                    let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
                    RiderManager.sharedInstance.riderLoginData = riderData
                    self.performSegue(withIdentifier: "segue_AddAddressPin", sender:nil)
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
    
//    func backToSearchAddress() {
//        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
//            for aViewController in viewControllers {
//                if aViewController is SearchAddressViewController {
//                    self.navigationController!.popToViewController(aViewController, animated: false)
//                }
//            }
//        }
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LocateAddressViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {}
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        AppManager.sharedInstance.dropCoordinates = position
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                self.lbl_Address.text = returnAddress
                self.coord = [mapView.camera.target.latitude, mapView.camera.target.longitude]
                self.finalAddress = returnAddress
                RiderManager.sharedInstance.dropAddress = returnAddress
            }
        })
    }
}
extension LocateAddressViewController:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.hideLoader()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.hideLoader()
           self.locManager.stopUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.hideLoader()
    }
}
