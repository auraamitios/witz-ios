//
//  ReservationPinDestinationViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/29/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import Alamofire
class ReservationPinDestinationViewController: WitzSuperViewController {
    
    @IBOutlet weak var Constraint_Bottom_PlanRideMap: NSLayoutConstraint!
    @IBOutlet weak var planRide_Map: GMSMapView!
    @IBOutlet weak var view_SelectLocation: UIView!
    @IBOutlet weak var lbl_PickUpAddress: UILabel!
    @IBOutlet weak var lbl_Destination: UILabel!
    @IBOutlet weak var btn_ConfirmRide: UIButton!
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var lbl_Toast: UILabel!
    
    @IBOutlet weak var constraint_ViewPickAdd: NSLayoutConstraint!
    //reserve vehicles view iVar
    @IBOutlet var view_ReserveVehicles: UIView!
    @IBOutlet weak var vehciles_CollectionView: UICollectionView!
    @IBOutlet weak var pinIcon: UIView!
    @IBOutlet weak var lbl_PayMethod: UILabel!
    @IBOutlet weak var btn_Change: UIButton!
    
    var lbl_Title : UILabel?
    let objWitzConnMgr = RiderConnectionManager()
    var locUpdate:LocationUpdate!
    var passsengerCount = "1"
    var currentPrice = ""
    var timeZoneId:String?
    
    var reservationRide:ReservationRideModal?
    var toggleArry = [""]
    
    var lowerPrice = 0
    var higherPice = 0
    var userSelectedVehicle = ""
    var cardAdded:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        locUpdate = LocationUpdate.sharedInstance
        locUpdate.delegate = self
        locUpdate.startTracking()
        showLoader()
        getReservationVehicles()
        fillReservationDetails()
        showPinLocationView()
        self.objWitzConnMgr.delegate = self
        DispatchQueue.global(qos: .background).async {
            self.getCurrentTimeZone()
        }
        if RiderManager.sharedInstance.dropAddress != "" {
            //            self.planRide_Map.settings.setAllGesturesEnabled(false)
            addPickLocationIcon()
            addDropLocationIcon()
            getPathPoints(false)
            self.pinIcon.isHidden = true
            self.planRide_Map.camera = GMSCameraPosition(target:AppManager.sharedInstance.dropCoordinates, zoom: 15, bearing: 0, viewingAngle: 0)
        }
        
        if RiderManager.sharedInstance.homeWorkAddressSelected ?? false {
            self.pinIcon.isHidden = true
        }
        
        lbl_Title = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: 0, width: 150, height: 30))
        lbl_Title?.font = UIFont.systemFont(ofSize: 15)
        lbl_Title?.textColor = .white
        lbl_Title?.textAlignment = .center
        self.navigationItem.titleView = lbl_Title
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (RiderManager.sharedInstance.riderLoginData?.cards?.count ?? 0) > 0{
            
            lbl_PayMethod.text = RiderManager.sharedInstance.riderLoginData?.cards?[0].cardType ?? "VISA"
            cardAdded = true
            self.btn_Change.setTitle("Change", for: .normal)
            
        }else {
            cardAdded = false
            self.btn_Change.setTitle("", for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        RiderManager.sharedInstance.dropAddress = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: API call
    fileprivate  func getSpecialPrice(_ vehicle:String){
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_price", withInputString: ["tripId":self.reservationRide?.id ?? "","vehicleType":vehicle], requestType: .post, isAuthorization: true, success: { (response) in
            print("response***price\(String(describing: response))")
            self.hideLoader()
            if response?["statusCode"] == 200 {
                if let price = response?["lowerPrice"].int {
                    self.lowerPrice = price
                }
                if let price1 = response?["higherPrice"].int {
                    self.higherPice = price1
                }
                self.vehciles_CollectionView.reloadData()
            }
        }, failure: { (error) in
            self.hideLoader()
            print(error.debugDescription)
        })
    }
    fileprivate func getReservationVehicles() {
        guard self.checkNetworkStatus() else {return}
        let params: Parameters = ["reservation": "true"]
        if RiderManager.sharedInstance.vehiclesData.count>0 {
            RiderManager.sharedInstance.vehiclesData.removeAll()
        }
        self.objWitzConnMgr.APICall(functionName: "rider/get_all_vehicles", withInputString: params, requestType: .post, withCurrentTask: .GET_ALL_VEHICLES, isAuthorization: true)
    }
    
    fileprivate func getCurrentTimeZone() {
        guard self.checkNetworkStatus() else {return}
        let locations = AppManager.sharedInstance.locationCoordinates
        let coordinates = locations.last?.coordinate
        let url = URL(string: "http://api.geonames.org/timezoneJSON?lat=\(coordinates?.latitude ?? 0)&lng=\(coordinates?.longitude ?? 0)&username=shanker.yapits")
        if url == nil {
            return
        }
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    print(json)
                    print("timeZone-->\(json["timezoneId"] ?? "" as AnyObject)******gmtOffset-->\(json["gmtOffset"] ?? "" as AnyObject)")
                    DispatchQueue.main.async {
                        self.timeZoneId = (json["timezoneId"] as? String)
                        RiderManager.sharedInstance.timeZoneId = (json["timezoneId"] as? String)
                        //                        self.lbl_TimeZone.text = (json["gmtOffset"] as? String)
                        print("timeZoneid--\(self.timeZoneId ?? "")")
                    }
                    OperationQueue.main.addOperation({
                    })
                }catch let error as NSError{
                    print(error)
                }
            }
        }).resume()
    }
    
    //MARK::book reservation ride
    fileprivate func bookReservationRide(data:[Any]) {
        let detail = data[0] as! [String:Any]
        let sCoor = detail["start_location"] as? [String:Any]
        let eCoor = detail["end_location"] as? [String:Any]
        let dist = detail["distance"] as? [String:Any]
        let time = detail["duration"] as? [String:Any]
        
        let distStr = dist!["text"] as! String
        let timeStr = time!["text"] as! String
        
        print("distance-->\(distStr)")
        print("time-->\(timeStr)")
        
        var finalTime = ""
        if timeStr.contains("hours") {
            var hrs = timeStr.components(separatedBy:" hours")
            print(hrs)
            var mins = [""]
            if hrs[1].contains("mins") {
                mins = hrs[1].components(separatedBy:" mins")
            }else {
                mins = hrs[1].components(separatedBy:" min")
            }
            print(mins)
            let hours = hrs[0]
            print(hours)
            var min = mins[0]
            print(min)
            let hrs1 = (hours.int ?? 0) * 60
            print(hrs1)
            min = min.replacingOccurrences(of: " ", with: "")
            print(min)
            print(min.int ?? 0)
            let tDurs = (min.int ?? 0) + hrs1
            print(tDurs)
            finalTime = "\(tDurs)"
        }else if timeStr.contains("hour"){
            var hrs = timeStr.components(separatedBy:" hour")
            print(hrs)
            var mins = [""]
            if hrs[1].contains("mins") {
                mins = hrs[1].components(separatedBy:" mins")
            }else {
                mins = hrs[1].components(separatedBy:" min")
            }
            print(mins)
            let hours = hrs[0]
            print(hours)
            var min = mins[0]
            print(min)
            let hrs1 = (hours.int ?? 0) * 60
            print(hrs1)
            min = min.replacingOccurrences(of: " ", with: "")
            print(min)
            print(min.int ?? 0)
            let tDurs = (min.int ?? 0) + hrs1
            print(tDurs)
            finalTime = "\(tDurs)"
        }
        else {
            finalTime = timeStr.replacingOccurrences(of: " mins", with: "")
        }
        let finalDist = distStr.replacingOccurrences(of: " km", with: "")
        print("journey duration in mins----\(finalTime)")
        
        guard self.checkNetworkStatus() else {return}
        showLoader()
        
        DispatchQueue.main.async {
            let startAddress = self.lbl_PickUpAddress.text ?? ""
            let endAddress = self.lbl_Destination.text ?? ""
            let params: Parameters = ["tripType":"2","start_lat":sCoor!["lat"]!, "start_lng":sCoor!["lng"]!,"end_lat":eCoor!["lat"]!, "end_lng":eCoor!["lng"]!, "distance":Double(finalDist) ?? 0.0, "duration":Double(finalTime) ?? 0.0, "start_address" :startAddress,"end_address": endAddress, "bookingDate":self.getUTCFormatDate(RiderManager.sharedInstance.reservationBookingDate ?? Date()), "localbookingDateTime":self.getUTCFormatDate(RiderManager.sharedInstance.reservationBookingDate ?? Date()), "timezoneString":self.timeZoneId ?? ""]
            
            RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/add_trip", withInputString: params, requestType: .post, isAuthorization: true, success: { (response) in
                print("response\(String(describing: response))")
                self.hideLoader()
                if response?["statusCode"] == 200 {
                    DispatchQueue.main.async {
                        let modal = ReservationRideModal.init(json: response?["data"] ?? ["":""])
                        self.reservationRide = modal
                        self.showReservedVehiclesWithPrice()
                    }
                }
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }
    }
    //MARK:: custom methods
    func showReservedVehiclesWithPrice() {
        self.pinIcon.isHidden = true
        Constraint_Bottom_PlanRideMap.constant = 340
        self.view_ReserveVehicles.frame = CGRect(x: 0, y: Utility.windowHeight()-415, width: Utility.windowWidth(), height: 350)
        self.view.addSubview(self.view_ReserveVehicles)
        self.view.bringSubview(toFront: self.view_ReserveVehicles)
        //let index =  vehciles_CollectionView.indexPathsForVisibleItems
        self.vehciles_CollectionView.reloadData()
        //self.vehciles_CollectionView.selectItem(at: index[0], animated: true, scrollPosition: .centeredHorizontally)
    }
    
    func showPinLocationView() {
        
        let sizeLeft = (pinIcon.frame.origin.y) + (pinIcon.frame.size.height)
        if  (Utility.windowHeight() - sizeLeft) < 332{
            Constraint_Bottom_PlanRideMap.constant = 240
            constraint_ViewPickAdd.constant = 60
            self.view_SelectLocation.frame = CGRect(x: 0, y: Utility.windowHeight()-310, width: Utility.windowWidth(), height: 250)
            self.view.addSubview(self.view_SelectLocation)
            self.view.bringSubview(toFront: self.view_SelectLocation)
        }else{
            Constraint_Bottom_PlanRideMap.constant = 255
            self.view_SelectLocation.frame = CGRect(x: 0, y: Utility.windowHeight()-329, width: Utility.windowWidth(), height: 265)
            self.view.addSubview(self.view_SelectLocation)
            self.view.bringSubview(toFront: self.view_SelectLocation)
        }
        self.view.layoutIfNeeded()
    }
    func fillReservationDetails() {
        self.lbl_DateTime.text = RiderManager.sharedInstance.resereBookDate ?? ""
        self.lbl_PickUpAddress.text = RiderManager.sharedInstance.pickUpAddress ?? ""
        self.lbl_Destination.text = RiderManager.sharedInstance.dropAddress ?? ""
    }
    //TODO::toggleArry
    func createLocalDictData() {
        print("vehicledCount-->\(RiderManager.sharedInstance.vehiclesData.count)")
        for _ in 0..<RiderManager.sharedInstance.vehiclesData.count {
            self.toggleArry.append("0")
        }
        print(self.toggleArry)
    }
    fileprivate func addPickLocationIcon() {
        let pickPosition = AppManager.sharedInstance.pickUpCoordinates
        let position = CLLocationCoordinate2D(latitude: pickPosition.latitude, longitude:pickPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.planRide_Map
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    
    fileprivate func addDropLocationIcon () {
        let dropPosition = AppManager.sharedInstance.dropCoordinates
        let position = CLLocationCoordinate2D(latitude: dropPosition.latitude, longitude:dropPosition.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.planRide_Map
        marker.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker.appearAnimation = GMSMarkerAnimation.pop
    }
    //MARK:: Draw root path
    fileprivate func getPathPoints(_ booking : Bool) {
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: AppManager.sharedInstance.pickUpCoordinates, destination: AppManager.sharedInstance.dropCoordinates) { (returnPath, distance) in
            self.hideLoader()
           
            self.showPath(polyStr: returnPath)

            if booking == true{
                 let arr = Utility.getTimeAndDistance(data: distance)
                DispatchQueue.main.async {
                    self.lbl_Title?.text = "\(arr[0]) Km  \(arr[1]) Min"
                }
                self.bookReservationRide(data: distance)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.planRide_Map
            let bounds = GMSCoordinateBounds(path: path)
            DispatchQueue.main.async {
                self.planRide_Map.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
            }
        }
    }
    
    func getUTCFormatDate(_ localDate: Date) -> String {
        let dateFormatter = DateFormatter()
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone = timeZone as TimeZone?
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateString: String = dateFormatter.string(from: localDate)
        return dateString
    }
    
    //MARK::IBOutlets methods
    @IBAction func action_Confirm(_ sender: UIButton) {
        guard !(self.lbl_PickUpAddress.text?.isEmpty)! && !(self.lbl_Destination.text?.isEmpty)! else {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message: LinkStrings.RiderAlerts.DropLocation, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void  in
            }, controller: self)
            
            return
        }
        showLoader()
        addPickLocationIcon()
        addDropLocationIcon()
        getPathPoints(true)
    }
    
    @IBAction func action_GetPriceOffer(_ sender: UIButton) {
        guard self.cardAdded ?? false else {
            self.showToastMsg()
            return
        }
        if userSelectedVehicle != ""{
            guard self.checkNetworkStatus() else {return}
            self.showLoader()
            var paymentId:String?
            if (RiderManager.sharedInstance.riderLoginData?.cards?.count ?? 0) > 0{
                if RiderManager.sharedInstance.riderLoginData?.cards?[0].defaultStatus ?? false {
                    paymentId = RiderManager.sharedInstance.riderLoginData?.cards?[0].id ?? "5a251b5aab354d6839d0bb89"
                }
            }
            //"cardId":"5a251b5aab354d6839d0bb89"
            RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/buy_price_offer", withInputString: ["tripId":self.reservationRide?.id ?? "","vehicleType":userSelectedVehicle,"cardId":paymentId ?? "5a251b5aab354d6839d0bb89"], requestType: .post, isAuthorization: true, success: { (response) in
                print("response***price\(String(describing: response))")
                self.hideLoader()
                if response?["statusCode"] == 200 {
                    self.performSegue(withIdentifier: "reservation_Notice", sender: nil)
                }else{
                    
                    AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: response?["message"].string ?? "", style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                        
                    })
                }
            }, failure: { (error) in
                self.hideLoader()
                print(error.debugDescription)
            })
        }else{
            
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: LinkStrings.RiderObserverKeys.PleaseSelectVehicle, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
        }
    }
    
    @IBAction func action_Payment(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "AddPaymentViewController") as? AddPaymentViewController{
            controller.fromTripReserve = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func showToastMsg() {
        self.lbl_Toast.isHidden = false
        self.lbl_Toast.text = LinkStrings.RiderBooking.CardNotAdded
        self.lbl_Toast.fadeIn(duration: 2.0, completion: { (complete) in
            self.lbl_Toast.fadeOut(duration: 1.0, completion: { (complete) in
            })
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ReservationPinDestinationViewController:UICollectionViewDelegate,UICollectionViewDataSource {
    
    //MARK:- UICollectionView DataSource Method(s)
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return RiderManager.sharedInstance.vehiclesData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell:ReservedVehiclesCell?
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReservedVehiclesCell.reuseIdentifier, for: indexPath) as? ReservedVehiclesCell
        cell?.configureCell(with: indexPath, RiderManager.sharedInstance.vehiclesData[indexPath.row])
        cell?.vehicle_ImgView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell?.lbl_SeatCount.textColor = UIColor.groupTableViewBackground
        cell?.lbl_SeatCount.text = "0-0" // by default value
        if self.toggleArry[indexPath.row] == "1"{
            cell?.lbl_SeatCount.text = "\(lowerPrice)-\(higherPice)"  // after changing value according to api response
            cell?.vehicle_ImgView.layer.borderColor = App_Base_Color.cgColor
            cell?.lbl_SeatCount.textColor = App_Base_Color
        }
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell:ReservedVehiclesCell = collectionView.cellForItem(at: indexPath) as! ReservedVehiclesCell
        //        cell.vehicle_ImgView.layer.borderColor = App_Base_Color.cgColor
        for i in 0..<toggleArry.count {
            self.toggleArry.remove(at: i)
            if indexPath.row == i {
                self.toggleArry.insert("1", at: i)
            }else{
                self.toggleArry.insert("0", at: i)
            }
        }
        let modal =  RiderManager.sharedInstance.vehiclesData[cell.lbl_VehicleName.tag]
        userSelectedVehicle = modal.name ?? ""
        getSpecialPrice(modal.name ?? "")
    }
}
//MARK::GMSMapViewDelegate
extension ReservationPinDestinationViewController:GMSMapViewDelegate {
    
    //called when the map is idle
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        //        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        //        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
        //            DispatchQueue.main.async {
        //                if RiderManager.sharedInstance.chooseDropLocResrve ?? false {
        //                    self.lbl_Destination.text = returnAddress
        //                }else {
        //                    self.lbl_PickUpAddress.text = returnAddress
        //                }
        //            }
        //        })
        //        AppManager.sharedInstance.pickUpCoordinates = position
    }
    
    func returnPostionOfMapView(mapView:GMSMapView){
        
        let latitute = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        let position = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)
        
        ReverseGeocode.getAddressByCoords(coords: position, returnAddress: { (returnAddress) in
            DispatchQueue.main.async {
                if RiderManager.sharedInstance.dropAddress == ""{
                    self.lbl_Destination.text = returnAddress
                    AppManager.sharedInstance.dropCoordinates = position
//                    if self.pinIcon.isHidden == false{
//                        self.getPathPoints(false)
//                    }
                    self.btn_ConfirmRide.isEnabled = true
                    self.btn_ConfirmRide.alpha = 1.0
                }
            }
        })
    }
}
//MARK::LocationDelegates
extension ReservationPinDestinationViewController:LocationDelegates {
    func authorizationStatus(status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.planRide_Map.isMyLocationEnabled = true
            self.planRide_Map.settings.myLocationButton = true
        }
    }
    
    func didUpdateLocation(locations: [CLLocation]) {
        if let location = locations.last {
            // address not pre fix zoom on current location
            self.planRide_Map.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            //            let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            //            let marker = GMSMarker(position: position)
            //            marker.title = "Witz"
            //            marker.map = self.planRide_Map
            //            marker.appearAnimation = GMSMarkerAnimation.pop
            //            marker.icon = UIImage(named: "vehicle-car_on-map")
            self.locUpdate.stopTracking()
            self.hideLoader()
        }
    }
    
    func didFailToUpdate(error: Error) {
    }
}

extension ReservationPinDestinationViewController:RiderConnectionManagerDelegate {
    
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        self.hideLoader()
        print("vehicle Data \(sender)")
        switch serviceType {
        case .GET_ALL_VEHICLES:
            let result = sender as! JSON
            if result["statusCode"] == 200 {
                self.vehciles_CollectionView.reloadData()
                self.createLocalDictData()
            }
            break
        default:
            break
        }
    }
    func didFailWithError(sender: AnyObject) {
        self.hideLoader()
        if sender is ServerError {
            let error = sender as! ServerError
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
            
        }else if sender is ServerResponse {
            let error = sender as! ServerResponse
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:error.response_message! , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }else {
            let errorMsg = sender as! String
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:errorMsg , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler, controller: self)
        }
    }
}
