//
//  ReservationSearchViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/19/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GooglePlaces
class ReservationSearchViewController: WitzSuperViewController {

    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnWork: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showAddressDetails()
    }
    
    func showAddressDetails() {
        self.btnHome.setTitle(RiderManager.sharedInstance.riderLoginData?.homeAddress ?? "Add home address", for: .normal)
        self.btnWork.setTitle(RiderManager.sharedInstance.riderLoginData?.workAddress ?? "Add work address", for: .normal)
        
        if RiderManager.sharedInstance.riderLoginData?.homeAddress == "" {
            self.btnHome.setTitle("Add home address", for: .normal)
        }
        if RiderManager.sharedInstance.riderLoginData?.workAddress == "" {
            self.btnWork.setTitle("Add work address", for: .normal)
        }
    }
    @IBAction func action_PinDest(_ sender: UIButton) {
        RiderManager.sharedInstance.dropAddress = ""
        RiderManager.sharedInstance.homeWorkAddressSelected = false
        self.performSegue(withIdentifier: "reserve_PinDestination", sender: nil)
    }
    
    @IBAction func action_SearchAdd(_ sender: UIButton) {
        RiderManager.sharedInstance.homeWorkAddressSelected = true
        openGooglePlacePrompt()
    }
    @IBAction func action_Home(_ sender: UIButton) {
        RiderManager.sharedInstance.homeWorkAddressSelected = true
        if sender.currentTitle == "Add home address" {
            self.performSegue(withIdentifier: "find_HomeAdd", sender: sender.tag)
        }else {
//            fetchAddressDetails2(address:sender.currentTitle ?? "")
            self.showLoader()
            self.fetchAddressDetails(arg: true, address: sender.currentTitle ?? "", completion: { (completion) in
                if completion {
                    self.performSegue(withIdentifier: "reserve_PinDestination", sender: nil)
                }
            })
        }
    }
    
    @IBAction func action_Work(_ sender: UIButton) {
        if sender.currentTitle == "Add work address" {
            self.performSegue(withIdentifier: "find_HomeAdd", sender: sender.tag)
        }else {
//            fetchAddressDetails2(address:sender.currentTitle ?? "")
            self.showLoader()
            self.fetchAddressDetails(arg: true, address: sender.currentTitle ?? "", completion: { (completion) in
                if completion {
                    self.performSegue(withIdentifier: "reserve_PinDestination", sender: nil)
                }
            })
        }
    }
    
    func fetchAddressDetails(arg: Bool, address:String, completion: @escaping (Bool) -> ()) {
        RiderManager.sharedInstance.dropAddress = address
        GeocodeHelper.getLocationFromAddress(from: address, returnData: { (returnLoc, info) in
            print(returnLoc,info)
            AppManager.sharedInstance.dropCoordinates = returnLoc
            self.hideLoader()
            completion(arg)
        })
    }
    
    func fetchAddressDetails2(address:String) {
        RiderManager.sharedInstance.dropAddress = address
        GeocodeHelper.getLocationFromAddress(from: address, returnData: { (returnLoc, info) in
            print(returnLoc,info)
            AppManager.sharedInstance.dropCoordinates = returnLoc
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "find_HomeAdd" {
            let tag = sender as? Int
            if tag == 1 {
                if let controller = segue.destination as?  SelectHomeWorkAddressViewController{
                    controller.addressType = "Add home address"
                }
            }else {
                if let controller = segue.destination as?  SelectHomeWorkAddressViewController{
                    controller.addressType = "Add work address"
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ReservationSearchViewController: GMSAutocompleteViewControllerDelegate {
    
    func openGooglePlacePrompt(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        //autocompleteController.tableCellBackgroundColor = UIColor.cyan
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red: 79.0/255, green: 89.0/255, blue: 89.0/255, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        
        //UISearchBar.appearance().barStyle = UIBarStyle.default
        UISearchBar.appearance().barTintColor = UIColor.white
        //UISearchBar.appearance().setTextColor = UIColor.white
        
        let search = UISearchBar()
        //        search.setNewcolor(color: UIColor.white)
        for subView: UIView in search.subviews {
            for secondLevelSubview: UIView in subView.subviews {
                if (secondLevelSubview is UITextField) {
                    let searchBarTextField = secondLevelSubview as? UITextField
                    //set font color here
                    searchBarTextField?.textColor = UIColor.white
                    break
                }
            }
        }
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("searched address***********\(place.formattedAddress ?? place.name)")
        
        self.showLoader()
        dismiss(animated: true) {
            self.getLatLongUsingAddress(address: place.formattedAddress ?? place.name)
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //**************** called in case of search address using Google Autocomplete ****************//
    func getLatLongUsingAddress(address:String) {
        GeocodeHelper.getLocationFromAddress(from: address) { (returnLoc, info) in
            AppManager.sharedInstance.dropCoordinates = returnLoc
            RiderManager.sharedInstance.dropAddress = address
            self.hideLoader()
            self.performSegue(withIdentifier: "reserve_PinDestination", sender: nil)
        }
    }
}
