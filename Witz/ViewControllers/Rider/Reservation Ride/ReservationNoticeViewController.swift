//
//  ReservationNoticeViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 12/14/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class ReservationNoticeViewController: WitzSuperViewController {

    var backBtn = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 10, width:Utility.windowWidth(), height: Utility.windowHeight()-120)

        self.navigationItem.titleView = nil
        self.navigationItem.title = "RESERVATION NOTICE"
//        self.left_barButton.addTarget(self, action: #selector(ReservationNoticeViewController.backToDashboard), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(backToDashboard))
        self.view.addGestureRecognizer(tap)
        self.left_barButton.isHidden = true
        self.backBtn = UIButton(type: .custom)
        self.backBtn.setImage(UIImage(named: "Round_back"), for: .normal)
        self.backBtn.addTarget(self, action: #selector(ReservationNoticeViewController.backToDashboard), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: self.backBtn)
        self.navigationItem.leftBarButtonItem = item1
    }
    
    func backToDashboard() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is TransferDashboardViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
