//
//  RiderFutureJourneyViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/1/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher
import Cosmos
class RiderFutureJourneyViewController: WitzSuperViewController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var lbl_Fare: UILabel!
    @IBOutlet weak var lbl_PickAddress: UILabel!
    @IBOutlet weak var lbl_DropAddress: UILabel!
    
    @IBOutlet weak var img_Pic: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_VehicleName: UILabel!
    @IBOutlet weak var lbl_VehicleNumber: UILabel!
    @IBOutlet weak var view_StarRating: CosmosView!
    
    var tripDetail : TripsRider?
    var tripType : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mapView.settings.setAllGesturesEnabled(false)
        mapView.delegate = self
        
        if tripDetail != nil{
            showTripDetails()
            getTripDetail()
        }
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.FutureJourney
    }
    
    func showTripDetails(){
        
        lbl_PickAddress.text = tripDetail?.startAddress ?? ""
        lbl_DropAddress.text = tripDetail?.endAddress ?? ""
        self.lbl_Fare.text = "\(tripDetail?.price?.twoDecimalString ?? "") ₺"
        lbl_DateTime.text = tripDetail?.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.startLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.startLocation?.coordinates?[0] ?? 0))
        let coordinates1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(tripDetail?.endLocation?.coordinates?[1] ?? 0), longitude:CLLocationDegrees(tripDetail?.endLocation?.coordinates?[0] ?? 0))
        
        //Marker for pick up
        let marker1 = GMSMarker(position: coordinates)
        marker1.title = "Pick UP Location"
        marker1.appearAnimation = GMSMarkerAnimation.pop
        marker1.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_starting_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker1.map = mapView
        
        //Marker for Drop
        let marker2 = GMSMarker(position: coordinates1)
        marker2.title = "Drop Location"
        marker2.appearAnimation = GMSMarkerAnimation.pop
        marker2.icon = Utility.imageWithImage(image: #imageLiteral(resourceName: "on_map_arrival_location"), scaledToSize: CGSize(width: 30.0, height: 30.0))
        marker2.map = mapView
        
        showLoader()
        self.showPolyLine(source: coordinates, destination: coordinates1)
    }
    fileprivate func showPolyLine(source:CLLocationCoordinate2D,destination:CLLocationCoordinate2D){
        hitsLimit_Google = 5
        GooglePathHelper.getPathPolyPoints(from: source, destination: destination) { (returnPath, distance) in
            DispatchQueue.main.async {
                self.showPath(polyStr: returnPath)
            }
        }
    }
    
    fileprivate func showPath(polyStr :String){
        if  let path = GMSPath(fromEncodedPath: polyStr){
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
            polyline.strokeColor = App_Base_Color
            polyline.map = self.mapView
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20.0))
        }
        hideLoader()
    }
    
    //MARK:: API call
    func getTripDetail() {
        guard self.checkNetworkStatus() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_trip_details", withInputString: ["tripId":tripDetail?.id ?? ""], requestType: .put, isAuthorization: true, success: { (response) in
            print("response\(String(describing: response))")
            self.hideLoader()
            if response?["statusCode"] == 200 {
                DispatchQueue.main.async {
                    if let detail =  response?["data"].dictionary{
                        print(detail)
            self.view_StarRating.rating =  Double(detail["driverData"]?["avgRating"].int ?? 0)
                        self.lbl_VehicleName.text = "\(detail["driverData"]?["vehicle_information"]["model"] ?? "")"
                        self.lbl_VehicleNumber.text = "\(detail["driverData"]?["vehicle_information"]["vehicle_number"] ?? "")"
                        self.lbl_Name.text = "\(detail["driverData"]?["personal_information"]["fullName"] ?? "")".nameFormatted
                        let imgUrl = "\(kBaseImageURL)\(detail["driverData"]?["pic"]["original"] ?? "")"
                        let url = imgUrl.url
//                        self.img_Pic.image = nil
                        self.img_Pic.setNeedsDisplay()
                        self.downloadImage(url!)
                        
                    }
                }
            }
            
        }, failure: { (error) in
            print(error.debugDescription)
            self.hideLoader()
        })
    }
    func downloadImage(_ url:URL) {
        KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            DispatchQueue.main.async {
                if let thumbImage = image {
                    self.img_Pic.image = thumbImage
                }
            }
        })
    }
    func cancelReservation() {
        AlertHelper.displayAlertView(title: LinkStrings.AlertTitle.Title, message:LinkStrings.RiderAlerts.RemoveFutureReservation, style: .alert, actionButtonTitle: [LinkStrings.MostCommon.Yes, LinkStrings.MostCommon.No], completionHandler: { (action:UIAlertAction!) -> Void in
            if action.title==LinkStrings.MostCommon.Yes {
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
                RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/cancel_ride", withInputString: ["tripId":self.tripDetail?.id ?? ""], requestType: .post, isAuthorization: true, success: { (response) in
                    print(response ?? "" )
                    self.hideLoader()
                    if response?["statusCode"] == 200 {
                        self.jumpToTripsMenuScreen()
                    }
                    AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: response?["message"].string ?? ""  , style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                        
                    })
                }) { (error ) in
                    self.hideLoader()
                }
            }
        })
    }
    
    func jumpToTripsMenuScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is TripsViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    
    lazy var storyBoard : UIStoryboard = UIStoryboard.init(name: LinkStrings.StoryboardName.Main, bundle: nil)
    @IBAction func action_Call(_ sender: UIButton) {
        let controller = storyBoard.instantiateViewController(withIdentifier: "CallingTwillioViewController") as! CallingTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    @IBAction func action_Msg(_ sender: UIButton) {
        let controller = storyBoard.instantiateViewController(withIdentifier: "MessageTwillioViewController") as! MessageTwillioViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func action_CancelReservation(_ sender: UIButton) {
        self.cancelReservation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// MARK: GMSMapViewDelegate
extension RiderFutureJourneyViewController : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        if let controller = storyBoard.instantiateViewController(withIdentifier: "DisplayRouteViewController") as? DisplayRouteViewController{
            controller.tripDetail = tripDetail
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
