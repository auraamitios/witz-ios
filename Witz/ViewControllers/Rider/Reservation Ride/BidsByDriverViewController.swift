//
//  BidsByDriverViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 2/9/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
class BidsByDriverViewController: WitzSuperViewController {

    @IBOutlet weak var tbl_Bids: UITableView!
    var tripID:String?
    var arrDriverList = [DriverDataRider]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bgLogoImageView.frame = CGRect(x: 0, y: 100, width:Utility.windowWidth(), height: Utility.windowHeight()-120)
        self.tbl_Bids.tableFooterView = UIView()
        if tripID != nil {showReviewData()}
    }
    
    func showReviewData(){
        guard self.checkNetworkStatus() else {return}
        showLoader()
        print(tripID ?? "")
        let parameters = ["tripId":tripID ?? ""]
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/get_price_offer_from_drivers", withInputString: parameters, requestType: .post, isAuthorization: true, success: { (response) in
            self.hideLoader()
            print(response ?? "")
            if let result = response?["data"]{
                let modal = BidFromDriverModal.init(json: result)
                self.arrDriverList = modal.driverData ?? []
                self.tbl_Bids.reloadData()
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BidsByDriverViewController:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: BidHeaderCell.identifier, for: indexPath) as! BidHeaderCell
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: BidInfoCell.identifier, for: indexPath) as! BidInfoCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
