//
//  BidApprovalOfferViewController.swift
//  Witz
//
//  Created by Amit Tripathi on 1/23/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
class BidApprovalOfferViewController: WitzSuperViewController {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_BidPrice: UILabel!
    @IBOutlet weak var img_DriverPic: UIImageView!
    @IBOutlet weak var lbl_Rating: UILabel!
    @IBOutlet weak var lbl_CarName: UILabel!
    @IBOutlet weak var lbl_CarNumber: UILabel!
    
    var tripID : String?
    var driverInfo:DriverDataRider?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.titleView = nil
        self.navigationItem.title = LinkStrings.TitleForView.ApprovalOffer
        if driverInfo != nil {
            showDriverDetails()
        }
    }

    @IBAction func action_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func action_Accept(_ sender: UIButton) {
        acceptBidOffer()
    }
    
    fileprivate  func showDriverDetails(){

        self.lbl_Name.text = driverInfo?.personalInformation?.fullName?.nameFormatted ?? ""
        self.lbl_CarName.text = "\(driverInfo?.vehicleInformation?.model ?? "") - \(driverInfo?.vehicleInformation?.color ?? "")"
        self.lbl_CarNumber.text = driverInfo?.vehicleInformation?.vehicleNumber ?? ""
        self.lbl_Rating.text = "\(String(format: "%.0f", driverInfo?.avgRating ?? 0.0))"
        self.lbl_BidPrice.text = "\(driverInfo?.offerPrice ?? 0.0 ) TL"
        let imgUrl = "\(kBaseImageURL)\(driverInfo?.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    if let thumbImage = image {
                        self.img_DriverPic.image = thumbImage
                    }
                    self.img_DriverPic.setNeedsDisplay()
                }
            })
        }
    }
    
    func acceptBidOffer() {
        guard self.checkNetworkStatus() else {return}
        self.showLoader()
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/accept_offer", withInputString: ["tripId":tripID ?? "","driverId":driverInfo?.id ?? "", "offerPrice":"\(driverInfo?.offerPrice ?? 0.0 )"], requestType: .post, isAuthorization: true, success: { (response) in
            print("response\(String(describing: response))")
            DispatchQueue.main.async {
                self.hideLoader()
                if response?["statusCode"] == 200 {
//                    self.jumpToTripsMenuScreen()
                    self.performSegue(withIdentifier: "segue_BidSuccess", sender: nil)
                }
            }
        }, failure: { (error) in
            self.hideLoader()
        })
    }
    
    func jumpToTripsMenuScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for aViewController in viewControllers {
                if aViewController is TripsViewController {
                    self.navigationController!.popToViewController(aViewController, animated: false)
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
