//
//  MessageTwillioViewController.swift
//  Witz
//
//  Created by abhishek kumar on 30/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MessageTwillioViewController: UIViewController {
    
    @IBOutlet weak var txt_Message: KMPlaceholderTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeView))
        self.view.addGestureRecognizer(tap)
        
        txt_Message.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    
    func closeView()  {
        txt_Message.resignFirstResponder()
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        
        guard validation() else {
            return
        }
        
        if AppManager.sharedInstance.loggedUserRider {
             guard let driverMobile = RiderManager.sharedInstance.bookedDriverProfile?.mobileNumber else { return }
            showLoader()
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "rider/send_sms", withInputString: ["message":txt_Message.text,"mobile":driverMobile], requestType: .post, isAuthorization: true, success: { (response) in
                self.hideLoader()
                
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    if response?["statusCode"] == 200{
                        self.dismiss(animated: true, completion: nil)
                    }
                }, controller: self)
                
            }, failure: { (error) in
                self.hideLoader()
            })
            
        }else {
            var riderMobile = ""
            if DriverManager.sharedInstance.isAggregateTrip == true{
                guard let phoneN = DriverManager.sharedInstance.ride_DetailsAggreagate?.riderMobile else {return}
                riderMobile = phoneN
            }else{
                guard let phoneN = DriverManager.sharedInstance.ride_Details?.riderData?.mobile else {return}
                riderMobile = phoneN
            }
//            guard let riderMobile = DriverManager.sharedInstance.ride_Details?.riderData?.mobile else { return }
            showLoader()
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/send_sms", withInputString: ["message":txt_Message.text,"mobile":riderMobile], requestType: .post, isAuthorization: true, success: { (response) in
                self.hideLoader()
                
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    if response?["statusCode"] == 200{
                        self.closeView()
                    }
                }, controller: self)
                
            }, failure: { (error) in
                self.hideLoader()
            })
        }
        
    }
    
    func validation() -> Bool {
        
        if  txt_Message.text?.isBlank() == true {
            AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:LinkStrings.ValidationString.EmptyMessage , style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                
            }, controller: self)
            return false
        }
        
        return true
    }
    
}
