//
//  CallingTwillioViewController.swift
//  Witz
//
//  Created by abhishek kumar on 30/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CallingTwillioViewController: UIViewController {
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_WhomToCall: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if AppManager.sharedInstance.loggedUserRider {
            lbl_Title.text = "Call to Driver"
            lbl_WhomToCall.text = "Please receive the call for connecting with your driver"
        }
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeView))
        self.view.addGestureRecognizer(tap)
    }
    
    func closeView()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func callUser(_ sender: UIButton) {
        
        guard self.checkNetworkStatus() else {return}
        if AppManager.sharedInstance.loggedUserRider {
            guard let driverMobile = RiderManager.sharedInstance.bookedDriverProfile?.mobileNumber else {return}
            guard let mobile = RiderManager.sharedInstance.riderInfo?.mobile else {return}
            showLoader()
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/create_call_without_auth?to=\(driverMobile)&from=\(twillioNumber)&dest=\(mobile)", withInputString: ["" : "" ], requestType: .get, isAuthorization: false, success: { (response) in
                self.hideLoader()

                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    if response?["statusCode"] == 200{
                        self.dismiss(animated: true, completion: nil)
                    }
                }, controller: self)

            }, failure: { (error) in
                self.hideLoader()
            })
        }else {
            guard let mobile = DriverManager.sharedInstance.user_Driver.mobile else {return}
            var riderMobile = ""
            if DriverManager.sharedInstance.isAggregateTrip == true{
                guard let phoneN = DriverManager.sharedInstance.ride_DetailsAggreagate?.riderMobile else {return}
                riderMobile = phoneN
            }else{
            guard let phoneN = DriverManager.sharedInstance.ride_Details?.riderData?.mobile else {return}
            riderMobile = phoneN
            }
            showLoader()
            WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/create_call_without_auth?to=\(riderMobile)&from=\(twillioNumber)&dest=\(mobile)", withInputString: ["" : "" ], requestType: .get, isAuthorization: false, success: { (response) in
                self.hideLoader()
                
                AlertHelper.displayOkAlert(title: LinkStrings.AlertTitle.Title, message:(response?["message"].stringValue ?? "")!, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: { (action:UIAlertAction!) -> Void in
                    if response?["statusCode"] == 200{
                        self.dismiss(animated: true, completion: nil)
                    }
                }, controller: self)
                
            }, failure: { (error) in
                self.hideLoader()
            })
        }
    }
}
