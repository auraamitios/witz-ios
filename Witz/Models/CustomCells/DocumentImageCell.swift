//
//  DocumentImageCell.swift
//  Witz
//
//  Created by abhishek kumar on 27/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

protocol DoumentImageCellDelegate : NSObjectProtocol{
    func deleteImageCalled(_ sender:UIButton)
}

class DocumentImageCell: UICollectionViewCell {
    
    weak var delegate : DoumentImageCellDelegate!
    
    @IBOutlet weak var imgView_Document: UIImageView!
    
    @IBOutlet weak var btn_Cancel: UIButton!
    
    
    func setupCell(with indexPath:IndexPath,_ image: UIImage ){
        
        self.imgView_Document.image = image
        self.btn_Cancel.tag = indexPath.row
        self.btn_Cancel.addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
    }
    
    func deleteImage(_ sender: UIButton ){
        delegate.deleteImageCalled(sender)
    }
}
