//
//  CellViewAllPassenger.swift
//  Witz
//
//  Created by abhishek kumar on 16/04/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class CellViewAllPassenger: UITableViewCell {
    
    @IBOutlet weak var lbl_Passenger: UILabel!
    @IBOutlet weak var lbl_StartLoc: UILabel!
    @IBOutlet weak var lbl_EndLoc: UILabel!
    @IBOutlet weak var btn_PickDrop: UIButton!
    @IBOutlet weak var btn_Call: UIButton!
    @IBOutlet weak var btn_Msg: UIButton!
    @IBOutlet weak var lbl_Seprator: UILabel!

    @IBOutlet weak var constraint_BtnPick_Drop: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(with data: [String:Any] ,index:IndexPath, shift : ShiftTime? , hideButton : Bool){
        
        hideRideOption(status: false)


        lbl_Passenger.text = (data[KeysForSharingPickUpViewController.NAME] as? String )?.nameFormatted
        if let startCoord = data[KeysForSharingPickUpViewController.COORDINATE] as? CLLocation{
            getAddress(using: startCoord.coordinate, with: lbl_StartLoc)

        }
        if let endCoord = data[KeysForSharingPickUpViewController.ENDCOORDINATE] as? CLLocation{
            getAddress(using: endCoord.coordinate, with: lbl_EndLoc)
        }
        btn_PickDrop.backgroundColor = UIColor.init(red: 0.1176, green: 0.5647, blue: 0.1843, alpha: 1.0)
        btn_PickDrop.setTitle("Pick", for: .normal)
        btn_PickDrop.tag = index.row
        btn_PickDrop.isSelected = false
        
        if KeysForSharingPickUpViewController.PickUPStatus.PICKED == data[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as? String ?? "" {
            
            btn_PickDrop.setTitle("Drop", for: .normal)
            btn_PickDrop.backgroundColor = .red
            btn_PickDrop.isSelected = true
            
        }else  if KeysForSharingPickUpViewController.PickUPStatus.DROPPED == data[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as? String ?? "" {
            
            btn_PickDrop.isHidden = true
            constraint_BtnPick_Drop.constant = 0
        }
        
        if shift != nil {
            if shift == ShiftTime.Evening{
                if let startCoord = data[KeysForSharingPickUpViewController.COORDINATE] as? CLLocation{
                    getAddress(using: startCoord.coordinate, with: lbl_EndLoc)
                }
                if let endCoord = data[KeysForSharingPickUpViewController.ENDCOORDINATE] as? CLLocation{
                    getAddress(using: endCoord.coordinate, with: lbl_StartLoc)
                }
            }
        }
        
        if hideButton == true{
           hideRideOption(status: true)
        }
    }
   
    /**
     Show or hide elements according to ride
     */
    
    func hideRideOption(status : Bool){
        btn_PickDrop.isHidden = status
        if status == true{
           constraint_BtnPick_Drop.constant = 0
        }else{
        constraint_BtnPick_Drop.constant = 100
        }
        btn_Call.isHidden = status
        btn_Msg.isHidden = status
        lbl_Seprator.isHidden = status
    }
    
    fileprivate func getAddress(using coords: CLLocationCoordinate2D , with label:UILabel){
        ReverseGeocode.getAddressByCoords(coords: coords) { (address) in
            label.text = address
        }
    }
}
