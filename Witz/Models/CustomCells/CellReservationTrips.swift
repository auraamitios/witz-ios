//
//  CellReservationTrips.swift
//  Witz
//
//  Created by abhishek kumar on 01/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CellReservationTrips: UITableViewCell {
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_Money: UILabel!
    @IBOutlet weak var lbl_BidCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:- *********** driver configure cell method ***************//
    func configureCell(with data: TripsInfo ,index:IndexPath ,isPending:Bool){
        
         lbl_Money.text = "\(data.driverEarning?.twoDecimalString ?? "") ₺"
        
        /*********  Instant ride trips**************/
        if data.tripType == "1"{
            
            if data.status == 21 {                                                // cancel by rider
                lbl_date.text =   "Cancel by Rider \n\( data.createdAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                
                lbl_Money.textColor = .red
            }else if data.status == 20 {                                            // cancel by Driver
                lbl_date.text = "Cancel by Driver \n\( data.createdAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
               
                lbl_Money.textColor = .red
            }else{                                                                   // Completed ride
                lbl_date.text = data.createdAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
               
                lbl_Money.textColor = UIColor.init(hex: "DEB850")
            }
            
        }
         /*********  Reservation trips**************/
        if data.tripType == "2"{
            
            if data.status == 21 {                                                // cancel by rider
                lbl_date.text =   "Cancel by Rider \n\( data.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                
                lbl_Money.textColor = .red
            }else if data.status == 20 {                                            // cancel by Driver
                lbl_date.text = "Cancel by Driver \n\( data.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                
                lbl_Money.textColor = .red
            }else{                                                                   // Completed ride
                lbl_date.text = data.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
              
                lbl_Money.textColor = UIColor.init(hex: "DEB850")
            }
            
            if isPending == true{
                if let arrBids = data.bidByDriver{
                    for data in arrBids{
                        if data.driveId == DriverManager.sharedInstance.user_Driver.driver_Id{
                            lbl_Money.text = "\(data.offerPriceByDriver?.twoDecimalString ?? "") ₺"
                        }
                    }
                }
            }
        }
    }
    
    //MARK:-*********** driver configure cell method for aggregate ***************//
    func configureAggregateCell(with data: AggregateModal2 ,index:IndexPath ,isPending:Bool){
        
        lbl_Money.text = "\(data.driverEarning?.twoDecimalString ?? "") ₺"

        /*********  Aggregate trips **************/
        
            if data.status == 21 {                                                // cancel by rider
                lbl_date.text =   "Cancel by Rider \n\( data.aggregateTripTime?.convertDateFormatter(formatNeeded: "d-MMM-yyyy", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                
                lbl_Money.textColor = .red
            }else if data.status == 20 {                                            // cancel by Driver
                lbl_date.text = "Cancel by Driver \n\( data.aggregateTripTime?.convertDateFormatter(formatNeeded: "d-MMM-yyyy", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                
                lbl_Money.textColor = .red
            }else{                                                                   // Completed ride
                lbl_date.text = data.aggregateTripTime?.convertDateFormatter(formatNeeded: "d-MMM-yyyy", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                
                lbl_Money.textColor = UIColor.init(hex: "DEB850")
                
                if data.aggregateTripType != nil {
                    var tripType = "Home-Work"
                    if data.aggregateTripType == ShiftTime.Evening.rawValue{
                        tripType = "Work-Home"
                    }
                   lbl_Money.text = "\(tripType) \n \(data.driverEarning?.twoDecimalString ?? "") ₺"
                }
            }
    }
    
    //MARK:- *********** driver configure cell method for aggregate pending ***************//
    func configureCell_PendingAggregate(with data: PendingAggregateModal){
        
        lbl_date.text = LinkStrings.DriverPopUpMessage.NewAggregate
        lbl_Money.text = "\(data.notificationUpdatedDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy/HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
    }
    
    
    //MARK:- *********** rider configure cell method ***************//
    func configureCellRider(with data: TripsRider ,index:IndexPath, tripType:[String],showBid:Bool){
        
        lbl_Money.text = "\(data.price?.twoDecimalString ?? "") ₺"
        
        /*********  Instant ride trips**************/
        if data.tripType == "1"{
            self.lbl_BidCount.isHidden = true
            if data.status == 21 {                                                // cancel by rider
                lbl_date.text = "\( data.createdAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                lbl_date.textColor = .red
                lbl_Money.textColor = .red
            }else if data.status == 12 {                                            // cancel by Driver
                lbl_date.text = "\( data.createdAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                lbl_date.textColor = .red
                lbl_Money.textColor = .red
                 lbl_Money.text = "Cancel"
            }
            else if data.status == 20 {                                            // cancel by Driver
                lbl_date.text = "\( data.createdAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                lbl_date.textColor = .red
                lbl_Money.textColor = .red
                lbl_Money.text = "Cancel"
            }else{                                                                   // Completed ride
                lbl_date.text = data.createdAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                lbl_date.textColor = UIColor.darkGray
                lbl_Money.textColor = UIColor.init(hex: "DEB850")
            }
            
        }
        /*********  Reservation trips**************/
        if data.tripType == "2"{
            
            if tripType[0] == LinkStrings.TripsType.PendingJourney && tripType[1] == LinkStrings.TripsType.Reservation {
                lbl_BidCount.isHidden = !showBid
                self.lbl_Money.isHidden = true
                print("bidCount-->\(data.bidByDriver?.count ?? 0)")
                self.lbl_BidCount.text = "\(data.bidByDriver?.count ?? 0)"
            }
            if data.status == 21 {                                                // cancel by rider
                lbl_date.text =   "\( data.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                lbl_Money.textColor = .red
            }else if data.status == 20 {                                            // cancel by Driver
                lbl_date.text = "\( data.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                lbl_date.textColor = .red
                lbl_Money.textColor = .red
                lbl_Money.text = "Cancel"

            }else if data.status == 12 {                                            // cancel by Driver
                lbl_date.text = "\( data.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                lbl_date.textColor = .red
                lbl_Money.textColor = .red
                lbl_Money.text = "Cancel"

            }
            else{                                                                   // Completed ride
                lbl_date.text = data.bookingDate?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                lbl_date.textColor = UIColor.darkGray
                lbl_Money.textColor = UIColor.init(hex: "DEB850")
            }
        }
        /*********  Aggregate trips **************/
        if data.tripType == "3"{
            self.lbl_BidCount.isHidden = false
            if data.status == 21 {                                                // cancel by rider
                lbl_date.text =   "Cancel by Rider \n\( data.updatedAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                
                lbl_date.textColor = .red
                lbl_Money.textColor = .red
            }else if data.status == 12 {                                            // cancel by Driver
                lbl_date.text = "Cancel by Driver \n\( data.updatedAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                lbl_date.textColor = .red
                lbl_Money.textColor = .red

            }
            else if data.status == 20 {                                            // cancel by Driver
                lbl_date.text = "Cancel by Driver \n\( data.updatedAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
                lbl_date.textColor = .red
                lbl_Money.textColor = .red

            }else{                                                                   // Completed ride
                lbl_date.text = data.updatedAt?.convertDateFormatter(formatNeeded: "d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                lbl_date.textColor = UIColor.darkGray
                lbl_Money.textColor = UIColor.init(hex: "DEB850")
            }
        }
    }
}
