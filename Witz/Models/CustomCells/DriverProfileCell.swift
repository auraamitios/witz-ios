//
//  DriverProfileCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/4/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class DriverProfileCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var txtValue: UITextField!
    
    class var identifier: String
    {
        return String.className(self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    open class func height() -> CGFloat {
        return 70.0
    }
    
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    public func updateCell(with indexpath:IndexPath,_ title:String, _ iconStr:String ,valueStr:String) {
        lblTitle.text = title
        txtValue.tag = indexpath.section
        txtValue.placeholder = title
        imgIcon.image = UIImage(named:iconStr)
        if valueStr != ""{
           txtValue.text = valueStr
        }
    
    }
}
