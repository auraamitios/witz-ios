//
//  CellTasksList.swift
//  Witz
//
//  Created by abhishek kumar on 27/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CellTasksList: UITableViewCell {
    
    @IBOutlet weak var lbl_PersonCount: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Location: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_TaskNumber: UILabel!
    
    
    //Completed Tasks List
    
    @IBOutlet weak var lbl_Status_CompletedTask: UILabel!
    @IBOutlet weak var lbl_Hour_CompletedTask: UILabel!
    @IBOutlet weak var lbl_Location_CompletedTask: UILabel!
    @IBOutlet weak var lbl_Name_CompletedTask: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(with data: [String:Any] ,index:IndexPath){
        
        self.lbl_TaskNumber.text = "\(data[KeysForSharingPickUpViewController.PICKUPNUMBER] as? Int ?? 0)"
        self.lbl_Name.text = (data[KeysForSharingPickUpViewController.NAME] as? String )?.nameFormatted
        self.lbl_Location.text = data[KeysForSharingPickUpViewController.ADDRESS] as? String
        self.lbl_Status.text = data[KeysForSharingPickUpViewController.PickUPStatus.STATUS] as? String
        self.lbl_TaskNumber.backgroundColor = UIColor.init(red: 0.1176, green: 0.5647, blue: 0.1843, alpha: 1.0)
        if KeysForSharingPickUpViewController.DROP == data[KeysForSharingPickUpViewController.TYPE] as? String ?? "" {
            self.lbl_TaskNumber.backgroundColor = .black
        }
        
    }
    
    func configureCellCompletedTask(with data: [String:Any] ,index:IndexPath){
        self.lbl_Status_CompletedTask.text = "\(data[KeysForSharingPickUpViewController.PICKUPNUMBER] as? Int ?? 0)"
        self.lbl_Name_CompletedTask.text = (data[KeysForSharingPickUpViewController.NAME] as? String)?.nameFormatted
        self.lbl_Location_CompletedTask.text = data[KeysForSharingPickUpViewController.ADDRESS] as? String
        self.lbl_Status_CompletedTask.backgroundColor = UIColor.init(red: 0.1176, green: 0.5647, blue: 0.1843, alpha: 1.0)
        if KeysForSharingPickUpViewController.DROP == data[KeysForSharingPickUpViewController.TYPE] as? String ?? "" {
            self.lbl_Status_CompletedTask.backgroundColor = .black
        }
    }
}
