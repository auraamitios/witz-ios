//
//  LeftMenuCell.swift
//  Witz
//
//  Created by Amit Tripathi on 9/14/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell {
    
    class var identifier: String
    {return String.className(self)}

    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuName: UILabel!
    
    @IBOutlet weak var lbl_NotificationCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    open class func height() -> CGFloat {return 40.0 }
    
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }

    public func updateCell(with indexpath:IndexPath,_ item:String, _ icon:UIImage,showNotification:Bool) {
        menuIcon.image = icon
        menuName.text = item
        
        lbl_NotificationCount.isHidden = !showNotification
        
        if showNotification {
            
            if item == LinkStrings.DriverSideMenuItem.NOTIFICATION{
            if let count =   DriverManager.sharedInstance.user_Driver.notificationCount{
                lbl_NotificationCount.text = "\(count)"
                if count > 0{
                      lbl_NotificationCount.isHidden = false
                }else{
                     lbl_NotificationCount.isHidden = true
                }
            }
            }
            if item == LinkStrings.DriverSideMenuItem.MYTRIPS{
                 let count =   (DriverManager.sharedInstance.user_Driver.reservationFutureTripCount ?? 0)+(DriverManager.sharedInstance.user_Driver.reservationPendingTripCount ?? 0)+(DriverManager.sharedInstance.user_Driver.serviceRequestPendingTripCount ?? 0)+(DriverManager.sharedInstance.user_Driver.serviceRequestFutureTripCount ?? 0)
                    lbl_NotificationCount.text = "\(count)"
                    if count > 0{
                        lbl_NotificationCount.isHidden = false
                    }else{
                        lbl_NotificationCount.isHidden = true
                    }
                
            }
        }
    }
}
