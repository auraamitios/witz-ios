//
//  CellFeedbackMultipleRider.swift
//  Witz
//
//  Created by abhishek kumar on 27/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher

protocol feedBackStarCount{
    func feedBackStars(star:Int , forCell:Int)
    
}

class CellFeedbackMultipleRider: UITableViewCell {
    
    var delegate : feedBackStarCount?
    
    @IBOutlet weak var imgView_ProfilePic: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var stackStars: UIStackView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func configureCell(with data: TripModalDriver ,index:IndexPath){
        let tagg = index.row
        let rider =  data.riderData
        lbl_Name.text = rider?.fullName?.nameFormatted
        
        let imgUrl = "\(kBaseImageURL)\(rider?.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.imgView_ProfilePic.image = thumbImage
                    }
                    self.imgView_ProfilePic.setNeedsDisplay()
                }
            })
        }
        
        
        btn1.tag = tagg
        btn2.tag = tagg
        btn3.tag = tagg
        btn4.tag = tagg
        btn5.tag = tagg
    }
    
    func configureCellAggregate(with data: AggregateDriverStartTripModal ,index:IndexPath){
        let tagg = index.row
//        let rider =  data.
        lbl_Name.text = data.riderName?.nameFormatted
        
//        let imgUrl = "\(kBaseImageURL)\(rider?.pic?.original ?? "")"
//        if let url = imgUrl.url{
//            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
//                DispatchQueue.main.async {
//
//                    if let thumbImage = image {
//                        self.imgView_ProfilePic.image = thumbImage
//                    }
//                    self.imgView_ProfilePic.setNeedsDisplay()
//                }
//            })
//        }
        
        
        btn1.tag = tagg
        btn2.tag = tagg
        btn3.tag = tagg
        btn4.tag = tagg
        btn5.tag = tagg
    }
    
    @IBAction func rating1(_ sender: UIButton) {
        
        btn1.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn2.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        btn3.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        btn4.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        btn5.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        self.delegate?.feedBackStars(star: 1, forCell: sender.tag)
    }
    
    @IBAction func rating2(_ sender: UIButton) {
        
        btn1.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn2.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn3.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        btn4.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        btn5.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        self.delegate?.feedBackStars(star: 2, forCell: sender.tag)
    }
    
    @IBAction func rating3(_ sender: UIButton) {
        btn1.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn2.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn3.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn4.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        btn5.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
       
        self.delegate?.feedBackStars(star: 3, forCell: sender.tag)
    }
    
    @IBAction func rating4(_ sender: UIButton) {
        btn1.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn2.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn3.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn4.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn5.setImage(#imageLiteral(resourceName: "star_empty"), for: .normal)
        self.delegate?.feedBackStars(star: 4, forCell: sender.tag)
    }
    
    @IBAction func rating5(_ sender: UIButton) {
        btn1.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn2.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn3.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn4.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        btn5.setImage(#imageLiteral(resourceName: "menu_page_my_points"), for: .normal)
        self.delegate?.feedBackStars(star: 5, forCell: sender.tag)
    }
    
}
