//
//  DriverDocumentCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/13/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class DriverDocumentCell: UITableViewCell {
    
    @IBOutlet weak var lblDocName: UILabel!
    @IBOutlet weak var imgPending: UIImageView!
    @IBOutlet weak var imgDocDone: UIImageView!
    @IBOutlet weak var lbl_DocStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }
    
    class var identifier: String {return String.className(self)}
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    public func updateCell(with indexpath:IndexPath,_ title:String, _ docUploadStatus:Bool , _ docVerifiedStatus:Bool, _ comment:String) {
        lblDocName.text = title
        lbl_DocStatus.text = comment
        if docVerifiedStatus == true{
            imgDocDone.isHidden = false
            imgPending.isHidden = true
            lbl_DocStatus.textColor = .green
        }else if docUploadStatus{
            imgDocDone.isHidden = true
            let img =  imgPending.image?.withRenderingMode(.alwaysTemplate)
            imgPending.image = img
            imgPending.isHidden = false
            imgPending.tintColor = .orange
            lbl_DocStatus.textColor = .orange
        }else {
            imgDocDone.isHidden = true
            imgPending.isHidden = false
             imgPending.tintColor = .red
            lbl_DocStatus.textColor = .red
        }
    }
}
