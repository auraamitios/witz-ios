//
//  CellMyTrips.swift
//  Witz
//
//  Created by abhishek kumar on 28/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CellMyTrips: UITableViewCell {

    //Outlet for cell
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var imgView_Icon: UIImageView!
    @IBOutlet weak var lbl_Count: UILabel!
    
    //Outlet for header view
    @IBOutlet weak var lbl_HeaderTitle: UILabel!
    @IBOutlet weak var view_Header: UIView!
    @IBOutlet weak var lbl_HeaderCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
