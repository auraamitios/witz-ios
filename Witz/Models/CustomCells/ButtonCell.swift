//
//  ButtonCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/4/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

protocol ButtonCellDelegate:class {
    func saveButtonClicked(sender:UIButton)
}

class ButtonCell: UITableViewCell {

    @IBOutlet weak var btnCell: UIButton!
    weak var cellDelegate:ButtonCellDelegate?
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        
    }
    
    public func setButtonTitle(title:String) {
        self.btnCell.setTitle(title, for: .normal)
       
    }
    @IBAction func action_Save(_ sender: UIButton) {
        if self.cellDelegate != nil {
            self.cellDelegate?.saveButtonClicked(sender: sender)
        }
    }
}
