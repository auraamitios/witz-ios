//
//  ReservedVehiclesCell.swift
//  Witz
//
//  Created by Amit Tripathi on 12/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
class ReservedVehiclesCell: UICollectionViewCell {
    
    @IBOutlet weak var vehicle_ImgView: UIImageView!
    @IBOutlet weak var lbl_Seat: UILabel!
    @IBOutlet weak var lbl_VehicleName: UILabel!
    @IBOutlet weak var lbl_SeatCount: UILabel!
    
    class var reuseIdentifier: String {return String.className(self)}
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialize()
        //fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    func initialize(){
        self.backgroundColor = UIColor.clear
        
    }
    func downloadImage(_ url:URL) {
        KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            DispatchQueue.main.async {
                if let thumbImage = image {
                    self.vehicle_ImgView.image = thumbImage
                }
            }
        })
    }
    func configureCell(with indexpath:IndexPath,_ info:VehiclesData) {
        
        let dict = info.picDetail
        let imgUrl = "\(kBaseImageURL)\(dict!["thumb"] as! String)"
        let url = imgUrl.url
        self.vehicle_ImgView.image = nil
        self.vehicle_ImgView.setNeedsDisplay()
        downloadImage(url!)
        self.lbl_Seat.text = "\(info.minBookingSeats ?? 0) - \(info.seats ?? 0)"
        self.lbl_VehicleName.text = info.name!
        self.lbl_VehicleName.tag = indexpath.row
    }
}
