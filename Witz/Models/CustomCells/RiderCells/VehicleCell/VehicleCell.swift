//
//  VehicleCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/27/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher

class VehicleCell: UICollectionViewCell {
    
    @IBOutlet weak var imgVehicle: UIImageView!
    @IBOutlet weak var lblSeat: UILabel!
    @IBOutlet weak var lblVehicleName: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    func initialize(){
//        self.layer.cornerRadius = 2.0
//        self.clipsToBounds = true
    }
    
    func downloadImage(_ url:URL) {
        
        KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            DispatchQueue.main.async {
//                self.activityView.stopAnimating()
//                self.activityView.isHidden = true
                if let thumbImage = image {
                    self.imgVehicle.image = thumbImage
                }
            }
        })
    }
    
    public func updateCell(with indexpath:IndexPath,_ info:VehiclesData) {
        print(info)
        let dict = info.picDetail
        let imgUrl = "\(kBaseImageURL)\(dict!["thumb"] as! String)"
        let url = imgUrl.url
        self.imgVehicle.image = nil
        self.imgVehicle.setNeedsDisplay()
        downloadImage(url!)
//        self.lblSeat.text = info.minBookingSeats?.string
        self.lblSeat.text =  "\(info.minBookingSeats ?? 0) - \(info.seats ?? 0)"
        
        self.lblVehicleName.text = info.name!.capitalized
    }
}
