//
//  SelectedVehicleCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/31/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import AlamofireImage
import Kingfisher
class SelectedVehicleCell: UICollectionViewCell {
    
    @IBOutlet weak var view_ImgView: UIView!
    @IBOutlet weak var vehicleImg: UIImageView!
    @IBOutlet weak var lblSeat: UILabel!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRiderCount: UILabel!
    @IBOutlet weak var vehicleStack: UIStackView!
    
    var textcolor = UIColor.white
    var seatCount = ["13-19","27-31", "4-8", "2-4"]
    
    class var reuseIdentifier: String { return String.className(self) }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialize()
        //fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    func initialize(){
        self.backgroundColor = UIColor.clear
        
    }
    override var isSelected: Bool{
        didSet{
            self.textcolor = isSelected == true ? UIColor.init(red: 0.8510, green: 0.6863, blue: 0.2784, alpha: 1.0) : UIColor.white
            self.lblSeat.textColor = self.textcolor
            self.lblPrice.textColor = self.textcolor
            self.lblRiderCount.textColor = self.textcolor
            self.lblVehicleName.textColor = self.textcolor
            self.view_ImgView.borderColor = isSelected == true ? UIColor.init(red: 0.8510, green: 0.6863, blue: 0.2784, alpha: 1.0) : UIColor.lightGray
        }
    }
    func downloadImage_(_ URLString:String) {
        self.vehicleImg.af_setImage(
            withURL: URL(string: URLString)!,
            placeholderImage: nil,
            filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: size, radius: 20.0),
            imageTransition: .crossDissolve(0.2)
        )
    }
    
    func downloadImage(_ url:URL) {
        KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            DispatchQueue.main.async {
                if let thumbImage = image {
                    self.vehicleImg.image = thumbImage
                }
            }
        })
    }
    
    func configureCell(with indexpath:IndexPath,_ info:VehiclesData) {
        
        let dict = info.picDetail
        let imgUrl = "\(kBaseImageURL)\(dict!["thumb"] as! String)"
        let url = imgUrl.url
        self.vehicleImg.image = nil
        self.vehicleImg.setNeedsDisplay()
        downloadImage(url!)
        self.lblSeat.text = "\(info.minBookingSeats ?? 0) - \(info.seats ?? 0)"
        self.lblVehicleName.text = info.name!.capitalized
    }
}
