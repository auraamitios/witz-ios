//
//  VehiclePriceCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/28/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import AlamofireImage
class VehiclePriceCell: UICollectionViewCell {
    
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var imgVehicle: UIImageView!
    @IBOutlet weak var lblSeatCount: UILabel!
    
    var seatCount = ["13-19","27-31", "4-8", "2-4"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    func initialize(){
        self.backgroundColor = UIColor.clear
    }
    
    func downloadImage(_ url:URL) {
        
        KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            DispatchQueue.main.async {
                if let thumbImage = image {
                    self.imgVehicle.image = thumbImage
                }
            }
        })
        
//        self.imgVehicle.af_setImage(
//            withURL: URL(string: URLString)!,
//            placeholderImage: placeholderImage,
//            filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: size, radius: 20.0),
//            imageTransition: .crossDissolve(0.2)
//        )
    }
    
    public func updateCell(with indexpath:IndexPath,_ info:VehiclesData) {
        print(info)
        let dict = info.picDetail
        let imgUrl = "\(kBaseImageURL)\(dict!["thumb"] as! String)"
        let url = imgUrl.url
        self.imgVehicle.image = nil
        self.imgVehicle.setNeedsDisplay()
        downloadImage(url!)

        //self.lblSeatCount.text = info.minBookingSeats?.string
        self.lblSeatCount.text = "\(info.minBookingSeats ?? 0) - \(info.seats ?? 0)"
        self.lblVehicleName.text = info.name!
    }
}
