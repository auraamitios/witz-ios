//
//  AggregatePendingTripCell.swift
//  Witz
//
//  Created by Amit Tripathi on 12/26/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class AggregatePendingTripCell: UITableViewCell {
    @IBOutlet weak var lbl_Days: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    class var identifier: String {return String.className(self)}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellRider(with data: Trips ,index:IndexPath){
        /*********  Aggregate trips **************/
        if data.tripType == "3"{
            if (data.workingTime?.count ?? 0) > 0 {
            let timeData = data.workingTime![0]
            self.lbl_Time.text = "\(timeData.time?.arivalTime ?? "")\("/")\(timeData.time?.departureTime ?? "")"
            }
            
            self.lbl_Days.text = aggregateDays(data: data)
        }
    }
    
    func aggregateDays(data:Trips)->String {
        var daysStr = ""
        if let count:Int = (data.workingTime?.count){
        for i in 0..<count {
            let dict = data.workingTime?[i]
            switch dict?.dayNumber {
            case 0?:
                daysStr = "\(daysStr)\("S ")"
                break
            case 1?:
                daysStr = "\(daysStr)\("M ")"
                break
            case 2?:
                daysStr = "\(daysStr)\("T ")"
                break
            case 3?:
                daysStr = "\(daysStr)\("W ")"
                break
            case 4?:
                daysStr = "\(daysStr)\("T ")"
                break
            case 5?:
                daysStr = "\(daysStr)\("F ")"
                break
            case 6?:
                daysStr = "\(daysStr)\("S ")"
                break
            default:
                break
            }
        }
        }
        return daysStr
    }

}
