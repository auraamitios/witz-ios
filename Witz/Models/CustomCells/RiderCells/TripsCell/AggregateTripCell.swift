//
//  AggregateTripCell.swift
//  Witz
//
//  Created by Amit Tripathi on 12/26/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class AggregateTripCell: UITableViewCell {

    @IBOutlet weak var lbl_Days: UILabel!
    @IBOutlet weak var lbl_DateTime: UILabel!
    @IBOutlet weak var lbl_Loc: UILabel!
    @IBOutlet weak var lbl_LocTime: UILabel!
    
    var homeToWork = "Home-Work"
    var workToHome = "Work-Home"
    
    class var identifier: String {return String.className(self)}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    func configureCellRider(with data: Trips ,index:IndexPath){
        /*********  Aggregate trips **************/
        if data.tripType == "3"{
               let timeData = data.workingTime![0]
            if data.aggregateTripType == 1 {
                self.lbl_Loc.text = self.homeToWork
                self.lbl_LocTime.text = "\(timeData.time?.arivalTime ?? "")"
                
//                self.lbl_LocTime.text = data.createdAt?.convertDateFormatter(formatNeeded: "HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? ""

            }else {
                self.lbl_Loc.text = self.workToHome
                self.lbl_LocTime.text = "\(timeData.time?.departureTime ?? "")"

            }
            
            self.lbl_DateTime.text = data.aggregateTripTime?.convertDateFormatter(formatNeeded: "d-MMM-yyyy", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? ""
            self.lbl_Days.text = aggregateDays(data: data)
           }
    }
    
    func aggregateDays(data:Trips)->String {
        var daysStr = ""
        let count:Int = (data.workingTime?.count)!
        for i in 0..<count {
            let dict = data.workingTime?[i]
            switch dict?.dayNumber {
            case 0?:
                daysStr = "\(daysStr)\("S ")"
                break
            case 1?:
                daysStr = "\(daysStr)\("M ")"
                break
            case 2?:
                daysStr = "\(daysStr)\("T ")"
                break
            case 3?:
                daysStr = "\(daysStr)\("W ")"
                break
            case 4?:
                daysStr = "\(daysStr)\("T ")"
                break
            case 5?:
                daysStr = "\(daysStr)\("F ")"
                break
            case 6?:
                daysStr = "\(daysStr)\("S ")"
                break
            default:
                break
            }
        }
        
        return daysStr
    }
}
