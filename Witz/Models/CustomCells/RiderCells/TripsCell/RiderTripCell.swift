//
//  RiderTripCell.swift
//  Witz
//
//  Created by Amit Tripathi on 11/29/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class RiderTripCell: UITableViewCell {

    //row outlets
    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Count: UILabel!
    
    //header outlets
    @IBOutlet weak var view_Header: UIView!
    @IBOutlet weak var lbl_HeaderTitle: UILabel!
        @IBOutlet weak var lbl_HeaderCount: UILabel!
    
    class var identifier: String {return String.className(self)}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
