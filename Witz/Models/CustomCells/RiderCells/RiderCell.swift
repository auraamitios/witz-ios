//
//  RiderCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/6/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class RiderCell: UITableViewCell {
    
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var txtValue: TextFieldInsets!
    
    class var identifier: String {return String.className(self)}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    public func updateCell(with indexpath:IndexPath, _ title:String, _ iconStr:String ,valueStr:String) {
        iconImg.image = UIImage(named:iconStr)
        txtValue.placeholder = title
        txtValue.tag = indexpath.section
        if valueStr != ""{
            txtValue.text = valueStr
        }
    }
}
