//
//  BidInfoCell.swift
//  Witz
//
//  Created by Amit Tripathi on 2/9/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON
class BidInfoCell: UITableViewCell {

    @IBOutlet weak var img_Pic: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_CarName: UILabel!
    @IBOutlet weak var lbl_CarNumber: UILabel!
    @IBOutlet weak var lbl_Fare: UILabel!
    @IBOutlet weak var lbl_Rating: UILabel!
    
    class var identifier: String{return String.className(self)}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellRider(with data: DriverDataRider ,index:IndexPath){
        self.lbl_Name.text = data.personalInformation?.fullName?.nameFormatted
        self.lbl_Rating.text = data.avgRating?.string
        self.lbl_CarName.text = "\(data.vehicleInformation?.brand ?? "") - \(data.vehicleInformation?.subBrand ?? "")"
        self.lbl_CarNumber.text = data.vehicleInformation?.vehicleNumber
        self.lbl_Fare.text = "\(data.offerPrice ?? 0.0 ) ₺"
        let imgUrl = "\(kBaseImageURL)\(data.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.img_Pic.image = thumbImage
                    }
                    self.img_Pic.setNeedsDisplay()
                }
            })
        }
    }
}
