//
//  BidHeaderCell.swift
//  Witz
//
//  Created by Amit Tripathi on 2/9/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class BidHeaderCell: UITableViewCell {

    @IBOutlet weak var lbl_DateTime: UILabel!
    
    class var identifier: String{return String.className(self)}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    public func configureCell(with indexpath:IndexPath,_ title:String) {
        lbl_DateTime.text = title
    }
}
