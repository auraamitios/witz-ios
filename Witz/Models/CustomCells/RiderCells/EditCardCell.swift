//
//  EditCardCell.swift
//  Witz
//
//  Created by Amit Tripathi on 2/7/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class EditCardCell: UITableViewCell {

    class var identifier: String {return String.className(self)}
    @IBOutlet weak var lbl_Value: UILabel!
    @IBOutlet weak var iconImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }

    public func configureCell(with indexpath:IndexPath,_ card:Cards, _ str:String) {
        iconImg.image = UIImage(named:str)
        switch indexpath.section {
        case 0:
            lbl_Value.text = card.nameOnCard ?? ""
            break
        case 1:
            lbl_Value.text = "**** **** **** \(card.lastFourDigit ?? "1111")"
            break
        case 2:
            lbl_Value.text = "--/--"
            break
        case 3:
            lbl_Value.text = "---"
            break
        case 4:
            lbl_Value.text = card.cardType ?? ""
            break
        default:
            break
        }
    }
}
