//
//  ListCardCell.swift
//  Witz
//
//  Created by Amit Tripathi on 2/6/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class ListCardCell: UITableViewCell {

    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var view_Bg: UIView!
    class var identifier: String {return String.className(self)}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
    }
    public func configureCell(with indexpath:IndexPath,_ card:Cards) {
        
        if card.defaultStatus ?? false {
            iconImg.image = UIImage(named:"ok_white")
            lblTitle.textColor = .white
            view_Bg.backgroundColor = App_Base_Color
        }else {
            lblTitle.textColor = App_Text_Color
            view_Bg.backgroundColor = .white
            iconImg.image = UIImage(named:"payment_method_credit_card")
        }
        lblTitle.text = card.cardType ?? ""
    }
}
