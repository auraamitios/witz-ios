//
//  AddCardCell.swift
//  Witz
//
//  Created by Amit Tripathi on 11/2/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class AddCardCell: UITableViewCell {

    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtValue: TextFieldInsets!
    
    class var identifier: String{return String.className(self)}
    var placeHolder = ["NAME SURNAME", "1234 1234 1234 1234", "_ _ /_ _", "_ _ _", "VISA"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    public func configureCell(with indexpath:IndexPath, _ title:String, _ iconStr:String,data:String?, delegate:AnyObject) {
        iconImg.image = UIImage(named:iconStr)
        lblTitle.text = title
        txtValue.tag = indexpath.section
        txtValue.delegate = (delegate as! UITextFieldDelegate)
        txtValue.placeholder = placeHolder[indexpath.section]
        if indexpath.section == 1 || indexpath.section == 2 || indexpath.section == 3{
            txtValue.keyboardType = .numberPad
        }
        if indexpath.section == 4 {
////           txtValue.text = title
            txtValue.keyboardType = .alphabet
        }
        if indexpath.section == 2 {
            let filteredVal = data?.replacingOccurrences(of: ".", with: "/")
            txtValue.text = filteredVal
            return
        }
        txtValue.text = data
    }
}
