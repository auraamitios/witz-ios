//
//  CardCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/31/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
    
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var img_Arrow: UIImageView!
    
    class var identifier: String {return String.className(self)}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    public func configureCell(with indexpath:IndexPath,_ title:String, _ iconStr:String) {
        iconImg.image = UIImage(named:iconStr)
        lblTitle.text = title
        
        if title == "ADD A PROMOTION CODE"{
            img_Arrow.isHidden = true
        }
    }
}
