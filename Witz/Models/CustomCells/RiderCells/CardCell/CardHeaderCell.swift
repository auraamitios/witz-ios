//
//  CardHeaderCell.swift
//  Witz
//
//  Created by Amit Tripathi on 2/6/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class CardHeaderCell: UITableViewCell {

    class var identifier: String {return String.className(self)}
    @IBOutlet weak var lbl_Title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
