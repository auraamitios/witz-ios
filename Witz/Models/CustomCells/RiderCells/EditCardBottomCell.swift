//
//  EditCardBottomCell.swift
//  Witz
//
//  Created by Amit Tripathi on 2/7/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class EditCardBottomCell: UITableViewCell {

    var titles = ["","","","","", "MAKE DEFAULT", "DELETE"]
    class var identifier: String {return String.className(self)}
    @IBOutlet weak var lbl_Title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    public func configureCell(with indexpath:IndexPath) {
        lbl_Title.text = titles[indexpath.section]
    }
}
