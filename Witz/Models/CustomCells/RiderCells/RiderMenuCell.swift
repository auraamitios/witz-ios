//
//  RiderMenuCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/12/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class RiderMenuCell: UITableViewCell {

    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuName: UILabel!
    @IBOutlet weak var lbl_NotificationCount: UILabel!
    
    class var identifier: String {return String.className(self)}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    open class func height() -> CGFloat {return 40.0 }
    
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }

    public func updateCell(with indexpath:IndexPath,_ item:String, _ icon:UIImage,showNotification:Bool) {
        menuIcon.image = icon
        menuName.text = item
        
        
        lbl_NotificationCount.isHidden = !showNotification
        
        if showNotification {
            
            if item == LinkStrings.RiderSideMenuItem.NOTIFICATION{
                if let count =   RiderManager.sharedInstance.allCounts?.notificationCount{
                    lbl_NotificationCount.text = "\(count)"
                    if count > 0{
                        lbl_NotificationCount.isHidden = false
                    }else{
                        lbl_NotificationCount.isHidden = true
                    }
                }
            }
            if item == LinkStrings.RiderSideMenuItem.TRIPS{
                let count =   (RiderManager.sharedInstance.allCounts?.reservationFutureTripCount ?? 0)+(RiderManager.sharedInstance.allCounts?.reservationPendingTripCount ?? 0)+(RiderManager.sharedInstance.allCounts?.serviceRequestPendingTripCount ?? 0)+(RiderManager.sharedInstance.allCounts?.serviceRequestFutureTripCount ?? 0)
                lbl_NotificationCount.text = "\(count)"
                if count > 0{
                    lbl_NotificationCount.isHidden = false
                }else{
                    lbl_NotificationCount.isHidden = true
                }
                
            }
        }
    }
}
