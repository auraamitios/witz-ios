//
//  AggregateSetTimeCell.swift
//  Witz
//
//  Created by Amit Tripathi on 2/22/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import UIKit

class AggregateSetTimeCell: UITableViewCell {

    @IBOutlet weak var lbl_Day: UILabel!
    @IBOutlet weak var btn_Box: UIButton!
    
    class var identifier: String {return String.className(self)}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    public func configureCell(with indexpath:IndexPath, _ title:String, selectFlag:Int) {
        self.lbl_Day.text = title
        self.btn_Box.tag = indexpath.row
        if selectFlag == 1 {
            self.btn_Box.setImage(#imageLiteral(resourceName: "check-min"), for: .normal)
        }else {
            self.btn_Box.setImage(#imageLiteral(resourceName: "uncheck-min"), for: .normal)
        }
    }
}
