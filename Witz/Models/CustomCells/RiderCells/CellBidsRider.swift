//
//  CellBidsRider.swift
//  Witz
//
//  Created by Amit Tripathi on 12/21/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON

class CellBidsRider: UITableViewCell {
    
    @IBOutlet weak var img_DriverPic: UIImageView!
    @IBOutlet weak var lbl_DriverRating: UILabel!
    @IBOutlet weak var lbl_DriverName: UILabel!
    @IBOutlet weak var lbl_VehicleName: UILabel!
    @IBOutlet weak var lbl_VehicleNumber: UILabel!
    @IBOutlet weak var lbl_BidRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCellRider(with data: DriverDataRider ,index:IndexPath){
        
        self.lbl_DriverName.text = data.personalInformation?.fullName?.nameFormatted
        self.lbl_DriverRating.text = data.avgRating?.string
        self.lbl_VehicleName.text = "\(data.vehicleInformation?.brand ?? "") - \(data.vehicleInformation?.subBrand ?? "")"
        self.lbl_VehicleNumber.text = data.vehicleInformation?.vehicleNumber
        self.lbl_BidRate.text = "\(data.offerPrice ?? 0.0 ) ₺"
        let imgUrl = "\(kBaseImageURL)\(data.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {
                    
                    if let thumbImage = image {
                        self.img_DriverPic.image = thumbImage
                    }
                    self.img_DriverPic.setNeedsDisplay()
                }
            })
        }
    }
    
    func configureCellAggregateRider(with data: AggreateRequestBidModal ,bidAmount:String ,index:IndexPath){
        if (data.bidData?.count ?? 0) > 0{
            let dataBid = data.bidData![0]
            self.lbl_DriverName.text = dataBid.driverId?.personalInformation?.fullName?.nameFormatted
        self.lbl_DriverRating.text = dataBid.driverId?.avgRating?.string
        self.lbl_VehicleName.text = "\(dataBid.driverId?.vehicleInformation?.subBrand ?? "") - \(dataBid.driverId?.vehicleInformation?.color ?? "")"
        self.lbl_VehicleNumber.text = dataBid.driverId?.vehicleInformation?.vehicleNumber
        self.lbl_BidRate.text = "\(bidAmount) ₺"
        let imgUrl = "\(kBaseImageURL)\(dataBid.driverId?.pic?.original ?? "")"
        if let url = imgUrl.url{
            KingfisherManager.shared.retrieveImage(with:url, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                DispatchQueue.main.async {

                    if let thumbImage = image {
                        self.img_DriverPic.image = thumbImage
                    }
                    self.img_DriverPic.setNeedsDisplay()
                }
            })
        }
        }
    }
}
