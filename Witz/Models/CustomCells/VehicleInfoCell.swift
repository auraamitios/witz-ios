//
//  VehicleInfoCell.swift
//  Witz
//
//  Created by Amit Tripathi on 10/5/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class VehicleInfoCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUp() {
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    public func updateCell(with indexpath:IndexPath,_ title:String,value:String, _ editingAllow : Bool ) {
        lblTitle.text = title
        txtValue.placeholder = title
        txtValue.isUserInteractionEnabled  = editingAllow
        txtValue.text = value
        txtValue.tag = indexpath.row
        
    }
}
