//
//  CellWallet.swift
//  Witz
//
//  Created by abhishek kumar on 10/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class CellWallet: UITableViewCell {
    
    @IBOutlet weak var lbl_amount: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(with data: Trips_Wallet ,index:IndexPath){
         lbl_amount.text = "\(data.driverEarning ?? 0) ₺"
        /*********  Instant ride trips**************/
        if data.tripType == "1"{
        
            if data.status == 21 {                                                // cancel by rider
                lbl_Date.text =   "Cancel by Rider \n\( data.createdAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
//                lbl_amount.text = "\(data.driverEarning ?? 0) ₺"
                lbl_amount.textColor = .red
            }else if data.status == 20 {                                            // cancel by Driver
                lbl_Date.text = "Cancel by Driver \n\( data.createdAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
//                lbl_amount.text = "\(data.driverEarning ?? 0) ₺"
                lbl_amount.textColor = .red
            }else{                                                                   // Completed ride
                lbl_Date.text = data.createdAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
//                lbl_amount.text = "\(data.driverEarning ?? 0) ₺"
                lbl_amount.textColor = UIColor.init(hex: "DEB850")
            }
        
        }
        /*********  Reservation trips**************/
        if data.tripType == "2"{

            if data.status == 21 {                                                // cancel by rider
                lbl_Date.text =   "Cancel by Rider \n\( data.bookingDate?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
               
                lbl_amount.textColor = .red
            }else if data.status == 20 {                                            // cancel by Driver
                lbl_Date.text = "Cancel by Driver \n\( data.bookingDate?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
               
                lbl_amount.textColor = .red
            }else{                                                                   // Completed ride
                lbl_Date.text = data.bookingDate?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
               
                lbl_amount.textColor = UIColor.init(hex: "DEB850")
            }

        }

         /*********  Aggregate trips **************/
        if data.tripType == "3"{

            if data.status == 21 {                                                // cancel by rider
                lbl_Date.text =   "Cancel by Rider \n\( data.updatedAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss .SSSZ") ?? "")"
             
                lbl_amount.textColor = .red
            }else if data.status == 20 {                                            // cancel by Driver
                lbl_Date.text = "Cancel by Driver \n\( data.updatedAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ") ?? "")"
              
                lbl_amount.textColor = .red
            }else{                                                                   // Completed ride
                lbl_Date.text = data.updatedAt?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                
                lbl_amount.textColor = UIColor.init(hex: "DEB850")
            }
        }
        
    }
}
