//
//  CellNotification.swift
//  Witz
//
//  Created by abhishek kumar on 28/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import SwiftyJSON

class CellNotification: UITableViewCell {
    
    @IBOutlet weak var imgView_Notification: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Description: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var btn_Clear: UIButton!
    @IBOutlet weak var view_BackGround: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //*********** configure cell for driver ***************//
    func updateCellData(_ modal: JSON , index:IndexPath){
    
        lbl_Description.text = modal["notificationMsg"].string ?? ""
        lbl_Date.text = modal["notificationDate"].string?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        btn_Clear.tag = index.row
        imgView_Notification.image = #imageLiteral(resourceName: "icon_shows_transfer_status")
        //************************* check if notification related to trip
        if let type  =  modal["tripType"].string{
            lbl_Title.text = modal["riderId"]["fullName"].string?.capitalized.nameFormatted
            switch (type){
            case "2":
                imgView_Notification.image = #imageLiteral(resourceName: "reservation_to_where_yellow")
                break
            case "3":
                imgView_Notification.image = #imageLiteral(resourceName: "menu_page_aggregate")
                break
                
            default:
                break
            }
        }
        else if let doc  =  modal["document_name"].string{
            lbl_Title.text = doc
            imgView_Notification.image = #imageLiteral(resourceName: "driver_reigster_my_documents")
        }
        
        //*********************** check if notification is read by user
        if let isRead  =  modal["isRead"].bool{
            if isRead == false{
                view_BackGround?.backgroundColor = UIColor.init(red: 0.9412, green: 0.8706, blue: 0.6863, alpha: 1.0)
            }else{
                view_BackGround?.backgroundColor = .white
            }
        }
    }
    
    //************ configure cell for rider ***************//
    func configureCellForRider(_ modal: JSON , index:IndexPath){
        
        lbl_Description.text = modal["notificationMsg"].string ?? ""
        lbl_Date.text = modal["notificationDate"].string?.convertDateFormatter(formatNeeded: "E, d-MMM-yyyy / HH:mm", formatHave: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        btn_Clear.tag = index.row
        imgView_Notification.image = #imageLiteral(resourceName: "icon_shows_transfer_status")
        //************************* check if notification related to trip
        if let type  =  modal["tripType"].string{
            lbl_Title.text = modal["driverId"]["personal_information"]["fullName"].string?.capitalized.nameFormatted
            switch (type){
            case "1":
                imgView_Notification.image = Utility.imageWithImage(image: #imageLiteral(resourceName: "driver_trip_time_yellow"), scaledToSize: CGSize(width: 30.0, height: 30.0))

                break
            case "2":
                imgView_Notification.image =  Utility.imageWithImage(image: #imageLiteral(resourceName: "reservation_to_where_yellow"), scaledToSize: CGSize(width: 30.0, height: 30.0))
                break
            case "3":
                imgView_Notification.image =  Utility.imageWithImage(image: #imageLiteral(resourceName: "menu_page_aggregate"), scaledToSize: CGSize(width: 30.0, height: 30.0))
                break
            default:
                break
            }
        }
        else if let doc  =  modal["document_name"].string{
            lbl_Title.text = doc
            imgView_Notification.image = #imageLiteral(resourceName: "driver_reigster_my_documents")
        }
        
        //*********************** check if notification is read by user
        if let isRead  =  modal["isRead"].bool{
            if isRead == false{
                view_BackGround?.backgroundColor = UIColor.init(red: 0.9412, green: 0.8706, blue: 0.6863, alpha: 1.0)
            }else{
                view_BackGround?.backgroundColor = .white
            }
        }
    }
}
