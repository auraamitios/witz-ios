//
//  DriverManager.swift
//  Witz
//
//  Created by abhishek kumar on 28/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
enum vehicleType : Int {
    case CAR = 1
    case VAN
    case MINIBUS
    case MIDIBUS    
}
enum TripOnState : Int {
    case QUEUING = 1
    case ON_THE_WAY
    case ONGOING
    case CANCELLED_BY_RIDER
    case CANCELLED_BY_DRIVER
    case COMPLETED
    case REACHED
    case REJECTED
    case PAYMENT_PENDING
    case BOOKING_ACCEPTED
    case BOOKING_REJECTED
    case RIDER_NOT_REACHED
    case AGGREGATE_TRIP_ACCEPTED_DRIVER
    case AGGREGATE_TRIP_HAVE_BIDS
    case AGGREGATE_TRIP_ACCEPTED_BY_RIDER
    case RESERVATION_BOOKING_PENDING
    case ADMIN_CREATED_YOUR_AGGREGATE_TRIP
    case ADMIN_CREATED_FINAL_AGGREGATE_TRIP = 19
    case Driver_CANCEL_TRIP_PNALITY
    case RIDER_CANCEL_TRIP_PNALITY
    case ONHOLD
    case RIDER_CANCELED_AGGREGATE_TRIP_REFUND
    case ABSENT_RIDER_AGGREGATE_TRIP_NOT_COMING
    case TRIPS_AGGREGATE_READY_START_BY_DRIVER
}

class DriverManager: NSObject {
    static var sharedInstance =  DriverManager()
    private override init() {} //This prevents others from using the default '()' initializer for this class.
    
    var addressSelected : String?
    var user_Driver = DriverInfo()
    var ride_Details : TripModalDriver?
    var tripId : String?
    var driverStartLocation : CLLocation?
    var driverEndLocation : CLLocation?
    var isDriverOnTrip : Bool?
    var reservationTripGoingOn : Bool?
    
    // Sharing variable
    var sharedSeatUsed : Int?
    var shareTripGoingOn : Bool?
    var newShareRequestAdded : Bool?
    var rideRequestCanceled : Bool?
    var arr_ShareTrips : [TripModalDriver]?
    var arr_SortedShareTrips : [[String:Any]]?
    var ride_DetailsDrop : TripModalDriver?
    var tripIdDrop : String?
    var ignoreRequestNow : Bool?
    var trip_Pending : TripPendingBackgorundModal?
    
    
    //Aggregate variable
    var isAggregateTrip : Bool?
    var ride_DetailsAggreagate : AggregateDriverStartTripModal?
    var ride_DetailsDropAggregate : AggregateDriverStartTripModal?
    var arr_ShareTripsAggregate : [TripsAggregate]?

    
    func VehicleOwnedbyDriver() -> UIImage{
        if  self.user_Driver.brand == "car"{
            return #imageLiteral(resourceName: "vehicle-car_on-map")
        }
        if  self.user_Driver.brand == "van"{
            return #imageLiteral(resourceName: "vehicle_van_on_map")
        }
        
        if  self.user_Driver.brand == "minibus"{
            return #imageLiteral(resourceName: "vehicle_bus_on_map")
        }
        
        if  self.user_Driver.brand == "midibus"{
            return #imageLiteral(resourceName: "vehicle_bus_on_map")
        }
        if  self.user_Driver.brand == "bus"{
            return #imageLiteral(resourceName: "vehicle_bus_on_map")
        }
        return #imageLiteral(resourceName: "vehicle-car_on-map")
    }
}
