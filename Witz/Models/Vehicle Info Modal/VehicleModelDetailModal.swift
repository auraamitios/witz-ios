//
//  VehicleModelDetailModal.swift
//  Witz
//
//  Created by abhishek kumar on 30/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class VehicleModelDetailModal {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
        static let brand = "brand"
        static let id = "_id"
    }
    
    // MARK: Properties
   
    var brand: [BrandModal]?
    public var id: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        id = json[SerializationKeys.id].string
        if let items = json[SerializationKeys.brand].array{ brand = items.map { BrandModal(json:$0) } }
       
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
       
        if let value = brand { dictionary[SerializationKeys.brand] = value.map { $0.dictionaryRepresentation() } }
        return dictionary
    }
    
}
