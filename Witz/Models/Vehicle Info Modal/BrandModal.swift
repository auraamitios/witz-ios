//
//  BrandModal.swift
//  Witz
//
//  Created by abhishek kumar on 30/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BrandModal {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let id = "_id"
        static let isDeleted = "isDeleted"
        static let pic = "pic"
        static let model = "model"
        static let isBlocked = "isBlocked"
    }
    
    // MARK: Properties
    public var name: String?
    public var id: String?
    public var isDeleted: Bool? = false
    var pic: picBrand?
    public var model: [Model]?
    public var isBlocked: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        name = json[SerializationKeys.name].string
        id = json[SerializationKeys.id].string
        isDeleted = json[SerializationKeys.isDeleted].boolValue
        pic = picBrand(json: json[SerializationKeys.pic])
        if let items = json[SerializationKeys.model].array { model = items.map { Model(json: $0) } }
        isBlocked = json[SerializationKeys.isBlocked].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        dictionary[SerializationKeys.isDeleted] = isDeleted
        if let value = pic { dictionary[SerializationKeys.pic] = value.dictionaryRepresentation() }
        if let value = model { dictionary[SerializationKeys.model] = value.map { $0.dictionaryRepresentation() } }
        dictionary[SerializationKeys.isBlocked] = isBlocked
        return dictionary
    }
    
}

public final class Model {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let id = "_id"
        static let isDeleted = "isDeleted"
        static let isBlocked = "isBlocked"
        static let age = "age"
        static let registered = "registered"
        static let pic = "pic"
        static let seats = "seats"
    }
    
    // MARK: Properties
    public var name: String?
    public var id: String?
    public var isDeleted: Bool? = false
    public var isBlocked: Bool? = false
    public var age: Int?
    public var registered: Int?
    var pic: picBrand?
    public var seats: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        name = json[SerializationKeys.name].string
        id = json[SerializationKeys.id].string
        isDeleted = json[SerializationKeys.isDeleted].boolValue
        isBlocked = json[SerializationKeys.isBlocked].boolValue
        age = json[SerializationKeys.age].int
        registered = json[SerializationKeys.registered].int
        pic = picBrand(json: json[SerializationKeys.pic])
        seats = json[SerializationKeys.seats].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        dictionary[SerializationKeys.isDeleted] = isDeleted
        dictionary[SerializationKeys.isBlocked] = isBlocked
        if let value = age { dictionary[SerializationKeys.age] = value }
        if let value = registered { dictionary[SerializationKeys.registered] = value }
        if let value = pic { dictionary[SerializationKeys.pic] = value.dictionaryRepresentation() }
        if let value = seats { dictionary[SerializationKeys.seats] = value }
        return dictionary
    }
    
}


class picBrand{
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
        static let thumb = "thumb"
        static let original = "original"
        
    }
    
    // MARK: Properties
    
    public var thumb: String?
    public var original: String?
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        original = json[SerializationKeys.original].string
        thumb = json[SerializationKeys.thumb].string
    }
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = original { dictionary[SerializationKeys.original] = value }
        if let value = thumb { dictionary[SerializationKeys.thumb] = value }
        return dictionary
    }
}
