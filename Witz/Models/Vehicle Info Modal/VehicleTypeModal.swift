//
//  VehicleTypeModal.swift
//  Witz
//
//  Created by abhishek kumar on 28/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON
public final class VehicleTypeModal {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
        static let pic = "pic"
        static let isShared = "isShared"
        static let isReserved = "isReserved"
        static let isInstant = "isInstant"
        static let isPrivate = "isPrivate"
        static let minBookingSeats = "minBookingSeats"
        static let seats = "seats"
        static let name = "name"
        static let id = "_id"
    }
    
    // MARK: Properties
     var pic: picVehicle?
    public var isShared: Bool?
    public var isReserved: Bool?
    public var isInstant: Bool?
    public var isPrivate: Bool?
    public var minBookingSeats: Int?
    public var seats: Int?
    public var name: String?
    public var id: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
       pic = picVehicle(json: json[SerializationKeys.pic])
        id = json[SerializationKeys.id].string
        name = json[SerializationKeys.name].string
        seats = json[SerializationKeys.seats].int
        minBookingSeats = json[SerializationKeys.minBookingSeats].int
        isShared = json[SerializationKeys.isShared].bool
        isReserved = json[SerializationKeys.isReserved].bool
        isInstant = json[SerializationKeys.isInstant].bool
        isPrivate = json[SerializationKeys.isPrivate].bool
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = pic {
            dictionary[SerializationKeys.pic] = value.dictionaryRepresentation() }
        
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = seats { dictionary[SerializationKeys.seats] = value }
        if let value = minBookingSeats { dictionary[SerializationKeys.minBookingSeats] = value }
        if let value = isShared { dictionary[SerializationKeys.isShared] = value }
        if let value = isReserved { dictionary[SerializationKeys.isReserved] = value }
        if let value = isInstant { dictionary[SerializationKeys.isInstant] = value }
        if let value = isPrivate { dictionary[SerializationKeys.isPrivate] = value }
        return dictionary
    }
   
}

class picVehicle{
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
        static let thumb = "thumb"
        static let original = "original"
        
    }
    
    // MARK: Properties
    
    public var thumb: String?
    public var original: String?
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        original = json[SerializationKeys.original].string
        thumb = json[SerializationKeys.thumb].string
    }
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = original { dictionary[SerializationKeys.original] = value }
        if let value = thumb { dictionary[SerializationKeys.thumb] = value }
        return dictionary
    }
}

