//
//  WalletFinalDriverModal.swift
//
//  Created by abhishek kumar on 06/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class WalletFinalDriverModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let referalAmount = "referalAmount"
    static let sharedTotalAmount = "sharedTotalAmount"
    static let totalAmount = "totalAmount"
    static let reservationTotalAmount = "reservationTotalAmount"
    static let aggregateTotalAmount = "aggregateTotalAmount"
    static let totalTrips = "totalTrips"
    static let instantTotalAmount = "instantTotalAmount"
    static let welcomeWallet = "welcomeWallet"
    static let totalKm = "totalKm"
    static let trips = "trips"
  }

  // MARK: Properties
  public var referalAmount: Float?
  public var sharedTotalAmount: Float?
  public var totalAmount: Float?
  public var reservationTotalAmount: Int?
  public var aggregateTotalAmount: Int?
  public var totalTrips: Int?
  public var instantTotalAmount: Int?
  public var welcomeWallet: Float?
  public var totalKm: Float?
  public var trips: [Trips_Wallet]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    referalAmount = json[SerializationKeys.referalAmount].float
    sharedTotalAmount = json[SerializationKeys.sharedTotalAmount].float
    totalAmount = json[SerializationKeys.totalAmount].float
    reservationTotalAmount = json[SerializationKeys.reservationTotalAmount].int
    aggregateTotalAmount = json[SerializationKeys.aggregateTotalAmount].int
    totalTrips = json[SerializationKeys.totalTrips].int
    instantTotalAmount = json[SerializationKeys.instantTotalAmount].int
    welcomeWallet = json[SerializationKeys.welcomeWallet].float
    totalKm = json[SerializationKeys.totalKm].float
    if let items = json[SerializationKeys.trips].array { trips = items.map { Trips_Wallet(json: $0) } }
  }

}
public final class Trips_Wallet {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let bidRejectedByDriver = "bidRejectedByDriver"
        static let driverReachedKM = "driverReachedKM"
        static let driverDeclineInstant = "driverDeclineInstant"
        static let localbookingDateTime = "localbookingDateTime"
        static let driverEarning = "driverEarning"
        static let schedulerId = "schedulerId"
        static let paymentStatus = "paymentStatus"
        static let endLocation = "endLocation"
        static let timezone = "timezone"
        static let transId = "transId"
        static let isFinalAggregate = "isFinalAggregate"
        static let workingTime = "workingTime"
        static let kmTravel = "kmTravel"
        static let riderRating = "riderRating"
        static let endAddress = "end_address"
        static let riderCancelationFee = "riderCancelationFee"
        static let seatsRequired = "seatsRequired"
        static let parentSharingTrip = "parentSharingTrip"
        static let driverRating = "driverRating"
        static let cardId = "cardId"
        static let bidByDriver = "bidByDriver"
        static let price = "price"
        static let vehicleType = "vehicleType"
        static let sentBookingReguestToDriver = "sentBookingReguestToDriver"
        static let adminMarkStartAddress = "admin_Mark_Start_address"
        static let timezoneString = "timezoneString"
        static let updatedAt = "updatedAt"
        static let adminMarkPickUpLocation = "adminMarkPickUpLocation"
        static let driverPushInQ = "driverPushInQ"
        static let bookingDate = "bookingDate"
        static let driverReachedTime = "driverReachedTime"
        static let driverCancelationFee = "driverCancelationFee"
        static let riderTaxApplied = "riderTaxApplied"
        static let aggregateRequestPendingByDriver = "aggregateRequestPendingByDriver"
        static let tripRefundAmount = "tripRefundAmount"
        static let distance = "distance"
        static let walletPayment = "walletPayment"
        static let bridgeTax = "bridgeTax"
        static let startLocation = "startLocation"
        static let duration = "duration"
        static let driverTaxApplied = "driverTaxApplied"
        static let adminMarkDropOffLocation = "adminMarkDropOffLocation"
        static let tripType = "tripType"
        static let adminMarkEndAddress = "admin_Mark_End_address"
        static let createdAt = "createdAt"
        static let aggregatePaymentStatus = "aggregatePaymentStatus"
        static let isShared = "isShared"
        static let riderId = "riderId"
        static let status = "status"
        static let startAddress = "start_address"
        static let id = "_id"
        static let v = "__v"
        static let driverId = "driverId"
        static let vehicleInformation = "vehicle_information"
        static let adminEarning = "adminEarning"
    }
    
    // MARK: Properties
    public var bidRejectedByDriver: [Any]?
    public var driverReachedKM: Int?
    public var driverDeclineInstant: [Any]?
    public var localbookingDateTime: String?
    public var driverEarning: Float?
    public var schedulerId: String?
    public var paymentStatus: String?
    public var endLocation: Location_Wallet?
    public var timezone: String?
    public var transId: String?
    public var isFinalAggregate: Bool? = false
    public var workingTime: [Any]?
    public var kmTravel: Int?
    public var riderRating: Int?
    public var endAddress: String?
    public var riderCancelationFee: Int?
    public var seatsRequired: Int?
    public var parentSharingTrip: Bool? = false
    public var driverRating: Int?
    public var cardId: CardId?
    public var bidByDriver: [BidByDriver]?
    public var price: Float?
    public var vehicleType: String?
    public var sentBookingReguestToDriver: [Any]?
    public var adminMarkStartAddress: String?
    public var timezoneString: String?
    public var updatedAt: String?
    public var adminMarkPickUpLocation: AdminMarkPickUpLocation?
    public var driverPushInQ: [Any]?
    public var bookingDate: String?
    public var driverReachedTime: Int?
    public var driverCancelationFee: Float?
    public var riderTaxApplied: Float?
    public var aggregateRequestPendingByDriver: [Any]?
    public var tripRefundAmount: Int?
    public var distance: Float?
    public var walletPayment: Int?
    public var bridgeTax: Int?
    public var startLocation: Location_Wallet?
    public var duration: Float?
    public var driverTaxApplied: Float?
    public var adminMarkDropOffLocation: AdminMarkDropOffLocation?
    public var tripType: String?
    public var adminMarkEndAddress: String?
    public var createdAt: String?
    public var aggregatePaymentStatus: Int?
    public var isShared: Bool? = false
    public var riderId: RiderId?
    public var status: Int?
    public var startAddress: String?
    public var id: String?
    public var v: Int?
    public var driverId: DriverId?
    public var vehicleInformation: VehicleInformation?
    public var adminEarning: Float?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        if let items = json[SerializationKeys.bidRejectedByDriver].array { bidRejectedByDriver = items.map { $0.object} }
        driverReachedKM = json[SerializationKeys.driverReachedKM].int
        if let items = json[SerializationKeys.driverDeclineInstant].array { driverDeclineInstant = items.map { $0.object} }
        localbookingDateTime = json[SerializationKeys.localbookingDateTime].string
        driverEarning = json[SerializationKeys.driverEarning].float
        schedulerId = json[SerializationKeys.schedulerId].string
        paymentStatus = json[SerializationKeys.paymentStatus].string
        endLocation = Location_Wallet(json: json[SerializationKeys.endLocation])
        timezone = json[SerializationKeys.timezone].string
        transId = json[SerializationKeys.transId].string
        isFinalAggregate = json[SerializationKeys.isFinalAggregate].boolValue
        if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { $0.object} }
        kmTravel = json[SerializationKeys.kmTravel].int
        riderRating = json[SerializationKeys.riderRating].int
        endAddress = json[SerializationKeys.endAddress].string
        riderCancelationFee = json[SerializationKeys.riderCancelationFee].int
        seatsRequired = json[SerializationKeys.seatsRequired].int
        parentSharingTrip = json[SerializationKeys.parentSharingTrip].boolValue
        driverRating = json[SerializationKeys.driverRating].int
        cardId = CardId(json: json[SerializationKeys.cardId])
        if let items = json[SerializationKeys.bidByDriver].array { bidByDriver = items.map { BidByDriver(json: $0) } }
        price = json[SerializationKeys.price].float
        vehicleType = json[SerializationKeys.vehicleType].string
        if let items = json[SerializationKeys.sentBookingReguestToDriver].array { sentBookingReguestToDriver = items.map { $0.object} }
        adminMarkStartAddress = json[SerializationKeys.adminMarkStartAddress].string
        timezoneString = json[SerializationKeys.timezoneString].string
        updatedAt = json[SerializationKeys.updatedAt].string
        adminMarkPickUpLocation = AdminMarkPickUpLocation(json: json[SerializationKeys.adminMarkPickUpLocation])
        if let items = json[SerializationKeys.driverPushInQ].array { driverPushInQ = items.map { $0.object} }
        bookingDate = json[SerializationKeys.bookingDate].string
        driverReachedTime = json[SerializationKeys.driverReachedTime].int
        driverCancelationFee = json[SerializationKeys.driverCancelationFee].float
        riderTaxApplied = json[SerializationKeys.riderTaxApplied].float
        if let items = json[SerializationKeys.aggregateRequestPendingByDriver].array { aggregateRequestPendingByDriver = items.map { $0.object} }
        tripRefundAmount = json[SerializationKeys.tripRefundAmount].int
        distance = json[SerializationKeys.distance].float
        walletPayment = json[SerializationKeys.walletPayment].int
        bridgeTax = json[SerializationKeys.bridgeTax].int
        startLocation = Location_Wallet(json: json[SerializationKeys.startLocation])
        duration = json[SerializationKeys.duration].float
        driverTaxApplied = json[SerializationKeys.driverTaxApplied].float
        adminMarkDropOffLocation = AdminMarkDropOffLocation(json: json[SerializationKeys.adminMarkDropOffLocation])
        tripType = json[SerializationKeys.tripType].string
        adminMarkEndAddress = json[SerializationKeys.adminMarkEndAddress].string
        createdAt = json[SerializationKeys.createdAt].string
        aggregatePaymentStatus = json[SerializationKeys.aggregatePaymentStatus].int
        isShared = json[SerializationKeys.isShared].boolValue
        riderId = RiderId(json: json[SerializationKeys.riderId])
        status = json[SerializationKeys.status].int
        startAddress = json[SerializationKeys.startAddress].string
        id = json[SerializationKeys.id].string
        v = json[SerializationKeys.v].int
        driverId = DriverId(json: json[SerializationKeys.driverId])
        vehicleInformation = VehicleInformation(json: json[SerializationKeys.vehicleInformation])
        adminEarning = json[SerializationKeys.adminEarning].float
    }
}
public final class Location_Wallet {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
}

public final class RiderId {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let mobile = "mobile"
        static let pic = "pic"
        static let email = "email"
        static let fullName = "fullName"
        static let id = "_id"
        static let riderStatus = "riderStatus"
    }
    
    // MARK: Properties
    public var mobile: String?
    public var pic: Pic?
    public var email: String?
    public var fullName: String?
    public var id: String?
    public var riderStatus: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        mobile = json[SerializationKeys.mobile].string
        pic = Pic(json: json[SerializationKeys.pic])
        email = json[SerializationKeys.email].string
        fullName = json[SerializationKeys.fullName].string
        id = json[SerializationKeys.id].string
        riderStatus = json[SerializationKeys.riderStatus].string
    }
}
public final class DriverId {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let totalRefferalAmount = "totalRefferalAmount"
        static let id = "_id"
        static let personalInformation = "personal_information"
        static let welcomeWallet = "welcomeWallet"
        static let vehicleInformation = "vehicle_information"
    }
    
    // MARK: Properties
    public var totalRefferalAmount: Int?
    public var id: String?
    public var personalInformation: PersonalInformation_Wallet?
    public var welcomeWallet: Int?
    public var vehicleInformation: VehicleInformation?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        totalRefferalAmount = json[SerializationKeys.totalRefferalAmount].int
        id = json[SerializationKeys.id].string
        personalInformation = PersonalInformation_Wallet(json: json[SerializationKeys.personalInformation])
        welcomeWallet = json[SerializationKeys.welcomeWallet].int
        vehicleInformation = VehicleInformation(json: json[SerializationKeys.vehicleInformation])
    }
  }

public final class PersonalInformation_Wallet {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let companyName = "company_name"
        static let email = "email"
        static let fullName = "fullName"
        static let isVerified = "isVerified"
        static let address = "address"
    }
    
    // MARK: Properties
    public var companyName: String?
    public var email: String?
    public var fullName: String?
    public var isVerified: Bool? = false
    public var address: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        companyName = json[SerializationKeys.companyName].string
        email = json[SerializationKeys.email].string
        fullName = json[SerializationKeys.fullName].string
        isVerified = json[SerializationKeys.isVerified].boolValue
        address = json[SerializationKeys.address].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = companyName { dictionary[SerializationKeys.companyName] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
        dictionary[SerializationKeys.isVerified] = isVerified
        if let value = address { dictionary[SerializationKeys.address] = value }
        return dictionary
    }
    
}


public final class CardId {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let nameOnCard = "nameOnCard"
        static let id = "_id"
        static let cardType = "cardType"
        static let isDeleted = "isDeleted"
        static let paymentGatewayId = "paymentGatewayId"
        static let defaultStatus = "defaultStatus"
        static let lastFourDigit = "lastFourDigit"
        static let v = "__v"
        static let isBlocked = "isBlocked"
        static let cardToken = "cardToken"
        static let riderId = "riderId"
    }
    
    // MARK: Properties
    public var nameOnCard: String?
    public var id: String?
    public var cardType: String?
    public var isDeleted: Bool? = false
    public var paymentGatewayId: String?
    public var defaultStatus: Bool? = false
    public var lastFourDigit: String?
    public var v: Int?
    public var isBlocked: Bool? = false
    public var cardToken: String?
    public var riderId: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        nameOnCard = json[SerializationKeys.nameOnCard].string
        id = json[SerializationKeys.id].string
        cardType = json[SerializationKeys.cardType].string
        isDeleted = json[SerializationKeys.isDeleted].boolValue
        paymentGatewayId = json[SerializationKeys.paymentGatewayId].string
        defaultStatus = json[SerializationKeys.defaultStatus].boolValue
        lastFourDigit = json[SerializationKeys.lastFourDigit].string
        v = json[SerializationKeys.v].int
        isBlocked = json[SerializationKeys.isBlocked].boolValue
        cardToken = json[SerializationKeys.cardToken].string
        riderId = json[SerializationKeys.riderId].string
    }
 }
public final class AdminMarkPickUpLocation {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
}
public final class AdminMarkDropOffLocation {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
}

