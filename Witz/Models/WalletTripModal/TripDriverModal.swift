//
//  TripInWalletModal.swift
//
//  Created by abhishek kumar on 10/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TripDriverModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let count = "count"
    static let tripsInfo = "tripsInfo"
    static let payload = "payload"
    static let todayDateDateTime = "todayDateDateTime"
    static let todayDateTimestamp = "todayDateTimestamp"
  }

  // MARK: Properties
  public var count: Int?
  public var tripsInfo: [TripsInfo]?
  public var payload: Payload?
  public var todayDateDateTime: String?
  public var todayDateTimestamp: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    count = json[SerializationKeys.count].int
    if let items = json[SerializationKeys.tripsInfo].array { tripsInfo = items.map { TripsInfo(json: $0) } }
    payload = Payload(json: json[SerializationKeys.payload])
    todayDateDateTime = json[SerializationKeys.todayDateDateTime].string
    todayDateTimestamp = json[SerializationKeys.todayDateTimestamp].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = count { dictionary[SerializationKeys.count] = value }
    if let value = tripsInfo { dictionary[SerializationKeys.tripsInfo] = value.map { $0.dictionaryRepresentation() } }
    if let value = payload { dictionary[SerializationKeys.payload] = value.dictionaryRepresentation() }
    if let value = todayDateDateTime { dictionary[SerializationKeys.todayDateDateTime] = value }
    if let value = todayDateTimestamp { dictionary[SerializationKeys.todayDateTimestamp] = value }
    return dictionary
  }

}

public final class TripsInfo {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
       static let wayEndPoints = "wayEndPoints"
        static let bookingDate = "bookingDate"
        static let driverReachedTime = "driverReachedTime"
        static let bidRejectedByDriver = "bidRejectedByDriver"
        static let driverReachedKM = "driverReachedKM"
        static let driverCancelationFee = "driverCancelationFee"
        static let driverDeclineInstant = "driverDeclineInstant"
        static let localbookingDateTime = "localbookingDateTime"
        static let driverEarning = "driverEarning"
        static let schedulerId = "schedulerId"
        static let riderTaxApplied = "riderTaxApplied"
        static let paymentStatus = "paymentStatus"
        static let aggregateRequestPendingByDriver = "aggregateRequestPendingByDriver"
        static let endLocation = "endLocation"
        static let isFinalAggregate = "isFinalAggregate"
        static let transId = "transId"
        static let distance = "distance"
        static let workingTime = "workingTime"
        static let timezone = "timezone"
        static let bridgeTax = "bridgeTax"
        static let startLocation = "startLocation"
        static let kmTravel = "kmTravel"
        static let duration = "duration"
        static let driverTaxApplied = "driverTaxApplied"
        static let riderRating = "riderRating"
        static let tripType = "tripType"
        static let endAddress = "end_address"
        static let riderCancelationFee = "riderCancelationFee"
        static let seatsRequired = "seatsRequired"
        static let createdAt = "createdAt"
        static let parentSharingTrip = "parentSharingTrip"
        static let driverRating = "driverRating"
        static let cardId = "cardId"
        static let bidByDriver = "bidByDriver"
        static let riderId = "riderId"
        static let price = "price"
        static let aggregatePaymentStatus = "aggregatePaymentStatus"
        static let isShared = "isShared"
        static let vehicleType = "vehicleType"
        static let status = "status"
        static let id = "_id"
        static let startAddress = "start_address"
        static let sentBookingReguestToDriver = "sentBookingReguestToDriver"
        static let updatedAt = "updatedAt"
        static let v = "__v"
        static let driverId = "driverId"
        static let driverPushInQ = "driverPushInQ"
        static let adminEarning = "adminEarning"
    }
    
    // MARK: Properties
    public var wayEndPoints: [WayEndPoints]?
    public var bookingDate: String?
    public var driverReachedTime: Int?
    public var bidRejectedByDriver: [Any]?
    public var driverReachedKM: Int?
    public var driverCancelationFee: Int?
    public var driverDeclineInstant: [Any]?
    public var localbookingDateTime: String?
    public var driverEarning: Float?
    public var schedulerId: String?
    public var riderTaxApplied: Float?
    public var paymentStatus: String?
    public var aggregateRequestPendingByDriver: [Any]?
    public var endLocation: Locations?
    public var isFinalAggregate: Bool? = false
    public var transId: String?
    public var distance: Float?
    public var workingTime: [Any]?
    public var timezone: String?
    public var bridgeTax: Int?
    public var startLocation: Locations?
    public var kmTravel: Int?
    public var duration: Float?
    public var driverTaxApplied: Float?
    public var riderRating: Int?
    public var tripType: String?
    public var endAddress: String?
    public var riderCancelationFee: Float?
    public var seatsRequired: Int?
    public var createdAt: String?
    public var parentSharingTrip: Bool? = false
    public var driverRating: Int?
    public var cardId: String?
    public var bidByDriver: [BidByDriverWallet]?
    public var riderId: String?
    public var price: Float?
    public var aggregatePaymentStatus: Int?
    public var isShared: Bool? = false
    public var vehicleType: String?
    public var status: Int?
    public var id: String?
    public var startAddress: String?
    public var sentBookingReguestToDriver: [Any]?
    public var updatedAt: String?
    public var v: Int?
    public var driverId: String?
    public var driverPushInQ: [Any]?
    public var adminEarning: Float?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        bookingDate = json[SerializationKeys.bookingDate].string
        driverReachedTime = json[SerializationKeys.driverReachedTime].int
        if let items = json[SerializationKeys.wayEndPoints].array { wayEndPoints = items.map { WayEndPoints(json: $0) } }
        if let items = json[SerializationKeys.bidRejectedByDriver].array { bidRejectedByDriver = items.map { $0.object} }
        driverReachedKM = json[SerializationKeys.driverReachedKM].int
        driverCancelationFee = json[SerializationKeys.driverCancelationFee].int
        if let items = json[SerializationKeys.driverDeclineInstant].array { driverDeclineInstant = items.map { $0.object} }
        localbookingDateTime = json[SerializationKeys.localbookingDateTime].string
        driverEarning = json[SerializationKeys.driverEarning].float
        schedulerId = json[SerializationKeys.schedulerId].string
        riderTaxApplied = json[SerializationKeys.riderTaxApplied].float
        paymentStatus = json[SerializationKeys.paymentStatus].string
        if let items = json[SerializationKeys.aggregateRequestPendingByDriver].array { aggregateRequestPendingByDriver = items.map { $0.object} }
        endLocation = Locations(json: json[SerializationKeys.endLocation])
        isFinalAggregate = json[SerializationKeys.isFinalAggregate].boolValue
        transId = json[SerializationKeys.transId].string
        distance = json[SerializationKeys.distance].float
        if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { $0.object} }
        timezone = json[SerializationKeys.timezone].string
        bridgeTax = json[SerializationKeys.bridgeTax].int
        startLocation = Locations(json: json[SerializationKeys.startLocation])
        kmTravel = json[SerializationKeys.kmTravel].int
        duration = json[SerializationKeys.duration].float
        driverTaxApplied = json[SerializationKeys.driverTaxApplied].float
        riderRating = json[SerializationKeys.riderRating].int
        tripType = json[SerializationKeys.tripType].string
        endAddress = json[SerializationKeys.endAddress].string
        riderCancelationFee = json[SerializationKeys.riderCancelationFee].float
        seatsRequired = json[SerializationKeys.seatsRequired].int
        createdAt = json[SerializationKeys.createdAt].string
        parentSharingTrip = json[SerializationKeys.parentSharingTrip].boolValue
        driverRating = json[SerializationKeys.driverRating].int
        cardId = json[SerializationKeys.cardId].string
        if let items = json[SerializationKeys.bidByDriver].array { bidByDriver = items.map { BidByDriverWallet(json: $0) } }
        riderId = json[SerializationKeys.riderId].string
        price = json[SerializationKeys.price].float
        aggregatePaymentStatus = json[SerializationKeys.aggregatePaymentStatus].int
        isShared = json[SerializationKeys.isShared].boolValue
        vehicleType = json[SerializationKeys.vehicleType].string
        status = json[SerializationKeys.status].int
        id = json[SerializationKeys.id].string
        startAddress = json[SerializationKeys.startAddress].string
        if let items = json[SerializationKeys.sentBookingReguestToDriver].array { sentBookingReguestToDriver = items.map { $0.object} }
        updatedAt = json[SerializationKeys.updatedAt].string
        v = json[SerializationKeys.v].int
        driverId = json[SerializationKeys.driverId].string
        if let items = json[SerializationKeys.driverPushInQ].array { driverPushInQ = items.map { $0.object} }
        adminEarning = json[SerializationKeys.adminEarning].float
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = wayEndPoints { dictionary[SerializationKeys.wayEndPoints] = value.map { $0.dictionaryRepresentation() } }
        if let value = bookingDate { dictionary[SerializationKeys.bookingDate] = value }
        if let value = driverReachedTime { dictionary[SerializationKeys.driverReachedTime] = value }
        if let value = bidRejectedByDriver { dictionary[SerializationKeys.bidRejectedByDriver] = value }
        if let value = driverReachedKM { dictionary[SerializationKeys.driverReachedKM] = value }
        if let value = driverCancelationFee { dictionary[SerializationKeys.driverCancelationFee] = value }
        if let value = driverDeclineInstant { dictionary[SerializationKeys.driverDeclineInstant] = value }
        if let value = localbookingDateTime { dictionary[SerializationKeys.localbookingDateTime] = value }
        if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
        if let value = schedulerId { dictionary[SerializationKeys.schedulerId] = value }
        if let value = riderTaxApplied { dictionary[SerializationKeys.riderTaxApplied] = value }
        if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
        if let value = aggregateRequestPendingByDriver { dictionary[SerializationKeys.aggregateRequestPendingByDriver] = value }
        if let value = endLocation { dictionary[SerializationKeys.endLocation] = value.dictionaryRepresentation() }
        dictionary[SerializationKeys.isFinalAggregate] = isFinalAggregate
        if let value = transId { dictionary[SerializationKeys.transId] = value }
        if let value = distance { dictionary[SerializationKeys.distance] = value }
        if let value = workingTime { dictionary[SerializationKeys.workingTime] = value }
        if let value = timezone { dictionary[SerializationKeys.timezone] = value }
        if let value = bridgeTax { dictionary[SerializationKeys.bridgeTax] = value }
        if let value = startLocation { dictionary[SerializationKeys.startLocation] = value.dictionaryRepresentation() }
        if let value = kmTravel { dictionary[SerializationKeys.kmTravel] = value }
        if let value = duration { dictionary[SerializationKeys.duration] = value }
        if let value = driverTaxApplied { dictionary[SerializationKeys.driverTaxApplied] = value }
        if let value = riderRating { dictionary[SerializationKeys.riderRating] = value }
        if let value = tripType { dictionary[SerializationKeys.tripType] = value }
        if let value = endAddress { dictionary[SerializationKeys.endAddress] = value }
        if let value = riderCancelationFee { dictionary[SerializationKeys.riderCancelationFee] = value }
        if let value = seatsRequired { dictionary[SerializationKeys.seatsRequired] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        dictionary[SerializationKeys.parentSharingTrip] = parentSharingTrip
        if let value = driverRating { dictionary[SerializationKeys.driverRating] = value }
        if let value = cardId { dictionary[SerializationKeys.cardId] = value }
        if let value = bidByDriver { dictionary[SerializationKeys.bidByDriver] = value.map { $0.dictionaryRepresentation() } }
        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
        if let value = price { dictionary[SerializationKeys.price] = value }
        if let value = aggregatePaymentStatus { dictionary[SerializationKeys.aggregatePaymentStatus] = value }
        dictionary[SerializationKeys.isShared] = isShared
        if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = startAddress { dictionary[SerializationKeys.startAddress] = value }
        if let value = sentBookingReguestToDriver { dictionary[SerializationKeys.sentBookingReguestToDriver] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = driverId { dictionary[SerializationKeys.driverId] = value }
        if let value = driverPushInQ { dictionary[SerializationKeys.driverPushInQ] = value }
        if let value = adminEarning { dictionary[SerializationKeys.adminEarning] = value }
        return dictionary
    }
    
}

public final class Payload {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let skip = "skip"
        static let timezone = "timezone"
        static let limit = "limit"
    }
    
    // MARK: Properties
    public var skip: Int?
    public var timezone: String?
    public var limit: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        skip = json[SerializationKeys.skip].int
        timezone = json[SerializationKeys.timezone].string
        limit = json[SerializationKeys.limit].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = skip { dictionary[SerializationKeys.skip] = value }
        if let value = timezone { dictionary[SerializationKeys.timezone] = value }
        if let value = limit { dictionary[SerializationKeys.limit] = value }
        return dictionary
    }
    
}

public final class BidByDriverWallet {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let offerPrice = "offerPrice"
        static let driveId = "driveId"
        static let id = "_id"
        static let offerPriceByDriver = "offerPriceByDriver"
    }
    
    // MARK: Properties
    public var offerPrice: Int?
    public var driveId: String?
    public var id: String?
    public var offerPriceByDriver: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        offerPrice = json[SerializationKeys.offerPrice].int
        driveId = json[SerializationKeys.driveId].string
        id = json[SerializationKeys.id].string
        offerPriceByDriver = json[SerializationKeys.offerPriceByDriver].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = offerPrice { dictionary[SerializationKeys.offerPrice] = value }
        if let value = driveId { dictionary[SerializationKeys.driveId] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = offerPriceByDriver { dictionary[SerializationKeys.offerPriceByDriver] = value }
        return dictionary
    }
    
}

public final class WayEndPoints {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
        static let id = "_id"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    public var id: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
        id = json[SerializationKeys.id].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        return dictionary
    }
    
}

