//
//  MapViewHandler.swift
//  Witz
//
//  Created by Amit Tripathi on 9/26/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
//import NMAKit

//MARK:: MapView delegate
@objc protocol MapViewHandlerDelegate:NSObjectProtocol {
//    func mapObjects(map: NMAMapView, objects:[NMAMapObject])
//    @objc optional func mapStartDragging(map: NMAMapView)
//    @objc optional func mapStopsDragging(map: NMAMapView)
}
//class MapViewHandler:NSObject, NMAMapViewDelegate {
//    
//    var map: NMAMapView?
//    weak var delegate:MapViewHandlerDelegate!
//    static let sharedInstance = MapViewHandler()
//    private override init() {} //This prevents others from using the default '()' initializer for this class.
//    
//    func mapViewDidSelectObjects(_ mapView: NMAMapView, objects: [NMAMapObject]) {
//        if self.delegate != nil {
//            self.delegate.mapObjects(map: mapView, objects: objects)
//        }
//    }
//    
//    func mapViewDidBeginMovement(_ mapView: NMAMapView) {
//        print("\(#function) -- \(self)")
////        if self.delegate != nil {
////            self.delegate.mapStartDragging!(map: mapView)
////        }
//    }
//    
//    func mapViewDidEndMovement(_ mapView: NMAMapView) {
////        if self.delegate != nil {
////            self.delegate.mapStopsDragging!(map: mapView)
////        }
//    }
//}

