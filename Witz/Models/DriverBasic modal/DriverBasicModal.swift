//
//  DriverBasicModal.swift
//  Witz
//
//  Created by abhishek kumar on 22/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

public final class DriverBasicModal {
    var name : String?
    var image : String?
    var rating : String?
    var vehicleName : String?
    var vehicleNumber : String?
    var km : String?
    var min : String?
     var mobileNumber : String?
    
    public required init(name:String ,image:String,km:String ,min:String, rating:String, vehicleName:String,vehicleNumber:String,mobileNumber:String) {
        self.name  = name
        self.image = image
        self.rating = rating
        self.vehicleName = vehicleName
        self.vehicleNumber = vehicleNumber
        self.km = km
        self.min = min
        self.mobileNumber = mobileNumber
    }
    
}


