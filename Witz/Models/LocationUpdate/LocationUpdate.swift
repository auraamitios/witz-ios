//
//  LocationUpdate.swift
//  Witz
//
//  Created by Amit Tripathi on 10/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationDelegates:NSObjectProtocol {
    func authorizationStatus(status: CLAuthorizationStatus)
    func didUpdateLocation(locations: [CLLocation])
    func didFailToUpdate(error:Error)
}
typealias CompletionHandler = (_ success:Bool) -> Void
class LocationUpdate: NSObject {

    static let sharedInstance = LocationUpdate()
    private override init() {} //This prevents others from using the default '()' initializer for this class.
    let locManager = CLLocationManager()
    weak var delegate:LocationDelegates!

     public func startTracking() {
//        locManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.Location.Denied, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: { (action:UIAlertAction!) -> Void in
                
            })
        return
        }
        fetchLocation()
//        switch(CLLocationManager.authorizationStatus()) {
//        case .notDetermined, .restricted, .denied:
//            print(">>>>>>>>>>>>>No access")
//        case .authorizedAlways, .authorizedWhenInUse:
//            fetchLocation()
//        }
    }
    
    public func grantLocationPermission() {
        locManager.requestAlwaysAuthorization()
        
    }
     public func fetchLocation() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locManager.distanceFilter = kCLDistanceFilterNone
        //locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        locManager.requestLocation()
    }
    
    public func stopTracking() {
        locManager.stopUpdatingLocation()
    }
    
    func initiateTracking () {
        grantLocationPermission() { success in
            self.startTracking()
            print(success)
        }
    }
    
    func grantLocationPermission(completionHandler:@escaping (Bool) -> ()) {
        locManager.requestAlwaysAuthorization()
        completionHandler(true)
    }
}

extension LocationUpdate:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if self.delegate != nil {
            self.delegate.authorizationStatus(status: status)
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        AppManager.sharedInstance.locationCoordinates = locations
        if self.delegate != nil {
            self.delegate.didUpdateLocation(locations: locations)
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if self.delegate != nil {
            self.delegate.didFailToUpdate(error: error)
        }
    }
}
