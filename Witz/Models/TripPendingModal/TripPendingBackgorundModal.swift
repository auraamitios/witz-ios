//
//  TripPendingBackgorundModal.swift
//
//  Created by abhishek kumar on 06/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TripPendingBackgorundModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let riderData = "riderData"
    static let isSharedTrip = "isSharedTrip"
    static let tripData = "tripData"
    static let tripStatus = "tripStatus"
  }

  // MARK: Properties
  public var riderData: RiderData?
  public var isSharedTrip: Bool? = false
  public var tripData: TripData?
  public var tripStatus: Bool? = false

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    riderData = RiderData(json: json[SerializationKeys.riderData])
    isSharedTrip = json[SerializationKeys.isSharedTrip].boolValue
    tripData = TripData(json: json[SerializationKeys.tripData])
    tripStatus = json[SerializationKeys.tripStatus].boolValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = riderData { dictionary[SerializationKeys.riderData] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.isSharedTrip] = isSharedTrip
    if let value = tripData { dictionary[SerializationKeys.tripData] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.tripStatus] = tripStatus
    return dictionary
  }

}


//public final class TripData_BackgroundModal {
//
//    // MARK: Declaration for string constants to be used to decode and also serialize.
//    private struct SerializationKeys {
//        static let bidRejectedByDriver = "bidRejectedByDriver"
//        static let driverReachedKM = "driverReachedKM"
//        static let driverDeclineInstant = "driverDeclineInstant"
//        static let localbookingDateTime = "localbookingDateTime"
//        static let driverEarning = "driverEarning"
//        static let schedulerId = "schedulerId"
//        static let paymentStatus = "paymentStatus"
//        static let endLocation = "endLocation"
//        static let isFinalAggregate = "isFinalAggregate"
//        static let timezone = "timezone"
//        static let workingTime = "workingTime"
//        static let kmTravel = "kmTravel"
//        static let riderRating = "riderRating"
//        static let endAddress = "end_address"
//        static let riderCancelationFee = "riderCancelationFee"
//        static let seatsRequired = "seatsRequired"
//        static let parentSharingTrip = "parentSharingTrip"
//        static let driverRating = "driverRating"
//        static let cardId = "cardId"
//        static let bidByDriver = "bidByDriver"
//        static let price = "price"
//        static let vehicleType = "vehicleType"
//        static let sentBookingReguestToDriver = "sentBookingReguestToDriver"
//        static let updatedAt = "updatedAt"
//        static let timezoneString = "timezoneString"
//        static let driverPushInQ = "driverPushInQ"
//        static let bookingDate = "bookingDate"
//        static let driverReachedTime = "driverReachedTime"
//        static let driverCancelationFee = "driverCancelationFee"
//        static let riderTaxApplied = "riderTaxApplied"
//        static let aggregateRequestPendingByDriver = "aggregateRequestPendingByDriver"
//        static let tripRefundAmount = "tripRefundAmount"
//        static let distance = "distance"
//        static let walletPayment = "walletPayment"
//        static let bridgeTax = "bridgeTax"
//        static let startLocation = "startLocation"
//        static let duration = "duration"
//        static let driverTaxApplied = "driverTaxApplied"
//        static let tripType = "tripType"
//        static let createdAt = "createdAt"
//        static let aggregatePaymentStatus = "aggregatePaymentStatus"
//        static let isShared = "isShared"
//        static let riderId = "riderId"
//        static let status = "status"
//        static let id = "_id"
//        static let startAddress = "start_address"
//        static let v = "__v"
//        static let driverId = "driverId"
//        static let vehicleInformation = "vehicle_information"
//        static let adminEarning = "adminEarning"
//    }
//
//    // MARK: Properties
//    public var bidRejectedByDriver: [Any]?
//    public var driverReachedKM: Int?
//    public var driverDeclineInstant: [Any]?
//    public var localbookingDateTime: String?
//    public var driverEarning: Float?
//    public var schedulerId: String?
//    public var paymentStatus: String?
//    public var endLocation: Locations?
//    public var isFinalAggregate: Bool? = false
//    public var timezone: String?
//    public var workingTime: [Any]?
//    public var kmTravel: Int?
//    public var riderRating: Int?
//    public var endAddress: String?
//    public var riderCancelationFee: Int?
//    public var seatsRequired: Int?
//    public var parentSharingTrip: Bool? = false
//    public var driverRating: Int?
//    public var cardId: String?
//    public var bidByDriver: [Any]?
//    public var price: Float?
//    public var vehicleType: String?
//    public var sentBookingReguestToDriver: [Any]?
//    public var updatedAt: String?
//    public var timezoneString: String?
//    public var driverPushInQ: [String]?
//    public var bookingDate: String?
//    public var driverReachedTime: Int?
//    public var driverCancelationFee: Int?
//    public var riderTaxApplied: Float?
//    public var aggregateRequestPendingByDriver: [Any]?
//    public var tripRefundAmount: Int?
//    public var distance: Float?
//    public var walletPayment: Int?
//    public var bridgeTax: Int?
//    public var startLocation: Locations?
//    public var duration: Float?
//    public var driverTaxApplied: Float?
//    public var tripType: String?
//    public var createdAt: String?
//    public var aggregatePaymentStatus: Int?
//    public var isShared: Bool? = false
//    public var riderId: String?
//    public var status: Int?
//    public var id: String?
//    public var startAddress: String?
//    public var v: Int?
//    public var driverId: String?
//    public var vehicleInformation: VehicleInformation?
//    public var adminEarning: Float?
//
//    // MARK: SwiftyJSON Initializers
//    /// Initiates the instance based on the object.
//    ///
//    /// - parameter object: The object of either Dictionary or Array kind that was passed.
//    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//
//    /// Initiates the instance based on the JSON that was passed.
//    ///
//    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        if let items = json[SerializationKeys.bidRejectedByDriver].array { bidRejectedByDriver = items.map { $0.object} }
//        driverReachedKM = json[SerializationKeys.driverReachedKM].int
//        if let items = json[SerializationKeys.driverDeclineInstant].array { driverDeclineInstant = items.map { $0.object} }
//        localbookingDateTime = json[SerializationKeys.localbookingDateTime].string
//        driverEarning = json[SerializationKeys.driverEarning].float
//        schedulerId = json[SerializationKeys.schedulerId].string
//        paymentStatus = json[SerializationKeys.paymentStatus].string
//        endLocation = Locations(json: json[SerializationKeys.endLocation])
//        isFinalAggregate = json[SerializationKeys.isFinalAggregate].boolValue
//        timezone = json[SerializationKeys.timezone].string
//        if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { $0.object} }
//        kmTravel = json[SerializationKeys.kmTravel].int
//        riderRating = json[SerializationKeys.riderRating].int
//        endAddress = json[SerializationKeys.endAddress].string
//        riderCancelationFee = json[SerializationKeys.riderCancelationFee].int
//        seatsRequired = json[SerializationKeys.seatsRequired].int
//        parentSharingTrip = json[SerializationKeys.parentSharingTrip].boolValue
//        driverRating = json[SerializationKeys.driverRating].int
//        cardId = json[SerializationKeys.cardId].string
//        if let items = json[SerializationKeys.bidByDriver].array { bidByDriver = items.map { $0.object} }
//        price = json[SerializationKeys.price].float
//        vehicleType = json[SerializationKeys.vehicleType].string
//        if let items = json[SerializationKeys.sentBookingReguestToDriver].array { sentBookingReguestToDriver = items.map { $0.object} }
//        updatedAt = json[SerializationKeys.updatedAt].string
//        timezoneString = json[SerializationKeys.timezoneString].string
//        if let items = json[SerializationKeys.driverPushInQ].array { driverPushInQ = items.map { $0.stringValue } }
//        bookingDate = json[SerializationKeys.bookingDate].string
//        driverReachedTime = json[SerializationKeys.driverReachedTime].int
//        driverCancelationFee = json[SerializationKeys.driverCancelationFee].int
//        riderTaxApplied = json[SerializationKeys.riderTaxApplied].float
//        if let items = json[SerializationKeys.aggregateRequestPendingByDriver].array { aggregateRequestPendingByDriver = items.map { $0.object} }
//        tripRefundAmount = json[SerializationKeys.tripRefundAmount].int
//        distance = json[SerializationKeys.distance].float
//        walletPayment = json[SerializationKeys.walletPayment].int
//        bridgeTax = json[SerializationKeys.bridgeTax].int
//        startLocation = Locations(json: json[SerializationKeys.startLocation])
//        duration = json[SerializationKeys.duration].float
//        driverTaxApplied = json[SerializationKeys.driverTaxApplied].float
//        tripType = json[SerializationKeys.tripType].string
//        createdAt = json[SerializationKeys.createdAt].string
//        aggregatePaymentStatus = json[SerializationKeys.aggregatePaymentStatus].int
//        isShared = json[SerializationKeys.isShared].boolValue
//        riderId = json[SerializationKeys.riderId].string
//        status = json[SerializationKeys.status].int
//        id = json[SerializationKeys.id].string
//        startAddress = json[SerializationKeys.startAddress].string
//        v = json[SerializationKeys.v].int
//        driverId = json[SerializationKeys.driverId].string
//        vehicleInformation = VehicleInformation(json: json[SerializationKeys.vehicleInformation])
//        adminEarning = json[SerializationKeys.adminEarning].float
//    }
//
//    /// Generates description of the object in the form of a NSDictionary.
//    ///
//    /// - returns: A Key value pair containing all valid values in the object.
//    public func dictionaryRepresentation() -> [String: Any] {
//        var dictionary: [String: Any] = [:]
//        if let value = bidRejectedByDriver { dictionary[SerializationKeys.bidRejectedByDriver] = value }
//        if let value = driverReachedKM { dictionary[SerializationKeys.driverReachedKM] = value }
//        if let value = driverDeclineInstant { dictionary[SerializationKeys.driverDeclineInstant] = value }
//        if let value = localbookingDateTime { dictionary[SerializationKeys.localbookingDateTime] = value }
//        if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
//        if let value = schedulerId { dictionary[SerializationKeys.schedulerId] = value }
//        if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
//        if let value = endLocation { dictionary[SerializationKeys.endLocation] = value.dictionaryRepresentation() }
//        dictionary[SerializationKeys.isFinalAggregate] = isFinalAggregate
//        if let value = timezone { dictionary[SerializationKeys.timezone] = value }
//        if let value = workingTime { dictionary[SerializationKeys.workingTime] = value }
//        if let value = kmTravel { dictionary[SerializationKeys.kmTravel] = value }
//        if let value = riderRating { dictionary[SerializationKeys.riderRating] = value }
//        if let value = endAddress { dictionary[SerializationKeys.endAddress] = value }
//        if let value = riderCancelationFee { dictionary[SerializationKeys.riderCancelationFee] = value }
//        if let value = seatsRequired { dictionary[SerializationKeys.seatsRequired] = value }
//        dictionary[SerializationKeys.parentSharingTrip] = parentSharingTrip
//        if let value = driverRating { dictionary[SerializationKeys.driverRating] = value }
//        if let value = cardId { dictionary[SerializationKeys.cardId] = value }
//        if let value = bidByDriver { dictionary[SerializationKeys.bidByDriver] = value }
//        if let value = price { dictionary[SerializationKeys.price] = value }
//        if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
//        if let value = sentBookingReguestToDriver { dictionary[SerializationKeys.sentBookingReguestToDriver] = value }
//        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
//        if let value = timezoneString { dictionary[SerializationKeys.timezoneString] = value }
//        if let value = driverPushInQ { dictionary[SerializationKeys.driverPushInQ] = value }
//        if let value = bookingDate { dictionary[SerializationKeys.bookingDate] = value }
//        if let value = driverReachedTime { dictionary[SerializationKeys.driverReachedTime] = value }
//        if let value = driverCancelationFee { dictionary[SerializationKeys.driverCancelationFee] = value }
//        if let value = riderTaxApplied { dictionary[SerializationKeys.riderTaxApplied] = value }
//        if let value = aggregateRequestPendingByDriver { dictionary[SerializationKeys.aggregateRequestPendingByDriver] = value }
//        if let value = tripRefundAmount { dictionary[SerializationKeys.tripRefundAmount] = value }
//        if let value = distance { dictionary[SerializationKeys.distance] = value }
//        if let value = walletPayment { dictionary[SerializationKeys.walletPayment] = value }
//        if let value = bridgeTax { dictionary[SerializationKeys.bridgeTax] = value }
//        if let value = startLocation { dictionary[SerializationKeys.startLocation] = value.dictionaryRepresentation() }
//        if let value = duration { dictionary[SerializationKeys.duration] = value }
//        if let value = driverTaxApplied { dictionary[SerializationKeys.driverTaxApplied] = value }
//        if let value = tripType { dictionary[SerializationKeys.tripType] = value }
//        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
//        if let value = aggregatePaymentStatus { dictionary[SerializationKeys.aggregatePaymentStatus] = value }
//        dictionary[SerializationKeys.isShared] = isShared
//        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
//        if let value = status { dictionary[SerializationKeys.status] = value }
//        if let value = id { dictionary[SerializationKeys.id] = value }
//        if let value = startAddress { dictionary[SerializationKeys.startAddress] = value }
//        if let value = v { dictionary[SerializationKeys.v] = value }
//        if let value = driverId { dictionary[SerializationKeys.driverId] = value }
//        if let value = vehicleInformation { dictionary[SerializationKeys.vehicleInformation] = value.dictionaryRepresentation() }
//        if let value = adminEarning { dictionary[SerializationKeys.adminEarning] = value }
//        return dictionary
//    }
//
//}
//public final class RiderData_BackgroundModal {
//
//    // MARK: Declaration for string constants to be used to decode and also serialize.
//    private struct SerializationKeys {
//        static let totalNumberRefferal = "totalNumberRefferal"
//        static let isBlocked = "isBlocked"
//        static let arrivalTime = "arrivalTime"
//        static let riderStatus = "riderStatus"
//        static let avgRating = "avgRating"
//        static let workAddress = "workAddress"
//        static let totalRefferalAmount = "totalRefferalAmount"
//        static let homeAddress = "homeAddress"
//        static let isVerified = "isVerified"
//        static let deviceToken = "deviceToken"
//        static let totalCredits = "totalCredits"
//        static let fullName = "fullName"
//        static let password = "password"
//        static let lan = "lan"
//        static let email = "email"
//        static let deviceType = "deviceType"
//        static let createdAt = "createdAt"
//        static let accessToken = "accessToken"
//        static let mobile = "mobile"
//        static let pic = "pic"
//        static let promotionTotalCredits = "promotionTotalCredits"
//        static let promotioncode = "promotioncode"
//        static let id = "_id"
//        static let isDeleted = "isDeleted"
//        static let currentLocation = "currentLocation"
//        static let v = "__v"
//        static let updatedAt = "updatedAt"
//        static let homeLocation = "homeLocation"
//        static let badgeCount = "badgeCount"
//        static let refferalCode = "refferalCode"
//        static let workLocation = "workLocation"
//    }
//
//    // MARK: Properties
//    public var totalNumberRefferal: Int?
//    public var isBlocked: Bool? = false
//    public var arrivalTime: Int?
//    public var riderStatus: String?
//    public var avgRating: Int?
//    public var workAddress: String?
//    public var totalRefferalAmount: Int?
//    public var homeAddress: String?
//    public var isVerified: Bool? = false
//    public var deviceToken: String?
//    public var totalCredits: Int?
//    public var fullName: String?
//    public var password: String?
//    public var lan: String?
//    public var email: String?
//    public var deviceType: String?
//    public var createdAt: String?
//    public var accessToken: String?
//    public var mobile: String?
//    public var pic: Pic?
//    public var promotionTotalCredits: Int?
//    public var promotioncode: String?
//    public var id: String?
//    public var isDeleted: Bool? = false
//    public var currentLocation: Locations?
//    public var v: Int?
//    public var updatedAt: String?
//    public var homeLocation: Locations?
//    public var badgeCount: Int?
//    public var refferalCode: String?
//    public var workLocation: Locations?
//
//    // MARK: SwiftyJSON Initializers
//    /// Initiates the instance based on the object.
//    ///
//    /// - parameter object: The object of either Dictionary or Array kind that was passed.
//    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//
//    /// Initiates the instance based on the JSON that was passed.
//    ///
//    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        totalNumberRefferal = json[SerializationKeys.totalNumberRefferal].int
//        isBlocked = json[SerializationKeys.isBlocked].boolValue
//        arrivalTime = json[SerializationKeys.arrivalTime].int
//        riderStatus = json[SerializationKeys.riderStatus].string
//        avgRating = json[SerializationKeys.avgRating].int
//        workAddress = json[SerializationKeys.workAddress].string
//        totalRefferalAmount = json[SerializationKeys.totalRefferalAmount].int
//        homeAddress = json[SerializationKeys.homeAddress].string
//        isVerified = json[SerializationKeys.isVerified].boolValue
//        deviceToken = json[SerializationKeys.deviceToken].string
//        totalCredits = json[SerializationKeys.totalCredits].int
//        fullName = json[SerializationKeys.fullName].string
//        password = json[SerializationKeys.password].string
//        lan = json[SerializationKeys.lan].string
//        email = json[SerializationKeys.email].string
//        deviceType = json[SerializationKeys.deviceType].string
//        createdAt = json[SerializationKeys.createdAt].string
//        accessToken = json[SerializationKeys.accessToken].string
//        mobile = json[SerializationKeys.mobile].string
//        pic = Pic(json: json[SerializationKeys.pic])
//        promotionTotalCredits = json[SerializationKeys.promotionTotalCredits].int
//        promotioncode = json[SerializationKeys.promotioncode].string
//        id = json[SerializationKeys.id].string
//        isDeleted = json[SerializationKeys.isDeleted].boolValue
//        currentLocation = Locations(json: json[SerializationKeys.currentLocation])
//        v = json[SerializationKeys.v].int
//        updatedAt = json[SerializationKeys.updatedAt].string
//        homeLocation = Locations(json: json[SerializationKeys.homeLocation])
//        badgeCount = json[SerializationKeys.badgeCount].int
//        refferalCode = json[SerializationKeys.refferalCode].string
//        workLocation = Locations(json: json[SerializationKeys.workLocation])
//    }
//
//    /// Generates description of the object in the form of a NSDictionary.
//    ///
//    /// - returns: A Key value pair containing all valid values in the object.
//    public func dictionaryRepresentation() -> [String: Any] {
//        var dictionary: [String: Any] = [:]
//        if let value = totalNumberRefferal { dictionary[SerializationKeys.totalNumberRefferal] = value }
//        dictionary[SerializationKeys.isBlocked] = isBlocked
//        if let value = arrivalTime { dictionary[SerializationKeys.arrivalTime] = value }
//        if let value = riderStatus { dictionary[SerializationKeys.riderStatus] = value }
//        if let value = avgRating { dictionary[SerializationKeys.avgRating] = value }
//        if let value = workAddress { dictionary[SerializationKeys.workAddress] = value }
//        if let value = totalRefferalAmount { dictionary[SerializationKeys.totalRefferalAmount] = value }
//        if let value = homeAddress { dictionary[SerializationKeys.homeAddress] = value }
//        dictionary[SerializationKeys.isVerified] = isVerified
//        if let value = deviceToken { dictionary[SerializationKeys.deviceToken] = value }
//        if let value = totalCredits { dictionary[SerializationKeys.totalCredits] = value }
//        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
//        if let value = password { dictionary[SerializationKeys.password] = value }
//        if let value = lan { dictionary[SerializationKeys.lan] = value }
//        if let value = email { dictionary[SerializationKeys.email] = value }
//        if let value = deviceType { dictionary[SerializationKeys.deviceType] = value }
//        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
//        if let value = accessToken { dictionary[SerializationKeys.accessToken] = value }
//        if let value = mobile { dictionary[SerializationKeys.mobile] = value }
//        if let value = pic { dictionary[SerializationKeys.pic] = value.dictionaryRepresentation() }
//        if let value = promotionTotalCredits { dictionary[SerializationKeys.promotionTotalCredits] = value }
//        if let value = promotioncode { dictionary[SerializationKeys.promotioncode] = value }
//        if let value = id { dictionary[SerializationKeys.id] = value }
//        dictionary[SerializationKeys.isDeleted] = isDeleted
//        if let value = currentLocation { dictionary[SerializationKeys.currentLocation] = value.dictionaryRepresentation() }
//        if let value = v { dictionary[SerializationKeys.v] = value }
//        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
//        if let value = homeLocation { dictionary[SerializationKeys.homeLocation] = value.dictionaryRepresentation() }
//        if let value = badgeCount { dictionary[SerializationKeys.badgeCount] = value }
//        if let value = refferalCode { dictionary[SerializationKeys.refferalCode] = value }
//        if let value = workLocation { dictionary[SerializationKeys.workLocation] = value.dictionaryRepresentation() }
//        return dictionary
//    }
//
//}
//public final class Locations {
//    
//    // MARK: Declaration for string constants to be used to decode and also serialize.
//    private struct SerializationKeys {
//        static let type = "type"
//        static let coordinates = "coordinates"
//    }
//    
//    // MARK: Properties
//    public var type: String?
//    public var coordinates: [Float]?
//    
//    // MARK: SwiftyJSON Initializers
//    /// Initiates the instance based on the object.
//    ///
//    /// - parameter object: The object of either Dictionary or Array kind that was passed.
//    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//    
//    /// Initiates the instance based on the JSON that was passed.
//    ///
//    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        type = json[SerializationKeys.type].string
//        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
//    }
//    
//    /// Generates description of the object in the form of a NSDictionary.
//    ///
//    /// - returns: A Key value pair containing all valid values in the object.
//    public func dictionaryRepresentation() -> [String: Any] {
//        var dictionary: [String: Any] = [:]
//        if let value = type { dictionary[SerializationKeys.type] = value }
//        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
//        return dictionary
//    }
//    
//}
public final class VehicleInformation {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let color = "color"
        static let subBrand = "subBrand"
        static let year = "year"
        static let model = "model"
        static let vehicleNumber = "vehicle_number"
        static let seatingCapacity = "seating_capacity"
        static let isVerified = "isVerified"
        static let brand = "brand"
    }
    
    // MARK: Properties
    public var color: String?
    public var subBrand: String?
    public var year: String?
    public var model: String?
    public var vehicleNumber: String?
    public var seatingCapacity: String?
    public var isVerified: Bool? = false
    public var brand: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        color = json[SerializationKeys.color].string
        subBrand = json[SerializationKeys.subBrand].string
        year = json[SerializationKeys.year].string
        model = json[SerializationKeys.model].string
        vehicleNumber = json[SerializationKeys.vehicleNumber].string
        seatingCapacity = json[SerializationKeys.seatingCapacity].string
        isVerified = json[SerializationKeys.isVerified].boolValue
        brand = json[SerializationKeys.brand].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = color { dictionary[SerializationKeys.color] = value }
        if let value = subBrand { dictionary[SerializationKeys.subBrand] = value }
        if let value = year { dictionary[SerializationKeys.year] = value }
        if let value = model { dictionary[SerializationKeys.model] = value }
        if let value = vehicleNumber { dictionary[SerializationKeys.vehicleNumber] = value }
        if let value = seatingCapacity { dictionary[SerializationKeys.seatingCapacity] = value }
        dictionary[SerializationKeys.isVerified] = isVerified
        if let value = brand { dictionary[SerializationKeys.brand] = value }
        return dictionary
    }
    
}
