//
//  RiderInfo.swift
//  Witz
//
//  Created by Amit Tripathi on 10/16/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct RiderInfo {
    
    var version:Double?
    var rider_Id:String?
    var accessToken:String?
    
    var arrivalTime:Double?
    var avgRating:Double?
    var badgeCount:Double?
    var deviceToken:String?
    
    var deviceType:Double?
    var fullName:String?
    var isBlocked:Bool?
    var email:String?
    
    var isDeleted:Bool?
    var isVerified:Bool?
    var mobile:String?
    var password:String?
    
    var refferalCode:String?
    var status:String?
    var totalCredits:Double?
    
    var totalNumberRefferal:Double?
    var totalRefferalAmount:Double?
    
    var homeAddress:String?
    var workAddress:String?
    var promotionTotalCredits:Double?
    var promotioncode:String?
    
    var cards:[JSON]?
    
    //*********
    
    //currentLocation
    var currentLocation:[String:AnyObject]?
    
    //HomeLocation
    var homeLocation:[String:AnyObject]?
    
    //WorkLocation
    var workLocation:[String:AnyObject]?
    
    //CurrentTrip data
    var currentTripData:[String:AnyObject]?
    
    //pic
    var picDetail:[String:AnyObject]?
    var original:String?
    var thumb:String?
    
    static func parseRiderInfo(data:[String:JSON], outerData:JSON)->RiderInfo{
        
        var riderInfo = RiderInfo()
        riderInfo.version = data["__v"]?.double
        riderInfo.rider_Id = data["_id"]?.string
        riderInfo.accessToken = data["accessToken"]?.string
        riderInfo.arrivalTime = data["arrivalTime"]?.double
        riderInfo.avgRating = data["avgRating"]?.double
        riderInfo.badgeCount = data["badgeCount"]?.double
        riderInfo.deviceToken = data["deviceToken"]?.string
        riderInfo.deviceType = data["deviceType"]?.double
        riderInfo.fullName = data["fullName"]?.string
        riderInfo.isBlocked = data["isBlocked"]?.bool
        riderInfo.isDeleted = data["isDeleted"]?.bool
        riderInfo.isVerified = data["isVerified"]?.bool
        riderInfo.mobile = data["mobile"]?.string
        riderInfo.password = data["password"]?.string
        riderInfo.refferalCode = data["refferalCode"]?.string
        riderInfo.status = data["status"]?.string
        riderInfo.totalCredits = data["totalCredits"]?.double
        riderInfo.totalNumberRefferal = data["totalNumberRefferal"]?.double
        riderInfo.totalRefferalAmount = data["totalRefferalAmount"]?.double
        riderInfo.promotioncode = data["promotioncode"]?.string
        riderInfo.promotionTotalCredits = data["promotionTotalCredits"]?.double
        riderInfo.email = data["email"]?.string
        UserDefaultManager.sharedManager.addValue(object: riderInfo.accessToken! as AnyObject, key: LinkStrings.UserDefaultKeys.RiderAccessToken)
        
        //pic detail
        let dict1 = data["pic"]?.dictionaryValue
        riderInfo.original = dict1!["original"]?.string
        riderInfo.thumb = dict1!["thumb"]?.string
        riderInfo.picDetail = ["original":riderInfo.original as AnyObject, "thumb":riderInfo.thumb as AnyObject,]
        print("dict value************\(String(describing: riderInfo.picDetail))")
        
        //current location info
        let dict2 = data["currentLocation"]?.dictionaryValue
        let point2 = dict2!["type"]?.stringValue
        let locArry = dict2!["coordinates"]?.arrayValue
        riderInfo.currentLocation = ["type":point2 as AnyObject, "coordinates":locArry as AnyObject]
        print("dict value************\(String(describing: riderInfo.currentLocation))")
        
        //home location info
        let dict3 = data["homeLocation"]?.dictionaryValue
        let point3 = dict3!["type"]?.stringValue
        let locArry2 = dict3!["coordinates"]?.arrayValue
        riderInfo.homeLocation = ["type":point3 as AnyObject, "coordinates":locArry2 as AnyObject]
        print("dict value************\(String(describing: riderInfo.homeLocation))")
        
        //work location info
        let dict4 = data["homeLocation"]?.dictionaryValue
        let point4 = dict4!["type"]?.stringValue
        let locArry3 = dict4!["coordinates"]?.arrayValue
        riderInfo.workLocation = ["type":point4 as AnyObject, "coordinates":locArry3 as AnyObject]
        print("dict value************\(String(describing: riderInfo.homeLocation))")
        
        //Trip data info
        let dict5 = data["currentTripData"]?.dictionaryValue
        let tripData = dict5!["tripData"]?.dictionaryValue as [String : AnyObject]?
        let tripStatus = dict5!["tripStatus"]?.doubleValue
        riderInfo.currentTripData = ["tripStatus":tripStatus as AnyObject, "tripData": tripData as AnyObject]
        
        //Cards
        riderInfo.cards = data["cards"]?.arrayValue
        return riderInfo
        
    }
    
    static func parseRiderInfoJson(data1: JSON?)->RiderInfo{
        let data = data1?["data"]
        var riderInfo = RiderInfo()
        riderInfo.version = data?["__v"].double
        riderInfo.rider_Id = data?["_id"].string
        riderInfo.accessToken = data?["accessToken"].string
        riderInfo.arrivalTime = data?["arrivalTime"].double
        riderInfo.avgRating = data?["avgRating"].double
        riderInfo.badgeCount = data?["badgeCount"].double
        riderInfo.deviceToken = data?["deviceToken"].string
        riderInfo.deviceType = data?["deviceType"].double
        riderInfo.fullName = data?["fullName"].string
        riderInfo.isBlocked = data?["isBlocked"].bool
        riderInfo.isDeleted = data?["isDeleted"].bool
        riderInfo.isVerified = data?["isVerified"].bool
        riderInfo.mobile = data?["mobile"].string
        riderInfo.password = data?["password"].string
        riderInfo.refferalCode = data?["refferalCode"].string
        riderInfo.status = data?["status"].string
        riderInfo.totalCredits = data?["totalCredits"].double
        riderInfo.totalNumberRefferal = data?["totalNumberRefferal"].double
        riderInfo.totalRefferalAmount = data?["totalRefferalAmount"].double
        riderInfo.promotioncode = data?["promotioncode"].string
        riderInfo.promotionTotalCredits = data?["promotionTotalCredits"].double
        riderInfo.email = data?["email"].string
        
        UserDefaultManager.sharedManager.addValue(object: riderInfo.accessToken! as AnyObject, key: LinkStrings.UserDefaultKeys.RiderAccessToken)
        
        //pic detail
        let dict1 = data?["pic"].dictionaryValue
        riderInfo.original = dict1!["original"]?.string
        riderInfo.thumb = dict1!["thumb"]?.string
        riderInfo.picDetail = ["original":riderInfo.original as AnyObject, "thumb":riderInfo.thumb as AnyObject,]
        print("dict value************\(String(describing: riderInfo.picDetail))")
        
        //current location info
        let dict2 = data?["currentLocation"].dictionaryValue
        let point2 = dict2!["type"]?.stringValue
        let locArry = dict2!["coordinates"]?.arrayValue
        riderInfo.currentLocation = ["type":point2 as AnyObject, "coordinates":locArry as AnyObject]
        print("dict value************\(String(describing: riderInfo.currentLocation))")
        
        //home location info
        let dict3 = data?["homeLocation"].dictionaryValue
        let point3 = dict3!["type"]?.stringValue
        let locArry2 = dict3!["coordinates"]?.arrayValue
        riderInfo.homeLocation = ["type":point3 as AnyObject, "coordinates":locArry2 as AnyObject]
        print("dict value************\(String(describing: riderInfo.homeLocation))")
        
        //work location info
        let dict4 = data?["homeLocation"].dictionaryValue
        let point4 = dict4!["type"]?.stringValue
        let locArry3 = dict4!["coordinates"]?.arrayValue
        riderInfo.workLocation = ["type":point4 as AnyObject, "coordinates":locArry3 as AnyObject]
        print("dict value************\(String(describing: riderInfo.homeLocation))")
        
        //Trip data info
        let dict5 = data?["currentTripData"].dictionaryValue
        let tripData = dict5!["tripData"]?.dictionaryValue as [String : AnyObject]?
        let tripStatus = dict5!["tripStatus"]?.doubleValue
        riderInfo.currentTripData = ["tripStatus":tripStatus as AnyObject, "tripData": tripData as AnyObject]
        
        //Cards
        riderInfo.cards = data?["cards"].arrayValue
        return riderInfo
        
    }
}
