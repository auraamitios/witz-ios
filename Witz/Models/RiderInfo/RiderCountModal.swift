//
//  RiderCountModal.swift
//
//  Created by Amit Tripathi on 12/18/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class RiderCountModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let reservationFutureTripCount = "reservationFutureTripCount"
    static let reservationPendingTripCount = "reservationPendingTripCount"
    static let serviceRequestPendingTripCount = "serviceRequestPendingTripCount"
    static let serviceRequestFutureTripCount = "serviceRequestFutureTripCount"
    static let notificationCount = "notificationCount"
  }

  // MARK: Properties
  public var reservationFutureTripCount: Int?
  public var reservationPendingTripCount: Int?
  public var serviceRequestPendingTripCount: Int?
  public var serviceRequestFutureTripCount: Int?
  public var notificationCount: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    reservationFutureTripCount = json[SerializationKeys.reservationFutureTripCount].int
    reservationPendingTripCount = json[SerializationKeys.reservationPendingTripCount].int
    serviceRequestPendingTripCount = json[SerializationKeys.serviceRequestPendingTripCount].int
    serviceRequestFutureTripCount = json[SerializationKeys.serviceRequestFutureTripCount].int
    notificationCount = json[SerializationKeys.notificationCount].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = reservationFutureTripCount { dictionary[SerializationKeys.reservationFutureTripCount] = value }
    if let value = reservationPendingTripCount { dictionary[SerializationKeys.reservationPendingTripCount] = value }
    if let value = serviceRequestPendingTripCount { dictionary[SerializationKeys.serviceRequestPendingTripCount] = value }
    if let value = serviceRequestFutureTripCount { dictionary[SerializationKeys.serviceRequestFutureTripCount] = value }
    if let value = notificationCount { dictionary[SerializationKeys.notificationCount] = value }
    return dictionary
  }

}
