//
//  ServerResponse.swift
//  DealXpress
//
//  Created by Amit Tripathi on 2/24/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ServerResponse {
    
    var response_code:String?
    var response_message:String?
    var status:String?
    
    static func parseResponse(data:JSON)->ServerResponse {
        
        var response = ServerResponse()
        response.response_message = data["message"].stringValue
        response.response_code = data["response_code"].stringValue
        response.status = data["statusCode"].stringValue

        return response
    }
}
