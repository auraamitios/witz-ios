//
//  ServerError.swift
//  DealXpress
//
//  Created by Amit Tripathi on 2/23/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ServerError {
    
    var response_code:String?
    var response_message:String?

    static func parseErrorData(error:JSON)->ServerError {
        
        var apiError = ServerError()
        apiError.response_message = error.description
        return apiError
    }
}
