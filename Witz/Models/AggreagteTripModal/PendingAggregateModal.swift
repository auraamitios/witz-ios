//
//  PendingAggregateModal.swift
//
//  Created by abhishek kumar on 24/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class PendingAggregateModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let notificationDate = "notificationDate"
    static let notificationMsg = "notificationMsg"
    static let tripType = "tripType"
    static let id = "_id"
    static let isDeleted = "isDeleted"
    static let notificationUpdatedDate = "notificationUpdatedDate"
    static let groupAggregateTripId = "groupAggregateTripId"
    static let isRead = "isRead"
    static let driverId = "driverId"
    static let v = "__v"
    static let notificationCode = "notificationCode"
  }

  // MARK: Properties
  public var notificationDate: String?
  public var notificationMsg: String?
  public var tripType: String?
  public var id: String?
  public var isDeleted: Bool? = false
  public var notificationUpdatedDate: String?
  public var groupAggregateTripId: String?
  public var isRead: Bool? = false
  public var driverId: String?
  public var v: Int?
  public var notificationCode: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    notificationDate = json[SerializationKeys.notificationDate].string
    notificationMsg = json[SerializationKeys.notificationMsg].string
    tripType = json[SerializationKeys.tripType].string
    id = json[SerializationKeys.id].string
    isDeleted = json[SerializationKeys.isDeleted].boolValue
    notificationUpdatedDate = json[SerializationKeys.notificationUpdatedDate].string
    groupAggregateTripId = json[SerializationKeys.groupAggregateTripId].string
    isRead = json[SerializationKeys.isRead].boolValue
    driverId = json[SerializationKeys.driverId].string
    v = json[SerializationKeys.v].int
    notificationCode = json[SerializationKeys.notificationCode].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = notificationDate { dictionary[SerializationKeys.notificationDate] = value }
    if let value = notificationMsg { dictionary[SerializationKeys.notificationMsg] = value }
    if let value = tripType { dictionary[SerializationKeys.tripType] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    dictionary[SerializationKeys.isDeleted] = isDeleted
    if let value = notificationUpdatedDate { dictionary[SerializationKeys.notificationUpdatedDate] = value }
    if let value = groupAggregateTripId { dictionary[SerializationKeys.groupAggregateTripId] = value }
    dictionary[SerializationKeys.isRead] = isRead
    if let value = driverId { dictionary[SerializationKeys.driverId] = value }
    if let value = v { dictionary[SerializationKeys.v] = value }
    if let value = notificationCode { dictionary[SerializationKeys.notificationCode] = value }
    return dictionary
  }

}
