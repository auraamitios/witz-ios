//
//  AggregateModal2.swift
//
//  Created by abhishek kumar on 24/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AggregateModal2 {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let aggregateTripTime = "aggregateTripTime"
    static let driverEarning = "driverEarning"
    static let riderId = "riderId"
    static let aggregateFinalTripId = "aggregateFinalTripId"
    static let endLocation = "endLocation"
    static let status = "status"
    static let id = "_id"
    static let workingTime = "workingTime"
    static let aggregateTripType = "aggregateTripType"
    static let startLocation = "startLocation"
    static let adminMarkDropOffLocation = "adminMarkDropOffLocation"
    static let vehicleInformation = "vehicle_information"
    static let adminMarkPickUpLocation = "adminMarkPickUpLocation"
    static let adminEarning = "adminEarning"
  }

  // MARK: Properties
  public var aggregateTripTime: String?
  public var driverEarning: Int?
  public var riderId: RiderIdAggregate2?
  public var aggregateFinalTripId: String?
  public var endLocation: LocationsAggregate2?
  public var status: Int?
  public var id: String?
  public var workingTime: [WorkingTimeAggregate2]?
  public var aggregateTripType: Int?
  public var startLocation: LocationsAggregate2?
  public var adminMarkDropOffLocation: LocationsAggregate2?
  public var vehicleInformation: VehicleInformation?
  public var adminMarkPickUpLocation: LocationsAggregate2?
  public var adminEarning: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    aggregateTripTime = json[SerializationKeys.aggregateTripTime].string
    driverEarning = json[SerializationKeys.driverEarning].int
    riderId = RiderIdAggregate2(json: json[SerializationKeys.riderId])
    aggregateFinalTripId = json[SerializationKeys.aggregateFinalTripId].string
    endLocation = LocationsAggregate2(json: json[SerializationKeys.endLocation])
    status = json[SerializationKeys.status].int
    id = json[SerializationKeys.id].string
    if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { WorkingTimeAggregate2(json: $0) } }
    aggregateTripType = json[SerializationKeys.aggregateTripType].int
    startLocation = LocationsAggregate2(json: json[SerializationKeys.startLocation])
    adminMarkDropOffLocation = LocationsAggregate2(json: json[SerializationKeys.adminMarkDropOffLocation])
    vehicleInformation = VehicleInformation(json: json[SerializationKeys.vehicleInformation])
    adminMarkPickUpLocation = LocationsAggregate2(json: json[SerializationKeys.adminMarkPickUpLocation])
    adminEarning = json[SerializationKeys.adminEarning].int
  }
}

public final class LocationsAggregate2 {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }

}
public final class WorkingTimeAggregate2 {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let dayNumber = "dayNumber"
        static let time = "time"
        static let id = "_id"
    }
    
    // MARK: Properties
    public var dayNumber: Int?
    public var time: Time?
    public var id: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        dayNumber = json[SerializationKeys.dayNumber].int
        time = Time(json: json[SerializationKeys.time])
        id = json[SerializationKeys.id].string
    }
}

public final class PicAggregate2 {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let thumb = "thumb"
        static let original = "original"
    }
    
    // MARK: Properties
    public var thumb: String?
    public var original: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        thumb = json[SerializationKeys.thumb].string
        original = json[SerializationKeys.original].string
    }
}
public final class RiderIdAggregate2 {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let mobile = "mobile"
        static let pic = "pic"
        static let email = "email"
        static let fullName = "fullName"
        static let id = "_id"
        static let riderStatus = "riderStatus"
    }
    
    // MARK: Properties
    public var mobile: String?
    public var pic: PicAggregate2?
    public var email: String?
    public var fullName: String?
    public var id: String?
    public var riderStatus: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        mobile = json[SerializationKeys.mobile].string
        pic = PicAggregate2(json: json[SerializationKeys.pic])
        email = json[SerializationKeys.email].string
        fullName = json[SerializationKeys.fullName].string
        id = json[SerializationKeys.id].string
        riderStatus = json[SerializationKeys.riderStatus].string
    }
    
}


