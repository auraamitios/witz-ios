//
//  AgrregateTripModal.swift
//
//  Created by abhishek kumar on 09/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AgrregateTripModal {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let routeName = "routeName"
        static let driverRating = "driverRating"
        static let createdAt = "createdAt"
        static let startLocation = "startLocation"
        static let numberOfSeats = "numberOfSeats"
        static let numberOfDays = "numberOfDays"
        static let driverPic = "driverPic"
        static let endLocation = "endLocation"
        static let status = "status"
        static let aggregateStartLocation = "aggregateStartLocation"
        static let id = "_id"
        static let driverWorkingTime = "driverWorkingTime"
        static let aggregateEndLocation = "aggregateEndLocation"
        static let workingTime = "workingTime"
        static let v = "__v"
        static let wayPoints = "wayPoints"
        static let updatedAt = "updatedAt"
        static let totalDistance = "totalDistance"
        static let totalTime = "totalTime"
        static let numberOFActiveRider = "numberOFActiveRider"
        static let trips = "trips"
        static let driverName = "driverName"
        static let driverId = "driverId"
        static let driverEarning = "driverEarning"
        static let driverMobile = "driverMobile"
        static let startDate = "startDate"
        static let aggregateTripTime = "aggregateTripTime"
        static let tripDateTime = "tripDateTime"
        
    }
    
    // MARK: Properties
    public var aggregateTripType: Int? // add manually not from 
    public var routeName: String?
    public var driverRating: Int?
    public var createdAt: String?
    public var startLocation: String?
    public var numberOfSeats: Int?
    public var numberOfDays: Int?
    public var driverPic: PicAggregate?

    public var endLocation: String?
    public var status: Int?
    public var aggregateStartLocation: String?
    public var id: String?
    public var driverWorkingTime: DriverWorkingTimeAggregate?
    public var aggregateEndLocation: String?
    public var workingTime: WorkingTimeAggregate?
    public var v: Int?
    public var updatedAt: String?
    public var totalDistance: Int?
    public var totalTime: Int?
    public var numberOFActiveRider: Int?
    public var trips: [TripsAggregate]?
    public var driverName: String?
    public var driverId: String?
    public var driverEarning: Float?
    public var driverMobile: String?
    public var startDate: String?
    public var aggregateTripTime: String?
    public var tripDateTime: String?
    public var wayPoints: [WayPointsAggregate]?

    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        routeName = json[SerializationKeys.routeName].string
        driverRating = json[SerializationKeys.driverRating].int
        createdAt = json[SerializationKeys.createdAt].string
        startLocation = json[SerializationKeys.startLocation].string
        numberOfSeats = json[SerializationKeys.numberOfSeats].int
        numberOfDays = json[SerializationKeys.numberOfDays].int
        driverPic = PicAggregate(json: json[SerializationKeys.driverPic])
        endLocation = json[SerializationKeys.endLocation].string
        status = json[SerializationKeys.status].int
        aggregateStartLocation = json[SerializationKeys.aggregateStartLocation].string
        id = json[SerializationKeys.id].string
        driverWorkingTime = DriverWorkingTimeAggregate(json: json[SerializationKeys.driverWorkingTime])
        aggregateEndLocation = json[SerializationKeys.aggregateEndLocation].string
        workingTime = WorkingTimeAggregate(json: json[SerializationKeys.workingTime])
        v = json[SerializationKeys.v].int
        updatedAt = json[SerializationKeys.updatedAt].string
        totalDistance = json[SerializationKeys.totalDistance].int
        totalTime = json[SerializationKeys.totalTime].int
        numberOFActiveRider = json[SerializationKeys.numberOFActiveRider].int
        if let items = json[SerializationKeys.trips].array { trips = items.map { TripsAggregate(json: $0) } }
        driverName = json[SerializationKeys.driverName].string
        driverId = json[SerializationKeys.driverId].string
        driverEarning = json[SerializationKeys.driverEarning].float
        driverMobile = json[SerializationKeys.driverMobile].string
        startDate = json[SerializationKeys.startDate].string
        aggregateTripTime = json[SerializationKeys.aggregateTripTime].string
        tripDateTime = json[SerializationKeys.tripDateTime].string
        
        if let items = json[SerializationKeys.wayPoints].array{
           wayPoints = items.map({ WayPointsAggregate.init(json: $0)})
        }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = routeName { dictionary[SerializationKeys.routeName] = value }
        if let value = driverRating { dictionary[SerializationKeys.driverRating] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = startLocation { dictionary[SerializationKeys.startLocation] = value }
        if let value = numberOfSeats { dictionary[SerializationKeys.numberOfSeats] = value }
        if let value = numberOfDays { dictionary[SerializationKeys.numberOfDays] = value }
        if let value = driverPic { dictionary[SerializationKeys.driverPic] = value.dictionaryRepresentation() }
        if let value = endLocation { dictionary[SerializationKeys.endLocation] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = aggregateStartLocation { dictionary[SerializationKeys.aggregateStartLocation] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = driverWorkingTime { dictionary[SerializationKeys.driverWorkingTime] = value.dictionaryRepresentation() }
        if let value = aggregateEndLocation { dictionary[SerializationKeys.aggregateEndLocation] = value }
        if let value = workingTime { dictionary[SerializationKeys.workingTime] = value.dictionaryRepresentation() }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = totalDistance { dictionary[SerializationKeys.totalDistance] = value }
        if let value = totalTime { dictionary[SerializationKeys.totalTime] = value }
        if let value = numberOFActiveRider { dictionary[SerializationKeys.numberOFActiveRider] = value }
        if let value = trips { dictionary[SerializationKeys.trips] = value.map { $0.dictionaryRepresentation() } }
        if let value = driverName { dictionary[SerializationKeys.driverName] = value }
        if let value = driverId { dictionary[SerializationKeys.driverId] = value }
        if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
        if let value = driverMobile { dictionary[SerializationKeys.driverMobile] = value }
        if let value = startDate { dictionary[SerializationKeys.startDate] = value }
        if let value = aggregateTripTime { dictionary[SerializationKeys.aggregateTripTime] = value }
        if let value = tripDateTime { dictionary[SerializationKeys.tripDateTime] = value }
        return dictionary
    }
    
}

public final class PicAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let thumb = "thumb"
        static let original = "original"
    }
    
    // MARK: Properties
    public var thumb: String?
    public var original: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        thumb = json[SerializationKeys.thumb].string
        original = json[SerializationKeys.original].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = thumb { dictionary[SerializationKeys.thumb] = value }
        if let value = original { dictionary[SerializationKeys.original] = value }
        return dictionary
    }
    
}

public final class DriverWorkingTimeAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let dayNumber = "dayNumber"
        static let time = "time"
    }
    
    // MARK: Properties
    public var dayNumber: [Int]?
    public var time: TimeAggregate?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        if let items = json[SerializationKeys.dayNumber].array { dayNumber = items.map { $0.intValue } }
        time = TimeAggregate(json: json[SerializationKeys.time])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = dayNumber { dictionary[SerializationKeys.dayNumber] = value }
        if let value = time { dictionary[SerializationKeys.time] = value.dictionaryRepresentation() }
        return dictionary
    }
}


public final class TimeAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let departureTime = "departureTime"
        static let arivalTime = "arivalTime"
    }
    
    // MARK: Properties
    public var departureTime: String?
    public var arivalTime: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        departureTime = json[SerializationKeys.departureTime].string
        arivalTime = json[SerializationKeys.arivalTime].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = departureTime { dictionary[SerializationKeys.departureTime] = value }
        if let value = arivalTime { dictionary[SerializationKeys.arivalTime] = value }
        return dictionary
    }
    
}
public final class LocationsAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}
public final class TripsAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let endAddress = "end_address"
        static let endWayPoints = "endWayPoints"
        static let tripId = "tripId"
        static let riderPic = "riderPic"
        static let biddata = "biddata"
        static let riderMobile = "riderMobile"
        static let riderId = "riderId"
        static let paymentStatus = "paymentStatus"
        static let aggregateAcceptedStatus = "aggregateAcceptedStatus"
        static let endLocation = "endLocation"
        static let status = "status"
        static let id = "_id"
        static let riderName = "riderName"
        static let startAddress = "start_address"
        static let tripTotalAmount = "tripTotalAmount"
        static let distance = "distance"
        static let amountCharged = "amountCharged"
        static let driverAmount = "driverAmount"
        static let bridgeTax = "bridgeTax"
        static let startLocation = "startLocation"
        static let duration = "duration"
        static let adminAmount = "adminAmount"
        static let riderRating = "riderRating"
        static let adminMarkPickUpLocation = "adminMarkPickUpLocation"
        static let adminMarkDropOffLocation = "adminMarkDropOffLocation"
    }
    
    // MARK: Properties
    public var endAddress: String?
    public var endWayPoints: [Any]?
    public var tripId: String?
    public var riderPic: PicAggregate?
    public var biddata: [Any]?
    public var riderMobile: String?
    public var riderId: String?
    public var paymentStatus: Int?
    public var aggregateAcceptedStatus: Int?
    public var endLocation: LocationsAggregate?
    public var status: Int?
    public var id: String?
    public var riderName: String?
    public var startAddress: String?
    public var tripTotalAmount: Int?
    public var distance: Float?
    public var amountCharged: Int?
    public var driverAmount: Int?
    public var bridgeTax: Int?
    public var startLocation: LocationsAggregate?
    public var duration: Float?
    public var adminAmount: Int?
    public var riderRating: Int?
    public var adminMarkPickUpLocation: LocationsAggregate?
    public var adminMarkDropOffLocation: LocationsAggregate?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        endAddress = json[SerializationKeys.endAddress].string
        if let items = json[SerializationKeys.endWayPoints].array { endWayPoints = items.map { $0.object} }
        tripId = json[SerializationKeys.tripId].string
        riderPic = PicAggregate(json: json[SerializationKeys.riderPic])
        if let items = json[SerializationKeys.biddata].array { biddata = items.map { $0.object} }
        riderMobile = json[SerializationKeys.riderMobile].string
        riderId = json[SerializationKeys.riderId].string
        paymentStatus = json[SerializationKeys.paymentStatus].int
        aggregateAcceptedStatus = json[SerializationKeys.aggregateAcceptedStatus].int
        endLocation = LocationsAggregate(json: json[SerializationKeys.endLocation])
        status = json[SerializationKeys.status].int
        id = json[SerializationKeys.id].string
        riderName = json[SerializationKeys.riderName].string
        startAddress = json[SerializationKeys.startAddress].string
        tripTotalAmount = json[SerializationKeys.tripTotalAmount].int
        distance = json[SerializationKeys.distance].float
        amountCharged = json[SerializationKeys.amountCharged].int
        driverAmount = json[SerializationKeys.driverAmount].int
        bridgeTax = json[SerializationKeys.bridgeTax].int
        startLocation = LocationsAggregate(json: json[SerializationKeys.startLocation])
        duration = json[SerializationKeys.duration].float
        adminAmount = json[SerializationKeys.adminAmount].int
        riderRating = json[SerializationKeys.riderRating].int
        adminMarkPickUpLocation = LocationsAggregate(json: json[SerializationKeys.adminMarkPickUpLocation])
        adminMarkDropOffLocation = LocationsAggregate(json: json[SerializationKeys.adminMarkDropOffLocation])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = endAddress { dictionary[SerializationKeys.endAddress] = value }
        if let value = endWayPoints { dictionary[SerializationKeys.endWayPoints] = value }
        if let value = tripId { dictionary[SerializationKeys.tripId] = value }
        if let value = riderPic { dictionary[SerializationKeys.riderPic] = value.dictionaryRepresentation() }
        if let value = biddata { dictionary[SerializationKeys.biddata] = value }
        if let value = riderMobile { dictionary[SerializationKeys.riderMobile] = value }
        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
        if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
        if let value = aggregateAcceptedStatus { dictionary[SerializationKeys.aggregateAcceptedStatus] = value }
        if let value = endLocation { dictionary[SerializationKeys.endLocation] = value.dictionaryRepresentation() }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = riderName { dictionary[SerializationKeys.riderName] = value }
        if let value = startAddress { dictionary[SerializationKeys.startAddress] = value }
        if let value = tripTotalAmount { dictionary[SerializationKeys.tripTotalAmount] = value }
        if let value = distance { dictionary[SerializationKeys.distance] = value }
        if let value = amountCharged { dictionary[SerializationKeys.amountCharged] = value }
        if let value = driverAmount { dictionary[SerializationKeys.driverAmount] = value }
        if let value = bridgeTax { dictionary[SerializationKeys.bridgeTax] = value }
        if let value = startLocation { dictionary[SerializationKeys.startLocation] = value.dictionaryRepresentation() }
        if let value = duration { dictionary[SerializationKeys.duration] = value }
        if let value = adminAmount { dictionary[SerializationKeys.adminAmount] = value }
        if let value = riderRating { dictionary[SerializationKeys.riderRating] = value }
        return dictionary
    }
    
}
public final class WorkingTimeAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let dayNumber = "dayNumber"
        static let time = "time"
    }
    
    // MARK: Properties
    public var dayNumber: [Int]?
    public var time: TimeAggregate?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        if let items = json[SerializationKeys.dayNumber].array { dayNumber = items.map { $0.intValue } }
        time = TimeAggregate(json: json[SerializationKeys.time])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = dayNumber { dictionary[SerializationKeys.dayNumber] = value }
        if let value = time { dictionary[SerializationKeys.time] = value.dictionaryRepresentation() }
        return dictionary
    }
}

public final class WayPointsAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
        static let id = "_id"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    public var id: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
        id = json[SerializationKeys.id].string
    }
}
