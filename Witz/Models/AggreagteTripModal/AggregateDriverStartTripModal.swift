//
//  AggregateDriverStartTripModal.swift
//
//  Created by abhishek kumar on 22/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AggregateDriverStartTripModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bidRejectedByDriver = "bidRejectedByDriver"
    static let driverReachedKM = "driverReachedKM"
    static let driverDeclineInstant = "driverDeclineInstant"
    static let localbookingDateTime = "localbookingDateTime"
    static let driverEarning = "driverEarning"
    static let aggregateFinalTripId = "aggregateFinalTripId"
    static let paymentStatus = "paymentStatus"
    static let endLocation = "endLocation"
    static let isFinalAggregate = "isFinalAggregate"
    static let timezone = "timezone"
    static let workingTime = "workingTime"
    static let kmTravel = "kmTravel"
    static let riderRating = "riderRating"
   static let riderMobile = "riderMobile"
    static let wayEndPoints = "wayEndPoints"
    static let endAddress = "end_address"
    static let riderCancelationFee = "riderCancelationFee"
    static let seatsRequired = "seatsRequired"
    static let parentSharingTrip = "parentSharingTrip"
    static let driverRating = "driverRating"
    static let aggregateTripTime = "aggregateTripTime"
    static let bidByDriver = "bidByDriver"
    static let price = "price"
    static let sentBookingReguestToDriver = "sentBookingReguestToDriver"
    static let updatedAt = "updatedAt"
    static let refTripId = "refTripId"
    static let aggregateTripType = "aggregateTripType"
    static let driverPushInQ = "driverPushInQ"
    static let bookingDate = "bookingDate"
    static let driverReachedTime = "driverReachedTime"
    static let driverCancelationFee = "driverCancelationFee"
    static let riderTaxApplied = "riderTaxApplied"
    static let aggregateRequestPendingByDriver = "aggregateRequestPendingByDriver"
    static let tripRefundAmount = "tripRefundAmount"
    static let distance = "distance"
    static let walletPayment = "walletPayment"
    static let bridgeTax = "bridgeTax"
    static let startLocation = "startLocation"
    static let duration = "duration"
    static let driverTaxApplied = "driverTaxApplied"
    static let tripType = "tripType"
    static let createdAt = "createdAt"
    static let aggregatePaymentStatus = "aggregatePaymentStatus"
    static let isShared = "isShared"
    static let riderId = "riderId"
    static let status = "status"
    static let id = "_id"
    static let riderName = "riderName"
    static let startAddress = "start_address"
    static let v = "__v"
    static let driverId = "driverId"
    static let vehicleInformation = "vehicle_information"
    static let adminEarning = "adminEarning"
  }

  // MARK: Properties
  public var bidRejectedByDriver: [Any]?
  public var driverReachedKM: Int?
  public var driverDeclineInstant: [Any]?
  public var localbookingDateTime: String?
  public var driverEarning: Float?
  public var aggregateFinalTripId: String?
  public var paymentStatus: String?
  public var endLocation: StartLocation_AggragateStartTrip?
  public var isFinalAggregate: Bool? = false
  public var timezone: String?
  public var workingTime: [WorkingTime_AggragateStartTrip]?
  public var kmTravel: Int?
  public var riderRating: Int?
    public var riderMobile : String?
  public var wayEndPoints: [Any]?
  public var endAddress: String?
  public var riderCancelationFee: Int?
  public var seatsRequired: Int?
  public var parentSharingTrip: Bool? = false
  public var driverRating: Int?
  public var aggregateTripTime: String?
  public var bidByDriver: [Any]?
  public var price: Float?
  public var sentBookingReguestToDriver: [Any]?
  public var updatedAt: String?
  public var refTripId: String?
  public var aggregateTripType: Int?
  public var driverPushInQ: [Any]?
  public var bookingDate: String?
  public var driverReachedTime: Int?
  public var driverCancelationFee: Int?
  public var riderTaxApplied: Int?
  public var aggregateRequestPendingByDriver: [Any]?
  public var tripRefundAmount: Int?
  public var distance: Float?
  public var walletPayment: Int?
  public var bridgeTax: Int?
  public var startLocation: StartLocation_AggragateStartTrip?
  public var duration: Float?
  public var driverTaxApplied: Int?
  public var tripType: String?
  public var createdAt: String?
  public var aggregatePaymentStatus: Int?
  public var isShared: Bool? = false
  public var riderId: String?
  public var status: Int?
  public var id: String?
  public var riderName: String?
  public var startAddress: String?
  public var v: Int?
  public var driverId: String?
  public var vehicleInformation: VehicleInformation?
  public var adminEarning: Float?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.bidRejectedByDriver].array { bidRejectedByDriver = items.map { $0.object} }
    driverReachedKM = json[SerializationKeys.driverReachedKM].int
    if let items = json[SerializationKeys.driverDeclineInstant].array { driverDeclineInstant = items.map { $0.object} }
    localbookingDateTime = json[SerializationKeys.localbookingDateTime].string
    driverEarning = json[SerializationKeys.driverEarning].float
    aggregateFinalTripId = json[SerializationKeys.aggregateFinalTripId].string
    paymentStatus = json[SerializationKeys.paymentStatus].string
    endLocation = StartLocation_AggragateStartTrip(json: json[SerializationKeys.endLocation])
    isFinalAggregate = json[SerializationKeys.isFinalAggregate].boolValue
    timezone = json[SerializationKeys.timezone].string
    if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { WorkingTime_AggragateStartTrip(json: $0) } }
    kmTravel = json[SerializationKeys.kmTravel].int
    riderRating = json[SerializationKeys.riderRating].int
    riderMobile = json[SerializationKeys.riderMobile].string
    if let items = json[SerializationKeys.wayEndPoints].array { wayEndPoints = items.map { $0.object} }
    endAddress = json[SerializationKeys.endAddress].string
    riderCancelationFee = json[SerializationKeys.riderCancelationFee].int
    seatsRequired = json[SerializationKeys.seatsRequired].int
    parentSharingTrip = json[SerializationKeys.parentSharingTrip].boolValue
    driverRating = json[SerializationKeys.driverRating].int
    aggregateTripTime = json[SerializationKeys.aggregateTripTime].string
    if let items = json[SerializationKeys.bidByDriver].array { bidByDriver = items.map { $0.object} }
    price = json[SerializationKeys.price].float
    if let items = json[SerializationKeys.sentBookingReguestToDriver].array { sentBookingReguestToDriver = items.map { $0.object} }
    updatedAt = json[SerializationKeys.updatedAt].string
    refTripId = json[SerializationKeys.refTripId].string
    aggregateTripType = json[SerializationKeys.aggregateTripType].int
    if let items = json[SerializationKeys.driverPushInQ].array { driverPushInQ = items.map { $0.object} }
    bookingDate = json[SerializationKeys.bookingDate].string
    driverReachedTime = json[SerializationKeys.driverReachedTime].int
    driverCancelationFee = json[SerializationKeys.driverCancelationFee].int
    riderTaxApplied = json[SerializationKeys.riderTaxApplied].int
    if let items = json[SerializationKeys.aggregateRequestPendingByDriver].array { aggregateRequestPendingByDriver = items.map { $0.object} }
    tripRefundAmount = json[SerializationKeys.tripRefundAmount].int
    distance = json[SerializationKeys.distance].float
    walletPayment = json[SerializationKeys.walletPayment].int
    bridgeTax = json[SerializationKeys.bridgeTax].int
    startLocation = StartLocation_AggragateStartTrip(json: json[SerializationKeys.startLocation])
    duration = json[SerializationKeys.duration].float
    driverTaxApplied = json[SerializationKeys.driverTaxApplied].int
    tripType = json[SerializationKeys.tripType].string
    createdAt = json[SerializationKeys.createdAt].string
    aggregatePaymentStatus = json[SerializationKeys.aggregatePaymentStatus].int
    isShared = json[SerializationKeys.isShared].boolValue
    riderId = json[SerializationKeys.riderId].string
    status = json[SerializationKeys.status].int
    id = json[SerializationKeys.id].string
    riderName = json[SerializationKeys.riderName].string
    startAddress = json[SerializationKeys.startAddress].string
    v = json[SerializationKeys.v].int
    driverId = json[SerializationKeys.driverId].string
    vehicleInformation = VehicleInformation(json: json[SerializationKeys.vehicleInformation])
    adminEarning = json[SerializationKeys.adminEarning].float
  }
}

public final class StartLocation_AggragateStartTrip {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
}
public final class Time_AggragateStartTrip {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let departureTime = "departureTime"
        static let arivalTime = "arivalTime"
    }
    
    // MARK: Properties
    public var departureTime: String?
    public var arivalTime: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        departureTime = json[SerializationKeys.departureTime].string
        arivalTime = json[SerializationKeys.arivalTime].string
    }
}
public final class VehicleInformation_AggragateStartTrip {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let color = "color"
        static let subBrand = "subBrand"
        static let year = "year"
        static let model = "model"
        static let vehicleNumber = "vehicle_number"
        static let seatingCapacity = "seating_capacity"
        static let isVerified = "isVerified"
        static let brand = "brand"
    }
    
    // MARK: Properties
    public var color: String?
    public var subBrand: String?
    public var year: String?
    public var model: String?
    public var vehicleNumber: String?
    public var seatingCapacity: String?
    public var isVerified: Bool? = false
    public var brand: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        color = json[SerializationKeys.color].string
        subBrand = json[SerializationKeys.subBrand].string
        year = json[SerializationKeys.year].string
        model = json[SerializationKeys.model].string
        vehicleNumber = json[SerializationKeys.vehicleNumber].string
        seatingCapacity = json[SerializationKeys.seatingCapacity].string
        isVerified = json[SerializationKeys.isVerified].boolValue
        brand = json[SerializationKeys.brand].string
    }
}
public final class WorkingTime_AggragateStartTrip {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let dayNumber = "dayNumber"
        static let time = "time"
        static let id = "_id"
    }
    
    // MARK: Properties
    public var dayNumber: Int?
    public var time: Time?
    public var id: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        dayNumber = json[SerializationKeys.dayNumber].int
        time = Time(json: json[SerializationKeys.time])
        id = json[SerializationKeys.id].string
    }
 
}
