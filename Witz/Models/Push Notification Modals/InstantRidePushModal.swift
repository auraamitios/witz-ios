//
//  InstantRidePushModal.swift
//
//  Created by abhishek kumar on 01/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class InstantRidePushModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let tripId = "tripId"
    static let pushDateTime = "pushDateTime"
    static let isSharedTrip = "isSharedTrip"
  }

  // MARK: Properties
  public var tripId: String?
  public var pushDateTime: String?
  public var isSharedTrip: Bool?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    tripId = json[SerializationKeys.tripId].string
    pushDateTime = json[SerializationKeys.pushDateTime].string
    isSharedTrip = json[SerializationKeys.isSharedTrip].bool
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = tripId { dictionary[SerializationKeys.tripId] = value }
    if let value = pushDateTime { dictionary[SerializationKeys.pushDateTime] = value }
    if let value = isSharedTrip { dictionary[SerializationKeys.isSharedTrip] = value }
    return dictionary
  }

}
