//
//  DriverInfo.swift
//  Witz
//
//  Created by Amit Tripathi on 9/22/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct DriverInfo {
    
    var pic: PicDetail?
    var version:Double?
    var driver_Id:String?
    var acc_type:String?
    var accessToken:String?
    
    var arrivalTime:Double?
    var avgRating:Double?
    var badgeCount:Double?
    var deviceToken:String?
    
    var deviceType:Double?
    var fiveStarTrips:Double?
    var fullName:String?
    var isBlocked:Bool?
    
    var isDeleted:Bool?
    var isVerified:Bool?
    var mobile:String?
    var password:String?
    
    var ratedTrips:Double?
    var refferalCode:String?
    var status:String?
    var totalCredits:Double?
    
    var totalKm:Double?
    var totalNumberRefferal:Double?
    var totalRefferalAmount:Double?
    var totalTrips:Double?
    
    var type:String?
    
    //*********
    //bank_information
    var bank_information:[String:AnyObject]?
    var account_name:String?
    var account_number:String?
    var bankAccountIsVerified:Bool?
    
    //currentLocation
    var currentLocation:[String:AnyObject]?
    var latitude:String?
    var longitude:String?
    var locPoint:String?
    
    //personal_information
    var personal_information:[String:AnyObject]?
    var driverAddress:String?
    var company_name:String?
    var driver_email:String?
    var driver_fullName:String?
    var driverVerfied:Bool?
    
    //pic
    var picDetail:[String:AnyObject]?
    var driver_OriginalUrl:String?
    var driver_ThumbUrl:String?
    
    //vehicle_information
    var vehicle_information:[String:AnyObject]?
    var brand:String?
    var subBrand:String?
    var color:String?
    var vehicleVerified:Bool?
    var model:String?
    var seating_capacity:Double?
    var year:Double?
    var vehicle_number:String?
    
    //documents
    var doucuments:[String:AnyObject]?
    var doucmentName:String?
    var doucmentPath:[AnyObject]?
    var uploadStatus:Bool?
    var isDoucmentVerified:Bool?
    var doucmentVerifyStatus:Bool?
    var doucmentComment:String?
    var documentsDriver: [DocumentsDriver]?
    
    // Updated Parameter
    var reservationGeneralSetting: ReservationGeneralSetting?
    var todayDateTimestamp: Int?
    var updatedAt: String?
    var currentTripData: CurrentTripData?
    var documentsActivityStatus: Int?
    var reservationFutureTripCount: Int?
    var createdAt: String?
    var arrivalDistance: Int?
    var lan: String?
    var updatedLastLocAt: String?
     var todayDateDateTime: String?
    var notificationCount: Int?
    var reservationPendingTripCount: Int?
    var serviceRequestPendingTripCount: Int?
    var serviceRequestFutureTripCount: Int?
    //FIXME::
    var arryDocuments:[AnyObject]?
    
    static func parseDriverInfo(data:[String:JSON], outerData:JSON)->DriverInfo{
        
        var driverInfo = DriverInfo()
        driverInfo.pic = PicDetail(json: data["pic"] ?? ["":""])
        driverInfo.version = data["__v"]?.double
        driverInfo.driver_Id = data["_id"]?.string
        driverInfo.accessToken = data["accessToken"]?.string
        driverInfo.acc_type = data["acc_type"]?.string
        driverInfo.arrivalTime = data["arrivalTime"]?.double
        driverInfo.avgRating = data["avgRating"]?.double
        driverInfo.badgeCount = data["badgeCount"]?.double
        driverInfo.deviceToken = data["deviceToken"]?.string
        driverInfo.deviceType = data["deviceType"]?.double
        driverInfo.fiveStarTrips = data["fiveStarTrips"]?.double
        driverInfo.fullName = data["fullName"]?.string
        driverInfo.isBlocked = data["isBlocked"]?.bool
        driverInfo.isDeleted = data["isDeleted"]?.bool
        driverInfo.isVerified = data["isVerified"]?.bool
        driverInfo.mobile = data["mobile"]?.string
        driverInfo.password = data["password"]?.string
        driverInfo.ratedTrips = data["ratedTrips"]?.double
        driverInfo.refferalCode = data["refferalCode"]?.string
        driverInfo.status = data["status"]?.string
        driverInfo.totalCredits = data["totalCredits"]?.double
        driverInfo.totalKm = data["totalKm"]?.double
        driverInfo.totalNumberRefferal = data["totalNumberRefferal"]?.double
        driverInfo.totalRefferalAmount = data["totalRefferalAmount"]?.double
        driverInfo.totalTrips = data["totalTrips"]?.double
        driverInfo.type = data["type"]?.string
        
        UserDefaultManager.sharedManager.addValue(object: driverInfo.accessToken! as AnyObject, key: LinkStrings.UserDefaultKeys.DriverAccessToken)
        
        //vehicle info
        let dict1 = data["vehicle_information"]?.dictionaryValue
        driverInfo.brand = dict1!["brand"]?.stringValue
        driverInfo.subBrand = dict1!["subBrand"]?.stringValue
        driverInfo.color = dict1!["color"]?.stringValue
        driverInfo.vehicleVerified = dict1!["isVerified"]?.boolValue
        driverInfo.model = dict1!["model"]?.stringValue
        driverInfo.seating_capacity = dict1!["seating_capacity"]?.doubleValue
        driverInfo.vehicle_number = dict1!["vehicle_number"]?.stringValue
        driverInfo.year = dict1!["year"]?.doubleValue
        
        driverInfo.vehicle_information = ["brand":driverInfo.brand as AnyObject, "subBrand":driverInfo.subBrand as AnyObject, "color":driverInfo.color as AnyObject, "isVerified":driverInfo.vehicleVerified as AnyObject, "model":driverInfo.model as AnyObject, "seating_capacity": driverInfo.seating_capacity as AnyObject, "vehicle_number":driverInfo.vehicle_number as AnyObject, "year":driverInfo.year as AnyObject]
        
        //personal info
        let dict2 = data["personal_information"]?.dictionary
        driverInfo.driverAddress = dict2!["address"]?.stringValue
        driverInfo.company_name = dict2!["company_name"]?.stringValue
        driverInfo.driver_email = dict2!["email"]?.stringValue
        driverInfo.driver_fullName = dict2!["fullName"]?.stringValue
        driverInfo.driverVerfied = dict2!["isVerified"]?.boolValue
        
        driverInfo.personal_information = ["address":driverInfo.driverAddress as AnyObject, "company_name":driverInfo.company_name as AnyObject, "email":driverInfo.driver_email as AnyObject, "fullName":driverInfo.driver_fullName as AnyObject, "seating_capacity": driverInfo.seating_capacity as AnyObject, "isVerified":driverInfo.driverVerfied as AnyObject]
        
        // account details
        let dictt = data["bank_information"]?.dictionaryValue
        driverInfo.account_name = dictt!["account_name"]?.stringValue
        driverInfo.account_number = dictt!["account_number"]?.stringValue
        driverInfo.bankAccountIsVerified = dictt!["isVerified"]?.boolValue
        
        driverInfo.bank_information = ["account_name":driverInfo.account_name as AnyObject, "account_number":driverInfo.account_number as AnyObject, "isVerified": driverInfo.bankAccountIsVerified as AnyObject]
        //pic detail
        let dict3 = data["pic"]?.dictionaryValue
        driverInfo.driver_OriginalUrl = dict3!["original"]?.stringValue
        driverInfo.driver_ThumbUrl = dict3!["thumb"]?.stringValue
        
        //location info
        let dict4 = data["currentLocation"]?.dictionaryValue
        driverInfo.locPoint = dict4!["type"]?.stringValue
        let locArry = dict4!["coordinates"]?.arrayValue
        driverInfo.currentLocation = ["type":driverInfo.locPoint as AnyObject, "coordinates":locArry as AnyObject]
//        print("dict value************\(String(describing: driverInfo.currentLocation))")
        
        //documents lists
        let dict5 = data["documents"]?.dictionaryValue
        driverInfo.isDoucmentVerified = dict5!["isVerified"]?.boolValue
        
        let docArry = dict5!["documents"]?.arrayValue
        var mainArry = [AnyObject]()
        var innerDict:[String:AnyObject]?
        
        for doc in docArry! {
            
            let modal = DocumentsDriver(json:doc)
            driverInfo.doucmentName = doc["name"].stringValue
            driverInfo.uploadStatus = doc["uploaded"].boolValue
            driverInfo.doucmentVerifyStatus = doc["isVerified"].boolValue
            driverInfo.doucmentComment = doc["comment"].stringValue

            innerDict = ["name":driverInfo.doucmentName as AnyObject, "uploaded":driverInfo.uploadStatus as AnyObject, "path":modal.path as AnyObject ,"comment":driverInfo.doucmentComment as AnyObject,"isVerified":driverInfo.doucmentVerifyStatus as AnyObject]
            
            mainArry.append(innerDict as AnyObject)
            
//            print("fetcheddata**********\(mainArry)")
        }
        
        driverInfo.arryDocuments = mainArry
        driverInfo.doucuments = ["documents":mainArry as AnyObject, "isVerified":driverInfo.isDoucmentVerified as AnyObject]
        innerDict = nil
//        print("documents final Dict**********\(String(describing: driverInfo.doucuments))")
        
        
        //Updated Parameters
        driverInfo.reservationGeneralSetting =  ReservationGeneralSetting(json: data["reservationGeneralSetting"] ?? ["":""])
        driverInfo.todayDateTimestamp = data["todayDateTimestamp"]?.int
        driverInfo.updatedAt = data["updatedAt"]?.string
        driverInfo.currentTripData = CurrentTripData(json: data["currentTripData"] ?? ["":""])
        driverInfo.documentsActivityStatus = data["documentsActivityStatus"]?.int
        driverInfo.reservationFutureTripCount =  data["reservationFutureTripCount"]?.int
        driverInfo.createdAt = data["createdAt"]?.string
        driverInfo.arrivalDistance = data["arrivalDistance"]?.int
        driverInfo.lan = data["lan"]?.string
        driverInfo.updatedLastLocAt = data["updatedLastLocAt"]?.string
        driverInfo.todayDateDateTime = data["todayDateDateTime"]?.string
        driverInfo.notificationCount = data["notificationCount"]?.int
        driverInfo.reservationPendingTripCount = data["reservationPendingTripCount"]?.int
        driverInfo.serviceRequestPendingTripCount = data["serviceRequestPendingTripCount"]?.int
         driverInfo.serviceRequestFutureTripCount = data["serviceRequestFutureTripCount"]?.int
        
        
        return driverInfo
        
    }
    
    static func parseDriverInfoJson(data1:JSON?)->DriverInfo{
        let data = data1?["data"]
        var driverInfo = DriverInfo()
        driverInfo.pic = PicDetail(json: data?["pic"] ?? ["":""])
        driverInfo.version = data?["__v"].double
        driverInfo.driver_Id = data?["_id"].string
        driverInfo.accessToken = data?["accessToken"].string
        driverInfo.acc_type = data?["acc_type"].string
        driverInfo.arrivalTime = data?["arrivalTime"].double
        driverInfo.avgRating = data?["avgRating"].double
        driverInfo.badgeCount = data?["badgeCount"].double
        driverInfo.deviceToken = data?["deviceToken"].string
        driverInfo.deviceType = data?["deviceType"].double
        driverInfo.fiveStarTrips = data?["fiveStarTrips"].double
        driverInfo.fullName = data?["fullName"].string
        driverInfo.isBlocked = data?["isBlocked"].bool
        driverInfo.isDeleted = data?["isDeleted"].bool
        driverInfo.isVerified = data?["isVerified"].bool
        driverInfo.mobile = data?["mobile"].string
        driverInfo.password = data?["password"].string
        driverInfo.ratedTrips = data?["ratedTrips"].double
        driverInfo.refferalCode = data?["refferalCode"].string
        driverInfo.status = data?["status"].string
        driverInfo.totalCredits = data?["totalCredits"].double
        driverInfo.totalKm = data?["totalKm"].double
        driverInfo.totalNumberRefferal = data?["totalNumberRefferal"].double
        driverInfo.totalRefferalAmount = data?["totalRefferalAmount"].double
        driverInfo.totalTrips = data?["totalTrips"].double
        driverInfo.type = data?["type"].string
        
        UserDefaultManager.sharedManager.addValue(object: driverInfo.accessToken! as AnyObject, key: LinkStrings.UserDefaultKeys.DriverAccessToken)
        
        //vehicle info
        let dict1 = data?["vehicle_information"].dictionaryValue
        driverInfo.brand = dict1!["brand"]?.stringValue
        driverInfo.subBrand = dict1!["subBrand"]?.stringValue
        driverInfo.color = dict1!["color"]?.stringValue
        driverInfo.vehicleVerified = dict1!["isVerified"]?.boolValue
        driverInfo.model = dict1!["model"]?.stringValue
        driverInfo.seating_capacity = dict1!["seating_capacity"]?.doubleValue
        driverInfo.vehicle_number = dict1!["vehicle_number"]?.stringValue
        driverInfo.year = dict1!["year"]?.doubleValue
        
        driverInfo.vehicle_information = ["brand":driverInfo.brand as AnyObject, "subBrand":driverInfo.subBrand as AnyObject, "color":driverInfo.color as AnyObject, "isVerified":driverInfo.vehicleVerified as AnyObject, "model":driverInfo.model as AnyObject, "seating_capacity": driverInfo.seating_capacity as AnyObject, "vehicle_number":driverInfo.vehicle_number as AnyObject, "year":driverInfo.year as AnyObject]
        
        //personal info
        let dict2 = data?["personal_information"].dictionary
        driverInfo.driverAddress = dict2!["address"]?.stringValue
        driverInfo.company_name = dict2!["company_name"]?.stringValue
        driverInfo.driver_email = dict2!["email"]?.stringValue
        driverInfo.driver_fullName = dict2!["fullName"]?.stringValue
        driverInfo.driverVerfied = dict2!["isVerified"]?.boolValue
        
        driverInfo.personal_information = ["address":driverInfo.driverAddress as AnyObject, "company_name":driverInfo.company_name as AnyObject, "email":driverInfo.driver_email as AnyObject, "fullName":driverInfo.driver_fullName as AnyObject, "seating_capacity": driverInfo.seating_capacity as AnyObject, "isVerified":driverInfo.driverVerfied as AnyObject]
        
        // account details
        let dictt = data?["bank_information"].dictionaryValue
        driverInfo.account_name = dictt!["account_name"]?.stringValue
        driverInfo.account_number = dictt!["account_number"]?.stringValue
        driverInfo.bankAccountIsVerified = dictt!["isVerified"]?.boolValue
        
        driverInfo.bank_information = ["account_name":driverInfo.account_name as AnyObject, "account_number":driverInfo.account_number as AnyObject, "isVerified": driverInfo.bankAccountIsVerified as AnyObject]
        //pic detail
        let dict3 = data?["pic"].dictionaryValue
        driverInfo.driver_OriginalUrl = dict3!["original"]?.stringValue
        driverInfo.driver_ThumbUrl = dict3!["thumb"]?.stringValue
        
        //location info
        let dict4 = data?["currentLocation"].dictionaryValue
        driverInfo.locPoint = dict4!["type"]?.stringValue
        let locArry = dict4!["coordinates"]?.arrayValue
        driverInfo.currentLocation = ["type":driverInfo.locPoint as AnyObject, "coordinates":locArry as AnyObject]
//        print("dict value************\(String(describing: driverInfo.currentLocation))")
        
        //documents lists
        let dict5 = data?["documents"].dictionaryValue
        driverInfo.isDoucmentVerified = dict5!["isVerified"]?.boolValue
        
        let docArry = dict5!["documents"]?.arrayValue
        var mainArry = [AnyObject]()
        var innerDict:[String:AnyObject]?
        
        for doc in docArry! {
            let modal = DocumentsDriver(json:doc)
          
            driverInfo.doucmentName = doc["name"].stringValue
            driverInfo.uploadStatus = doc["uploaded"].boolValue
            driverInfo.doucmentVerifyStatus = doc["isVerified"].boolValue
            driverInfo.doucmentComment = doc["comment"].stringValue
            
            innerDict = ["name":driverInfo.doucmentName as AnyObject, "uploaded":driverInfo.uploadStatus as AnyObject, "path": modal.path as AnyObject ,"comment":driverInfo.doucmentComment as AnyObject,"isVerified":driverInfo.doucmentVerifyStatus as AnyObject]
            mainArry.append(innerDict as AnyObject)
//            print("fetcheddata**********\(mainArry)")
        }
        driverInfo.arryDocuments = mainArry
        driverInfo.doucuments = ["documents":mainArry as AnyObject, "isVerified":driverInfo.isDoucmentVerified as AnyObject]
        innerDict = nil
//        print("documents final Dict**********\(String(describing: driverInfo.doucuments))")
        
        //Updated Parameters
        driverInfo.reservationGeneralSetting =  ReservationGeneralSetting(json: data?["reservationGeneralSetting"] ?? ["":""])
        driverInfo.todayDateTimestamp = data?["todayDateTimestamp"].int
        driverInfo.updatedAt = data?["updatedAt"].string
        driverInfo.currentTripData = CurrentTripData(json: data?["currentTripData"] ?? ["":""])
        driverInfo.documentsActivityStatus = data?["documentsActivityStatus"].int
        driverInfo.reservationFutureTripCount =  data?["reservationFutureTripCount"].int
        driverInfo.createdAt = data?["createdAt"].string
        driverInfo.arrivalDistance = data?["arrivalDistance"].int
         driverInfo.lan = data?["lan"].string
         driverInfo.updatedLastLocAt = data?["updatedLastLocAt"].string
         driverInfo.todayDateDateTime = data?["todayDateDateTime"].string
         driverInfo.notificationCount = data?["notificationCount"].int
         driverInfo.reservationPendingTripCount = data?["reservationPendingTripCount"].int
        driverInfo.serviceRequestPendingTripCount = data?["serviceRequestPendingTripCount"].int
         driverInfo.serviceRequestFutureTripCount = data?["serviceRequestFutureTripCount"].int
        return driverInfo
        
    }
}
class PicDetail{
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
        static let thumb = "thumb"
        static let original = "original"
        
    }
    
    // MARK: Properties
    
    public var thumb: String?
    public var original: String?
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        original = json[SerializationKeys.original].string
        thumb = json[SerializationKeys.thumb].string
    }
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = original { dictionary[SerializationKeys.original] = value }
        if let value = thumb { dictionary[SerializationKeys.thumb] = value }
        return dictionary
    }
}

public final class DocumentsDriver {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let comment = "comment"
        static let name = "name"
        static let path = "path"
        static let uploaded = "uploaded"
        static let isVerified = "isVerified"
    }
    
    // MARK: Properties
    public var comment: String?
    public var name: String?
    public var path: [String]?
    public var uploaded: Bool? = false
    public var isVerified: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        comment = json[SerializationKeys.comment].string
        name = json[SerializationKeys.name].string
        if let items = json[SerializationKeys.path].array { path = items.map { $0.stringValue } }
        uploaded = json[SerializationKeys.uploaded].boolValue
        isVerified = json[SerializationKeys.isVerified].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = comment { dictionary[SerializationKeys.comment] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = path { dictionary[SerializationKeys.path] = value }
        dictionary[SerializationKeys.uploaded] = uploaded
        dictionary[SerializationKeys.isVerified] = isVerified
        return dictionary
    }
    
}
public final class ReservationGeneralSetting {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let singleDayRiderOfferClosureDuration = "singleDayRiderOfferClosureDuration"
        static let minimumReservationTime = "minimumReservationTime"
        static let futureDayRiderBidAcceptanceDuration = "futureDayRiderBidAcceptanceDuration"
        static let singleDayDriverAcceptanceDuration = "singleDayDriverAcceptanceDuration"
        static let reservationDriverMaxRequestPerDay = "reservationDriverMaxRequestPerDay"
        static let sendOfferPriceToRiderIntervalTime = "sendOfferPriceToRiderIntervalTime"
        static let futureDayRiderOfferClosureDuration = "futureDayRiderOfferClosureDuration"
        static let singleDayRiderTopFiveBidsDuration = "singleDayRiderTopFiveBidsDuration"
        static let futureDayRiderTopFiveBidsDuration = "futureDayRiderTopFiveBidsDuration"
        static let driverBookingReminderTimeCronId = "driverBookingReminderTimeCronId"
        static let sendOfferPriceToRiderIntervalTimeCronId = "sendOfferPriceToRiderIntervalTimeCronId"
        static let singleDayRiderBidAcceptanceDuration = "singleDayRiderBidAcceptanceDuration"
        static let driverBookingReminderTime = "driverBookingReminderTime"
        static let driverReadyTime = "driverReadyTime"
        static let futureDayDriverAcceptanceDuration = "futureDayDriverAcceptanceDuration"
        static let reservationDiameter = "reservationDiameter"
    }
    
    // MARK: Properties
    public var singleDayRiderOfferClosureDuration: Int?
    public var minimumReservationTime: Float?
    public var futureDayRiderBidAcceptanceDuration: Int?
    public var singleDayDriverAcceptanceDuration: Int?
    public var reservationDriverMaxRequestPerDay: Int?
    public var sendOfferPriceToRiderIntervalTime: Int?
    public var futureDayRiderOfferClosureDuration: Int?
    public var singleDayRiderTopFiveBidsDuration: Int?
    public var futureDayRiderTopFiveBidsDuration: Int?
    public var driverBookingReminderTimeCronId: String?
    public var sendOfferPriceToRiderIntervalTimeCronId: String?
    public var singleDayRiderBidAcceptanceDuration: Int?
    public var driverBookingReminderTime: Int?
    public var driverReadyTime: Int?
    public var futureDayDriverAcceptanceDuration: Int?
    public var reservationDiameter: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        singleDayRiderOfferClosureDuration = json[SerializationKeys.singleDayRiderOfferClosureDuration].int
        minimumReservationTime = json[SerializationKeys.minimumReservationTime].float
        futureDayRiderBidAcceptanceDuration = json[SerializationKeys.futureDayRiderBidAcceptanceDuration].int
        singleDayDriverAcceptanceDuration = json[SerializationKeys.singleDayDriverAcceptanceDuration].int
        reservationDriverMaxRequestPerDay = json[SerializationKeys.reservationDriverMaxRequestPerDay].int
        sendOfferPriceToRiderIntervalTime = json[SerializationKeys.sendOfferPriceToRiderIntervalTime].int
        futureDayRiderOfferClosureDuration = json[SerializationKeys.futureDayRiderOfferClosureDuration].int
        singleDayRiderTopFiveBidsDuration = json[SerializationKeys.singleDayRiderTopFiveBidsDuration].int
        futureDayRiderTopFiveBidsDuration = json[SerializationKeys.futureDayRiderTopFiveBidsDuration].int
        driverBookingReminderTimeCronId = json[SerializationKeys.driverBookingReminderTimeCronId].string
        sendOfferPriceToRiderIntervalTimeCronId = json[SerializationKeys.sendOfferPriceToRiderIntervalTimeCronId].string
        singleDayRiderBidAcceptanceDuration = json[SerializationKeys.singleDayRiderBidAcceptanceDuration].int
        driverBookingReminderTime = json[SerializationKeys.driverBookingReminderTime].int
        driverReadyTime = json[SerializationKeys.driverReadyTime].int
        futureDayDriverAcceptanceDuration = json[SerializationKeys.futureDayDriverAcceptanceDuration].int
        reservationDiameter = json[SerializationKeys.reservationDiameter].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = singleDayRiderOfferClosureDuration { dictionary[SerializationKeys.singleDayRiderOfferClosureDuration] = value }
        if let value = minimumReservationTime { dictionary[SerializationKeys.minimumReservationTime] = value }
        if let value = futureDayRiderBidAcceptanceDuration { dictionary[SerializationKeys.futureDayRiderBidAcceptanceDuration] = value }
        if let value = singleDayDriverAcceptanceDuration { dictionary[SerializationKeys.singleDayDriverAcceptanceDuration] = value }
        if let value = reservationDriverMaxRequestPerDay { dictionary[SerializationKeys.reservationDriverMaxRequestPerDay] = value }
        if let value = sendOfferPriceToRiderIntervalTime { dictionary[SerializationKeys.sendOfferPriceToRiderIntervalTime] = value }
        if let value = futureDayRiderOfferClosureDuration { dictionary[SerializationKeys.futureDayRiderOfferClosureDuration] = value }
        if let value = singleDayRiderTopFiveBidsDuration { dictionary[SerializationKeys.singleDayRiderTopFiveBidsDuration] = value }
        if let value = futureDayRiderTopFiveBidsDuration { dictionary[SerializationKeys.futureDayRiderTopFiveBidsDuration] = value }
        if let value = driverBookingReminderTimeCronId { dictionary[SerializationKeys.driverBookingReminderTimeCronId] = value }
        if let value = sendOfferPriceToRiderIntervalTimeCronId { dictionary[SerializationKeys.sendOfferPriceToRiderIntervalTimeCronId] = value }
        if let value = singleDayRiderBidAcceptanceDuration { dictionary[SerializationKeys.singleDayRiderBidAcceptanceDuration] = value }
        if let value = driverBookingReminderTime { dictionary[SerializationKeys.driverBookingReminderTime] = value }
        if let value = driverReadyTime { dictionary[SerializationKeys.driverReadyTime] = value }
        if let value = futureDayDriverAcceptanceDuration { dictionary[SerializationKeys.futureDayDriverAcceptanceDuration] = value }
        if let value = reservationDiameter { dictionary[SerializationKeys.reservationDiameter] = value }
        return dictionary
    }
    
}
public final class CurrentTripData {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let tripData = "tripData"
        static let tripStatus = "tripStatus"
    }
    
    // MARK: Properties
    public var tripData: TripData?
    public var tripStatus: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        tripData = TripData(json: json[SerializationKeys.tripData])
        tripStatus = json[SerializationKeys.tripStatus].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = tripData { dictionary[SerializationKeys.tripData] = value.dictionaryRepresentation() }
        dictionary[SerializationKeys.tripStatus] = tripStatus
        return dictionary
    }
    
}



