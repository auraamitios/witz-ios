//
//  RideTimeDriverModal.swift
//  Witz
//
//  Created by abhishek kumar on 02/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class TripModalDriver : NSObject,NSCoding{
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let tripData = "tripData"
        static let riderData = "riderData"
    }
    
    // MARK: Properties
    public var tripData: TripData?
    public var riderData: RiderData?
    
    // MARK: SwiftyJSON Initializers
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    public required init(json: JSON) {
        tripData = TripData(json: json[SerializationKeys.tripData])
        riderData = RiderData(json: json[SerializationKeys.riderData])
    }
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = tripData { dictionary[SerializationKeys.tripData] = value.dictionaryRepresentation() }
        if let value = riderData { dictionary[SerializationKeys.riderData] = value.dictionaryRepresentation() }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.riderData = aDecoder.decodeObject(forKey: SerializationKeys.riderData) as? RiderData
        self.tripData = aDecoder.decodeObject(forKey: SerializationKeys.tripData) as? TripData
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(riderData, forKey: SerializationKeys.riderData)
        aCoder.encode(tripData, forKey: SerializationKeys.tripData)
    }
}

public final class TripData : NSObject,NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let bookingDate = "bookingDate"
        static let driverReachedTime = "driverReachedTime"
        static let adminEarning = "adminEarning"
        static let driverReachedKM = "driverReachedKM"
        static let driverCancelationFee = "driverCancelationFee"
        static let driverDeclineInstant = "driverDeclineInstant"
        static let driverEarning = "driverEarning"
        static let riderTaxApplied = "riderTaxApplied"
        static let paymentStatus = "paymentStatus"
        static let aggregateRequestPendingByDriver = "aggregateRequestPendingByDriver"
        static let schedulerId = "schedulerId"
        static let endLocation = "endLocation"
        static let tempSchedulerId = "tempSchedulerId"
        static let isFinalAggregate = "isFinalAggregate"
        static let distance = "distance"
        static let workingTime = "workingTime"
        static let bridgeTax = "bridgeTax"
        static let kmTravel = "kmTravel"
        static let duration = "duration"
        static let driverTaxApplied = "driverTaxApplied"
        static let riderRating = "riderRating"
        static let startLocation = "startLocation"
        static let tripType = "tripType"
        static let endAddress = "end_address"
        static let riderCancelationFee = "riderCancelationFee"
        static let seatsRequired = "seatsRequired"
        static let createdAt = "createdAt"
        static let parentSharingTrip = "parentSharingTrip"
        static let driverRating = "driverRating"
        static let cardId = "cardId"
        static let bidByDriver = "bidByDriver"
        static let riderId = "riderId"
        static let price = "price"
        static let aggregatePaymentStatus = "aggregatePaymentStatus"
        static let isShared = "isShared"
        static let vehicleType = "vehicleType"
        static let status = "status"
        static let id = "_id"
        static let startAddress = "start_address"
        static let sentBookingReguestToDriver = "sentBookingReguestToDriver"
        static let updatedAt = "updatedAt"
        static let v = "__v"
        static let driverPushInQ = "driverPushInQ"
        static let bidRejectedByDriver = "bidRejectedByDriver"
        
        static let localbookingDateTime = "localbookingDateTime"
        
        static let timezone = "timezone"
        
        static let timezoneString = "timezoneString"
        
        static let tripRefundAmount = "tripRefundAmount"
        static let walletPayment = "walletPayment"
        
        static let driverId = "driverId"
        static let vehicleInformation = "vehicle_information"
        
    }
    
    // MARK: Properties
    public var bookingDate: String?
    public var driverReachedTime: Int?
    public var adminEarning: Float?
    public var driverReachedKM: Int?
    public var driverCancelationFee: Int?
    public var driverDeclineInstant: [Any]?
    public var driverEarning: Float?
    public var riderTaxApplied: Float?
    public var paymentStatus: String?
    public var aggregateRequestPendingByDriver: [Any]?
    public var schedulerId: String?
    public var endLocation: Locations?
    public var tempSchedulerId: String?
    public var isFinalAggregate: Bool? = false
    public var distance: Float?
    public var workingTime: [Any]?
    public var bridgeTax: Int?
    public var kmTravel: Int?
    public var duration: Float?
    public var driverTaxApplied: Float?
    public var riderRating: Int?
    public var startLocation: Locations?
    public var tripType: String?
    public var endAddress: String?
    public var riderCancelationFee: Int?
    public var seatsRequired: Int?
    public var createdAt: String?
    public var parentSharingTrip: Bool? = false
    public var driverRating: Int?
    public var cardId: String?
    public var bidByDriver: [BidByDriver]?
    public var riderId: String?
    public var price: Float?
    public var aggregatePaymentStatus: Int?
    public var isShared: Bool? = false
    public var vehicleType: String?
    public var status: Int?
    public var id: String?
    public var startAddress: String?
    public var sentBookingReguestToDriver: [Any]?
    public var updatedAt: String?
    public var v: Int?
    public var driverPushInQ: [Any]?
    public var bidRejectedByDriver: [Any]?
    public var localbookingDateTime: String?
    public var timezone: String?
    public var timezoneString: String?
    public var tripRefundAmount: Int?
    public var walletPayment: Int?
    public var driverId: String?
    public var vehicleInformation: VehicleInformation?
    
    // MARK: SwiftyJSON Initializers
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    
    public required init(json: JSON) {
        bookingDate = json[SerializationKeys.bookingDate].string
        driverReachedTime = json[SerializationKeys.driverReachedTime].int
        adminEarning = json[SerializationKeys.adminEarning].float
        driverReachedKM = json[SerializationKeys.driverReachedKM].int
        driverCancelationFee = json[SerializationKeys.driverCancelationFee].int
        if let items = json[SerializationKeys.driverDeclineInstant].array { driverDeclineInstant = items.map { $0.object} }
        if let items = json[SerializationKeys.bidByDriver].array { bidByDriver = items.map { BidByDriver(json: $0) } }
        driverEarning = json[SerializationKeys.driverEarning].float
        riderTaxApplied = json[SerializationKeys.riderTaxApplied].float
        paymentStatus = json[SerializationKeys.paymentStatus].string
        if let items = json[SerializationKeys.aggregateRequestPendingByDriver].array { aggregateRequestPendingByDriver = items.map { $0.object} }
        schedulerId = json[SerializationKeys.schedulerId].string
        endLocation = Locations(json: json[SerializationKeys.endLocation])
        tempSchedulerId = json[SerializationKeys.tempSchedulerId].string
        isFinalAggregate = json[SerializationKeys.isFinalAggregate].boolValue
        distance = json[SerializationKeys.distance].float
        if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { $0.object} }
        bridgeTax = json[SerializationKeys.bridgeTax].int
        kmTravel = json[SerializationKeys.kmTravel].int
        duration = json[SerializationKeys.duration].float
        driverTaxApplied = json[SerializationKeys.driverTaxApplied].float
        riderRating = json[SerializationKeys.riderRating].int
        startLocation = Locations(json: json[SerializationKeys.startLocation])
        tripType = json[SerializationKeys.tripType].string
        endAddress = json[SerializationKeys.endAddress].string
        riderCancelationFee = json[SerializationKeys.riderCancelationFee].int
        seatsRequired = json[SerializationKeys.seatsRequired].int
        createdAt = json[SerializationKeys.createdAt].string
        parentSharingTrip = json[SerializationKeys.parentSharingTrip].boolValue
        driverRating = json[SerializationKeys.driverRating].int
        cardId = json[SerializationKeys.cardId].string
        riderId = json[SerializationKeys.riderId].string
        price = json[SerializationKeys.price].float
        aggregatePaymentStatus = json[SerializationKeys.aggregatePaymentStatus].int
        isShared = json[SerializationKeys.isShared].boolValue
        vehicleType = json[SerializationKeys.vehicleType].string
        status = json[SerializationKeys.status].int
        id = json[SerializationKeys.id].string
        startAddress = json[SerializationKeys.startAddress].string
        if let items = json[SerializationKeys.sentBookingReguestToDriver].array { sentBookingReguestToDriver = items.map { $0.object} }
        updatedAt = json[SerializationKeys.updatedAt].string
        v = json[SerializationKeys.v].int
        if let items = json[SerializationKeys.driverPushInQ].array { driverPushInQ = items.map { $0.object} }
        if let items = json[SerializationKeys.bidRejectedByDriver].array { bidRejectedByDriver = items.map { $0.object} }
    }
    
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = bookingDate { dictionary[SerializationKeys.bookingDate] = value }
        if let value = driverReachedTime { dictionary[SerializationKeys.driverReachedTime] = value }
        if let value = adminEarning { dictionary[SerializationKeys.adminEarning] = value }
        if let value = driverReachedKM { dictionary[SerializationKeys.driverReachedKM] = value }
        if let value = driverCancelationFee { dictionary[SerializationKeys.driverCancelationFee] = value }
        if let value = driverDeclineInstant { dictionary[SerializationKeys.driverDeclineInstant] = value }
        if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
        if let value = riderTaxApplied { dictionary[SerializationKeys.riderTaxApplied] = value }
        if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
        if let value = aggregateRequestPendingByDriver { dictionary[SerializationKeys.aggregateRequestPendingByDriver] = value }
        if let value = schedulerId { dictionary[SerializationKeys.schedulerId] = value }
        if let value = endLocation { dictionary[SerializationKeys.endLocation] = value.dictionaryRepresentation() }
        if let value = tempSchedulerId { dictionary[SerializationKeys.tempSchedulerId] = value }
        dictionary[SerializationKeys.isFinalAggregate] = isFinalAggregate
        if let value = distance { dictionary[SerializationKeys.distance] = value }
        if let value = workingTime { dictionary[SerializationKeys.workingTime] = value }
        if let value = bridgeTax { dictionary[SerializationKeys.bridgeTax] = value }
        if let value = kmTravel { dictionary[SerializationKeys.kmTravel] = value }
        if let value = duration { dictionary[SerializationKeys.duration] = value }
        if let value = driverTaxApplied { dictionary[SerializationKeys.driverTaxApplied] = value }
        if let value = riderRating { dictionary[SerializationKeys.riderRating] = value }
        if let value = startLocation { dictionary[SerializationKeys.startLocation] = value.dictionaryRepresentation() }
        if let value = tripType { dictionary[SerializationKeys.tripType] = value }
        if let value = endAddress { dictionary[SerializationKeys.endAddress] = value }
        if let value = riderCancelationFee { dictionary[SerializationKeys.riderCancelationFee] = value }
        if let value = seatsRequired { dictionary[SerializationKeys.seatsRequired] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        dictionary[SerializationKeys.parentSharingTrip] = parentSharingTrip
        if let value = driverRating { dictionary[SerializationKeys.driverRating] = value }
        if let value = cardId { dictionary[SerializationKeys.cardId] = value }
        if let value = bidByDriver { dictionary[SerializationKeys.bidByDriver] = value.map { $0.dictionaryRepresentation() } }
        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
        if let value = price { dictionary[SerializationKeys.price] = value }
        if let value = aggregatePaymentStatus { dictionary[SerializationKeys.aggregatePaymentStatus] = value }
        dictionary[SerializationKeys.isShared] = isShared
        if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = startAddress { dictionary[SerializationKeys.startAddress] = value }
        if let value = sentBookingReguestToDriver { dictionary[SerializationKeys.sentBookingReguestToDriver] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = driverPushInQ { dictionary[SerializationKeys.driverPushInQ] = value }
        if let value = bidRejectedByDriver { dictionary[SerializationKeys.bidRejectedByDriver] = value }
        return dictionary
    }
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.bidRejectedByDriver = aDecoder.decodeObject(forKey: SerializationKeys.bidRejectedByDriver) as? [Any]
        self.driverReachedKM = aDecoder.decodeObject(forKey: SerializationKeys.driverReachedKM) as? Int
        self.driverDeclineInstant = aDecoder.decodeObject(forKey: SerializationKeys.driverDeclineInstant) as? [Any]
        self.localbookingDateTime = aDecoder.decodeObject(forKey: SerializationKeys.localbookingDateTime) as? String
        self.driverEarning = aDecoder.decodeObject(forKey: SerializationKeys.driverEarning) as? Float
        self.schedulerId = aDecoder.decodeObject(forKey: SerializationKeys.schedulerId) as? String
        self.paymentStatus = aDecoder.decodeObject(forKey: SerializationKeys.paymentStatus) as? String
        self.endLocation = aDecoder.decodeObject(forKey: SerializationKeys.endLocation) as? Locations
        self.isFinalAggregate = aDecoder.decodeBool(forKey: SerializationKeys.isFinalAggregate)
        self.timezone = aDecoder.decodeObject(forKey: SerializationKeys.timezone) as? String
        self.workingTime = aDecoder.decodeObject(forKey: SerializationKeys.workingTime) as? [Any]
        self.kmTravel = aDecoder.decodeObject(forKey: SerializationKeys.kmTravel) as? Int
        self.riderRating = aDecoder.decodeObject(forKey: SerializationKeys.riderRating) as? Int
        self.endAddress = aDecoder.decodeObject(forKey: SerializationKeys.endAddress) as? String
        self.riderCancelationFee = aDecoder.decodeObject(forKey: SerializationKeys.riderCancelationFee) as? Int
        self.seatsRequired = aDecoder.decodeObject(forKey: SerializationKeys.seatsRequired) as? Int
        self.parentSharingTrip = aDecoder.decodeBool(forKey: SerializationKeys.parentSharingTrip)
        self.driverRating = aDecoder.decodeObject(forKey: SerializationKeys.driverRating) as? Int
        self.cardId = aDecoder.decodeObject(forKey: SerializationKeys.cardId) as? String
        self.bidByDriver = aDecoder.decodeObject(forKey: SerializationKeys.bidByDriver) as? [BidByDriver]
        self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Float
        self.vehicleType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleType) as? String
        self.sentBookingReguestToDriver = aDecoder.decodeObject(forKey: SerializationKeys.sentBookingReguestToDriver) as? [Any]
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.timezoneString = aDecoder.decodeObject(forKey: SerializationKeys.timezoneString) as? String
        self.driverPushInQ = aDecoder.decodeObject(forKey: SerializationKeys.driverPushInQ) as? [Any]
        self.bookingDate = aDecoder.decodeObject(forKey: SerializationKeys.bookingDate) as? String
        self.driverReachedTime = aDecoder.decodeObject(forKey: SerializationKeys.driverReachedTime) as? Int
        self.driverCancelationFee = aDecoder.decodeObject(forKey: SerializationKeys.driverCancelationFee) as? Int
        self.riderTaxApplied = aDecoder.decodeObject(forKey: SerializationKeys.riderTaxApplied) as? Float
        self.aggregateRequestPendingByDriver = aDecoder.decodeObject(forKey: SerializationKeys.aggregateRequestPendingByDriver) as? [Any]
        self.tripRefundAmount = aDecoder.decodeObject(forKey: SerializationKeys.tripRefundAmount) as? Int
        self.distance = aDecoder.decodeObject(forKey: SerializationKeys.distance) as? Float
        self.walletPayment = aDecoder.decodeObject(forKey: SerializationKeys.walletPayment) as? Int
        self.bridgeTax = aDecoder.decodeObject(forKey: SerializationKeys.bridgeTax) as? Int
        self.startLocation = aDecoder.decodeObject(forKey: SerializationKeys.startLocation) as? Locations
        self.duration = aDecoder.decodeObject(forKey: SerializationKeys.duration) as? Float
        self.driverTaxApplied = aDecoder.decodeObject(forKey: SerializationKeys.driverTaxApplied) as? Float
        self.tripType = aDecoder.decodeObject(forKey: SerializationKeys.tripType) as? String
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.aggregatePaymentStatus = aDecoder.decodeObject(forKey: SerializationKeys.aggregatePaymentStatus) as? Int
        self.isShared = aDecoder.decodeBool(forKey: SerializationKeys.isShared)
        self.riderId = aDecoder.decodeObject(forKey: SerializationKeys.riderId) as? String
        self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? Int
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.startAddress = aDecoder.decodeObject(forKey: SerializationKeys.startAddress) as? String
        self.v = aDecoder.decodeObject(forKey: SerializationKeys.v) as? Int
        self.driverId = aDecoder.decodeObject(forKey: SerializationKeys.driverId) as? String
        self.vehicleInformation = aDecoder.decodeObject(forKey: SerializationKeys.vehicleInformation) as? VehicleInformation
        self.adminEarning = aDecoder.decodeObject(forKey: SerializationKeys.adminEarning) as? Float
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(bidRejectedByDriver, forKey: SerializationKeys.bidRejectedByDriver)
        aCoder.encode(driverReachedKM, forKey: SerializationKeys.driverReachedKM)
        aCoder.encode(driverDeclineInstant, forKey: SerializationKeys.driverDeclineInstant)
        aCoder.encode(localbookingDateTime, forKey: SerializationKeys.localbookingDateTime)
        aCoder.encode(driverEarning, forKey: SerializationKeys.driverEarning)
        aCoder.encode(schedulerId, forKey: SerializationKeys.schedulerId)
        aCoder.encode(paymentStatus, forKey: SerializationKeys.paymentStatus)
        aCoder.encode(endLocation, forKey: SerializationKeys.endLocation)
        aCoder.encode(isFinalAggregate, forKey: SerializationKeys.isFinalAggregate)
        aCoder.encode(timezone, forKey: SerializationKeys.timezone)
        aCoder.encode(workingTime, forKey: SerializationKeys.workingTime)
        aCoder.encode(kmTravel, forKey: SerializationKeys.kmTravel)
        aCoder.encode(riderRating, forKey: SerializationKeys.riderRating)
        aCoder.encode(endAddress, forKey: SerializationKeys.endAddress)
        aCoder.encode(riderCancelationFee, forKey: SerializationKeys.riderCancelationFee)
        aCoder.encode(seatsRequired, forKey: SerializationKeys.seatsRequired)
        aCoder.encode(parentSharingTrip, forKey: SerializationKeys.parentSharingTrip)
        aCoder.encode(driverRating, forKey: SerializationKeys.driverRating)
        aCoder.encode(cardId, forKey: SerializationKeys.cardId)
        aCoder.encode(bidByDriver, forKey: SerializationKeys.bidByDriver)
        aCoder.encode(price, forKey: SerializationKeys.price)
        aCoder.encode(vehicleType, forKey: SerializationKeys.vehicleType)
        aCoder.encode(sentBookingReguestToDriver, forKey: SerializationKeys.sentBookingReguestToDriver)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(timezoneString, forKey: SerializationKeys.timezoneString)
        aCoder.encode(driverPushInQ, forKey: SerializationKeys.driverPushInQ)
        aCoder.encode(bookingDate, forKey: SerializationKeys.bookingDate)
        aCoder.encode(driverReachedTime, forKey: SerializationKeys.driverReachedTime)
        aCoder.encode(driverCancelationFee, forKey: SerializationKeys.driverCancelationFee)
        aCoder.encode(riderTaxApplied, forKey: SerializationKeys.riderTaxApplied)
        aCoder.encode(aggregateRequestPendingByDriver, forKey: SerializationKeys.aggregateRequestPendingByDriver)
        aCoder.encode(tripRefundAmount, forKey: SerializationKeys.tripRefundAmount)
        aCoder.encode(distance, forKey: SerializationKeys.distance)
        aCoder.encode(walletPayment, forKey: SerializationKeys.walletPayment)
        aCoder.encode(bridgeTax, forKey: SerializationKeys.bridgeTax)
        aCoder.encode(startLocation, forKey: SerializationKeys.startLocation)
        aCoder.encode(duration, forKey: SerializationKeys.duration)
        aCoder.encode(driverTaxApplied, forKey: SerializationKeys.driverTaxApplied)
        aCoder.encode(tripType, forKey: SerializationKeys.tripType)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(aggregatePaymentStatus, forKey: SerializationKeys.aggregatePaymentStatus)
        aCoder.encode(isShared, forKey: SerializationKeys.isShared)
        aCoder.encode(riderId, forKey: SerializationKeys.riderId)
        aCoder.encode(status, forKey: SerializationKeys.status)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(startAddress, forKey: SerializationKeys.startAddress)
        aCoder.encode(v, forKey: SerializationKeys.v)
        aCoder.encode(driverId, forKey: SerializationKeys.driverId)
        aCoder.encode(vehicleInformation, forKey: SerializationKeys.vehicleInformation)
        aCoder.encode(adminEarning, forKey: SerializationKeys.adminEarning)
    }
}
public final class BidByDriver : NSObject,NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let offerPrice = "offerPrice"
        static let driveId = "driveId"
        static let id = "_id"
        static let offerPriceByDriver = "offerPriceByDriver"
    }
    
    // MARK: Properties
    public var offerPrice: Int?
    public var driveId: String?
    public var id: String?
    public var offerPriceByDriver: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        offerPrice = json[SerializationKeys.offerPrice].int
        driveId = json[SerializationKeys.driveId].string
        id = json[SerializationKeys.id].string
        offerPriceByDriver = json[SerializationKeys.offerPriceByDriver].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = offerPrice { dictionary[SerializationKeys.offerPrice] = value }
        if let value = driveId { dictionary[SerializationKeys.driveId] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = offerPriceByDriver { dictionary[SerializationKeys.offerPriceByDriver] = value }
        return dictionary
    }
    
    required public init(coder aDecoder: NSCoder) {
        self.driveId = aDecoder.decodeObject(forKey: SerializationKeys.driveId) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.offerPrice = aDecoder.decodeObject(forKey: SerializationKeys.offerPrice) as? Int
        self.offerPriceByDriver = aDecoder.decodeObject(forKey: SerializationKeys.offerPriceByDriver) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
//        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)

        aCoder.encode(self.driveId, forKey: SerializationKeys.driveId)
        aCoder.encode(self.id, forKey: SerializationKeys.id)
        aCoder.encode(self.offerPrice, forKey: SerializationKeys.offerPrice)
        aCoder.encode(self.offerPriceByDriver, forKey: SerializationKeys.offerPriceByDriver)
    }
    
}
public final class RiderData : NSObject,NSCoding{
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let avgRating = "avgRating"
        static let email = "email"
        static let id = "_id"
        static let mobile = "mobile"
        static let fullName = "fullName"
        static let pic = "pic"
        static let totalNumberRefferal = "totalNumberRefferal"
                static let isBlocked = "isBlocked"
                static let arrivalTime = "arrivalTime"
                static let riderStatus = "riderStatus"
                static let workAddress = "workAddress"
                static let totalRefferalAmount = "totalRefferalAmount"
                static let homeAddress = "homeAddress"
                static let isVerified = "isVerified"
                static let deviceToken = "deviceToken"
                static let totalCredits = "totalCredits"
                static let password = "password"
                static let lan = "lan"
                static let deviceType = "deviceType"
                static let createdAt = "createdAt"
                static let accessToken = "accessToken"
                static let promotionTotalCredits = "promotionTotalCredits"
                static let promotioncode = "promotioncode"
                static let isDeleted = "isDeleted"
                static let currentLocation = "currentLocation"
                static let v = "__v"
                static let updatedAt = "updatedAt"
                static let homeLocation = "homeLocation"
                static let badgeCount = "badgeCount"
                static let refferalCode = "refferalCode"
                static let workLocation = "workLocation"
    }
    
    // MARK: Properties
    public var avgRating: Double?
    public var email: String?
    public var id: String?
    public var mobile: String?
    public var fullName: String?
    public var pic: Pic?
    public var totalNumberRefferal: Int?
        public var isBlocked: Bool? = false
        public var arrivalTime: Int?
        public var riderStatus: String?
        public var workAddress: String?
        public var totalRefferalAmount: Int?
        public var homeAddress: String?
        public var isVerified: Bool? = false
        public var deviceToken: String?
        public var totalCredits: Int?
        public var password: String?
        public var lan: String?
        public var deviceType: String?
        public var createdAt: String?
        public var accessToken: String?
        public var promotionTotalCredits: Int?
        public var promotioncode: String?
        public var isDeleted: Bool? = false
        public var currentLocation: Locations?
        public var v: Int?
        public var updatedAt: String?
        public var homeLocation: Locations?
        public var badgeCount: Int?
        public var refferalCode: String?
        public var workLocation: Locations?
    
    // MARK: SwiftyJSON Initializers
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    
    public required init(json: JSON) {
        avgRating = json[SerializationKeys.avgRating].double
        email = json[SerializationKeys.email].string
        id = json[SerializationKeys.id].string
        mobile = json[SerializationKeys.mobile].string
        fullName = json[SerializationKeys.fullName].string
        pic = Pic(json: json[SerializationKeys.pic])
    }
    
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = avgRating { dictionary[SerializationKeys.avgRating] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = mobile { dictionary[SerializationKeys.mobile] = value }
        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
        if let value = pic { dictionary[SerializationKeys.pic] = value.dictionaryRepresentation() }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.totalNumberRefferal = aDecoder.decodeObject(forKey: SerializationKeys.totalNumberRefferal) as? Int
        self.isBlocked = aDecoder.decodeBool(forKey: SerializationKeys.isBlocked)
        self.arrivalTime = aDecoder.decodeObject(forKey: SerializationKeys.arrivalTime) as? Int
        self.riderStatus = aDecoder.decodeObject(forKey: SerializationKeys.riderStatus) as? String
        self.avgRating = aDecoder.decodeObject(forKey: SerializationKeys.avgRating) as? Double
        self.workAddress = aDecoder.decodeObject(forKey: SerializationKeys.workAddress) as? String
        self.totalRefferalAmount = aDecoder.decodeObject(forKey: SerializationKeys.totalRefferalAmount) as? Int
        self.homeAddress = aDecoder.decodeObject(forKey: SerializationKeys.homeAddress) as? String
        self.isVerified = aDecoder.decodeBool(forKey: SerializationKeys.isVerified)
        self.deviceToken = aDecoder.decodeObject(forKey: SerializationKeys.deviceToken) as? String
        self.totalCredits = aDecoder.decodeObject(forKey: SerializationKeys.totalCredits) as? Int
        self.fullName = aDecoder.decodeObject(forKey: SerializationKeys.fullName) as? String
        self.password = aDecoder.decodeObject(forKey: SerializationKeys.password) as? String
        self.lan = aDecoder.decodeObject(forKey: SerializationKeys.lan) as? String
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.deviceType = aDecoder.decodeObject(forKey: SerializationKeys.deviceType) as? String
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.accessToken = aDecoder.decodeObject(forKey: SerializationKeys.accessToken) as? String
        self.mobile = aDecoder.decodeObject(forKey: SerializationKeys.mobile) as? String
        self.pic = aDecoder.decodeObject(forKey: SerializationKeys.pic) as? Pic
        self.promotionTotalCredits = aDecoder.decodeObject(forKey: SerializationKeys.promotionTotalCredits) as? Int
        self.promotioncode = aDecoder.decodeObject(forKey: SerializationKeys.promotioncode) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.isDeleted = aDecoder.decodeBool(forKey: SerializationKeys.isDeleted)
        self.currentLocation = aDecoder.decodeObject(forKey: SerializationKeys.currentLocation) as? Locations
        self.v = aDecoder.decodeObject(forKey: SerializationKeys.v) as? Int
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.homeLocation = aDecoder.decodeObject(forKey: SerializationKeys.homeLocation) as? Locations
        self.badgeCount = aDecoder.decodeObject(forKey: SerializationKeys.badgeCount) as? Int
        self.refferalCode = aDecoder.decodeObject(forKey: SerializationKeys.refferalCode) as? String
        self.workLocation = aDecoder.decodeObject(forKey: SerializationKeys.workLocation) as? Locations
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(totalNumberRefferal, forKey: SerializationKeys.totalNumberRefferal)
        aCoder.encode(isBlocked, forKey: SerializationKeys.isBlocked)
        aCoder.encode(arrivalTime, forKey: SerializationKeys.arrivalTime)
        aCoder.encode(riderStatus, forKey: SerializationKeys.riderStatus)
        aCoder.encode(avgRating, forKey: SerializationKeys.avgRating)
        aCoder.encode(workAddress, forKey: SerializationKeys.workAddress)
        aCoder.encode(totalRefferalAmount, forKey: SerializationKeys.totalRefferalAmount)
        aCoder.encode(homeAddress, forKey: SerializationKeys.homeAddress)
        aCoder.encode(isVerified, forKey: SerializationKeys.isVerified)
        aCoder.encode(deviceToken, forKey: SerializationKeys.deviceToken)
        aCoder.encode(totalCredits, forKey: SerializationKeys.totalCredits)
        aCoder.encode(fullName, forKey: SerializationKeys.fullName)
        aCoder.encode(password, forKey: SerializationKeys.password)
        aCoder.encode(lan, forKey: SerializationKeys.lan)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(deviceType, forKey: SerializationKeys.deviceType)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(accessToken, forKey: SerializationKeys.accessToken)
        aCoder.encode(mobile, forKey: SerializationKeys.mobile)
        aCoder.encode(pic, forKey: SerializationKeys.pic)
        aCoder.encode(promotionTotalCredits, forKey: SerializationKeys.promotionTotalCredits)
        aCoder.encode(promotioncode, forKey: SerializationKeys.promotioncode)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(isDeleted, forKey: SerializationKeys.isDeleted)
        aCoder.encode(currentLocation, forKey: SerializationKeys.currentLocation)
        aCoder.encode(v, forKey: SerializationKeys.v)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(homeLocation, forKey: SerializationKeys.homeLocation)
        aCoder.encode(badgeCount, forKey: SerializationKeys.badgeCount)
        aCoder.encode(refferalCode, forKey: SerializationKeys.refferalCode)
        aCoder.encode(workLocation, forKey: SerializationKeys.workLocation)
    }
    
}
public final class Locations : NSObject,NSCoding{
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
        self.coordinates = aDecoder.decodeObject(forKey: SerializationKeys.coordinates) as? [Float]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(type, forKey: SerializationKeys.type)
        aCoder.encode(coordinates, forKey: SerializationKeys.coordinates)
    }
}

//public final class EndLocation : NSObject {
//
//    // MARK: Declaration for string constants to be used to decode and also serialize.
//    private struct SerializationKeys {
//        static let type = "type"
//        static let coordinates = "coordinates"
//    }
//
//    // MARK: Properties
//    public var type: String?
//    public var coordinates: [Float]?
//
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//
//
//    public required init(json: JSON) {
//        type = json[SerializationKeys.type].string
//        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
//    }
//
//    public func dictionaryRepresentation() -> [String: Any] {
//        var dictionary: [String: Any] = [:]
//        if let value = type { dictionary[SerializationKeys.type] = value }
//        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
//        return dictionary
//    }
//}

//public final class StartLocation : NSObject{
//
//    // MARK: Declaration for string constants to be used to decode and also serialize.
//    private struct SerializationKeys {
//        static let type = "type"
//        static let coordinates = "coordinates"
//    }
//
//    // MARK: Properties
//    public var type: String?
//    public var coordinates: [Float]?
//
//
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//
//
//    public required init(json: JSON) {
//        type = json[SerializationKeys.type].string
//        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
//    }
//
//
//    public func dictionaryRepresentation() -> [String: Any] {
//        var dictionary: [String: Any] = [:]
//        if let value = type { dictionary[SerializationKeys.type] = value }
//        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
//        return dictionary
//    }
//
//}
public final class Pic : NSObject,NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let thumb = "thumb"
        static let original = "original"
    }
    
    // MARK: Properties
    public var thumb: String?
    public var original: String?
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    public required init(json: JSON) {
        thumb = json[SerializationKeys.thumb].string
        original = json[SerializationKeys.original].string
    }
    
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = thumb { dictionary[SerializationKeys.thumb] = value }
        if let value = original { dictionary[SerializationKeys.original] = value }
        return dictionary
    }
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.thumb = aDecoder.decodeObject(forKey: SerializationKeys.thumb) as? String
        self.original = aDecoder.decodeObject(forKey: SerializationKeys.original) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(thumb, forKey: SerializationKeys.thumb)
        aCoder.encode(original, forKey: SerializationKeys.original)
    }
}
