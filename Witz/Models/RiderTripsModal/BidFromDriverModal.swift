//
//  BidFromDriverModal.swift
//
//  Created by Amit Tripathi on 12/21/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BidFromDriverModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bookingDate = "bookingDate"
    static let todayDateDateTime = "todayDateDateTime"
    static let timezone = "timezone"
    static let createdAt = "createdAt"
    static let todayDateTimestamp = "todayDateTimestamp"
    static let driverData = "driverData"
    static let localTime = "localTime"
  }

  // MARK: Properties
  public var bookingDate: String?
  public var todayDateDateTime: String?
  public var timezone: String?
  public var createdAt: String?
  public var todayDateTimestamp: Int?
  public var driverData: [DriverDataRider]?
  public var localTime: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    bookingDate = json[SerializationKeys.bookingDate].string
    todayDateDateTime = json[SerializationKeys.todayDateDateTime].string
    timezone = json[SerializationKeys.timezone].string
    createdAt = json[SerializationKeys.createdAt].string
    todayDateTimestamp = json[SerializationKeys.todayDateTimestamp].int
    if let items = json[SerializationKeys.driverData].array { driverData = items.map { DriverDataRider(json: $0) } }
    localTime = json[SerializationKeys.localTime].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = bookingDate { dictionary[SerializationKeys.bookingDate] = value }
    if let value = todayDateDateTime { dictionary[SerializationKeys.todayDateDateTime] = value }
    if let value = timezone { dictionary[SerializationKeys.timezone] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = todayDateTimestamp { dictionary[SerializationKeys.todayDateTimestamp] = value }
    if let value = driverData { dictionary[SerializationKeys.driverData] = value.map { $0.dictionaryRepresentation() } }
    if let value = localTime { dictionary[SerializationKeys.localTime] = value }
    return dictionary
  }

}

public final class DriverDataRider {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let bankInformation = "bank_information"
        static let totalNumberRefferal = "totalNumberRefferal"
        static let documents = "documents"
        static let isBlocked = "isBlocked"
        static let arrivalTime = "arrivalTime"
        static let avgRating = "avgRating"
        static let totalTrips = "totalTrips"
        static let totalRefferalAmount = "totalRefferalAmount"
        static let type = "type"
        static let welcomeWallet = "welcomeWallet"
        static let personalInformation = "personal_information"
        static let isVerified = "isVerified"
        static let deviceToken = "deviceToken"
        static let totalCredits = "totalCredits"
        static let fullName = "fullName"
        static let fiveStarTrips = "fiveStarTrips"
        static let password = "password"
        static let updatedLastLocAt = "updatedLastLocAt"
        static let lan = "lan"
        static let arrivalDistance = "arrivalDistance"
        static let offerPrice = "offerPrice"
        static let deviceType = "deviceType"
        static let createdAt = "createdAt"
        static let mobile = "mobile"
        static let accessToken = "accessToken"
        static let pic = "pic"
        static let totalKm = "totalKm"
        static let status = "status"
        static let documentsActivityStatus = "documentsActivityStatus"
        static let id = "_id"
        static let isDeleted = "isDeleted"
        static let currentLocation = "currentLocation"
        static let updatedAt = "updatedAt"
        static let accType = "acc_type"
        static let v = "__v"
        static let ratedTrips = "ratedTrips"
        static let vehicleInformation = "vehicle_information"
        static let badgeCount = "badgeCount"
        static let refferalCode = "refferalCode"
    }
    
    // MARK: Properties
   
    public var totalNumberRefferal: Int?

    public var isBlocked: Bool? = false
    public var arrivalTime: Int?
    public var avgRating: Float?
    public var totalTrips: Int?
    public var totalRefferalAmount: Int?
    public var type: String?
    public var welcomeWallet: Int?
    public var personalInformation: PersonalInformationRider?
    public var isVerified: Bool? = false
    public var deviceToken: String?
    public var totalCredits: Float?
    public var fullName: String?
    public var fiveStarTrips: Int?
    public var password: String?
    public var updatedLastLocAt: String?
    public var lan: String?
    public var arrivalDistance: Int?
    public var offerPrice: Float?
    public var deviceType: String?
    public var createdAt: String?
    public var mobile: String?
    public var accessToken: String?
    public var pic: Pic?
    public var totalKm: Int?
    public var status: String?
    public var documentsActivityStatus: Int?
    public var id: String?
    public var isDeleted: Bool? = false

    public var updatedAt: String?
    public var accType: String?
    public var v: Int?
    public var ratedTrips: Int?
    public var vehicleInformation: VehicleInformationRider?
    public var badgeCount: Int?
    public var refferalCode: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        totalNumberRefferal = json[SerializationKeys.totalNumberRefferal].int
        
        isBlocked = json[SerializationKeys.isBlocked].boolValue
        arrivalTime = json[SerializationKeys.arrivalTime].int
        avgRating = json[SerializationKeys.avgRating].float
        totalTrips = json[SerializationKeys.totalTrips].int
        totalRefferalAmount = json[SerializationKeys.totalRefferalAmount].int
        type = json[SerializationKeys.type].string
        welcomeWallet = json[SerializationKeys.welcomeWallet].int
        personalInformation = PersonalInformationRider(json: json[SerializationKeys.personalInformation])
        isVerified = json[SerializationKeys.isVerified].boolValue
        deviceToken = json[SerializationKeys.deviceToken].string
        totalCredits = json[SerializationKeys.totalCredits].float
        fullName = json[SerializationKeys.fullName].string
        fiveStarTrips = json[SerializationKeys.fiveStarTrips].int
        password = json[SerializationKeys.password].string
        updatedLastLocAt = json[SerializationKeys.updatedLastLocAt].string
        lan = json[SerializationKeys.lan].string
        arrivalDistance = json[SerializationKeys.arrivalDistance].int
        offerPrice = json[SerializationKeys.offerPrice].float
        deviceType = json[SerializationKeys.deviceType].string
        createdAt = json[SerializationKeys.createdAt].string
        mobile = json[SerializationKeys.mobile].string
        accessToken = json[SerializationKeys.accessToken].string
        pic = Pic(json: json[SerializationKeys.pic])
        totalKm = json[SerializationKeys.totalKm].int
        status = json[SerializationKeys.status].string
        documentsActivityStatus = json[SerializationKeys.documentsActivityStatus].int
        id = json[SerializationKeys.id].string
        isDeleted = json[SerializationKeys.isDeleted].boolValue
        
        updatedAt = json[SerializationKeys.updatedAt].string
        accType = json[SerializationKeys.accType].string
        v = json[SerializationKeys.v].int
        ratedTrips = json[SerializationKeys.ratedTrips].int
        vehicleInformation = VehicleInformationRider(json: json[SerializationKeys.vehicleInformation])
        badgeCount = json[SerializationKeys.badgeCount].int
        refferalCode = json[SerializationKeys.refferalCode].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        if let value = totalNumberRefferal { dictionary[SerializationKeys.totalNumberRefferal] = value }
        dictionary[SerializationKeys.isBlocked] = isBlocked
        if let value = arrivalTime { dictionary[SerializationKeys.arrivalTime] = value }
        if let value = avgRating { dictionary[SerializationKeys.avgRating] = value }
        if let value = totalTrips { dictionary[SerializationKeys.totalTrips] = value }
        if let value = totalRefferalAmount { dictionary[SerializationKeys.totalRefferalAmount] = value }
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = welcomeWallet { dictionary[SerializationKeys.welcomeWallet] = value }
        if let value = personalInformation { dictionary[SerializationKeys.personalInformation] = value.dictionaryRepresentation() }
        dictionary[SerializationKeys.isVerified] = isVerified
        if let value = deviceToken { dictionary[SerializationKeys.deviceToken] = value }
        if let value = totalCredits { dictionary[SerializationKeys.totalCredits] = value }
        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
        if let value = fiveStarTrips { dictionary[SerializationKeys.fiveStarTrips] = value }
        if let value = password { dictionary[SerializationKeys.password] = value }
        if let value = updatedLastLocAt { dictionary[SerializationKeys.updatedLastLocAt] = value }
        if let value = lan { dictionary[SerializationKeys.lan] = value }
        if let value = arrivalDistance { dictionary[SerializationKeys.arrivalDistance] = value }
        if let value = offerPrice { dictionary[SerializationKeys.offerPrice] = value }
        if let value = deviceType { dictionary[SerializationKeys.deviceType] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = mobile { dictionary[SerializationKeys.mobile] = value }
        if let value = accessToken { dictionary[SerializationKeys.accessToken] = value }
        if let value = pic { dictionary[SerializationKeys.pic] = value.dictionaryRepresentation() }
        if let value = totalKm { dictionary[SerializationKeys.totalKm] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = documentsActivityStatus { dictionary[SerializationKeys.documentsActivityStatus] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        dictionary[SerializationKeys.isDeleted] = isDeleted
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = accType { dictionary[SerializationKeys.accType] = value }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = ratedTrips { dictionary[SerializationKeys.ratedTrips] = value }
        if let value = vehicleInformation { dictionary[SerializationKeys.vehicleInformation] = value.dictionaryRepresentation() }
        if let value = badgeCount { dictionary[SerializationKeys.badgeCount] = value }
        if let value = refferalCode { dictionary[SerializationKeys.refferalCode] = value }
        return dictionary
    }
    
}
public final class PersonalInformationRider {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let address = "address"
        static let email = "email"
        static let fullName = "fullName"
        static let isVerified = "isVerified"
        static let companyName = "company_name"
    }
    
    // MARK: Properties
    public var address: String?
    public var email: String?
    public var fullName: String?
    public var isVerified: Bool? = false
    public var companyName: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        address = json[SerializationKeys.address].string
        email = json[SerializationKeys.email].string
        fullName = json[SerializationKeys.fullName].string
        isVerified = json[SerializationKeys.isVerified].boolValue
        companyName = json[SerializationKeys.companyName].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
        dictionary[SerializationKeys.isVerified] = isVerified
        if let value = companyName { dictionary[SerializationKeys.companyName] = value }
        return dictionary
    }
    
}
public final class VehicleInformationRider {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let brand = "brand"
        static let subBrand = "subBrand"
        static let year = "year"
        static let model = "model"
        static let vehicleNumber = "vehicle_number"
        static let seatingCapacity = "seating_capacity"
        static let isVerified = "isVerified"
        static let color = "color"
    }
    
    // MARK: Properties
    public var brand: String?
    public var subBrand: String?
    public var year: String?
    public var model: String?
    public var vehicleNumber: String?
    public var seatingCapacity: String?
    public var isVerified: Bool? = false
    public var color: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        brand = json[SerializationKeys.brand].string
        subBrand = json[SerializationKeys.subBrand].string
        year = json[SerializationKeys.year].string
        model = json[SerializationKeys.model].string
        vehicleNumber = json[SerializationKeys.vehicleNumber].string
        seatingCapacity = json[SerializationKeys.seatingCapacity].string
        isVerified = json[SerializationKeys.isVerified].boolValue
        color = json[SerializationKeys.color].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = brand { dictionary[SerializationKeys.brand] = value }
        if let value = subBrand { dictionary[SerializationKeys.subBrand] = value }
        if let value = year { dictionary[SerializationKeys.year] = value }
        if let value = model { dictionary[SerializationKeys.model] = value }
        if let value = vehicleNumber { dictionary[SerializationKeys.vehicleNumber] = value }
        if let value = seatingCapacity { dictionary[SerializationKeys.seatingCapacity] = value }
        dictionary[SerializationKeys.isVerified] = isVerified
        if let value = color { dictionary[SerializationKeys.color] = value }
        return dictionary
    }
    
}
