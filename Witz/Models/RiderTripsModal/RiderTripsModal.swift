//
//  RiderTripsModal.swift
//
//  Created by Amit Tripathi on 12/21/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class RiderTripsModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let todayDateTimestamp = "todayDateTimestamp"
    static let count = "count"
    static let todayDateDateTime = "todayDateDateTime"
    static let trips = "trips"
  }

  // MARK: Properties
  public var todayDateTimestamp: Int?
  public var count: Int?
  public var todayDateDateTime: String?
  public var trips: [TripsRider]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    todayDateTimestamp = json[SerializationKeys.todayDateTimestamp].int
    count = json[SerializationKeys.count].int
    todayDateDateTime = json[SerializationKeys.todayDateDateTime].string
    if let items = json[SerializationKeys.trips].array { trips = items.map { TripsRider(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = todayDateTimestamp { dictionary[SerializationKeys.todayDateTimestamp] = value }
    if let value = count { dictionary[SerializationKeys.count] = value }
    if let value = todayDateDateTime { dictionary[SerializationKeys.todayDateDateTime] = value }
    if let value = trips { dictionary[SerializationKeys.trips] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}

public final class TripsRider {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let adminEarning = "adminEarning"
        static let driverReachedKM = "driverReachedKM"
        static let driverDeclineInstant = "driverDeclineInstant"
        static let localbookingDateTime = "localbookingDateTime"
        static let driverEarning = "driverEarning"
        static let schedulerId = "schedulerId"
        static let paymentStatus = "paymentStatus"
        static let endLocation = "endLocation"
        static let isFinalAggregate = "isFinalAggregate"
        static let timezone = "timezone"
        static let workingTime = "workingTime"
        static let transId = "transId"
        static let kmTravel = "kmTravel"
        static let riderRating = "riderRating"
        static let wayEndPoints = "wayEndPoints"
        static let endAddress = "end_address"
        static let riderCancelationFee = "riderCancelationFee"
        static let seatsRequired = "seatsRequired"
        static let parentSharingTrip = "parentSharingTrip"
        static let driverRating = "driverRating"
        static let cardId = "cardId"
        static let bidByDriver = "bidByDriver"
        static let price = "price"
        static let vehicleType = "vehicleType"
        static let sentBookingReguestToDriver = "sentBookingReguestToDriver"
        static let updatedAt = "updatedAt"
        static let timezoneString = "timezoneString"
        static let driverPushInQ = "driverPushInQ"
        static let bookingDate = "bookingDate"
        static let driverReachedTime = "driverReachedTime"
        static let driverCancelationFee = "driverCancelationFee"
        static let riderTaxApplied = "riderTaxApplied"
        static let aggregateRequestPendingByDriver = "aggregateRequestPendingByDriver"
        static let tripRefundAmount = "tripRefundAmount"
        static let distance = "distance"
        static let walletPayment = "walletPayment"
        static let bridgeTax = "bridgeTax"
        static let startLocation = "startLocation"
        static let duration = "duration"
        static let driverTaxApplied = "driverTaxApplied"
        static let tripType = "tripType"
        static let createdAt = "createdAt"
        static let aggregatePaymentStatus = "aggregatePaymentStatus"
        static let isShared = "isShared"
        static let riderId = "riderId"
        static let status = "status"
        static let id = "_id"
        static let startAddress = "start_address"
        static let v = "__v"
        static let driverId = "driverId"
        static let bidRejectedByDriver = "bidRejectedByDriver"
    }
    
    // MARK: Properties
    public var adminEarning: Float?
    public var driverReachedKM: Int?
    public var driverDeclineInstant: [String]?
    public var localbookingDateTime: String?
    public var driverEarning: Float?
    public var schedulerId: String?
    public var paymentStatus: String?
    public var endLocation: EndLocation_Rider?
    public var isFinalAggregate: Bool? = false
    public var timezone: String?
    public var workingTime: [Any]?
    public var transId: String?
    public var kmTravel: Int?
    public var riderRating: Int?
    public var wayEndPoints: [WayEndPoints_Rider]?
    public var endAddress: String?
    public var riderCancelationFee: Float?
    public var seatsRequired: Int?
    public var parentSharingTrip: Bool? = false
    public var driverRating: Int?
    public var cardId: String?
    public var bidByDriver: [BidByDriver_Rider]?
    public var price: Float?
    public var vehicleType: String?
    public var sentBookingReguestToDriver: [Any]?
    public var updatedAt: String?
    public var timezoneString: String?
    public var driverPushInQ: [Any]?
    public var bookingDate: String?
    public var driverReachedTime: Int?
    public var driverCancelationFee: Int?
    public var riderTaxApplied: Float?
    public var aggregateRequestPendingByDriver: [Any]?
    public var tripRefundAmount: Int?
    public var distance: Float?
    public var walletPayment: Float?
    public var bridgeTax: Int?
    public var startLocation: StartLocation_Rider?
    public var duration: Int?
    public var driverTaxApplied: Float?
    public var tripType: String?
    public var createdAt: String?
    public var aggregatePaymentStatus: Int?
    public var isShared: Bool? = false
    public var riderId: String?
    public var status: Int?
    public var id: String?
    public var startAddress: String?
    public var v: Int?
    public var driverId: String?
    public var bidRejectedByDriver: [Any]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        adminEarning = json[SerializationKeys.adminEarning].float
        driverReachedKM = json[SerializationKeys.driverReachedKM].int
        if let items = json[SerializationKeys.driverDeclineInstant].array { driverDeclineInstant = items.map { $0.stringValue } }
        localbookingDateTime = json[SerializationKeys.localbookingDateTime].string
        driverEarning = json[SerializationKeys.driverEarning].float
        schedulerId = json[SerializationKeys.schedulerId].string
        paymentStatus = json[SerializationKeys.paymentStatus].string
        endLocation = EndLocation_Rider(json: json[SerializationKeys.endLocation])
        isFinalAggregate = json[SerializationKeys.isFinalAggregate].boolValue
        timezone = json[SerializationKeys.timezone].string
        if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { $0.object} }
        transId = json[SerializationKeys.transId].string
        kmTravel = json[SerializationKeys.kmTravel].int
        riderRating = json[SerializationKeys.riderRating].int
        if let items = json[SerializationKeys.wayEndPoints].array { wayEndPoints = items.map { WayEndPoints_Rider(json: $0) } }
        endAddress = json[SerializationKeys.endAddress].string
        riderCancelationFee = json[SerializationKeys.riderCancelationFee].float
        seatsRequired = json[SerializationKeys.seatsRequired].int
        parentSharingTrip = json[SerializationKeys.parentSharingTrip].boolValue
        driverRating = json[SerializationKeys.driverRating].int
        cardId = json[SerializationKeys.cardId].string
        if let items = json[SerializationKeys.bidByDriver].array { bidByDriver = items.map { BidByDriver_Rider(json: $0) } }
        price = json[SerializationKeys.price].float
        vehicleType = json[SerializationKeys.vehicleType].string
        if let items = json[SerializationKeys.sentBookingReguestToDriver].array { sentBookingReguestToDriver = items.map { $0.object} }
        updatedAt = json[SerializationKeys.updatedAt].string
        timezoneString = json[SerializationKeys.timezoneString].string
        if let items = json[SerializationKeys.driverPushInQ].array { driverPushInQ = items.map { $0.object} }
        bookingDate = json[SerializationKeys.bookingDate].string
        driverReachedTime = json[SerializationKeys.driverReachedTime].int
        driverCancelationFee = json[SerializationKeys.driverCancelationFee].int
        riderTaxApplied = json[SerializationKeys.riderTaxApplied].float
        if let items = json[SerializationKeys.aggregateRequestPendingByDriver].array { aggregateRequestPendingByDriver = items.map { $0.object} }
        tripRefundAmount = json[SerializationKeys.tripRefundAmount].int
        distance = json[SerializationKeys.distance].float
        walletPayment = json[SerializationKeys.walletPayment].float
        bridgeTax = json[SerializationKeys.bridgeTax].int
        startLocation = StartLocation_Rider(json: json[SerializationKeys.startLocation])
        duration = json[SerializationKeys.duration].int
        driverTaxApplied = json[SerializationKeys.driverTaxApplied].float
        tripType = json[SerializationKeys.tripType].string
        createdAt = json[SerializationKeys.createdAt].string
        aggregatePaymentStatus = json[SerializationKeys.aggregatePaymentStatus].int
        isShared = json[SerializationKeys.isShared].boolValue
        riderId = json[SerializationKeys.riderId].string
        status = json[SerializationKeys.status].int
        id = json[SerializationKeys.id].string
        startAddress = json[SerializationKeys.startAddress].string
        v = json[SerializationKeys.v].int
        driverId = json[SerializationKeys.driverId].string
        if let items = json[SerializationKeys.bidRejectedByDriver].array { bidRejectedByDriver = items.map { $0.object} }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = adminEarning { dictionary[SerializationKeys.adminEarning] = value }
        if let value = driverReachedKM { dictionary[SerializationKeys.driverReachedKM] = value }
        if let value = driverDeclineInstant { dictionary[SerializationKeys.driverDeclineInstant] = value }
        if let value = localbookingDateTime { dictionary[SerializationKeys.localbookingDateTime] = value }
        if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
        if let value = schedulerId { dictionary[SerializationKeys.schedulerId] = value }
        if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
        if let value = endLocation { dictionary[SerializationKeys.endLocation] = value.dictionaryRepresentation() }
        dictionary[SerializationKeys.isFinalAggregate] = isFinalAggregate
        if let value = timezone { dictionary[SerializationKeys.timezone] = value }
        if let value = workingTime { dictionary[SerializationKeys.workingTime] = value }
        if let value = transId { dictionary[SerializationKeys.transId] = value }
        if let value = kmTravel { dictionary[SerializationKeys.kmTravel] = value }
        if let value = riderRating { dictionary[SerializationKeys.riderRating] = value }
        if let value = wayEndPoints { dictionary[SerializationKeys.wayEndPoints] = value.map { $0.dictionaryRepresentation() } }
        if let value = endAddress { dictionary[SerializationKeys.endAddress] = value }
        if let value = riderCancelationFee { dictionary[SerializationKeys.riderCancelationFee] = value }
        if let value = seatsRequired { dictionary[SerializationKeys.seatsRequired] = value }
        dictionary[SerializationKeys.parentSharingTrip] = parentSharingTrip
        if let value = driverRating { dictionary[SerializationKeys.driverRating] = value }
        if let value = cardId { dictionary[SerializationKeys.cardId] = value }
        if let value = bidByDriver { dictionary[SerializationKeys.bidByDriver] = value.map { $0.dictionaryRepresentation() } }
        if let value = price { dictionary[SerializationKeys.price] = value }
        if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
        if let value = sentBookingReguestToDriver { dictionary[SerializationKeys.sentBookingReguestToDriver] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = timezoneString { dictionary[SerializationKeys.timezoneString] = value }
        if let value = driverPushInQ { dictionary[SerializationKeys.driverPushInQ] = value }
        if let value = bookingDate { dictionary[SerializationKeys.bookingDate] = value }
        if let value = driverReachedTime { dictionary[SerializationKeys.driverReachedTime] = value }
        if let value = driverCancelationFee { dictionary[SerializationKeys.driverCancelationFee] = value }
        if let value = riderTaxApplied { dictionary[SerializationKeys.riderTaxApplied] = value }
        if let value = aggregateRequestPendingByDriver { dictionary[SerializationKeys.aggregateRequestPendingByDriver] = value }
        if let value = tripRefundAmount { dictionary[SerializationKeys.tripRefundAmount] = value }
        if let value = distance { dictionary[SerializationKeys.distance] = value }
        if let value = walletPayment { dictionary[SerializationKeys.walletPayment] = value }
        if let value = bridgeTax { dictionary[SerializationKeys.bridgeTax] = value }
        if let value = startLocation { dictionary[SerializationKeys.startLocation] = value.dictionaryRepresentation() }
        if let value = duration { dictionary[SerializationKeys.duration] = value }
        if let value = driverTaxApplied { dictionary[SerializationKeys.driverTaxApplied] = value }
        if let value = tripType { dictionary[SerializationKeys.tripType] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = aggregatePaymentStatus { dictionary[SerializationKeys.aggregatePaymentStatus] = value }
        dictionary[SerializationKeys.isShared] = isShared
        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = startAddress { dictionary[SerializationKeys.startAddress] = value }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = driverId { dictionary[SerializationKeys.driverId] = value }
        if let value = bidRejectedByDriver { dictionary[SerializationKeys.bidRejectedByDriver] = value }
        return dictionary
    }
    
}
public final class StartLocation_Rider {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}

public final class EndLocation_Rider {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}

public final class WayEndPoints_Rider {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
        static let id = "_id"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    public var id: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
        id = json[SerializationKeys.id].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        return dictionary
    }
    
}

public final class BidByDriver_Rider {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let offerPrice = "offerPrice"
        static let driveId = "driveId"
        static let id = "_id"
        static let offerPriceByDriver = "offerPriceByDriver"
    }
    
    // MARK: Properties
    public var offerPrice: Float?
    public var driveId: String?
    public var id: String?
    public var offerPriceByDriver: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        offerPrice = json[SerializationKeys.offerPrice].float
        driveId = json[SerializationKeys.driveId].string
        id = json[SerializationKeys.id].string
        offerPriceByDriver = json[SerializationKeys.offerPriceByDriver].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = offerPrice { dictionary[SerializationKeys.offerPrice] = value }
        if let value = driveId { dictionary[SerializationKeys.driveId] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = offerPriceByDriver { dictionary[SerializationKeys.offerPriceByDriver] = value }
        return dictionary
    }
    
}
