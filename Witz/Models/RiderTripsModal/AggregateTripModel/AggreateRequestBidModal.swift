//
//  AggreateRequestBid.swift
//
//  Created by abhishek kumar on 07/03/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AggreateRequestBidModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let notificationDate = "notificationDate"
    static let tripId = "tripId"
    static let notificationMsg = "notificationMsg"
    static let notificationUpdatedDate = "notificationUpdatedDate"
    static let groupAggregateTripId = "groupAggregateTripId"
    static let isRead = "isRead"
    static let riderId = "riderId"
    static let notificationCode = "notificationCode"
    static let id = "_id"
    static let isDeleted = "isDeleted"
    static let v = "__v"
    static let bidData = "bidData"
    static let tripType = "tripType"
  }

  // MARK: Properties
  public var notificationDate: String?
  public var tripId: TripId_BidAggregate?
  public var notificationMsg: String?
  public var notificationUpdatedDate: String?
  public var groupAggregateTripId: String?
  public var isRead: Bool? = false
  public var riderId: String?
  public var notificationCode: String?
  public var id: String?
  public var isDeleted: Bool? = false
  public var v: Int?
  public var bidData: [BidData_BidAggregate]?
  public var tripType: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    notificationDate = json[SerializationKeys.notificationDate].string
    tripId = TripId_BidAggregate(json: json[SerializationKeys.tripId])
    notificationMsg = json[SerializationKeys.notificationMsg].string
    notificationUpdatedDate = json[SerializationKeys.notificationUpdatedDate].string
    groupAggregateTripId = json[SerializationKeys.groupAggregateTripId].string
    isRead = json[SerializationKeys.isRead].boolValue
    riderId = json[SerializationKeys.riderId].string
    notificationCode = json[SerializationKeys.notificationCode].string
    id = json[SerializationKeys.id].string
    isDeleted = json[SerializationKeys.isDeleted].boolValue
    v = json[SerializationKeys.v].int
    if let items = json[SerializationKeys.bidData].array { bidData = items.map { BidData_BidAggregate(json: $0) } }
    tripType = json[SerializationKeys.tripType].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = notificationDate { dictionary[SerializationKeys.notificationDate] = value }
    if let value = tripId { dictionary[SerializationKeys.tripId] = value.dictionaryRepresentation() }
    if let value = notificationMsg { dictionary[SerializationKeys.notificationMsg] = value }
    if let value = notificationUpdatedDate { dictionary[SerializationKeys.notificationUpdatedDate] = value }
    if let value = groupAggregateTripId { dictionary[SerializationKeys.groupAggregateTripId] = value }
    dictionary[SerializationKeys.isRead] = isRead
    if let value = riderId { dictionary[SerializationKeys.riderId] = value }
    if let value = notificationCode { dictionary[SerializationKeys.notificationCode] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    dictionary[SerializationKeys.isDeleted] = isDeleted
    if let value = v { dictionary[SerializationKeys.v] = value }
    if let value = bidData { dictionary[SerializationKeys.bidData] = value.map { $0.dictionaryRepresentation() } }
    if let value = tripType { dictionary[SerializationKeys.tripType] = value }
    return dictionary
  }

}

public final class BidData_BidAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let acceptedRiders = "acceptedRiders"
        static let bidStatus = "bidStatus"
        static let minseatRequire = "minseatRequire"
        static let id = "_id"
        static let createdAt = "createdAt"
        static let groupAggregateId = "groupAggregateId"
        static let v = "__v"
        static let perseatPrice = "perseatPrice"
        static let driverId = "driverId"
        static let updatedAt = "updatedAt"
        static let bidOffer = "bidOffer"
    }
    
    // MARK: Properties
    public var acceptedRiders: [Any]?
    public var bidStatus: Int?
    public var minseatRequire: Int?
    public var id: String?
    public var createdAt: String?
    public var groupAggregateId: String?
    public var v: Int?
    public var perseatPrice: Float?
    public var driverId: DriverId_BidAggregate?
    public var updatedAt: String?
    public var bidOffer: Float?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        if let items = json[SerializationKeys.acceptedRiders].array { acceptedRiders = items.map { $0.object} }
        bidStatus = json[SerializationKeys.bidStatus].int
        minseatRequire = json[SerializationKeys.minseatRequire].int
        id = json[SerializationKeys.id].string
        createdAt = json[SerializationKeys.createdAt].string
        groupAggregateId = json[SerializationKeys.groupAggregateId].string
        v = json[SerializationKeys.v].int
        perseatPrice = json[SerializationKeys.perseatPrice].float
        driverId = DriverId_BidAggregate(json: json[SerializationKeys.driverId])
        updatedAt = json[SerializationKeys.updatedAt].string
        bidOffer = json[SerializationKeys.bidOffer].float
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = acceptedRiders { dictionary[SerializationKeys.acceptedRiders] = value }
        if let value = bidStatus { dictionary[SerializationKeys.bidStatus] = value }
        if let value = minseatRequire { dictionary[SerializationKeys.minseatRequire] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = groupAggregateId { dictionary[SerializationKeys.groupAggregateId] = value }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = perseatPrice { dictionary[SerializationKeys.perseatPrice] = value }
        if let value = driverId { dictionary[SerializationKeys.driverId] = value.dictionaryRepresentation() }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = bidOffer { dictionary[SerializationKeys.bidOffer] = value }
        return dictionary
    }
    
}
public final class DriverId_BidAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "_id"
        static let mobile = "mobile"
        static let accType = "acc_type"
        static let avgRating = "avgRating"
        static let pic = "pic"
        static let personalInformation = "personal_information"
        static let vehicleInformation = "vehicle_information"
    }
    
    // MARK: Properties
    public var id: String?
    public var mobile: String?
    public var accType: String?
    public var avgRating: Int?
    public var pic: Pic?
    public var personalInformation: PersonalInformation_BidAggregate?
    public var vehicleInformation: VehicleInformation_BidAggregate?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].string
        mobile = json[SerializationKeys.mobile].string
        accType = json[SerializationKeys.accType].string
        avgRating = json[SerializationKeys.avgRating].int
        pic = Pic(json: json[SerializationKeys.pic])
        personalInformation = PersonalInformation_BidAggregate(json: json[SerializationKeys.personalInformation])
        vehicleInformation = VehicleInformation_BidAggregate(json: json[SerializationKeys.vehicleInformation])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = mobile { dictionary[SerializationKeys.mobile] = value }
        if let value = accType { dictionary[SerializationKeys.accType] = value }
        if let value = avgRating { dictionary[SerializationKeys.avgRating] = value }
        if let value = pic { dictionary[SerializationKeys.pic] = value.dictionaryRepresentation() }
        if let value = personalInformation { dictionary[SerializationKeys.personalInformation] = value.dictionaryRepresentation() }
        if let value = vehicleInformation { dictionary[SerializationKeys.vehicleInformation] = value.dictionaryRepresentation() }
        return dictionary
    }
    
}
public final class Location_BidAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}

public final class PersonalInformation_BidAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let address = "address"
        static let email = "email"
        static let fullName = "fullName"
        static let isVerified = "isVerified"
        static let companyName = "company_name"
    }
    
    // MARK: Properties
    public var address: String?
    public var email: String?
    public var fullName: String?
    public var isVerified: Bool? = false
    public var companyName: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        address = json[SerializationKeys.address].string
        email = json[SerializationKeys.email].string
        fullName = json[SerializationKeys.fullName].string
        isVerified = json[SerializationKeys.isVerified].boolValue
        companyName = json[SerializationKeys.companyName].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
        dictionary[SerializationKeys.isVerified] = isVerified
        if let value = companyName { dictionary[SerializationKeys.companyName] = value }
        return dictionary
    }
    
}

public final class TripId_BidAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
         static let numberOfDays = "numberOfDays"
        static let bookingDate = "bookingDate"
        static let driverReachedTime = "driverReachedTime"
        static let adminEarning = "adminEarning"
        static let driverReachedKM = "driverReachedKM"
        static let driverCancelationFee = "driverCancelationFee"
        static let driverDeclineInstant = "driverDeclineInstant"
        static let localbookingDateTime = "localbookingDateTime"
        static let driverEarning = "driverEarning"
        static let riderTaxApplied = "riderTaxApplied"
        static let paymentStatus = "paymentStatus"
        static let aggregateRequestPendingByDriver = "aggregateRequestPendingByDriver"
        static let tripRefundAmount = "tripRefundAmount"
        static let endLocation = "endLocation"
        static let isFinalAggregate = "isFinalAggregate"
        static let timezone = "timezone"
        static let distance = "distance"
        static let walletPayment = "walletPayment"
        static let workingTime = "workingTime"
        static let bridgeTax = "bridgeTax"
        static let kmTravel = "kmTravel"
        static let duration = "duration"
        static let driverTaxApplied = "driverTaxApplied"
        static let riderRating = "riderRating"
        static let startLocation = "startLocation"
        static let tripType = "tripType"
        static let endAddress = "end_address"
        static let riderCancelationFee = "riderCancelationFee"
        static let seatsRequired = "seatsRequired"
        static let createdAt = "createdAt"
        static let parentSharingTrip = "parentSharingTrip"
        static let driverRating = "driverRating"
        static let aggregatePaymentStatus = "aggregatePaymentStatus"
        static let bidByDriver = "bidByDriver"
        static let riderId = "riderId"
        static let price = "price"
        static let isShared = "isShared"
        static let status = "status"
        static let id = "_id"
        static let startAddress = "start_address"
        static let sentBookingReguestToDriver = "sentBookingReguestToDriver"
        static let updatedAt = "updatedAt"
        static let v = "__v"
        static let vehicleInformation = "vehicle_information"
        static let driverPushInQ = "driverPushInQ"
        static let bidRejectedByDriver = "bidRejectedByDriver"
    }
    
    // MARK: Properties
    public var numberOfDays: Int?
    public var bookingDate: String?
    public var driverReachedTime: Int?
    public var adminEarning: Int?
    public var driverReachedKM: Int?
    public var driverCancelationFee: Int?
    public var driverDeclineInstant: [Any]?
    public var localbookingDateTime: String?
    public var driverEarning: Int?
    public var riderTaxApplied: Int?
    public var paymentStatus: String?
    public var aggregateRequestPendingByDriver: [Any]?
    public var tripRefundAmount: Int?
    public var endLocation: Location_BidAggregate?
    public var isFinalAggregate: Bool? = false
    public var timezone: String?
    public var distance: Float?
    public var walletPayment: Int?
    public var workingTime: [WorkingTime_BidAggregate]?
    public var bridgeTax: Int?
    public var kmTravel: Int?
    public var duration: Float?
    public var driverTaxApplied: Int?
    public var riderRating: Int?
    public var startLocation: Location_BidAggregate?
    public var tripType: String?
    public var endAddress: String?
    public var riderCancelationFee: Int?
    public var seatsRequired: Int?
    public var createdAt: String?
    public var parentSharingTrip: Bool? = false
    public var driverRating: Int?
    public var aggregatePaymentStatus: Int?
    public var bidByDriver: [Any]?
    public var riderId: String?
    public var price: Float?
    public var isShared: Bool? = false
    public var status: Int?
    public var id: String?
    public var startAddress: String?
    public var sentBookingReguestToDriver: [Any]?
    public var updatedAt: String?
    public var v: Int?
    public var vehicleInformation: VehicleInformation_BidAggregate?
    public var driverPushInQ: [Any]?
    public var bidRejectedByDriver: [Any]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        bookingDate = json[SerializationKeys.bookingDate].string
        driverReachedTime = json[SerializationKeys.driverReachedTime].int
        adminEarning = json[SerializationKeys.adminEarning].int
        numberOfDays = json[SerializationKeys.numberOfDays].int

        driverReachedKM = json[SerializationKeys.driverReachedKM].int
        driverCancelationFee = json[SerializationKeys.driverCancelationFee].int
        if let items = json[SerializationKeys.driverDeclineInstant].array { driverDeclineInstant = items.map { $0.object} }
        localbookingDateTime = json[SerializationKeys.localbookingDateTime].string
        driverEarning = json[SerializationKeys.driverEarning].int
        riderTaxApplied = json[SerializationKeys.riderTaxApplied].int
        paymentStatus = json[SerializationKeys.paymentStatus].string
        if let items = json[SerializationKeys.aggregateRequestPendingByDriver].array { aggregateRequestPendingByDriver = items.map { $0.object} }
        tripRefundAmount = json[SerializationKeys.tripRefundAmount].int
        endLocation = Location_BidAggregate(json: json[SerializationKeys.endLocation])
        isFinalAggregate = json[SerializationKeys.isFinalAggregate].boolValue
        timezone = json[SerializationKeys.timezone].string
        distance = json[SerializationKeys.distance].float
        walletPayment = json[SerializationKeys.walletPayment].int
        if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { WorkingTime_BidAggregate(json: $0) } }
        bridgeTax = json[SerializationKeys.bridgeTax].int
        kmTravel = json[SerializationKeys.kmTravel].int
        duration = json[SerializationKeys.duration].float
        driverTaxApplied = json[SerializationKeys.driverTaxApplied].int
        riderRating = json[SerializationKeys.riderRating].int
        startLocation = Location_BidAggregate(json: json[SerializationKeys.startLocation])
        tripType = json[SerializationKeys.tripType].string
        endAddress = json[SerializationKeys.endAddress].string
        riderCancelationFee = json[SerializationKeys.riderCancelationFee].int
        seatsRequired = json[SerializationKeys.seatsRequired].int
        createdAt = json[SerializationKeys.createdAt].string
        parentSharingTrip = json[SerializationKeys.parentSharingTrip].boolValue
        driverRating = json[SerializationKeys.driverRating].int
        aggregatePaymentStatus = json[SerializationKeys.aggregatePaymentStatus].int
        if let items = json[SerializationKeys.bidByDriver].array { bidByDriver = items.map { $0.object} }
        riderId = json[SerializationKeys.riderId].string
        price = json[SerializationKeys.price].float
        isShared = json[SerializationKeys.isShared].boolValue
        status = json[SerializationKeys.status].int
        id = json[SerializationKeys.id].string
        startAddress = json[SerializationKeys.startAddress].string
        if let items = json[SerializationKeys.sentBookingReguestToDriver].array { sentBookingReguestToDriver = items.map { $0.object} }
        updatedAt = json[SerializationKeys.updatedAt].string
        v = json[SerializationKeys.v].int
        vehicleInformation = VehicleInformation_BidAggregate(json: json[SerializationKeys.vehicleInformation])
        if let items = json[SerializationKeys.driverPushInQ].array { driverPushInQ = items.map { $0.object} }
        if let items = json[SerializationKeys.bidRejectedByDriver].array { bidRejectedByDriver = items.map { $0.object} }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = bookingDate { dictionary[SerializationKeys.bookingDate] = value }
        if let value = driverReachedTime { dictionary[SerializationKeys.driverReachedTime] = value }
        if let value = adminEarning { dictionary[SerializationKeys.adminEarning] = value }
        if let value = numberOfDays { dictionary[SerializationKeys.numberOfDays] = value }
        if let value = driverReachedKM { dictionary[SerializationKeys.driverReachedKM] = value }
        if let value = driverCancelationFee { dictionary[SerializationKeys.driverCancelationFee] = value }
        if let value = driverDeclineInstant { dictionary[SerializationKeys.driverDeclineInstant] = value }
        if let value = localbookingDateTime { dictionary[SerializationKeys.localbookingDateTime] = value }
        if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
        if let value = riderTaxApplied { dictionary[SerializationKeys.riderTaxApplied] = value }
        if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
        if let value = aggregateRequestPendingByDriver { dictionary[SerializationKeys.aggregateRequestPendingByDriver] = value }
        if let value = tripRefundAmount { dictionary[SerializationKeys.tripRefundAmount] = value }
        if let value = endLocation { dictionary[SerializationKeys.endLocation] = value.dictionaryRepresentation() }
        dictionary[SerializationKeys.isFinalAggregate] = isFinalAggregate
        if let value = timezone { dictionary[SerializationKeys.timezone] = value }
        if let value = distance { dictionary[SerializationKeys.distance] = value }
        if let value = walletPayment { dictionary[SerializationKeys.walletPayment] = value }
        if let value = workingTime { dictionary[SerializationKeys.workingTime] = value.map { $0.dictionaryRepresentation() } }
        if let value = bridgeTax { dictionary[SerializationKeys.bridgeTax] = value }
        if let value = kmTravel { dictionary[SerializationKeys.kmTravel] = value }
        if let value = duration { dictionary[SerializationKeys.duration] = value }
        if let value = driverTaxApplied { dictionary[SerializationKeys.driverTaxApplied] = value }
        if let value = riderRating { dictionary[SerializationKeys.riderRating] = value }
        if let value = startLocation { dictionary[SerializationKeys.startLocation] = value.dictionaryRepresentation() }
        if let value = tripType { dictionary[SerializationKeys.tripType] = value }
        if let value = endAddress { dictionary[SerializationKeys.endAddress] = value }
        if let value = riderCancelationFee { dictionary[SerializationKeys.riderCancelationFee] = value }
        if let value = seatsRequired { dictionary[SerializationKeys.seatsRequired] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        dictionary[SerializationKeys.parentSharingTrip] = parentSharingTrip
        if let value = driverRating { dictionary[SerializationKeys.driverRating] = value }
        if let value = aggregatePaymentStatus { dictionary[SerializationKeys.aggregatePaymentStatus] = value }
        if let value = bidByDriver { dictionary[SerializationKeys.bidByDriver] = value }
        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
        if let value = price { dictionary[SerializationKeys.price] = value }
        dictionary[SerializationKeys.isShared] = isShared
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = startAddress { dictionary[SerializationKeys.startAddress] = value }
        if let value = sentBookingReguestToDriver { dictionary[SerializationKeys.sentBookingReguestToDriver] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = vehicleInformation { dictionary[SerializationKeys.vehicleInformation] = value.dictionaryRepresentation() }
        if let value = driverPushInQ { dictionary[SerializationKeys.driverPushInQ] = value }
        if let value = bidRejectedByDriver { dictionary[SerializationKeys.bidRejectedByDriver] = value }
        return dictionary
    }
    
}
public final class VehicleInformation_BidAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let brand = "brand"
        static let subBrand = "subBrand"
        static let year = "year"
        static let model = "model"
        static let vehicleNumber = "vehicle_number"
        static let seatingCapacity = "seating_capacity"
        static let isVerified = "isVerified"
        static let color = "color"
    }
    
    // MARK: Properties
    public var brand: String?
    public var subBrand: String?
    public var year: String?
    public var model: String?
    public var vehicleNumber: String?
    public var seatingCapacity: String?
    public var isVerified: Bool? = false
    public var color: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        brand = json[SerializationKeys.brand].string
        subBrand = json[SerializationKeys.subBrand].string
        year = json[SerializationKeys.year].string
        model = json[SerializationKeys.model].string
        vehicleNumber = json[SerializationKeys.vehicleNumber].string
        seatingCapacity = json[SerializationKeys.seatingCapacity].string
        isVerified = json[SerializationKeys.isVerified].boolValue
        color = json[SerializationKeys.color].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = brand { dictionary[SerializationKeys.brand] = value }
        if let value = subBrand { dictionary[SerializationKeys.subBrand] = value }
        if let value = year { dictionary[SerializationKeys.year] = value }
        if let value = model { dictionary[SerializationKeys.model] = value }
        if let value = vehicleNumber { dictionary[SerializationKeys.vehicleNumber] = value }
        if let value = seatingCapacity { dictionary[SerializationKeys.seatingCapacity] = value }
        dictionary[SerializationKeys.isVerified] = isVerified
        if let value = color { dictionary[SerializationKeys.color] = value }
        return dictionary
    }
    
}
public final class WorkingTime_BidAggregate {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let dayNumber = "dayNumber"
        static let time = "time"
        static let id = "_id"
    }
    
    // MARK: Properties
    public var dayNumber: Int?
    public var time: Time?
    public var id: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        dayNumber = json[SerializationKeys.dayNumber].int
        time = Time(json: json[SerializationKeys.time])
        id = json[SerializationKeys.id].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = dayNumber { dictionary[SerializationKeys.dayNumber] = value }
        if let value = time { dictionary[SerializationKeys.time] = value.dictionaryRepresentation() }
        if let value = id { dictionary[SerializationKeys.id] = value }
        return dictionary
    }
    
}

