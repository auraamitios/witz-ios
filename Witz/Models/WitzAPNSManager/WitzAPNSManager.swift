//
//  WitzAPNSManager.swift
//  Witz
//
//  Created by Amit Tripathi on 9/23/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import SwiftyJSON

class WitzAPNSManager:NSObject,UNUserNotificationCenterDelegate {
    
    static let sharedInstance = WitzAPNSManager()
    var isAPNSRequestGranted:Bool = false
    var isAPNSEnabled:Bool = false
    var deviceToken = String()
    
    private override init(){
        super.init()
        let center = UNUserNotificationCenter.current()
        center.delegate = self
    }
    
    //MARK:- Public Method(s)
    func requestAuthorization(completionHandler:@escaping (Bool,Error?) -> ()) {
        
//        let center = UNUserNotificationCenter.current()
//        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
//            // Enable or disable features based on authorization.
//            if granted{
//                self.isAPNSRequestGranted = true
//                // Register with APNs
//                UIApplication.shared.registerForRemoteNotifications()
//            }
//            else{
//                print("Error in accessing APNS \(String(describing: error?.localizedDescription))")
//                self.isAPNSRequestGranted = false
//            }
//            completionHandler(granted,error)
//        }
        //*******************
        
        #if (arch(i386) || arch(x86_64)) && (os(iOS) || os(watchOS) || os(tvOS))
            print(" Running Simulator ")
        #else
            print(" Running on Device ")
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
                
                guard error == nil else {
                    //Display Error.. Handle Error.. etc..
                    print("Error in accessing APNS \(String(describing: error?.localizedDescription))")
                    self.isAPNSRequestGranted = false
                    return
                }
                if granted {
                    //Do stuff here..
                    //Register for RemoteNotifications. Your Remote Notifications can display alerts now :)
                    guard Utility.isConnectedToNetwork() else{
                        AlertHelper.showSettingsAlertView()
                        return
                    }
//                    DispatchQueue.main.async {
//                        self.isAPNSRequestGranted = true
//                        UIApplication.shared.registerForRemoteNotifications()
//                    }
                    DispatchQueue.main.async(execute: {
                        self.isAPNSRequestGranted = true
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }
                else {
                    //Handle user denying permissions..
                }
                completionHandler(granted,error)
            }
        #endif
    }
    
    func setUpDeviceToken(deviceToken:Data){
//        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//        self.deviceToken = token
//        print("###Device Token\(token)### ->" + self.deviceToken)
//        let notificationName = Notification.Name("NotificationIdentifier")
//        NotificationCenter.default.post(name:notificationName, object: nil)
        //*******************
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        self.deviceToken = token
        print("###Device Token:🍓\(token)### ->" + self.deviceToken)
        UserDefaultManager.sharedManager.addValue(object: token as AnyObject, key: LinkStrings.UserDefaultKeys.DeviceToken)
    
//        AlertHelper.displayAlertViewOKButton(title: "Hi", message: self.deviceToken, style: .alert, actionButtonTitle: "Ok") { (action) in
//        }
    }
    
    //MARK:- Notification Center Delegate Method(s)
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
       
        
        // Print full message.
        print("did recieve \(userInfo)")
//        self.handleNotificationResponse(Notificaiton: userInfo)
        
        completionHandler()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
       
        
        
        // Delivers a notification to an app running in the foreground.
        let userInfo = notification.request.content.userInfo
        
        
        // Print full message.
        print("will present \(userInfo)")
        let aps = userInfo[AnyHashable("aps")] as? [String:Any]
        let message = userInfo[AnyHashable("message")] as? [String:Any]
      
//        AppDelegate.sharedInstance().handleNotificationResponse(Notificaiton: userInfo)
        
        //********* Rider notifications *********//
//        if AppManager.sharedInstance.loggedUserRider == true{
//
//            if (aps!["alert"] as? String ?? "").contains("Driver has reached waiting for you") {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: LinkStrings.RiderObserverKeys.ShowDriverInfo), object: nil, userInfo: userInfo)
//            }
//
//        }
//        else {
//            //********* Driver notifications *********//
//            if (aps!["alert"] as? String ?? "").contains("cancel"){
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RIDEDISMISS"), object: nil)
//            }else  if (aps!["alert"] as? String ?? "").contains("Rider is coming"){
//                AppDelegate.sharedInstance().handleNotificationResponse(Notificaiton: userInfo ,withType:2 )
//            }
//            else
//            {
//                 AppDelegate.sharedInstance().handleNotificationResponse(Notificaiton: userInfo ,withType:1 )
//            }
//        }
        
//                let actionHandler = { (action:UIAlertAction!) -> Void in
//                }
//                AlertHelper.displayAlertViewOKButton(title: LinkStrings.App.AppName, message: (aps!["alert"] as? String)!, style: .alert, actionButtonTitle: LinkStrings.MostCommon.Ok, completionHandler: actionHandler)
//                self.handleNotificationResponse(Notificaiton: userInfo)
        if UIApplication.shared.applicationState == .active {
            completionHandler([.badge,.sound])
        }
    }
}
