//
//  SharedPrice.swift
//
//  Created by Amit Tripathi on 2/22/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SharedPrice {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let vehicleType = "vehicleType"
    static let riderTaxApplied = "riderTaxApplied"
    static let driverEarning = "driverEarning"
    static let driverTaxApplied = "driverTaxApplied"
    static let price = "price"
    static let adminEarning = "adminEarning"
  }

  // MARK: Properties
  public var vehicleType: String?
  public var riderTaxApplied: Float?
  public var driverEarning: Float?
  public var driverTaxApplied: Float?
  public var price: String?
  public var adminEarning: Float?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    vehicleType = json[SerializationKeys.vehicleType].string
    riderTaxApplied = json[SerializationKeys.riderTaxApplied].float
    driverEarning = json[SerializationKeys.driverEarning].float
    driverTaxApplied = json[SerializationKeys.driverTaxApplied].float
    price = json[SerializationKeys.price].string
    adminEarning = json[SerializationKeys.adminEarning].float
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
    if let value = riderTaxApplied { dictionary[SerializationKeys.riderTaxApplied] = value }
    if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
    if let value = driverTaxApplied { dictionary[SerializationKeys.driverTaxApplied] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = adminEarning { dictionary[SerializationKeys.adminEarning] = value }
    return dictionary
  }

}
