//
//  RiderNotificationCount.swift
//
//  Created by Amit Tripathi on 11/27/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class RiderNotificationCount {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let message = "message"
    static let notificationCounts = "data"
    static let statusCode = "statusCode"
  }

  // MARK: Properties
  public var message: String?
   var notificationCounts: NotificationCounts?
  public var statusCode: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    message = json[SerializationKeys.message].string
    notificationCounts = NotificationCounts(json: json[SerializationKeys.notificationCounts])
    statusCode = json[SerializationKeys.statusCode].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = notificationCounts { dictionary[SerializationKeys.notificationCounts] = value.dictionaryRepresentation() }
    if let value = statusCode { dictionary[SerializationKeys.statusCode] = value }
    return dictionary
  }

}

 class NotificationCounts {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let reservationFutureTripCount = "reservationFutureTripCount"
        static let reservationPendingTripCount = "reservationPendingTripCount"
        static let serviceRequestPendingTripCount = "serviceRequestPendingTripCount"
        static let notificationCount = "notificationCount"
    }
    
    // MARK: Properties
    public var reservationFutureTripCount: Int?
    public var reservationPendingTripCount: Int?
    public var serviceRequestPendingTripCount: Int?
    public var notificationCount: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        reservationFutureTripCount = json[SerializationKeys.reservationFutureTripCount].int
        reservationPendingTripCount = json[SerializationKeys.reservationPendingTripCount].int
        serviceRequestPendingTripCount = json[SerializationKeys.serviceRequestPendingTripCount].int
        notificationCount = json[SerializationKeys.notificationCount].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = reservationFutureTripCount { dictionary[SerializationKeys.reservationFutureTripCount] = value }
        if let value = reservationPendingTripCount { dictionary[SerializationKeys.reservationPendingTripCount] = value }
        if let value = serviceRequestPendingTripCount { dictionary[SerializationKeys.serviceRequestPendingTripCount] = value }
        if let value = notificationCount { dictionary[SerializationKeys.notificationCount] = value }
        return dictionary
    }
    
}
