//
//  VehiclesData.swift
//  Witz
//
//  Created by Amit Tripathi on 10/27/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct VehiclesData {
    
    var vehicle_Id:String?
    var name:String?
    var isPrivate:Bool?
    var isShared:Bool?
    var minBookingSeats:Int?
    var seats:Int?
    
    //pic
    var picDetail:[String:AnyObject]?
    var original:String?
    var thumb:String?
    
    static func parseVehiclesData(data:[JSON], outerData:JSON)->VehiclesData{
        
        var vehiclesData = VehiclesData()
        for dict in data {
            vehiclesData.vehicle_Id = dict["_id"].stringValue
            vehiclesData.isPrivate = dict["isPrivate"].boolValue
            vehiclesData.isShared = dict["isShared"].boolValue
            vehiclesData.minBookingSeats = dict["minBookingSeats"].intValue
            vehiclesData.name = dict["name"].stringValue
            vehiclesData.seats = dict["seats"].intValue

            //pic detail
            let dict1 = dict["pic"].dictionaryValue
            vehiclesData.original = dict1["original"]?.string
            vehiclesData.thumb = dict1["thumb"]?.string
            vehiclesData.picDetail = ["original":vehiclesData.original as AnyObject, "thumb":vehiclesData.thumb as AnyObject,]
            print("dict value************\(String(describing: vehiclesData.picDetail))")
            RiderManager.sharedInstance.vehiclesData.append(vehiclesData)
        }
        return vehiclesData
    }
}
