//
//  ReservationGeneralSetting.swift
//
//  Created by Amit Tripathi on 12/20/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class RiderReservationGeneralSetting {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let singleDayRiderOfferClosureDuration = "singleDayRiderOfferClosureDuration"
    static let minimumReservationTime = "minimumReservationTime"
    static let futureDayRiderBidAcceptanceDuration = "futureDayRiderBidAcceptanceDuration"
    static let singleDayDriverAcceptanceDuration = "singleDayDriverAcceptanceDuration"
    static let reservationDriverMaxRequestPerDay = "reservationDriverMaxRequestPerDay"
    static let sendOfferPriceToRiderIntervalTime = "sendOfferPriceToRiderIntervalTime"
    static let futureDayRiderOfferClosureDuration = "futureDayRiderOfferClosureDuration"
    static let singleDayRiderTopFiveBidsDuration = "singleDayRiderTopFiveBidsDuration"
    static let futureDayRiderTopFiveBidsDuration = "futureDayRiderTopFiveBidsDuration"
    static let driverBookingReminderTimeCronId = "driverBookingReminderTimeCronId"
    static let sendOfferPriceToRiderIntervalTimeCronId = "sendOfferPriceToRiderIntervalTimeCronId"
    static let singleDayRiderBidAcceptanceDuration = "singleDayRiderBidAcceptanceDuration"
    static let driverBookingReminderTime = "driverBookingReminderTime"
    static let driverReadyTime = "driverReadyTime"
    static let futureDayDriverAcceptanceDuration = "futureDayDriverAcceptanceDuration"
    static let reservationDiameter = "reservationDiameter"
  }

  // MARK: Properties
  public var singleDayRiderOfferClosureDuration: Int?
  public var minimumReservationTime: Float?
  public var futureDayRiderBidAcceptanceDuration: Int?
  public var singleDayDriverAcceptanceDuration: Int?
  public var reservationDriverMaxRequestPerDay: Int?
  public var sendOfferPriceToRiderIntervalTime: Int?
  public var futureDayRiderOfferClosureDuration: Int?
  public var singleDayRiderTopFiveBidsDuration: Int?
  public var futureDayRiderTopFiveBidsDuration: Int?
  public var driverBookingReminderTimeCronId: String?
  public var sendOfferPriceToRiderIntervalTimeCronId: String?
  public var singleDayRiderBidAcceptanceDuration: Int?
  public var driverBookingReminderTime: Int?
  public var driverReadyTime: Int?
  public var futureDayDriverAcceptanceDuration: Int?
  public var reservationDiameter: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    singleDayRiderOfferClosureDuration = json[SerializationKeys.singleDayRiderOfferClosureDuration].int
    minimumReservationTime = json[SerializationKeys.minimumReservationTime].float
    futureDayRiderBidAcceptanceDuration = json[SerializationKeys.futureDayRiderBidAcceptanceDuration].int
    singleDayDriverAcceptanceDuration = json[SerializationKeys.singleDayDriverAcceptanceDuration].int
    reservationDriverMaxRequestPerDay = json[SerializationKeys.reservationDriverMaxRequestPerDay].int
    sendOfferPriceToRiderIntervalTime = json[SerializationKeys.sendOfferPriceToRiderIntervalTime].int
    futureDayRiderOfferClosureDuration = json[SerializationKeys.futureDayRiderOfferClosureDuration].int
    singleDayRiderTopFiveBidsDuration = json[SerializationKeys.singleDayRiderTopFiveBidsDuration].int
    futureDayRiderTopFiveBidsDuration = json[SerializationKeys.futureDayRiderTopFiveBidsDuration].int
    driverBookingReminderTimeCronId = json[SerializationKeys.driverBookingReminderTimeCronId].string
    sendOfferPriceToRiderIntervalTimeCronId = json[SerializationKeys.sendOfferPriceToRiderIntervalTimeCronId].string
    singleDayRiderBidAcceptanceDuration = json[SerializationKeys.singleDayRiderBidAcceptanceDuration].int
    driverBookingReminderTime = json[SerializationKeys.driverBookingReminderTime].int
    driverReadyTime = json[SerializationKeys.driverReadyTime].int
    futureDayDriverAcceptanceDuration = json[SerializationKeys.futureDayDriverAcceptanceDuration].int
    reservationDiameter = json[SerializationKeys.reservationDiameter].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = singleDayRiderOfferClosureDuration { dictionary[SerializationKeys.singleDayRiderOfferClosureDuration] = value }
    if let value = minimumReservationTime { dictionary[SerializationKeys.minimumReservationTime] = value }
    if let value = futureDayRiderBidAcceptanceDuration { dictionary[SerializationKeys.futureDayRiderBidAcceptanceDuration] = value }
    if let value = singleDayDriverAcceptanceDuration { dictionary[SerializationKeys.singleDayDriverAcceptanceDuration] = value }
    if let value = reservationDriverMaxRequestPerDay { dictionary[SerializationKeys.reservationDriverMaxRequestPerDay] = value }
    if let value = sendOfferPriceToRiderIntervalTime { dictionary[SerializationKeys.sendOfferPriceToRiderIntervalTime] = value }
    if let value = futureDayRiderOfferClosureDuration { dictionary[SerializationKeys.futureDayRiderOfferClosureDuration] = value }
    if let value = singleDayRiderTopFiveBidsDuration { dictionary[SerializationKeys.singleDayRiderTopFiveBidsDuration] = value }
    if let value = futureDayRiderTopFiveBidsDuration { dictionary[SerializationKeys.futureDayRiderTopFiveBidsDuration] = value }
    if let value = driverBookingReminderTimeCronId { dictionary[SerializationKeys.driverBookingReminderTimeCronId] = value }
    if let value = sendOfferPriceToRiderIntervalTimeCronId { dictionary[SerializationKeys.sendOfferPriceToRiderIntervalTimeCronId] = value }
    if let value = singleDayRiderBidAcceptanceDuration { dictionary[SerializationKeys.singleDayRiderBidAcceptanceDuration] = value }
    if let value = driverBookingReminderTime { dictionary[SerializationKeys.driverBookingReminderTime] = value }
    if let value = driverReadyTime { dictionary[SerializationKeys.driverReadyTime] = value }
    if let value = futureDayDriverAcceptanceDuration { dictionary[SerializationKeys.futureDayDriverAcceptanceDuration] = value }
    if let value = reservationDiameter { dictionary[SerializationKeys.reservationDiameter] = value }
    return dictionary
  }

}
