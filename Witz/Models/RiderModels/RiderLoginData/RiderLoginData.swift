//
//  RiderLoginData.swift
//
//  Created by Amit Tripathi on 12/20/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class RiderLoginData {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let reservationGeneralSetting = "reservationGeneralSetting"
    static let totalNumberRefferal = "totalNumberRefferal"
    static let isBlocked = "isBlocked"
    static let homeAddress = "homeAddress"
    static let avgRating = "avgRating"
    static let arrivalTime = "arrivalTime"
    static let totalRefferalAmount = "totalRefferalAmount"
    static let riderStatus = "riderStatus"
    static let workAddress = "workAddress"
    static let isVerified = "isVerified"
    static let deviceToken = "deviceToken"
    static let totalCredits = "totalCredits"
    static let reservationTripCount = "reservationTripCount"
    static let fullName = "fullName"
    static let password = "password"
    static let lan = "lan"
    static let email = "email"
    static let deviceType = "deviceType"
    static let createdAt = "createdAt"
    static let mobile = "mobile"
    static let accessToken = "accessToken"
    static let pic = "pic"
    static let promotionTotalCredits = "promotionTotalCredits"
    static let promotioncode = "promotioncode"
    static let id = "_id"
    static let isDeleted = "isDeleted"
    static let cards = "cards"
    static let homeLocation = "homeLocation"
    static let updatedAt = "updatedAt"
    static let currentLocation = "currentLocation"
    static let v = "__v"
    static let currentTripData = "currentTripData"
    static let badgeCount = "badgeCount"
    static let refferalCode = "refferalCode"
    static let workLocation = "workLocation"
  }

  // MARK: Properties
  public var reservationGeneralSetting: RiderReservationGeneralSetting?
  public var totalNumberRefferal: Int?
  public var isBlocked: Bool? = false
  public var homeAddress: String?
  public var avgRating: Int?
  public var arrivalTime: Int?
  public var totalRefferalAmount: Int?
  public var riderStatus: String?
  public var workAddress: String?
  public var isVerified: Bool? = false
  public var deviceToken: String?
  public var totalCredits: Int?
  public var reservationTripCount: Int?
  public var fullName: String?
  public var password: String?
  public var lan: String?
  public var email: String?
  public var deviceType: String?
  public var createdAt: String?
  public var mobile: String?
  public var accessToken: String?
  public var pic: RiderProfilePic?
  public var promotionTotalCredits: Int?
  public var promotioncode: String?
  public var id: String?
  public var isDeleted: Bool? = false
  public var cards: [Cards]?
  public var homeLocation: RiderHomeLocation?
  public var updatedAt: String?
  public var currentLocation: RiderCurrentLocation?
  public var v: Int?
  public var currentTripData: RiderCurrentTripData?
  public var badgeCount: Int?
  public var refferalCode: String?
  public var workLocation: RiderWorkLocation?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    reservationGeneralSetting = RiderReservationGeneralSetting(json: json[SerializationKeys.reservationGeneralSetting])
    totalNumberRefferal = json[SerializationKeys.totalNumberRefferal].int
    isBlocked = json[SerializationKeys.isBlocked].boolValue
    homeAddress = json[SerializationKeys.homeAddress].string
    avgRating = json[SerializationKeys.avgRating].int
    arrivalTime = json[SerializationKeys.arrivalTime].int
    totalRefferalAmount = json[SerializationKeys.totalRefferalAmount].int
    riderStatus = json[SerializationKeys.riderStatus].string
    workAddress = json[SerializationKeys.workAddress].string
    isVerified = json[SerializationKeys.isVerified].boolValue
    deviceToken = json[SerializationKeys.deviceToken].string
    totalCredits = json[SerializationKeys.totalCredits].int
    reservationTripCount = json[SerializationKeys.reservationTripCount].int
    fullName = json[SerializationKeys.fullName].string
    password = json[SerializationKeys.password].string
    lan = json[SerializationKeys.lan].string
    email = json[SerializationKeys.email].string
    deviceType = json[SerializationKeys.deviceType].string
    createdAt = json[SerializationKeys.createdAt].string
    mobile = json[SerializationKeys.mobile].string
    accessToken = json[SerializationKeys.accessToken].string
    pic = RiderProfilePic(json: json[SerializationKeys.pic])
    promotionTotalCredits = json[SerializationKeys.promotionTotalCredits].int
    promotioncode = json[SerializationKeys.promotioncode].string
    id = json[SerializationKeys.id].string
    isDeleted = json[SerializationKeys.isDeleted].boolValue
    if let items = json[SerializationKeys.cards].array { cards = items.map { Cards(json: $0) } }
    homeLocation = RiderHomeLocation(json: json[SerializationKeys.homeLocation])
    updatedAt = json[SerializationKeys.updatedAt].string
    currentLocation = RiderCurrentLocation(json: json[SerializationKeys.currentLocation])
    v = json[SerializationKeys.v].int
    currentTripData = RiderCurrentTripData(json: json[SerializationKeys.currentTripData])
    badgeCount = json[SerializationKeys.badgeCount].int
    refferalCode = json[SerializationKeys.refferalCode].string
    workLocation = RiderWorkLocation(json: json[SerializationKeys.workLocation])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = reservationGeneralSetting { dictionary[SerializationKeys.reservationGeneralSetting] = value.dictionaryRepresentation() }
    if let value = totalNumberRefferal { dictionary[SerializationKeys.totalNumberRefferal] = value }
    dictionary[SerializationKeys.isBlocked] = isBlocked
    if let value = homeAddress { dictionary[SerializationKeys.homeAddress] = value }
    if let value = avgRating { dictionary[SerializationKeys.avgRating] = value }
    if let value = arrivalTime { dictionary[SerializationKeys.arrivalTime] = value }
    if let value = totalRefferalAmount { dictionary[SerializationKeys.totalRefferalAmount] = value }
    if let value = riderStatus { dictionary[SerializationKeys.riderStatus] = value }
    if let value = workAddress { dictionary[SerializationKeys.workAddress] = value }
    dictionary[SerializationKeys.isVerified] = isVerified
    if let value = deviceToken { dictionary[SerializationKeys.deviceToken] = value }
    if let value = totalCredits { dictionary[SerializationKeys.totalCredits] = value }
    if let value = reservationTripCount { dictionary[SerializationKeys.reservationTripCount] = value }
    if let value = fullName { dictionary[SerializationKeys.fullName] = value }
    if let value = password { dictionary[SerializationKeys.password] = value }
    if let value = lan { dictionary[SerializationKeys.lan] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = deviceType { dictionary[SerializationKeys.deviceType] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = mobile { dictionary[SerializationKeys.mobile] = value }
    if let value = accessToken { dictionary[SerializationKeys.accessToken] = value }
    if let value = pic { dictionary[SerializationKeys.pic] = value.dictionaryRepresentation() }
    if let value = promotionTotalCredits { dictionary[SerializationKeys.promotionTotalCredits] = value }
    if let value = promotioncode { dictionary[SerializationKeys.promotioncode] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    dictionary[SerializationKeys.isDeleted] = isDeleted
    if let value = cards { dictionary[SerializationKeys.cards] = value.map { $0.dictionaryRepresentation() } }
    if let value = homeLocation { dictionary[SerializationKeys.homeLocation] = value.dictionaryRepresentation() }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = currentLocation { dictionary[SerializationKeys.currentLocation] = value.dictionaryRepresentation() }
    if let value = v { dictionary[SerializationKeys.v] = value }
    if let value = currentTripData { dictionary[SerializationKeys.currentTripData] = value.dictionaryRepresentation() }
    if let value = badgeCount { dictionary[SerializationKeys.badgeCount] = value }
    if let value = refferalCode { dictionary[SerializationKeys.refferalCode] = value }
    if let value = workLocation { dictionary[SerializationKeys.workLocation] = value.dictionaryRepresentation() }
    return dictionary
  }

}

public final class RiderWorkLocation {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Int]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.intValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
}

public final class RiderHomeLocation {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}

public final class RiderCurrentLocation {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}

public final class RiderProfilePic {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let thumb = "thumb"
        static let original = "original"
    }
    
    // MARK: Properties
    public var thumb: String?
    public var original: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        thumb = json[SerializationKeys.thumb].string
        original = json[SerializationKeys.original].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = thumb { dictionary[SerializationKeys.thumb] = value }
        if let value = original { dictionary[SerializationKeys.original] = value }
        return dictionary
    }
    
}

public final class RiderCurrentTripData {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let tripData = "tripData"
        static let tripStatus = "tripStatus"
    }
    
    // MARK: Properties
    public var tripData: RiderTripData?
    public var tripStatus: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        tripData = RiderTripData(json: json[SerializationKeys.tripData])
        tripStatus = json[SerializationKeys.tripStatus].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = tripData { dictionary[SerializationKeys.tripData] = value.dictionaryRepresentation() }
        dictionary[SerializationKeys.tripStatus] = tripStatus
        return dictionary
    }
}

public final class RiderTripData {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
    }
    
    // MARK: Properties
    
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        return dictionary
    }
    
}

