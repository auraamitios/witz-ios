//
//  Cards.swift
//
//  Created by Amit Tripathi on 12/20/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Cards {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let nameOnCard = "nameOnCard"
    static let id = "_id"
    static let cardType = "cardType"
    static let isDeleted = "isDeleted"
    static let paymentGatewayId = "paymentGatewayId"
    static let defaultStatus = "defaultStatus"
    static let lastFourDigit = "lastFourDigit"
    static let v = "__v"
    static let isBlocked = "isBlocked"
    static let cardToken = "cardToken"
    static let riderId = "riderId"
  }

  // MARK: Properties
  public var nameOnCard: String?
  public var id: String?
  public var cardType: String?
  public var isDeleted: Bool? = false
  public var paymentGatewayId: String?
  public var defaultStatus: Bool? = false
  public var lastFourDigit: String?
  public var v: Int?
  public var isBlocked: Bool? = false
  public var cardToken: String?
  public var riderId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    nameOnCard = json[SerializationKeys.nameOnCard].string
    id = json[SerializationKeys.id].string
    cardType = json[SerializationKeys.cardType].string
    isDeleted = json[SerializationKeys.isDeleted].boolValue
    paymentGatewayId = json[SerializationKeys.paymentGatewayId].string
    defaultStatus = json[SerializationKeys.defaultStatus].boolValue
    lastFourDigit = json[SerializationKeys.lastFourDigit].string
    v = json[SerializationKeys.v].int
    isBlocked = json[SerializationKeys.isBlocked].boolValue
    cardToken = json[SerializationKeys.cardToken].string
    riderId = json[SerializationKeys.riderId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = nameOnCard { dictionary[SerializationKeys.nameOnCard] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = cardType { dictionary[SerializationKeys.cardType] = value }
    dictionary[SerializationKeys.isDeleted] = isDeleted
    if let value = paymentGatewayId { dictionary[SerializationKeys.paymentGatewayId] = value }
    dictionary[SerializationKeys.defaultStatus] = defaultStatus
    if let value = lastFourDigit { dictionary[SerializationKeys.lastFourDigit] = value }
    if let value = v { dictionary[SerializationKeys.v] = value }
    dictionary[SerializationKeys.isBlocked] = isBlocked
    if let value = cardToken { dictionary[SerializationKeys.cardToken] = value }
    if let value = riderId { dictionary[SerializationKeys.riderId] = value }
    return dictionary
  }

}
