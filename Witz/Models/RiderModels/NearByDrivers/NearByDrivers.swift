//
//  NearByDrivers.swift
//
//  Created by Amit Tripathi on 2/14/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class NearByDrivers {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let driver = "driver"
  }

  // MARK: Properties
  public var driver: [Driver]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.driver].array { driver = items.map { Driver(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = driver { dictionary[SerializationKeys.driver] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}

public final class Driver {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "_id"
        static let vehicleInformation = "vehicle_information"
        static let accessToken = "accessToken"
        static let currentLocation = "currentLocation"
    }
    
    // MARK: Properties
    public var id: String?
    public var vehicleInformation: VehicleInformation?
    public var accessToken: String?
    public var currentLocation: CurrentLocation?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].string
        vehicleInformation = VehicleInformation(json: json[SerializationKeys.vehicleInformation])
        accessToken = json[SerializationKeys.accessToken].string
        currentLocation = CurrentLocation(json: json[SerializationKeys.currentLocation])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = vehicleInformation { dictionary[SerializationKeys.vehicleInformation] = value.dictionaryRepresentation() }
        if let value = accessToken { dictionary[SerializationKeys.accessToken] = value }
        if let value = currentLocation { dictionary[SerializationKeys.currentLocation] = value.dictionaryRepresentation() }
        return dictionary
    }
    
}

public final class CurrentLocation {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}
