//
//  MinReservationTime.swift
//
//  Created by Amit Tripathi on 12/7/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class MinReservationTime {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let message = "message"
    static let reservedTime = "data"
    static let statusCode = "statusCode"
  }

  // MARK: Properties
  public var message: String?
  var reservedTime: ReservedTime?
  public var statusCode: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    message = json[SerializationKeys.message].string
    reservedTime = ReservedTime(json: json[SerializationKeys.reservedTime])
    statusCode = json[SerializationKeys.statusCode].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = reservedTime { dictionary[SerializationKeys.reservedTime] = value.dictionaryRepresentation() }
    if let value = statusCode { dictionary[SerializationKeys.statusCode] = value }
    return dictionary
  }

}

public final class ReservedTime {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let todayDateDateTime = "todayDateDateTime"
        static let minimumReservationTime = "minimumReservationTime"
        static let todayDateTimestamp = "todayDateTimestamp"
    }
    
    // MARK: Properties
    public var todayDateDateTime: String?
    public var minimumReservationTime: Float?
    public var todayDateTimestamp: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        todayDateDateTime = json[SerializationKeys.todayDateDateTime].string
        minimumReservationTime = json[SerializationKeys.minimumReservationTime].float
        todayDateTimestamp = json[SerializationKeys.todayDateTimestamp].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = todayDateDateTime { dictionary[SerializationKeys.todayDateDateTime] = value }
        if let value = minimumReservationTime { dictionary[SerializationKeys.minimumReservationTime] = value }
        if let value = todayDateTimestamp { dictionary[SerializationKeys.todayDateTimestamp] = value }
        return dictionary
    }
    
}
