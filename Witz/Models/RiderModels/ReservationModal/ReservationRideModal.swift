//
//  ReservationRideModal.swift
//
//  Created by Amit Tripathi on 12/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ReservationRideModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bookingDate = "bookingDate"
    static let driverReachedTime = "driverReachedTime"
    static let adminEarning = "adminEarning"
    static let driverReachedKM = "driverReachedKM"
    static let driverCancelationFee = "driverCancelationFee"
    static let driverDeclineInstant = "driverDeclineInstant"
    static let localbookingDateTime = "localbookingDateTime"
    static let driverEarning = "driverEarning"
    static let riderTaxApplied = "riderTaxApplied"
    static let paymentStatus = "paymentStatus"
    static let aggregateRequestPendingByDriver = "aggregateRequestPendingByDriver"
    static let endLocation = "endLocation"
    static let isFinalAggregate = "isFinalAggregate"
    static let timezone = "timezone"
    static let distance = "distance"
    static let walletPayment = "walletPayment"
    static let workingTime = "workingTime"
    static let bridgeTax = "bridgeTax"
    static let kmTravel = "kmTravel"
    static let duration = "duration"
    static let driverTaxApplied = "driverTaxApplied"
    static let riderRating = "riderRating"
    static let startLocation = "startLocation"
    static let tripType = "tripType"
    static let endAddress = "end_address"
    static let riderCancelationFee = "riderCancelationFee"
    static let seatsRequired = "seatsRequired"
    static let createdAt = "createdAt"
    static let parentSharingTrip = "parentSharingTrip"
    static let driverRating = "driverRating"
    static let aggregatePaymentStatus = "aggregatePaymentStatus"
    static let bidByDriver = "bidByDriver"
    static let riderId = "riderId"
    static let price = "price"
    static let isShared = "isShared"
    static let status = "status"
    static let id = "_id"
    static let startAddress = "start_address"
    static let sentBookingReguestToDriver = "sentBookingReguestToDriver"
    static let updatedAt = "updatedAt"
    static let timezoneString = "timezoneString"
    static let v = "__v"
    static let driverPushInQ = "driverPushInQ"
    static let bidRejectedByDriver = "bidRejectedByDriver"
  }

  // MARK: Properties
  public var bookingDate: String?
  public var driverReachedTime: Int?
  public var adminEarning: Int?
  public var driverReachedKM: Int?
  public var driverCancelationFee: Int?
  public var driverDeclineInstant: [Any]?
  public var localbookingDateTime: String?
  public var driverEarning: Int?
  public var riderTaxApplied: Int?
  public var paymentStatus: String?
  public var aggregateRequestPendingByDriver: [Any]?
  public var endLocation: RideEndLocation?
  public var isFinalAggregate: Bool? = false
  public var timezone: String?
  public var distance: Float?
  public var walletPayment: Int?
  public var workingTime: [Any]?
  public var bridgeTax: Int?
  public var kmTravel: Int?
  public var duration: Int?
  public var driverTaxApplied: Int?
  public var riderRating: Int?
  public var startLocation: RideStartLocation?
  public var tripType: String?
  public var endAddress: String?
  public var riderCancelationFee: Int?
  public var seatsRequired: Int?
  public var createdAt: String?
  public var parentSharingTrip: Bool? = false
  public var driverRating: Int?
  public var aggregatePaymentStatus: Int?
  public var bidByDriver: [Any]?
  public var riderId: String?
  public var price: Int?
  public var isShared: Bool? = false
  public var status: Int?
  public var id: String?
  public var startAddress: String?
  public var sentBookingReguestToDriver: [Any]?
  public var updatedAt: String?
  public var timezoneString: String?
  public var v: Int?
  public var driverPushInQ: [Any]?
  public var bidRejectedByDriver: [Any]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    bookingDate = json[SerializationKeys.bookingDate].string
    driverReachedTime = json[SerializationKeys.driverReachedTime].int
    adminEarning = json[SerializationKeys.adminEarning].int
    driverReachedKM = json[SerializationKeys.driverReachedKM].int
    driverCancelationFee = json[SerializationKeys.driverCancelationFee].int
    if let items = json[SerializationKeys.driverDeclineInstant].array { driverDeclineInstant = items.map { $0.object} }
    localbookingDateTime = json[SerializationKeys.localbookingDateTime].string
    driverEarning = json[SerializationKeys.driverEarning].int
    riderTaxApplied = json[SerializationKeys.riderTaxApplied].int
    paymentStatus = json[SerializationKeys.paymentStatus].string
    if let items = json[SerializationKeys.aggregateRequestPendingByDriver].array { aggregateRequestPendingByDriver = items.map { $0.object} }
    endLocation = RideEndLocation(json: json[SerializationKeys.endLocation])
    isFinalAggregate = json[SerializationKeys.isFinalAggregate].boolValue
    timezone = json[SerializationKeys.timezone].string
    distance = json[SerializationKeys.distance].float
    walletPayment = json[SerializationKeys.walletPayment].int
    if let items = json[SerializationKeys.workingTime].array { workingTime = items.map { $0.object} }
    bridgeTax = json[SerializationKeys.bridgeTax].int
    kmTravel = json[SerializationKeys.kmTravel].int
    duration = json[SerializationKeys.duration].int
    driverTaxApplied = json[SerializationKeys.driverTaxApplied].int
    riderRating = json[SerializationKeys.riderRating].int
    startLocation = RideStartLocation(json: json[SerializationKeys.startLocation])
    tripType = json[SerializationKeys.tripType].string
    endAddress = json[SerializationKeys.endAddress].string
    riderCancelationFee = json[SerializationKeys.riderCancelationFee].int
    seatsRequired = json[SerializationKeys.seatsRequired].int
    createdAt = json[SerializationKeys.createdAt].string
    parentSharingTrip = json[SerializationKeys.parentSharingTrip].boolValue
    driverRating = json[SerializationKeys.driverRating].int
    aggregatePaymentStatus = json[SerializationKeys.aggregatePaymentStatus].int
    if let items = json[SerializationKeys.bidByDriver].array { bidByDriver = items.map { $0.object} }
    riderId = json[SerializationKeys.riderId].string
    price = json[SerializationKeys.price].int
    isShared = json[SerializationKeys.isShared].boolValue
    status = json[SerializationKeys.status].int
    id = json[SerializationKeys.id].string
    startAddress = json[SerializationKeys.startAddress].string
    if let items = json[SerializationKeys.sentBookingReguestToDriver].array { sentBookingReguestToDriver = items.map { $0.object} }
    updatedAt = json[SerializationKeys.updatedAt].string
    timezoneString = json[SerializationKeys.timezoneString].string
    v = json[SerializationKeys.v].int
    if let items = json[SerializationKeys.driverPushInQ].array { driverPushInQ = items.map { $0.object} }
    if let items = json[SerializationKeys.bidRejectedByDriver].array { bidRejectedByDriver = items.map { $0.object} }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = bookingDate { dictionary[SerializationKeys.bookingDate] = value }
    if let value = driverReachedTime { dictionary[SerializationKeys.driverReachedTime] = value }
    if let value = adminEarning { dictionary[SerializationKeys.adminEarning] = value }
    if let value = driverReachedKM { dictionary[SerializationKeys.driverReachedKM] = value }
    if let value = driverCancelationFee { dictionary[SerializationKeys.driverCancelationFee] = value }
    if let value = driverDeclineInstant { dictionary[SerializationKeys.driverDeclineInstant] = value }
    if let value = localbookingDateTime { dictionary[SerializationKeys.localbookingDateTime] = value }
    if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
    if let value = riderTaxApplied { dictionary[SerializationKeys.riderTaxApplied] = value }
    if let value = paymentStatus { dictionary[SerializationKeys.paymentStatus] = value }
    if let value = aggregateRequestPendingByDriver { dictionary[SerializationKeys.aggregateRequestPendingByDriver] = value }
    if let value = endLocation { dictionary[SerializationKeys.endLocation] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.isFinalAggregate] = isFinalAggregate
    if let value = timezone { dictionary[SerializationKeys.timezone] = value }
    if let value = distance { dictionary[SerializationKeys.distance] = value }
    if let value = walletPayment { dictionary[SerializationKeys.walletPayment] = value }
    if let value = workingTime { dictionary[SerializationKeys.workingTime] = value }
    if let value = bridgeTax { dictionary[SerializationKeys.bridgeTax] = value }
    if let value = kmTravel { dictionary[SerializationKeys.kmTravel] = value }
    if let value = duration { dictionary[SerializationKeys.duration] = value }
    if let value = driverTaxApplied { dictionary[SerializationKeys.driverTaxApplied] = value }
    if let value = riderRating { dictionary[SerializationKeys.riderRating] = value }
    if let value = startLocation { dictionary[SerializationKeys.startLocation] = value.dictionaryRepresentation() }
    if let value = tripType { dictionary[SerializationKeys.tripType] = value }
    if let value = endAddress { dictionary[SerializationKeys.endAddress] = value }
    if let value = riderCancelationFee { dictionary[SerializationKeys.riderCancelationFee] = value }
    if let value = seatsRequired { dictionary[SerializationKeys.seatsRequired] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    dictionary[SerializationKeys.parentSharingTrip] = parentSharingTrip
    if let value = driverRating { dictionary[SerializationKeys.driverRating] = value }
    if let value = aggregatePaymentStatus { dictionary[SerializationKeys.aggregatePaymentStatus] = value }
    if let value = bidByDriver { dictionary[SerializationKeys.bidByDriver] = value }
    if let value = riderId { dictionary[SerializationKeys.riderId] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    dictionary[SerializationKeys.isShared] = isShared
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = startAddress { dictionary[SerializationKeys.startAddress] = value }
    if let value = sentBookingReguestToDriver { dictionary[SerializationKeys.sentBookingReguestToDriver] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = timezoneString { dictionary[SerializationKeys.timezoneString] = value }
    if let value = v { dictionary[SerializationKeys.v] = value }
    if let value = driverPushInQ { dictionary[SerializationKeys.driverPushInQ] = value }
    if let value = bidRejectedByDriver { dictionary[SerializationKeys.bidRejectedByDriver] = value }
    return dictionary
  }

}
