//
//  PaymentGateway.swift
//  Witz
//
//  Created by Amit Tripathi on 11/4/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class PaymentGateway {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
        static let gatewayId = "_id"
        static let status = "status"
        static let paymentGatewayName = "paymentGatewayName"
        static let payOrder = "order"
        static let __v = "__v"
        static let activationDate = "activationDate"
        static let paySetting = "setting"
        static let logo = "logo"
    }
    
    // MARK: Properties
    var setId: settingGateID?
    var log: logo?
    public var gatewayId: String?
    public var status: String?
    public var paymentGatewayName: String?
    public var __vv: String?
    public var activationDate: String?
    public var payOrder:String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        setId = settingGateID(json: json[SerializationKeys.paySetting])
        gatewayId = json[SerializationKeys.gatewayId].string
        status = json[SerializationKeys.status].string
        paymentGatewayName = json[SerializationKeys.paymentGatewayName].string
        __vv = json[SerializationKeys.__v].string
        activationDate = json[SerializationKeys.activationDate].string
        log = logo(json: json[SerializationKeys.logo])
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = gatewayId { dictionary[SerializationKeys.gatewayId] = value }
        if let value = setId {
            dictionary[SerializationKeys.paySetting] = value.dictionaryRepresentation() }
        
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = paymentGatewayName { dictionary[SerializationKeys.paymentGatewayName] = value }
        if let value = payOrder { dictionary[SerializationKeys.payOrder] = value }
        if let value = __vv { dictionary[SerializationKeys.__v] = value }
        if let value = activationDate { dictionary[SerializationKeys.activationDate] = value }
        if let value = log { dictionary[SerializationKeys.logo] = value.dictionaryRepresentation()}
        
        return dictionary
    }
}

class settingGateID {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let client_id = "client_id"
        static let password = "password"
        static let currency_code = "currency_code"
        static let url = "url"
        static let user_name = "user_name"
    }
    
    // MARK: Properties
    public var client_id: String?
    public var password: String?
    public var currency_code: String?
    public var url: String?
    public var user_name: String?
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        client_id = json[SerializationKeys.client_id].string
        password = json[SerializationKeys.password].string
        currency_code = json[SerializationKeys.currency_code].string
        url = json[SerializationKeys.url].string
        user_name = json[SerializationKeys.user_name].string
    }
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = client_id { dictionary[SerializationKeys.client_id] = value }
        if let value = password { dictionary[SerializationKeys.password] = value }

        if let value = currency_code { dictionary[SerializationKeys.currency_code] = value }
        if let value = url { dictionary[SerializationKeys.url] = value }
        if let value = user_name { dictionary[SerializationKeys.user_name] = value }
        return dictionary
    }
}

class logo{
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let thumb = "thumb"
        static let original = "original"
    }
    
    // MARK: Properties
    public var thumb: String?
    public var original: String?
    
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        original = json[SerializationKeys.original].string
        thumb = json[SerializationKeys.thumb].string
    }
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = original { dictionary[SerializationKeys.original] = value }
        if let value = thumb { dictionary[SerializationKeys.thumb] = value }
        return dictionary
    }
}
