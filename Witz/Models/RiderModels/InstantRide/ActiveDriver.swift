//
//  ActiveDriver.swift
//  Witz
//
//  Created by Amit Tripathi on 11/1/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ActiveDriver {
    
    var driver_Id:String?
    var vehicle_information:[String:Any]?
    var statusCode:Double?
    var accessToken:String?
    var message:String?
 
    
    static func parseActiveDriverData(data:[String:JSON], outerData:JSON)->ActiveDriver {
        
        let dataArry = data["driver"]?.array
        
        var activeDriver:ActiveDriver?
        for dict in dataArry! {
            activeDriver = ActiveDriver()
            activeDriver?.driver_Id = dict["_id"].stringValue
            activeDriver?.accessToken = dict["accessToken"].stringValue
            let vehiclInfo = dict["vehicle_information"].dictionaryValue
            activeDriver?.vehicle_information = ["color":vehiclInfo["color"] as Any, "vehicle_number":vehiclInfo["vehicle_number"] as Any, "isVerified":vehiclInfo["isVerified"] as Any, "subBrand":vehiclInfo["subBrand"] as Any, "model":vehiclInfo["model"] as Any, "year":vehiclInfo["year"] as Any, "brand":vehiclInfo["brand"] as Any, "seating_capacity":vehiclInfo["seating_capacity"] as Any]
            
            RiderManager.sharedInstance.activeDriver.append(activeDriver!)
            
        }
        var outerData = ActiveDriver()
        outerData.statusCode = data["statusCode"]?.double
        outerData.message = data["message"]?.string
        return outerData
        
    }
}
