//
//  GetPrice.swift
//  Witz
//
//  Created by Amit Tripathi on 11/1/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct GetPrice {
    
    var price:String?
    var adminEarning:String?
    var distance:Double?
    var duration:Double?
    var isShared:Bool?
    var riderTaxApplied:String?
    var tripType:String?
    var vehicleType:String?
    var higherPrice:Int?
    var lowerPrice:Int?
    
    static func parsePriceDat(data:[String:JSON], outerData:JSON)->GetPrice {
        
        var priceData = GetPrice()
        priceData.adminEarning = data["adminEarning"]?.string
        priceData.price = data["price"]?.string
        priceData.distance = data["distance"]?.double
        priceData.duration = data["duration"]?.double
        priceData.isShared = data["isShared"]?.bool
        priceData.riderTaxApplied = data["riderTaxApplied"]?.string
        priceData.vehicleType = data["vehicleType"]?.string
        priceData.tripType = data["tripType"]?.string
        priceData.higherPrice = outerData["higherPrice"].int
        priceData.lowerPrice = outerData["lowerPrice"].int

        
//        let endDict = data["endLocation"]?.dictionary
//        let coorArry = endDict!["coordinates"]?.arrayValue
//        tripData.endLocation = ["coordinates":coorArry as Any, "type":endDict!["type"] as Any]
//
//        let startDict = data["startLocation"]?.dictionary
//        let coorArry2 = startDict!["coordinates"]?.arrayValue
//        tripData.endLocation = ["coordinates":coorArry2 as Any, "type":startDict!["type"] as Any]
//
//        let bidAry = data["bidByDriver"]?.arrayValue
//        tripData.bidByDriver = (bidAry! as [AnyObject])
//
//        let rejecArry = data["bidRejectedByDriver"]?.arrayValue
//        tripData.bidRejectedByDriver = (rejecArry! as [AnyObject])
//
//        let driverDec = data["driverDeclineInstant"]?.arrayValue
//        tripData.driverDeclineInstant = (driverDec! as [AnyObject])
//
//        let driverPushQ = data["driverPushInQ"]?.arrayValue
//        tripData.driverPushInQ = (driverPushQ! as [Any])
//
//        let driverSentBook = data["sentBookingReguestToDriver"]?.arrayValue
//        tripData.sentBookingReguestToDriver = (driverSentBook! as [Any])
//
        return priceData
    }
}
