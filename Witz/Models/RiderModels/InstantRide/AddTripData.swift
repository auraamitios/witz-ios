//
//  AddTripData.swift
//  Witz
//
//  Created by Amit Tripathi on 10/30/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import SwiftyJSON
public struct AddTripData {
    
    var trip_Id:String?
    var riderId:String?
    var version:Double?
    var adminEarning:Int?
    var aggregatePaymentStatus:Bool?
    var aggregateRequestPendingByDriver:[AnyObject]?
    var bidByDriver:[AnyObject]?
    var bidRejectedByDriver:[AnyObject]?
    var bookingDate:String?
    var bridgeTax:Int?
    var createdAt:String?
    var distance:Double?
    var driverCancelationFee:Int?
    var driverDeclineInstant:[AnyObject]?
    var driverEarning:Double?
    var driverPushInQ:[Any]?
    var driverRating:Int?
    var driverReachedKM:Double?
    var driverReachedTime:Double?
    var driverTaxApplied:Bool?
    var duration:Double?
    var isFinalAggregate:Bool?
    var isShared:Bool?
    var kmTravel:Int?
    var parentSharingTrip:Int?
    var paymentStatus:String?
    var price:Int?
    var riderCancelationFee:Int?
    var riderRating:Int?
    var riderTaxApplied:Bool?
    var seatsRequired:Int?
    var sentBookingReguestToDriver:[Any]?
    var endLocation:[String:Any]?
    var startLocation:[String:Any]?
    var status:String?
    var tripType:String?
    var updatedAt:String?
    var workingTime:[Any]?
    
    
    static func parseVehiclesData(data:[String:JSON], outerData:JSON)->AddTripData{
        
        var tripData = AddTripData()
        tripData.trip_Id = data["_id"]?.string
        tripData.riderId = data["riderId"]?.string
        tripData.version = data["__v"]?.double
        tripData.adminEarning = data["adminEarning"]?.int
        tripData.aggregatePaymentStatus = data["aggregatePaymentStatus"]?.bool
        tripData.bookingDate = data["bookingDate"]?.string
        tripData.bridgeTax = data["bridgeTax"]?.int
        tripData.createdAt = data["createdAt"]?.string
        tripData.distance = data["distance"]?.double
        tripData.duration = data["duration"]?.double
        tripData.driverCancelationFee = data["driverCancelationFee"]?.int
        tripData.driverEarning = data["driverEarning"]?.double
        tripData.driverReachedKM = data["driverReachedKM"]?.double
        tripData.driverReachedTime = data["driverReachedTime"]?.double
        tripData.driverTaxApplied = data["driverReachedTime"]?.bool
        tripData.isShared = data["isShared"]?.bool
        tripData.isFinalAggregate = data["isFinalAggregate"]?.bool
        tripData.kmTravel = data["kmTravel"]?.int
        tripData.parentSharingTrip = data["parentSharingTrip"]?.int
        tripData.paymentStatus = data["paymentStatus"]?.string
        tripData.price = data["price"]?.int
        tripData.riderCancelationFee = data[""]?.int
        tripData.riderRating = data["riderRating"]?.int
        tripData.riderTaxApplied = data["riderTaxApplied"]?.bool
        tripData.seatsRequired = data["seatsRequired"]?.int
        tripData.tripType = data["tripType"]?.string
        tripData.updatedAt = data["updatedAt"]?.string
        
        let endDict = data["endLocation"]?.dictionary
        let coorArry = endDict!["coordinates"]?.arrayValue
        tripData.endLocation = ["coordinates":coorArry as Any, "type":endDict!["type"] as Any]
        
        let startDict = data["startLocation"]?.dictionary
        let coorArry2 = startDict!["coordinates"]?.arrayValue
        tripData.endLocation = ["coordinates":coorArry2 as Any, "type":startDict!["type"] as Any]
        
        let bidAry = data["bidByDriver"]?.arrayValue
        tripData.bidByDriver = (bidAry! as [AnyObject])
        
        let rejecArry = data["bidRejectedByDriver"]?.arrayValue
        tripData.bidRejectedByDriver = (rejecArry! as [AnyObject])
        
        let driverDec = data["driverDeclineInstant"]?.arrayValue
        tripData.driverDeclineInstant = (driverDec! as [AnyObject])
        
        let driverPushQ = data["driverPushInQ"]?.arrayValue
        tripData.driverPushInQ = (driverPushQ! as [Any])
        
        let driverSentBook = data["sentBookingReguestToDriver"]?.arrayValue
        tripData.sentBookingReguestToDriver = (driverSentBook! as [Any])

        return tripData
    }
}
