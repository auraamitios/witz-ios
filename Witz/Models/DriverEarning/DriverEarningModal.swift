//
//  DriverEariningModal.swift
//
//  Created by abhishek kumar on 20/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class DriverEarningModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let earning = "earning"
    static let kM = "KM"
    static let count = "count"
  }

  // MARK: Properties
  public var earning: Float?
  public var kM: Float?
  public var count: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    earning = json[SerializationKeys.earning].float
    kM = json[SerializationKeys.kM].float
    count = json[SerializationKeys.count].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = earning { dictionary[SerializationKeys.earning] = value }
    if let value = kM { dictionary[SerializationKeys.kM] = value }
    if let value = count { dictionary[SerializationKeys.count] = value }
    return dictionary
  }

}
