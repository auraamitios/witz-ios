//
//  AppManager.swift
//  Witz
//
//  Created by Amit Tripathi on 9/5/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
enum APIType:Int {
    //****** DRIVER API's ******//
    case Mobile_Verification_Driver = 10
    case Confirm_Mobile_Verification_Driver
    case Create_Password_Driver
    case Create_Driver
    case Forgot_Password
    case Driver_Login
    case Driver_Logout
    case Driver_Info
    case Driver_Duty
    
    //****** RIDER API's ******//
    case RIDER_MOBILE_VERIFICATION
    case CONFIRM_RIDER_MOBILE_VERIFICATION
    case CREATE_RIDER_PASSWORD
    case CREATE_RIDER
    case LOGIN_RIDER
    case START_INSTANT_RIDE
    case GET_ALL_VEHICLES
    case RIDER_LOGOUT
    case ADD_RIDER_TRIP
    case GET_PRICE
    case GET_SHARED_PRICE
}

class AppManager: NSObject {
    static let sharedInstance = AppManager()
    private override init() {} //This prevents others from using the default '()' initializer for this class.
    
    var loggedUserRider = Bool()
    var locationCoordinates = [CLLocation]()
    var pickUpCoordinates = CLLocationCoordinate2D()
    var dropCoordinates = CLLocationCoordinate2D()

}

