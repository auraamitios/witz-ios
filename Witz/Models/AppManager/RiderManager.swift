//
//  RiderManager.swift
//  Witz
//
//  Created by Amit Tripathi on 10/27/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit

class RiderManager: NSObject {
    static let sharedInstance = RiderManager()
    private override init() {} //This prevents others from using the default '()' initializer for this class.
    var navigationControl : UINavigationController?
    var vehiclesData = [VehiclesData]()
    var activeDriver = [ActiveDriver]()
    var riderGatewayDetail : PaymentGateway?
    var riderInfo : RiderInfo?
    var tripIDForRider : String?
    var bookedDriverId : String?
    var bookedDriverProfile : DriverBasicModal?
    var notificationCount : RiderNotificationCount?
    var comingFromReservation:Bool?
    var timeZoneId:String?
    var allCounts : RiderCountModal?
    var pickUpAddress:String?
    var dropAddress:String?
    var riderLoginData:RiderLoginData?
    var homeWorkAddressSelected:Bool?
    var currentTripData: AddTripData?
    var changePasswordFromProfile:Bool?
    var reservationBookingDate:Date?
    var resereBookDate:String?
    var nearestDriverTime : String?
}
