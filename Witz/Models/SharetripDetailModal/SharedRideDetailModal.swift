//
//  SharedRideDetailModal.swift
//
//  Created by Amit Tripathi on 1/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SharedRideDetailModal {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let logsData = "logsData"
    static let totalDistance = "totalDistance"
    static let totalDuration = "totalDuration"
  }

  // MARK: Properties
  public var logsData: LogsData?
  public var totalDistance: Float?
  public var totalDuration: Float?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    logsData = LogsData(json: json[SerializationKeys.logsData])
    totalDistance = json[SerializationKeys.totalDistance].float
    totalDuration = json[SerializationKeys.totalDuration].float
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = logsData { dictionary[SerializationKeys.logsData] = value.dictionaryRepresentation() }
    if let value = totalDistance { dictionary[SerializationKeys.totalDistance] = value }
    if let value = totalDuration { dictionary[SerializationKeys.totalDuration] = value }
    return dictionary
  }

}
public final class SharingInfoData {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let tripId = "tripId"
        static let endAddress = "end_address"
        static let driverRating = "driverRating"
        static let riderPayment = "riderPayment"
        static let riderMobile = "riderMobile"
        static let riderId = "riderId"
        static let price = "price"
        static let driverEarning = "driverEarning"
        static let endLocation = "endLocation"
        static let status = "status"
        static let id = "_id"
        static let startAddress = "start_address"
        static let riderName = "riderName"
        static let distance = "distance"
        static let startLocation = "startLocation"
        static let riderRating = "riderRating"
        static let duration = "duration"
    }
    
    // MARK: Properties
    public var tripId: String?
    public var endAddress: String?
    public var driverRating: Int?
    public var riderPayment: Float?
    public var riderMobile: String?
    public var riderId: String?
    public var price: Float?
    public var driverEarning: Float?
    public var endLocation: EndLocationShare?
    public var status: Int?
    public var id: String?
    public var startAddress: String?
    public var riderName: String?
    public var distance: Float?
    public var startLocation: StartLocationShare?
    public var riderRating: Int?
    public var duration: Float?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        tripId = json[SerializationKeys.tripId].string
        endAddress = json[SerializationKeys.endAddress].string
        driverRating = json[SerializationKeys.driverRating].int
        riderPayment = json[SerializationKeys.riderPayment].float
        riderMobile = json[SerializationKeys.riderMobile].string
        riderId = json[SerializationKeys.riderId].string
        price = json[SerializationKeys.price].float
        driverEarning = json[SerializationKeys.driverEarning].float
        endLocation = EndLocationShare(json: json[SerializationKeys.endLocation])
        status = json[SerializationKeys.status].int
        id = json[SerializationKeys.id].string
        startAddress = json[SerializationKeys.startAddress].string
        riderName = json[SerializationKeys.riderName].string
        distance = json[SerializationKeys.distance].float
        startLocation = StartLocationShare(json: json[SerializationKeys.startLocation])
        riderRating = json[SerializationKeys.riderRating].int
        duration = json[SerializationKeys.duration].float
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = tripId { dictionary[SerializationKeys.tripId] = value }
        if let value = endAddress { dictionary[SerializationKeys.endAddress] = value }
        if let value = driverRating { dictionary[SerializationKeys.driverRating] = value }
        if let value = riderPayment { dictionary[SerializationKeys.riderPayment] = value }
        if let value = riderMobile { dictionary[SerializationKeys.riderMobile] = value }
        if let value = riderId { dictionary[SerializationKeys.riderId] = value }
        if let value = price { dictionary[SerializationKeys.price] = value }
        if let value = driverEarning { dictionary[SerializationKeys.driverEarning] = value }
        if let value = endLocation { dictionary[SerializationKeys.endLocation] = value.dictionaryRepresentation() }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = startAddress { dictionary[SerializationKeys.startAddress] = value }
        if let value = riderName { dictionary[SerializationKeys.riderName] = value }
        if let value = distance { dictionary[SerializationKeys.distance] = value }
        if let value = startLocation { dictionary[SerializationKeys.startLocation] = value.dictionaryRepresentation() }
        if let value = riderRating { dictionary[SerializationKeys.riderRating] = value }
        if let value = duration { dictionary[SerializationKeys.duration] = value }
        return dictionary
    }
    
}
public final class StartLocationShare {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}
public final class EndLocationShare {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let type = "type"
        static let coordinates = "coordinates"
    }
    
    // MARK: Properties
    public var type: String?
    public var coordinates: [Float]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        type = json[SerializationKeys.type].string
        if let items = json[SerializationKeys.coordinates].array { coordinates = items.map { $0.floatValue } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = coordinates { dictionary[SerializationKeys.coordinates] = value }
        return dictionary
    }
    
}
public final class LogsData {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let numberOfPersonRiding = "numberOfPersonRiding"
        static let id = "_id"
        static let sharedTripId = "sharedTripId"
        static let sharingInfoData = "sharingInfoData"
        static let v = "__v"
        static let driverId = "driverId"
        static let updatedAt = "updatedAt"
        static let maxSeatingCapacity = "maxSeatingCapacity"
    }
    
    // MARK: Properties
    public var numberOfPersonRiding: Int?
    public var id: String?
    public var sharedTripId: String?
    public var sharingInfoData: [SharingInfoData]?
    public var v: Int?
    public var driverId: String?
    public var updatedAt: String?
    public var maxSeatingCapacity: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        numberOfPersonRiding = json[SerializationKeys.numberOfPersonRiding].int
        id = json[SerializationKeys.id].string
        sharedTripId = json[SerializationKeys.sharedTripId].string
        if let items = json[SerializationKeys.sharingInfoData].array { sharingInfoData = items.map { SharingInfoData(json: $0) } }
        v = json[SerializationKeys.v].int
        driverId = json[SerializationKeys.driverId].string
        updatedAt = json[SerializationKeys.updatedAt].string
        maxSeatingCapacity = json[SerializationKeys.maxSeatingCapacity].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = numberOfPersonRiding { dictionary[SerializationKeys.numberOfPersonRiding] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = sharedTripId { dictionary[SerializationKeys.sharedTripId] = value }
        if let value = sharingInfoData { dictionary[SerializationKeys.sharingInfoData] = value.map { $0.dictionaryRepresentation() } }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = driverId { dictionary[SerializationKeys.driverId] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = maxSeatingCapacity { dictionary[SerializationKeys.maxSeatingCapacity] = value }
        return dictionary
    }
    
}
