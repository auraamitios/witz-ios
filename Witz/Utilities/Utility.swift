//
//  Utility.swift
//  DealXpress
//
//  Created by Amit Tripathi on 2/21/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Alamofire
import SwifterSwift
import SlideMenuControllerSwift
import SVProgressHUD
import NVActivityIndicatorView

protocol UtilityTimerDelegate:NSObjectProtocol {
    
    func countDownTimer(value:Int)
}
class Utility: NSObject {
    
    static weak var delegate:UtilityTimerDelegate?
    static var timeForTimer = 0
    static var timer: Timer?
    
    class func addEqualConstraints(for view: UIView, inSuperView superView: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        superView.addSubview(view)
        let bottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: superView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: superView, attribute: .left, multiplier: 1.0, constant: 0)
        let rightConstraint = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: superView, attribute: .right, multiplier: 1.0, constant: 0)
        let topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: superView, attribute: .top, multiplier: 1.0, constant: 0)
        superView.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
    }
    
    class func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect.init(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext() ?? image
        UIGraphicsEndImageContext()
        return newImage
    }
  
    class func isConnectedToNetwork()->Bool{
        if !NetworkReachabilityConstructs.sharedInstance.isReachable {
            return false
        }else{
            return true
        }
    }
    
    class func windowHeight()->CGFloat {
        let screenSize = UIScreen.main.bounds
        return screenSize.height
    }
    class func windowWidth()->CGFloat {
        let screenSize = UIScreen.main.bounds
        return screenSize.width
    }
    
    class func showNetworkIndicator() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    class func hideNetworkIndicator() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    final class NetworkReachabilityConstructs {
        private static var networkReachbility = NetworkReachabilityManager();
        private init() {
            NetworkReachabilityConstructs.networkReachbility?.startListening()
        }
        deinit{
            NetworkReachabilityConstructs.networkReachbility?.stopListening()
        }
        static var sharedInstance:NetworkReachabilityManager{
            get{
                return NetworkReachabilityConstructs.networkReachbility!
            }
        }
    }
    
    class func jumpToMapScreen() {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "RootNav")
        let appDelegate = AppDelegate.sharedInstance()
        appDelegate.window?.rootViewController = rootVC
    }
    
    class func jumpToLoginScreen() {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "LoginNav")
        let appDelegate = AppDelegate.sharedInstance()
        appDelegate.window?.rootViewController = rootVC
    }
    
    class func driverSideMenuSetUp () {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Main, bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "RootNav")
        let leftVC = storyBoard.instantiateViewController(withIdentifier: "LeftViewController")
        let appDelegate = AppDelegate.sharedInstance()
        
        let slideMenuController = ExSlideMenuController(mainViewController:rootVC, leftMenuViewController: leftVC)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = rootVC as? SlideMenuControllerDelegate
        appDelegate.window?.rootViewController = slideMenuController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    class func riderSideMenuSetUp() {
        let storyBoard = UIStoryboard(name: LinkStrings.StoryboardName.Rider, bundle: nil)
        let rootVC = storyBoard.instantiateViewController(withIdentifier: "RootRider")
        let leftVC = storyBoard.instantiateViewController(withIdentifier: "RiderLeftMenuViewController")
        let appDelegate = AppDelegate.sharedInstance()
        
        let slideMenuController = RiderSlideMenu(mainViewController:rootVC, leftMenuViewController: leftVC)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = rootVC as? SlideMenuControllerDelegate
        appDelegate.window?.rootViewController = slideMenuController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    //MARK:: fetch Rider detail on Auto login
    
    class func getLoggedRiderDetail(_ success:@escaping (_ complete : Bool )  -> Void ) {
        guard Utility.isConnectedToNetwork() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/accesstokenlogin", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            print("response\(String(describing: response))")
            if response?["statusCode"] == 200 {
                //DispatchQueue.main.async {
                    let rider = RiderInfo.parseRiderInfoJson(data1: response)
                    RiderManager.sharedInstance.riderInfo = rider
                    //**** TODO::Dec20******
                    let riderData = RiderLoginData.init(json: response?["data"] ?? ["":""])
                    RiderManager.sharedInstance.riderLoginData = riderData
                success(true)
                //}
            }
            
        }, failure: { (error) in
            print(error?.localizedDescription as Any)
        })
    }
    
      //MARK::API's call
        class func getRiderTripsCounts(_ success:@escaping (_ complete : Bool )  -> Void ) {
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/getCounts", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            print(response ?? "" )
            if response?["statusCode"] == 200 {
                let riderCount = RiderCountModal.init(json: response?["data"] ?? "")
            RiderManager.sharedInstance.allCounts = riderCount
             success(true)
            }
        }) { (error ) in
            
        }
    }
    class func getTimeAndDistance(data:[Any]) ->[String] {
        let detail = data[0] as! [String:Any]
        let dist = detail["distance"] as? [String:Any]
        let time = detail["duration"] as? [String:Any]

        let distStr = dist!["text"] as! String
        let timeStr = time!["text"] as! String

        print("distance-->\(distStr)")
        print("time-->\(timeStr)")

        var finalTime = ""
        if timeStr.contains("hours") {
            var hrs = timeStr.components(separatedBy:" hours")
            //            print(hrs)
            var mins = [""]
            if hrs[1].contains("mins") {
                mins = hrs[1].components(separatedBy:" mins")
            }else {
                mins = hrs[1].components(separatedBy:" min")
            }
            //            print(mins)
            let hours = hrs[0]
            //            print(hours)
            var min = mins[0]
            //            print(min)
            let hrs1 = (hours.int ?? 0) * 60
            //            print(hrs1)
            min = min.replacingOccurrences(of: " ", with: "")
            //            print(min)
            //            print(min.int ?? 0)
            let tDurs = (min.int ?? 0) + hrs1
            //            print(tDurs)
            finalTime = "\(tDurs)"
        }else if timeStr.contains("hour"){
            var hrs = timeStr.components(separatedBy:" hour")
            //            print(hrs)
            var mins = [""]
            if hrs[1].contains("mins") {
                mins = hrs[1].components(separatedBy:" mins")
            }else {
                mins = hrs[1].components(separatedBy:" min")
            }
            //            print(mins)
            let hours = hrs[0]
            //            print(hours)
            var min = mins[0]
            //            print(min)
            let hrs1 = (hours.int ?? 0) * 60
            //            print(hrs1)
            min = min.replacingOccurrences(of: " ", with: "")
            //            print(min)
            //            print(min.int ?? 0)
            let tDurs = (min.int ?? 0) + hrs1
            //            print(tDurs)
            finalTime = "\(tDurs)"
        }
        else {
            finalTime = timeStr.replacingOccurrences(of: " mins", with: "")
        }
        let finalDist = distStr.replacingOccurrences(of: " km", with: "")
        print("journey duration in mins----\(finalTime)")

        return [finalDist,finalTime]
    }
    
    //MARK:: get rider notification badge count
    
    class func getRiderNotificationBadgeCount() {
        guard Utility.isConnectedToNetwork() else {return}
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/getCounts", withInputString: ["":""], requestType: .post, isAuthorization: true, success: { (response) in
            print("response..Counts>>>>>\(String(describing: response))")
            if response?["statusCode"] == 200 {
                let modal = RiderNotificationCount.init(json: response!)
                RiderManager.sharedInstance.notificationCount = modal
            }
            
        }, failure: { (error) in
            print(error?.localizedDescription as Any)
        })
    }
    
    //update rider location to server
    class func updateRiderLocation() {
        guard Utility.isConnectedToNetwork() else {return}
        let locations = AppManager.sharedInstance.locationCoordinates
        let coordinates = locations.last?.coordinate
        let params = ["lat": (coordinates?.latitude ?? 0), "lng": (coordinates?.longitude ?? 0)]
        RiderConnectionManager().makeAPICallUrlEncoded(functionName: "rider/update_location", withInputString: params, requestType: .post, isAuthorization: true, success: { (response) in
            print("response..>>>\(String(describing: response))")
            
        }, failure: { (error) in
            print(error?.localizedDescription as Any)
        })
    }
    
    //MARK:: reservation time calculations
    class func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    class func getMinsFromHours(hours:Int)-> Int {
        return (hours * 60)
    }
    class func getMinsFromSeconds(seconds:Int)-> Int {
        return ((seconds % 36400) / 60)
    }
    
    //MARK:- Driver Update Modal Info
    class func getDriverInfoRefreshed() {
    guard Utility.isConnectedToNetwork() else {return}
    var localTimeZoneName: String { return TimeZone.current.identifier }
        WitzConnectionManager().makeAPICallUrlEncoded(functionName: "driver/accesstokenlogin", withInputString: ["timezone":localTimeZoneName], requestType: .put, isAuthorization: true, success: { (response) in
            if response?["statusCode"] == 200{
                DispatchQueue.main.async {
                let driver = DriverInfo.parseDriverInfoJson(data1: response)
                DriverManager.sharedInstance.user_Driver = driver
                }
            }
        }) { (error) in
            
        }
    }
    
    //MARK:- Week days calculation Aggregate
    class func aggregateDays(data:Trips)->String {
        var daysStr = ""
        let count:Int = (data.workingTime?.count)!
        for i in 0..<count {
            let dict = data.workingTime?[i]
            switch dict?.dayNumber {
            case 0?:
                daysStr = "\(daysStr)\("S ")"
                break
            case 1?:
                daysStr = "\(daysStr)\("M ")"
                break
            case 2?:
                daysStr = "\(daysStr)\("T ")"
                break
            case 3?:
                daysStr = "\(daysStr)\("W ")"
                break
            case 4?:
                daysStr = "\(daysStr)\("T ")"
                break
            case 5?:
                daysStr = "\(daysStr)\("F ")"
                break
            case 6?:
                daysStr = "\(daysStr)\("S ")"
                break
            default:
                break
            }
        }
        
        return daysStr
    }
    
    class func aggregateDaysDriver(data:AgrregateTripModal)->String {
        var daysStr = ""
        let count:Int = data.driverWorkingTime?.dayNumber?.count ?? 0
        for i in 0..<count {
            let number = data.driverWorkingTime?.dayNumber?[i]
            switch number {
            case 0?:
                daysStr = "\(daysStr)\("S ")"
                break
            case 1?:
                daysStr = "\(daysStr)\("M ")"
                break
            case 2?:
                daysStr = "\(daysStr)\("T ")"
                break
            case 3?:
                daysStr = "\(daysStr)\("W ")"
                break
            case 4?:
                daysStr = "\(daysStr)\("T ")"
                break
            case 5?:
                daysStr = "\(daysStr)\("F ")"
                break
            case 6?:
                daysStr = "\(daysStr)\("S ")"
                break
            default:
                break
            }
        }
        
        return daysStr
    }
    
    //MARK:- TIMER Function
    
    class func runCode(at date:Date, _ code:DispatchWorkItem?)
    {
       let timeInterval = date.timeIntervalSinceNow
       self.stopTimer(after: timeInterval, code)
    }
      
    // Stop Timer after specfic time
   class func stopTimer(after timeInterval:TimeInterval, _ code:DispatchWorkItem?)
    {
        Utility.timeForTimer = Int(timeInterval)
        print(timeInterval)
        Utility.startTimer()
        DispatchQueue.main.asyncAfter(
            deadline: .now() + timeInterval,
            execute:code!)
    }

    class func startTimer() {
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Utility.loop), userInfo: nil, repeats: true)
        }
    }
    
    class func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
   class func loop() {
        Utility.timeForTimer =  Utility.timeForTimer-1
        Utility.delegate?.countDownTimer(value: Utility.timeForTimer)
   }
    //MARK::Time format 12 hours to 24 hours
    class func convertTime12HrsTo24Hrs(selectedDate:String)->String {
        let dateAsString = selectedDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "HH:mm"
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        return Date24
    }
    
    //MARK::Time format 24 hours to 12 hours
    class func convertTime24HrsTo12Hrs(selectedDate:String)->String {
        let dateAsString = selectedDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        print("12 hour formatted Date:",Date12)
        return Date12
    }

}
extension Formatter {
    // create static date formatters for your date representations
    static let preciseLocalTime: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss.SSS"
        return formatter
    }()
    static let preciseGMTTime: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "HH:mm:ss.SSS"
        return formatter
    }()
    
    public func createUTCDate(fromString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        guard let date = dateFormatter.date(from: fromString) else {
            return Date()
        }
        return date
    }
}
public extension UISearchBar {
    public func setNewcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
    }
}
extension Double {
    var dollarString:String {
        return String(format: "$%.2f", self)
    }
}

extension Int {
    var seconds: Int {
        return self
    }
    var minutes: Int {
        return self.seconds * 60
    }
    var hours: Int {
        return self.minutes * 60
    }
    var days: Int {
        return self.hours * 24
    }
    var weeks: Int {
        return self.days * 7
    }
    var months: Int {
        return self.weeks * 4
    }
    var years: Int {
        return self.months * 12
    }
}
extension Int {
    var twoDecimalString:String {
        return String(format: "%.2f", Double(self))
    }
    var dollarString:String {
        return String(format: "$%.2f", self)
    }
}
extension Float {
    var twoDecimalString:String {
        return String(format: "%.2f", self)
    }
    var dollarString:String {
        return String(format: "$%.2f", self)
    }
}
extension String {
    var nameFormatted : String{
        var token = self.components(separatedBy: " ")
        if token.count > 1{
            return token[0].uppercased() + " " + (token[1].firstCharacterAsString?.uppercased() ?? "") + "."
        }
        return token[0].uppercased()
    }
    
    var westernArabicNumeralsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
            .flatMap { pattern ~= $0 ? Character($0) : nil })
    }
    
//    var numbers: String {
//        return String(characters.filter { "0"..."9" ~= $0 })
//    }
    
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.count
    }
    
    func isBlank() -> Bool {
        
        if  self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            return true
        }
        if self.isEmpty{
            return true
        }
        return false
    }
    
//    func contains(find: String) -> Bool{
//        return self.range(of: find) != nil
//    }
//    func containsIgnoringCase(find: String) -> Bool{
//        return self.range(of: find, options: .caseInsensitive) != nil
//    }
    
}

extension UICollectionView {
    func setItemsInRow(_ items: Int) {
        if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
            let contentInset = self.contentInset
            let itemsInRow: CGFloat = CGFloat(items);
            let innerSpace = layout.minimumInteritemSpacing * (itemsInRow - 1.0)
            let insetSpace = contentInset.left + contentInset.right + layout.sectionInset.left + layout.sectionInset.right
            let width = floor((frame.width - insetSpace - innerSpace) / itemsInRow);
            layout.itemSize = CGSize(width: width, height: layout.itemSize.height)
        }
    }
    
    func deselectAllItems(animated: Bool = false) {
        for indexPath in self.indexPathsForSelectedItems ?? [] {
            self.deselectItem(at: indexPath, animated: animated)
        }
    }
    
}
extension UIViewController {
    func checkNetworkStatus()->Bool {
        if !Utility.isConnectedToNetwork() {
            AlertHelper.showSettingsAlertView()
            return false
        }
        return true
    }
    
    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "hambuger-menu")!)//
        //self.addRightBarButtonWithImage(UIImage(named: "ic_notifications_black_24dp")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
}

extension UILabel
{
    var optimalHeight : CGFloat
    {
        get
        {
            let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = self.lineBreakMode
            label.font = self.font
            label.text = self.text
            
            label.sizeToFit()
            return label.frame.height
        }
    }
}
extension UIView {
    func fadeIn(duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(duration: TimeInterval = 1.0, delay: TimeInterval = 3.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)  }
    
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    class func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        let className = String.className(viewType)
        return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
    
    class func loadNib() -> Self {
        return loadNib(self)
    }
    
    func shake(count : Float? = nil,for duration : TimeInterval? = nil,withTranslation translation : Float? = nil) {
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        animation.repeatCount = count ?? 2
        animation.duration = (duration ?? 0.5)/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.byValue = translation ?? -5
        layer.add(animation, forKey: "shake")
    }
}

extension UISearchBar {
    public func setSerchTextcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
    }
    public func hideClearButton() {
        
        let subview: UIView? = self.subviews.first
        //SearchBar only have one subview (UIView)
        //There are three sub subviews (UISearchBarBackground, UINavigationButton, UISearchBarTextField)
        for subsubview: UIView in (subview?.subviews)! {
            //The UISearchBarTextField class is a UITextField. We can't use UISearchBarTextField directly here.
            if (subsubview is UITextField) {
                (subsubview as? UITextField)?.clearButtonMode = .never
            }
        }
    }
}

extension Collection {
    public func chunk(n: IndexDistance) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}
extension UIColor {
    
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }
    
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
}

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
    func stringByRemovingAll(subStrings: [String]) -> String {
        var resultString = self
       _ = subStrings.map { resultString = resultString.replacingOccurrences(of: $0, with: "") }
        return resultString
    }
}

extension UIViewController : NVActivityIndicatorViewable {
    
    func showLoader(_ msg: String? = ""){
        DispatchQueue.main.async { self.startAnimating(CGSize.init(width: 70, height: 70) , message: msg,type:NVActivityIndicatorType.ballRotateChase)}
    }
//
    func hideLoader(){
        DispatchQueue.main.async {self.stopAnimating()}
    }
    
//    public func showLoader()  {
//        ActivityIndicatorUtil.enableActivityIndicator(self.view, status: nil, mask: SVProgressHUDMaskType.custom, maskColor: SpinnerBGColor, style: SVProgressHUDStyle.dark)
//    }

//    public func hideLoader()  {
//        SVProgressHUD.dismiss()
//    }
}

