//
//  Constants.swift
//  DealXpress
//
//  Created by Amit Tripathi on 2/20/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Foundation

enum NotificationType : Int {
    case RIDE_REQUEST = 1
    case RIDE_REQUEST_ACCEPTED_BY_DRIVER = 2
    case DRIVER_REACHED_STARTING_LOCATION = 3
    case INFORM_DRIVER = 4
    case REACHED_ARRIVAL_LOCATION = 5
    case RIDE_CANCEL_BY_RIDER = 6
    case DRIVER_START_TRIP = 7
    case BOOKING_REQUEST = 8
    case SEND_OFFER_PRICE = 9
    case ACCEPT_BOOKING_OFFER = 10
    case BOOKING_REMINDER = 11
    case RIDE_CANCEL_BY_DRIVER = 20
    case NO_DRIVER = 21
    case BRIDGE_CROSSED = 22
    case RIDER_NOT_COMING = 23
    case NEW_AGGREGATE_REQUEST = 24
    case AGGREGATE_BID_OFFERS_RIDER = 25
    case RESERVATION_RIDE_CANCEL_BY_DRIVER = 26
    case OFFER_PRICE_NOT_SEND_BY_DRIVER = 27
    case RIDER_CASHBACK_ADDED_IN_WALLET = 28
    case CASH_ADDED_DRIVER_WALLET = 29
    case FINAL_AGGREGATE_RIDE = 30
    case RIDER_AGGREGATE_CONFIRM_BY_MANAGER = 31
    case ADMIN_NEW_BID_NOTIFICATION = 32
    case CUSTOMER_ACCEPT_NOTIFICATION = 33
    case NEW_AGGREGATE_TRIP_RIDER = 34
    case DRIVER_DOCUMENT_ACTION_ADMIN = 36
    
}

enum SharingTripType : Int {
    case QUEUING = 1
    case ON_THE_WAY = 2
    case ONGOING = 3
    case CANCELLED_BY_RIDER = 4
    case CANCELLED_BY_DRIVER = 5
    case COMPLETED = 6
    case REACHED = 7
    case REJECTED = 8
    case PAYMENT_PENDING = 9
    case BOOKING_ACCEPTED = 10
    case BOOKING_REJECTED = 11
    case RIDER_NOT_REACHED = 12
    case AGGREGATE_TRIP_ACCEPTED_DRIVER = 13
    case AGGREGATE_TRIP_HAVE_BIDS = 14
    case AGGREGATE_TRIP_ACCEPTED_BY_RIDER = 15
    case RESERVATION_BOOKING_PENDING = 16
    case ADMIN_CREATED_YOUR_AGGREGATE_TRIP = 17
    case ADMIN_CREATED_FINAL_AGGREGATE_TRIP = 19
    case Driver_CANCEL_TRIP_PNALITY = 20
    case RIDER_CANCEL_TRIP_PNALITY = 21
    case ONHOLD = 22
    case RIDER_CANCELED_AGGREGATE_TRIP_REFUND = 23
    case ABSENT_RIDER_AGGREGATE_TRIP_NOT_COMING = 24
    case TRIPS_AGGREGATE_READY_START_BY_DRIVER = 25
}

enum AggregateRequestDay:Int {
    case Monday = 0
    case Tuesday
    case Wednesday
    case Thrusday
    case Friday
    case Saturday
    case Sunday
}
enum FlagType : Int{
    case TURKEY = 0
    case SAUDI
    case INDIA
}

enum JourneyType : Int{
    case PAST = 0
    case FUTURE
    case PENDING
}

let SpinnerBGColor : UIColor = UIColor (red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.3)//white transaprent
let App_Base_Color : UIColor = UIColor.init(hex: "DEB850")
let App_Text_Color : UIColor = UIColor.init(hex: "4E5959")
let Default_Time_Zone = "Asia/Kolkata"

let APPLEOFFICELOCATION_LAT = 37.33182
let APPLEOFFICELOCATION_LNG =  -122.03118

let RideDismiss = "RIDEDISMISS"
let RideDismissShare = "RIDEDISMISSSHARE"
let RideDismissReservation = "RESERVATIONDISMISS"
let RemoveOneRide = "REMOVEONERIDE"


//MARK::Base URL for image

//let kBaseURL = "http://207.154.239.203:1003/api/"       //:-Dev server URL
//let kBaseURL = "http://207.154.239.203:1005/api/"     //:-Prod. server URL
//let kBaseImageURL = "http://207.154.239.203:1003"  //******** dev URL *******//
//let kBaseImageURL = "http://207.154.239.203:1005"  //******** Live URL *******//
//let kBaseImageURL = "http://35.158.85.91:3002"       //:-AWS server URL old

//MARK::AWS server URL
//let kBaseURL = "http://35.158.85.91:3002/api/"     //:-AWS server URL old

let kBaseImageURL = "http://35.158.85.91:3005"       //:-AWS server URL active
let kBaseURL = "http://35.158.85.91:3005/api/"     //:-AWS server URL active

//MARK::Help pages link

let faqURL = "http://stage1.witz.com.tr/#/public/faq"
let termURL = "http://stage1.witz.com.tr/#/public/terms"
let privacyURL = "http://stage1.witz.com.tr/#/public/privacy"
let tagURL = "http://stage1.witz.com.tr/#/public/tag"

//MARK::HERE Map Keys
let HERE_MAP_APP_ID = "TDbIa1RuhJYBbVLlkyah"
let HERE_MAP_APP_CODE = "524_HlmMx95xRVyk2G-iQA"

//MARK::Google Map key
let GOOGLE_MAP_API_KEY = "AIzaSyBOCZ8tABf2SKywGuS2fe_M-nqh_Kw3ZD8"
let GOOGLE_PLACES_API_KEY = "AIzaSyBiiHt19Mml2y6mBBXMdXNU33_a5K9FbbE"
let GOOGLE_DIRECTIONS_API_KEY = "AIzaSyBuCIarKFWOdx6fwSLbEjI1uwTY4h8Mz9s"
let GOOGLE_STYLE_SILVER =  """
[
{
"elementType": "geometry",
"stylers": [
{
"color": "#f5f5f5"
}
]
},
{
"elementType": "labels.icon",
"stylers": [
{
"visibility": "off"
}
]
},
{
"elementType": "labels.text.fill",
"stylers": [
{
"color": "#616161"
}
]
},
{
"elementType": "labels.text.stroke",
"stylers": [
{
"color": "#f5f5f5"
}
]
},
{
"featureType": "administrative.land_parcel",
"elementType": "labels.text.fill",
"stylers": [
{
"color": "#bdbdbd"
}
]
},
{
"featureType": "poi",
"elementType": "geometry",
"stylers": [
{
"color": "#eeeeee"
}
]
},
{
"featureType": "poi",
"elementType": "labels.text.fill",
"stylers": [
{
"color": "#757575"
}
]
},
{
"featureType": "poi.park",
"elementType": "geometry",
"stylers": [
{
"color": "#e5e5e5"
}
]
},
{
"featureType": "poi.park",
"elementType": "labels.text.fill",
"stylers": [
{
"color": "#9e9e9e"
}
]
},
{
"featureType": "road",
"elementType": "geometry",
"stylers": [
{
"color": "#ffffff"
}
]
},
{
"featureType": "road.arterial",
"elementType": "labels.text.fill",
"stylers": [
{
"color": "#757575"
}
]
},
{
"featureType": "road.highway",
"elementType": "geometry",
"stylers": [
{
"color": "#dadada"
}
]
},
{
"featureType": "road.highway",
"elementType": "labels.text.fill",
"stylers": [
{
"color": "#616161"
}
]
},
{
"featureType": "road.local",
"elementType": "labels.text.fill",
"stylers": [
{
"color": "#9e9e9e"
}
]
},
{
"featureType": "transit.line",
"elementType": "geometry",
"stylers": [
{
"color": "#e5e5e5"
}
]
},
{
"featureType": "transit.station",
"elementType": "geometry",
"stylers": [
{
"color": "#eeeeee"
}
]
},
{
"featureType": "water",
"elementType": "geometry",
"stylers": [
{
"color": "#c9c9c9"
}
]
},
{
"featureType": "water",
"elementType": "labels.text.fill",
"stylers": [
{
"color": "#9e9e9e"
}
]
}
]
"""

//MARK::Cell Identifier
let bottomCellIdentifier =  "ButtonCell"

//Define the version for testing purpose only
let versionBuild = "39"

//MARK::Twillio Mobile Number to show

let twillioNumber = "+12092276868"

extension NSDictionary {
    func toDictionary() ->[String : AnyObject] {
        var dict = [String : AnyObject]()
        for key in self.allKeys {
            dict[key as! String] = self.value(forKey: key as! String) as AnyObject?
        }
        return dict
    }
}

//MARK:: email validation

func isValidEmail(testStr:String) -> Bool {
    // println("validate calendar: \(testStr)")
    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}
