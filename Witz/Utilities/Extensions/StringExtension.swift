//
//  StringExtension.swift
//  Witz
//
//  Created by abhishek kumar on 15/11/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

public extension  String {
    // convert string date to NSdate
    func convertDateFormatter(formatNeeded:String , formatHave:String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatHave //this your string date format
//        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        guard let date = dateFormatter.date(from: self) else {
            
            return ""
        }
        dateFormatter.dateFormat = formatNeeded ///this is you want to convert format
//        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let dateStamp = dateFormatter.string(from: date)
        
        return dateStamp
    }
    func convertDateFormatterWithTimeZone(formatNeeded:String , formatHave:String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatHave //this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        guard let date = dateFormatter.date(from: self) else {
            
            return ""
        }
        dateFormatter.dateFormat = formatNeeded ///this is you want to convert format
                dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let dateStamp = dateFormatter.string(from: date)
        
        return dateStamp
    }
    func getDateFromString(formatHave:String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatHave //this your string date format
       
         let date = dateFormatter.date(from: self)
        return date ?? Date()
    }
}
