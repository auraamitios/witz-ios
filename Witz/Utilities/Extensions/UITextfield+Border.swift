//
//  UITextfield+Border.swift
//  EchoLimo
//
//  Created by Deepak Khiwani on 8/18/15.
//  Copyright (c) 2015 Deepak Khiwani. All rights reserved.
//

import UIKit


public extension  UITextField {
    
    func setCornerRadiusAndColor(cornerRadius: CGFloat,borderWidth: CGFloat, color:UIColor) -> Void{
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
    }
    
    func setPlaceholder(placeholderString: NSString, color:UIColor) -> Void{
        self.attributedPlaceholder = NSAttributedString(string:placeholderString as String,
            attributes:[NSForegroundColorAttributeName: color])
    }
    
    func setMargin(margin: CGFloat){
        self.layer.sublayerTransform = CATransform3DMakeTranslation(margin, 0, 0)
    }
    
}

private var maxLengths = [UITextField: Int]()
extension UITextField {
     @IBInspectable var maxLength: Int {
        get {
            guard let length = maxLengths[self] else {
                return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            addTarget(
                self,
                action: #selector(limitLength),
                for: UIControlEvents.editingChanged
            )
        }
    }
    
    func limitLength(textField: UITextField) {

        guard let prospectiveText = textField.text, prospectiveText.count > maxLength else {
                return
        }
        let selection = selectedTextRange
        text = prospectiveText.substring(with: prospectiveText.startIndex ..< prospectiveText.startIndex.advanced(by: maxLength))
        selectedTextRange = selection
    }
    
}
