//
//  UserDefaultManager.swift
//  Nehao
//
//  Created by Amit Tripathi on 1/23/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

public class UserDefaultManager: NSObject {
    static let sharedManager = UserDefaultManager()
    private override init() {} //This prevents others from using the default '()' initializer for this class.

    private var userDefault:UserDefaults = UserDefaults.standard
    
    //MARK:: stored value as AnyObject
    func addValue(object:AnyObject,key:String){
        userDefault.set(object, forKey: key)
        userDefault.synchronize()
    }
    
    //MARK:: method to fetch stored value
    func objectForKey(key:String) -> AnyObject? {
        return userDefault.object(forKey: key) as AnyObject?
    }
    
    //MARK:: method to fetch Bool value
    func boolForKey(Key:String)->Bool{
        return userDefault.bool(forKey: Key)
    }
    //MARK:: method to stored Bool value
    func setBool(key:String){
        userDefault.set(true, forKey: key)
        userDefault.synchronize()
    }
    func setDynamicBool(object:Bool,key:String){
        userDefault.set(object, forKey: key)
        userDefault.synchronize()
    }

    //MARK:: method to remove value from userdefault
    func removeValue(key:String){
        userDefault.removeObject(forKey: key)
        userDefault.synchronize()
    }
}

