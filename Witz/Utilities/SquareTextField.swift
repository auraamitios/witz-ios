 //
//  SquareTextField.swift
//  DealXpress
//
//  Created by Amit Tripathi on 3/23/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class SquareTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.textColor = UIColor.black
        self.textAlignment = .center
    }
    // placeholder position
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
    // text position
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }


}
