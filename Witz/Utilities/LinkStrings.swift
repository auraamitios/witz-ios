//
//  LinkStrings.swift
//  Alchemy
//
//  Created by Amit Tripathi on 1/23/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

public struct LinkStrings {
    
    public struct App {
        static let AppName = NSLocalizedString("Witz", comment: "Witz app name")
    }
    public struct MostCommon {
        static let Ok = NSLocalizedString("OK", comment: "OK")
        static let Yes = NSLocalizedString("Yes", comment: "Yes")
        static let No = NSLocalizedString("No", comment: "No")
        static let cancel = NSLocalizedString("Cancel", comment: "Cancel")
        static let Retry = NSLocalizedString("Retry", comment: "Retry")
    }
    public struct StoryboardName {
        static let Main = NSLocalizedString("Main", comment: "Main Storyboard")
        static let Rider = NSLocalizedString("Rider", comment: "Rider Storyboard")
        static let DatePicker = NSLocalizedString("WitzDatePicker", comment: "Picker Storyboard")
        static let RiderAggregate = NSLocalizedString("RiderAggregate", comment: "R Aggregate")
        static let DriverAggregate = NSLocalizedString("DriverAggregate", comment: "D Aggregate")
    }
    
    public struct TripsType {
        static let PastJourney = NSLocalizedString("Past Journeys", comment: "Past Journeys")
        static let FutureJourney = NSLocalizedString("Future Journeys", comment: "Future Journeys")
        static let PendingJourney = NSLocalizedString("Pending Request", comment: "Pending Journeys")
        
        static let Instant = NSLocalizedString("Instant Journey", comment: "INSTANT JOURNEY")
        static let Reservation = NSLocalizedString("Reservation", comment: "RESERVATION")
        static let Aggregate = NSLocalizedString("Aggregate", comment: "AGGREGATE")
    }
    
    public struct RiderObserverKeys {
        static let ShowDriverInfo = NSLocalizedString("DriverInfo", comment: "DriverInfo")
        static let RideTime = NSLocalizedString("RideTimeRider", comment: "RideTimeRider")
        static let RideStart = NSLocalizedString("RideStart", comment: "RideStart")
        static let Tripended = NSLocalizedString("FinishTripInstant", comment: "FinishTripInstant")
        static let PleaseSelectVehicle = NSLocalizedString("Please Select Vehicle Type", comment: "Please Select Vehicle Type")
        static let ShowRetryOption = NSLocalizedString("ShowRetryOption", comment: "ShowRetryOption")
         static let  RideCancelMsg = NSLocalizedString("If you do not cancel in 5 min. you can be charged.", comment: "cancel Msg")
        static let  WannaCancel = NSLocalizedString("Do you want to cancel?", comment: "cancel ride")
    }
    public struct Notifications {
        static let Title = NSLocalizedString("Notifications", comment: "Notifications title")
        static let WantDelete = NSLocalizedString("This action will delete All your notification", comment: "Delete all notification")
        static let TimeOut = NSLocalizedString("Time passed to accept the request", comment: "timeout")
       static let TimeOutOffer = NSLocalizedString( "Time passed to accept an offer", comment: "timeoutoffer")
    }
    public struct Location {
        static let Denied = NSLocalizedString("Location Services Disabled", comment: "Disabled")
    }
    public struct AlertTitle {
        static let Title = NSLocalizedString("Witz", comment: "App Name")
        static let Soon = NSLocalizedString("Coming Soon..", comment: "Coming Soon..")
    }
    public struct RiderReservation {
        static let PastTimeBooking = NSLocalizedString("Booking in past time is not allowed", comment: "Past booking")
        static let NoOffer = NSLocalizedString("There is no offer for your reservation", comment: "No offer")
        static let Change = NSLocalizedString("Change", comment: "Change")

    }
    
    public struct TitleForView {
        static let Profile = NSLocalizedString("Profile", comment: "Profile Title")
        static let EditProfile = NSLocalizedString("Edit Profile", comment: "Edit Profile Title")
        static let Notification = NSLocalizedString("Notifications", comment: "Notifications Title")
        static let TripDetails = NSLocalizedString("Trip Details", comment: "Trip Details Title")
        static let Contact = NSLocalizedString("Contact", comment: "Contact Title")
        static let MyWallet = NSLocalizedString("My Wallet", comment: "My Wallet Title")
        static let AddCard = NSLocalizedString("Add Card", comment: "Add Card Title")
        static let EditCard = NSLocalizedString("Edit Card", comment: "Edit Card Title")
        static let CarOnRoad = NSLocalizedString("Your car is on the road", comment: "Car On Road Title")
        static let PastTrip = NSLocalizedString("Past Journey", comment: "Past Trip Title")
        static let FutureJourney = NSLocalizedString("Future Journey", comment: "Journey Future Title")
        static let JourneyPlan = NSLocalizedString("Journey Plan", comment: "Journey Plan Title")
        static let PendingRequest = NSLocalizedString("Pending Request", comment: "Pending Request Title")
        static let PriceDeals = NSLocalizedString("Price Deals", comment: "Price Deals  Title")
        static let ApprovalOffer = NSLocalizedString("Approval Offer", comment: "Approval Offer Title")
        static let Rating = NSLocalizedString("Rating", comment: "Rating Title")

    }
    
    public struct RiderBooking {
        static let CardNotAdded = NSLocalizedString("Please select Payment mode", comment: "Card not Add")
    }
    public struct RiderNotificationsMsg {
        static let AggreagteAssign = NSLocalizedString("Your aggregate trip has been assigned by our Managers to Drivers.\nIt will be started from the first week of next month.", comment: "Aggeragte trip assign")
    }
    
    public struct DriverOnTrip {
        static let Drop = NSLocalizedString("Do You really want to end trip", comment: "End trip")
        static let WannaStart = NSLocalizedString("Do You  want to start the ride", comment: "start trip")
        static let StartNow = NSLocalizedString("You can start trip now", comment: "start trip now")
        static let WaitRider = NSLocalizedString("Wait For Rider To Reach", comment: "Wait for rider")
        static let RiderComing = NSLocalizedString("Rider is Coming", comment: "Rider coming")
        static let  WannaCancel = NSLocalizedString("Do you want to cancel?", comment: "cancel ride")
        static let  RideCancelMsg = NSLocalizedString("If you do not cancel in 10 min. you can be charged.", comment: "cancel Msg")
        static let  RiderNotReached = NSLocalizedString("Rider not reached in given time ?", comment: "cancel Msg")
        static let  RiderWait = NSLocalizedString("Waiting for rider's arrival", comment: "Rider wait Msg")
        static let RideCanceled = NSLocalizedString("Ride canceled by rider", comment: "Ride canceled by rider")
        struct Days {
            static let Today = NSLocalizedString("Today", comment: "Today")
            static let Yesterday = NSLocalizedString("Yesterday", comment: "Yesterday")
            static let Last7Days = NSLocalizedString("Last 7 days", comment: "Last 7 days")
            static let Tomorrow = NSLocalizedString("Tomorrow", comment: "Tomorrow")
            static let ThisWeek = NSLocalizedString("This Week", comment: "ThisWeek")
            static let ThisMonth = NSLocalizedString("This Month", comment: "ThisMonth")
            static let LastMonth = NSLocalizedString("Last Month", comment: "LastMonth")
            static let All = NSLocalizedString("All", comment: "All")
        }
        static let TripPending = NSLocalizedString("TripGoingOn", comment: "driver still not completed the trip")
        static let SharedTripPending = NSLocalizedString("shareTripGoingOn", comment: "driver still not completed the share trip")
        static let CompleteTrip = NSLocalizedString("Complete Trip", comment: "Complete Trip")

        
        struct Aggregate{
            static let MorningCompleted = NSLocalizedString("You have completed your Arrival Trip", comment: "Morning trip completed")
            static let EveningCompleted = NSLocalizedString("You have completed your Return Trip", comment: "Evening trip completed")
            
        }
    }
    
    public struct DriverSideMenuItem {
        static let HOME = NSLocalizedString("HOME", comment: "HOME")
        static let MYTRIPS = NSLocalizedString("MY TRIPS", comment: "MY TRIPS")
        static let MYWALLET = NSLocalizedString("MY WALLET", comment: "MY WALLET")
        static let POINTS = NSLocalizedString("POINTS", comment: "POINTS")
        static let NOTIFICATION = NSLocalizedString("NOTIFICATION", comment: "NOTIFICATION")
        static let INVITE = NSLocalizedString("INVITE", comment: "INVITE")
        static let LOGOUT = NSLocalizedString("LOGOUT", comment: "LOGOUT")
    }
    
    public struct DriverPopUpMessage {
        static let NewAggregate = NSLocalizedString("You have a aggregate request", comment: "NewAggregate")
        static let DropAllFirst = NSLocalizedString("Please drop all rider before ending Aggregate", comment: "DropAllFirst")

        static let CancelReservation = NSLocalizedString("Do you really want to cancel reservation", comment: "CancelReservation")
        static let CancelRide = NSLocalizedString("Do you really want to cancel ride", comment: "CancelRide")
        static let ProfileInactiveInfo = NSLocalizedString("If you make any changes, your status will turn to inactive for the confirming issues and will be active again after confirmed the changed data.", comment: "ProfileInactiveInfo")

    }
    public struct VehilceDetail {
        struct Colors {
            static let Black = NSLocalizedString("Black", comment: "Black")
            static let Blue = NSLocalizedString("Blue", comment: "Blue")
            static let White = NSLocalizedString("White", comment: "White")
            static let Red = NSLocalizedString("Red", comment: "Red")
            static let Yellow = NSLocalizedString("Yellow", comment: "Yellow")
            static let Green = NSLocalizedString("Green", comment: "Green")
            static let Grey = NSLocalizedString("Grey", comment: "Grey")
            
        }
        struct RowTitles {
            static let VEHICLE = NSLocalizedString("VEHICLE TYPE", comment: "VEHICLE TYPE")
            static let BRAND = NSLocalizedString("BRAND", comment: "BRAND")
            static let MODEL = NSLocalizedString("MODEL", comment: "MODEL")
            static let PLATE = NSLocalizedString("PLATE", comment: "PLATE")
            static let YEAR = NSLocalizedString("YEAR", comment: "YEAR")
            static let COLOR = NSLocalizedString("COLOR", comment: "COLOR")
            static let SEAT = NSLocalizedString("SEAT SELECTION(Except Driver Seat)", comment: "SEAT")
        }
        static let CancelReservation = NSLocalizedString("Do you really want to cancel reservation", comment: "CancelReservation")
    }
    public struct RiderSideMenuItem {
        static let HOME = NSLocalizedString("HOME", comment: "HOME")
        static let INSTANT = NSLocalizedString("INSTANT TRANSFER", comment: "INSTANT TRANSFER")
        static let RESERVATION = NSLocalizedString("RESERVATION", comment: "RESERVATION")
        static let AGGREGATE = NSLocalizedString("AGGREGATE", comment: "AGGREGATE")
        static let NOTIFICATION = NSLocalizedString("NOTIFICATION", comment: "NOTIFICATION")
        static let TRIPS = NSLocalizedString("TRIPS", comment: "TRIPS")
        static let MYWALLET = NSLocalizedString("MY WALLET", comment: "MY WALLET")
        static let INVITE = NSLocalizedString("INVITE", comment: "INVITE")
        static let LOGOUT = NSLocalizedString("LOGOUT", comment: "LOGOUT")
        static let MailAlert = NSLocalizedString("Your mailbox is not configured.\nPlease configure your mailbox first.", comment: "Mailbox")
          static let MsgAlert = NSLocalizedString("Issue with Messages ", comment: "MsgAlert")
    }
    public struct ActivityIndicator {
        static let Loading = NSLocalizedString("Loading...", comment: "Loading...")
        static let Logging = NSLocalizedString("Logging In...", comment: "Logging In...")
    }
    
    
    public struct SignIn {
        static let RePassword = NSLocalizedString("Password must be same", comment: "Miss Matched password")
    }
    public struct RiderAlerts {
        static let DropLocation = NSLocalizedString("Please select the destination location", comment: "Drop Location Missed")
        static let DriverNotAvailable = NSLocalizedString("Driver not found", comment: "Driver not found")
        static let InstantCancelRide = NSLocalizedString("Are you sure to cancel the ride?", comment: "InstantCancelRide")
        static let PenaltyCancelRide = NSLocalizedString("You would be charged a penalty if you cancel the trip.", comment: "PenaltyCancelRide")
        static let InternetNotWorking = NSLocalizedString("Seems to be internet not working", comment: "Cancel Instant")
        static let TimePassed = NSLocalizedString("Seems to be you exceed the time to accept", comment: "TimePassed")
        static let JumpToSpecial = NSLocalizedString("Jumping to special to lower your costing", comment: "SpecialJump")
        static let NoDriverCurrently = NSLocalizedString("Currently, there are no drivers available", comment: "No Driver")
        static let BusyCar = NSLocalizedString("Our all cars are busy right now ", comment: "BusyCar")
        static let RemoveReservation = NSLocalizedString("Do you want to remove this reservation", comment: "RemoveReservation")
        static let RemoveFutureReservation = NSLocalizedString("If you cancel this reservation, you will be charged a penalty fee. Would you like to cancel the reservation service?", comment: "RemoveFutureReservation")

        static let RemoveAggregate = NSLocalizedString("Do you want to remove this aggregate", comment: "RemoveAggregate")
        static let DriverCancelTrip = NSLocalizedString("Your trip is canceled by Driver", comment: "DriverCancelTrip")

    }
    
    public struct Register {
        static let EmptyFields = NSLocalizedString("Please fill the mandatory fields", comment: "Empty field")
        static let InvalidEmail = NSLocalizedString("Please enter a valid email id", comment: "Invalid email message")
        static let VehicleInProgress = NSLocalizedString("Vehicle In-process for verification.For change Contact Us", comment: "Vehicle InProgress message")
    }
    public struct ValidationString {
        static let EMptyProfileImage = NSLocalizedString("Please Select the Profile Image", comment: "Empty Image")
        static let EmptyName = NSLocalizedString("Please fill the Name", comment: "Empty Name")
        static let EmptyEmail = NSLocalizedString("Please fill the Email", comment: "Empty Email")
        static let EmptyAddress = NSLocalizedString("Please fill the Address", comment: "Empty Address")
        static let InvalidEmail = NSLocalizedString("Please fill Valid Email", comment: "Invalid Email")
        static let EmptyPassword = NSLocalizedString("Please fill the  Password", comment: "Empty Password")
        static let EmptyCompany = NSLocalizedString("Please fill Company Name", comment: "Empty Company Name")
        static let EmptyAccountName = NSLocalizedString("Please fill Account Name", comment: "Empty Account Name")
        static let EmptyBankAccount = NSLocalizedString("Please fill Account Number", comment: "Empty Account Number")
        static let EmptyTopic = NSLocalizedString("Please fill Topic", comment: "Empty topic")
        static let EmptyMessage = NSLocalizedString("Please fill Message", comment: "Empty Message")
        static let EmptyAmount = NSLocalizedString("Please fill Amount", comment: "Empty Amount")
    }
    public struct Copied{
        
        static let ContentCopied = NSLocalizedString("Content copied", comment: "for code copy")
    }
    public struct NetworkError {
        static let NoNetwork = NSLocalizedString("No Network connectivity.\nPlease check your network settings", comment: "Device Offline")
    }
    public struct InValidQRCode {
        static let QRNotValid = NSLocalizedString("Invalid QR Code. Please try again", comment: "Invalid QR code")
    }
    public struct Settings {
        static let Logout = NSLocalizedString("Are you sure you want to logout?", comment: "Logout")
        static let EditProfile = NSLocalizedString("Edit Profile", comment: "Edit Profile Title")
        static let Name = NSLocalizedString("Name", comment: "name setting")
        static let Links = NSLocalizedString("Links", comment: "links setting")
        static let AvatarUploaded = NSLocalizedString("You’ve updated your Avatar.\n\nIt may take a few minutes for your new avatar/header to appear on Witz, so please be patient. It’ll be live soon!", comment: "Avatar updated copy")
        static let CoverImageUploaded = NSLocalizedString("You’ve updated your Header.\n\nIt may take a few minutes for your new avatar/header to appear on Witz, so please be patient. It’ll be live soon!", comment: "Cover Image updated copy")
    }
    public struct PIN {
        static let EmptyPIN = NSLocalizedString("Please enter PIN in previous box", comment: "PIN Alert")
    }
    public struct MakeACall {
        static let DeviceNotMakeCallCurrently = NSLocalizedString("Device cannot place a call at this time.\nSIM might be removed", comment: "CallNotSupport")
        static let CallNotSupported = NSLocalizedString("This device does not have calling feature", comment: "CallNotSupport")
    }
    public struct UploadDocument {
        static let LimitReached = NSLocalizedString("You can upload maximum 5 images of particular Document", comment: "LimitReached")
        static let  MinOneNeeded = NSLocalizedString("Please select document first", comment: "select one atleast")
    }
    public struct NoData {
        static let Category = NSLocalizedString("No data for selected category", comment: "No data")
        static let Coupons = NSLocalizedString("No coupons", comment: "No data")
    }
    
    public struct UserDefaultKeys {
        static let MobileNo = NSLocalizedString("MobileNo", comment: "Mobile")
        static let UserLogin = NSLocalizedString("UserLogin", comment: "Login")
        static let DriverAccessToken = NSLocalizedString("DriverAccessToken", comment: "Token")
        static let RiderAccessToken = NSLocalizedString("RiderAccessToken", comment: "Token")
        static let SelectedCity = NSLocalizedString("SelectedCity", comment: "City")
        static let IsSearching = NSLocalizedString("IsSearching", comment: "Search")
        static let AddBannerView = NSLocalizedString("AddBannerView", comment: "Banner")
        static let DeviceToken = NSLocalizedString("DeviceToken", comment: "Token")
    }
    public struct KeychainKeys {
        static let UUID = NSLocalizedString("VendorUUID", comment: "UUID")
    }
    
    public struct Subscription {
        static let NotPaid = NSLocalizedString("You are not authorised to redeem this coupon.\nPlease buy your subscription first!", comment: "NotPaid")
        static let COD = NSLocalizedString("Your order is in process", comment: "NotDeliverd")
    }
    
    //The network connection was lost
    
    public struct AlamofireNetworkError {
        static let ConnectionLost = NSLocalizedString("The network connection was lost.", comment: "ConnectionLost")
    }
    
    public struct Profile {
        static let Title = NSLocalizedString("Profile", comment: "Profile Title")
        static let Mention = NSLocalizedString("@ Mention", comment: "Mention button title")
        static let EditProfile = NSLocalizedString("Edit Profile", comment: "Edit Profile button title")
    }
    
    public struct PushNotifications {
        static let PermissionPrompt = NSLocalizedString("Witz would like to send you push notifications.\n\nWe will let you know when you have new notifications. You can makes changes in your settings.\n", comment: "Turn on Push Notifications prompt")
        static let PermissionYes = NSLocalizedString("Yes please", comment: "Allow")
        static let PermissionNo = NSLocalizedString("No thanks", comment: "Disallow")    
    }
    
    public struct ImagePicker {
        static let ChooseSource = NSLocalizedString("Choose a photo source", comment: "choose photo source (camera or library)")
        static let Camera = NSLocalizedString("Camera", comment: "camera button")
        static let Library = NSLocalizedString("Library", comment: "library button")
        static let NoSourceAvailable = NSLocalizedString("Sorry, but your device doesn’t have a photo library!", comment: "device doesn't support photo library")
        static let TakePhoto = NSLocalizedString("Take Photo Or Video", comment: "Camera button")
        static let PhotoLibrary = NSLocalizedString("Photo Library", comment: "Library button")
        static let AddImagesTemplate = NSLocalizedString("Add %lu Image(s)", comment: "Add Images")
    }
    
    static let GenericError = NSLocalizedString("Something went wrong!", comment: "Generic error message")
    static let UnknownError = NSLocalizedString("Unknown error", comment: "Unknown error message")
    
    static let Yes = NSLocalizedString("Yes", comment: "Yes")
    static let No = NSLocalizedString("No", comment: "No")
    static let Cancel = NSLocalizedString("Cancel", comment: "Cancel")
    static let Retry = NSLocalizedString("Retry", comment: "Retry")
    static let AreYouSure = NSLocalizedString("Are You Sure?", comment: "are you sure question")
    static let OK = NSLocalizedString("OK", comment: "OK")
    static let ThatIsOK = NSLocalizedString("It’s OK, I understand!", comment: "It’s OK, I understand!")
    static let Delete = NSLocalizedString("Delete", comment: "Delete")
    static let Next = NSLocalizedString("Next", comment: "Next button")
    static let Done = NSLocalizedString("Done", comment: "Done button title")
    static let Skip = NSLocalizedString("Skip", comment: "Skip action")
    static let SettingsAction = NSLocalizedString("Settings", comment: "Settings")
    static let Add = NSLocalizedString("Add", comment: "Add")
}
