//
//  Maps.swift
//  Witz
//
//  Created by Malvinder Singh on 06/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import GoogleMaps

class Maps {
    
    func initMap(_latitude: Double, _longitude: Double, _zoom: Float) -> GMSMapView {
        let camera = GMSCameraPosition.camera(withLatitude: _latitude, longitude: _longitude, zoom: _zoom)
        
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        
        let marker = GMSMarker()
        marker.position = camera.target
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.icon =  UIImage(named: "flag_icon")
        marker.map = mapView
        
        return mapView
    }
    
    
}
