//
//  ReverseGeocode.swift
//  Witz
//
//  Created by Amit Tripathi on 10/24/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps

class ReverseGeocode: NSObject {

    class func getAddressByCoords(coords:CLLocationCoordinate2D, returnAddress: @escaping ( _ returnAddress :String)->Void) {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(coords.latitude, coords.longitude)
        var currentAddress = String()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                currentAddress = lines.joined(separator: "\n")
                print ("address---GMSGeocoder--->>>>>>>>>>\(currentAddress)")
                returnAddress(currentAddress)
            }
        }
    }
    
    class func getAddressByCoords_(coords: CLLocationCoordinate2D)->String {
        
        let location = CLLocation(latitude: coords.latitude, longitude: coords.longitude)
        var adressString:String = ""
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            if error != nil {
                print("error----->>>\(String(describing: error?.localizedDescription))")
            } else {
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + "\n"
                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + "\n"
                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                    print ("address--CLGeocoder----->>>>>>>>>>\(adressString)")
                    
                }
            }
        }
        return adressString
    }
}
