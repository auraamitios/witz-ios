//
//  GeocodeHelper.swift
//  Witz
//
//  Created by Amit Tripathi on 10/24/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
class GeocodeHelper: NSObject {
    
    class func getLocationFromAddress(from address:String, returnData: @escaping ( _ returnLoc :CLLocationCoordinate2D, _ info:JSON)->Void) {
        var lat : Double = 0.0
        var lon : Double = 0.0
        
        do {
            let url = String(format: "https://maps.google.com/maps/api/geocode/json?sensor=false&address=%@&key=\(GOOGLE_MAP_API_KEY)", (address.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!))
            let result = try Data(contentsOf: URL(string: url)!)
            let json = JSON(data: result)
            print(json)
            
            lat = json["results"][0]["geometry"]["location"]["lat"].doubleValue
            lon = json["results"][0]["geometry"]["location"]["lng"].doubleValue
            
            returnData(CLLocationCoordinate2D(latitude: lat, longitude: lon), json)
            
        }
        catch let error{
            print(error)
        }
    }
}
