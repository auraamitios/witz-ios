//
//  GooglePathHelper.swift
//  Witz
//
//  Created by Amit Tripathi on 10/26/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
var hitsLimit_Google : Int?

class GooglePathHelper: NSObject {
    
    

    class func getPathPolyPoints(from source:CLLocationCoordinate2D, destination:CLLocationCoordinate2D, returnPath: @escaping ( _ returnPath :String, _ distance:[Any])->Void) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving&key=\(GOOGLE_DIRECTIONS_API_KEY)")!
        
        var currentPath = String()
        var legsArry = [Any]()
        
        print("Directions URL------>\(url)")
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        let routes = json["routes"] as? [Any]
                        if (routes?.count ?? 0) == 0 {
                            if hitsLimit_Google != nil {
                                hitsLimit_Google = hitsLimit_Google!-1
                                if hitsLimit_Google == 0{
                                    return
                                }
                            }
                            self.getPathPolyPoints(from: source, destination: destination, returnPath: { (returnPath, distance) in
                                
                            })
                            
                            return
                        }
                        let overview_polyline = routes?[0] as? [String:Any]
                        let polyString = overview_polyline?["overview_polyline"] as?[String:Any]
                        let pointsStr = polyString!["points"] as? String
                        let legsData = overview_polyline?["legs"] as? [Any]
                        legsArry = legsData! 
                        if pointsStr == nil {
                            return
                        }
                        currentPath = pointsStr!
                        returnPath(currentPath, legsArry)
                       
                    }
                }catch{
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    class func getPathPolyPointsWalking(from source:CLLocationCoordinate2D, destination:CLLocationCoordinate2D, returnPath: @escaping ( _ returnPath :String, _ distance:[Any])->Void) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=walking&key=\(GOOGLE_DIRECTIONS_API_KEY)")!
        
        var currentPath = String()
        var legsArry = [Any]()
        
        print("Directions URL------>\(url)")
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        let routes = json["routes"] as? [Any]
                        if (routes?.count ?? 0) == 0 {
                            if hitsLimit_Google != nil {
                                hitsLimit_Google = hitsLimit_Google!-1
                                if hitsLimit_Google == 0{
                                    return
                                }
                            }
                            self.getPathPolyPoints(from: source, destination: destination, returnPath: { (returnPath, distance) in
                                
                            })
                            
                            return
                        }
                        let overview_polyline = routes?[0] as? [String:Any]
                        let polyString = overview_polyline?["overview_polyline"] as?[String:Any]
                        let pointsStr = polyString!["points"] as? String
                        let legsData = overview_polyline?["legs"] as? [Any]
                        legsArry = legsData!
                        if pointsStr == nil {
                            return
                        }
                        currentPath = pointsStr!
                        returnPath(currentPath, legsArry)
                        
                    }
                }catch{
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
}
