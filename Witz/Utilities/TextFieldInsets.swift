//
//  TextFieldInsets.swift
//  DealXpress
//
//  Created by Amit Tripathi on 2/20/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class TextFieldInsets: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func draw(_ rect: CGRect) {
        //Drawing code
        //self.layer.borderWidth = 1.0
        //self.layer.borderColor = kDarkGreyColor.CGColor
        self.textColor = UIColor.black
    }
    // placeholder position
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
    // text position
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
}


