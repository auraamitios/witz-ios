//
//  AlertHelper.swift
//  DealXpress
//
//  Created by Amit Tripathi on 2/21/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Foundation

struct AlertHelper {
    
    static func displayActionSheet(title:String, message:String, style:UIAlertControllerStyle, actionButtonTitle:[String],completionHandler:@escaping (UIAlertAction)->Void,controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle[0], style: UIAlertActionStyle.default, handler: completionHandler))
        alert.addAction(UIAlertAction(title: actionButtonTitle[1], style: UIAlertActionStyle.default, handler: completionHandler))
        alert.addAction(UIAlertAction(title:  actionButtonTitle[2], style: UIAlertActionStyle.cancel, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func displayAlert(title:String, message:String, style:UIAlertControllerStyle, actionButtonTitle:[String],completionHandler:@escaping (UIAlertAction)->Void,controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle[0], style: UIAlertActionStyle.default, handler: completionHandler))
        alert.addAction(UIAlertAction(title:  actionButtonTitle[1], style: UIAlertActionStyle.default, handler: completionHandler))
        controller.present(alert, animated: true, completion: nil)
        
    }
    static func displayOkAlert(title:String, message:String, style:UIAlertControllerStyle, actionButtonTitle:String,completionHandler:@escaping (UIAlertAction)->Void,controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle, style: UIAlertActionStyle.default, handler: completionHandler))
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func displayAlertViewOKButton(title:String, message:String, style:UIAlertControllerStyle, actionButtonTitle:String,completionHandler:@escaping (UIAlertAction)->Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle, style: UIAlertActionStyle.default, handler: completionHandler))
        UIApplication.shared.keyWindow!.rootViewController!.present(alert, animated: true, completion: { _ in })
    }
    
    static func displayAlertView(title:String, message:String, style:UIAlertControllerStyle, actionButtonTitle:[String],completionHandler:@escaping (UIAlertAction)->Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle[0], style: UIAlertActionStyle.default, handler: completionHandler))
        alert.addAction(UIAlertAction(title:  actionButtonTitle[1], style: UIAlertActionStyle.default, handler: nil))
        UIApplication.shared.keyWindow!.rootViewController!.present(alert, animated: true, completion: { _ in })
    }
    static func showAlertWithTitle(with title:String) {
        let alertController = UIAlertController(title: "Error", message: title, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        UIApplication.shared.keyWindow!.rootViewController!.present(alertController, animated: true, completion: { _ in })    }
    
    static func showViewController(on topVc:UIViewController){
        var vc = UIApplication.shared.keyWindow!.rootViewController!
        while (vc.presentedViewController != nil) {
            vc = vc.presentedViewController!
        }
        vc.present(topVc, animated: true, completion: nil)
    }
    
    static func showSettingsAlertView() {
        DispatchQueue.main.async(execute: {() -> Void in
            let alertController = UIAlertController(title: LinkStrings.AlertTitle.Title, message: LinkStrings.NetworkError.NoNetwork, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            })
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        //print("Settings opened: \(success)") // Prints true
                    })
                }
            }
            alertController.addAction(cancelAction)
            alertController.addAction(settingsAction)
            UIApplication.shared.keyWindow!.rootViewController!.present(alertController, animated: true, completion: { _ in })
        })
    }
}
