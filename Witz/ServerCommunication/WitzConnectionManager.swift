//
//  WitzConnectionManager.swift
//  Witz
//
//  Created by Amit Tripathi on 9/6/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON


//MARK:: Xpress Manager delegate
protocol WitzConnectionManagerDelegate:NSObjectProtocol {
    func responseReceived_WithAPIType(sender: AnyObject, responseData:AnyObject, serviceType:APIType)
    func didFailWithError(sender: AnyObject)
}


//MARK:: Witz Connection enums
class WitzConnectionManager: NSObject {
    
    //let obj = AlamofireClient()
    weak var delegate:WitzConnectionManagerDelegate!
    //var currentService:Current_Service!
    
 //MARK:- Simple UrlEndcoded Type
        func makeAPICall(functionName: String, withInputString params: Parameters?, requestType:HTTPMethod,withCurrentTask inputTask: APIType, isAuthorization: Bool)->() {
    
        //self.currentService! = inputTask;
        
        if !Utility.isConnectedToNetwork() {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.NetworkError.NoNetwork, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler)
            return
        }

        let url : NSURL = NSURL.init(string: "\(kBaseURL)\(functionName)")!
        let apiUrl = url.absoluteURL
        print("requestURL:\(String(describing: apiUrl))")
            AlamofireClient.sharedInstance.delegate = self
        AlamofireClient.sharedInstance.alamofireAPIRequest(apiUrl: apiUrl!, withInputString: params, requestType: requestType, withCurrentTask: inputTask, isAuthorization: isAuthorization)

    }
    
    //MARK:- Define the Header type too
    func makeAPICall(functionName: String, withInputString params: Parameters?, requestType:HTTPMethod,withCurrentTask inputTask: APIType?, isAuthorization: Bool ,headerType:HeaderType ,arr_img:[UIImage]? , success:successHanlder?,failure:failureHanlder?)->() {

        if !Utility.isConnectedToNetwork() {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.NetworkError.NoNetwork, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler)
            return
        }
        let url : NSURL = NSURL.init(string: "\(kBaseURL)\(functionName)")!
        let apiUrl = url.absoluteURL
        print("requestURL:\(String(describing: apiUrl))")
        AlamofireClient.sharedInstance.alamofireAPIRequest(apiUrl: apiUrl!, withInputString: params, requestType: requestType, withCurrentTask: inputTask, isAuthorization: isAuthorization,headerType:headerType,arr_img:arr_img,success: { (response) in
            
            if success != nil{
                success!(response)
            }
        }, failure: { (error) in
            
            if failure != nil{
                
                failure!(error)
            }
        })
    }
    
    //MARK:- Define urlencoded method to use
    func makeAPICallUrlEncoded(functionName: String, withInputString params: Parameters?, requestType:HTTPMethod, isAuthorization: Bool , success:successHanlder?,failure:failureHanlder?)->() {
        
        if !Utility.isConnectedToNetwork() {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.NetworkError.NoNetwork, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler)
            return
        }
        
        let url : NSURL = NSURL.init(string: "\(kBaseURL)\(functionName)")!
        let apiUrl = url.absoluteURL
        print("requestURL:\(String(describing: apiUrl))")
        AlamofireClient.sharedInstance.HTTPMethodCalling(api:apiUrl!,  isAuthorization:isAuthorization , requestType:requestType, params:params!, success: { (response) in
            //
            if success != nil{
                success!(response)
            }
        }) { (error) in
            
            if failure != nil{
                
                failure!(error)
            }
        }
    }
}

extension WitzConnectionManager:AlamofireClientDelegate {
    
    func success(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {

        switch serviceType {
        //MARK::Mobile_Verification_Driver 🥝
        case .Mobile_Verification_Driver:
            if (self.delegate != nil) { 
                self.delegate.responseReceived_WithAPIType(sender: sender, responseData: responseData, serviceType: .Mobile_Verification_Driver)
            }
            break
        //MARK::Confirm_Mobile_Verification_Driver 🥝
        case .Confirm_Mobile_Verification_Driver:
            if (self.delegate != nil) {
                self.delegate.responseReceived_WithAPIType(sender: sender, responseData: responseData, serviceType: .Confirm_Mobile_Verification_Driver)
            }
            break
        //MARK::Create_Password_Driver 🥝
        case .Create_Password_Driver:
            let driverInfo = DriverInfo.parseDriverInfo(data: responseData as! [String:JSON] , outerData: sender as! JSON)
           
                DriverManager.sharedInstance.user_Driver = driverInfo
            if (self.delegate != nil) {
                self.delegate.responseReceived_WithAPIType(sender: sender,responseData: responseData, serviceType: .Create_Password_Driver)
            }
            break
        //MARK::Create_Driver 🥝
        case .Create_Driver:
            if (self.delegate != nil) {
                self.delegate.responseReceived_WithAPIType(sender: sender, responseData: responseData,serviceType: .Create_Driver)
            }
            break
        //MARK::Forgot_Password 🥝
        case .Forgot_Password:
            if (self.delegate != nil) {
                self.delegate.responseReceived_WithAPIType(sender: sender, responseData: responseData,serviceType: .Forgot_Password)
            }
            break
        //MARK::Driver_Login 🥝
        case .Driver_Login:
            let driverInfo = DriverInfo.parseDriverInfo(data: responseData as! [String:JSON] , outerData: sender as! JSON)
            if (self.delegate != nil) {
                self.delegate.responseReceived_WithAPIType(sender: sender, responseData: driverInfo as AnyObject,serviceType: .Driver_Login)
            }
            break
        //MARK::Driver_Info 🥝
        case .Driver_Info:
            let driverInfo = DriverInfo.parseDriverInfo(data: responseData as! [String:JSON] , outerData: sender as! JSON)
            if (self.delegate != nil) {
                self.delegate.responseReceived_WithAPIType(sender: sender, responseData: driverInfo as DriverInfo as AnyObject,serviceType: .Driver_Info)
            }
            break
        //MARK::Driver_Duty 🥝
        case .Driver_Duty:
            if (self.delegate != nil) {
                self.delegate.responseReceived_WithAPIType(sender: sender, responseData: responseData,serviceType: .Driver_Duty)
            }
            break
        //MARK::Driver_Logout 🥝
        case .Driver_Logout:
            if (self.delegate != nil) {
                self.delegate.responseReceived_WithAPIType(sender: sender, responseData: responseData,serviceType: .Driver_Logout)
            }
            break

        default:
            break
        }
    } 
    
    func failed(sender: AnyObject) {
        
        if (self.delegate != nil) {
            self.delegate.didFailWithError(sender: sender)
        }
    }
}
