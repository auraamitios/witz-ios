//
//  AlamofireClient.swift
//  Witz
//
//  Created by Amit Tripathi on 9/19/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

//MARK:: Client delegate
protocol AlamofireClientDelegate:NSObjectProtocol {
    func success(sender: AnyObject, responseData:AnyObject, serviceType:APIType)
    func failed(sender: AnyObject)
}

/*Closure made for calling api thru blocks*/
typealias successHanlder = (_ response:JSON? )->Void
typealias failureHanlder = (_ error:Error?)->Void
typealias progressHanlder = (_ progress:Double)->Void


enum HeaderType:Int {
    case Form  = 20
    case URLENCODED
}
class AlamofireClient: NSObject {
    
    weak var delegate:AlamofireClientDelegate!
    static let sharedInstance = AlamofireClient()
    private override init() {} //This prevents others from using the default '()' initializer for this class.
    
    //MARK:: Method type 1
    func alamofireAPIRequest(apiUrl:URL, withInputString params: Parameters?, requestType:HTTPMethod, withCurrentTask inputTask: APIType, isAuthorization:Bool )->() {
        
        //        let headers: HTTPHeaders = [
        //            "Content-type": "application/x-www-form-urlencoded"
        //        ]
        
        let headers: HTTPHeaders!
        if !isAuthorization {
            headers =  ["Content-type": "application/x-www-form-urlencoded"]
        }else {
            if AppManager.sharedInstance.loggedUserRider {
                let tokenKey = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.RiderAccessToken) as! String
                let tokenStr = "bearer "+tokenKey
                headers = [
                    "Content-type": "application/x-www-form-urlencoded", "authorization":tokenStr
                ]
            }else {
                let tokenKey = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DriverAccessToken) as! String
                let tokenStr = "bearer "+tokenKey
                headers =  [
                    "Content-type": "application/x-www-form-urlencoded", "authorization":tokenStr
                ]
                
            }
        }
        
        Alamofire.request(apiUrl, method:requestType, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            //MARK:: Success case
            case .success(_):
                if response.result.value != nil{
                    //                    print("request body--->>>>>\(String(data: (response.request?.httpBody)!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!)")
                    //print("debug request header---->\(String(describing: response.response?.allHeaderFields))")
                    //print("debug request header count---->\(String(describing: response.response?.allHeaderFields.count))")
                    
                    //                    print("response data*********-->\(response.result.value!)")
                    let outerData = JSON(response.result.value!)
                    let swiftyJsonVar = JSON(response.result.value!)["data"].dictionary
                    let swiftyDataArry = JSON(response.result.value!)["data"].array
                    let statusCode = outerData["statusCode"].doubleValue
                    if statusCode == 401{
                        AppManager.sharedInstance.loggedUserRider = false
                        UserDefaultManager.sharedManager.removeValue(key:LinkStrings.UserDefaultKeys.DriverAccessToken)
                        UserDefaultManager.sharedManager.removeValue(key:LinkStrings.UserDefaultKeys.RiderAccessToken)
                        Utility.jumpToLoginScreen()
                        return
                    }
                    var dataArryPresent:Bool = false
                    if swiftyDataArry != nil {
                        dataArryPresent = true
                    }
                    guard (swiftyJsonVar != nil || swiftyDataArry != nil) || statusCode == 200 else {
                        let swiftyiVar = JSON(response.result.value!)
                        let responseInfo = ServerResponse.parseResponse(data: swiftyiVar)
                        if (self.delegate != nil) {
                            self.delegate.failed(sender: responseInfo as AnyObject)
                        }
                        return
                    }
                    
                    
                    if (self.delegate != nil)  {
                        if !dataArryPresent {
                            self.delegate.success(sender: outerData as AnyObject, responseData: swiftyJsonVar as AnyObject, serviceType: inputTask)
                        }else {
                            self.delegate.success(sender: outerData as AnyObject, responseData: swiftyDataArry as AnyObject, serviceType: inputTask)
                        }
                    }
                }
                break
            //MARK:: Failure case
            case .failure(_):
                //                print("error********\(String(describing: response.error))")
                //                print(response.result.error!)
                
                if response.result.value == nil {
                    let error = response.result.error?.localizedDescription
                    if (self.delegate != nil) {
                        self.delegate.failed(sender: error as AnyObject)
                    }
                }else {
                    let swiftyData = JSON(response.result.value!)
                    if swiftyData == JSON.null {
                        return
                    }
                    let serverError = ServerError.parseErrorData(error: swiftyData)
                    if (self.delegate != nil) {
                        self.delegate.failed(sender: serverError as AnyObject)
                    }
                }
                break
            }
        }
    }
    
    //MARK:: Method type 2
    //MARK:: Alamofire Request with explict header type
    func alamofireAPIRequest(apiUrl:URL, withInputString params: Parameters?, requestType:HTTPMethod, withCurrentTask inputTask: APIType?, isAuthorization:Bool , headerType:HeaderType,arr_img:[UIImage]? , success:successHanlder?,failure:failureHanlder?)->() {
        
        Utility.showNetworkIndicator()
        
        let headers: HTTPHeaders!
        if !isAuthorization {
            headers = {
                if headerType == HeaderType.Form{
                    return ["Content-type": "multipart/form-data; boundary=file"]
                }else{
                    return ["Content-type": "application/x-www-form-urlencoded"]
                }
            }()
        }else {
            if AppManager.sharedInstance.loggedUserRider {
                let tokenKey = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.RiderAccessToken) as! String
                let tokenStr = "bearer "+tokenKey
                headers = {
                    if headerType == HeaderType.Form{
                        return [
                            "Content-type": "multipart/form-data; boundary=file", "authorization":tokenStr
                        ]
                    }else{
                        return [
                            "Content-type": "application/x-www-form-urlencoded", "authorization":tokenStr
                        ]
                    }
                }()
            }else {
                let tokenKey = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DriverAccessToken) as! String
                let tokenStr = "bearer "+tokenKey
                headers = {
                    if headerType == HeaderType.Form{
                        return [
                            "Content-type": "multipart/form-data; boundary=file", "authorization":tokenStr
                        ]
                    }else{
                        return [
                            "Content-type": "application/x-www-form-urlencoded", "authorization":tokenStr
                        ]
                    }
                }()
            }
        }
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if arr_img != nil{
                
                for imgPic in arr_img!{
                    let imageData = UIImageJPEGRepresentation(imgPic , 0.5)
                    multipartFormData.append(imageData!, withName: "pic", fileName: "swift_file\(arc4random_uniform(100)).jpeg", mimeType: "image/jpeg")
                }
                
            }
            for key in (params?.keys)!{
                let name = String(key)
                if let val = params?[name!] as? String{
                    multipartFormData.append(val.data(using: .utf8)!, withName: name!)
                }
            }
        }, to:apiUrl, method: requestType, headers: headers)
        { (result) in
            
            Utility.hideNetworkIndicator()
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                })
                
                upload.responseJSON { response in
                    //                    print (response)
                    if response.response?.statusCode == 200{
                        switch(response.result){
                        case .success(_):
                            if response.data != nil{
                                
                                var json : JSON?
                                do{
                                    json = try JSON(data: response.data!)
                                    if success != nil{
                                        success!(json)
                                    }
                                }catch _{ }
                            }
                            break
                        case .failure(_):
                            if failure != nil{
                                
                                failure!(response.result.error)
                            }
                            break
                        }
                    } else if response.response?.statusCode == 401{
                        AppManager.sharedInstance.loggedUserRider = false
                        UserDefaultManager.sharedManager.removeValue(key:LinkStrings.UserDefaultKeys.DriverAccessToken)
                        UserDefaultManager.sharedManager.removeValue(key:LinkStrings.UserDefaultKeys.RiderAccessToken)
                        Utility.jumpToLoginScreen()
                    }
                    else{
                        switch(response.result){
                        case .success(_):
                            if response.data != nil{
                                
                                var json : JSON?
                                do{
                                    json = try JSON(data: response.data!)
                                    if success != nil{
                                        success!(json)
                                    }
                                }catch _{ }
                            }
                            break
                        case .failure(_):
                            if failure != nil{
                                
                                failure!(response.result.error)
                            }
                            break
                        }
                    }
                }
                break
            case .failure(let encodingError):
                if failure != nil{
                    
                    failure!(encodingError)
                }
                //                print(encodingError)
                break
            }
        }
    }
    
    //MARK:: Method type 3
    /**
     method with use of blocks and return the json if success otherwise returns the error on view controller where from its calls
     */
    func HTTPMethodCalling(api:URL,  isAuthorization:Bool , requestType:HTTPMethod, params:[String:Any], success:successHanlder?,failure:failureHanlder?){
        
        Utility.showNetworkIndicator()
        let headers: HTTPHeaders!
        if !isAuthorization {
            headers = ["Content-type": "application/x-www-form-urlencoded"]
            
        }else {
            if AppManager.sharedInstance.loggedUserRider {
                let tokenKey = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.RiderAccessToken) as! String
                let tokenStr = "bearer "+tokenKey
                headers =  [
                    "Content-type": "application/x-www-form-urlencoded", "authorization":tokenStr
                ]
                
            }else {
                let tokenKey = UserDefaultManager.sharedManager.objectForKey(key: LinkStrings.UserDefaultKeys.DriverAccessToken) as! String
                let tokenStr = "bearer "+tokenKey
                headers  = [
                    "Content-type": "application/x-www-form-urlencoded", "authorization":tokenStr
                ]
            }
        }
        print("data to api ========== \(params)")
        Alamofire.request(api, method: requestType, parameters: params, headers: headers ).validate(contentType: ["application/json","application/text" , "text/html"])
            .responseJSON { (response:DataResponse<Any>) in
                //print (response)
                //                print("response data*********-->\(String(describing: response.result.value))")
                Utility.hideNetworkIndicator()
                if response.response?.statusCode == 200{
                    switch(response.result){
                    case .success(_):
                        if response.data != nil{
                            var json : JSON?
                            do{
                                json = JSON(data: response.data!)
                                if success != nil{
                                    success!(json)
                                }
                            }
                        }
                        break
                    case .failure(_):
                        if failure != nil{
                            failure!(response.result.error)
                        }
                        break
                    }
                }else if response.response?.statusCode == 400{
                    
                    switch(response.result){
                    case .success(_):
                        if response.data != nil{
                            
                            var json : JSON?
                            do{
                                json =  JSON(data: response.data!)
                                
                                if success != nil{
                                    success!(json)
                                }
                            }
                        }
                        break
                    case .failure(_):
                        if failure != nil{
                            failure!(response.result.error)
                        }
                        break
                    }
                }else if response.response?.statusCode == 401{
                    AppManager.sharedInstance.loggedUserRider = false
                    UserDefaultManager.sharedManager.removeValue(key:LinkStrings.UserDefaultKeys.DriverAccessToken)
                    UserDefaultManager.sharedManager.removeValue(key:LinkStrings.UserDefaultKeys.RiderAccessToken)
                    Utility.jumpToLoginScreen()
                }else{
                    if failure != nil{
                        failure!(response.result.error)
                    }
                }
                
        }
    }
}


