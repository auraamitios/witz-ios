//
//  RiderConnectionManager.swift
//  Witz
//
//  Created by Malvinder Singh on 19/09/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol RiderConnectionManagerDelegate:NSObjectProtocol {
    func responseReceived_WithAPIType(sender: AnyObject, responseData: AnyObject, serviceType: APIType)
    func didFailWithError(sender: AnyObject)
}

//MARK:: Rider Connection enums
class RiderConnectionManager: NSObject {
    
    weak var delegate:RiderConnectionManagerDelegate!
    
    func APICall(functionName: String, withInputString params: Parameters?, requestType: HTTPMethod, withCurrentTask inputTask: APIType, isAuthorization: Bool)->() {
        if !Utility.isConnectedToNetwork() {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.NetworkError.NoNetwork, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler)
            return
        }
        
        Utility.showNetworkIndicator()
        let url: NSURL = NSURL.init(string: "\(kBaseURL)\(functionName)")!
        let apiURL = url.absoluteURL
        print("requested URL: \(String(describing: apiURL))")
        
        AlamofireClient.sharedInstance.delegate = self
        AlamofireClient.sharedInstance.alamofireAPIRequest(apiUrl: apiURL!, withInputString: params, requestType: requestType, withCurrentTask: inputTask, isAuthorization: isAuthorization)
    }
    
    //MARK:- Define the Header type too
    func makeAPICall(functionName: String, withInputString params: Parameters?, requestType:HTTPMethod,withCurrentTask inputTask: APIType, isAuthorization: Bool ,headerType:HeaderType ,arr_img:[UIImage]? , success:successHanlder?,failure:failureHanlder?)->() {
        
        if !Utility.isConnectedToNetwork() {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.NetworkError.NoNetwork, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler)
            return
        }
        
        let url : NSURL = NSURL.init(string: "\(kBaseURL)\(functionName)")!
        let apiUrl = url.absoluteURL
        print("requestURL:\(String(describing: apiUrl))")
        AlamofireClient.sharedInstance.alamofireAPIRequest(apiUrl: apiUrl!, withInputString: params, requestType: requestType, withCurrentTask: inputTask, isAuthorization: isAuthorization,headerType:headerType,arr_img:nil,success: { (response) in
            
            if success != nil{
                success!(response)
            }
        }, failure: { (error) in
            
            if failure != nil{
                
                failure!(error)
            }
        })
        // making call for overloaded function with header type
    }
    
    //MARK:- Define urlencoded method to use
    func makeAPICallUrlEncoded(functionName: String, withInputString params: Parameters?, requestType:HTTPMethod, isAuthorization: Bool , success:successHanlder?,failure:failureHanlder?)->() {
        
        if !Utility.isConnectedToNetwork() {
            let actionHandler = { (action:UIAlertAction!) -> Void in
            }
            AlertHelper.displayAlertViewOKButton(title: LinkStrings.AlertTitle.Title, message: LinkStrings.NetworkError.NoNetwork, style: .alert, actionButtonTitle: LinkStrings.OK, completionHandler: actionHandler)
            return
        }
        
        let url : NSURL = NSURL.init(string: "\(kBaseURL)\(functionName)")!
        let apiUrl = url.absoluteURL
        print("requestURL:\(String(describing: apiUrl))")
        AlamofireClient.sharedInstance.HTTPMethodCalling(api:apiUrl!,  isAuthorization:isAuthorization , requestType:requestType, params:params!, success: { (response) in
//
            if success != nil{
                success!(response)
            }
        }) { (error) in

            if failure != nil{
                failure!(error)
            }
        }

        // making call for overloaded function with header type
    }
}

extension RiderConnectionManager:AlamofireClientDelegate {
    
    func success(sender: AnyObject, responseData: AnyObject, serviceType: APIType) {
        Utility.hideNetworkIndicator()
        switch serviceType {
            case .CONFIRM_RIDER_MOBILE_VERIFICATION:
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: responseData, serviceType: .CONFIRM_RIDER_MOBILE_VERIFICATION)
                }
                break
        
            case .RIDER_MOBILE_VERIFICATION:
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: responseData, serviceType: .RIDER_MOBILE_VERIFICATION)
                }
                break
            
            case .CREATE_RIDER_PASSWORD:
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: responseData, serviceType: .CREATE_RIDER_PASSWORD)
                }
                break
            
            case .CREATE_RIDER:
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: responseData, serviceType: .RIDER_MOBILE_VERIFICATION)
                }
                break
            
            case .LOGIN_RIDER:
                let riderData = RiderInfo.parseRiderInfo(data: responseData as! [String:JSON] , outerData: sender as! JSON)
                RiderManager.sharedInstance.riderInfo = riderData
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: riderData as AnyObject, serviceType: .LOGIN_RIDER)
                }
               break
            
            case .START_INSTANT_RIDE:
                let activerDriverData = ActiveDriver.parseActiveDriverData(data: responseData as! [String:JSON], outerData: sender as! JSON)
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: activerDriverData as AnyObject, serviceType: .START_INSTANT_RIDE)
                }
                break
            
            case .GET_ALL_VEHICLES:
                let vehicleData = VehiclesData.parseVehiclesData(data: responseData as! [JSON], outerData: sender as! JSON)
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: vehicleData as AnyObject, serviceType: .GET_ALL_VEHICLES)
                }
                break
            case .RIDER_LOGOUT:
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: responseData, serviceType: .RIDER_LOGOUT)
                }
                break
            case .ADD_RIDER_TRIP:
                let tripData = AddTripData.parseVehiclesData(data: responseData as! [String:JSON] , outerData: sender as! JSON)
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: tripData as AnyObject, serviceType: .ADD_RIDER_TRIP)
                }
            break
            case .GET_PRICE:
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: responseData, serviceType: .GET_PRICE)
                }
                break
            case .GET_SHARED_PRICE:
                if (self.delegate != nil) {
                    self.delegate.responseReceived_WithAPIType(sender: sender,responseData: responseData, serviceType: .GET_SHARED_PRICE)
                }
                break
            
            default:
                break
        }
    }
    
    func failed(sender: AnyObject) {
        Utility.hideNetworkIndicator()
        if (self.delegate != nil) {
            self.delegate.didFailWithError(sender: sender)
        }
    }
}
